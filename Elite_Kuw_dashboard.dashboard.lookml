- dashboard: elite_dashboard__wip
  title: Elite dashboard - WIP
  layout: newspaper
  elements:
  - name: UAE
    type: text
    title_text: UAE
    row: 5
    col: 0
    width: 24
    height: 2
  - name: Memberwise Stats
    type: text
    title_text: Memberwise Stats
    row: 3
    col: 0
    width: 24
    height: 2
  - name: '2020'
    type: text
    title_text: '2020'
    row: 10
    col: 0
    width: 12
    height: 3
  - name: '2019'
    type: text
    title_text: '2019'
    row: 10
    col: 12
    width: 12
    height: 3
  - title: Total Prospects
    name: Total Prospects
    model: group_customer
    explore: factretailsales
    type: single_value
    fields: [factretailsales.musy_key_count]
    filters:
      base_customers.is_muse_elite: 'Yes'
    sorts: [factretailsales.musy_key_count desc]
    limit: 500
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    limit_displayed_rows: false
    hide_legend: true
    legend_position: center
    series_types: {}
    point_style: none
    show_value_labels: true
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_dropoff: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    show_row_numbers: true
    truncate_column_names: false
    hide_totals: false
    hide_row_totals: false
    table_theme: editable
    defaults_version: 1
    transpose: false
    truncate_text: true
    size_to_fit: true
    header_text_alignment: left
    header_font_size: 12
    rows_font_size: 12
    value_labels: legend
    label_type: labPer
    hidden_fields: []
    listen: {}
    row: 0
    col: 0
    width: 6
    height: 3
  - title: Target Achieved %
    name: Target Achieved %
    model: group_customer
    explore: factretailsales
    type: single_value
    fields: [factretailsales.Sales_Amount_USD, factretailsales.period]
    filters:
      base_customers.is_muse_elite: 'Yes'
    sorts: [factretailsales.period desc]
    limit: 500
    dynamic_fields: [{table_calculation: target_achieved, label: Target Achieved %,
        expression: "${factretailsales.Sales_Amount_USD}/2890277\n", value_format: !!null '',
        value_format_name: percent_0, _kind_hint: measure, _type_hint: number}]
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    font_size_main: '20'
    orientation: auto
    style_target_achieved: "#12165C"
    show_title_target_achieved: true
    defaults_version: 1
    series_types: {}
    show_view_names: false
    show_row_numbers: true
    transpose: false
    truncate_text: true
    hide_totals: false
    hide_row_totals: false
    size_to_fit: true
    table_theme: white
    limit_displayed_rows: false
    header_text_alignment: left
    header_font_size: 12
    rows_font_size: 12
    truncate_column_names: false
    hidden_fields: [factretailsales.Sales_Amount_USD, factretailsales.period]
    hidden_points_if_no: []
    series_labels: {}
    listen:
      Date Range: factretailsales.current_date_range
      Comapred to: factretailsales.compare_to
    row: 0
    col: 18
    width: 6
    height: 3
  - title: Target
    name: Target
    model: group_customer
    explore: factretailsales
    type: single_value
    fields: [factretailsales.atr_trandate_year]
    fill_fields: [factretailsales.atr_trandate_year]
    filters:
      base_customers.is_muse_elite: 'Yes'
    limit: 500
    dynamic_fields: [{table_calculation: target_sales, label: Target Sales, expression: '2890277

          ', value_format: !!null '', value_format_name: usd_0, _kind_hint: dimension,
        _type_hint: number}]
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    value_format: "$##,###"
    conditional_formatting: [{type: equal to, value: !!null '', background_color: "#173589",
        font_color: !!null '', color_application: {collection_id: 1bc1f1d8-7461-4bfd-8c3b-424b924287b5,
          palette_id: 46a4b248-19f7-4e71-9cf0-59fcc2c3039e}, bold: false, italic: false,
        strikethrough: false, fields: !!null ''}]
    defaults_version: 1
    series_types: {}
    show_view_names: false
    show_row_numbers: true
    transpose: false
    truncate_text: true
    hide_totals: false
    hide_row_totals: false
    size_to_fit: true
    table_theme: white
    limit_displayed_rows: false
    header_text_alignment: left
    header_font_size: 12
    rows_font_size: 12
    hidden_fields: [factretailsales.atr_trandate_year]
    listen: {}
    row: 0
    col: 12
    width: 6
    height: 3
  - name: space
    type: text
    title_text: space
    row: 7
    col: 0
    width: 24
    height: 3
  - title: Total Onbaorded
    name: Total Onbaorded
    model: group_customer
    explore: factretailsales
    type: single_value
    fields: [factretailsales.musy_key_count]
    filters:
      base_customers.is_muse_elite: 'Yes'
    sorts: [factretailsales.musy_key_count desc]
    limit: 500
    dynamic_fields: [{table_calculation: onboarded_count, label: Onboarded Count,
        expression: '5', value_format: !!null '', value_format_name: !!null '', _kind_hint: dimension,
        _type_hint: number}]
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    limit_displayed_rows: false
    hide_legend: true
    legend_position: center
    series_types: {}
    point_style: none
    show_value_labels: true
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_dropoff: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    show_row_numbers: true
    truncate_column_names: false
    hide_totals: false
    hide_row_totals: false
    table_theme: editable
    defaults_version: 1
    transpose: false
    truncate_text: true
    size_to_fit: true
    header_text_alignment: left
    header_font_size: 12
    rows_font_size: 12
    value_labels: legend
    label_type: labPer
    hidden_fields: [factretailsales.musy_key_count]
    listen: {}
    row: 0
    col: 6
    width: 6
    height: 3
  - title: Total YTD sales
    name: Total YTD sales
    model: group_customer
    explore: factretailsales
    type: looker_column
    fields: [factretailsales.Sales_Amount_USD, factretailsales.period]
    pivots: [factretailsales.period]
    filters:
      base_customers.is_muse_elite: 'Yes'
    sorts: [factretailsales.Sales_Amount_USD desc 0]
    limit: 500
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    limit_displayed_rows: false
    legend_position: center
    point_style: none
    show_value_labels: true
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    hide_legend: false
    label_value_format: "$##,###"
    series_types: {}
    series_labels:
      factretailsales.Sales_Amount_USD: Sales (USD)
    label_color: []
    reference_lines: []
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    value_format: "$##,###"
    conditional_formatting: [{type: equal to, value: !!null '', background_color: "#173589",
        font_color: !!null '', color_application: {collection_id: 1bc1f1d8-7461-4bfd-8c3b-424b924287b5,
          palette_id: 46a4b248-19f7-4e71-9cf0-59fcc2c3039e}, bold: false, italic: false,
        strikethrough: false, fields: !!null ''}]
    defaults_version: 1
    show_row_numbers: true
    transpose: false
    truncate_text: true
    hide_totals: false
    hide_row_totals: false
    size_to_fit: true
    table_theme: white
    header_text_alignment: left
    header_font_size: 12
    rows_font_size: 12
    hidden_fields:
    listen:
      Date Range: factretailsales.current_date_range
      Comapred to: factretailsales.compare_to
    row: 13
    col: 0
    width: 24
    height: 7
  - title: Total Active Elite
    name: Total Active Elite
    model: group_customer
    explore: factretailsales
    type: looker_column
    fields: [factretailsales.period, factretailsales.musy_key_count]
    pivots: [factretailsales.period]
    filters:
      base_customers.is_muse_elite: 'Yes'
    sorts: [factretailsales.musy_key_count desc 0]
    limit: 500
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    limit_displayed_rows: false
    legend_position: center
    point_style: none
    show_value_labels: true
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    hide_legend: false
    series_types: {}
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    value_format: "$##,###"
    conditional_formatting: [{type: equal to, value: !!null '', background_color: "#173589",
        font_color: !!null '', color_application: {collection_id: 1bc1f1d8-7461-4bfd-8c3b-424b924287b5,
          palette_id: 46a4b248-19f7-4e71-9cf0-59fcc2c3039e}, bold: false, italic: false,
        strikethrough: false, fields: !!null ''}]
    defaults_version: 1
    show_row_numbers: true
    transpose: false
    truncate_text: true
    hide_totals: false
    hide_row_totals: false
    size_to_fit: true
    table_theme: white
    header_text_alignment: left
    header_font_size: 12
    rows_font_size: 12
    hidden_fields:
    listen:
      Date Range: factretailsales.current_date_range
      Comapred to: factretailsales.compare_to
    row: 20
    col: 0
    width: 24
    height: 8
  - title: YTD ATV
    name: YTD ATV
    model: group_customer
    explore: factretailsales
    type: looker_column
    fields: [factretailsales.period, factretailsales.musy_key_count, factretailsales.Sales_Amount_USD,
      factretailsales.Transaction_Count]
    pivots: [factretailsales.period]
    filters:
      base_customers.is_muse_elite: 'Yes'
    sorts: [factretailsales.musy_key_count desc 0]
    limit: 500
    dynamic_fields: [{table_calculation: atv, label: ATV, expression: "${factretailsales.Sales_Amount_USD}/${factretailsales.Transaction_Count}",
        value_format: !!null '', value_format_name: usd_0, _kind_hint: measure, _type_hint: number}]
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    limit_displayed_rows: false
    legend_position: center
    point_style: none
    show_value_labels: true
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    hide_legend: true
    series_types: {}
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    value_format: "$##,###"
    conditional_formatting: [{type: equal to, value: !!null '', background_color: "#173589",
        font_color: !!null '', color_application: {collection_id: 1bc1f1d8-7461-4bfd-8c3b-424b924287b5,
          palette_id: 46a4b248-19f7-4e71-9cf0-59fcc2c3039e}, bold: false, italic: false,
        strikethrough: false, fields: !!null ''}]
    defaults_version: 1
    show_row_numbers: true
    transpose: false
    truncate_text: true
    hide_totals: false
    hide_row_totals: false
    size_to_fit: true
    table_theme: white
    header_text_alignment: left
    header_font_size: 12
    rows_font_size: 12
    hidden_fields: [factretailsales.Sales_Amount_USD, factretailsales.musy_key_count,
      factretailsales.Transaction_Count]
    listen:
      Date Range: factretailsales.current_date_range
      Comapred to: factretailsales.compare_to
    row: 28
    col: 0
    width: 24
    height: 8
  - title: YTD ACV
    name: YTD ACV
    model: group_customer
    explore: factretailsales
    type: looker_column
    fields: [factretailsales.period, factretailsales.musy_key_count, factretailsales.Sales_Amount_USD,
      factretailsales.Transaction_Count]
    pivots: [factretailsales.period]
    filters:
      base_customers.is_muse_elite: 'Yes'
    sorts: [factretailsales.musy_key_count desc 0]
    limit: 500
    dynamic_fields: [{table_calculation: acv, label: ACV, expression: "${factretailsales.Sales_Amount_USD}/${factretailsales.musy_key_count}",
        value_format: !!null '', value_format_name: usd_0, _kind_hint: measure, _type_hint: number}]
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: true
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: row
    stacking: ''
    limit_displayed_rows: false
    legend_position: center
    point_style: none
    show_value_labels: true
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    hide_legend: false
    series_types: {}
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    value_format: "$##,###"
    conditional_formatting: [{type: equal to, value: !!null '', background_color: "#173589",
        font_color: !!null '', color_application: {collection_id: 1bc1f1d8-7461-4bfd-8c3b-424b924287b5,
          palette_id: 46a4b248-19f7-4e71-9cf0-59fcc2c3039e}, bold: false, italic: false,
        strikethrough: false, fields: !!null ''}]
    defaults_version: 1
    show_row_numbers: true
    transpose: false
    truncate_text: true
    hide_totals: false
    hide_row_totals: false
    size_to_fit: true
    table_theme: white
    header_text_alignment: left
    header_font_size: 12
    rows_font_size: 12
    hidden_fields: [factretailsales.Sales_Amount_USD, factretailsales.musy_key_count,
      factretailsales.Transaction_Count]
    listen:
      Date Range: factretailsales.current_date_range
      Comapred to: factretailsales.compare_to
    row: 36
    col: 0
    width: 24
    height: 10
  - title: Monthly Trend
    name: Monthly Trend
    model: group_customer
    explore: factretailsales
    type: looker_column
    fields: [factretailsales.musy_key_count, factretailsales.Sales_Amount_USD, factretailsales.Transaction_Count,
      factretailsales.atr_trandate_month]
    fill_fields: [factretailsales.atr_trandate_month]
    filters:
      base_customers.is_muse_elite: 'Yes'
    sorts: [factretailsales.atr_trandate_month]
    limit: 500
    x_axis_gridlines: true
    y_axis_gridlines: true
    show_view_names: false
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    limit_displayed_rows: false
    legend_position: center
    point_style: circle
    show_value_labels: true
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    color_application:
      collection_id: b43731d5-dc87-4a8e-b807-635bef3948e7
      palette_id: fb7bb53e-b77b-4ab6-8274-9d420d3d73f3
      options:
        steps: 5
    y_axes: [{label: '', orientation: left, series: [{axisId: factretailsales.Sales_Amount_USD,
            id: factretailsales.period___null - factretailsales.Sales_Amount_USD,
            name: "∅ - Sales Fact Sales Amount USD"}], showLabels: true, showValues: true,
        unpinAxis: false, tickDensity: default, type: linear}, {label: '', orientation: right,
        series: [{axisId: factretailsales.Transaction_Count, id: factretailsales.period___null
              - factretailsales.Transaction_Count, name: "∅ - Sales Fact Transaction\
              \ Count"}], showLabels: true, showValues: true, unpinAxis: false, tickDensity: default,
        type: linear}]
    hide_legend: true
    series_types:
      factretailsales.Transaction_Count: line
      factretailsales.period___null - factretailsales.Transaction_Count: line
    series_colors: {}
    series_labels:
      factretailsales.Transaction_Count: Transactions
    series_point_styles:
      factretailsales.period___null - factretailsales.Transaction_Count: diamond
    trend_lines: []
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    value_format: "$##,###"
    conditional_formatting: [{type: equal to, value: !!null '', background_color: "#173589",
        font_color: !!null '', color_application: {collection_id: 1bc1f1d8-7461-4bfd-8c3b-424b924287b5,
          palette_id: 46a4b248-19f7-4e71-9cf0-59fcc2c3039e}, bold: false, italic: false,
        strikethrough: false, fields: !!null ''}]
    defaults_version: 1
    show_row_numbers: true
    transpose: false
    truncate_text: true
    hide_totals: false
    hide_row_totals: false
    size_to_fit: true
    table_theme: white
    header_text_alignment: left
    header_font_size: 12
    rows_font_size: 12
    hidden_fields: [factretailsales.musy_key_count]
    listen:
      Date Range: factretailsales.current_date_range
      Comapred to: factretailsales.compare_to
    row: 46
    col: 0
    width: 24
    height: 8
  - title: Monthly Trend (copy)
    name: Monthly Trend (copy)
    model: group_customer
    explore: factretailsales
    type: looker_column
    fields: [locations.district_name, factretailsales.period, factretailsales.Sales_Amount_USD]
    pivots: [factretailsales.period]
    filters:
      base_customers.is_muse_elite: 'Yes'
    sorts: [factretailsales.period]
    limit: 500
    x_axis_gridlines: true
    y_axis_gridlines: true
    show_view_names: true
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: pivot
    stacking: normal
    limit_displayed_rows: false
    legend_position: center
    point_style: circle
    show_value_labels: true
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    color_application:
      collection_id: b43731d5-dc87-4a8e-b807-635bef3948e7
      palette_id: fb7bb53e-b77b-4ab6-8274-9d420d3d73f3
      options:
        steps: 5
    y_axes: [{label: '', orientation: left, series: [{axisId: factretailsales.Sales_Amount_USD,
            id: factretailsales.period___null - factretailsales.Sales_Amount_USD,
            name: "∅ - Sales Fact Sales Amount USD"}], showLabels: true, showValues: true,
        unpinAxis: false, tickDensity: default, type: linear}, {label: '', orientation: right,
        series: [{axisId: factretailsales.Transaction_Count, id: factretailsales.period___null
              - factretailsales.Transaction_Count, name: "∅ - Sales Fact Transaction\
              \ Count"}], showLabels: true, showValues: true, unpinAxis: false, tickDensity: default,
        type: linear}]
    hide_legend: true
    series_types: {}
    series_colors: {}
    series_labels:
      factretailsales.Transaction_Count: Transactions
    series_point_styles:
      factretailsales.period___null - factretailsales.Transaction_Count: diamond
    trend_lines: []
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    value_format: "$##,###"
    conditional_formatting: [{type: equal to, value: !!null '', background_color: "#173589",
        font_color: !!null '', color_application: {collection_id: 1bc1f1d8-7461-4bfd-8c3b-424b924287b5,
          palette_id: 46a4b248-19f7-4e71-9cf0-59fcc2c3039e}, bold: false, italic: false,
        strikethrough: false, fields: !!null ''}]
    defaults_version: 1
    show_row_numbers: true
    transpose: false
    truncate_text: true
    hide_totals: false
    hide_row_totals: false
    size_to_fit: true
    table_theme: white
    header_text_alignment: left
    header_font_size: 12
    rows_font_size: 12
    hidden_fields: []
    value_labels: legend
    label_type: labPer
    listen:
      Date Range: factretailsales.current_date_range
      Comapred to: factretailsales.compare_to
    row: 54
    col: 0
    width: 24
    height: 8
  - title: Monthly Trend (copy 2)
    name: Monthly Trend (copy 2)
    model: group_customer
    explore: factretailsales
    type: looker_column
    fields: [factretailsales.musy_key_count, factretailsales.Sales_Amount_USD, factretailsales.Transaction_Count,
      factretailsales.atr_trandate_month]
    fill_fields: [factretailsales.atr_trandate_month]
    filters:
      base_customers.is_muse_elite: 'Yes'
    sorts: [factretailsales.atr_trandate_month]
    limit: 500
    x_axis_gridlines: true
    y_axis_gridlines: true
    show_view_names: false
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    y_axis_scale_mode: linear
    x_axis_reversed: false
    y_axis_reversed: false
    plot_size_by_field: false
    trellis: ''
    stacking: ''
    limit_displayed_rows: false
    legend_position: center
    point_style: circle
    show_value_labels: true
    label_density: 25
    x_axis_scale: auto
    y_axis_combined: true
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    color_application:
      collection_id: b43731d5-dc87-4a8e-b807-635bef3948e7
      palette_id: fb7bb53e-b77b-4ab6-8274-9d420d3d73f3
      options:
        steps: 5
    y_axes: [{label: '', orientation: left, series: [{axisId: factretailsales.Sales_Amount_USD,
            id: factretailsales.period___null - factretailsales.Sales_Amount_USD,
            name: "∅ - Sales Fact Sales Amount USD"}], showLabels: true, showValues: true,
        unpinAxis: false, tickDensity: default, type: linear}, {label: '', orientation: right,
        series: [{axisId: factretailsales.Transaction_Count, id: factretailsales.period___null
              - factretailsales.Transaction_Count, name: "∅ - Sales Fact Transaction\
              \ Count"}], showLabels: true, showValues: true, unpinAxis: false, tickDensity: default,
        type: linear}]
    hide_legend: true
    series_types:
      factretailsales.Transaction_Count: line
      factretailsales.period___null - factretailsales.Transaction_Count: line
    series_colors: {}
    series_labels:
      factretailsales.Transaction_Count: Transactions
    series_point_styles:
      factretailsales.period___null - factretailsales.Transaction_Count: diamond
    trend_lines: []
    custom_color_enabled: true
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    value_format: "$##,###"
    conditional_formatting: [{type: equal to, value: !!null '', background_color: "#173589",
        font_color: !!null '', color_application: {collection_id: 1bc1f1d8-7461-4bfd-8c3b-424b924287b5,
          palette_id: 46a4b248-19f7-4e71-9cf0-59fcc2c3039e}, bold: false, italic: false,
        strikethrough: false, fields: !!null ''}]
    defaults_version: 1
    show_row_numbers: true
    transpose: false
    truncate_text: true
    hide_totals: false
    hide_row_totals: false
    size_to_fit: true
    table_theme: white
    header_text_alignment: left
    header_font_size: 12
    rows_font_size: 12
    hidden_fields: [factretailsales.musy_key_count]
    listen:
      Date Range: factretailsales.current_date_range
      Comapred to: factretailsales.compare_to
    row: 62
    col: 0
    width: 24
    height: 8
  - title: Distinct Brands Shopped
    name: Distinct Brands Shopped
    model: group_customer
    explore: factretailsales
    type: table
    fields: [base_customers.distinct_brands_shopped, factretailsales.period]
    filters:
      base_customers.is_muse_elite: 'Yes'
    sorts: [base_customers.distinct_brands_shopped desc]
    limit: 500
    show_view_names: false
    show_row_numbers: true
    truncate_column_names: false
    hide_totals: false
    hide_row_totals: false
    table_theme: editable
    limit_displayed_rows: false
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    defaults_version: 1
    listen:
      Date Range: factretailsales.current_date_range
      Comapred to: factretailsales.compare_to
    row: 70
    col: 0
    width: 8
    height: 6
  filters:
  - name: Date Range
    title: Date Range
    type: field_filter
    default_value: 2020/01/01 to 2020/08/31
    allow_multiple_values: true
    required: false
    model: group_customer
    explore: factretailsales
    listens_to_filters: [Comapred to]
    field: factretailsales.current_date_range
  - name: Comapred to
    title: Comapred to
    type: field_filter
    default_value: Year
    allow_multiple_values: true
    required: false
    model: group_customer
    explore: factretailsales
    listens_to_filters: [Date Range]
    field: factretailsales.compare_to
