test: fact_soh_sales_poc_test {
    explore_source: fact_soh_sales_poc {
        column: inv_stock_qty {}
        sort: {
          field: inv_stock_qty
          desc: yes
        }
        limit: 1
      }
      assert: qty_is_not_null {
        expression: NOT is_null(${fact_soh_sales_poc.inv_stock_qty}) ;;
      }
    }
