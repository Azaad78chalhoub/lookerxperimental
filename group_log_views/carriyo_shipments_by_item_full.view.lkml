include: "../_common/period_over_period.view"

view: carriyo_shipments_by_item_full {
  sql_table_name: `chb-prod-stage-oracle.prod_logistics.carriyo_shipments_by_item_full`
    ;;

  extends: [period_over_period]
  dimension_group: pop_no_tz {
    sql: ${update_date_date} ;;
  }

  dimension: approximate_creation_date_time {
    type: number
    hidden: yes
    sql: ${TABLE}.approximate_creation_date_time ;;
  }

  dimension: pk {
    primary_key: yes
    hidden: yes
    sql: CONCAT(${TABLE}.shipmentId, ${TABLE}.sku, ${TABLE}.status, ${TABLE}.approximate_creation_date_time, ${TABLE}.carrier, ${TABLE}.sequence_number, ${TABLE}.price, ${TABLE}.description) ;;
  }

  dimension_group: promisedDeliverydate {
    type: time
    group_label: "Date - Promised Delivery"
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.promisedDeliverydate AS TIMESTAMP) ;;
  }

  dimension: latest_status {
    hidden: yes
    type: string
    sql: ${TABLE}.latest_status ;;
  }

  dimension: latest_status_boolean {
    group_label: "Statuses"
    label: "Filter by latest status"
    type: yesno
    sql: ${latest_status}=1 ;;

  }

  dimension: quantity {
    group_label: "Payment Info"
    type: number
    sql: ${TABLE}.quantity ;;
  }

  dimension: dangerous_goods_BOOL  {
    type: yesno
    hidden: yes
    sql: ${TABLE}.dangerous_goods_BOOL ;;
  }

  dimension: price {
    group_label: "Payment Info"
    type: number
    sql: ${TABLE}.price ;;
  }

  dimension: currency {
  group_label: "Payment Info"
    type: string
    sql: ${TABLE}.currency ;;
  }

  dimension: origin_country {
    group_label: "Address Related Info"
    type: string
    sql: ${TABLE}.origin_country ;;
  }

  dimension: description {
    group_label: "Item Info"
    type: string
    sql: ${TABLE}.description ;;
  }

  dimension: SKU {
    group_label: "Item Info"
    type: string
    sql: ${TABLE}.SKU ;;
  }

  dimension: event_name {
    group_label: "Statuses"
    type: number
    sql: ${TABLE}.event_name ;;
  }

  dimension: sequence_number {
    type: number
    hidden: yes
    sql: ${TABLE}.sequence_number ;;
  }

  dimension: hsCode {
    type: string
    hidden: yes
    sql: ${TABLE}.hsCode ;;
  }

  dimension: notes {
    group_label: "Statuses"
    type: string
    sql: ${TABLE}.notes ;;
  }

  dimension_group: booked_at {
    type: time
    group_label: "1- Date - Booked At"
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.booked_at AS TIMESTAMP) ;;
  }

  dimension: carrier {
    type: string
    group_label: "Carrier Info"
    sql: ${TABLE}.carrier ;;
  }

  dimension: partnerShipmentReference {
    label: "Internal Order #"
    group_label: "Shipment Info"
    type: string
    sql: ${TABLE}.partnerShipmentReference ;;
  }

  dimension: partnerOrderReference {
    label: "External Order #"
    group_label: "Shipment Info"
    type: string
    sql: ${TABLE}.partnerOrderReference ;;
  }


  dimension: carrier_account_name {
    type: string
    group_label: "Carrier Info"
    sql: ${TABLE}.carrier_account_name ;;
  }

  dimension: carrier_id {
    type: string
    group_label: "Carrier Info"
    sql: ${TABLE}.carrier_id ;;
  }

  dimension: carrier_status {
    type: string
    group_label: "Statuses"
    sql: upper(${TABLE}.carrier_status) ;;
  }

  dimension: carrier_status_description {
    type: string
    group_label: "Carrier Info"
    sql: ${TABLE}.carrier_status_description ;;
  }

  dimension_group: confirmation_date {
    type: time
    group_label: "4- Date - Confirmed At"
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.confirmation_date AS TIMESTAMP) ;;
  }

  dimension_group: creation_date {
    type: time
    group_label: "3- Date - Created At"
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.creation_date AS TIMESTAMP) ;;
  }

  dimension: delivery_type {
    group_label: "Statuses"
    type: string
    sql: ${TABLE}.deliveryType ;;
  }

  dimension: dropoff_address1 {
    group_label: "Address Related Info"
    type: string
    sql: ${TABLE}.dropoff_address1 ;;
  }

  dimension: dropoff_address2 {
    group_label: "Address Related Info"
    type: string
    sql: ${TABLE}.dropoff_address2 ;;
  }

  dimension: dropoff_city {
    group_label: "Address Related Info"
    type: string
    sql: ${TABLE}.dropoff_city ;;
  }

  dimension: dropoff_contact_email {
    group_label: "Address Related Info"
    hidden: yes
    type: string
    sql: ${TABLE}.dropoff_contact_email ;;
  }

  dimension: dropoff_contact_name {
    group_label: "Address Related Info"
    hidden: yes
    type: string
    sql: ${TABLE}.dropoff_contact_name ;;
  }

  dimension: dropoff_contact_phone {
    group_label: "Address Related Info"
    hidden: yes
    type: string
    sql: ${TABLE}.dropoff_contact_phone ;;
  }

  dimension: dropoff_country {
    group_label: "Address Related Info"
    type: string
    sql: ${TABLE}.dropoff_country ;;
  }

  dimension: dropoff_notes {
    group_label: "Address Related Info"
    type: string
    sql: ${TABLE}.dropoff_notes ;;
  }

  dimension: dropoff_post_code {
    group_label: "Address Related Info"
    type: string
    sql: ${TABLE}.dropoff_post_code ;;
  }

  dimension: dropoff_state {
    group_label: "Address Related Info"
    type: string
    sql: ${TABLE}.dropoff_state ;;
  }

  dimension_group: failed_attempt_at {
    type: time
    group_label: "8- Date - Failed Attempt At"
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.failed_attempt_at AS TIMESTAMP) ;;
  }

  dimension: merchant {
    group_label: "Item Info"
    label: "Brand"
    type: string
    sql: CASE
            WHEN ${TABLE}.merchant='TORY-BURCH' THEN 'TORY BURCH'
            WHEN ${TABLE}.merchant='MOLTON-BROWN' THEN 'MOLTON BROWN'
            WHEN ${TABLE}.merchant='TANAGRA' THEN 'TANAGRA & ADV'
            WHEN ${TABLE}.merchant='MUFE' THEN 'MUFE - MANAGEMENT'
            WHEN ${TABLE}.merchant='YSL' THEN 'SAJ'
            WHEN ${TABLE}.merchant='PENHALIGONS' THEN "PENHALIGON'S"
            WHEN ${TABLE}.merchant='THE-DEAL' THEN 'THE DEAL'
            ELSE ${TABLE}.merchant
         END;;
  }

  dimension_group: order_date {
    type: time
    group_label: "2- Date - Ordered at"
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.order_date AS TIMESTAMP) ;;
  }

  dimension_group: out_for_delivery_at {
    type: time
    group_label: "6- Date - Out For Delivery"
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.out_for_delivery_at AS TIMESTAMP) ;;
  }

  dimension: parcels {
    group_label: "Shipment Info"
    type: string
    sql: ${TABLE}.parcels ;;
  }

  dimension: payment_mode {
    group_label: "Payment Info"
    type: string
    sql: ${TABLE}.payment_mode ;;
  }

  dimension: payment_payment_currency {
    group_label: "Payment Info"
    type: string
    sql: ${TABLE}.payment_payment_currency ;;
  }

  dimension: payment_total_amount {
    group_label: "Payment Info"
    hidden: yes
    type: string
    sql: ${TABLE}.payment_total_amount ;;
  }

  dimension: pickup_address1 {
    group_label: "Address Related Info"
    type: string
    sql: ${TABLE}.pickup_address1 ;;
  }

  dimension: pickup_city {
    group_label: "Address Related Info"
    type: string
    sql: ${TABLE}.pickup_city ;;
  }

  dimension: pickup_contact_email {
    group_label: "Address Related Info"
    hidden: yes
    type: string
    sql: ${TABLE}.pickup_contact_email ;;
  }

  dimension: pickup_contact_name {
    group_label: "Address Related Info"
    hidden: yes
    type: string
    sql: ${TABLE}.pickup_contact_name ;;
  }

  dimension: pickup_contact_phone {
    group_label: "Address Related Info"
    hidden: yes
    type: string
    sql: ${TABLE}.pickup_contact_phone ;;
  }

  dimension: pickup_country {
    group_label: "Address Related Info"
    type: string
    sql: ${TABLE}.pickup_country ;;
  }

  dimension: pickup_post_code {
    group_label: "Address Related Info"
    type: string
    sql: ${TABLE}.pickup_post_code ;;
  }

  dimension_group: returned_at {
    type: time
    group_label: "9- Date - Returned At"
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.returned_at AS TIMESTAMP) ;;
  }

  dimension: shipment_id {
    group_label: "Shipment Info"
    type: string
    sql: ${TABLE}.shipmentId ;;
  }

  dimension_group: shipped_at {
    type: time
    group_label: "5- Date - Shipped At"
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.shipped_at AS TIMESTAMP) ;;
  }

  dimension: status {
    group_label: "Statuses"
    type: string
    sql: upper(${TABLE}.status) ;;
  }

  dimension: suppress_communication {
    hidden: yes
    type: string
    sql: ${TABLE}.suppress_communication ;;
  }

  dimension: tracking_no {
    group_label: "Shipment Info"
    type: string
    sql: ${TABLE}.tracking_no ;;
  }

  dimension_group: update_date {
    type: time
    label: "Core Date"
    group_label: "Date - Core Date"
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.update_date AS TIMESTAMP) ;;
  }

  dimension_group: delivered_at {
    type: time
    group_label: "7- Date - Delivered At"
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql:  CAST(${TABLE}.delivered_at AS TIMESTAMP) ;;
  }

  dimension_group: time_spent_pending {
    type: duration
    hidden: yes
    intervals: [second]
    sql_start: ${confirmation_date_raw} ;;
    sql_end: ${shipped_at_raw} ;;
  }

  #(customer placed returns) Time to Collect from Customer

  dimension_group: time_to_first_attempt {
    type: duration
    hidden: yes
    intervals: [second]
    sql_start: ${shipped_at_raw} ;;
    sql_end: CASE
                  WHEN ${failed_attempt_at_raw} IS NULL THEN ${delivered_at_raw}
                  ELSE ${failed_attempt_at_raw}
             END;;
  }

  dimension_group: time_to_deliver {
    type: duration
    hidden: yes
    intervals: [second]
    sql_start: ${shipped_at_raw} ;;
    sql_end: ${delivered_at_raw} ;;
  }

  dimension_group: time_to_out_to_deliver {
    type: duration
    hidden: yes
    intervals: [second]
    sql_start: ${shipped_at_raw} ;;
    sql_end: ${out_for_delivery_at_raw} ;;
  }

  dimension_group: time_to_returned {
    type: duration
    hidden: yes
    intervals: [second]
    sql_start: ${shipped_at_raw} ;;
    sql_end: ${returned_at_raw} ;;
  }

  dimension_group: time_to_promised_deliver {
    type: duration
    hidden: yes
    intervals: [second]
    sql_start: ${shipped_at_raw} ;;
    sql_end: ${promisedDeliverydate_raw} ;;
  }

  dimension_group: order_creation_to_booked {
    type: duration
    hidden: yes
    intervals: [second]
    sql_start: ${creation_date_raw} ;;
    sql_end: ${booked_at_raw} ;;
  }

  dimension_group: order_creation_to_shipped {
    type: duration
    hidden: yes
    intervals: [second]
    sql_start: ${creation_date_raw} ;;
    sql_end: ${shipped_at_raw} ;;
  }

  dimension_group: order_creation_to_out_for_delivery {
    type: duration
    hidden: yes
    intervals: [second]
    sql_start: ${creation_date_raw} ;;
    sql_end: ${out_for_delivery_at_raw} ;;
  }

  dimension_group: order_creation_to_first_attempt {
    type: duration
    hidden: yes
    intervals: [second]
    sql_start: ${creation_date_raw} ;;
    sql_end: CASE
                  WHEN ${failed_attempt_at_raw} IS NULL THEN ${delivered_at_raw}
                  ELSE ${failed_attempt_at_raw}
             END;;
  }

  dimension_group: order_creation_to_delivered {
    type: duration
    hidden: yes
    intervals: [second]
    sql_start: ${creation_date_raw} ;;
    sql_end: ${delivered_at_raw} ;;
  }

  dimension_group: order_creation_to_returned {
    type: duration
    hidden: yes
    intervals: [second]
    sql_start: ${creation_date_raw} ;;
    sql_end: ${returned_at_raw} ;;
  }

  dimension_group: confirmed_to_out_for_delivery{
    type: duration
    hidden: yes
    intervals: [second]
    sql_start: ${confirmation_date_raw} ;;
    sql_end: ${out_for_delivery_at_raw}   ;;
  }

  dimension_group: out_for_delivery_to_first_attempt {
    type: duration
    hidden: yes
    intervals: [second]
    sql_start: ${out_for_delivery_at_raw}  ;;
    sql_end:
    CASE
    WHEN ${failed_attempt_at_raw} IS NULL THEN ${delivered_at_raw}
    ELSE ${failed_attempt_at_raw}
    END;;
  }

  dimension_group: first_attempt_to_delivered {
    type: duration
    hidden: yes
    intervals: [second]
    sql_start: ${failed_attempt_at_raw} ;;
    sql_end: ${delivered_at_raw} ;;
  }

  dimension_group: delivered_to_returned {
    type: duration
    hidden: yes
    intervals: [second]
    sql_start: ${delivered_at_raw} ;;
    sql_end: ${returned_at_raw} ;;
  }

  dimension: delivery_Within_promised  {
    type: yesno
    group_label: "Within Promised Booleans"
    sql: ${seconds_time_to_deliver}<=${seconds_time_to_promised_deliver};;
  }

  dimension: attempt_Within_promised  {
    type: yesno
    group_label: "Within Promised Booleans"
    sql: CASE
              WHEN ${failed_attempt_at_raw} IS NULL then ${delivered_at_raw}<=${promisedDeliverydate_raw}
              ELSE ${failed_attempt_at_raw}<=${promisedDeliverydate_raw}
         END;;
  }

  parameter: date_granularity {
    type: string
    allowed_value: { value: "Hour" }
    allowed_value: { value: "Day" }
    allowed_value: { value: "Month" }
    allowed_value: { value: "Week" }
  }

  dimension: shipped_equals_delivered {
    hidden: yes
    type: yesno
    sql: ${shipped_at_date}=${delivered_at_date} ;;
  }

  dimension: shipped_equals_returned  {
    hidden: yes
    type: yesno
    sql: ${shipped_at_date}=${returned_at_date} ;;

  }

  dimension:  chb_status{
    type: string
    group_label: "Statuses"
    label: "Chalhoub statuses"
    sql: ${TABLE}.chb_status ;;
  }

  ################################################
  #############      measure       ###############
  ################################################

#   measure: total_paid {
#     type: sum_distinct
#     label: "Payment Amount"
#     group_label: "Payment Related"
#     description: "Payment received by courier"
#     value_format:"$#.00"
#     sql_distinct_key: ${shipment_id} ;;
#     sql: CAST(${payment_total_amount} AS FLOAT64) ;;
#   }
#
#

  measure: time_spent_in_confirmed_status {
    type: average
    label_from_parameter: date_granularity
    label: "Time Spent in Confirmed Status"
    group_label: "2- Time Spent in Status"
    description: "Time spent from confirmed to out for delivery"
    sql:
        CASE
          WHEN {% parameter date_granularity %} = 'Hour' THEN ${seconds_confirmed_to_out_for_delivery}/3600
          WHEN {% parameter date_granularity %} = 'Day' THEN ${seconds_confirmed_to_out_for_delivery}*1.1574e-5
          WHEN {% parameter date_granularity %} = 'Week' THEN ${seconds_confirmed_to_out_for_delivery}*1.6534285714e-6
          WHEN {% parameter date_granularity %} = 'Month' THEN ${seconds_confirmed_to_out_for_delivery}*3.80514651484217549e-7
          ELSE ${seconds_confirmed_to_out_for_delivery}*1.1574e-5
        END;;
  }

  measure: time_spent_in_out_for_delivery_status {
    type: average
    label_from_parameter: date_granularity
    label: "Time Spent in Out For Delivery Status"
    group_label: "2- Time Spent in Status"
    description: "Time spent from out for delivery to first attempt"
    sql:
        CASE
          WHEN {% parameter date_granularity %} = 'Hour' THEN ${seconds_out_for_delivery_to_first_attempt}/3600
          WHEN {% parameter date_granularity %} = 'Day' THEN ${seconds_out_for_delivery_to_first_attempt}*1.1574e-5
          WHEN {% parameter date_granularity %} = 'Week' THEN ${seconds_out_for_delivery_to_first_attempt}*1.6534285714e-6
          WHEN {% parameter date_granularity %} = 'Month' THEN ${seconds_out_for_delivery_to_first_attempt}*3.80514651484217549e-7
          ELSE ${seconds_out_for_delivery_to_first_attempt}*1.1574e-5
        END;;
  }

  measure: time_spent_in_first_attempt_status {
    type: average
    label_from_parameter: date_granularity
    label: "Time Spent in First Attempt Status"
    group_label: "2- Time Spent in Status"
    description: "Time spent from first attempt to delivered"
    sql:
        CASE
          WHEN {% parameter date_granularity %} = 'Hour' THEN ${seconds_first_attempt_to_delivered}/3600
          WHEN {% parameter date_granularity %} = 'Day' THEN ${seconds_first_attempt_to_delivered}*1.1574e-5
          WHEN {% parameter date_granularity %} = 'Week' THEN ${seconds_first_attempt_to_delivered}*1.6534285714e-6
          WHEN {% parameter date_granularity %} = 'Month' THEN ${seconds_first_attempt_to_delivered}*3.80514651484217549e-7
          ELSE ${seconds_first_attempt_to_delivered}*1.1574e-5
        END;;
  }

  measure: time_spent_in_delivered_status {
    type: average
    label_from_parameter: date_granularity
    label: "Time Spent in Delivered Status"
    group_label: "2- Time Spent in Status"
    description: "Time spent from delivered to returned"
    sql:
        CASE
          WHEN {% parameter date_granularity %} = 'Hour' THEN ${seconds_delivered_to_returned}/3600
          WHEN {% parameter date_granularity %} = 'Day' THEN ${seconds_delivered_to_returned}*1.1574e-5
          WHEN {% parameter date_granularity %} = 'Week' THEN ${seconds_delivered_to_returned}*1.6534285714e-6
          WHEN {% parameter date_granularity %} = 'Month' THEN ${seconds_delivered_to_returned}*3.80514651484217549e-7
          ELSE ${seconds_delivered_to_returned}*1.1574e-5
        END;;
  }

  measure: order_creation_to_booked_time  {
    type: average
    label_from_parameter: date_granularity
    label: "Click to Collect"
    group_label: "3- Click to Status"
    description: "Time from carriyo order creation to booked in days"
    value_format:"#.00"
    sql:
        CASE
          WHEN {% parameter date_granularity %} = 'Hour' THEN ${seconds_order_creation_to_booked}/3600
          WHEN {% parameter date_granularity %} = 'Day' THEN ${seconds_order_creation_to_booked}*1.1574e-5
          WHEN {% parameter date_granularity %} = 'Week' THEN ${seconds_order_creation_to_booked}*1.6534285714e-6
          WHEN {% parameter date_granularity %} = 'Month' THEN ${seconds_order_creation_to_booked}*3.80514651484217549e-7
          ELSE ${seconds_order_creation_to_booked}*1.1574e-5
        END;;
  }

  measure: order_creation_to_shipped_time {
    type: average
    label_from_parameter: date_granularity
    label: "Click to Shipped"
    group_label: "3- Click to Status"
    description: "Time from carriyo order creation to shipped"
    value_format:"#.00"
    sql:
        CASE
          WHEN {% parameter date_granularity %} = 'Hour' THEN ${seconds_order_creation_to_shipped}/3600
          WHEN {% parameter date_granularity %} = 'Day' THEN ${seconds_order_creation_to_shipped}*1.1574e-5
          WHEN {% parameter date_granularity %} = 'Week' THEN ${seconds_order_creation_to_shipped}*1.6534285714e-6
          WHEN {% parameter date_granularity %} = 'Month' THEN ${seconds_order_creation_to_shipped}*3.80514651484217549e-7
          ELSE ${seconds_order_creation_to_shipped}*1.1574e-5
        END;;
  }

  measure: order_creation_to_out_for_delivery_ {
    type: average
    label_from_parameter: date_granularity
    label: "Click to Out For Delivery"
    group_label: "3- Click to Status"
    description: "Time from carriyo order creation to out for delivery"
    value_format:"#.00"
    sql:
        CASE
          WHEN {% parameter date_granularity %} = 'Hour' THEN ${seconds_order_creation_to_out_for_delivery}/3600
          WHEN {% parameter date_granularity %} = 'Day' THEN ${seconds_order_creation_to_out_for_delivery}*1.1574e-5
          WHEN {% parameter date_granularity %} = 'Week' THEN ${seconds_order_creation_to_out_for_delivery}*1.6534285714e-6
          WHEN {% parameter date_granularity %} = 'Month' THEN ${seconds_order_creation_to_out_for_delivery}*3.80514651484217549e-7
          ELSE ${seconds_order_creation_to_out_for_delivery}*1.1574e-5
        END;;
  }

  measure: order_creation_to_first_attempt_ {
    type: average
    label_from_parameter: date_granularity
    label: "Click to First Attempt"
    group_label: "3- Click to Status"
    description: "Time from carriyo order creation to first attempt"
    value_format:"#.00"
    sql:
        CASE
          WHEN {% parameter date_granularity %} = 'Hour' THEN ${seconds_order_creation_to_first_attempt}/3600
          WHEN {% parameter date_granularity %} = 'Day' THEN ${seconds_order_creation_to_first_attempt}*1.1574e-5
          WHEN {% parameter date_granularity %} = 'Week' THEN ${seconds_order_creation_to_first_attempt}*1.6534285714e-6
          WHEN {% parameter date_granularity %} = 'Month' THEN ${seconds_order_creation_to_first_attempt}*3.80514651484217549e-7
          ELSE ${seconds_order_creation_to_first_attempt}*1.1574e-5
        END;;
  }

  measure: order_creation_to_delivered_ {
    type: average
    label_from_parameter: date_granularity
    label: "Click to Delivered"
    group_label: "3- Click to Status"
    description: "Time from carriyo order creation to delivered"
    value_format:"#.00"
    sql:
        CASE
          WHEN {% parameter date_granularity %} = 'Hour' THEN ${seconds_order_creation_to_delivered}/3600
          WHEN {% parameter date_granularity %} = 'Day' THEN ${seconds_order_creation_to_delivered}*1.1574e-5
          WHEN {% parameter date_granularity %} = 'Week' THEN ${seconds_order_creation_to_delivered}*1.6534285714e-6
          WHEN {% parameter date_granularity %} = 'Month' THEN ${seconds_order_creation_to_delivered}*3.80514651484217549e-7
          ELSE ${seconds_order_creation_to_delivered}*1.1574e-5
        END;;
  }

  measure: order_creation_to_returned_ {
    type: average
    label_from_parameter: date_granularity
    label: "Click to Returned"
    group_label: "3- Click to Status"
    description: "Time from carriyo order creation to returned"
    value_format:"#.00"
    sql:
        CASE
          WHEN {% parameter date_granularity %} = 'Hour' THEN ${seconds_order_creation_to_returned}/3600
          WHEN {% parameter date_granularity %} = 'Day' THEN ${seconds_order_creation_to_returned}*1.1574e-5
          WHEN {% parameter date_granularity %} = 'Week' THEN ${seconds_order_creation_to_returned}*1.6534285714e-6
          WHEN {% parameter date_granularity %} = 'Month' THEN ${seconds_order_creation_to_returned}*3.80514651484217549e-7
          ELSE ${seconds_order_creation_to_returned}*1.1574e-5
        END;;
  }

  measure: time_spent_pending_time {
    type: average
    label_from_parameter: date_granularity
    label: "Time spent in pending"
    group_label: "4- DSP Efficiency"
    description: "Time from order confirmation to shipped"
    value_format:"#.00"
    sql:
        CASE
          WHEN {% parameter date_granularity %} = 'Hour' THEN ${seconds_time_spent_pending}/3600
          WHEN {% parameter date_granularity %} = 'Day' THEN ${seconds_time_spent_pending}*1.1574e-5
          WHEN {% parameter date_granularity %} = 'Week' THEN ${seconds_time_spent_pending}*1.6534285714e-6
          WHEN {% parameter date_granularity %} = 'Month' THEN ${seconds_time_spent_pending}*3.80514651484217549e-7
          ELSE ${seconds_time_spent_pending}*1.1574e-5
        END;;
  }

  measure: time_to_first_attempt_time{
    type: average
    label_from_parameter: date_granularity
    label: "Time to first attempt"
    group_label: "4- DSP Efficiency"
    description: "Time from shipped at to first attempt at delivery"
    value_format:"#.00"
    sql: CASE
          WHEN {% parameter date_granularity %} = 'Hour' THEN ${seconds_time_to_first_attempt}/3600
          WHEN {% parameter date_granularity %} = 'Day' THEN ${seconds_time_to_first_attempt}*1.1574e-5
          WHEN {% parameter date_granularity %} = 'Week' THEN ${seconds_time_to_first_attempt}*1.6534285714e-6
          WHEN {% parameter date_granularity %} = 'Month' THEN ${seconds_time_to_first_attempt}*3.80514651484217549e-7
          ELSE ${seconds_time_to_first_attempt}*1.1574e-5
        END;;
  }

  measure: time_to_deliver_ {
    type: average
    label_from_parameter: date_granularity
    group_label: "4- DSP Efficiency"
    description: "Time from shipped at to delivered"
    value_format:"#.00"
    sql:  CASE
          WHEN {% parameter date_granularity %} = 'Hour' THEN ${seconds_time_to_deliver}/3600
          WHEN {% parameter date_granularity %} = 'Day' THEN ${seconds_time_to_deliver}*1.1574e-5
          WHEN {% parameter date_granularity %} = 'Week' THEN ${seconds_time_to_deliver}*1.6534285714e-6
          WHEN {% parameter date_granularity %} = 'Month' THEN ${seconds_time_to_deliver}*3.80514651484217549e-7
          ELSE ${seconds_time_to_deliver}*1.1574e-5
        END;;
  }

  measure: time_to_out_to_deliver_ {
    type: average
    label_from_parameter: date_granularity
    group_label: "4- DSP Efficiency"
    description: "Time from shipped at to out to deliver"
    value_format:"#.00"
    sql:  CASE
          WHEN {% parameter date_granularity %} = 'Hour' THEN ${seconds_time_to_out_to_deliver}/3600
          WHEN {% parameter date_granularity %} = 'Day' THEN ${seconds_time_to_out_to_deliver}*1.1574e-5
          WHEN {% parameter date_granularity %} = 'Week' THEN ${seconds_time_to_out_to_deliver}*1.6534285714e-6
          WHEN {% parameter date_granularity %} = 'Month' THEN ${seconds_time_to_out_to_deliver}*3.80514651484217549e-7
          ELSE ${seconds_time_to_out_to_deliver}*1.1574e-5
        END;;
  }

  measure: time_to_return_failed_delivery {
    type: average
    label_from_parameter: date_granularity
    group_label: "4- DSP Efficiency"
    description: "Time from shipped at to returned"
    value_format:"#.00"
    sql:  CASE
          WHEN {% parameter date_granularity %} = 'Hour' THEN ${seconds_time_to_returned}/3600
          WHEN {% parameter date_granularity %} = 'Day' THEN ${seconds_time_to_returned}*1.1574e-5
          WHEN {% parameter date_granularity %} = 'Week' THEN ${seconds_time_to_returned}*1.6534285714e-6
          WHEN {% parameter date_granularity %} = 'Month' THEN ${seconds_time_to_returned}*3.80514651484217549e-7
          ELSE ${seconds_time_to_returned}*1.1574e-5
        END;;
  }

  measure: delivery_within_promised_yes {
    type: count_distinct
    hidden: yes
    sql: ${shipment_id} ;;
    filters: [delivery_Within_promised: "Yes"]
  }

  measure: delivery_within_promised_no {
    type: count_distinct
    hidden: yes
    sql: ${shipment_id} ;;
    filters: [delivery_Within_promised: "No"]
  }

  measure: ecommerce_ontime_ratio {
    type: number
    description: "Ratio of deliveries made within promise time versus not made within promised time"
    group_label: "5- Last Mile Reliability"
    value_format_name: percent_2
    sql: ${delivery_within_promised_yes}/${delivery_within_promised_no} ;;
  }


# SPD 267 Start
#   dimension: lead_time_delivery{
#     label: "Lead Time In Days"
#     hidden: yes
#     description: "Date difference between order and delivery, in days"
#     type: number
#     value_format_name: decimal_0
#     sql: date_diff(${delivered_at_date},${order_date_date},day) ;;
#   }

  dimension: lead_time_delivery{
    label: "Lead Time In Days"
    hidden: yes
    description: "Date difference between order and delivery, in days"
    type: number
    value_format:"#.00"
    sql: date_diff(${delivered_at_date},${booked_at_date},day) ;;
  }

  measure: count_distinct_shipment {
    label: "Total Orders"
    group_label: "1- Order Totals By Chb Status"
    description: "Distinct shipment orders"
    type: count_distinct
    filters: {
      field: latest_status_boolean
      value: "Yes"
    }
    sql: ${shipment_id} ;;
  }

  measure: chb_count_distinct_cancelled {
    label: "Total Cancelled Orders"
    group_label: "1- Order Totals By Chb Status"
    description: "Distinct shipment of cancelled orders"
    type: count_distinct
    filters: {
      field: chb_status
      value: "Cancelled"
    }
    filters: {
      field: latest_status_boolean
      value: "Yes"
    }
    sql: ${shipment_id} ;;
  }

  measure: chb_count_distinct_delivered {
    label: "Total Delivered Orders"
    group_label: "1- Order Totals By Chb Status"
    description: "Distinct shipment of delivered orders"
    type: count_distinct
    filters: {
      field: chb_status
      value: "Delivered"
    }
    filters: {
      field: latest_status_boolean
      value: "Yes"
    }
    sql: ${shipment_id} ;;
  }

  measure: chb_count_distinct_lost {
    label: "Total Lost Orders"
    group_label: "1- Order Totals By Chb Status"
    description: "Distinct shipment of lost orders"
    type: count_distinct
    filters: {
      field: chb_status
      value: "Lost"
    }
    filters: {
      field: latest_status_boolean
      value: "Yes"
    }
    sql: ${shipment_id} ;;
  }

  measure: chb_count_distinct_not_delivered {
    label: "Total Not Delivered Orders"
    group_label: "1- Order Totals By Chb Status"
    description: "Distinct shipment of not delivered orders"
    type: count_distinct
    filters: {
      field: chb_status
      value: "Not Delivered"
    }
    filters: {
      field: latest_status_boolean
      value: "Yes"
    }
    sql: ${shipment_id} ;;
  }

  measure: chb_count_distinct_not_picked {
    label: "Total Not Picked Orders"
    group_label: "1- Order Totals By Chb Status"
    description: "Distinct shipment of not picked orders"
    type: count_distinct
    filters: {
      field: chb_status
      value: "Not Picked"
    }
    filters: {
      field: latest_status_boolean
      value: "Yes"
    }
    sql: ${shipment_id} ;;
  }

  measure: chb_count_distinct_returned {
    label: "Total Returned Orders"
    group_label: "1- Order Totals By Chb Status"
    description: "Distinct shipment of returned orders"
    type: count_distinct
    filters: {
      field: chb_status
      value: "Returned"
    }
    filters: {
      field: latest_status_boolean
      value: "Yes"
    }
    sql: ${shipment_id} ;;
  }

  measure: chb_count_distinct_to_be_cancelled {
    label: "Total To Be Cancelled Orders"
    group_label: "1- Order Totals By Chb Status"
    description: "Distinct shipment of to be cancelled orders"
    type: count_distinct
    filters: {
      field: chb_status
      value: "To be Cancelled"
    }
    filters: {
      field: latest_status_boolean
      value: "Yes"
    }
    sql: ${shipment_id} ;;
  }

  measure: chb_count_distinct_to_be_returned {
    label: "Total To Be Returned Orders"
    group_label: "1- Order Totals By Chb Status"
    description: "Distinct shipment of to be returned orders"
    type: count_distinct
    filters: {
      field: chb_status
      value: "To be Returned"
    }
    filters: {
      field: latest_status_boolean
      value: "Yes"
    }
    sql: ${shipment_id} ;;
  }

  # measure: count_distinct_shipment_delivered_on_shipped_date {
  #   label: "Total Orders Delivered Based On Shipped Date"
  #   group_label: "1- Order Totals By Milestone"
  #   description: "Distinct shipment orders delivered"
  #   type: count_distinct
  #   filters: {
  #     field: status
  #     value: "DELIVERED"
  #   }
  #   filters: {
  #     field: shipped_equals_delivered
  #     value: "yes"
  #   }
  #   sql: ${shipment_id} ;;
  # }

  # measure: count_distinct_shipment_delivered {
  #   label: "Total Orders Delivered"
  #   group_label: "1- Order Totals By Milestone"
  #   description: "Distinct shipment orders delivered"
  #   type: count_distinct
  #   filters: {
  #     field: status
  #     value: "DELIVERED"
  #   }
  #   sql: ${shipment_id} ;;
  # }

  # measure: count_distinct_shipment_pending {
  #   label: "Total Orders Pending"
  #   group_label: "1- Order Totals By Milestone"
  #   description: "Distinct shipment orders pending"
  #   type: count_distinct
  #   filters: {
  #     field: status
  #     value: "PENDING"
  #   }
  #   sql: ${shipment_id} ;;
  # }


  measure: count_delivered_0 {
    label: "Same Day"
    description: "Orders delivered in same day"
    group_label: "6- Order Delivery Lead Time"
    type: count_distinct
    sql: case when ${lead_time_delivery} <= 0 then ${shipment_id} else null end ;;
  }

  measure: count_delivered_1 {
    label: "1 Day"
    description: "Orders delivered in one days of time"
    group_label: "6- Order Delivery Lead Time"
    type: count_distinct
    sql: case when ${lead_time_delivery} = 1 then ${shipment_id} else null end ;;
  }

  measure: count_delivered_2 {
    label: "2 Day"
    description: "Orders delivered in two days of time"
    group_label: "6- Order Delivery Lead Time"
    type: count_distinct
    sql: case when ${lead_time_delivery} = 2 then ${shipment_id} else null end ;;
  }

  measure: count_delivered_3 {
    label: "3 Day"
    description: "Orders delivered in three days of time"
    group_label: "6- Order Delivery Lead Time"
    type: count_distinct
    sql: case when ${lead_time_delivery} = 3 then ${shipment_id} else null end ;;
  }

  measure: count_delivered_4 {
    label: "4 Day"
    description: "Orders delivered in four days of time"
    group_label: "6- Order Delivery Lead Time"
    type: count_distinct
    sql: case when ${lead_time_delivery} = 4 then ${shipment_id} else null end ;;
  }

  measure: count_delivered_5 {
    label: "5 Day"
    description: "Orders delivered in five days of time"
    group_label: "6- Order Delivery Lead Time"
    type: count_distinct
    sql: case when ${lead_time_delivery} = 5 then ${shipment_id} else null end ;;
  }

  measure: count_delivered_6 {
    label: "6 Day"
    description: "Orders delivered in six days of time"
    group_label: "6- Order Delivery Lead Time"
    type: count_distinct
    sql: case when ${lead_time_delivery} = 6 then ${shipment_id} else null end ;;
  }

  measure: count_delivered_7 {
    label: "7 Day"
    description: "Orders delivered in seven days of time"
    group_label: "6- Order Delivery Lead Time"
    type: count_distinct
    sql: case when ${lead_time_delivery} = 7 then ${shipment_id} else null end ;;
  }

  measure: count_delivered_8 {
    label: ">7"
    description: "Orders delivered in more than 7 days of time"
    group_label: "6- Order Delivery Lead Time"
    type: count_distinct
    sql: case when ${lead_time_delivery} > 7 then ${shipment_id} else null end ;;
  }

# SPD 267 End

# SPD 268 Start

  # measure: count_orders_picked {
  #   label: "Total Orders Collected"
  #   hidden: no
  #   group_label: "1- Order Totals By Milestone"
  #   description: "Orders Collected by DSP's"
  #   type: count_distinct
  #   filters: [status: "SHIPPED"]
  #   sql: ${shipment_id} ;;
  # }

  # measure: count_orders_not_picked {
  #   label: "Total Orders Not Collected"
  #   group_label: "1- Order Totals By Milestone"
  #   description: "Orders Not Collected by DSP's"
  #   type: count_distinct
  #   filters: [status: "-CANCELLED, -DELIVERED, -ERROR, -FAILED_ATTEMPT, -FAILED_DELIVERY_ATTEMPT, -IN_TRANSIT, -MISSING, -OUT_FOR_DELIVERY, -READY_FOR_RETURN, -RETURN_IN_TRANSIT, -RETURNED, -SHIPPED, -SUSPENDED"]
  #   sql: ${shipment_id} ;;
  # }

# SPD 268 End

# SPD 253 start

  measure: cancelled_order{
    type: count_distinct
    hidden: yes
    filters: {
      field: status
      value: "cancelled"
    }
    sql:${shipment_id};;
  }

  measure: refuse_rate {
    group_label: "5- Last Mile Reliability"
    label: "Refuse Rate"
    description: "Number of Cancelled Orders divided by Total Shipments"
    type: number
    sql: SAFE_DIVIDE(${cancelled_order},${count_distinct_shipment}) ;;
    value_format_name: percent_2
  }

# SPD 253 End



# SPD 276 Start

  # measure: count_of_orders_deliverd_late {
  #   label: "Total Orders Delivered Late"
  #   group_label: "1- Order Totals By Milestone"
  #   type: count_distinct
  #   sql:case when ${delivered_at_date} >= ${promisedDeliverydate_date} then ${shipment_id} else null end;;
  # }
# SPD 276 End

# SPD 277 Start
  dimension_group: order_creation_in_carriyo {
    type: duration
    hidden: yes
    intervals: [second]
    sql_start: ${order_date_raw} ;;
    sql_end: ${creation_date_raw} ;;
  }

  measure: carriyo_order_creation_in_carriyo_time  {
    type: average
    label_from_parameter: date_granularity
    label: "Click to Order Creation in Carriyo"
    group_label: "3- Click to Status"
    description: "Time from Order Placement by Customer to First Created in Carriyo"
    value_format:"#.00"
    sql:
        CASE
          WHEN {% parameter date_granularity %} = 'Hour' THEN ${seconds_order_creation_in_carriyo}/3600
          WHEN {% parameter date_granularity %} = 'Day' THEN ${seconds_order_creation_in_carriyo}*1.1574e-5
          WHEN {% parameter date_granularity %} = 'Week' THEN ${seconds_order_creation_in_carriyo}*1.6534285714e-6
          WHEN {% parameter date_granularity %} = 'Month' THEN ${seconds_order_creation_in_carriyo}*3.80514651484217549e-7
          ELSE ${seconds_order_creation_in_carriyo}*1.1574e-5
        END;;
  }

# SPD 277 End

# # SPD 304 Start
#   measure: count_shipment_returned_on_shipped_date {
#     label: "Total Orders Returned Due to Failed Delivery Based On Shipped Date"
#     group_label: "1- Order Totals By Milestone"
#     description: "Distinct shipment orders returned by DSP"
#     type: count_distinct
#     filters: {
#       field: status
#       value: "RETURNED"
#     }
#     filters: {
#       field: shipped_equals_returned
#       value: "yes"
#     }
#     sql: ${shipment_id} ;;
#   }

  # measure: count_shipment_returned {
  #   label: "Total Orders Returned Due to Failed Delivery"
  #   group_label: "1- Order Totals By Milestone"
  #   description: "Distinct shipment orders returned by DSP"
  #   type: count_distinct
  #   filters: {
  #     field: status
  #     value: "RETURNED"
  #   }
  #   sql: ${shipment_id} ;;
  # }
# SPD 304 End

#SPD 307 Start
  # measure: count_shipment_received {
  #   label: "Total Orders Received"
  #   group_label: "1- Order Totals By Milestone"
  #   description: "Distinct shipment orders Received by DSP"
  #   type: count_distinct
  #   sql: case
  #       when ${status} ='BOOKED' then ${shipment_id} else null end ;;
  # }
#SPD 307 End

  measure: c_to_p {
    label: "Service Level Agreement"
    group_label: "5- Last Mile Reliability"
    description: "Time (in days) under which a delivery for the shipment is promised"
    value_format:"#.00"
    type: number
    sql: date_diff(${first_promised_delivery_date},${first_order_date}, day) ;;
  }

#   measure: c_to_S {
#     label: "Click to Ship - C2S"
#     group_label: "4- Last Mile Reliability"
#     description: "Time (in days) taken to ship the shipment since its ordered date"
#     value_format:"#.00"
#     type: number
#     sql: date_diff(${first_shipped_date}, ${first_order_date}, day) ;;
#   }
  measure: first_order_date {
    hidden: yes
    type: date
    sql:MIN(${order_date_date});;
  }
  measure: first_promised_delivery_date {
    hidden: yes
    type: date
    sql:MIN(${promisedDeliverydate_date});;
  }
  measure: first_failed_attempt_date {
    hidden: yes
    type: date
    sql:MIN(${failed_attempt_at_date});;
  }
  measure: first_ofd_date {
    hidden: yes
    type: date
    sql:MIN(${out_for_delivery_at_date});;
  }
  measure: first_delivered_date {
    hidden: yes
    type: date
    sql:MIN(${delivered_at_date});;
  }
  measure: first_shipped_date {
    hidden: yes
    type: date
    sql:MIN(${shipped_at_date});;
  }
#   measure: Shipped_to_first_attempt  {
#     label: "Ship from DC to First attempt - S2F"
#     group_label: "Courier Milestone Efficiency"
#     description: "Time (in days) taken to have the first attempt since the package physically was picked up from DC by the DSP "
#     value_format:"#.00"
#     type: number
#     sql:  (date_diff((coalesce((${first_failed_attempt_date}),(${first_ofd_date}),(${first_delivered_date}))),(${first_shipped_date}),  day));;
#   }

#  measure: first_attempt_to_delivery{
#    label: "First OFD to Delivery (in days) - F2D"
#    description: "Time (in days) taken to have the delivered status since the first attempt"
#    type: number
#    sql:  (date_diff((coalesce((${first_failed_attempt_date}),(${first_ofd_date}),(${first_delivered_date}))),(${first_delivered_date}),  day));;
#  }

#  measure: dea {
#    label: "Delivery Estimate Accuracy"
#    description: "Count of the orders which were attempted at least once before the promise date"
#    type: number
#    sql:  case when (coalesce((${first_failed_attempt_date}),(${first_ofd_date}),(${first_delivered_date})) < ${promisedDeliverydate_date} then (${count_distinct_shipment} else 0 end;;
#  }
  measure: is_fdds {
    type: yesno
    label: "Is First Day Delivery Success?"
    group_label: "5- Last Mile Reliability"
    description: "Has the shipment been delivered on its first attempt? Is it a first day delivery success?"
    sql: (date_diff(coalesce((${first_ofd_date}),(${first_failed_attempt_date}),(${first_delivered_date})), ${first_delivered_date},day))=0;;
  }

#  measure: fdds {
#    label: "First Day Delivery Success"
#    description: "Count of orders which were delivered on the very fist attempt "
#    type: count_distinct
#    sql:  case when ${is_fdds}='yes' then ${shipment_id} else 0 end;;
#  }
#  measure: fdds_prepaid {
#    label: "First Day Delivery Success-Prepaid"
#    description: "Count of Prepaid orders which were delivered on the very fist attempt "
#    type: number
#    sql:  case when ${is_fdds} and (${payment_mode}='PRE_PAID') then (${count_distinct_shipment} else 0 end;;
#  }
#  measure: fdds_cod {
#    label: "First Day Delivery Success-Cash On Delivery"
#    description: "Count of Cash on Delivery orders which were delivered on the very fist attempt "
#    type: number
#    sql:  case when ${is_fdds} and (${payment_mode}='CASH_ON_DELIVERY') then (${count_distinct_shipment} else 0 end;;
#  }
}
