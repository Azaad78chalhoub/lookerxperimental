view: dim_order_qty_exclusion {
  sql_table_name: `chb-prod-supplychain-data.prod_warehouse_logistics_supply_chain.dim_order_qty_exclusion`
    ;;

  dimension: pk {
    primary_key: yes
    type: string
    hidden: yes
    sql: ${order_key} ;;
  }

  dimension: Country {
    type: string
    sql: ${TABLE}.Country ;;
  }

  dimension: wh_id {
    type: string
    sql: ${TABLE}.wh_id ;;
  }

  dimension: order_key {
    type: number
    sql: ${TABLE}.order_key ;;
  }

  dimension: fc_issue {
    type: number
    sql: ${TABLE}.FC_ISSUE ;;
  }

  dimension: division_issue {
    type: number
    sql: ${TABLE}.DIVISION_ISSUE ;;
  }

  dimension: category {
    type: string
    sql: ${TABLE}.Category ;;
  }

  dimension: comment {
    type: string
    sql: ${TABLE}.Comment ;;
  }

  dimension: month {
    type: string
    sql: ${TABLE}.Month ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }

  measure: total_division_issue {
    label: "Total Division Exclusion"
    type: sum
    sql:  ${division_issue};;
  }
}
