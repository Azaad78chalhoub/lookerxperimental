view: store_collection_schedule {
  sql_table_name: `chb-prod-stage-oracle.prod_logistics.store_collection_schedule`
    ;;

  dimension: _key {
    type: string
    sql: ${TABLE}._key ;;
  }

  dimension: bu {
    type: string
    sql: ${TABLE}.bu ;;
  }

  dimension: city {
    type: string
    sql: ${TABLE}.city ;;
  }

  dimension: collection_time_1_end {
    type: string
    sql: ${TABLE}.collection_time_1_end ;;
  }

  dimension: collection_time_1_start {
    type: number
    sql: floor(safe_cast(${TABLE}.collection_time_1_start as FLOAT64)) ;;
  }

  dimension: collection_time_1_start_as_time {
    type: date_time
    sql: cast(convert(char(5),dateadd(minute,60*${collection_time_1_start},0),108) as datetime) ;;
  }

  dimension: collection_time_2_end {
    type: string
    sql: ${TABLE}.collection_time_2_end ;;
  }

  dimension: collection_time_2_start {
    type: string
    sql: ${TABLE}.collection_time_2_start ;;
  }

  dimension: country {
    type: string
    map_layer_name: countries
    sql: ${TABLE}.country ;;
  }

  dimension: date {
    type: string
    sql: ${TABLE}.date ;;
  }

  dimension: day {
    type: string
    sql: ${TABLE}.day ;;
  }

  dimension: dsp {
    type: string
    sql: case when ${TABLE}.dsp = "RLM" then "FAREYE" else upper(${TABLE}.dsp) end ;;
  }

  dimension: ingestion_time {
    type: string
    sql: ${TABLE}.ingestion_time ;;
  }

  dimension: next_day_time_end {
    type: string
    sql: ${TABLE}.next_day_time_end ;;
  }

  dimension: next_day_time_start {
    type: string
    sql: ${TABLE}.next_day_time_start ;;
  }

  dimension: next_next_day_time_end {
    type: string
    sql: ${TABLE}.next_next_day_time_end ;;
  }

  dimension: next_next_day_time_start {
    type: string
    sql: ${TABLE}.next_next_day_time_start ;;
  }

  dimension: store_id {
    type: string
    sql: ${TABLE}.store_id ;;
  }

  dimension: store_name {
    type: string
    sql: ${TABLE}.store_name ;;
  }

  measure: count {
    type: count
    drill_fields: [store_name]
  }


}
