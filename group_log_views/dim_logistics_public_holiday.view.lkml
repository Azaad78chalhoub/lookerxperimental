view: dim_logistics_public_holiday {
  sql_table_name: `chb-prod-supplychain-data.prod_warehouse_logistics_supply_chain.dim_logistics_public_holiday`
    ;;

  dimension: comments {
    type: string
    sql: ${TABLE}.Comments ;;
  }

  dimension: country {
    type: string
    map_layer_name: countries
    sql: ${TABLE}.Country ;;
  }

  dimension_group: holiday_start {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.Holiday_Start_Date ;;
  }

  dimension: no_of_days_until_reopening {
    type: number
    sql: ${TABLE}.No_of_days_until_reopening ;;
  }

  dimension_group: reopening {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.Reopening_Date ;;
  }

  dimension: wh_id {
    type: string
    sql: ${TABLE}.WH_ID ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
