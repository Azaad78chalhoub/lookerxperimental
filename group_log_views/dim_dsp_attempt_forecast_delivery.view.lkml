view: dim_dsp_attempt_forecast_delivery {
  sql_table_name: `chb-prod-supplychain-data.prod_warehouse_logistics_supply_chain.dim_dsp_attempt_forecast_delivery`
    ;;


  dimension: pk {
    hidden: yes
    primary_key: yes
    sql: concat(${country},"-",${service_type}) ;;
  }


  dimension: attempt_1 {
    type: number
    sql: ${TABLE}.attempt_1 ;;
  }

  dimension: attempt_2 {
    type: number
    sql: ${TABLE}.attempt_2 ;;
  }

  dimension: attempt_3 {
    type: number
    sql: ${TABLE}.attempt_3 ;;
  }

  dimension: country {
    type: string
    map_layer_name: countries
    sql: ${TABLE}.country ;;
  }

  dimension: service_type {
    type: string
    sql: ${TABLE}.service_type ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
