view: b2b_order_fulfillment {

  # this view is to get the details at order level as a derived table
  derived_table: {
      explore_source: dm_fulfillment_wm9_orders_b2b {
        column: order_id {}
        #column: unit_fulfilled { field: dm_fulfillment_wm9_orders_details_b2b.unit_fulfilled }
        column: order_ship_quantity { field: dm_fulfillment_wm9_orders_details_b2b.order_ship_quantity}
        column: order_order_quantity { field: dm_fulfillment_wm9_orders_details_b2b.order_order_quantity}
        column: order_picked_quantity { field: dm_fulfillment_wm9_orders_details_b2b.order_picked_quantity}
        filters: {
          field: dm_fulfillment_wm9_orders_b2b.order_status_description
          value: "Closed,Shipped Complete,Delivered,Loaded"
        }
        # filters: {
        #   field: dm_fulfillment_wm9_orders_b2b.order_creation_time
        #   value: "2021/02/09"
        # }
        # filters: {
        #   field: dm_fulfillment_wm9_orders_b2b.facility_name
        #   value: "JAFZA"
        # }
        # filters: {
        #   field: dm_fulfillment_wm9_orders_b2b.order_id
        #   value: "1020300647"
        # }
      }
  }

  set: Order_fulfillment {
    fields: [dm_fulfillment_wm9_orders_details_b2b.facility_name,
      dm_fulfillment_wm9_orders_details_b2b.order_id,
      dm_fulfillment_wm9_orders_details_b2b.order_extern_orderkey,
      dm_fulfillment_wm9_orders_details_b2b.order_status_description,
      dm_fulfillment_wm9_orders_details_b2b.storerkey,
      dm_fulfillment_wm9_orders_details_b2b.owner,
      dm_fulfillment_wm9_orders_details_b2b.vas_applied,
      dm_fulfillment_wm9_orders_details_b2b.vas_reason,
      dm_fulfillment_wm9_orders_details_b2b.external_factor,
      dm_fulfillment_wm9_orders_details_b2b.fc_controllable,
      dm_fulfillment_wm9_orders_details_b2b.order_creation_raw,
      dm_fulfillment_wm9_orders_details_b2b.picked_raw,
      dm_fulfillment_wm9_orders_details_b2b.packed_raw,
      dm_fulfillment_wm9_orders_details_b2b.ready_to_ship_raw,
      dm_fulfillment_wm9_orders_details_b2b.shipped_raw,
      dm_fulfillment_wm9_orders_details_b2b.order_order_quantity,
      dm_fulfillment_wm9_orders_details_b2b.order_ship_quantity,
      dm_fulfillment_wm9_orders_details_b2b.order_picked_quantity,
      dm_fulfillment_wm9_orders_details_b2b.perfect_order,
      dim_order_qty_exclusion.total_division_issue,
      dm_fulfillment_wm9_orders_details_b2b.pack_on_time,
      is_order_fulfilled,
      dim_logistics_claims.valid_claims]
  }

  dimension: pk {
    primary_key: yes
    type: string
    hidden: yes
    sql: ${order_id} ;;
  }

  dimension: order_id {}
  dimension: order_order_quantity {}
  dimension: order_ship_quantity {}
  dimension: order_picked_quantity {}

  measure: order_order_quantity_sum {
    label: "Total Order quantity"
    type: sum
    sql:  ${order_order_quantity};;
    #filters: [dm_fulfillment_wm9_orders_b2b.order_status_code: "-00"]
    #filters: [dm_fulfillment_wm9_orders_b2b.order_status_code: "102,95"]
  }

  measure: order_ship_quantity_sum {
    label: "Total Shipped quantity"
    type: sum
    sql:  ${order_ship_quantity};;
    #filters: [dm_fulfillment_wm9_orders_b2b.order_status_code: "-00"]
    #filters: [dm_fulfillment_wm9_orders_b2b.order_status_code: "102,95"]
  }

  measure: order_pick_quantity_sum {
    label: "Total Picked quantity"
    type: sum
    sql:  ${order_picked_quantity};;
    #filters: [dm_fulfillment_wm9_orders_b2b.order_status_code: "-00"]
    #filters: [dm_fulfillment_wm9_orders_b2b.order_status_code: "102,95"]
  }

  measure: order_code{
    type: sum
    sql: case
          when ${dm_fulfillment_wm9_orders_b2b.order_status_code} in ('102','88') then   ${order_picked_quantity}
          when ${dm_fulfillment_wm9_orders_b2b.order_status_code} in ('95','101') then ${order_ship_quantity}
        end  ;;
  }

  measure: order_fulfilled_percnt {
    label: "Order Fulfilled %"
    description: "Shipped qty vs order qty at order level"
    type: number
    value_format_name: percent_2
    #sql: safe_divide(${order_ship_quantity_sum} + ${dim_order_qty_exclusion.total_division_issue},${order_order_quantity_sum}) ;;
    sql: safe_divide(${order_code} + ${dim_order_qty_exclusion.total_division_issue},${order_order_quantity_sum});;
    #filters: [dm_fulfillment_wm9_orders_b2b.order_status_code: "102,95"]
}

  dimension: is_order_fulfilled {
    label: "Order Fulfilled Check"
    type: yesno
    #sql: ${order_order_quantity} = ${order_ship_quantity} + coalesce(${dim_order_qty_exclusion.division_issue},0) ;;
    sql:
        case
          when ${dm_fulfillment_wm9_orders_b2b.order_status_code} in ('102','88') then
            ${order_order_quantity} = ${order_picked_quantity} + coalesce(${dim_order_qty_exclusion.division_issue},0)
          when ${dm_fulfillment_wm9_orders_b2b.order_status_code} in ('95','101') then
            ${order_order_quantity} = ${order_ship_quantity} + coalesce(${dim_order_qty_exclusion.division_issue},0)
        end;;
  }

  measure: order_fulfilled_100_pcnt {
    label: "Count Of Order Fulfilled 100%"
    type: count_distinct
    sql: ${order_id};;
    #filters: [is_order_fulfilled: "Yes",dm_fulfillment_wm9_orders_b2b.order_status_code: "-00"]
    filters: [is_order_fulfilled: "Yes"]
    #,dm_fulfillment_wm9_orders_b2b.order_status_code: "102,95"]
    drill_fields: [Order_fulfillment*]
  }

  measure: order_fulfilled_not_100_pcnt {
    label: "Count Of Order Not Fulfilled 100%"
    type: count_distinct
    sql: ${order_id};;
    #filters: [is_order_fulfilled: "Yes",dm_fulfillment_wm9_orders_b2b.order_status_code: "-00"]
    filters: [is_order_fulfilled: "No"]
    #,dm_fulfillment_wm9_orders_b2b.order_status_code: "102,95"]
    drill_fields: [Order_fulfillment*]
  }

}
