view: tp95_logitude {
  derived_table: {
    explore_source: logitude_summary {
      column: shipment_id {}
      column: direction {}
      column: main_carriage_transport_mode {}
      column: country_of_departure {}
      column: country_of_destination {}
      column: is_courier {}
      column: dgr {}
      column: goods_readiness_date {}
      column: delivery_date {}
      column: target_delivery_date {}
      column: air_export_uae_saudi_courier_leadtime {}
      column: air_export_uae_saudi_leadtime {}
      column: air_export_uae_saudi_courier_dg_leadtime {}
      column: air_export_uae_saudi_dg_leadtime {}
      column: land_export_uae_saudi_dg_leadtime {}
      column: land_export_uae_saudi_courier_leadtime  {}
      column: land_export_uae_saudi_courier_dg_leadtime  {}
      column: land_export_uae_saudi_leadtime  {}
      column: ocean_export_UAE_saudi_leadtime {}
      column: air_import_US_saudi_dg_leadtime {}
      column: air_import_US_saudi_leadtime  {}
      column: air_import_ITA_saudi_dg_leadtime {}
      column: air_import_ITA_saudi_leadtime {}
      column: air_import_HK_saudi_dg_leadtime {}
      column: air_import_HK_saudi_leadtime {}
      column: air_import_france_saudi_dg_leadtime {}
      column: air_import_france_saudi_courier_leadtime {}
      column: air_import_france_saudi_leadtime {}
      column: ocean_import_france_saudi_leadtime {}
      column: ocean_import_US_saudi_leadtime {}
      column: ocean_import_HK_saudi_leadtime {}
      column: ocean_import_ITA_saudi_leadtime {}
      column: ocean_import_france_uae_leadtime {}
      column: ocean_import_ita_uae_leadtime {}
      column: ocean_import_HK_uae_leadtime {}
      column: air_import_france_uae_dg_leadtime {}
      column: air_import_france_uae_courier_leadtime {}
      column: air_import_france_uae_leadtime {}
      column: air_import_HK_uae_dg_leadtime {}
      column: air_import_HK_uae_leadtime {}
      column: air_import_ITA_uae_dg_leadtime {}
      column: air_import_ITA_uae_leadtime {}
      column: air_import_ITA_uae_courier_leadtime {}
      column: air_import_US_uae_leadtime {}
      column: air_import_US_uae_dg_leadtime {}
      column: ocean_import_US_kuwait_leadtime {}
      column: air_import_US_kuwait_leadtime {}
      column: air_import_US_kuwait_dg_leadtime {}
      column: ocean_import_france_kuwait_leadtime {}
      column: air_import_france_kuwait_leadtime {}
      column: air_import_france_kuwait_dg_leadtime {}
      column: ocean_import_ITA_kuwait_leadtime {}
      column: air_import_ITA_kuwait_leadtime {}
      column: air_import_ITA_kuwait_dg_leadtime {}
      column: ocean_import_HK_kuwait_leadtime {}
      column: air_import_HK_kuwait_leadtime {}
      column: air_import_HK_kuwait_dg_leadtime {}
      column: ocean_export_UAE_kuwait_leadtime {}
      column: land_export_uae_kuwait_dg_leadtime {}
      column: land_export_uae_kuwait_courier_leadtime {}
      column: land_export_uae_kuwait_leadtime {}
      column: air_export_uae_kuwait_dg_leadtime {}
      column: air_export_uae_kuwait_courier_leadtime {}
      column: air_export_uae_kuwait_courier_dg_leadtime {}
      column: air_export_uae_kuwait_leadtime {}
      column: ocean_import_france_bahrain_leadtime {}
      column: ocean_import_US_bahrain_leadtime {}
      column: ocean_import_HK_bahrain_leadtime {}
      column: ocean_import_ITA_bahrain_leadtime {}
      column: ocean_export_UAE_bahrain_leadtime {}
      column: air_import_US_bahrain_dg_leadtime {}
      column: air_import_US_bahrain_leadtime {}
      column: air_import_ITA_bahrain_leadtime {}
      column: air_import_ITA_bahrain_dg_leadtime {}
      column: air_import_HK_bahrain_dg_leadtime {}
      column: air_import_HK_bahrain_leadtime {}
      column: air_import_france_bahrain_dg_leadtime {}
      column: air_import_france_bahrain_courier_leadtime {}
      column: air_import_france_bahrain_leadtime {}
      column: land_export_uae_bahrain_dg_leadtime {}
      column: land_export_uae_bahrain_courier_leadtime {}
      column: land_export_uae_bahrain_leadtime {}
      column: air_export_uae_bahrain_dg_leadtime {}
      column: air_export_uae_bahrain_leadtime {}
      column:  air_export_uae_bahrain_courier_dg_leadtime{}
      column: ocean_import_US_qatar_leadtime {}
      column: ocean_import_ITA_qatar_leadtime {}
      column: ocean_import_HK_qatar_leadtime {}
      column: ocean_import_france_qatar_leadtime {}
      column: ocean_export_UAE_qatar_leadtime {}
      column: air_import_US_qatar_dg_leadtime {}
      column: air_import_US_qatar_leadtime {}
      column: air_import_france_qatar_leadtime {}
      column: air_import_france_qatar_dg_leadtime {}
      column: air_import_ITA_qatar_leadtime {}
      column: air_import_ITA_qatar_dg_leadtime {}
      column: air_import_HK_qatar_leadtime {}
      column: air_import_HK_qatar_dg_leadtime {}
      column: land_export_uae_qatar_dg_leadtime {}
      column: land_export_uae_qatar_courier_leadtime {}
      column: land_export_uae_qatar_courier_dg_leadtime {}
      column: land_export_uae_qatar_leadtime {}
      column: air_export_uae_qatar_dg_leadtime {}
      column: air_export_uae_qatar_courier_leadtime {}
      column: air_export_uae_qatar_courier_dg_leadtime {}
      column: air_export_uae_qatar_leadtime {}
      column: air_import_ITA_egypt_leadtime {}
      column: air_import_france_egypt_leadtime {}
      column: air_import_france_egypt_dg_leadtime {}
      column: air_import_HK_egypt_leadtime {}
      column: ocean_export_UAE_egypt_leadtime {}
      column: land_export_UAE_egypt_leadtime {}
      column: air_export_UAE_egypt_leadtime {}
      column: air_export_UAE_egypt_courier_leadtime {}

      derived_column: tp95_air_export_uae_saudi_courier {
        sql: CASE WHEN  air_export_uae_saudi_courier_leadtime IS NULL THEN NULL
    WHEN  air_export_uae_saudi_courier_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date,target_delivery_date ORDER BY air_export_uae_saudi_leadtime)*100)<=95 THEN "YES" ELSE "NO" END;;
      }
      derived_column: tp95_air_export_uae_saudi {
        sql: CASE WHEN air_export_uae_saudi_leadtime IS NULL THEN NULL
        WHEN air_export_uae_saudi_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY air_export_uae_saudi_leadtime)*100)<=95 THEN "YES" ELSE "NO" END;;
      }
      derived_column: tp95_air_export_uae_saudi_courier_dg {
        sql:  CASE WHEN air_export_uae_saudi_courier_dg_leadtime IS NULL THEN NULL
        WHEN air_export_uae_saudi_courier_dg_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY air_export_uae_saudi_courier_dg_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_air_export_uae_saudi_dg {
        sql: CASE WHEN air_export_uae_saudi_dg_leadtime IS NULL THEN NULL
        WHEN air_export_uae_saudi_dg_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY air_export_uae_saudi_dg_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_land_export_uae_saudi_dg {
        sql: CASE WHEN land_export_uae_saudi_dg_leadtime IS NULL THEN NULL
        WHEN land_export_uae_saudi_dg_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY land_export_uae_saudi_dg_leadtime)*100)<=95 THEN "YES" ELSE "NO" END;;
      }
      derived_column: tp95_land_export_uae_saudi_courier {
        sql: CASE WHEN land_export_uae_saudi_courier_leadtime IS NULL THEN NULL
          WHEN land_export_uae_saudi_courier_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY land_export_uae_saudi_courier_leadtime)*100)<=95 THEN "YES" ELSE "NO" END;;
      }
      derived_column: tp95_land_export_uae_saudi_courier_dg {
        sql: CASE WHEN land_export_uae_saudi_courier_dg_leadtime IS NULL THEN NULL
          WHEN land_export_uae_saudi_courier_dg_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY land_export_uae_saudi_courier_dg_leadtime)*100)<=95 THEN "YES" ELSE "NO" END;;
      }
      derived_column: tp95_land_export_uae_saudi {
        sql: CASE WHEN land_export_uae_saudi_leadtime IS NULL THEN NULL
          WHEN land_export_uae_saudi_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY land_export_uae_saudi_leadtime)*100)<=95 THEN "YES" ELSE "NO" END;;
      }
      derived_column: tp95_ocean_export_UAE_saudi {
        sql: CASE WHEN ocean_export_UAE_saudi_leadtime IS NULL THEN NULL
          WHEN ocean_export_UAE_saudi_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY ocean_export_UAE_saudi_leadtime)*100)<=95 THEN "YES" ELSE "NO" END  ;;
      }
      derived_column: tp95_air_import_US_saudi_dg{
        sql: CASE WHEN air_import_US_saudi_dg_leadtime IS NULL THEN NULL
          WHEN air_import_US_saudi_dg_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY air_import_US_saudi_dg_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_air_import_US_saudi{
        sql: CASE WHEN air_import_US_saudi_leadtime IS NULL THEN NULL
          WHEN air_import_US_saudi_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY air_import_US_saudi_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_air_import_ITA_saudi_dg{
        sql: CASE WHEN air_import_ITA_saudi_dg_leadtime IS NULL THEN NULL
        WHEN air_import_ITA_saudi_dg_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY air_import_ITA_saudi_dg_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_air_import_ITA_saudi {
        sql: CASE WHEN air_import_ITA_saudi_leadtime IS NULL THEN NULL
        WHEN air_import_ITA_saudi_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY air_import_ITA_saudi_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_air_import_HK_saudi_dg {
        sql: CASE WHEN air_import_HK_saudi_dg_leadtime IS NULL THEN NULL
        WHEN air_import_HK_saudi_dg_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY air_import_HK_saudi_dg_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_air_import_HK_saudi{
        sql: CASE WHEN air_import_HK_saudi_leadtime IS NULL THEN NULL
        WHEN air_import_HK_saudi_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY air_import_HK_saudi_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_air_import_france_saudi_dg{
        sql: CASE WHEN air_import_france_saudi_dg_leadtime IS NULL THEN NULL
        WHEN air_import_france_saudi_dg_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY air_import_france_saudi_dg_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_air_import_france_saudi_courier{
        sql: CASE WHEN air_import_france_saudi_courier_leadtime IS NULL THEN NULL
          WHEN air_import_france_saudi_dg_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY air_import_france_saudi_courier_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_air_import_france_saudi{
        sql: CASE WHEN air_import_france_saudi_leadtime IS NULL THEN NULL
          WHEN air_import_france_saudi_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY air_import_france_saudi_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_ocean_import_france_saudi{
        sql: CASE WHEN ocean_import_france_saudi_leadtime IS NULL THEN NULL
        WHEN ocean_import_france_saudi_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY ocean_import_france_saudi_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_ocean_import_US_saudi {
        sql: CASE WHEN ocean_import_US_saudi_leadtime IS NULL THEN NULL
        WHEN ocean_import_US_saudi_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY ocean_import_US_saudi_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_ocean_import_HK_saudi{
        sql: CASE WHEN ocean_import_HK_saudi_leadtime IS NULL THEN NULL
        WHEN ocean_import_HK_saudi_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY ocean_import_HK_saudi_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_ocean_import_ITA_saudi {
        sql: CASE WHEN ocean_import_ITA_saudi_leadtime IS NULL THEN NULL
        WHEN ocean_import_ITA_saudi_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY ocean_import_ITA_saudi_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_ocean_import_france_uae{
        sql: CASE WHEN ocean_import_france_uae_leadtime IS NULL THEN NULL
        WHEN ocean_import_france_uae_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY ocean_import_france_uae_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_ocean_import_ita_uae {
        sql: CASE WHEN ocean_import_ita_uae_leadtime IS NULL THEN NULL
        WHEN ocean_import_ita_uae_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY ocean_import_ita_uae_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_ocean_import_HK_uae {
        sql: CASE WHEN ocean_import_HK_uae_leadtime IS NULL THEN NULL
        WHEN ocean_import_HK_uae_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY ocean_import_HK_uae_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_air_import_france_uae_dg{
        sql: CASE WHEN air_import_france_uae_dg_leadtime IS NULL THEN NULL
        WHEN air_import_france_uae_dg_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY air_import_france_uae_dg_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_air_import_france_uae_courier {
        sql: CASE WHEN air_import_france_uae_courier_leadtime IS NULL THEN NULL
        WHEN air_import_france_uae_courier_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY air_import_france_uae_courier_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_air_import_france_uae {
        sql: CASE WHEN air_import_france_uae_leadtime IS NULL THEN NULL
        WHEN air_import_france_uae_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY air_import_france_uae_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_air_import_HK_uae_dg {
        sql: CASE WHEN air_import_HK_uae_dg_leadtime IS NULL THEN NULL
        WHEN air_import_HK_uae_dg_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY air_import_HK_uae_dg_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_air_import_HK_uae {
        sql: CASE WHEN air_import_HK_uae_leadtime IS NULL THEN NULL
        WHEN air_import_HK_uae_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY air_import_HK_uae_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_air_import_ITA_uae_dg {
        sql: CASE WHEN air_import_ITA_uae_dg_leadtime IS NULL THEN NULL
        WHEN air_import_ITA_uae_dg_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY air_import_ITA_uae_dg_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }

      derived_column: tp95_air_import_ITA_uae {
        sql: CASE WHEN air_import_ITA_uae_leadtime IS NULL THEN NULL
          WHEN air_import_ITA_uae_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY air_import_ITA_uae_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_air_import_ITA_uae_courier {
        sql: CASE WHEN air_import_ITA_uae_courier_leadtime IS NULL THEN NULL
        WHEN air_import_ITA_uae_courier_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY air_import_ITA_uae_courier_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_air_import_US_uae_dg {
        sql: CASE WHEN air_import_US_uae_dg_leadtime IS NULL THEN NULL
          WHEN air_import_US_uae_dg_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY air_import_US_uae_dg_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }

      derived_column: tp95_air_import_US_uae {
        sql: CASE WHEN air_import_US_uae_leadtime IS NULL THEN NULL
          WHEN air_import_US_uae_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY air_import_US_uae_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_ocean_import_US_kuwait{
        sql: CASE WHEN ocean_import_US_kuwait_leadtime IS NULL THEN NULL
        WHEN ocean_import_US_kuwait_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY ocean_import_US_kuwait_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_air_import_US_kuwait {
        sql: CASE WHEN air_import_US_kuwait_leadtime IS NULL THEN NULL
        WHEN air_import_US_kuwait_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY air_import_US_kuwait_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_air_import_US_kuwait_dg{
        sql: CASE WHEN air_import_US_kuwait_dg_leadtime IS NULL THEN NULL
        WHEN air_import_US_kuwait_dg_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY air_import_US_kuwait_dg_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_ocean_import_france_kuwait{
        sql: CASE WHEN ocean_import_france_kuwait_leadtime IS NULL THEN NULL
        WHEN ocean_import_france_kuwait_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY ocean_import_france_kuwait_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_air_import_france_kuwait{
       sql: CASE WHEN air_import_france_kuwait_leadtime IS NULL THEN NULL
        WHEN air_import_france_kuwait_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY air_import_france_kuwait_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_air_import_france_kuwait_dg{
        sql: CASE WHEN air_import_france_kuwait_dg_leadtime IS NULL THEN NULL
        WHEN air_import_france_kuwait_dg_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY air_import_france_kuwait_dg_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_ocean_import_ITA_kuwait{
        sql: CASE WHEN ocean_import_ITA_kuwait_leadtime IS NULL THEN NULL
        WHEN ocean_import_ITA_kuwait_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY ocean_import_ITA_kuwait_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_air_import_ITA_kuwait{
        sql: CASE WHEN air_import_ITA_kuwait_leadtime IS NULL THEN NULL
        WHEN air_import_ITA_kuwait_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY air_import_ITA_kuwait_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_air_import_ITA_kuwait_dg{
        sql: CASE WHEN air_import_ITA_kuwait_dg_leadtime IS NULL THEN NULL
        WHEN air_import_ITA_kuwait_dg_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY air_import_ITA_kuwait_dg_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_ocean_import_HK_kuwait{
        sql: CASE WHEN ocean_import_HK_kuwait_leadtime IS NULL THEN NULL
        WHEN ocean_import_HK_kuwait_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY ocean_import_HK_kuwait_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_air_import_HK_kuwait{
        sql: CASE WHEN air_import_HK_kuwait_leadtime IS NULL THEN NULL
        WHEN air_import_HK_kuwait_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY air_import_HK_kuwait_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_air_import_HK_kuwait_dg{
        sql: CASE WHEN air_import_HK_kuwait_dg_leadtime IS NULL THEN NULL
        WHEN air_import_HK_kuwait_dg_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY air_import_HK_kuwait_dg_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_ocean_export_UAE_kuwait{
        sql:CASE WHEN ocean_export_UAE_kuwait_leadtime IS NULL THEN NULL
        WHEN ocean_export_UAE_kuwait_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY ocean_export_UAE_kuwait_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_land_export_uae_kuwait_dg{
        sql:CASE WHEN land_export_uae_kuwait_dg_leadtime IS NULL THEN NULL
        WHEN land_export_uae_kuwait_dg_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY land_export_uae_kuwait_dg_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_land_export_uae_kuwait_courier{
        sql:CASE WHEN land_export_uae_kuwait_courier_leadtime IS NULL THEN NULL
        WHEN land_export_uae_kuwait_courier_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY land_export_uae_kuwait_courier_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_land_export_uae_kuwait{
        sql:CASE WHEN land_export_uae_kuwait_leadtime IS NULL THEN NULL
        WHEN land_export_uae_kuwait_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY land_export_uae_kuwait_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_air_export_uae_kuwait_dg{
        sql: CASE WHEN air_export_uae_kuwait_dg_leadtime IS NULL THEN NULL
        WHEN air_export_uae_kuwait_dg_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY air_export_uae_kuwait_dg_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_air_export_uae_kuwait_courier{
        sql: CASE WHEN air_export_uae_kuwait_courier_leadtime IS NULL THEN NULL
        WHEN air_export_uae_kuwait_courier_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY air_export_uae_kuwait_courier_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_air_export_uae_kuwait_courier_dg{
        sql: CASE WHEN air_export_uae_kuwait_courier_dg_leadtime IS NULL THEN NULL
        WHEN air_export_uae_kuwait_courier_dg_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY air_export_uae_kuwait_courier_dg_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_air_export_uae_kuwait{
        sql: CASE WHEN air_export_uae_kuwait_leadtime IS NULL THEN NULL
        WHEN air_export_uae_kuwait_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY air_export_uae_kuwait_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_ocean_import_france_bahrain {
        sql: CASE WHEN ocean_import_france_bahrain_leadtime IS NULL THEN NULL
        WHEN ocean_import_france_bahrain_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY ocean_import_france_bahrain_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_ocean_import_US_bahrain {
        sql: CASE WHEN ocean_import_US_bahrain_leadtime IS NULL THEN NULL
        WHEN ocean_import_US_bahrain_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY ocean_import_US_bahrain_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_ocean_import_HK_bahrain{
        sql: CASE WHEN ocean_import_HK_bahrain_leadtime IS NULL THEN NULL
        WHEN ocean_import_HK_bahrain_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY ocean_import_HK_bahrain_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_ocean_import_ITA_bahrain{
        sql: CASE WHEN ocean_import_ITA_bahrain_leadtime IS NULL THEN NULL
        WHEN ocean_import_ITA_bahrain_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY ocean_import_ITA_bahrain_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_ocean_export_UAE_bahrain{
        sql: CASE WHEN ocean_export_UAE_bahrain_leadtime IS NULL THEN NULL
        WHEN ocean_export_UAE_bahrain_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY ocean_export_UAE_bahrain_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_air_import_US_bahrain_dg{
        sql: CASE WHEN air_import_US_bahrain_dg_leadtime IS NULL THEN NULL
        WHEN air_import_US_bahrain_dg_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY air_import_US_bahrain_dg_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_air_import_US_bahrain{
        sql: CASE WHEN air_import_US_bahrain_leadtime IS NULL THEN NULL
        WHEN air_import_US_bahrain_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY air_import_US_bahrain_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_air_import_ITA_bahrain{
        sql: CASE WHEN air_import_ITA_bahrain_leadtime IS NULL THEN NULL
        WHEN air_import_ITA_bahrain_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY air_import_ITA_bahrain_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_air_import_ITA_bahrain_dg{
        sql: CASE WHEN air_import_ITA_bahrain_dg_leadtime IS NULL THEN NULL
        WHEN air_import_ITA_bahrain_dg_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY air_import_ITA_bahrain_dg_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_air_import_HK_bahrain_dg{
        sql: CASE WHEN air_import_HK_bahrain_dg_leadtime IS NULL THEN NULL
        WHEN air_import_HK_bahrain_dg_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY air_import_HK_bahrain_dg_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_air_import_HK_bahrain{
        sql: CASE WHEN air_import_HK_bahrain_leadtime IS NULL THEN NULL
        WHEN air_import_HK_bahrain_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY air_import_HK_bahrain_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_air_import_france_bahrain_dg{
        sql: CASE WHEN air_import_france_bahrain_dg_leadtime IS NULL THEN NULL
        WHEN air_import_france_bahrain_dg_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY air_import_france_bahrain_dg_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_air_import_france_bahrain_courier{
        sql: CASE WHEN air_import_france_bahrain_courier_leadtime IS NULL THEN NULL
        WHEN air_import_france_bahrain_courier_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY air_import_france_bahrain_courier_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_air_import_france_bahrain{
        sql: CASE WHEN air_import_france_bahrain_leadtime IS NULL THEN NULL
        WHEN air_import_france_bahrain_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY air_import_france_bahrain_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_land_export_uae_bahrain_dg{
        sql: CASE WHEN land_export_uae_bahrain_dg_leadtime IS NULL THEN NULL
        WHEN land_export_uae_bahrain_dg_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY land_export_uae_bahrain_dg_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_land_export_uae_bahrain_courier{
        sql: CASE WHEN land_export_uae_bahrain_courier_leadtime IS NULL THEN NULL
        WHEN land_export_uae_bahrain_courier_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY land_export_uae_bahrain_courier_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_land_export_uae_bahrain{
        sql: CASE WHEN land_export_uae_bahrain_leadtime IS NULL THEN NULL
        WHEN land_export_uae_bahrain_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY land_export_uae_bahrain_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_air_export_uae_bahrain_dg{
        sql: CASE WHEN air_export_uae_bahrain_dg_leadtime IS NULL THEN NULL
        WHEN air_export_uae_bahrain_dg_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY air_export_uae_bahrain_dg_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_air_export_uae_bahrain{
        sql: CASE WHEN air_export_uae_bahrain_leadtime IS NULL THEN NULL
        WHEN air_export_uae_bahrain_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY air_export_uae_bahrain_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_air_export_uae_bahrain_courier_dg{
        sql: CASE WHEN air_export_uae_bahrain_courier_dg_leadtime IS NULL THEN NULL
        WHEN air_export_uae_bahrain_courier_dg_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY air_export_uae_bahrain_courier_dg_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }

      derived_column: tp95_ocean_import_US_qatar{
        sql: CASE WHEN ocean_import_US_qatar_leadtime IS NULL THEN NULL
        WHEN ocean_import_US_qatar_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY ocean_import_US_qatar_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_ocean_import_ITA_qatar{
        sql: CASE WHEN ocean_import_ITA_qatar_leadtime IS NULL THEN NULL
        WHEN ocean_import_ITA_qatar_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY ocean_import_ITA_qatar_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_ocean_import_HK_qatar{
        sql: CASE WHEN ocean_import_HK_qatar_leadtime IS NULL THEN NULL
        WHEN ocean_import_HK_qatar_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY ocean_import_HK_qatar_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_ocean_import_france_qatar{
        sql: CASE WHEN ocean_import_france_qatar_leadtime IS NULL THEN NULL
        WHEN ocean_import_france_qatar_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY ocean_import_france_qatar_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_ocean_export_UAE_qatar{
        sql: CASE WHEN ocean_export_UAE_qatar_leadtime IS NULL THEN NULL
        WHEN ocean_export_UAE_qatar_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY ocean_export_UAE_qatar_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_air_import_US_qatar_dg{
        sql: CASE WHEN air_import_US_qatar_dg_leadtime IS NULL THEN NULL
        WHEN air_import_US_qatar_dg_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY air_import_US_qatar_dg_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_air_import_US_qatar{
        sql: CASE WHEN air_import_US_qatar_leadtime IS NULL THEN NULL
        WHEN air_import_US_qatar_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY air_import_US_qatar_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_air_import_france_qatar{
        sql: CASE WHEN air_import_france_qatar_leadtime IS NULL THEN NULL
        WHEN air_import_france_qatar_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY air_import_france_qatar_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_air_import_france_qatar_dg{
        sql: CASE WHEN air_import_france_qatar_dg_leadtime IS NULL THEN NULL
        WHEN air_import_france_qatar_dg_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY air_import_france_qatar_dg_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_air_import_ITA_qatar{
        sql: CASE WHEN air_import_ITA_qatar_leadtime IS NULL THEN NULL
        WHEN air_import_ITA_qatar_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY air_import_ITA_qatar_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_air_import_ITA_qatar_dg{
        sql: CASE WHEN air_import_ITA_qatar_dg_leadtime IS NULL THEN NULL
        WHEN air_import_ITA_qatar_dg_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY air_import_ITA_qatar_dg_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_air_import_HK_qatar{
        sql: CASE WHEN air_import_HK_qatar_leadtime IS NULL THEN NULL
        WHEN air_import_HK_qatar_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY air_import_HK_qatar_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_air_import_HK_qatar_dg{
        sql: CASE WHEN air_import_HK_qatar_dg_leadtime IS NULL THEN NULL
        WHEN air_import_HK_qatar_dg_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY air_import_HK_qatar_dg_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_land_export_uae_qatar_dg{
        sql: CASE WHEN land_export_uae_qatar_dg_leadtime IS NULL THEN NULL
        WHEN land_export_uae_qatar_dg_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY land_export_uae_qatar_dg_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_land_export_uae_qatar_courier{
        sql: CASE WHEN land_export_uae_qatar_courier_leadtime IS NULL THEN NULL
        WHEN land_export_uae_qatar_courier_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY land_export_uae_qatar_courier_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_land_export_uae_qatar_courier_dg{
        sql: CASE WHEN land_export_uae_qatar_courier_dg_leadtime IS NULL THEN NULL
        WHEN land_export_uae_qatar_courier_dg_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY land_export_uae_qatar_courier_dg_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_land_export_uae_qatar{
        sql: CASE WHEN land_export_uae_qatar_leadtime IS NULL THEN NULL
        WHEN land_export_uae_qatar_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY land_export_uae_qatar_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_air_export_uae_qatar_dg{
        sql: CASE WHEN air_export_uae_qatar_dg_leadtime IS NULL THEN NULL
        WHEN air_export_uae_qatar_dg_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY air_export_uae_qatar_dg_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_air_export_uae_qatar_courier{
        sql: CASE WHEN air_export_uae_qatar_courier_leadtime IS NULL THEN NULL
        WHEN air_export_uae_qatar_courier_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY air_export_uae_qatar_courier_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_air_export_uae_qatar_courier_dg{
        sql: CASE WHEN air_export_uae_qatar_courier_dg_leadtime IS NULL THEN NULL
        WHEN air_export_uae_qatar_courier_dg_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY air_export_uae_qatar_courier_dg_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_air_export_uae_qatar{
        sql: CASE WHEN air_export_uae_qatar_leadtime IS NULL THEN NULL
        WHEN air_export_uae_qatar_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY air_export_uae_qatar_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_air_import_ITA_egypt{
        sql: CASE WHEN air_import_ITA_egypt_leadtime IS NULL THEN NULL
        WHEN air_import_ITA_egypt_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY air_import_ITA_egypt_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_air_import_france_egypt{
        sql: CASE WHEN air_import_france_egypt_leadtime IS NULL THEN NULL
        WHEN air_import_france_egypt_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY air_import_france_egypt_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_air_import_france_egypt_dg {
        sql: CASE WHEN air_import_france_egypt_dg_leadtime IS NULL THEN NULL
        WHEN air_import_france_egypt_dg_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY air_import_france_egypt_dg_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_air_import_HK_egypt{
        sql: CASE WHEN air_import_HK_egypt_leadtime IS NULL THEN NULL
        WHEN air_import_HK_egypt_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY air_import_HK_egypt_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_ocean_export_UAE_egypt{
        sql: CASE WHEN ocean_export_UAE_egypt_leadtime IS NULL THEN NULL
        WHEN ocean_export_UAE_egypt_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY ocean_export_UAE_egypt_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_land_export_UAE_egypt{
        sql: CASE WHEN land_export_UAE_egypt_leadtime IS NULL THEN NULL
        WHEN land_export_UAE_egypt_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY land_export_UAE_egypt_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_air_export_UAE_egypt{
        sql: CASE WHEN air_export_UAE_egypt_leadtime IS NULL THEN NULL
        WHEN air_export_UAE_egypt_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY air_export_UAE_egypt_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }
      derived_column: tp95_air_export_UAE_egypt_courier {
        sql: CASE WHEN air_export_UAE_egypt_courier_leadtime IS NULL THEN NULL
        WHEN air_export_UAE_egypt_courier_leadtime IS NOT NULL AND ROUND(PERCENT_RANK() OVER(PARTITION BY shipment_id,direction,main_carriage_transport_mode,country_of_departure,country_of_destination,is_courier,dgr,goods_readiness_date,delivery_date ORDER BY air_export_UAE_egypt_courier_leadtime)*100)<=95 THEN "YES" ELSE "NO" END ;;
      }

      }
}
  dimension: direction {}
  dimension: shipment_id {}
  dimension: main_carriage_transport_mode {}
  dimension: country_of_departure {}
  dimension: country_of_destination {}
  dimension: is_courier {}
  dimension: dgr {}
  dimension: goods_readiness_date {}
  dimension_group: goods_readiness_date_filter  {
    label: "Goods Readiness Date"
    type: time
    timeframes: [raw,time,date,week,month,quarter,year]
    sql:  CAST((CASE WHEN ${goods_readiness_date} = "nan" OR ${goods_readiness_date} = "NaT" THEN NULL
        WHEN ${goods_readiness_date} LIKE "2021-%" OR ${goods_readiness_date} LIKE "2020-%" THEN PARSE_TIMESTAMP('%d-%M-%Y %T',CONCAT(SUBSTRING(SPLIT(${goods_readiness_date},'-')[offset(2)],0,2),'-',SPLIT(${goods_readiness_date},'-')[offset(1)],'-',SPLIT(${goods_readiness_date},'-')[offset(0)],' ',SPLIT(${goods_readiness_date},' ')[offset(1)]))
        ELSE PARSE_TIMESTAMP('%d %b %Y %T', ${goods_readiness_date})END)  AS TIMESTAMP) ;;
  }
  dimension: delivery_date {}
  dimension_group: delivery_date_filter {
    label: "Delivery Date"
    type: time
    timeframes: [raw,time,date,week,month,quarter,year]
    sql: CAST((CASE WHEN ${delivery_date} = "nan" OR ${delivery_date} = "NaT"THEN NULL
           WHEN ${delivery_date} LIKE "2021-%" OR ${delivery_date} LIKE "2020-%" THEN PARSE_TIMESTAMP('%d-%M-%Y %T',CONCAT(SUBSTRING(SPLIT(${delivery_date},'-')[offset(2)],0,2),'-',SPLIT(${delivery_date},'-')[offset(1)],'-',SPLIT(${delivery_date},'-')[offset(0)],' ',SPLIT(${delivery_date},' ')[offset(1)]))
      ELSE PARSE_TIMESTAMP('%d %b %Y',${delivery_date})END) AS TIMESTAMP) ;;
  }
  dimension: target_delivery_date {}
  dimension_group: target_delivery_date_filter {
    type: time
    timeframes: [raw,
      time,
      date,
      week,
      month,
      quarter,
      year]
    sql: CAST(${TABLE}.target_delivery_date AS TIMESTAMP) ;;
  }

  dimension: air_export_uae_saudi_courier_leadtime {
    type: number
  }
  dimension: tp95_air_export_uae_saudi_courier {
    type: string
  }
  dimension: air_export_uae_saudi_leadtime {
    type: number
  }
  dimension: tp95_air_export_uae_saudi {
    type: string
  }
  dimension: air_export_uae_saudi_courier_dg_leadtime {
    type: number
  }
  dimension: tp95_air_export_uae_saudi_courier_dg {
    type: string
  }
  dimension: air_export_uae_saudi_dg_leadtime {
    type: number
  }
  dimension: tp95_air_export_uae_saudi_dg {
    type: string
  }
  dimension: land_export_uae_saudi_dg_leadtime {
    type: number
  }
  dimension: tp95_land_export_uae_saudi_dg  {
    type: string
  }
  dimension: land_export_uae_saudi_courier_leadtime  {
    type: number
  }
  dimension: tp95_land_export_uae_saudi_courier {
    type: string
  }
  dimension: land_export_uae_saudi_courier_dg_leadtime  {
    type: number
  }
  dimension: tp95_land_export_uae_saudi_courier_dg {
    type: string
  }
  dimension: land_export_uae_saudi_leadtime  {
    type: number
  }
  dimension: tp95_land_export_uae_saudi {
    type: string
  }
  dimension: ocean_export_UAE_saudi_leadtime  {
    type: number
  }
  dimension: tp95_ocean_export_UAE_saudi {
    type: string
  }
  dimension: air_import_US_saudi_dg_leadtime {
    type: number
  }
  dimension: tp95_air_import_US_saudi_dg{
    type: string
  }

  dimension: air_import_US_saudi_leadtime  {
    type: number
  }
  dimension: tp95_air_import_US_saudi  {
    type: string
  }
  dimension: air_import_ITA_saudi_dg_leadtime {
    type: number
  }
  dimension: tp95_air_import_ITA_saudi_dg{
    type: string
  }
  dimension: air_import_ITA_saudi_leadtime {
    type: number
  }
  dimension: tp95_air_import_ITA_saudi{
    type: string
  }
  dimension: air_import_HK_saudi_dg_leadtime {
    type: number
  }
  dimension: tp95_air_import_HK_saudi_dg{
    type: string
  }
  dimension: air_import_HK_saudi_leadtime {
    type: number
  }
  dimension: tp95_air_import_HK_saudi{
    type: string
  }
  dimension: air_import_france_saudi_dg_leadtime {
    type: number
  }
  dimension:tp95_air_import_france_saudi_dg{
    type: string
  }
  dimension: air_import_france_saudi_courier_leadtime {
    type: number
  }
  dimension: tp95_air_import_france_saudi_courier {
    type: string
  }
  dimension: air_import_france_saudi_leadtime {
    type: number
  }
  dimension: tp95_air_import_france_saudi {
    type: string
  }
  dimension: ocean_import_france_saudi_leadtime {
    type: number
  }
  dimension: tp95_ocean_import_france_saudi {
    type: string
  }
  dimension: ocean_import_US_saudi_leadtime {
    type: number
  }
  dimension: tp95_ocean_import_US_saudi {
    type: string
  }
  dimension: ocean_import_HK_saudi_leadtime {
    type: number
  }
  dimension: tp95_ocean_import_HK_saudi {
    type: string
  }
  dimension: ocean_import_ITA_saudi_leadtime {
    type: number
  }
  dimension: tp95_ocean_import_ITA_saudi {
    type: string
  }
  dimension: ocean_import_france_uae_leadtime {
    type: number
  }
  dimension: tp95_ocean_import_france_uae{
    type: string
  }

  dimension: ocean_import_ita_uae_leadtime {
    type: number
  }
  dimension: tp95_ocean_import_ita_uae {
    type: string
  }
  dimension: ocean_import_HK_uae_leadtime {
    type: number
  }
  dimension: tp95_ocean_import_HK_uae {
    type: string
  }
  dimension: air_import_france_uae_dg_leadtime {
    type: number
  }
  dimension:tp95_air_import_france_uae_dg {
    type: string
  }
  dimension: air_import_france_uae_courier_leadtime {
    type: number
  }
  dimension: tp95_air_import_france_uae_courier {
    type: string
  }
  dimension: air_import_france_uae_leadtime {
    type: number
  }
  dimension: tp95_air_import_france_uae {
    type: string
  }
  dimension: air_import_HK_uae_dg_leadtime {
    type: number
  }
  dimension: tp95_air_import_HK_uae_dg {
    type: string
  }
  dimension: air_import_HK_uae_leadtime {
    type: number
  }
  dimension: tp95_air_import_HK_uae {
    type: string
  }
  dimension: air_import_ITA_uae_dg_leadtime {
    type: number
  }
  dimension: tp95_air_import_ITA_uae_dg {
    type: string
  }
  dimension: air_import_ITA_uae_leadtime {
    type: number
  }
  dimension: tp95_air_import_ITA_uae {
    type: string
  }
  dimension: air_import_ITA_uae_courier_leadtime {
    type: number
  }
  dimension: tp95_air_import_ITA_uae_courier {
    type: string
  }
  dimension: air_import_US_uae_dg_leadtime {
    type: number
  }
  dimension:tp95_air_import_US_uae_dg {
    type: string
  }
  dimension: air_import_US_uae_leadtime {
    type: number
  }
  dimension: tp95_air_import_US_uae {
    type: string
  }
  dimension: tp95_ocean_import_US_kuwait{
    type: string
  }
  dimension: tp95_air_import_US_kuwait {
    type: string
  }
  dimension: tp95_air_import_US_kuwait_dg{
    type: string
  }
  dimension: tp95_ocean_import_france_kuwait{
    type: string
  }
  dimension: tp95_air_import_france_kuwait{
    type: string
  }
  dimension: tp95_air_import_france_kuwait_dg{
    type: string
  }
  dimension: tp95_ocean_import_ITA_kuwait{
    type: string
  }
  dimension: tp95_air_import_ITA_kuwait{
    type: string
  }
  dimension: tp95_air_import_ITA_kuwait_dg{
    type: string
  }
  dimension: tp95_ocean_import_HK_kuwait{
    type: string
  }
  dimension: tp95_air_import_HK_kuwait{
    type: string
  }
  dimension: tp95_air_import_HK_kuwait_dg{
    type: string
  }
  dimension: tp95_ocean_export_UAE_kuwait{
    type: string
  }
  dimension: tp95_land_export_uae_kuwait_dg{
    type: string
  }
  dimension: tp95_land_export_uae_kuwait_courier{
    type: string
  }
  dimension: tp95_land_export_uae_kuwait{
    type: string
  }
  dimension: tp95_air_export_uae_kuwait_dg{
    type: string
  }
  dimension: tp95_air_export_uae_kuwait_courier{
   type: string
  }
  dimension: tp95_air_export_uae_kuwait_courier_dg{
    type: string
  }
  dimension: tp95_air_export_uae_kuwait{
    type: string
  }
  dimension: ocean_import_US_kuwait_leadtime {
    type: number
  }
  dimension: air_import_US_kuwait_leadtime {
    type: number
  }
  dimension: air_import_US_kuwait_dg_leadtime {
    type: number
  }
  dimension: ocean_import_france_kuwait_leadtime {
    type: number
  }
  dimension: air_import_france_kuwait_leadtime {
    type: number
  }
  dimension: air_import_france_kuwait_dg_leadtime {
    type: number
  }
  dimension: ocean_import_ITA_kuwait_leadtime {
    type: number
  }
  dimension: air_import_ITA_kuwait_leadtime {
    type: number
  }
  dimension: air_import_ITA_kuwait_dg_leadtime {
    type: number
  }
  dimension: ocean_import_HK_kuwait_leadtime {
    type: number
  }
  dimension: air_import_HK_kuwait_leadtime {
    type: number
  }
  dimension: air_import_HK_kuwait_dg_leadtime {
    type: number
  }
  dimension: ocean_export_UAE_kuwait_leadtime {
    type: number
  }
  dimension: land_export_uae_kuwait_dg_leadtime {
    type: number
  }
  dimension: land_export_uae_kuwait_courier_leadtime {
    type: number
  }
  dimension: land_export_uae_kuwait_leadtime {
    type: number
  }
  dimension: air_export_uae_kuwait_dg_leadtime {
    type: number
  }
  dimension: air_export_uae_kuwait_courier_leadtime {
    type: number
  }
  dimension: air_export_uae_kuwait_courier_dg_leadtime {
    type: number
  }
  dimension: air_export_uae_kuwait_leadtime {
    type: number
  }
  dimension: ocean_import_france_bahrain_leadtime {
    type: number
  }
  dimension: ocean_import_US_bahrain_leadtime {
    type: number
  }
  dimension: ocean_import_HK_bahrain_leadtime {
    type: number
  }
  dimension: ocean_import_ITA_bahrain_leadtime {
    type: number
  }
  dimension: ocean_export_UAE_bahrain_leadtime {
    type: number
  }
  dimension: air_import_US_bahrain_dg_leadtime {
    type: number
  }
  dimension: air_import_US_bahrain_leadtime {
    type: number
  }
  dimension: air_import_ITA_bahrain_leadtime {
    type: number
  }
  dimension: air_import_ITA_bahrain_dg_leadtime {
    type: number
  }
  dimension: air_import_HK_bahrain_dg_leadtime {
    type: number
  }
  dimension: air_import_HK_bahrain_leadtime {
    type: number
  }
  dimension: air_import_france_bahrain_dg_leadtime {
    type: number
  }
  dimension: air_import_france_bahrain_courier_leadtime {
    type: number
  }
  dimension: air_import_france_bahrain_leadtime {
    type: number
  }
  dimension: land_export_uae_bahrain_dg_leadtime {
    type: number
  }
  dimension: land_export_uae_bahrain_courier_leadtime {
    type: number
  }
  dimension: land_export_uae_bahrain_leadtime {
    type: number
  }
  dimension: air_export_uae_bahrain_dg_leadtime {
    type: number
  }
  dimension: air_export_uae_bahrain_leadtime {
    type: number
  }
  dimension:  air_export_uae_bahrain_courier_dg_leadtime{
    type: number
  }
  dimension: tp95_ocean_import_france_bahrain {
    type: string
  }
  dimension: tp95_ocean_import_US_bahrain {
    type: string
  }
  dimension: tp95_ocean_import_HK_bahrain{
    type: string
  }
  dimension: tp95_ocean_import_ITA_bahrain{
    type: string
  }
  dimension: tp95_ocean_export_UAE_bahrain{
    type: string
  }
  dimension: tp95_air_import_US_bahrain_dg{
    type: string
  }
  dimension: tp95_air_import_US_bahrain{
    type: string
  }
  dimension: tp95_air_import_ITA_bahrain{
    type: string
  }
  dimension: tp95_air_import_ITA_bahrain_dg{
    type: string
  }
  dimension: tp95_air_import_HK_bahrain_dg{
    type: string
  }
  dimension: tp95_air_import_HK_bahrain{
    type: string
  }
  dimension: tp95_air_import_france_bahrain_dg{
    type: string
  }
  dimension: tp95_air_import_france_bahrain_courier{
    type: string
  }
  dimension: tp95_air_import_france_bahrain{
    type: string
  }
  dimension: tp95_land_export_uae_bahrain_dg{
    type: string
  }
  dimension: tp95_land_export_uae_bahrain_courier{
    type: string
  }
  dimension: tp95_land_export_uae_bahrain{
    type: string
  }
  dimension: tp95_air_export_uae_bahrain_dg{
    type: string
  }
  dimension: tp95_air_export_uae_bahrain{
    type: string
  }
  dimension: tp95_air_export_uae_bahrain_courier_dg{
    type: string
  }
  dimension: ocean_import_US_qatar_leadtime {
    type: number
  }
  dimension: ocean_import_ITA_qatar_leadtime {
    type: number
  }
  dimension: ocean_import_HK_qatar_leadtime {
    type: number
  }
  dimension: ocean_import_france_qatar_leadtime {
    type: number
  }
  dimension: ocean_export_UAE_qatar_leadtime {
    type: number
  }
  dimension: air_import_US_qatar_dg_leadtime {
    type: number
  }
  dimension: air_import_US_qatar_leadtime {
    type: number
  }
  dimension: air_import_france_qatar_leadtime {
    type: number
  }
  dimension: air_import_france_qatar_dg_leadtime {
    type: number
  }
  dimension: air_import_ITA_qatar_leadtime {
    type: number
  }
  dimension: air_import_ITA_qatar_dg_leadtime {
    type: number
  }
  dimension: air_import_HK_qatar_leadtime {
    type: number
  }
  dimension: air_import_HK_qatar_dg_leadtime {
    type: number
  }
  dimension: land_export_uae_qatar_dg_leadtime {
    type: number
  }
  dimension: land_export_uae_qatar_courier_leadtime {
    type: number
  }
  dimension: land_export_uae_qatar_courier_dg_leadtime {
    type: number
  }
  dimension: land_export_uae_qatar_leadtime {
    type: number
  }
  dimension: air_export_uae_qatar_dg_leadtime {
    type: number
  }
  dimension: air_export_uae_qatar_courier_leadtime {
    type: number
  }
  dimension: air_export_uae_qatar_courier_dg_leadtime {
    type: number
  }
  dimension: air_export_uae_qatar_leadtime {
    type: number
  }
  dimension: tp95_ocean_import_US_qatar{
    type: string
  }
  dimension: tp95_ocean_import_ITA_qatar{
    type: string
  }
  dimension: tp95_ocean_import_HK_qatar{
    type: string
  }
  dimension: tp95_ocean_import_france_qatar{
    type: string
  }
  dimension: tp95_ocean_export_UAE_qatar{
    type: string
  }
  dimension: tp95_air_import_US_qatar_dg{
    type: string
  }
  dimension: tp95_air_import_US_qatar{
    type: string
  }
  dimension: tp95_air_import_france_qatar{
    type: string
  }
  dimension: tp95_air_import_france_qatar_dg{
    type: string
  }
  dimension: tp95_air_import_ITA_qatar{
    type: string
  }
  dimension: tp95_air_import_ITA_qatar_dg{
    type: string
  }
  dimension: tp95_air_import_HK_qatar{
    type: string
  }
  dimension: tp95_air_import_HK_qatar_dg{
    type: string
  }
  dimension: tp95_land_export_uae_qatar_dg{
    type: string
  }
  dimension: tp95_land_export_uae_qatar_courier{
    type: string
  }
  dimension: tp95_land_export_uae_qatar_courier_dg{
    type: string
  }
  dimension: tp95_land_export_uae_qatar{
    type: string
  }
  dimension: tp95_air_export_uae_qatar_dg{
    type: string
  }
  dimension: tp95_air_export_uae_qatar_courier{
    type: string
  }
  dimension: tp95_air_export_uae_qatar_courier_dg{
    type: string
  }
  dimension: tp95_air_export_uae_qatar{
    type: string
  }
  dimension: tp95_air_import_ITA_egypt{type: string}
  dimension: tp95_air_import_france_egypt{type: string}
  dimension: tp95_air_import_france_egypt_dg {type: string}
  dimension: tp95_air_import_HK_egypt{type: string}
  dimension: tp95_ocean_export_UAE_egypt{type: string}
  dimension: tp95_land_export_UAE_egypt{type: string}
  dimension: tp95_air_export_UAE_egypt{type: string}
  dimension: tp95_air_export_UAE_egypt_courier {type: string}
  dimension: air_import_ITA_egypt_leadtime {type:number}
  dimension: air_import_france_egypt_leadtime {type:number}
  dimension: air_import_france_egypt_dg_leadtime {type:number}
  dimension: air_import_HK_egypt_leadtime {type:number}
  dimension: ocean_export_UAE_egypt_leadtime {type:number}
  dimension: land_export_UAE_egypt_leadtime {type:number}
  dimension: air_export_UAE_egypt_leadtime {type:number}
  dimension: air_export_UAE_egypt_courier_leadtime {type:number}
}
# view: tp95_logitude {
#   # Or, you could make this view a derived table, like this:
#   derived_table: {
#     sql: SELECT
#         user_id as user_id
#         , COUNT(*) as lifetime_orders
#         , MAX(orders.created_at) as most_recent_purchase_at
#       FROM orders
#       GROUP BY user_id
#       ;;
#   }
#
#   # Define your dimensions and measures here, like this:
#   dimension: user_id {
#     description: "Unique ID for each user that has ordered"
#     type: number
#     sql: ${TABLE}.user_id ;;
#   }
#
#   dimension: lifetime_orders {
#     description: "The total number of orders for each user"
#     type: number
#     sql: ${TABLE}.lifetime_orders ;;
#   }
#
#   dimension_group: most_recent_purchase {
#     description: "The date when each user last ordered"
#     type: time
#     timeframes: [date, week, month, year]
#     sql: ${TABLE}.most_recent_purchase_at ;;
#   }
#
#   measure: total_lifetime_orders {
#     description: "Use this for counting lifetime orders across many users"
#     type: sum
#     sql: ${lifetime_orders} ;;
#   }
# }
