view: dm_logistics_bu {
  # this view is to get the details at distinct facility and BU as a derived table

  # derived_table: {
  #   explore_source: dim_logistics_bu_mapping {
  #     column: facility_name {}
  #     column: bu_code {}
  #     column: bu_desc_icsb {}
  #   }
  # }

  derived_table: {
    sql_trigger_value: SELECT FLOOR((TIMESTAMP_DIFF(CURRENT_TIMESTAMP(),'1970-01-01 00:00:00',SECOND)) / (12*60*60)) ;;
    sql:
      select  distinct
        managed_by,
        vertical,
        legal_entity,
        bu_code,
        bu_desc_icsb,
        facility_name
      from chb-prod-supplychain-data.prod_warehouse_logistics_supply_chain.dim_logistics_bu_mapping
      group by managed_by,
               vertical,
               legal_entity,
               bu_code,
               bu_desc_icsb,
               facility_name;;
  }

  dimension: pk {
    primary_key: yes
    type: string
    hidden: yes
    sql: concat(${legal_entity},${facility_name},${bu_code});;
  }

  dimension: managed_by {
    label: "Managed By"
    type: string
  }

  dimension: vertical {
    label: "Vertical"
    type: string
  }

  dimension: legal_entity {
    label: "Legal Entity"
    type: string
  }

  dimension: bu_code {
    label: "Bussiness Unit Code"
    type: string
  }
  dimension: bu_desc_icsb {
    label: "Bussiness Unit Desc"
    type: string
  }
  dimension: facility_name {
    label: "Facility Name"
    type: string
  }

}
