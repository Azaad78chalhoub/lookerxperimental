view: dm_wh_ecom_order_forecast_data {
  #sql_table_name: `chb-prod-supplychain-data.prod_warehouse_logistics_supply_chain.dm_wh_ecom_order_forecast_data`
  sql_table_name: `chb-prod-supplychain-data.prod_warehouse_logistics_supply_chain.dm_wh_ecom_order_forecast_data`
    ;;

  dimension: pk {
    primary_key: yes
    type: string
    hidden: yes
    sql: concat(${schema},${brand},${date_date}) ;;
  }

  dimension: schema {
    type: string
    sql: ${TABLE}.schema ;;
  }

  dimension: fc_name {
    type: string
    sql: ${TABLE}.facility ;;
  }

  dimension_group: date {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.date ;;
  }

  dimension: brand {
    type: string
    sql: ${TABLE}.brand ;;
  }

  dimension: order_count {
    type: number
    #sql: cast(${TABLE}.order_count as int64);;
    sql: ${TABLE}.order_count;;
  }

  dimension: pieces_count {
    type: number
    sql: ${TABLE}.pieces_count;;
  }

  measure: count {
    hidden: yes
    type: count
    drill_fields: [fc_name]
  }

  measure: forecast_order_count {
    type: sum
    sql: ${order_count} ;;
  }

  measure: forecast_pieces_count {
    type: sum
    sql: ${pieces_count};;
  }

  measure: p_forecast {
    label: "Forecasted Orders"
    group_label: "WH Capacity"
    type: sum
    sql:
    case
      when ${date_master.date_date} = cast(${dm_wh_ecom_order_forecast_data.date_raw} as date) then ${order_count} else 0
    end ;;
  }

  measure: p_forecast_unit {
    label: "Forecasted Units"
    group_label: "WH Capacity"
    type: sum
    sql:
    case
      when ${date_master.date_date} = cast(${dm_wh_ecom_order_forecast_data.date_raw} as date) then ${pieces_count} else 0
    end ;;
  }

}
