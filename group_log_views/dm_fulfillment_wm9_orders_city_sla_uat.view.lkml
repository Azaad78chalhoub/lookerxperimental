view: dm_fulfillment_wm9_orders_city_sla_uat {
  sql_table_name: `chb-prod-supplychain-data.prod_warehouse_logistics_supply_chain.dm_fulfillment_wm9_orders_city_sla_uat`
    ;;

  dimension: _4_hour {
    type: number
    sql: ${TABLE}._4_hour ;;
  }

  dimension: clean_sla_city {
    type: string
    sql: ${TABLE}.clean_sla_city ;;
  }

  dimension: city_from_sla_matrix {
    type: string
    label: "Is city in SLA matrix?"
    sql: CASE WHEN ${clean_sla_city} ="rest of cities" THEN "using fallback SLA" WHEN ${clean_sla_city} !="rest of cities" AND ${clean_sla_city} IS NOT NULL THEN "city coming from SLA Matrix"  ELSE "missing SLA" END;;
  }

  dimension_group: creation {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.creation_date ;;
  }

  dimension: d_0 {
    type: number
    sql: ${TABLE}.d_0 ;;
  }

  dimension: d_1 {
    type: number
    sql: ${TABLE}.d_1 ;;
  }

  dimension: d_2 {
    type: number
    sql: ${TABLE}.d_2 ;;
  }

  dimension: d_3 {
    type: number
    sql: ${TABLE}.d_3 ;;
  }

  dimension: d_4 {
    type: number
    sql: ${TABLE}.d_4 ;;
  }

  dimension: delivery_type {
    type: string
    sql: ${TABLE}.deliveryType ;;
  }

  dimension: dropoff_city {
    type: string
    sql: ${TABLE}.dropoff_city ;;
  }

  dimension: dropoff_country {
    type: string
    sql: ${TABLE}.dropoff_country ;;
  }

  dimension: partner_order_reference {
    type: string
    sql: ${TABLE}.partnerOrderReference ;;
  }

  dimension: partner_shipment_reference {
    type: string
    sql: ${TABLE}.partnerShipmentReference ;;
  }

  dimension: pickup_city {
    type: string
    sql: ${TABLE}.pickup_city ;;
  }

  dimension: pickup_country {
    type: string
    sql: ${TABLE}.pickup_country ;;
  }

  dimension: possible_misspell {
    type: string
    sql: ${TABLE}.possible_misspell ;;
  }

  dimension: sla_city {
    type: string
    sql: ${TABLE}.sla_city ;;
  }

  dimension: sla_city_accuracy {
    type: number
    label: "SLA City Accuracy"
    sql: CAST(${TABLE}.sla_city_accuracy AS INT64) ;;
  }

  dimension: sla_rule {
    type: string
    sql: ${TABLE}.sla_rule ;;
  }

  dimension: tracking_no {
    type: string
    sql: ${TABLE}.tracking_no ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
