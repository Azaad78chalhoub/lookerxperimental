view: dm_fulfillment_wm9_orders_with_sla {
  sql_table_name: `chb-prod-supplychain-data.prod_warehouse_logistics_supply_chain.dm_fulfillment_wm9_orders_with_sla_uat`
    ;;

  dimension: awb {
    type: string
    sql: ${TABLE}.awb ;;
    hidden: yes
  }

  dimension: carrier_code {
    type: string
    sql: ${TABLE}.carrier_code ;; hidden: yes
  }

  dimension: carrier_name {
    type: string
    sql: ${TABLE}.carrier_name ;; hidden: yes
  }

  dimension: codelkup_otype_description {
    type: string
    sql: ${TABLE}.codelkup_otype_description ;; hidden: yes
  }

  dimension: codelkup_priority_description {
    type: string
    sql: ${TABLE}.codelkup_priority_description ;; hidden: yes
  }

  dimension: codelkup_type_description {
    type: string
    sql: ${TABLE}.codelkup_type_description ;; hidden: yes
  }

  dimension: country {
    type: string
    map_layer_name: countries
    sql: ${TABLE}.country ;; hidden: yes
  }

  dimension: facility_name {
    type: string
    sql: ${TABLE}.facility_name ;; hidden: yes
  }

  dimension: order_c_city {
    type: string
    sql: ${TABLE}.order_c_city ;;hidden: yes
  }

  dimension: order_consigneekey {
    type: string
    sql: ${TABLE}.order_consigneekey ;;hidden: yes
  }

  dimension_group: order_creation {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.order_creation_date ;;hidden: yes
  }

  dimension: order_extern_orderkey {
    type: string
    sql: ${TABLE}.order_extern_orderkey ;;hidden: yes
  }

  dimension: order_id {
    type: string
    sql: ${TABLE}.order_id ;;hidden: yes
  }

  dimension: order_ohtype {
    type: string
    sql: ${TABLE}.order_ohtype ;;hidden: yes
  }

  dimension: order_priority {
    type: string
    sql: ${TABLE}.order_priority ;;hidden: yes
  }

  dimension_group: order_requested_ship {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.order_requested_ship_date ;;hidden: yes
  }

  dimension: order_status_code {
    type: string
    sql: ${TABLE}.order_status_code ;;hidden: yes
  }

  dimension: order_status_description {
    type: string
    sql: ${TABLE}.order_status_description ;;hidden: yes
  }

  dimension: order_storerkey {
    type: string
    sql: ${TABLE}.order_storerkey ;;hidden: yes
  }

  dimension: order_type {
    type: string
    sql: ${TABLE}.order_type ;;hidden: yes
  }

  dimension: order_vas_activities {
    hidden: yes
    sql: ${TABLE}.order_vas_activities ;;
  }

  dimension: sla {
    type: number
    sql: ${TABLE}.sla ;;hidden: yes
  }

  dimension: storer_customer {
    type: string
    sql: ${TABLE}.storer_customer ;;hidden: yes
  }

  dimension: storer_owner {
    type: string
    sql: ${TABLE}.storer_owner ;;hidden: yes
  }

  dimension_group: transmitlog_status_c_mctorderpicked {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.transmitlog_status_c_mctorderpicked_date ;;hidden: yes
  }

  dimension_group: transmitlog_status_c_orderallocationrun {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.transmitlog_status_c_orderallocationrun_date ;;hidden: yes
  }

  dimension_group: transmitlog_status_c_orderpacked {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.transmitlog_status_c_orderpacked_date ;;hidden: yes
  }

  dimension_group: transmitlog_status_c_ordershipped {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.transmitlog_status_c_ordershipped_date ;;hidden: yes
  }

  dimension_group: transmitlog_status_mctorderpicked {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.transmitlog_status_mctorderpicked_date ;;hidden: yes
  }

  dimension_group: transmitlog_status_orderallocationrun {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.transmitlog_status_orderallocationrun_date ;;hidden: yes
  }

  dimension_group: transmitlog_status_ordercancelled {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.transmitlog_status_ordercancelled_date ;;hidden: yes
  }

  dimension_group: transmitlog_status_orderpacked {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.transmitlog_status_orderpacked_date ;;hidden: yes
  }

  dimension_group: transmitlog_status_ordershipped {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.transmitlog_status_ordershipped_date ;;hidden: yes
  }

  dimension_group: transmitlog_status_so {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.transmitlog_status_SO_date ;;hidden: yes
  }

  dimension: vas_applied {
    type: yesno
    sql: ${TABLE}.vas_applied ;;hidden: yes
  }

  dimension: warehouse_id {
    type: string
    sql: ${TABLE}.warehouse_id ;;hidden: yes
  }

  measure: count {
    type: count
    hidden: yes
    drill_fields: [carrier_name, facility_name]
  }
}

# view: dm_fulfillment_wm9_orders_with_sla__order_vas_activities {
#   dimension: box_in_box {
#     type: string
#     sql: ${TABLE}.box_in_box ;;
#   }

#   dimension: giftmessage2 {
#     type: string
#     sql: ${TABLE}.giftmessage2 ;;
#   }

#   dimension: giftmessage3 {
#     type: string
#     sql: ${TABLE}.giftmessage3 ;;
#   }

#   dimension: giftmessage4 {
#     type: string
#     sql: ${TABLE}.giftmessage4 ;;
#   }

#   dimension: giftwrapstraptype {
#     type: string
#     sql: ${TABLE}.giftwrapstraptype ;;
#   }

#   dimension: gitfmessage {
#     type: string
#     sql: ${TABLE}.gitfmessage ;;
#   }

#   dimension: inkjet_printing {
#     type: string
#     sql: ${TABLE}.inkjet_printing ;;
#   }

#   dimension: laser_print {
#     type: string
#     sql: ${TABLE}.laser_print ;;
#   }

#   dimension: over_wrap {
#     type: string
#     sql: ${TABLE}.over_wrap ;;
#   }

#   dimension: scanning {
#     type: string
#     sql: ${TABLE}.scanning ;;
#   }

#   dimension: shrink_wrap {
#     type: string
#     sql: ${TABLE}.shrink_wrap ;;
#   }

#   dimension: sleeve_cello {
#     type: string
#     sql: ${TABLE}.sleeve_cello ;;
#   }

#   dimension: stitching {
#     type: string
#     sql: ${TABLE}.stitching ;;
#   }

#   dimension: stitching_unlabelling {
#     type: string
#     sql: ${TABLE}.stitching_unlabelling ;;
#   }

#   dimension: strapping {
#     type: string
#     sql: ${TABLE}.strapping ;;
#   }

#   dimension: tagging {
#     type: string
#     sql: ${TABLE}.tagging ;;
#   }
# }
