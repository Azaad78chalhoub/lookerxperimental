include: "../group_log_views/dm_fulfillment_wm9_orders_details.view"

view: dm_fulfillment_wm9_orders_details_b2c {

  extends: [dm_fulfillment_wm9_orders_details]

  measure: p_order_quantity {
    label: "Total Order quantity"
    group_label: "WH Capacity"
    type: sum
    sql:
    case
      when ${date_master.date_date} = cast(${dm_fulfillment_wm9_orders.order_creation_raw} as date) then ${original_qty} else 0
    end ;;
  }

  measure: p_actuals_units_percnt {
    group_label: "WH Capacity"
    label: "Actual Ordered Units Percnt"
    type: number
    value_format: "0.00\%"
    #sql: safe_divide(${dm_fulfillment_wm9_orders_details.p_order_quantity},${dm_wh_ecom_order_forecast_data.p_forecast_unit})*100;;
    sql: safe_divide(${p_order_quantity},${dm_wh_ecom_order_forecast_data.p_forecast_unit})*100;;
  }

  measure: actuals_pieces_percnt {
    group_label: "Order Count"
    label: "Actual Pieces Percnt"
    type: number
    value_format: "0.00\%"
    #sql: safe_divide(${dm_fulfillment_wm9_orders_details.order_order_quantity},${dm_wh_ecom_order_forecast_data.forecast_pieces_count})*100;;
    sql: safe_divide(${order_order_quantity},${dm_wh_ecom_order_forecast_data.forecast_pieces_count})*100;;
  }

  measure: p_quantity_shipped {
    label: "Total Shipped quantity"
    group_label: "WH Capacity"
    type: sum
    sql:
    case
      when ${date_master.date_date} = cast(${dm_fulfillment_wm9_orders.shipped_raw} as date) then ${shipped_qty} else 0
    end ;;
  }

}
