view: dim_logistics_claims {
  sql_table_name: `chb-prod-supplychain-data.prod_warehouse_logistics_supply_chain.dim_logistics_claims`
    ;;

  dimension: pk {
    hidden: yes
    primary_key: yes
    type: string
    sql: concat(${facility},${order_number}) ;;
  }
  dimension: facility {
    label: "Warehouse ID"
    hidden: yes
    type: string
    sql: ${TABLE}.FACILITY ;;
  }

  dimension: order_number {
    label: "Order ID"
    type: number
    sql: ${TABLE}.Order_Number ;;
  }

  dimension_group: claim_received {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.Claim_Received_Date ;;
  }

  dimension: claim_ref {
    type: string
    sql: ${TABLE}.Claim_Ref ;;
  }

  dimension: claim_responsibility {
    type: string
    sql: ${TABLE}.Claim_Responsibility ;;
  }

  dimension: claims_lines {
    type: number
    sql: ${TABLE}.Claims_Lines ;;
  }

  dimension: country {
    type: string
    map_layer_name: countries
    sql: ${TABLE}.COUNTRY ;;
  }

  dimension: customer_complaint {
    type: string
    sql: ${TABLE}.Customer_Complaint ;;
  }

  dimension: damaged__qty {
    type: number
    sql: ${TABLE}.Damaged__Qty ;;
  }

  dimension: details_of_investigation {
    type: string
    sql: ${TABLE}.Details_of_Investigation ;;
  }

  dimension: excess_qty {
    type: number
    sql: ${TABLE}.Excess_Qty ;;
  }

  dimension_group: investigation_completed {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.Investigation_Completed ;;
  }

  dimension: lines_valid {
    type: number
    value_format_name: id
    sql: ${TABLE}.Lines_Valid ;;
  }



  dimension: qty_valid {
    type: number
    value_format_name: id
    sql: ${TABLE}.Qty_Valid ;;
  }

  dimension: short_qty {
    type: number
    sql: ${TABLE}.Short_Qty ;;
  }

  dimension: total_claim_qty {
    type: number
    sql: ${TABLE}.Total_Claim_Qty ;;
  }

  dimension: valid_claims {
    type: number
    sql: ${TABLE}.Valid_Claims ;;
  }

  dimension: working_days_lt {
    type: number
    sql: ${TABLE}.Working_Days_LT ;;
  }

  dimension: wrong_qty {
    type: number
    sql: ${TABLE}.Wrong_Qty ;;
  }

  measure: count_claims {
    label: "Total Claims"
    type: count_distinct
    sql: ${order_number} ;;
  }

  measure: count_valid_claims {
    label: "Total Valid Claims"
    type: count_distinct
    sql: ${order_number} ;;
    filters: [valid_claims: "1"]
  }

  measure: count_invalid_claims {
    label: "Total Invalid Claims"
    type: count_distinct
    sql: ${order_number} ;;
    filters: [valid_claims: "0"]
  }

  measure: count_claims_investigated {
    label: "Total Investigated Claims"
    type: count_distinct
    sql: ${order_number} ;;
    filters: [investigation_completed_date: "not null"]
  }

}
