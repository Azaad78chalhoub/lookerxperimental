view: dm_fulfillment_wm9_orders {
  sql_table_name: `chb-prod-supplychain-data.prod_warehouse_logistics_supply_chain.dm_fulfillment_wm9_orders`
    ;;

  dimension: pk {
    primary_key: yes
    type: string
    hidden: yes
    sql: concat(${warehouse_id},${order_id}) ;;
  }


  dimension: facility_name {
    label: "Facility"
    description: "Warehouse name"
    type: string
    sql:
    case
      when ${TABLE}.facility_name = 'REAL Emirates - DIP' then 'DIP'
      when ${TABLE}.facility_name = 'REAL KSA RIYADH' then 'RYD'
      when ${TABLE}.facility_name = 'CADF' then 'CADF'
      when ${TABLE}.facility_name = 'REAL KSA JEDDAH' then 'JED'
      when ${TABLE}.facility_name = 'H&C KUWAIT' then 'H&C'
      when ${TABLE}.facility_name = 'ASHRAF BGDC' then 'BAH'
      when ${TABLE}.facility_name = 'MAC' then 'MAC'
      when ${TABLE}.facility_name = 'QLC-Qatar' then 'QLC'
      when ${TABLE}.facility_name = 'Real FZE JAFZA' then 'JAFZA'
      --when ${TABLE}.facility_name = 'AM MALKI' then 'AL MALKI'
    end ;;
  }

  dimension_group: order_creation {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql:cast(${TABLE}.order_creation_date as TIMESTAMP);;
    # sql:
    # case
    #   when ${TABLE}.facility_name = 'MAC' then
    #     cast(timestamp_add(${TABLE}.order_creation_date_gmt, Interval 4 HOUR) as TIMESTAMP)
    #   else cast(${TABLE}.order_creation_date as TIMESTAMP)
    # end;;
  }

  dimension_group: edit_date {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: cast(${TABLE}.order_edit_date as TIMESTAMP);;
  }

  dimension_group: ready_to_ship {
    label: "Ready To Ship Date"
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: cast(${TABLE}.ready_to_ship_date as TIMESTAMP);;
  }

  dimension: order_id {
    type: string
    sql: ${TABLE}.order_id ;;
  }

  dimension: order_status_code {
    hidden: yes
    type: string
    sql: ${TABLE}.order_status_code ;;
  }

  dimension: order_status_description {
    label: "Order Status"
    type: string
    sql: ${TABLE}.order_status_description ;;
  }

  dimension: iso_country_code {
    hidden: yes
    type: string
    sql: ${TABLE}.iso_country_code ;;
  }

  dimension: iso_country_code_desc {
    hidden: no
    type: string
    sql: ${TABLE}.iso_country_code_description ;;
  }

  dimension_group: picked {
    label: "Pick Complete"
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.picked_date AS TIMESTAMP) ;;
  }

  dimension_group: packed {
    label: "Pack Complete"
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.packed_date AS TIMESTAMP) ;;
  }

  dimension_group: shipped {
    label: "Ship Complete"
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.shipped_date AS TIMESTAMP);;
  }

  dimension: warehouse_id {
    hidden: no
    type: string
    sql: ${TABLE}.warehouse_id ;;
  }

  dimension: storerkey {
    label: "Storer Key"
    hidden: no
    type: string
    sql: ${TABLE}.order_storerkey ;;
  }

  dimension: owner {
    label: "Storer"
    hidden:  no
    description: "Goods in the warehouse owned by Storer"
    type: string
    sql: ${TABLE}.storer_owner ;;
  }

  dimension: consigneekey {
    hidden: yes
    type: string
    sql: ${TABLE}.order_consigneekey ;;
  }

  dimension: customer {
    label: "Consignee"
    description: "Can be a Customer, Supplier or Carrier"
    type: string
    sql: ${TABLE}.storer_customer ;;
  }

  dimension: carrier_name {
    hidden: no
    type: string
    sql: upper(${TABLE}.carrier_name) ;;
  }

  dimension: carrier_code {
    hidden: no
    type: string
    sql:
    case
    when ${TABLE}.carrier_code  ='REAL-LDD' then 'FAREYE'
    when ${TABLE}.carrier_code  ='RLM' then 'FAREYE'
    when ${TABLE}.carrier_code  ='REAL-LMD' then 'FAREYE'
    when ${TABLE}.carrier_code  ='REAL-LMD UAE' then 'FAREYE'
    else upper(${TABLE}.carrier_code)
    end ;;
  }

  dimension: order_type {
    hidden: yes
    type: string
    sql: ${TABLE}.order_type ;;
  }

  dimension: codelkup_type_description {
    label: "Order Type"
    description: "Order type like Normal, E-Commerce, Kitting etc"
    type: string
    sql: ${TABLE}.codelkup_type_description ;;
  }

  dimension: order_ohtype {
    label: "Order Handing Code"
    hidden: no
    type: string
    sql: ${TABLE}.order_ohtype ;;
  }

  dimension: codelkup_otype_description {
    label: "Order Handling Type"
    description: "Order handling type like Distribution, Customer Order, T-Retail, RTV etc"
    type: string
    sql: ${TABLE}.codelkup_otype_description ;;
  }

  dimension: country {
    label: "Country"
    type: string
    sql: ${TABLE}.country ;;
  }

  dimension: country_access_filter {
    label: "Country Access Filter"
    hidden: yes
    type: string
    sql:
    case
    when ${country} = 'UAE' then ${country}
    when ${country} = 'KSA' then ${country}
    when ${country} = 'KUWAIT' then 'KWT'
    when ${country} = 'QATAR' then 'QAT'
    when ${country} = 'BAHRAIN' then 'BAH'
    when ${country} = 'EGYPT' then 'EGY'
    end ;;
  }

  dimension: order_c_city {
    label: "City"
    type: string
    sql: ${TABLE}.order_c_city ;;
  }

  dimension: city_p1_cleaned {
    label: "Cleaned City P1"
    type: string
    sql:
    case
    when ${facility_name} = 'DIP' and ${priority} ='1' then
          case when  ${order_c_city} in ('ABU DHABI','ABU DHABI ','ABU-DHABI','Abu dhabi ')  then 'ABUDHABI'
               when  ${order_c_city} in ('AL GARHOUD ','AL SAFA 2 DUBAI ','DUBAI','DUBAI ','DXB','Dubai ','JUMERIAH')  then 'DUBAI'
               when  ${order_c_city} in ('SHARJAH','SHARJAH ','Sharjah')  then 'SHARJAH'
               when  ${order_c_city} in ('AJMAN','AJMAN ')  then 'AJMAN'
               else 'DEFAULT'
          end
    when ${facility_name} = 'JED' and ${priority} ='1' then
           case when ${order_c_city} in ('JEDDAH') then 'JEDDAH'
                else 'DEFAULT'
           end
    when ${facility_name} = 'RYD' and ${priority} ='1' then
           case when ${order_c_city} in ('RIYADH') then 'RIYADH'
                else 'DEFAULT'
           end
    when ${facility_name} = 'QLC' and ${priority} ='1' then
           case when ${order_c_city} in ('DOHA') then 'DOHA'
                else 'DEFAULT'
           end
    when ${facility_name} = 'BAH' and ${priority} ='1'
          then 'DEFAULT'
    when ${facility_name} = 'MAC' and ${priority} ='1'
          then 'DEFAULT'
    when ${facility_name} = 'H&C' and ${priority} ='1' then
         case when upper(${order_c_city}) not in ('ABDALLY','ALI SUBAH ALI-SALEM','KHIRAN','SABAH AL-AHMAD','WAFRA')
          then 'DEFAULT'
         end
    end ;;
  }

  dimension: priority {
    hidden: yes
    type: string
    sql: ${TABLE}.order_priority ;;
  }

  dimension: priority_desc {
    label: "Priority"
    description: "Priority of the order"
    type: string
    sql: ${TABLE}.codelkup_priority_description ;;
  }

  dimension_group: order_requested_ship_date {
    label: "Requested Ship Date"
    description: "Date on which the order should be dispatched from the Warehouse"
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.order_requested_ship_date AS TIMESTAMP) ;;
  }

  dimension: order_extern_orderkey {
    label: "External Order Key"
    hidden: no
    description: "Key in WM9 to identify the orders from E-commerce"
    type: string
    sql: ${TABLE}.order_extern_orderkey ;;
  }

  dimension: vas_applied {
    label: "VAS Activity Applied"
    description: "Key to identify any VAS activities applied on the orders or not"
    type: yesno
    sql: ${TABLE}.vas_applied ;;
  }

  dimension: gitfmessage {
    hidden: no
    type: string
    sql: ${TABLE}.order_vas_activities.gitfmessage ;;
  }

  dimension: gitfmessage2 {
    hidden: yes
    type: string
    sql: ${TABLE}.order_vas_activities.giftmessage2 ;;
  }

  dimension: giftmessage3 {
    hidden: yes
    type: string
    sql: ${TABLE}.order_vas_activities.giftmessage3 ;;
  }

  dimension: giftmessage4 {
    hidden: yes
    type: string
    sql: ${TABLE}.order_vas_activities.giftmessage4 ;;
  }

  dimension: giftwrapstraptype {
    hidden: no
    type: string
    sql: ${TABLE}.order_vas_activities.giftwrapstraptype ;;
  }

  dimension: vas_reason {
    label: "VAS Activity"
    description: "Different VAS activities applied on the order"
    type: string
    sql: trim(concat(ifnull(${gitfmessage},''),",",ifnull(${gitfmessage2},''),",",ifnull(${giftmessage3},''),",",ifnull(${giftmessage4},''),",",ifnull(${giftwrapstraptype},'')),",") ;;
  }

  dimension: external_factor {
    label: "External Factor"
    description: "External Factors applied on the order"
    type: string
    sql: trim(substr(${TABLE}.external_factor,1,3)) ;;
  }

  dimension: fc_controllable {
    label: "FC Controllable"
    description: "External Factors applied on the order"
    type: string
    sql: case
           when ${external_factor} in ("HVL","HBD","WFD","BRI","BCI","EDI","SRI","ACI","ADI","IDM","OOS","AWB","UAR")
           then "No"
           when ${external_factor} in ("OFD","OTP","OFS","OPA","PRB","BDO","APS","MIS")
           then "Yes"
           else "Yes"
          end ;;
  }

  dimension: awb {
    label: "AirWay Bill"
    hidden: yes
    description: "Airway bill number to track the orders from DSP's"
    type: string
    sql: ${TABLE}.awb ;;
  }

  dimension: pick_count {
    hidden: yes
    label: "Pick Line Count"
    description: "Number of picks done on the order"
    type: number
    sql: ${TABLE}.pick_count ;;
  }

  dimension: order_once_in_error_status {
    label: "Carriyo Error"
    hidden: no
    description: "Order got errored in carriyo at any time during the order life cycle"
    type: yesno
    sql: ${TABLE}.order_once_in_error_status ;;
  }

  dimension: transportation_mode {
    label: "Transportation Mode"
    hidden: yes
    type: string
    sql: ${TABLE}.transportation_mode ;;
  }

  dimension: b2b_vas_applied {
    label: "B2B Vas Activity"
    hidden: yes
    description: "Key to identify any VAS activities applied on the orders or not"
    type: yesno
    sql: ${TABLE}.b2b_vas_applied ;;
  }

}
