view: oms_x_carriyo {
  sql_table_name: `chb-prod-stage-oracle.prod_logistics.oms_x_carriyo`
    ;;


  dimension: bu_brand_for_access_filter {
    hidden: yes
    sql:  case when ${brand_vertical_mapping_log.bu_brand} is null then 'x' else ${brand_vertical_mapping_log.bu_brand} end ;;
  }

  dimension: vertical_for_access_filter {
    hidden: yes
    sql:  case when ${brand_vertical_mapping_log.vertical} is null then 'x' else ${brand_vertical_mapping_log.vertical} end ;;
  }

  dimension: dropoff_country {
    type: string
    sql: ${TABLE}.dropoff_country ;;
  }

  dimension: split_ship_bool {
    label: "Is Split Shipment (Yes/No)"
    type: string
    sql: ${TABLE}.split_ship_bool ;;
  }

  dimension: ecom_pickup_city {
    type: string
    sql: ${TABLE}.ecom_pickup_city ;;
  }

  dimension: ecom_dropoff_city {
    type: string
    sql: ${TABLE}.ecom_dropoff_city ;;
  }

  dimension: ecom_pickup_region {
    type: string
    sql: ${TABLE}.ecom_dropoff_region ;;
    map_layer_name: saudi_map_layer
  }

  dimension: entityType {
    label: "Entity Type"
    type: string
    sql: ${TABLE}.entityType ;;
  }

  dimension: ecom_dropoff_region {
    type: string
    sql: ${TABLE}.ecom_dropoff_region ;;
    map_layer_name: saudi_map_layer
  }

  dimension: ca_delivered_within_SLA_int {
    type: string
    label: "Delivered within SLA INT"
    description: "Boolean denoting whether the first attempt SLA has passed (based on integer)"
    group_label: "SLA"
    sql:  CASE
                WHEN CAST(${TABLE}.ca_delivery_within_SLA_int AS STRING)='true' THEN 'Yes'
                WHEN CAST(${TABLE}.ca_delivery_within_SLA_int AS STRING)='false' THEN 'No'
              ELSE NULL
            END;;
  }

  dimension: ca_delivered_within_SLA_circadian {
    type: string
    label: "Delivered within SLA Circadian"
    description: "Boolean denoting whether the first attempt SLA has passed (based on circadian)"
    group_label: "SLA"
    sql: CASE
                WHEN CAST(${TABLE}.ca_delivery_within_SLA_circadian AS STRING)='true' THEN 'Yes'
                WHEN CAST(${TABLE}.ca_delivery_within_SLA_circadian AS STRING)='false' THEN 'No'
              ELSE NULL
            END;;
  }

  dimension: additional_day_express_sla {
    type: number
    hidden: yes
    sql: ${TABLE}.additional_day_express_SLA ;;
  }

  dimension: ca_first_attempt_within_sla_circadian {
    type: string
    hidden: yes
    sql: ${TABLE}.ca_first_attempt_within_SLA_circadian ;;
  }

  dimension: ca_first_attempt_within_sla_int {
    type: string
    hidden: yes
    sql: ${TABLE}.ca_first_attempt_within_SLA_int ;;
  }

  dimension: ca_second_attempt_within_sla_circadian {
    type: string
    hidden: yes
    sql: ${TABLE}.ca_second_attempt_within_SLA_circadian ;;
  }

  dimension: ca_second_attempt_within_sla_int {
    type: string
    hidden: yes
    sql: ${TABLE}.ca_second_attempt_within_SLA_int ;;
  }

  dimension_group: ca_sla_first_attempt {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      second,
      minute,
      hour,
      time_of_day,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.ca_SLA_first_attempt AS TIMESTAMP) ;;
  }

  dimension_group: ca_sla_second_attempt {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      second,
      minute,
      hour,
      time_of_day,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.ca_SLA_second_attempt AS TIMESTAMP) ;;
  }

  dimension_group: ca_sla_third_attempt {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      second,
      minute,
      hour,
      time_of_day,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.ca_SLA_third_attempt AS TIMESTAMP) ;;
  }

  dimension: ca_third_attempt_within_sla_circadian {
    type: string
    hidden: yes
    sql: ${TABLE}.ca_third_attempt_within_SLA_circadian ;;
  }

  dimension: ca_third_attempt_within_sla_int {
    type: string
    hidden: yes
    sql: ${TABLE}.ca_third_attempt_within_SLA_int ;;
  }

  dimension: cancellation_reason {
    type: string
    sql: ${TABLE}.cancellation_reason ;;
  }

  dimension: carrier {
    type: string
    sql: ${TABLE}.carrier ;;
  }

  dimension: carrier_status {
    type: string
    sql: ${TABLE}.carrier_status ;;
  }

  dimension_group: carriyo_creation {
    type: time
    timeframes: [
      raw,
      time,
      second,
      minute,
      hour,
      time_of_day,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.carriyo_creation_date AS TIMESTAMP) ;;
  }

  dimension: carriyo_status {
    type: string
    sql: ${TABLE}.carriyo_status ;;
  }

  dimension: chb_status {
    type: string
    hidden: no
    sql: ${TABLE}.chb_status ;;
  }

  dimension_group: cu_collected {
    type: time
    timeframes: [
      raw,
      time,
      second,
      minute,
      hour,
      time_of_day,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.store_pickup AS TIMESTAMP) ;;
  }

  dimension_group: cu_confirmed {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      second,
      minute,
      hour,
      time_of_day,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.cu_confirmed_date AS TIMESTAMP) ;;
  }

  dimension_group: cu_delivered {
    type: time
    timeframes: [
      raw,
      time,
      second,
      minute,
      hour,
      time_of_day,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.cu_delivered_date AS TIMESTAMP) ;;
  }

  dimension_group: cu_out_for_delivery {
    type: time
    timeframes: [
      raw,
      time,
      second,
      minute,
      hour,
      time_of_day,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.cu_out_for_delivery_at AS TIMESTAMP) ;;
  }

  dimension_group: cu_collected_at_alt {
    type: time
    timeframes: [
      raw,
      time,
      second,
      minute,
      hour,
      time_of_day,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.cu_collected_at AS TIMESTAMP) ;;
  }

  dimension_group: cu_first_attempt {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      second,
      minute,
      hour,
      time_of_day,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.cu_first_attempt AS TIMESTAMP) ;;
  }

  dimension_group: cu_promised_delivery {
    type: time
    timeframes: [
      raw,
      time,
      second,
      minute,
      hour,
      time_of_day,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.cu_promised_delivery_date AS TIMESTAMP) ;;
  }

  dimension_group: cu_returned {
    type: time
    timeframes: [
      raw,
      time,
      second,
      minute,
      hour,
      time_of_day,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.cu_returned_date AS TIMESTAMP) ;;
  }

  dimension_group: cu_second_attempt {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      second,
      minute,
      hour,
      time_of_day,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.cu_second_attempt AS TIMESTAMP) ;;
  }

  dimension_group: cu_third_attempt {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      second,
      minute,
      hour,
      time_of_day,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.cu_third_attempt AS TIMESTAMP) ;;
  }

  dimension_group: cu_update {
    type: time
    timeframes: [
      raw,
      time,
      second,
      minute,
      hour,
      time_of_day,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.cu_update_date AS TIMESTAMP) ;;
  }

  dimension: customer_email {
    type: string
    sql: ${TABLE}.customer_email ;;
  }

  dimension: customer_phonenumber {
    type: string
    sql: ${TABLE}.customer_phonenumber ;;
  }

  dimension_group: dsp_creation {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      second,
      minute,
      hour,
      time_of_day,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.dsp_creation_date AS TIMESTAMP) ;;
  }

  dimension: fault_with {
    type: string
    hidden: yes
    sql: ${TABLE}.fault_with ;;
  }

  dimension_group: first_return {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      second,
      minute,
      hour,
      time_of_day,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.first_return AS TIMESTAMP) ;;
  }

  dimension: item_barcode {
    type: string
    sql: ${TABLE}.item_barcode ;;
  }

  dimension: item_brand {
    type: string
    sql: ${TABLE}.item_brand ;;
  }

  dimension: item_discount {
    type: number
    sql: ${TABLE}.item_discount ;;
  }

  dimension: item_name {
    type: string
    sql: ${TABLE}.item_name ;;
  }

  dimension: item_price {
    type: number
    sql: ${TABLE}.item_price ;;
  }

  dimension: item_price_incl_tax {
    type: number
    sql: ${TABLE}.item_price_incl_tax ;;
  }

  dimension: item_price_original {
    type: number
    sql: ${TABLE}.item_price_original ;;
  }

  dimension: item_sku {
    type: string
    sql: ${TABLE}.item_sku ;;
  }

  dimension: item_tax {
    type: number
    sql: ${TABLE}.item_tax ;;
  }

  dimension: item_tax_amount {
    type: number
    sql: ${TABLE}.item_tax_amount ;;
  }

  dimension: latest_status {
    type: string
    sql: ${TABLE}.latest_status ;;
  }

  dimension: latest_status_boolean {
    label: "Filter by latest status"
    type: yesno
    sql: ${latest_status}=1 ;;
  }

  dimension: latest_undelivered_reason {
    type: string
    sql: ${TABLE}.latest_undelivered_reason ;;
  }

  dimension: merchant {
    type: string
    sql: ${TABLE}.merchant ;;
  }

  dimension: no_of_attempts {
    hidden: yes
    type: number
    sql: ${TABLE}.no_of_attempts ;;
  }

  dimension: ocd_first_attempt_within_sla_circadian {
    type: string
    hidden: yes
    sql: ${TABLE}.ocd_first_attempt_within_SLA_circadian ;;
  }

  dimension: ocd_first_attempt_within_sla_int {
    type: string
    hidden: yes
    sql: ${TABLE}.ocd_first_attempt_within_SLA_int ;;
  }

  dimension: ocd_second_attempt_within_sla_circadian {
    type: string
    hidden: yes
    sql: ${TABLE}.ocd_second_attempt_within_SLA_circadian ;;
  }

  dimension: ocd_second_attempt_within_sla_int {
    type: string
    hidden: yes
    sql: ${TABLE}.ocd_second_attempt_within_SLA_int ;;
  }

  dimension_group: ocd_sla_first_attempt {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      second,
      minute,
      hour,
      time_of_day,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.ocd_SLA_first_attempt AS TIMESTAMP) ;;
  }

  dimension_group: ocd_sla_second_attempt {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      second,
      minute,
      hour,
      time_of_day,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.ocd_SLA_second_attempt AS TIMESTAMP) ;;
  }

  dimension_group: ocd_sla_third_attempt {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      second,
      minute,
      hour,
      time_of_day,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.ocd_SLA_third_attempt AS TIMESTAMP) ;;
  }

  dimension: ocd_third_attempt_within_sla_circadian {
    type: string
    hidden: yes
    sql: ${TABLE}.ocd_third_attempt_within_SLA_circadian ;;
  }

  dimension: ocd_third_attempt_within_sla_int {
    type: string
    hidden: yes
    sql: ${TABLE}.ocd_third_attempt_within_SLA_int ;;
  }

  dimension_group: oms_creation {
    description: "Order creation time based on fulfilment location local time"
    type: time
    timeframes: [
      raw,
      time,
      second,
      minute,
      hour,
      time_of_day,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql:  case when ${order_id} LIKE "%APPENKSA%"
          or ${order_id} LIKE "%APPENKWT%"
          or ${order_id} LIKE "%APPENBHR%"
          or ${order_id} LIKE "%APPENOMN%"
          or ${order_id} LIKE "%APPORKSA%"
          or ${order_id} LIKE "%APPORKWT%"
          or ${order_id} LIKE "%APPORBHR%"
          or ${order_id} LIKE "%APPOROMN%"
          or ${order_id} LIKE "%ORKSA%"
          or ${order_id} LIKE "%ORKWT%"
          or ${order_id} LIKE "%ORBHR%"
          or ${order_id} LIKE "%TRSA%"
          or ${order_id} LIKE "%TRKW%" and ${fulfilment_location_id} = "11001"
          then timestamp_add(CAST(${TABLE}.oms_creation_date AS TIMESTAMP), interval 1 hour)

          else CAST(${TABLE}.oms_creation_date AS TIMESTAMP) end ;;
  }

  dimension_group: oms_creation_webshop_time {
    type: time
    description: "Order creation time based on webshop local time"
    timeframes: [
      raw,
      time,
      second,
      minute,
      hour,
      time_of_day,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql:CAST(${TABLE}.oms_creation_date AS TIMESTAMP) ;;
  }

  dimension_group: oms_packed {
    type: time
    timeframes: [
      raw,
      time,
      second,
      minute,
      hour,
      time_of_day,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: case when ${order_id} LIKE "%APPENKSA%"
              or ${order_id} LIKE "%APPENKWT%"
              or ${order_id} LIKE "%APPENBHR%"
              or ${order_id} LIKE "%APPENOMN%"
              or ${order_id} LIKE "%APPORKSA%"
              or ${order_id} LIKE "%APPORKWT%"
              or ${order_id} LIKE "%APPOROMN%"
              or ${order_id} LIKE "%APPORBHR%"
              or ${order_id} LIKE "%ORKSA%"
              or ${order_id} LIKE "%ORKWT%"
              or ${order_id} LIKE "%ORBHR%"
              or ${order_id} LIKE "%TRSA%"
              or ${order_id} LIKE "%TRKW%" and ${fulfilment_location_id} = "11001"
          then timestamp_add(CAST(${TABLE}.oms_packed_date AS TIMESTAMP), interval 1 hour)

          else CAST(${TABLE}.oms_packed_date AS TIMESTAMP) end;;
  }


  dimension: sales_origin_country {
    type: string
    sql: case when ${sales_origin_name} LIKE "%(AE)%"
              or ${sales_origin_name} LIKE "%UAE%" then "AE"
              when ${sales_origin_name} LIKE "%(SA)%"
              or ${sales_origin_name} LIKE "%KSA%" then "SA"
              when ${sales_origin_name} LIKE "%Qatar%"
              or ${sales_origin_name} LIKE "%(QA)%" then "QA"
              when ${sales_origin_name} LIKE "%(KW)%"
              or ${sales_origin_name} LIKE "%Kuwait%" then "KW"
              when ${sales_origin_name} LIKE "%Bahrain%" then "BH"
              when ${sales_origin_name} LIKE "%EG%" then "EG"
              else null end
              ;;
  }

  dimension_group: time_now {
    hidden: no
    type: time
    timeframes: [
      raw,
      time,
      second,
      minute,
      hour,
      time_of_day,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: case when ${sales_origin_country} = "AE" then DATETIME(current_timestamp(), "Asia/Dubai")
              when ${sales_origin_country} = "SA" then DATETIME(current_timestamp(), "Asia/Riyadh")
              when ${sales_origin_country} = "KW" then DATETIME(current_timestamp(), "Asia/Kuwait")
              when ${sales_origin_country} = "BH" then DATETIME(current_timestamp(), "Asia/Bahrain")
              when ${sales_origin_country} = "QA" then DATETIME(current_timestamp(), "Asia/Qatar")
              when ${sales_origin_country} = "EG" then DATETIME(current_timestamp(), "Egypt")
          else null
          end
              ;;
  }

  dimension_group: oms_claimed {
    type: time
    timeframes: [
      raw,
      time,
      second,
      minute,
      hour,
      time_of_day,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.oms_claimed_date AS TIMESTAMP) ;;
  }

  dimension_group: oms_picked {
    type: time
    timeframes: [
      raw,
      time,
      second,
      minute,
      hour,
      time_of_day,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: case when ${order_id} LIKE "%APPENKSA%"
              or ${order_id} LIKE "%APPENKWT%"
              or ${order_id} LIKE "%APPENBHR%"
              or ${order_id} LIKE "%APPENOMN%"
              or ${order_id} LIKE "%APPORKSA%"
              or ${order_id} LIKE "%APPORKWT%"
              or ${order_id} LIKE "%APPOROMN%"
              or ${order_id} LIKE "%APPORNHR%"
              or ${order_id} LIKE "%ORKSA%"
              or ${order_id} LIKE "%ORKWT%"
              or ${order_id} LIKE "%ORBHR%"
              or ${order_id} LIKE "%TRSA%"
              or ${order_id} LIKE "%TRKW%" and ${fulfilment_location_id} = "11001"
          then timestamp_add(CAST(${TABLE}.oms_picked_date AS TIMESTAMP), interval 1 hour)
          else CAST(${TABLE}.oms_picked_date AS TIMESTAMP) end ;;
  }

  dimension: oms_shipped_boolean {
    type: number
    hidden: yes
    sql: ${TABLE}.oms_shipped_boolean ;;
  }

  dimension_group: oms_shipped {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      second,
      minute,
      hour,
      time_of_day,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.oms_shipped_date AS TIMESTAMP) ;;
  }

  dimension_group: cu_shipped {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      second,
      minute,
      hour,
      time_of_day,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.cu_shipped_date AS TIMESTAMP) ;;
  }

  dimension: oms_status {
    type: string
    sql: ${TABLE}.oms_status ;;
  }

  dimension_group: oms_update {
    type: time
    timeframes: [
      raw,
      time,
      second,
      minute,
      hour,
      time_of_day,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.oms_update_date AS TIMESTAMP) ;;
  }


  dimension_group: cc_informed_customer {
    type: time
    timeframes: [
      raw,
      time,
      second,
      minute,
      hour,
      time_of_day,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.cc_informed_customer_time AS TIMESTAMP) ;;
  }

  dimension_group: cc_order_received {
    type: time
    timeframes: [
      raw,
      time,
      second,
      minute,
      hour,
      time_of_day,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.cc_order_received_time AS TIMESTAMP) ;;
  }

  dimension_group: cc_customer_collect {
    type: time
    timeframes: [
      raw,
      time,
      second,
      minute,
      hour,
      time_of_day,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.cc_customer_collect_time AS TIMESTAMP) ;;
  }

  dimension: unified_status {
    type: string
    sql: LOWER(COALESCE(REPLACE(${oms_status}, '_', ' '), COALESCE(${chb_status},${carriyo_status}))) ;;
  }

  dimension: fulfilment_employee_id{
    type: string
    sql: ${TABLE}.fulfilment_employee_id ;;
  }

  dimension: fulfilment_location_id {
    type: string
    sql: ${TABLE}.shipping_location_internal_id ;;
  }

  dimension: fulfilment_location_name {
    type: string
    sql: ${TABLE}.shipping_location_name ;;
  }


  dimension: order_currency {
    type: string
    sql: ${TABLE}.order_currency ;;
  }

  dimension: order_custom_duty_fee {
    type: number
    sql: ${TABLE}.order_custom_duty_fee ;;
  }

  dimension: brand_url {
    type: string
    hidden: yes
    sql: case when ${merchant} = 'SWAROVSKI' then 'swa'
          when ${merchant} =  'LEVEL SHOES' then 'lvs'
          when ${merchant} =  'FACES' then 'fcs'
          when ${merchant} =  'TRYANO' then 'trn'
          when ${merchant} =  'THE DEAL' then 'tdl' end;;
  }


  dimension: order_id {
    type: string
    sql: ${TABLE}.order_id ;;
    link: {
      label: "View Order in OMS"
      url: "https://{{oms_x_carriyo.brand_url._value}}.retailunity.com/cpanel/cpanel_includes/inc_datatables/orders/orders_all.php7?action=edit&id={{oms_x_carriyo.retailunity_order_id._value}}"
    }
  }

  dimension: order_payment_fee {
    type: number
    sql: ${TABLE}.order_payment_fee ;;
  }

  dimension: order_shipping {
    type: number
    sql: ${TABLE}.order_shipping ;;
  }

  dimension: order_total {
    type: number
    sql: ${TABLE}.order_total ;;
  }

  dimension: partner_shipment_reference {
    type: string
    sql: ${TABLE}.partner_shipment_reference ;;
  }

  dimension: payment_method {
    type: string
    sql: ${TABLE}.payment_method ;;
  }

  dimension: payment_status {
    type: string
    sql: ${TABLE}.payment_status ;;
  }

  dimension: products_in_order {
    type: number
    sql: ${TABLE}.products_in_order ;;
  }

  dimension: retailunity_order_id {
    type: string
    sql: ${TABLE}.retailunity_order_id ;;
  }

  dimension: sales_origin_id {
    type: number
    sql: ${TABLE}.sales_origin_id ;;
  }

  dimension: sales_origin_internal_id {
    type: string
    sql: ${TABLE}.sales_origin_internal_id ;;
  }


  dimension: sales_origin_name {
    type: string
    sql: ${TABLE}.sales_origin_name ;;
  }

  dimension: sla_city {
    type: string
    hidden: yes
    sql: ${TABLE}.sla_city ;;
  }

  dimension: sla_dropoff_city {
    type: string
    sql: ${TABLE}.sla_dropoff_city ;;
  }

  dimension: sla_pickup_city {
    type: string
    sql: ${TABLE}.sla_pickup_city ;;
  }

  dimension: sla_req {
    type: string
    hidden: yes
    sql: ${TABLE}.SLA_REQ ;;
  }

  dimension_group: sla_returned_by {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      second,
      minute,
      hour,
      time_of_day,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.SLA_returned_by AS TIMESTAMP) ;;
  }

  dimension: sla_rule {
    type: string
    hidden: yes
    sql: ${TABLE}.sla_rule ;;
  }

  dimension: soi_first_attempt_within_sla_circadian {
    type: string
    hidden: yes
    sql: ${TABLE}.soi_first_attempt_within_SLA_circadian ;;
  }

  dimension: soi_first_attempt_within_sla_int {
    type: string
    hidden: yes
    sql: ${TABLE}.soi_first_attempt_within_SLA_int ;;
  }

  dimension: soi_second_attempt_within_sla_circadian {
    type: string
    hidden: yes
    sql: ${TABLE}.soi_second_attempt_within_SLA_circadian ;;
  }

  dimension: soi_second_attempt_within_sla_int {
    type: string
    hidden: yes
    sql: ${TABLE}.soi_second_attempt_within_SLA_int ;;
  }

  dimension_group: soi_sla_first_attempt {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      second,
      minute,
      hour,
      time_of_day,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.soi_SLA_first_attempt AS TIMESTAMP) ;;
  }

  dimension_group: soi_sla_second_attempt {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      second,
      minute,
      hour,
      time_of_day,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.soi_SLA_second_attempt AS TIMESTAMP) ;;
  }

  dimension_group: soi_sla_third_attempt {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      second,
      minute,
      hour,
      time_of_day,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.soi_SLA_third_attempt AS TIMESTAMP) ;;
  }

  dimension: soi_third_attempt_within_sla_circadian {
    type: string
    hidden: yes
    sql: ${TABLE}.soi_third_attempt_within_SLA_circadian ;;
  }

  dimension: soi_third_attempt_within_sla_int {
    type: string
    hidden: yes
    sql: ${TABLE}.soi_third_attempt_within_SLA_int ;;
  }

  dimension: status {
    type: string
    hidden: yes
    sql: ${TABLE}.status ;;
  }

  dimension: tracking_no {
    type: string
    sql: ${TABLE}.tracking_no ;;
    link: {
      label: "See order last mile details"
      url: "https://chalhoubgroup.de.looker.com/looks/3987?&f[carriyo_unified.tracking_no]={{ value | url_encode}}"
    }
  }


  dimension_group: update_date {
    type: time
    timeframes: [
      raw,
      time,
      second,
      minute,
      hour,
      time_of_day,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.update_date AS TIMESTAMP) ;;
  }

  dimension: warehouse_vs_store {
    type: string
    hidden: yes
    sql: ${TABLE}.warehouse_vs_store ;;
  }

  dimension: fulfilment_employee_name {
    type: string
    sql: ${TABLE}.employee_name;;
  }

  dimension: fulfilment_location_city {
    type: string
    sql: ${TABLE}.shipping_location_city ;;
  }

  dimension: fulfilment_location_country {
    type: string
    sql: ${TABLE}.shipping_location_country ;;
  }

  dimension: fulfilment_location_type {
    type: string
    sql: ${TABLE}.shipping_location_type ;;
  }

  dimension_group: adjusted_order_time{
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      second,
      minute,
      hour,
      time_of_day,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: IF(EXTRACT(HOUR from CAST(${TABLE}.oms_creation_date AS TIMESTAMP)) >= 21,
          TIMESTAMP(DATETIME(EXTRACT(YEAR from TIMESTAMP_ADD(CAST(${TABLE}.oms_creation_date AS TIMESTAMP), INTERVAL 1 DAY)),
                  EXTRACT(MONTH from TIMESTAMP_ADD(CAST(${TABLE}.oms_creation_date AS TIMESTAMP), INTERVAL 1 DAY)),
                  EXTRACT(DAY from TIMESTAMP_ADD(CAST(${TABLE}.oms_creation_date AS TIMESTAMP), INTERVAL 1 DAY)),
                  9, 0, 0)),
          IF(EXTRACT(HOUR from CAST(${TABLE}.oms_creation_date AS TIMESTAMP)) < 9,
                  TIMESTAMP(DATETIME(EXTRACT(YEAR from CAST(${TABLE}.oms_creation_date AS TIMESTAMP)),
                          EXTRACT(MONTH from CAST(${TABLE}.oms_creation_date AS TIMESTAMP)),
                          EXTRACT(DAY from CAST(${TABLE}.oms_creation_date AS TIMESTAMP)),
                  9, 0, 0)),
              CAST(${TABLE}.oms_creation_date AS TIMESTAMP)))  ;;
  }

  dimension: delivery_type {
    type: string
    sql: LOWER(COALESCE(${TABLE}.delivery_type, ${TABLE}.order_delivery_type)) ;;
  }

  parameter: timestamp_option {
    hidden: yes
    label: "Fulfilment Shop Time vs Webshop Time"
    type: string
    allowed_value: { value: "Fulfilment Shop Time" }
    allowed_value: { value: "Webshop Time" }
  }

  measure: item_price_sum {
    type: sum_distinct
    label: "Total Value"
    value_format_name: decimal_1
    filters: [chb_status: "Delivered"]
    description: "Total item price excluding taxes"
    sql: ${item_price} ;;
    sql_distinct_key: CONCAT(${TABLE}.order_id, ${TABLE}.carrier, ${TABLE}.tracking_no, ${TABLE}.detail_id) ;;
  }

  measure: total_orders {
    group_label: "Order Count"
    type: count_distinct
    sql: ${order_id} ;;
  }

  measure: total_orders_packed {
    group_label: "Order Count"
    type: count_distinct
    sql: ${order_id} ;;
    filters: [Order_with_Packing_date: "Yes" ]
    label: "Total Orders Packed"
  }

  measure: total_orders_delivered {
    group_label: "Order Count"
    type: count_distinct
    sql: ${order_id} ;;
    filters: [Order_with_Delivered_date: "Yes"]
    label: "Total Orders Delivered"
  }

  measure: total_qty {
    group_label: "Order Quantity"
    type: count_distinct
    sql: ${TABLE}.detail_id ;;
    label: "Total Quantity"
  }

  measure: total_qty_packed {
    group_label: "Order Quantity"
    type: count_distinct
    sql: ${TABLE}.detail_id ;;
    filters: [Order_with_Packing_date: "yes"]
    label: "Total Quantity Packed"
  }

  measure: total_qty_shipped {
    hidden: yes
    group_label: "Order Quantity"
    type: count_distinct
    sql: ${TABLE}.detail_id ;;
    filters: [oms_status: "Shipped"]
    label: "Total Quantity Shipped"
  }

  measure: total_qty_picked {
    group_label: "Order Quantity"
    type: count_distinct
    sql: ${TABLE}.detail_id ;;
    filters: [oms_status: "Picked"]
    label: "Total Quantity Picked"
  }

  measure: total_qty_new {
    hidden: yes
    type: count_distinct
    sql: ${TABLE}.detail_id ;;
    filters: [oms_status: "New"]
    label: "Total Quantity New"
  }

  measure: total_qty_delivered {
    group_label: "Order Quantity"
    type: count_distinct
    sql: ${TABLE}.detail_id ;;
    filters: [oms_status: "Delivered"]
    label: "Total Quantity Delivered"
  }


  measure: number_of_unique_sku_picked{
    group_label: "Order Quantity"
    type: count_distinct
    sql: CONCAT(${TABLE}.item_sku, '|', ${order_id}) ;;
    label: "Total Number of Unique SKU Picked"
  }

  measure: number_of_unique_sku_picked_per_employee{
    group_label: "Order Quantity"
    type: count_distinct
    sql: ${TABLE}.item_sku ;;
  }

  measure: number_of_additional_units_per_sku{
    group_label: "Order Quantity"
    type:  number
    sql:  ${total_qty_picked} - ${number_of_unique_sku_picked} ;;
  }

  dimension: Order_with_Packing_date {
    hidden: no
    type: yesno
    sql: ${oms_packed_date} is NOT NULL ;;
  }

  dimension: Order_with_Delivered_date {
    hidden: no
    type: yesno
    sql: ${cu_delivered_date} is NOT NULL ;;
  }

  measure: click_to_pack  {
    label: "Click to Pack (D:H:MM)"
    description: "Average time taken from order creation to packing"
    hidden: yes
    type: average
    value_format: "D:H:MM"
    sql:DATETIME_DIFF(CAST(${oms_packed_raw} as datetime),CAST(${oms_creation_raw} as datetime), SECOND)/86400 ;;
    filters: [unified_status: "packed"]
  }

  measure: click_to_pack_working_hours  {
    label: "Click to Pack - working hours (D:H:MM)"
    description: "Average time taken from order creation to packing considering working hours"
    hidden: yes
    type: average
    value_format: "D:H:MM"
    sql:  CASE WHEN ${oms_creation_date} = ${oms_packed_date} AND
              EXTRACT(HOUR from CAST(${TABLE}.oms_creation_date AS TIMESTAMP)) >= 21
              THEN DATETIME_DIFF(cast(${oms_packed_raw} as datetime),cast(${oms_creation_raw} as datetime), SECOND)/86400
          ELSE DATETIME_DIFF(cast(${oms_packed_raw} as datetime),cast(${adjusted_order_time_raw} as datetime), SECOND)/86400 END;;
    filters: [Order_with_Packing_date: "Yes"]
  }

  dimension: click_to_pack_order_level  {
    label: "Order Click to Pack (D:H:MM) "
    description: "Average time taken from order creation to packing at order level"
    type: string
    sql:FLOOR(DATETIME_DIFF(CAST(${oms_packed_raw} as datetime),CAST(${oms_creation_raw} as datetime), SECOND)/86400) || ":" ||
        FLOOR(MOD(DATETIME_DIFF(CAST(${oms_packed_raw} as datetime),CAST(${oms_creation_raw} as datetime), SECOND),86400)/3600) || ":" ||
        FLOOR(MOD(DATETIME_DIFF(CAST(${oms_packed_raw} as datetime),CAST(${oms_creation_raw} as datetime), SECOND),3600)/60);;
  }

  dimension: click_to_pack_adjusted_order_level  {
    label: "Order Click to Pack Adjusted (D:H:MM) "
    description: "Average time taken from order creation to packing at order level during work hours"
    type: string
    sql:CASE WHEN ${oms_creation_date} = ${oms_packed_date} AND
          EXTRACT(HOUR from CAST(${TABLE}.oms_creation_date AS TIMESTAMP)) >= 21
        THEN
          FLOOR(DATETIME_DIFF(CAST(${oms_packed_raw} as datetime),CAST(${oms_creation_raw} as datetime), SECOND)/86400) || ":" ||
          FLOOR(MOD(DATETIME_DIFF(CAST(${oms_packed_raw} as datetime),CAST(${oms_creation_raw} as datetime), SECOND),86400)/3600) || ":" ||
          FLOOR(MOD(DATETIME_DIFF(CAST(${oms_packed_raw} as datetime),CAST(${oms_creation_raw} as datetime), SECOND),3600)/60)
        ELSE
          FLOOR(DATETIME_DIFF(CAST(${oms_packed_raw} as datetime),CAST(${adjusted_order_time_raw} as datetime), SECOND)/86400) || ":" ||
          FLOOR(MOD(DATETIME_DIFF(CAST(${oms_packed_raw} as datetime),CAST(${adjusted_order_time_raw} as datetime), SECOND),86400)/3600) || ":" ||
          FLOOR(MOD(DATETIME_DIFF(CAST(${oms_packed_raw} as datetime),CAST(${adjusted_order_time_raw} as datetime), SECOND),3600)/60)
        END ;;
  }

  measure: click_to_deliver  {
    label: "Click to Deliver (D:H:MM)"
    description: "Average time taken from order creation to delivered"
    type: average
    hidden: yes
    value_format: "D:HH:MM"
    sql:DATETIME_DIFF(CAST(${cu_delivered_raw} as datetime),CAST(${oms_creation_raw} as datetime), SECOND)/86400 ;;
    filters: [unified_status: "delivered"]
  }

  measure: click_to_deliver_in_days {
    label: "Click to Deliver (in Days)"
    description: "Average time taken from order creation to delivered"
    type: average
    hidden: yes
    value_format: "#.00;(#.00)"
    sql:DATETIME_DIFF(CAST(${cu_delivered_raw} as datetime),CAST(${oms_creation_raw} as datetime), SECOND)/86400 ;;
    filters: [unified_status: "delivered"]
  }

  dimension: click_to_deliver_in_sec  {
    label: "Click to Deliver (in sec)"
    description: "Average time taken from order creation to delivered"
    type: number
    hidden: yes
    sql:DATETIME_DIFF(CAST(${cu_delivered_raw} as datetime),CAST(${oms_creation_raw} as datetime), SECOND)/86400 ;;
  }

  measure: click_to_deliver_avg  {
    label: "Click to Deliver (avg)"
    description: "Average time taken from order creation to delivered"
    type: average
    hidden: yes
    sql: ${click_to_deliver_in_sec};;
    filters: [unified_status: "delivered"]
  }

  measure: click_to_deliver_sum  {
    label: "Click to Deliver (sum)"
    description: "Average time taken from order creation to delivered"
    type: sum
    hidden: yes
    sql: ${click_to_deliver_in_sec};;
    filters: [unified_status: "delivered"]
  }

  measure: pack_to_deliver  {
    label: "Pack to Deliver (D:H:MM)"
    description: "Average time taken from order packing to delivered"
    hidden: yes
    type: average
    value_format: "D:H:MM"
    sql:DATETIME_DIFF(CAST(${cu_delivered_raw} as datetime),CAST(${oms_packed_raw} as datetime), SECOND)/86400 ;;
    filters: [unified_status: "delivered"]
  }

  measure: pack_to_ship {
    label: "Pack to Ship (D:H:MM)"
    description: "Average time taken from order packing to order shipping"
    hidden: yes
    type: average
    value_format: "D:H:MM"
    sql:DATETIME_DIFF(CAST(${cu_collected_raw} as datetime),CAST(${oms_packed_raw} as datetime), SECOND)/86400 ;;
  }

  measure: dsp_collect_to_ship  {
    label: "DSP Collect to Ship (D:H:MM)"
    description: "Average time taken from package collection by DSP to order shipping"
    hidden: yes
    type: average
    value_format: "D:H:MM"
    sql:DATETIME_DIFF(CAST(${oms_shipped_raw} as datetime),CAST(${cu_collected_raw} as datetime), SECOND)/86400 ;;
  }

  measure: ship_to_delivered  {
    label: "Ship to Deliver (D:H:MM)"
    description: "Average time taken from order shipping to delivered"
    hidden: yes
    type: average
    value_format: "D:H:MM"
    sql:DATETIME_DIFF(CAST(${cu_delivered_raw} as datetime),CAST(${cu_collected_raw} as datetime), SECOND)/86400 ;;
    filters: [unified_status: "delivered"]
  }

  dimension: store_whs_location_type {
    label: "Fulfilment Location (WHS/Store)"
    hidden: no
    type: string
    sql: CASE WHEN ${fulfilment_location_name} LIKE '%Warehouse%'
          OR ${fulfilment_location_name} LIKE '%warehouse%'
          OR ${fulfilment_location_name} LIKE '%WHS%'
          OR ${fulfilment_location_name} LIKE '%WH%'
          OR ${fulfilment_location_name} LIKE '%ECOMMERCE%'
          OR ${fulfilment_location_name} LIKE '%FASHION%'
        THEN 'Warehouse'
          WHEN ${fulfilment_location_name} is NULL
        THEN 'None'
          ELSE 'Store' END;;
  }

  measure: pack_to_customer_collect  {
    label: "C&C Pack to Customer Collect (D:H:MM)"
    description: "Average time taken from packing to customer collection"
    hidden: yes
    type: average
    value_format: "D:H:MM"
    sql:DATETIME_DIFF(CAST(${cc_customer_collect_raw} as datetime),CAST(${oms_packed_raw} as datetime), SECOND)/86400 ;;
    filters: [unified_status: "collected_by_customer"]
  }

  measure: pack_to_cc_confirm  {
    label: "C&C Pack to Receive (D:H:MM)"
    description: "Average time taken from packing to receiving and confirming the order"
    hidden: yes
    type: average
    value_format: "D:H:MM"
    sql:DATETIME_DIFF(CAST(${cc_order_received_raw} as datetime),CAST(${oms_packed_raw} as datetime), SECOND)/86400 ;;
    filters: [unified_status: "recieved_in_shop"]
  }

  measure: cc_confirm_to_inform_customer  {
    label: "C&C Receive to Inform Customer (D:H:MM)"
    description: "Average time taken from receiving and confirming the order to informing the customer"
    hidden: yes
    type: average
    value_format: "D:H:MM"
    sql:DATETIME_DIFF(CAST(${cc_informed_customer_raw} as datetime),CAST(${cc_order_received_raw} as datetime), SECOND)/86400 ;;
    filters: [unified_status: "ready_for_collection"]
  }

  measure: inform_customer_to_customer_collect  {
    label: "C&C Inform to Customer Collect (D:H:MM)"
    description: "Average time taken from informing the customer to collection"
    hidden: yes
    type: average
    value_format: "D:H:MM"
    sql:DATETIME_DIFF(CAST(${cc_customer_collect_raw} as datetime),CAST(${cc_informed_customer_raw} as datetime), SECOND)/86400 ;;
    filters: [unified_status: "collected_by_customer"]
  }

  measure: click_to_customer_collect  {
    label: "C&C Click to Customer Collect (D:H:MM)"
    description: "Average time taken from order creation to customer collection"
    hidden: yes
    type: average
    value_format: "D:H:MM"
    sql:DATETIME_DIFF(CAST(${cc_customer_collect_raw} as datetime),CAST(${oms_creation_raw} as datetime), SECOND)/86400 ;;
    filters: [unified_status: "collected_by_customer"]
  }

  dimension: Sendsale_vs_Webshop {
    label: "Sendsale Orders"
    type: yesno
    sql: ${order_id} LIKE '08001%' ;;
  }

  set: order_details {
    fields: [bu_brand_for_access_filter, order_id, fulfilment_location_name, fulfilment_location_country, carrier, tracking_no, oms_creation_time, oms_packed_time, Booked_time, cu_collected_time, cu_delivered_time, click_to_deliver_in_hrs, click_to_pack_in_hrs, pack_to_book_in_hrs, book_to_collect_in_hrs, collect_to_deliver_in_hrs]
  }

  measure: click_to_deliver_in_hrs {
    label: "Click to Deliver (in hrs)"
    group_label: "Lead Time"
    description: "Average time taken from order creation to delivered"
    type: average
    value_format: "#.0;(#.0)"
    sql:DATETIME_DIFF(CAST(${cu_delivered_raw} as datetime),CAST(${oms_creation_raw} as datetime), SECOND)/3600 ;;
    drill_fields: [order_details*]
    link: {
      label: "Explore Orders Timestamps (sorted from highest order to deliver time)"
      url: "{{click_to_pack_in_hrs._link}}&sorts=oms_x_carriyo.click_to_deliver_in_hrs+desc"
    }
  }

  measure: click_to_claim_in_hrs  {
    label: "Click to Claim (in hrs)"
    group_label: "Lead Time"
    description: "Average time taken from order creation to claiming the order"
    type: average
    value_format: "#.0;(#.0)"
    sql:DATETIME_DIFF(CAST(${oms_claimed_raw} as datetime),CAST(${oms_creation_raw} as datetime), SECOND)/3600 ;;
    drill_fields: [order_details*]
    link: {
      label: "Explore Orders Timestamps (sorted from highest order to pack time)"
      url: "{{click_to_pack_in_hrs._link}}&sorts=oms_x_carriyo.click_to_claim_in_hrs+desc"
    }
  }


  measure: click_to_pack_in_hrs  {
    label: "Click to Pack (in hrs)"
    group_label: "Lead Time"
    description: "Average time taken from order creation to packing"
    type: average
    value_format: "#.0;(#.0)"
    sql:DATETIME_DIFF(CAST(${oms_packed_raw} as datetime),CAST(${oms_creation_raw} as datetime), SECOND)/3600 ;;
    drill_fields: [order_details*]
    link: {
      label: "Explore Orders Timestamps (sorted from highest order to pack time)"
      url: "{{click_to_pack_in_hrs._link}}&sorts=oms_x_carriyo.click_to_pack_in_hrs+desc"
    }
  }

  measure: claim_to_pack_in_hrs  {
    label: "Claim to Pack (in hrs)"
    group_label: "Lead Time"
    description: "Average time taken from claiming the order to packing"
    type: average
    value_format: "#.0;(#.0)"
    sql:DATETIME_DIFF(CAST(${oms_packed_raw} as datetime),CAST(${oms_claimed_raw} as datetime), SECOND)/3600 ;;
    drill_fields: [order_details*]
    link: {
      label: "Explore Orders Timestamps (sorted from highest order to pack time)"
      url: "{{click_to_pack_in_hrs._link}}&sorts=oms_x_carriyo.claim_to_pack_in_hrs+desc"
    }
  }

  measure: claim_to_pack_in_mins  {
    label: "Claim to Pack (in mins)"
    group_label: "Lead Time"
    description: "Average time taken from claiming the order to packing"
    type: average
    value_format: "#.0;(#.0)"
    sql:DATETIME_DIFF(CAST(${oms_packed_raw} as datetime),CAST(${oms_claimed_raw} as datetime), SECOND)/60 ;;
    drill_fields: [order_details*]
    link: {
      label: "Explore Orders Timestamps (sorted from highest order to pack time)"
      url: "{{click_to_pack_in_hrs._link}}&sorts=oms_x_carriyo.claim_to_pack_in_mins+desc"
    }
  }

  measure: pack_to_book_in_hrs {
    label: "Pack to Book (in hrs)"
    group_label: "Lead Time"
    description: "Average time taken from order packing to order booking"
    type: average
    value_format: "#.0;(#.0)"
    sql:DATETIME_DIFF(CAST(${Booked_raw} as datetime),CAST(${oms_packed_raw} as datetime), SECOND)/3600 ;;
    drill_fields: [order_details*]
    link: {
      label: "Explore Orders Timestamps (sorted from highest pack to book time)"
      url: "{{click_to_pack_in_hrs._link}}&sorts=oms_x_carriyo.pack_to_book_in_hrs+desc"
    }
  }

  measure: book_to_collect_in_hrs  {
    label: "Book to Collect (in hrs)"
    group_label: "Lead Time"
    description: "Average time taken from order booking to store pick up by DSP"
    type: average
    value_format: "#.0;(#.0)"
    sql:DATETIME_DIFF(CAST(${cu_collected_raw} as datetime),CAST(${Booked_raw} as datetime), SECOND)/3600 ;;
    drill_fields: [order_details*]
    link: {
      label: "Explore Orders Timestamps (sorted from highest book to collect time)"
      url: "{{click_to_pack_in_hrs._link}}&sorts=oms_x_carriyo.book_to_collect_in_hrs+desc"
    }
  }

  measure: collect_to_deliver_in_hrs  {
    label: "Collect to Deliver (in hrs)"
    group_label: "Lead Time"
    description: "Average time taken from order collection to delivered"
    type: average
    value_format: "#.0;(#.0)"
    sql:DATETIME_DIFF(CAST(${cu_delivered_raw} as datetime),CAST(${cu_collected_raw} as datetime), SECOND)/3600 ;;
    drill_fields: [order_details*]
    link: {
      label: "Explore Orders Timestamps (sorted from highest collect to deliver time)"
      url: "{{click_to_pack_in_hrs._link}}&sorts=oms_x_carriyo.collect_to_deliver_in_hrs+desc"
    }
  }

  dimension_group: Booked{
    type: time
    timeframes: [
      raw,
      time,
      second,
      minute,
      hour,
      time_of_day,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.booked_at AS TIMESTAMP) ;;
  }

  dimension: pending_new_not_claim  {
    group_label: "Pending Orders (in Hrs)"
    label: "New Order - Not Yet Claimed"
    description: "No. of hours the order has been pending to be claimed by a location"
    hidden: no
    type: number
    value_format: "#.0;(#.0)"
    sql:DATETIME_DIFF(CAST(${time_now_raw} as datetime),CAST(${oms_creation_webshop_time_raw} as datetime), SECOND)/3600 ;;
  }

  dimension: pending_new_not_claim_grouping  {
    group_label: "Pending Orders (in Hrs)"
    label: "New Order Pending - Hour Buckets"
    case: {
      when: {
        sql: ${pending_new_not_claim} <=5 ;;
        label: "0-5 hrs"
      }
      when: {
        sql: ${pending_new_not_claim} > 5 AND ${pending_new_not_claim} <=24 ;;
        label: "5-24 hrs"
      }
      when: {
        sql: ${pending_new_not_claim} > 24 AND ${pending_new_not_claim} <=48 ;;
        label: "24-48 hrs"
      }
      when: {
        sql: ${pending_new_not_claim} > 48 ;;
        label: "> 48 hrs"
      }
      else: "Unknown"
    }
  }

  dimension: pending_claim_not_pack  {
    group_label: "Pending Orders (in Hrs)"
    label: "Claimed - Not Packed"
    description: "No. of hours the order has been pending to be packed after claiming"
    hidden: no
    type: number
    value_format: "#.0;(#.0)"
    sql:DATETIME_DIFF(CAST(${time_now_raw} as datetime),CAST(${oms_claimed_raw} as datetime), SECOND)/3600 ;;
  }


  dimension: pending_claim_not_pack_grouping  {
    group_label: "Pending Orders (in Hrs)"
    label: "Claimed - Not Packed - Hour Buckets"
    case: {
      when: {
        sql: ${pending_claim_not_pack} <=5 ;;
        label: "0-5 hrs"
      }
      when: {
        sql: ${pending_claim_not_pack} > 5 AND ${pending_claim_not_pack} <=24 ;;
        label: "5-24 hrs"
      }
      when: {
        sql: ${pending_claim_not_pack} > 24 AND ${pending_claim_not_pack} <=48 ;;
        label: "24-48 hrs"
      }
      when: {
        sql: ${pending_claim_not_pack} > 48 ;;
        label: "> 48 hrs"
      }
      else: "Unknown"
    }
  }


}
