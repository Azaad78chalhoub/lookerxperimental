view: dim_wh_pack_on_time_sla {
  sql_table_name: `chb-prod-supplychain-data.prod_warehouse_logistics_supply_chain.dim_wh_pack_on_time_sla`
    ;;

  dimension: country {
    type: string
    #map_layer_name: countries
    sql: ${TABLE}.COUNTRY ;;
  }

  dimension: wh_id {
    type: string
    sql: ${TABLE}.WH_ID ;;
  }

  dimension: priority {
    type: number
    sql: ${TABLE}.PRIORITY ;;
  }

  dimension: custom_type {
    type: string
    sql: ${TABLE}.CUSTOM_TYPE ;;
  }

  dimension: weekday {
    type: string
    sql: ${TABLE}.WEEKDAY ;;
  }

  dimension: cutoff_time {
    type: string
    sql: ${TABLE}.cut_off ;;
  }

  dimension: pack_time {
    type: string
    sql: ${TABLE}.PACK_TIME ;;
  }

  dimension: before_d_day {
    type: number
    sql: ${TABLE}.BEFORE_D_DAY ;;
  }

  dimension: before_day {
    type: string
    sql: ${TABLE}.BEFORE_DAY ;;
  }

  dimension: after_d_day {
    type: number
    sql: ${TABLE}.AFTER_D_DAY ;;
  }

  dimension: after_day {
    type: string
    sql: ${TABLE}.AFTER_DAY ;;
  }

  dimension: rule_start_date {
    type:  date
    sql:  ${TABLE}.rule_start_date ;;
  }


  dimension: rule_end_date {
    type:  date
    sql:  ${TABLE}.rule_end_date ;;
  }

  dimension: comments {
    type: string
    sql: ${TABLE}.COMMENTS ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
