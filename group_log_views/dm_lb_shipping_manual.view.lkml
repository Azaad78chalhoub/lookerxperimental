view: dm_lb_shipping_manual {
  sql_table_name: `chb-prod-stage-oracle.prod_supply_chain.ref_union_shipping_manual_prod_regd_others_activity_reference`
    ;;

  dimension: activity_name {
    type: string
    sql: ${TABLE}.ACTIVITY_NAME ;;
  }

  dimension: bu_code {
    type: string
    sql: ${TABLE}.BU_CODE ;;
  }

  dimension: bu_desc_icsb {
    type: string
    sql: ${TABLE}.BU_DESC_ICSB ;;
  }

  dimension: charge_country {
    type: string
    sql: ${TABLE}.CHARGE_COUNTRY ;;
  }

  dimension: charge_currency {
    type: string
    sql: ${TABLE}.CHARGE_CURRENCY ;;
  }

  dimension: charge_rate {
    type: string
    sql: ${TABLE}.CHARGE_RATE ;;
  }

  dimension: column_name {
    type: string
    sql: ${TABLE}.COLUMN_NAME ;;
  }

  dimension: facility_name {
    type: string
    sql: ${TABLE}.FACILITY_NAME ;;
  }

  dimension: icsb_service_name {
    type: string
    sql: ${TABLE}.ICSB_SERVICE_NAME ;;
  }

  dimension: legal_entity {
    type: string
    sql: ${TABLE}.LEGAL_ENTITY ;;
  }

  dimension: location {
    type: string
    sql: ${TABLE}.location ;;
  }

  dimension: manual_invoice {
    type: string
    sql: ${TABLE}.MANUAL_INVOICE ;;
  }

  dimension: period_from {
    type: string
    sql: ${TABLE}.period_from ;;
  }

  dimension: period_to {
    type: string
    sql: ${TABLE}.period_to ;;
  }

  dimension: quantity {
    type: string
    sql: ${TABLE}.QUANTITY ;;
  }

  dimension: service_type {
    type: string
    sql: ${TABLE}.SERVICE_TYPE ;;
  }

  dimension: total_amount {
    type: string
    sql: ${TABLE}.Total_Amount ;;
  }

  measure: count {
    type: count
    drill_fields: [activity_name, column_name, icsb_service_name, facility_name]
  }
}
