view: dm_logistics_unified {
  sql_table_name: `chb-prod-supplychain-data.prod_supply_chain.dm_logistics_unified`
    ;;

  dimension: amount_refunded {
    group_label: "Sales"
    type: number
    sql: ${TABLE}.amount_refunded ;;
  }

  dimension: approximate_creation_date_time {
    type: number
    group_label: "DSP"
    sql: ${TABLE}.approximate_creation_date_time ;;
  }

  dimension: awb {
    type: string
    label: "Airway Bill"
    description: "Airway Bill number from WM9"
    sql: ${TABLE}.awb ;;
  }

  dimension_group: booked {
    group_label: "DSP"
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.booked_at ;;
  }

  dimension: brand {
    description: "Brand name from Sales data"
    type: string
    sql: ${TABLE}.brand ;;
  }

  dimension: canceled_amount_local {
    type: number
    group_label: "Sales"
    sql: ${TABLE}.canceled_amount_local ;;
  }

  dimension: canceled_amount_usd {
    type: number
    group_label: "Sales"
    sql: ${TABLE}.canceled_amount_usd ;;
  }

  dimension_group: car_order {
    label: "Order placed in DSP"
    group_label: "DSP"
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.car_order_date ;;
  }

  dimension: car_price {
    type: string
    group_label: "DSP"
    sql: ${TABLE}.car_price ;;
  }

  dimension: car_sku {
    type: string
    group_label: "DSP"
    sql: ${TABLE}.car_sku ;;
  }

  dimension: car_status {
    type: string
    group_label: "DSP"
    sql: ${TABLE}.car_status ;;
  }

  dimension: carrier {
    type: string
    group_label: "DSP"
    sql: ${TABLE}.carrier ;;
  }

  dimension: carrier_account_name {
    type: string
    group_label: "DSP"
    sql: ${TABLE}.carrier_account_name ;;
  }

  dimension: carrier_code {
    type: string
    description: "Carrier code from wm9"
    hidden: yes
    sql: ${TABLE}.carrier_code ;;
  }

  dimension: carrier_id {
    type: string
    hidden: yes
    description: "Carrier ID from DSP"
    sql: ${TABLE}.carrier_id ;;
  }

  dimension: carrier_name {
    type: string
    description: "Carrier name from wm9"
    hidden: yes
    sql: ${TABLE}.carrier_name ;;
  }

  dimension: carrier_status {
    type: string
    description: "Order status from DSP"
    sql: ${TABLE}.carrier_status ;;
  }

  dimension: carrier_status_description {
    type: string
    description: "Order status description from DSP"
    hidden: yes
    sql: ${TABLE}.carrier_status_description ;;
  }

  dimension: chb_status {
    type: string
    description: "Final from DSP"
    hidden: yes
    sql: ${TABLE}.CHB_STATUS ;;
  }

  dimension: codelkup_otype_description {
    type: string
    hidden: yes
    sql: ${TABLE}.codelkup_otype_description ;;
  }

  dimension: codelkup_priority_description {
    type: string
    hidden: yes
    description: "States the priority of the order (from WM9)"
    sql: ${TABLE}.codelkup_priority_description ;;
  }

  dimension: codelkup_type_description {
    type: string
    hidden: yes
    sql: ${TABLE}.codelkup_type_description ;;
  }

  dimension_group: confirmation {
    description: "Order confirmation date in WM9"
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.confirmation_date ;;
  }

  dimension: country {
    type: string
    description: "Country from WM9"
    hidden: yes
    map_layer_name: countries
    sql: ${TABLE}.country ;;
  }

  dimension_group: creation {
    description: "Order creation date in DSP"
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.creation_date ;;
  }

  dimension: customer_email {
    type: string
    hidden: yes
    sql: ${TABLE}.customer_email ;;
  }

  dimension: dangerous_goods_bool {
    type: yesno
    hidden: yes
    sql: ${TABLE}.dangerous_goods_BOOL ;;
  }

  dimension_group: delivered {
    description: "Date of order delivery from DSP"
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.delivered_at ;;
  }

  dimension: delivery_type {
    type: string
    hidden: yes
    sql: ${TABLE}.deliveryType ;;
  }

  dimension: description {
    type: string
    hidden: yes
    sql: ${TABLE}.description ;;
  }

  dimension: discount_amount {
    type: number
    group_label: "Sales"
    sql: ${TABLE}.discount_amount ;;
  }

  dimension: discount_refunded {
    type: number
    group_label: "Sales"
    sql: ${TABLE}.discount_refunded ;;
  }

  dimension: dropoff_address1 {
    type: string
    hidden: yes
    sql: ${TABLE}.dropoff_address1 ;;
  }

  dimension: dropoff_address2 {
    type: string
    hidden: yes
    sql: ${TABLE}.dropoff_address2 ;;
  }

  dimension: dropoff_city {
    type: string
    description: "Order destination city from DSP"
    sql: ${TABLE}.dropoff_city ;;
  }

  dimension: dropoff_contact_email {
    type: string
    hidden: yes
    sql: ${TABLE}.dropoff_contact_email ;;
  }

  dimension: dropoff_contact_name {
    type: string
    hidden: yes
    sql: ${TABLE}.dropoff_contact_name ;;
  }

  dimension: dropoff_contact_phone {
    type: string
    hidden: yes
    sql: ${TABLE}.dropoff_contact_phone ;;
  }

  dimension: dropoff_country {
    type: string
    hidden: yes
    sql: ${TABLE}.dropoff_country ;;
  }

  dimension: dropoff_notes {
    type: string
    hidden: yes
    sql: ${TABLE}.dropoff_notes ;;
  }

  dimension: dropoff_post_code {
    type: string
    hidden: yes
    sql: ${TABLE}.dropoff_post_code ;;
  }

  dimension: dropoff_state {
    type: string
    hidden: yes
    sql: ${TABLE}.dropoff_state ;;
  }

  dimension: event_name {
    type: string
    hidden: yes
    sql: ${TABLE}.event_name ;;
  }

  dimension: facility_name {
    type: string
    description: "States the facility name/WH location of order fulfilment (obtained from wm9)"
    sql: ${TABLE}.facility_name ;;
  }

  dimension_group: failed_attempt {
    description: "Date of failed attempt to deliver the order from DSP"
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.failed_attempt_at ;;
  }

  dimension_group: first_attempt {
    description: "Date of failed attempt to deliver the order from DSP"
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.first_attempt ;;
  }

  dimension: gross_minus_cancel_local {
    group_label: "Sales"
    type: number
    sql: ${TABLE}.gross_minus_cancel_local ;;
  }

  dimension: gross_minus_cancel_usd {
    type: number
    group_label: "Sales"
    sql: ${TABLE}.gross_minus_cancel_usd ;;
  }

  dimension: gross_net_local {
    type: number
    group_label: "Sales"
    sql: ${TABLE}.gross_net_local ;;
  }

  dimension: gross_net_usd {
    type: number
    group_label: "Sales"
    sql: ${TABLE}.gross_net_usd ;;
  }

  dimension: gross_revenue_local {
    type: number
    group_label: "Sales"
    sql: ${TABLE}.gross_revenue_local ;;
  }

  dimension: gross_revenue_usd {
    type: number
    group_label: "Sales"
    sql: ${TABLE}.gross_revenue_usd ;;
  }

  dimension: hs_code {
    type: string
    hidden: yes
    sql: ${TABLE}.hsCode ;;
  }

  dimension: latest_status {
    type: number
    hidden: yes
    sql: ${TABLE}.latest_status ;;
  }

  dimension: merchant {
    type: string
    label: "Business Unit"
    description: "Merchant name from DSP"
    sql: ${TABLE}.merchant ;;
  }

  dimension: no_of_attempts {
    type: number
    sql: ${TABLE}.no_of_attempts ;;
  }

  dimension: notes {
    type: string
    hidden: yes
    sql: ${TABLE}.notes ;;
  }

  dimension: order_c_city {
    type: string
    hidden: yes
    sql: ${TABLE}.order_c_city ;;
  }

  dimension: order_consigneekey {
    type: string
    hidden: yes
    sql: ${TABLE}.order_consigneekey ;;
  }

  dimension_group: order_creation {
    description: "Order creation date in WM9"
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.order_creation_date ;;
  }

  dimension_group: order_creation_date_gmt {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.order_creation_date_gmt ;;
  }

  dimension_group: order_datetime {
    label: "Order created"
    description: "Order Creation datetime in Sales system"
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.order_datetime ;;
  }

  dimension: order_extern_orderkey {
    hidden: yes
    type: string
    sql: ${TABLE}.order_extern_orderkey ;;
  }

  dimension: order_extern_orderkey_2 {
    hidden: yes
    type: string
    sql: ${TABLE}.order_extern_orderkey_2 ;;
  }

  dimension: order_ohtype {
    type: string
    hidden: yes
    sql: ${TABLE}.order_ohtype ;;
  }

  dimension: order_priority {
    type: string
    hidden: yes
    sql: ${TABLE}.order_priority ;;
  }

  dimension: order_promotion_detail_campaign_id {
    type: string
    group_label: "Sales"
    sql: ${TABLE}.order_promotion_detail_campaign_id ;;
  }

  dimension: order_promotion_detail_coupon_code {
    type: string
    group_label: "Sales"
    sql: ${TABLE}.order_promotion_detail_coupon_code ;;
  }

  dimension: order_promotion_detail_coupon_id {
    type: string
    group_label: "Sales"
    sql: ${TABLE}.order_promotion_detail_coupon_id ;;
  }

  dimension: order_promotion_detail_discount {
    group_label: "Sales"
    type: number
    sql: ${TABLE}.order_promotion_detail_discount ;;
  }

  dimension: order_promotion_detail_promotion_id {
    type: string
    group_label: "Sales"
    sql: ${TABLE}.order_promotion_detail_promotion_id ;;
  }

  dimension: order_promotion_detail_promotion_text {
    type: string
    group_label: "Sales"
    sql: ${TABLE}.order_promotion_detail_promotion_text ;;
  }

  dimension_group: order_requested_ship {
    hidden: yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.order_requested_ship_date ;;
  }

  dimension_group: order_requested_ship_date_gmt {
    hidden: yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.order_requested_ship_date_gmt ;;
  }

  dimension: order_status_code {
    type: string
    hidden: yes
    sql: ${TABLE}.order_status_code ;;
  }

  dimension: order_status_description {
    type: string
    hidden: yes
    sql: ${TABLE}.order_status_description ;;
  }

  dimension: order_storerkey {
    type: string
    hidden: yes
    sql: ${TABLE}.order_storerkey ;;
  }

  dimension: order_type {
    type: string
    hidden: yes
    sql: ${TABLE}.order_type ;;
  }

  dimension: order_vas_activities {
    hidden: yes
    sql: ${TABLE}.order_vas_activities ;;
  }

  dimension: origin_country {
    label: "Country of Origin for the order"
    type: string
    sql: ${TABLE}.origin_country ;;
  }

  dimension_group: out_for_delivery {
    description: "Out for delivery date for the order from DSP"
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.out_for_delivery_at ;;
  }

  dimension: parcels {
    hidden: yes
    type: string
    sql: ${TABLE}.parcels ;;
  }

  dimension: partner_shipment_reference {
    label: "Order #"
    type: string
    sql: ${TABLE}.partnerShipmentReference ;;
  }

  dimension: payment_method {
    type: string
    hidden: yes
    sql: ${TABLE}.payment_method ;;
  }

  dimension: payment_mode {
    type: string
    description: "Payment mode for the order from DSP"
    sql: ${TABLE}.payment_mode ;;
  }

  dimension: payment_payment_currency {
    type: string
    hidden: yes
    sql: ${TABLE}.payment_payment_currency ;;
  }

  dimension: payment_total_amount {
    type: string
    sql: ${TABLE}.payment_total_amount ;;
  }

  dimension: pickup_address1 {
    type: string
    sql: ${TABLE}.pickup_address1 ;;
  }

  dimension: pickup_city {
    type: string
    sql: ${TABLE}.pickup_city ;;
  }

  dimension: pickup_contact_email {
    type: string
    hidden: yes
    sql: ${TABLE}.pickup_contact_email ;;
  }

  dimension: pickup_contact_name {
    type: string
    hidden: yes
    sql: ${TABLE}.pickup_contact_name ;;
  }

  dimension: pickup_contact_phone {
    type: string
    hidden: yes
    sql: ${TABLE}.pickup_contact_phone ;;
  }

  dimension: pickup_country {
    type: string
    sql: ${TABLE}.pickup_country ;;
  }

  dimension: pickup_post_code {
    type: string
    hidden: yes
    sql: ${TABLE}.pickup_post_code ;;
  }

  dimension: primary_key {
    type: string
    hidden: yes
    group_label: "Sales"
    sql: ${TABLE}.primary_key ;;
  }

  dimension_group: promised_delivery {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.promisedDeliveryDate ;;
  }

  dimension: promotion_detail_campaign_id {
    type: string
    group_label: "Sales"
    sql: ${TABLE}.promotion_detail_campaign_id ;;
  }

  dimension: promotion_detail_coupon_code {
    type: string
    group_label: "Sales"
    sql: ${TABLE}.promotion_detail_coupon_code ;;
  }

  dimension: promotion_detail_coupon_id {
    type: string
    group_label: "Sales"
    sql: ${TABLE}.promotion_detail_coupon_id ;;
  }

  dimension: promotion_detail_discount {
    type: number
    group_label: "Sales"
    sql: ${TABLE}.promotion_detail_discount ;;
  }

  dimension: promotion_detail_promotion_id {
    type: string
    group_label: "Sales"
    sql: ${TABLE}.promotion_detail_promotion_id ;;
  }

  dimension: promotion_detail_promotion_text {
    type: string
    group_label: "Sales"
    sql: ${TABLE}.promotion_detail_promotion_text ;;
  }

  dimension: qty_canceled {
    type: number
    group_label: "Sales"
    sql: ${TABLE}.qty_canceled ;;
  }

  dimension: qty_ordered {
    type: number
    group_label: "Sales"
    sql: ${TABLE}.qty_ordered ;;
  }

  dimension: qty_refunded {
    type: number
    group_label: "Sales"
    sql: ${TABLE}.qty_refunded ;;
  }

  dimension: qty_shipped {
    type: number
    group_label: "Sales"
    sql: ${TABLE}.qty_shipped ;;
  }

  dimension: quantity {
    type: string
    sql: ${TABLE}.quantity ;;
  }

  dimension_group: record {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.record_date ;;
  }

  dimension_group: record_datetime {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.record_datetime ;;
  }

  dimension: record_type {
    type: string
    group_label: "Sales"
    description: "Transaction type in Sales System"
    sql: ${TABLE}.record_type ;;
  }

  dimension: refund_amount_local {
    type: number
    group_label: "Sales"
    sql: ${TABLE}.refund_amount_local ;;
  }

  dimension: refund_amount_usd {
    type: number
    group_label: "Sales"
    sql: ${TABLE}.refund_amount_usd ;;
  }

  dimension_group: returned {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.returned_at ;;
  }

  dimension_group: second_attempt {
    description: "Second attempt date for order delivery from DSP"
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.second_attempt ;;
  }

  dimension: sequence_number {
    type: string
    hidden: yes
    sql: ${TABLE}.sequence_number ;;
  }

  dimension: ship_net_local {
    type: number
    group_label: "Sales"
    sql: ${TABLE}.ship_net_local ;;
  }

  dimension: ship_net_usd {
    type: number
    group_label: "Sales"
    sql: ${TABLE}.ship_net_usd ;;
  }

  dimension: shipment_id {
    type: string
    hidden: yes
    sql: ${TABLE}.shipmentId ;;
  }

  dimension: shipment_net_sales_incl_tax {
    type: number
    group_label: "Sales"
    sql: ${TABLE}.shipment_net_sales_incl_tax ;;
  }

  dimension: shipment_net_sales_usd_incl_tax {
    type: number
    group_label: "Sales"
    sql: ${TABLE}.shipment_net_sales_usd_incl_tax ;;
  }

  dimension: shipment_revenue_local {
    type: number
    group_label: "Sales"
    sql: ${TABLE}.shipment_revenue_local ;;
  }

  dimension: shipment_revenue_usd {
    type: number
    group_label: "Sales"
    sql: ${TABLE}.shipment_revenue_usd ;;
  }

  dimension_group: shipped {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.shipped_at ;;
  }

  dimension: shipped_minus_cancel_local {
    type: number
    group_label: "Sales"
    sql: ${TABLE}.shipped_minus_cancel_local ;;
  }

  dimension: shipped_minus_cancel_usd {
    type: number
    group_label: "Sales"
    sql: ${TABLE}.shipped_minus_cancel_usd ;;
  }

  dimension_group: so_order {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.so_order_date ;;
  }

  dimension: so_order_id {
    type: string
    group_label: "Sales"
    description: "Order ID in Sales Data"
    sql: ${TABLE}.so_order_id ;;
  }

  dimension: so_price {
    type: number
    group_label: "Sales"
    sql: ${TABLE}.so_price ;;
  }

  dimension: so_sku {
    type: string
    label: "Product ID"
    group_label: "Sales"
    description: "Product ID in Sales Data"
    sql: ${TABLE}.so_sku ;;
  }

  dimension: so_status {
    type: string
    group_label: "Sales"
    sql: ${TABLE}.so_status ;;
  }

  dimension: source {
    type: string
    group_label: "Sales"
    description: "Source system for the E-COM sales. eg: SFCC, MAGENTO, Shopify etc"
    sql: ${TABLE}.source ;;
  }

  dimension: status_hierarchy {
    type: number
    sql: ${TABLE}.status_hierarchy ;;
  }

  dimension: store_country {
    type: string
    group_label: "Sales"
    description: "Country location for sales origin"
    sql: ${TABLE}.store_country ;;
  }

  dimension: store_id {
    type: number
    group_label: "Sales"
    description: "Location number for sales origin"
    sql: ${TABLE}.store_id ;;
  }

  dimension: storer_customer {
    type: string
    hidden: yes
    sql: ${TABLE}.storer_customer ;;
  }

  dimension: storer_owner {
    type: string
    hidden: yes
    sql: ${TABLE}.storer_owner ;;
  }

  dimension: suppress_communication {
    type: string
    hidden: yes
    sql: ${TABLE}.suppress_communication ;;
  }

  dimension_group: third_attempt {
    description: "Third attempt date for order delivery from DSP"
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.third_attempt ;;
  }

  dimension: tracking_no {
    type: string
    description: "Order tracking no from DSP"
    sql: ${TABLE}.tracking_no ;;
  }

  dimension_group: transmitlog_status_c_mctorderpicked_date_gmt {
    hidden: yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.transmitlog_status_c_mctorderpicked_date_gmt ;;
  }

  dimension_group: transmitlog_status_c_orderallocationrun_date_gmt {
    hidden: yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.transmitlog_status_c_orderallocationrun_date_gmt ;;
  }

  dimension_group: transmitlog_status_c_orderpacked_date_gmt {
    hidden: yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.transmitlog_status_c_orderpacked_date_gmt ;;
  }

  dimension_group: transmitlog_status_c_ordershipped_date_gmt {
    hidden: yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.transmitlog_status_c_ordershipped_date_gmt ;;
  }

  dimension_group: transmitlog_status_mctorderpicked {
    hidden: yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.transmitlog_status_mctorderpicked_date ;;
  }

  dimension_group: transmitlog_status_mctorderpicked_date_gmt {
    hidden: yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.transmitlog_status_mctorderpicked_date_gmt ;;
  }

  dimension_group: transmitlog_status_orderallocationrun_date_gmt {
    hidden: yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.transmitlog_status_orderallocationrun_date_gmt ;;
  }

  dimension_group: transmitlog_status_ordercancelled_date_gmt {
    hidden: yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.transmitlog_status_ordercancelled_date_gmt ;;
  }

  dimension_group: transmitlog_status_orderpacked {
    hidden: yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.transmitlog_status_orderpacked_date ;;
  }

  dimension_group: transmitlog_status_orderpacked_date_gmt {
    hidden: yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.transmitlog_status_orderpacked_date_gmt ;;
  }

  dimension_group: transmitlog_status_ordershipped {
    hidden: yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.transmitlog_status_ordershipped_date ;;
  }

  dimension_group: transmitlog_status_ordershipped_date_gmt {
    hidden: yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.transmitlog_status_ordershipped_date_gmt ;;
  }

  dimension_group: transmitlog_status_so_date_gmt {
    hidden: yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.transmitlog_status_SO_date_gmt ;;
  }

  dimension_group: update {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.update_date ;;
  }

  dimension: vas_applied {
    type: yesno
    label: "VAS Applied(yes/no)"
    description: "Indicator from WM9 stating if a value added service has been applied on the ggiven order"
    sql: ${TABLE}.vas_applied ;;
  }

  dimension: vertical {
    type: string
    description: "Vertical from Sales data"
    sql: ${TABLE}.vertical ;;
  }

  dimension: warehouse_id {
    type: string
    sql: ${TABLE}.warehouse_id ;;
  }

  dimension: wm9_order_id {
    hidden: yes
    type: string
    sql: ${TABLE}.wm9_order_id ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      pickup_contact_name,
      facility_name,
      dropoff_contact_name,
      event_name,
      carrier_account_name,
      carrier_name
    ]
  }
}

view: dm_logistics_unified__order_vas_activities {
  dimension: box_in_box {
    type: string
    sql: ${TABLE}.box_in_box ;;
  }

  dimension: giftmessage2 {
    type: string
    sql: ${TABLE}.giftmessage2 ;;
  }

  dimension: giftmessage3 {
    type: string
    sql: ${TABLE}.giftmessage3 ;;
  }

  dimension: giftmessage4 {
    type: string
    sql: ${TABLE}.giftmessage4 ;;
  }

  dimension: giftwrapstraptype {
    type: string
    sql: ${TABLE}.giftwrapstraptype ;;
  }

  dimension: gitfmessage {
    type: string
    sql: ${TABLE}.gitfmessage ;;
  }

  dimension: inkjet_printing {
    type: string
    sql: ${TABLE}.inkjet_printing ;;
  }

  dimension: laser_print {
    type: string
    sql: ${TABLE}.laser_print ;;
  }

  dimension: over_wrap {
    type: string
    sql: ${TABLE}.over_wrap ;;
  }

  dimension: scanning {
    type: string
    sql: ${TABLE}.scanning ;;
  }

  dimension: shrink_wrap {
    type: string
    sql: ${TABLE}.shrink_wrap ;;
  }

  dimension: sleeve_cello {
    type: string
    sql: ${TABLE}.sleeve_cello ;;
  }

  dimension: stitching {
    type: string
    sql: ${TABLE}.stitching ;;
  }

  dimension: stitching_unlabelling {
    type: string
    sql: ${TABLE}.stitching_unlabelling ;;
  }

  dimension: strapping {
    type: string
    sql: ${TABLE}.strapping ;;
  }

  dimension: tagging {
    type: string
    sql: ${TABLE}.tagging ;;
  }
}
