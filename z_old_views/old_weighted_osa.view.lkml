view: weighted_osa {
  derived_table: {
    explore_source: fact_soh_sales_poc {
      column: granularity_from_parameter {}
      column: grading { field: prod_grading.grading }
      column: on_shelf_availability {}
      bind_all_filters: yes
    }
  }

  dimension: pk {
    primary_key: yes
    hidden: yes
    type: string
    sql: CONCAT(${granularity_from_parameter}, ' - ', ${grading}) ;;
  }

  dimension: granularity_from_parameter {
    hidden: yes
    type: string
  }

  dimension: grading {
    hidden: yes
    type: string
  }

  dimension: on_shelf_availability {
    hidden: yes
    type: number
  }

  dimension: osa_temp {
    hidden: yes
    type: number
    sql:
    CASE
      WHEN ${grading} = 'A' THEN 0.7
      WHEN ${grading} = 'B' THEN 0.2
      WHEN ${grading} = 'C' THEN 0.1
      ELSE 1
    END * ${on_shelf_availability}
    ;;
  }

  measure: weighted_osa {
    view_label: "KPI's"
    type: sum
    sql: ${osa_temp} ;;
    value_format_name: percent_2
  }

}
