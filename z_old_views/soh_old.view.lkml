view: soh_history_day_loc_item_partitioned {

  sql_table_name: supply_chain_poc.SOH_HISTORY_DAY_LOC_ITEM_PARTITIONED ;;



  dimension: atr_currencycode {

    type: string

    sql: ${TABLE}.atr_currencycode ;;

    hidden:  yes

  }



  dimension: atr_mallname {

    type: string

    sql: ${TABLE}.atr_mallname ;;

    label: "Mall Name"

    group_label: "Stores"

  }



  dimension: av_cost {

    type: number

    hidden:  yes

    sql: ${TABLE}.av_cost ;;

  }



  dimension: brand {

    type: string

    label: "Brand"

    group_label: "Product"

    sql: ${TABLE}.brand ;;

  }



  dimension_group: changed_on_dt {

    type: time

    hidden:  yes

    timeframes: [

      raw,

      time,

      date,

      week,

      month,

      quarter,

      year

    ]

    sql: CAST(${TABLE}.changed_on_dt AS TIMESTAMP) ;;

  }



  dimension: class_name {

    type: string

    label: "Product Class"

    group_label: "Product"

    sql: ${TABLE}.class_name ;;

  }



  dimension_group: created_on_dt {

    type: time

    hidden:  yes

    timeframes: [

      raw,

      time,

      date,

      week,

      month,

      quarter,

      year

    ]

    sql: CAST(${TABLE}.created_on_dt AS TIMESTAMP) ;;

  }



  dimension: delete_flg {

    hidden: yes

    type: string

    sql: ${TABLE}.delete_flg ;;

  }



  dimension: desc_areaname {

    type: string

    label: "Store Area"

    group_label: "Stores"

    sql: ${TABLE}.desc_areaname ;;

  }



  dimension: desc_brand {

    type: string

    label: "Business Unit"

    suggestable: yes

    sql: ${TABLE}.desc_brand ;;

  }



  dimension: desc_regionname {

    type: string

    label: "Region Name"

    group_label: "Stores"

    sql: ${TABLE}.desc_regionname ;;

  }



  dimension: desc_storename {

    type: string

    label: "Store Name"

    group_label: "Stores"

    sql: ${TABLE}.desc_storename ;;

  }



  dimension: dm_recd_status {

    type: string

    hidden: yes

    sql: ${TABLE}.dm_recd_status ;;

  }





  dimension: division_name {

    type: string

    hidden: no

    sql: ${TABLE}.division_name ;;

  }



  dimension: doc_curr_code {

    type: string

    hidden:  yes

    sql: ${TABLE}.doc_curr_code ;;

  }



  dimension: first_received {

    type: date_time

    label: "First Received"

    group_label: "Product"

    sql:CAST(${TABLE}.first_received AS DATETIME)  ;;

  }



  dimension: first_receivedModified {

    type: date_time

    label: "First ReceivedModified"

    group_label: "Product"

    sql:DATE(CAST(first_received AS TIMESTAMP)) ;;

  }





  dimension_group: first_sold {

    type: time

    label: "First Sold"

    group_label: "Product"

    timeframes: [

      raw,

      time,

      date,

      week,

      month,

      quarter,

      year

    ]

    sql: CAST(${TABLE}.first_sold AS TIMESTAMP) ;;

  }



  dimension: from_dt_wid {

    type: number

    value_format_name: id

    hidden: yes

    sql: ${TABLE}.from_dt_wid ;;

  }



  dimension: gender {

    type: string

    label: "Gender"

    group_label: "Product"

    sql: ${TABLE}.gender ;;

  }



  dimension: global1_exchange_rate {

    type: number

    hidden: yes

    sql: ${TABLE}.global1_exchange_rate ;;

  }



  dimension: integration_id {

    type: string

    hidden:yes

    sql: ${TABLE}.integration_id ;;

  }



  dimension: inv_soh_cost_amt_lcl {

    type: number

    hidden: yes

    sql: ${TABLE}.inv_soh_cost_amt_lcl ;;

  }



  dimension: inv_soh_qty {

    type: number

    hidden: no

    sql: ${TABLE}.inv_soh_qty ;;

  }



  dimension: inv_soh_rtl_amt_lcl {

    type: number

    hidden: yes

    sql: ${TABLE}.inv_soh_rtl_amt_lcl ;;

  }



  dimension: inv_soh_rtl_wot_amt_lcl {

    type: number

    hidden: yes

    sql: ${TABLE}.inv_soh_rtl_wot_amt_lcl ;;

  }



  dimension: item_desc {

    type: string

    label: "Item Description"

    group_label: "Product"

    sql: ${TABLE}.item_desc ;;

  }



  dimension: loc_curr_code {

    type: string

    hidden: yes

    sql: ${TABLE}.loc_curr_code ;;

  }



  dimension: org_num {

    type: string

    label: "Org ID"

    group_label: "Stores"

    sql: ${TABLE}.org_num ;;

  }



  dimension: org_scd1_wid {

    type: number

    hidden: yes

    value_format_name: id

    sql: ${TABLE}.org_scd1_wid ;;

  }



  dimension: org_wid {

    type: number

    hidden: yes

    value_format_name: id

    sql: ${TABLE}.org_wid ;;

  }



  dimension: prod_num {

    type: string

    label: "Product ID"

    group_label: "Product"

    sql: ${TABLE}.prod_num ;;

  }



  dimension: prod_scd1_wid {

    type: number

    hidden:  yes

    value_format_name: id

    sql: ${TABLE}.prod_scd1_wid ;;

  }



  dimension: prod_stat_wid_c {

    type: number

    hidden: yes

    sql: ${TABLE}.prod_stat_wid_c ;;

  }



  dimension: prod_wid {

    type: number

    hidden: yes

    value_format_name: id

    sql: ${TABLE}.prod_wid ;;

  }



  dimension: row_wid {

    type: number

    hidden: yes

    value_format_name: id

    sql: ${TABLE}.row_wid ;;

  }



  dimension: sale_qty {

    type: number

    hidden: yes

    sql: ${TABLE}.Sale_STY ;;

  }



  dimension: sales_amt_usd {

    type: number

    hidden: yes

    sql: ${TABLE}.SalesAmtUSD ;;

  }



  dimension: season_desc {

    type: string

    label: "Product Season"

    group_label: "Product"

    sql: ${TABLE}.season_desc ;;

  }



  dimension: status {

    type: string

    label: "Product Status"

    group_label: "Product"

    sql: ${TABLE}.status ;;

  }



  dimension_group: stock {

    type: time

    timeframes: [

      raw,

      time,

      date,

      week,

      month,

      quarter,

      year

    ]

    sql: CAST(${TABLE}.stock_date AS TIMESTAMP) ;;

  }



  dimension_group: stock_date_ts {

    type: time

    timeframes: [

      raw,

      time,

      date,

      week,

      month,

      quarter,

      year

    ]

    sql: ${TABLE}.stock_date_ts ;;

  }



  dimension: subclass_name {

    type: string

    label: "Product Subclass"

    group_label: "Product"

    sql: ${TABLE}.subclass_name ;;

  }



  dimension: unit_cost {

    type: number

    hidden: yes

    sql: ${TABLE}.unit_cost ;;

  }



  dimension: unit_selling_price_usd {

    type: number

    hidden:yes

    sql: ${TABLE}.UnitSellingPriceUSD ;;

  }



  dimension_group: w_insert_dt {

    type: time

    hidden: yes

    timeframes: [

      raw,

      time,

      date,

      week,

      month,

      quarter,

      year

    ]

    sql: CAST(${TABLE}.w_insert_dt AS TIMESTAMP) ;;

  }



  dimension_group: w_update_dt {

    type: time

    hidden: yes

    timeframes: [

      raw,

      time,

      date,

      week,

      month,

      quarter,

      year

    ]

    sql: CAST(${TABLE}.w_update_dt AS TIMESTAMP) ;;

  }



  measure: count {

    type: count

    drill_fields: [detail*]

  }



  # ----- Sets of fields for drilling ------

  set: detail {

    fields: [

      desc_storename,

      desc_areaname,

      class_name,

      desc_regionname,

      atr_mallname,

      subclass_name

    ]

  }

  measure: gmv_usd {

    label: "Revenue Gross USD"

    type: sum

    value_format:"$#.00;($#.00)"

    sql: ${TABLE}.SalesAmtUSD ;;

  }

  measure: qty_sold {

    label: "Quantity Sold"

    type: sum

    sql: ${TABLE}.Sale_STY ;;

  }

  measure: active_sku_count {

    label: "Active SKUs Count"

    type: count_distinct

    sql:

       CASE WHEN ${TABLE}.status = 'A'

       THEN ${TABLE}.prod_num

       ELSE NULL

       END ;;

    }

    measure: in_stock_sku_count {

      label: "In Stock SKUs Count"

      type: count_distinct

      sql:

       CASE WHEN ${TABLE}.status = 'A' AND ${TABLE}.inv_soh_qty > 0

       THEN ${TABLE}.prod_num

       ELSE NULL

       END ;;

      }

      measure: product_availabilty {

        value_format: "0.00%"

        type: number

        sql: SAFE_DIVIDE(${in_stock_sku_count},${active_sku_count}) ;;

      }

      measure: stock_value {

        type: sum

        value_format:"$#.00;($#.00)"

        sql: ${TABLE}.inv_soh_qty *${unit_cost} ;;

      }

      measure: avg_unit_cost {

        type: average

        value_format:"$#.00;($#.00)"

        sql: ${unit_cost} ;;

      }





      measure: Active_SKU_SPAN {

        label: "Active SKUs SPAN"

        type: number

        sql:  COUNT(CASE WHEN status='A' and first_received is not null and DATE(CAST(first_received  AS TIMESTAMP)) <  DATE(Stock_Date_TS) THEN ${prod_num}  END ) ;;

      }





      measure: InStock_SKU_SPAN {

        label: "In Stock SKUs SPAN"

        type: number

        sql: COUNT(CASE WHEN status='A' and inv_soh_qty > 0 and first_received is not null and DATE(CAST(first_received  AS TIMESTAMP)) <  DATE(Stock_Date_TS) THEN ${prod_num} END ) ;;

      }





      measure: OSA {

        value_format: "0.00%"

        drill_fields: [prod_num, org_num, OSA]

        type: number

        sql: SAFE_DIVIDE(${InStock_SKU_SPAN},${Active_SKU_SPAN}) ;;



      }





      measure: AvgSales {

        type: average

        value_format:"$#.00;($#.00)"

        sql: ${sales_amt_usd};;

      }



      measure: AvgSalesWhenInStock {

        type: number

        value_format:"$#.00;($#.00)"

        sql: ${SaleProbabilty}*${AvgSales} ;;

      }



      measure: koko {

        type: number

        value_format:"$#.00;($#.00)"

        sql: AVG(CASE WHEN ${TABLE}.SalesAmtUSD > 0  and ${TABLE}.inv_soh_qty > 0 THEN ${TABLE}.SalesAmtUSD END ) ;;

      }



      measure: kokoSUM {

        type: number

        value_format:"$#.00;($#.00)"

        sql: SUM(CASE WHEN ${TABLE}.SalesAmtUSD > 0  and ${TABLE}.inv_soh_qty > 0 THEN ${TABLE}.SalesAmtUSD END ) ;;

      }



      measure: AvailableAndSelling{

        type: number

        sql: COUNT(CASE WHEN ${TABLE}.SalesAmtUSD > 0  and ${TABLE}.inv_soh_qty > 0 THEN ${TABLE}.prod_num END );;

      }



      measure: AvailableWhetherSellingORNot{

        type: number

        sql: COUNT(CASE WHEN ${TABLE}.inv_soh_qty > 0 THEN ${TABLE}.prod_num END );;

      }







      measure: SaleProbabilty {

        value_format: "0.00%"

        type: number

        sql: CASE WHEN${AvailableWhetherSellingORNot} = 0

                 THEN 0

                 ELSE ${AvailableAndSelling} / ${AvailableWhetherSellingORNot}

                 END ;;



        }





        measure: TotalSales {

          label: "Total Sales"

          type: sum

          value_format:"$#.00;($#.00)"

          sql: ${TABLE}.SalesAmtUSD ;;

        }



        measure: TotalSales2 {

          label: "Total Sales2"

          type: number

          value_format:"$#.00;($#.00)"

          sql: sum(${sales_amt_usd})  ;;

        }


      }
