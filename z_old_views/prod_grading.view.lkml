view: prod_grading {
  derived_table: {
    explore_source: fact_soh_sales_poc {
      column: prod_num {}
      column: is_novelty {}
      column: store { field: dim_retail_loc.store }
      column: total_sales_amt_usd {}
      column: total_sales_amt_usd_over_percent_rank_store {}
      filters: {
        field: fact_soh_sales_poc.stock_date
#         value: "14 months ago for 12 months"
        value: "last 7 days"
      }
      sort: {
        desc: yes
        field: total_sales_amt_usd
      }
    }
  }

  dimension: pk {
    primary_key: yes
    hidden: yes
    sql: CONCAT(${store},' - ',${prod_num}) ;;
  }

  dimension: prod_num {
    hidden: yes
  }

  dimension: store {
    hidden: yes
  }

  dimension: is_novelty {
    hidden: yes
    type: string
    sql: ${TABLE}.is_novelty ;;
  }

  dimension: total_sales_amt_usd {
    hidden: yes
    value_format_name: usd
    type: number
    sql: ${TABLE}.total_sales_amt_usd ;;
  }

  dimension: total_sales_amt_usd_over_percent_rank_store {
    hidden: yes
    value_format_name: decimal_2
    type: number
    sql: CAST(${TABLE}.total_sales_amt_usd_over_percent_rank_store AS FLOAT64) ;;
  }

  dimension: grading {
    view_label: "Sales and Stock Information"
    sql:
      CASE
        WHEN ${total_sales_amt_usd_over_percent_rank_store} > 0.3 OR ${is_novelty} THEN 'A'
        WHEN ${total_sales_amt_usd_over_percent_rank_store} > 0.1 THEN 'B'
        WHEN ${total_sales_amt_usd_over_percent_rank_store} > 0 THEN 'C'
        ELSE 'C'
      END
    ;;
  }

  measure: total_sales_value {
    hidden: yes
    type: sum
    sql: ${total_sales_amt_usd} ;;
  }

}
