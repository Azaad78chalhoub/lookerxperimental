connection: "chb-prod-supplychain-data"
include: "../group_supply_chain_views/*"
include: "../_common/*.view"
include: "../_common/dim_suggest/*.view"
include: "../sensitive_data_views/*.view.lkml"
include: "../group_supply_chain_tests/test_hist_values.lkml"
include: "../group_supply_chain_ext/*.view"
include: "../finance_views/*.view"
week_start_day: sunday
label: "Group Supply Chain"

explore: sell_through_on_receipts {
  label: "Good Receipt Notes (BETA)"
  view_label: "Good Receipt Notes"
  hidden: yes

  always_filter: {
    filters: {
      field: receipt_season
      value: "after 2019/01/01"
    }
    filters: {
      field: sales_season
      value: "after 2019/01/01"
    }

  }

 join: receipt_running_sums{
  type: left_outer
  sql_on: ${sell_through_on_receipts.date} = ${receipt_running_sums.date}
  AND ${sell_through_on_receipts.location} = ${receipt_running_sums.location}
  AND ${sell_through_on_receipts.item} = ${receipt_running_sums.item};;
  relationship: one_to_one
  }

  join: dm_soh_sales_wac_rsp {
    type: left_outer
    sql_on: ${sell_through_on_receipts.date}=${dm_soh_sales_wac_rsp.stock_date}
    AND ${sell_through_on_receipts.item} = ${dm_soh_sales_wac_rsp.prod_num}
    AND ${sell_through_on_receipts.location}=${dm_soh_sales_wac_rsp.org_num};;
    relationship: one_to_one
  }

  join: dim_item_loc_traits {
    type: left_outer
    sql_on:
     ${sell_through_on_receipts.item} = ${dim_item_loc_traits.item}
    AND CAST(${sell_through_on_receipts.location} AS STRING) = CAST(${dim_item_loc_traits.loc} AS STRING);;
    relationship: one_to_one
  }

  join: ch_trait_attribute{
    type: left_outer
    sql_on:
     ${sell_through_on_receipts.item} = ${dim_item_loc_traits.item}
     AND  ${sell_through_on_receipts.location} = ${dim_item_loc_traits.loc}
    AND ${dim_item_loc_traits.report_code}=${ch_trait_attribute.code};;
    relationship: one_to_many
  }

  join: dim_retail_product {
    type: left_outer
    sql_on:
    ${sell_through_on_receipts.item} = ${dim_retail_product.item};;
    relationship: many_to_one
  }

  join: dim_bu_cdl {
    type: left_outer
    sql_on:
      ${sell_through_on_receipts.item} = ${dim_bu_cdl.prod_num}
      AND
      ${sell_through_on_receipts.location} = ${dim_bu_cdl.org_num};;
    relationship: many_to_one
  }

  join: dim_alternate_bu_hierarchy {
    type: left_outer
    sql_on:
    ${dim_bu_cdl.business_unit_final} = ${dim_alternate_bu_hierarchy.bu_code};;
    relationship: many_to_one
  }

# -----------------------BR76 fix starts -----------------------------
  # join: ch_concession {
  #   type: left_outer
  #   sql_on:
  #   ${dim_retail_product.class} = ${ch_concession.class}
  #   AND
  #   ${dim_retail_product.dept_no} = ${ch_concession.dept}
  #   AND
  #   ${sell_through_on_receipts.location} = ${ch_concession.store};;
  #   relationship: many_to_many
  # }

  join: dim_prod_org_concession {
    type: left_outer
    sql_on:
    ${dim_retail_product.item} = ${dim_prod_org_concession.prod_num}
    AND
    ${sell_through_on_receipts.location} = ${dim_prod_org_concession.org_num};;
    relationship: many_to_many
  }
 # -----------------------BR76 fix starts -----------------------------



}
explore: rtv_head_rms {
  label: "Return to Vendor"

  join: rtv_detail_rms {
    relationship: many_to_one
    type: left_outer
    sql_on: ${rtv_head_rms.rtv_order_no} = ${rtv_detail_rms.rtv_order_no} ;;
  }
  join: dim_retail_loc {
    relationship: many_to_one
    type: left_outer
    sql_on: ${rtv_head_rms.store} = ${dim_retail_loc.store} ;;
  }


 }

explore: inv_adj {
  label: "INV ADJ (BETA)"

  join:  inv_adj_reason{
    relationship: many_to_one
    type: left_outer
    sql_on: ${inv_adj.reason}=${inv_adj_reason.reason} ;;
  }
join: dm_soh_sales_inv {
  relationship: many_to_one
  type: left_outer
  sql_on: ${inv_adj.location} = ${dm_soh_sales_inv.loc_code}
  and ${inv_adj.item} = ${dm_soh_sales_inv.item}
  and ${inv_adj.adj_date} = ${dm_soh_sales_inv.stock_date} ;;
}
  join: dim_retail_product{
    view_label: "VPN and Stock"
    relationship: many_to_one
    type: left_outer
    sql_on: ${inv_adj.item} = ${dim_retail_product.item};;
  }

}
explore: baan_sales {
  label: "BAAN Sales & Stock (BETA)"
  view_label: "Sales"

  join: baan_stock {
    view_label: "Stock"
    relationship: many_to_one
    type: full_outer
    sql_on: ${baan_sales.item} = ${baan_stock.item};;
  }
  join: dim_retail_product{
    view_label: "VPN and Stock"
    relationship: many_to_one
    type: left_outer
    sql_on: ${dim_retail_product.vpn} = ${baan_stock.item};;
  }
}

explore: dm_soh_sales_weekly {
  label: "Sales and Stock Weekly (BETA)"
  view_label: "Sales and Stock Weekly"

  always_filter: {

    filters: {
      field: vertical
      value: "{{ _user_attributes['vertical'] }}"
    }


    filters: {
      field: business_unit
      value: "{{ _user_attributes['brand'] }}"
    }

    filters: {
      field: country_code
      value: "{{ _user_attributes['country'] }}"
    }




}
  access_filter: {
    field: country_code
    user_attribute: country
  }
  access_filter: {
    field: vertical
    user_attribute: vertical
  }
  access_filter: {
    field: business_unit
    user_attribute: brand
  }



}

explore: dm_provision_vs_sales {
  label: "Provision Performance"
  view_label: "Transactions from provision product"
  hidden: yes

  always_filter: {

    filters: {
      field: dim_alternate_bu_hierarchy.brand
      value: "{{ _user_attributes['brand'] }}"
    }



    filters: {
      field: purchase_date_month
      value: "2017-01-01 to 2020-01-01"
    }
  }

  access_filter: {
    field: dim_alternate_bu_hierarchy.country
    user_attribute: country
  }
  access_filter: {
    field: dim_alternate_bu_hierarchy.vertical
    user_attribute: vertical
  }
  access_filter: {
    field: dim_alternate_bu_hierarchy.brand
    user_attribute: brand
  }
  join: udas {
    type: left_outer
    sql_on:
          ${dm_provision_vs_sales.prod_num} = ${udas.item};;
    relationship: many_to_one
  }
  join: dim_retail_product {
    type: left_outer
    sql_on:
      ${dm_provision_vs_sales.prod_num} = ${dim_retail_product.item};;
    relationship: many_to_one
  }

  join: udas2 {
    type: left_outer
    sql_on:
           ${dm_provision_vs_sales.prod_num} = ${udas2.item};;
    relationship: many_to_one
  }
  join: dim_bu_cdl {
    type: left_outer
    sql_on:
      ${dim_bu_cdl.prod_num} =  ${dm_provision_vs_sales.prod_num}
      AND
      ${dim_bu_cdl.org_num} = ${dm_provision_vs_sales.org_num} ;;
    relationship: many_to_one
  }
  join: dim_alternate_bu_hierarchy {
    type: left_outer
    sql_on:
      ${dim_bu_cdl.business_unit_final} = ${dim_alternate_bu_hierarchy.bu_code};;
    relationship: one_to_one
  }

# adding cast to fix the issue in Location name
  join: dim_retail_loc {
    type: left_outer
    sql_on:
    ${dm_provision_vs_sales.org_num}= cast (${dim_retail_loc.store} as string);;
    relationship: many_to_one
  }
  join: dim_item_loc_traits {
    type: left_outer
    sql_on: ${dim_item_loc_traits.item} = ${dm_provision_vs_sales.prod_num}
      and ${dim_item_loc_traits.loc} = ${dm_provision_vs_sales.org_num};;
    relationship: many_to_one
  }

  join: dim_distr_ret_loc {
    type: left_outer
    sql_on:
    ${dm_provision_vs_sales.org_num}= cast (${dim_distr_ret_loc.loc_code} as string);;
    relationship: many_to_one
  }


}


explore: fifo_transactions {
  label: "Sell Through % Transactions"
  view_label: "Transactions"
  hidden: yes

  always_filter: {
    filters: {
      field: season
      value: "2016-12-01 to 2017-05-01"
    }
    filters: {
      field: sales_season
      value: "2017-01-01 to 2019-01-01"
    }
    filters: {
      field: Create_date
      value: "2017-07-01 to 2019-01-01"
    }

    filters: {
      field: bu_brand
      value: "SWAROVSKI"
    }

    filters: {
      field: deal_transfer
      value: "no"
    }


    filters: {
      field: udas2.uda_value_desc
      value: "-BASIC,-REGULAR"
    }
  }


  join: dim_retail_product {
    type: left_outer
    sql_on:
      ${fifo_transactions.item} = ${dim_retail_product.item};;
    relationship: many_to_one
  }

  join: dim_retail_loc {
    type: left_outer
    sql_on:
    ${fifo_transactions.bk_storeid} = ${dim_retail_loc.store};;
    relationship: many_to_one
  }


  join: fifo_transactions_orders {
    type: left_outer
    sql_on:
          ${fifo_transactions.item} = ${fifo_transactions_orders.bk_productid}
          ;;
    relationship: many_to_one
  }

  join: udas {
    type: left_outer
    sql_on:
          ${fifo_transactions.item} = ${udas.item};;
    relationship: many_to_one
  }

  join: udas2 {
    type: left_outer
    sql_on:
          ${fifo_transactions.item} = ${udas2.item};;
    relationship: many_to_one
  }



}

explore: carolina_herrera_sales_at_time_interval {
  label: "Carolina Herrera Sales"
  view_label: "Sales Data"

  always_filter: {
    filters: {
      field: business_date
      value: "Last 3 months"

    }
    filters: {
      field: carolina_herrera_store_master.store_name

    }
  }


  join: carolina_herrera_category_mapping {
    type: left_outer
    sql_on: cast(${carolina_herrera_sales_at_time_interval.ref_code2} as string) = ${carolina_herrera_category_mapping.reference_code} ;;
    relationship: many_to_one
  }

  join: carolina_herrera_handbags_mapping {
    type: left_outer
    sql_on: cast(${carolina_herrera_sales_at_time_interval.sku_externalcode} as string)=cast(${carolina_herrera_handbags_mapping.ref_code_wt_size} as string) ;;
    relationship: many_to_one
  }

  join: carolina_herrera_store_master {
    type: left_outer
    sql_on: cast(${carolina_herrera_sales_at_time_interval.store} as string) = cast(${carolina_herrera_store_master.ilion_store_id} as string);;
    relationship: many_to_one
  }

  join: carolina_herrera_staff_ilion_codes {
    type: left_outer
    sql_on: cast(${carolina_herrera_sales_at_time_interval.idhr} as string) = cast(${carolina_herrera_staff_ilion_codes.ilion_code} as string);;
    relationship: many_to_one
  }

  join: carolina_herrera_prices_quotation_at_given_date {
    type: left_outer
    sql_on: cast(${carolina_herrera_sales_at_time_interval.store} as string) = cast(${carolina_herrera_prices_quotation_at_given_date.store} as string) and
      cast(${carolina_herrera_sales_at_time_interval.sku_externalcode} as string) = cast(${carolina_herrera_prices_quotation_at_given_date.sku_code} as string);;
  }

  join: ch_product_grading {
    type: left_outer
    sql_on: cast(${carolina_herrera_sales_at_time_interval.sku_externalcode} as string) = cast(${ch_product_grading.sku_externalcode} as string) ;;
  }

  }

explore: carolina_herrera_theoretical_stock {
  label: "Carolina Herrera Stock"
  view_label: "Stock Details"

  always_filter: {

    filters: {
      field: carolina_herrera_store_master.store_name

    }
    filters: {
      field: stock_date
      value: "yesterday"

    }
  }

  join: carolina_herrera_category_mapping {
    type: left_outer
    sql_on: cast(${carolina_herrera_theoretical_stock.ref_code2} as string) = cast(${carolina_herrera_category_mapping.reference_code} as string);;
    relationship: many_to_one
  }
  join: carolina_herrera_handbags_mapping {
    type: left_outer
    sql_on: cast(${carolina_herrera_theoretical_stock.sku_externalcode} as string)=cast(${carolina_herrera_handbags_mapping.ref_code_wt_size} as string) ;;
    relationship: many_to_one
  }

  join: carolina_herrera_store_master {
    type: left_outer
    sql_on: cast(${carolina_herrera_theoretical_stock.store} as string) = cast(${carolina_herrera_store_master.ilion_store_id} as string);;
    relationship: many_to_one
  }

  join: carolina_herrera_prices_quotation_at_given_date {
    type: left_outer
    sql_on: cast(${carolina_herrera_theoretical_stock.store} as string) = cast(${carolina_herrera_prices_quotation_at_given_date.store} as string) and
            cast(${carolina_herrera_theoretical_stock.sku_externalcode} as string) = cast(${carolina_herrera_prices_quotation_at_given_date.sku_code} as string);;
  }

  join: out_of_stock_days_bucket {
    type: left_outer
    sql_on: cast(${carolina_herrera_store_master.store_cd_name} as string) = cast(${out_of_stock_days_bucket.carolina_herrera_store_master_store_cd_name} as string) and
            cast(${carolina_herrera_theoretical_stock.sku_externalcode} as string) = cast(${out_of_stock_days_bucket.carolina_herrera_theoretical_stock_sku_externalcode} as string);;
  }

  join: ch_product_grading {
    type: left_outer
    sql_on: cast(${carolina_herrera_theoretical_stock.sku_externalcode} as string) = cast(${ch_product_grading.sku_externalcode} as string) ;;
  }

  join: pg_product_grading {
    type: left_outer
    sql_on: cast(${carolina_herrera_theoretical_stock.sku_externalcode} as string) = cast(${pg_product_grading.sku_externalcode} as string) ;;
  }

  join: carolina_herrera_latest_stock_availability_date {
    type: left_outer
    sql_on:cast(${carolina_herrera_theoretical_stock.store} as string) = cast(${carolina_herrera_latest_stock_availability_date.store} as string) and
            cast(${carolina_herrera_theoretical_stock.sku_externalcode} as string) = cast(${carolina_herrera_latest_stock_availability_date.sku_externalcode} as string)  ;;
  }

  join: carolina_herrera_ytd_sales {
    type: left_outer
    sql_on: cast(${carolina_herrera_theoretical_stock.store} as string) = cast(${carolina_herrera_ytd_sales.store} as string) and
            cast(${carolina_herrera_theoretical_stock.sku_externalcode} as string) = cast(${carolina_herrera_ytd_sales.sku_externalcode} as string)  ;;
  }

  join: sellthrough_ch_pg {
    type: left_outer
    sql_on: cast(${carolina_herrera_theoretical_stock.store} as string) = cast(${sellthrough_ch_pg.store} as string) and
            cast(${carolina_herrera_theoretical_stock.sku_externalcode} as string) = cast(${sellthrough_ch_pg.sku_externalcode} as string) ;;
  }

  join: carolina_herrera_first_received_date {
    type: left_outer
    sql_on: cast(${carolina_herrera_theoretical_stock.store} as string) = cast(${carolina_herrera_first_received_date.store} as string) and
      cast(${carolina_herrera_theoretical_stock.sku_externalcode} as string) = cast(${carolina_herrera_first_received_date.sku_externalcode} as string)  ;;
  }

}


explore: dm_supply_orders_detailed {
  label: "Purchase Orders (BETA)"
  view_label: "Orders"
  hidden: no
  access_filter: {
    field: country_for_access_filter
    user_attribute: country
  }
  access_filter: {
    field: vertical_for_access_filter
    user_attribute: vertical
  }
  access_filter: {
    field: brand_for_access_filter
    user_attribute: brand
  }


  join: dm_supply_orders_last_day {
    type: left_outer
    sql_on:
         cast(${dm_supply_orders_detailed.loc_code} AS string)  = ${dm_supply_orders_last_day.org_num}
          AND
        cast(  ${dm_supply_orders_detailed.item}  AS string)= ${dm_supply_orders_last_day.prod_num}
          ;;
    relationship: many_to_one
  }

  join: awac_new {
    type: left_outer
    sql_on:
          cast(${dm_supply_orders_detailed.loc_code} AS string)= ${awac_new.loc_code}
           AND
          cast(${dm_supply_orders_detailed.item} AS string) = ${awac_new.item}

          ;;
    relationship: many_to_one
  }

  join: dm_receipts_detailed_all {
    type: left_outer
    sql_on:
          cast(${dm_supply_orders_detailed.order_no} AS string)= ${dm_receipts_detailed_all.order_no}
          AND
          cast(${dm_supply_orders_detailed.item} AS string) = ${dm_receipts_detailed_all.item}
          AND
          cast(${dm_supply_orders_detailed.loc_code} AS string) = ${dm_receipts_detailed_all.location}
          ;;
    relationship: one_to_many
  }



}

explore: sell_through_on_date{
  label: "Sell Through on PO and FIFO"
  view_label: "Orders"
  hidden: yes

  always_filter: {
    filters: {
      field: season
      value: "2016-12-01 to 2017-05-01"
    }
    filters: {
      field: sales_season
      value: "2017-01-01 to 2019-01-01"
    }
    filters: {
      field: exclude_previously_purchased
      value: "no"
    }
    filters: {
      field: bu_brand
      value: "SWAROVSKI"
    }
    filters: {
      field: udas2.uda_value_desc
      value: "-BASIC,-REGULAR"
    }


  }

  join: dim_retail_product {
    type: left_outer
    sql_on:
      ${sell_through_on_date.item} = ${dim_retail_product.item};;
    relationship: one_to_many
  }

  join: sell_through_on_date_cost {
    type: left_outer
    sql_on:
      ${sell_through_on_date.item} = ${sell_through_on_date_cost.item};;
    relationship: many_to_one
  }

  join: udas {
    type: left_outer
    sql_on:
    ${sell_through_on_date.item} = ${udas.item};;
    relationship: many_to_one
  }

  join: udas2 {
    type: left_outer
    sql_on:
    ${sell_through_on_date.item} = ${udas2.item};;
    relationship: many_to_one
  }


}

# explore: orders {
#   label: "Purchase Orders"
#   view_label: "Orders"
#
#   access_filter: {
#     field: dim_alternate_bu_hierarchy.country
#     user_attribute: country
#   }
#   access_filter: {
#     field: dim_alternate_bu_hierarchy.vertical
#     user_attribute: vertical
#   }
#   access_filter: {
#     field: dim_alternate_bu_hierarchy.brand
#     user_attribute: brand
#   }
#
#   join: cost_facts {
#     type: left_outer
#     sql_on:
#           ${orders.written_date} = ${cost_facts.stock_date_raw}
#           AND
#           CAST(${orders.location} AS STRING) = ${cost_facts.org_num}
#           AND
#           ${orders.item} = ${cost_facts.prod_num}
#           ;;
#     relationship: many_to_one
#   }
#
#   join: awac {
#     type: left_outer
#     sql_on:
#           CAST(${orders.location} AS STRING) = ${awac.org_num}
#           AND
#           ${orders.item} = ${awac.prod_num}
#           ;;
#     relationship: many_to_one
#   }
#
#   join: sups {
#     view_label: "Suppliers"
#     type: left_outer
#     sql_on:
#           ${orders.supplier} = ${sups.supplier}
#           ;;
#     relationship: many_to_one
#   }
#
#
#
#   join: dim_bu_cdl {
#     type: left_outer
#     sql_on:
#       ${dim_bu_cdl.prod_num} = ${orders.item}
#       AND
#       ${dim_bu_cdl.org_num} = CAST(${orders.location} AS STRING);;
#     relationship: one_to_many
#   }
#
#   join: dim_alternate_bu_hierarchy {
#     type: left_outer
#     sql_on:
#       ${dim_bu_cdl.business_unit_final} = ${dim_alternate_bu_hierarchy.bu_code};;
#     relationship: one_to_many
#   }
#   join: dim_retail_product {
#     type: left_outer
#     sql_on:
#       ${dim_retail_product.item} = ${orders.item};;
#     relationship: one_to_many
#   }
#
# #   join: dim_retail_loc {
# #     type: left_outer
# #     sql_on:
# #     ${orders.location}= ${dim_retail_loc.store};;
# #     relationship: many_to_one
# #   }
#
#
#   join: dim_distr_ret_loc {
#     type: left_outer
#     sql_on:
#           ${orders.location}= ${dim_distr_ret_loc.loc_code};;
#     relationship: many_to_one
#     fields: [dim_distr_ret_loc.loc_code, dim_distr_ret_loc.loc_name, dim_distr_ret_loc.region_name, dim_distr_ret_loc.district_name, dim_distr_ret_loc.chain_name ,dim_distr_ret_loc.store_close_date,dim_distr_ret_loc.store_close_month,dim_distr_ret_loc.store_close_quarter,dim_distr_ret_loc.store_close_raw ,
#       dim_distr_ret_loc.store_close_time,dim_distr_ret_loc.store_close_week,dim_distr_ret_loc.store_close_year]
#   }
#
#
#
#   join: dim_item_loc_traits {
#     type: left_outer
#     sql_on:
#     ${dim_item_loc_traits.item} = ${orders.item}
#     AND ${dim_item_loc_traits.loc} =  ${orders.location};;
#     relationship: one_to_one
#   }
#
#   join: dm_supply_orders_last_day {
#     type: left_outer
#     sql_on:
#          cast(${orders.location} AS string)  = ${dm_supply_orders_last_day.org_num}
#           AND
#         cast(  ${orders.item}  AS string)= ${dm_supply_orders_last_day.prod_num}
#           ;;
#     relationship: many_to_one
#   }
#
#
#
#
# }


explore: dm_age_provision {
  label: "Provisions"
  view_label: "Provisions"
  always_filter: {
    filters: {
      field: param_prov_month_daily

    }
    filters: {
      field: date_date
      value: "Last 1 months"
    }
  }

  access_filter: {
    field: country_for_access_filter
    user_attribute: country
  }
  access_filter: {
    field: vertical_for_access_filter
    user_attribute: vertical
  }
  access_filter: {
    field: business_unit_for_access_filter
    user_attribute: brand
  }

  join: new_provision_last_date {
    type: left_outer
    sql_on:
    ${new_provision_last_date.last_date} = ${dm_age_provision.date_date}
    ;;
    relationship: one_to_one
  }

  # join: dim_item_loc_traits {
  #   type: left_outer
  #   sql_on:
  #   CAST(${dim_item_loc_traits.item} AS STRING) = ${dm_age_provision.prod_num}
  #   AND
  #   CAST(${dim_item_loc_traits.loc} AS STRING) =  ${dm_age_provision.org_num};;
  #   relationship: one_to_one
  # }

  join: dim_retail_product {
    type: left_outer
    sql_on:
    ${dm_age_provision.prod_num} = ${dim_retail_product.item};;
    relationship: many_to_one
  }

  join: udas {
    type: left_outer
    sql_on:
          ${dm_age_provision.prod_num} = ${udas.item};;
    relationship: many_to_one
  }

  join: udas2 {
    type: left_outer
    sql_on:
         ${dm_age_provision.prod_num}  = ${udas2.item};;
    relationship: many_to_one
  }

  join: item_hts {
    type: left_outer
    sql_on:
    ${dim_retail_product.item}= ${item_hts.item};;
    relationship: one_to_many
  }

}

explore: new_reversals {

  access_filter: {
    field: country_access_filter
    user_attribute: country
  }
  access_filter: {
    field: vertical_access_filter
    user_attribute: vertical
  }
  access_filter: {
    field: business_unit_access_filter
    user_attribute: brand
  }

  label: "Reversals"
  view_label: "Reversals"


#   join: dim_retail_product {
#     type: left_outer
#     sql_on:
#       ${dim_retail_product.item} = ${new_reversals.prod_num};;
#     relationship: one_to_many
#   }

#   join: dim_bu_cdl {
#     type: left_outer
#     sql_on:
#       ${dim_bu_cdl.prod_num} = ${new_reversals.prod_num}
#       AND
#       ${dim_bu_cdl.org_num} = CAST(${new_reversals.org_num} AS STRING);;
#     relationship: one_to_many
#   }

#   join: dim_alternate_bu_hierarchy {
#     type: left_outer
#     sql_on:
#       ${dim_bu_cdl.business_unit_final} = ${dim_alternate_bu_hierarchy.bu_code};;
#     relationship: one_to_many
#   }

# # adding cast to fix the issue in Location name
#   join: dim_retail_loc {
#     type: left_outer
#     sql_on:
#     ${new_reversals.org_num}= cast (${dim_retail_loc.store} as string);;
#     relationship: many_to_one
#   }




}

explore: order_sums {
  label: "Sell Throughs"
  view_label: "Sell Throughs"
  always_filter: {
    filters: {
      field: season
      value: "after 2019/03/01"
    }
    filters: {
      field: sales_season
      value: "after 2019/10/01"
    }

  }

  access_filter: {
    field: dim_alternate_bu_hierarchy.country
    user_attribute: country
  }
  access_filter: {
    field: dim_alternate_bu_hierarchy.vertical
    user_attribute: vertical
  }
  access_filter: {
    field: dim_alternate_bu_hierarchy.brand
    user_attribute: brand
  }

  join: targets_level_zone {
    type: left_outer
    sql_on:
      ${order_sums.yearno} =  ${targets_level_zone.year}
      AND
      ${order_sums.weekno} = ${targets_level_zone.weeknum}
      AND
      ${dim_item_loc_traits.report_code} = ${targets_level_zone.zone}
      AND
      ${dim_item_loc_traits.alt_storeage_location_bucket} = ${targets_level_zone.loc_season}
;;
    relationship: many_to_many
  }


  join: targets_level_brand {
    type: left_outer
    sql_on:
      ${order_sums.yearno} =  ${targets_level_brand.year}
      AND
      ${order_sums.weekno} = ${targets_level_brand.weeknum}
      AND
      ${dim_retail_product.brand} = ${targets_level_brand.brand}
      AND
      ${dim_item_loc_traits.report_code} = ${targets_level_brand.zone}
      AND
      ${dim_item_loc_traits.alt_storeage_location_bucket} = ${targets_level_zone.loc_season}
      AND
      ${dim_alternate_bu_hierarchy.brand} = ${targets_level_brand.bu_code};;
    relationship: many_to_many
  }

  join: dim_retail_product {
    type: left_outer
    sql_on:
      ${order_sums.item} = ${dim_retail_product.item} ;;
    relationship: many_to_one
  }

  join: dim_bu_cdl {
    type: left_outer
    sql_on:
    ${order_sums.item} = ${dim_bu_cdl.prod_num}
    AND
    CAST(${order_sums.location} AS STRING) = ${dim_bu_cdl.org_num};;
    relationship: many_to_one
  }

  join: dim_alternate_bu_hierarchy {
    type: left_outer
    sql_on:
      ${dim_bu_cdl.business_unit_final} = ${dim_alternate_bu_hierarchy.bu_code};;
    relationship: many_to_one
  }

  join: dim_retail_loc {
    type: left_outer
    sql_on:
    ${order_sums.location}= ${dim_retail_loc.store};;
    relationship: many_to_one
  }

  join: dim_distr_ret_loc {
    type: left_outer
    sql_on:
    ${order_sums.location}= ${dim_distr_ret_loc.loc_code};;
    relationship: many_to_one
  }

  join: dim_item_loc_traits {
    type: left_outer
    sql_on:
    ${dim_item_loc_traits.item} = ${order_sums.item}
    AND ${dim_item_loc_traits.loc} =  ${order_sums.location};;
    relationship: one_to_one
  }

  join: ch_trait_attribute{
    type: left_outer
    sql_on:
    ${dim_item_loc_traits.item} = ${order_sums.item}
    AND ${dim_item_loc_traits.loc} =  ${order_sums.location}
    AND ${dim_item_loc_traits.report_code}=${ch_trait_attribute.code};;
    relationship: one_to_many
  }

  # -----------------------------BR76 fix starts---------------------------------
  # join: ch_concession {
  #   type: left_outer
  #   sql_on:
  #   ${dim_retail_product.class} = ${ch_concession.class}
  #   AND
  #   ${dim_retail_product.dept_no} = ${ch_concession.dept}
  #   AND
  #   ${order_sums.location} = ${ch_concession.store};;
  #   relationship: many_to_many
  # }

  join: dim_prod_org_concession {
    type: left_outer
    sql_on:
    ${dim_retail_product.item} = ${dim_prod_org_concession.prod_num}
    AND
    cast(${order_sums.location} as string) = ${dim_prod_org_concession.org_num};;
    relationship: many_to_many
  }
  # -----------------------------BR76 fix ends ---------------------------------

}


explore: level_local_targets {
  extension: required
}



explore: fact_retail_sales {
  extends: [level_local_targets]
  always_filter: {
    filters: {
      field: business_date
      value: "Last 1 months"
    }

    filters: {
      field: dim_alternate_bu_hierarchy.brand
      value: "{{ _user_attributes['brand'] }}"
    }

  }
  access_filter: {
    field: dim_alternate_bu_hierarchy.country
    user_attribute: country
  }
  access_filter: {
    field: dim_alternate_bu_hierarchy.vertical
    user_attribute: vertical
  }
  access_filter: {
    field: dim_alternate_bu_hierarchy.brand
    user_attribute: brand
  }
  label: "Transaction Analysis"
  view_label: "Sales Information"

  join: dim_retail_product {
    type: left_outer
    sql_on:
    ${fact_retail_sales.bk_productid} = ${dim_retail_product.item};;
    relationship: many_to_one
  }

  join: fiscal_dates {
    view_label: "Fiscal Dates"
    type: left_outer
    sql_on:
          ${fact_retail_sales.business_date}= ${fiscal_dates.calendar_date};;
    relationship: one_to_one
  }

  # join: tryano_ranks {
  #   type: left_outer
  #   sql_on:
  #   ${ch_trait_attribute.code_desc}= ${tryano_ranks.code_desc}
  #   AND
  #   ${tryano_ranks.brand} = ${dim_retail_product.brand}
  #   AND
  #   ${ch_concession.concession_product}=${tryano_ranks.concession_product};;
  #   relationship: many_to_one
  # }

  join: item_hts {
    type: left_outer
    sql_on:
    ${dim_retail_product.item}= ${item_hts.item};;
    relationship: one_to_many
  }


 join: budgetstarget {
    type: left_outer
   sql_on:
  ${fact_retail_sales.bk_storeid} = ${budgetstarget.bk_store}
  AND
  ${fact_retail_sales.business_date} = ${budgetstarget.business_date};;
  relationship: many_to_one
}
  join: Yearly_R0_Budget {
    type: left_outer
    sql_on:
      CAST(${fact_retail_sales.bk_storeid} AS STRING) = ${Yearly_R0_Budget.store_code}
      ;;
    relationship: many_to_one
  }
  join: Monthly_R0_Budget {
    type: left_outer
    sql_on:
      CAST(${fact_retail_sales.bk_storeid} AS STRING) = ${Monthly_R0_Budget.store_code}
      AND
      ${fact_retail_sales.month_no} = ${Monthly_R0_Budget.month}
      ;;
    relationship: many_to_one
  }
  join: budgetsretail {
    type: left_outer
    sql_on:
    ${fact_retail_sales.bk_storeid} = ${budgetsretail.bk_store}
    AND
    ${fact_retail_sales.business_date} = ${budgetsretail.business_date};;
    relationship: many_to_one
  }

 join: topN_stores {
   type: left_outer
   sql_on:
   ${fact_retail_sales.bk_storeid} = ${topN_stores.store}
  ;;
  }

  join: retail_transaction_classification {
    type: left_outer
    sql_on:
    cast(${fact_retail_sales.bk_storeid} as string) = cast(${retail_transaction_classification.location_number} as string)
    AND
    cast(${fact_retail_sales.atr_cginvoiceno} as string) = cast(${retail_transaction_classification.invoice_number} as string)
    AND
    ${fact_retail_sales.atr_trandate_date} = ${retail_transaction_classification.business_date};;

  }

  # join: dim_employees {
  #  type: left_outer
  #  sql_on:
  #  ${fact_retail_sales.atr_salesperson} = ${dim_employees.column_0};;
  #  relationship: one_to_many
  #}

  #Added to solve bug BR-68 (LK)
  join: dim_employee_details {
    fields: [dim_employee_details.empcode,
      dim_employee_details.title,
      dim_employee_details.employee_name
    ]
    type: left_outer
    sql_on:
    ${fact_retail_sales.atr_salesperson} = ${dim_employee_details.empcode};;
    relationship: one_to_many
  }

  join: employee_cross_sell {
    type: left_outer
    sql_on:
    ${fact_retail_sales.atr_salesperson} = ${employee_cross_sell.empcode}
    AND ${fact_retail_sales.atr_cginvoiceno}=${employee_cross_sell.atr_cginvoiceno};;
    relationship: many_to_one
  }

  join: dim_conversion_daily_rates {
    type: left_outer
    sql_on:
    ${budgetstarget.business_date} = ${dim_conversion_daily_rates.conversion_date}
    AND
    ${budgetstarget.from_currency} = ${dim_conversion_daily_rates.from_currency}
    AND
    ${budgetstarget.to_currency} = ${dim_conversion_daily_rates.to_currency}
    AND
    ${budgetstarget.conversion_type} = ${dim_conversion_daily_rates.conversion_type}
     ;;
    relationship: many_to_one
  }



  join: levelshoes_budgets_daily {
    type: left_outer
    sql_on:
    ${dim_item_loc_traits.report_code} = ${levelshoes_budgets_daily.zone}
    AND
    ${fact_retail_sales.bk_storeid} = ${levelshoes_budgets_daily.store_code}
    AND
    ${fact_retail_sales.business_month} = ${levelshoes_budgets_daily.date_month}
    AND
    ${fact_retail_sales.business_year} = ${levelshoes_budgets_daily.date_year};;
    relationship: many_to_one
  }

  join: dim_item_loc_traits{
    type: left_outer
    sql_on:
    ${fact_retail_sales.bk_productid} = ${dim_item_loc_traits.item}
     AND   ${fact_retail_sales.bk_storeid} = ${dim_item_loc_traits.loc};;
    relationship: one_to_one
  }
  join: ch_trait_attribute{
    type: left_outer
    sql_on:
     ${fact_retail_sales.bk_productid} = ${dim_item_loc_traits.item}
     AND  ${fact_retail_sales.bk_storeid} = ${dim_item_loc_traits.loc}
    AND ${dim_item_loc_traits.report_code}=${ch_trait_attribute.code};;
    relationship: one_to_many
  }

  # -----------------------------BR76 fix starts ---------------------------------

  #-----Missing from the transaction analysis explore (dimension - store concession)
  # join: ch_concession {
  #   type: left_outer
  #   sql_on:
  #   ${dim_retail_product.class} = ${ch_concession.class}
  #   AND
  #   ${dim_retail_product.dept_no} = ${ch_concession.dept}
  #   AND
  #   ${fact_retail_sales.bk_storeid} = ${ch_concession.store}
  # ;;
  #   relationship: many_to_many
  # }

  join: dim_prod_org_concession {
    type: left_outer
    sql_on:
    ${dim_retail_product.item} = ${dim_prod_org_concession.prod_num}
    AND
    cast(${fact_retail_sales.bk_storeid} as string)  = ${dim_prod_org_concession.org_num};;
    relationship: many_to_many
  }
  # -----------------------------BR76 fix ends ---------------------------------


  join: dim_retail_loc {
    type: left_outer
    sql_on:
    ${fact_retail_sales.bk_storeid} = ${dim_retail_loc.store};;
    relationship: many_to_one
  }

  join: dim_alternate_bu_hierarchy {
    type: left_outer
    sql_on:
    CAST(${fact_retail_sales.bk_storeid} AS STRING) = ${dim_alternate_bu_hierarchy.store_code};;
    relationship: many_to_one
  }

  join: uda_product_theme {
    type: left_outer
    sql_on:
    ${fact_retail_sales.bk_productid} = ${uda_product_theme.item};;
    relationship: many_to_one
  }



# -----------SPD-275 changes to include consignment WAC starts ----------------------------------------------



  join : ch_commission_valid_data {
    type: left_outer
        sql_on: ${dim_retail_product.dept_no} =  ${ch_commission_valid_data.dept}
          and  case when  ${ch_commission_valid_data.class}  is null then 1 else ${dim_retail_product.class} end   = case when  ${ch_commission_valid_data.class}  is null then 1 else  ${ch_commission_valid_data.class}  end
          and  case when  ${ch_commission_valid_data.subclass}  is null then 1 else ${dim_retail_product.subclass} end   = case when  ${ch_commission_valid_data.subclass}  is null then 1 else  ${ch_commission_valid_data.subclass}  end
          and ${fact_retail_sales.bk_storeid}= ${ch_commission_valid_data.store}
          and ${dim_retail_loc.country_id} =  ${ch_commission_valid_data.country}
        ;;
    relationship: many_to_one
  }

  join : ch_supplier_commission_info {
    type: left_outer
    sql_on: ${dim_retail_product.dept_no} = ${ch_supplier_commission_info.dept}
          and ${dim_retail_product.class} = ${ch_supplier_commission_info.class}
          and ${dim_retail_product.subclass} = ${ch_supplier_commission_info.subclass}
          and ${fact_retail_sales.bk_storeid}= ${ch_supplier_commission_info.store};;
    relationship: many_to_one
  }

  join: dim_sups {
    type: left_outer
    sql_on: ${ch_supplier_commission_info.supplier} = ${dim_sups.supplier} ;;
    relationship: many_to_one
  }

# ------------------SPD-275 changes to include consignment WAC ends --------------------------------------

}


explore: +fact_retail_sales {
}


explore:targets_level_zone_2 {
  sql_always_where: ${targets_level_zone_2.date_date} < CURRENT_DATE();;
  label: "Level Targets"
  always_filter: {
    filters: {
      field: fact_level_sales.exclude_concession
      value: "Yes"
    }
    filters: {
      field: fact_level_sales.exclude_consignment
      value: "yes"
    }
  }

  join: fact_level_sales {
    fields: [fact_level_sales.net_sales_amount_local,fact_level_sales.gross_margin_local,fact_level_sales.report_code,fact_level_sales.exclude_consignment,fact_level_sales.exclude_concession ]
    type: full_outer
    sql_on:
    ${targets_level_zone_2.date_date} = ${fact_level_sales.business_date}
    AND
    ${targets_level_zone_2.zone} = ${fact_level_sales.report_code}
    AND
    ${targets_level_zone_2.location} = CAST(${fact_level_sales.bk_storeid} AS STRING)
;;
    relationship: one_to_many
  }


}

explore: reversals {
  label: "Reversals"
  hidden: yes
  view_label: "Reversals"
  access_filter: {
    field: dim_alternate_bu_hierarchy.country
    user_attribute: country
  }
  access_filter: {
    field: dim_alternate_bu_hierarchy.vertical
    user_attribute: vertical
  }
  access_filter: {
    field: dim_alternate_bu_hierarchy.brand
    user_attribute: brand
  }

  join: dim_retail_loc {
    type: left_outer
    sql_on:
    ${reversals.org_num} = CAST(${dim_retail_loc.store} AS STRING);;
    relationship: many_to_one
  }

  join: dim_alternate_bu_hierarchy {
    type: left_outer
    sql_on:
    CAST(${reversals.org_num} AS STRING) = ${dim_alternate_bu_hierarchy.store_code};;
    relationship: many_to_one
  }
}


explore: age_provision {
  label: "Provisions History"
  view_label: "Provisions"
  hidden: yes
  always_filter: {
    filters: {
      field: provision_date
      value: "Last 1 months"
    }
  }
  access_filter: {
    field: dim_alternate_bu_hierarchy.country
    user_attribute: country
  }
  access_filter: {
    field: dim_alternate_bu_hierarchy.vertical
    user_attribute: vertical
  }
  access_filter: {
    field: dim_alternate_bu_hierarchy.brand
    user_attribute: brand
  }

  join: provision_last_date {
    type: left_outer
    sql_on:
    ${provision_last_date.last_date} = ${age_provision.provision_date}
    ;;
    relationship: one_to_one
  }

  join: dim_item_loc_traits {
    type: left_outer
    sql_on:
    CAST(${dim_item_loc_traits.item} AS STRING) = ${age_provision.prod_num}
    AND
    CAST(${dim_item_loc_traits.loc} AS STRING) =  ${age_provision.org_num};;
    relationship: one_to_one
  }

  join: dim_retail_product {
    type: left_outer
    sql_on:
    ${age_provision.prod_num} = ${dim_retail_product.item};;
    relationship: many_to_one
  }


  join: dim_retail_loc {
    type: left_outer
    sql_on:
    ${age_provision.org_num} = CAST(${dim_retail_loc.store} AS STRING);;
    relationship: many_to_one
  }


  join: dim_distr_ret_loc {
    type: left_outer
    sql_on:
    ${age_provision.org_num} = CAST(${dim_distr_ret_loc.loc_code} AS STRING);;
    relationship: many_to_one
  }

  join: dim_bu_cdl {
    type: left_outer
    sql_on:
      ${dim_bu_cdl.prod_num} = ${age_provision.prod_num}
      AND
      ${dim_bu_cdl.org_num} = CAST(${age_provision.org_num} AS STRING);;
    relationship: one_to_many
  }

  join: dim_alternate_bu_hierarchy {
    type: left_outer
    sql_on:
      ${dim_bu_cdl.business_unit_final} = ${dim_alternate_bu_hierarchy.bu_code};;
    relationship: one_to_many
  }

#   join: dim_alternate_bu_hierarchy {
#     type: left_outer
#     sql_on:
#     CAST(${age_provision.org_num} AS STRING) = ${dim_alternate_bu_hierarchy.store_code};;
#     relationship: many_to_one
#   }
}


# explore: sell_through_report {
#   label: "Sell Through Variances"
#   view_label: "Sell Through Variances"
#
#   always_filter: {
#     filters: [location: "8001"]
#   }
#
# }

explore: dm_soh_sales_arrays {
  sql_always_where: ${business_unit_hash} = {% parameter dm_soh_sales_arrays.bu_name %};;
  label: "Stock in Arrays (BETA)"
  view_label: "Stock and Sales"
  hidden: yes

  join: stock_dates {
    view_label: "Dates"
    sql: LEFT JOIN UNNEST(stock_date) as stock_dates ;;
    relationship: one_to_many
  }

  always_filter: {


    filters: {
      field: bu_name
    }

    filters: {
      field: vertical
      value: "{{ _user_attributes['vertical'] }}"
    }

    filters: {
      field: country_code
      value: "{{ _user_attributes['country'] }}"
    }




  }
  access_filter: {
    field: country_code
    user_attribute: country
  }
  access_filter: {
    field: vertical
    user_attribute: vertical
  }
  access_filter: {
    field: business_unit
    user_attribute: brand
  }

}



explore: dm_soh_in_transfer {
  label: "Stock in Transfer (BETA)"
  view_label: "Stock in Transfer (BETA)"
  hidden: yes
}

explore: baan_stock_aging {
  label: "BAAN Stock"
  view_label: "BAAN Stock"
  hidden: yes
}
explore: finance_budgets {
  label: "finance budgets"
  view_label: "finance budgets"
  hidden: no
}
explore: dm_stock_transfers {
  label: "Historical Transfers (BETA)"
  view_label: "Historical Transfers (BETA)"
  hidden: no

  access_filter: {
    field: dim_alternate_bu_hierarchy.country
    user_attribute: country
  }
  access_filter: {
    field: dim_alternate_bu_hierarchy.vertical
    user_attribute: vertical
  }
  access_filter: {
    field: dim_alternate_bu_hierarchy.brand
    user_attribute: brand
  }

  join: dim_bu_cdl {
    type: left_outer
    sql_on:
      ${dim_bu_cdl.prod_num} = ${dm_stock_transfers.item}
      AND
      ${dim_bu_cdl.org_num} = CAST(${dm_stock_transfers.to_loc} AS STRING);;
    relationship: one_to_many
  }

  join: dim_alternate_bu_hierarchy {
    type: left_outer
    sql_on:
      ${dim_bu_cdl.business_unit_final} = ${dim_alternate_bu_hierarchy.bu_code};;
    relationship: one_to_many
  }

  join: dim_retail_product {
    type: left_outer
    sql_on:
    ${dm_stock_transfers.item} = ${dim_retail_product.item}
     ;;
    relationship: many_to_one
  }
  join: cost_facts {
    type: left_outer
    sql_on:
          ${dm_stock_transfers.create_date} = ${cost_facts.stock_date_raw}
          AND
          CAST(${dm_stock_transfers.from_loc} AS STRING) = ${cost_facts.org_num}
          AND
          ${dm_stock_transfers.item} = ${cost_facts.prod_num}
          ;;
    relationship: many_to_one
  }
}

explore: dm_distr_sales {

  access_filter: {
    field: dim_alternate_bu_hierarchy.country
    user_attribute: country
  }
  access_filter: {
    field: dim_alternate_bu_hierarchy.vertical
    user_attribute: vertical
  }
  access_filter: {
    field: dim_alternate_bu_hierarchy.brand
    user_attribute: brand
  }

  label: "Distribution Sales"
  view_label: "Distribution Sales"

  join: dim_bu_cdl {
    type: left_outer
    sql_on:
      ${dim_bu_cdl.prod_num} = ${dm_distr_sales.item}
      AND
      ${dim_bu_cdl.org_num} = CAST(${dm_distr_sales.loc_code} AS STRING);;
    # relationship: one_to_many
    relationship: many_to_one
  }

  join: dim_alternate_bu_hierarchy {
    type: left_outer
    sql_on:
      ${dim_bu_cdl.business_unit_final} = ${dim_alternate_bu_hierarchy.bu_code};;
    # relationship: one_to_many
    relationship: many_to_one

  }

  join: dim_conversion_daily_rates {
    type: left_outer
    sql_on:
    ${dm_distr_sales.invoice_date} = ${dim_conversion_daily_rates.conversion_date}
    AND
    ${dm_distr_sales.from_currency} = ${dim_conversion_daily_rates.from_currency}
    AND
    ${dm_distr_sales.to_currency} = ${dim_conversion_daily_rates.to_currency}
    AND
    ${dm_distr_sales.conversion_type} = ${dim_conversion_daily_rates.conversion_type}
     ;;
    relationship: many_to_one
  }

  join: cost_facts {
    type: left_outer
    sql_on:
          DATE(${dm_distr_sales.creation_raw}) = ${cost_facts.stock_date_raw}
          AND
          CAST(${dm_distr_sales.loc_code} AS STRING) = ${cost_facts.org_num}
          AND
          ${dm_distr_sales.item} = ${cost_facts.prod_num}
          ;;
    relationship: many_to_one
  }

  join: dm_distr_sales_lines {
    type: left_outer
    sql_on: ${dm_distr_sales.order_number}  = ${dm_distr_sales_lines.order_number}
    and coalesce(${dm_distr_sales.invoice_number},'1')  = coalesce(${dm_distr_sales_lines.invoice_number},'1')
    and ${dm_distr_sales.line_id} = ${dm_distr_sales_lines.line_id} ;;
    relationship: one_to_one
  }


}


explore: order_sums2 {
  label: "Purchase vs Sales"
  view_label: "Purchase vs Sales"
  hidden: yes
  always_filter: {
    filters: {
      field: season
      value: "after 2019/03/01"
    }
    filters: {
      field: sales_season
      value: "after 2019/10/01"
    }

  }

  access_filter: {
    field: dim_alternate_bu_hierarchy.country
    user_attribute: country
  }
  access_filter: {
    field: dim_alternate_bu_hierarchy.vertical
    user_attribute: vertical
  }
  access_filter: {
    field: dim_alternate_bu_hierarchy.brand
    user_attribute: brand
  }

  join: awac_str {
    type: left_outer
    sql_on:
      ${order_sums2.item}=${awac_str.item} ;;
    relationship: many_to_one
  }


  join: dim_retail_product {
    type: left_outer
    sql_on:
      ${order_sums2.item}=${dim_retail_product.item} ;;
    relationship: many_to_one
  }

  join: dim_bu_cdl {
    type: left_outer
    sql_on:
      ${order_sums2.item}=${dim_bu_cdl.prod_num}
      AND
      CAST(${order_sums2.location} AS STRING) = ${dim_bu_cdl.org_num};;
    relationship: many_to_one
  }

  join: dim_alternate_bu_hierarchy {
    type: left_outer
    sql_on:
     ${dim_alternate_bu_hierarchy.bu_code}= ${dim_bu_cdl.business_unit_final};;
    relationship: many_to_one
  }

  join: dim_retail_loc {
    type: left_outer
    sql_on:
    ${order_sums2.location}= ${dim_retail_loc.store};;
    relationship: many_to_one
  }

  join: dim_item_loc_traits {
    type: left_outer
    sql_on:
     ${order_sums2.item}=${dim_item_loc_traits.item}
    AND ${order_sums2.location}=${dim_item_loc_traits.loc};;
    relationship: many_to_one
  }
# ------------------------------BR76 starts ---------------------------------
  # join: ch_concession {
  #   type: left_outer
  #   sql_on:
  #   ${dim_retail_product.class} = ${ch_concession.class}
  #   AND
  #   ${dim_retail_product.dept_no} = ${ch_concession.dept}
  #   AND
  #   ${order_sums2.location} = ${ch_concession.store};;
  #   relationship: many_to_many
  # }

  join: dim_prod_org_concession {
    type: left_outer
    sql_on:
    ${dim_retail_product.item} = ${dim_prod_org_concession.prod_num}
    AND
    cast(${order_sums2.location} as string)  = ${dim_prod_org_concession.org_num};;
    relationship: many_to_many
  }
  # ------------------------------BR76 ends ---------------------------------



  join: udas {
    type: left_outer
    sql_on:
    ${order_sums2.item} = ${udas.item};;
    relationship: many_to_one
  }

  join: udas2 {
    type: left_outer
    sql_on:
    ${order_sums2.item} = ${udas2.item};;
    relationship: many_to_one
  }

}


explore: str_age {
  label: "Sell Through on Age"
  view_label: "Sell Through on Age"
  always_filter: {
    filters: {
      field: report_country
      value: "United Arab Emirates"
    }
  }

  hidden: no
  access_filter: {
    field: dim_alternate_bu_hierarchy.country
    user_attribute: country
  }
  access_filter: {
    field: dim_alternate_bu_hierarchy.vertical
    user_attribute: vertical
  }
  access_filter: {
    field: dim_alternate_bu_hierarchy.brand
    user_attribute: brand
  }

  join: dim_bu_cdl {
    type: left_outer
    sql_on:
      ${dim_bu_cdl.prod_num} = ${str_age.item}
      AND
      ${dim_bu_cdl.org_num} = CAST(${str_age.org_num} AS STRING);;
    relationship: one_to_many
  }

  join: dim_alternate_bu_hierarchy {
    type: left_outer
    sql_on:
      ${dim_bu_cdl.business_unit_final} = ${dim_alternate_bu_hierarchy.bu_code};;
    relationship: one_to_many
  }

  join: dim_retail_loc {
    type: left_outer
    sql_on:
    CAST(${str_age.org_num} AS INT64)= ${dim_retail_loc.store};;
    relationship: many_to_one
  }

  join: dim_retail_product {
    type: left_outer
    sql_on:
    ${str_age.item} = ${dim_retail_product.item}
     ;;
    relationship: many_to_one
  }


}


# explore: dm_dsp_shipping_data {
#   label: "Logistics Data"
#   view_label: "Logistics"
#   hidden: yes


#   access_filter: {
#     field: brand_vertical_mapping.bu_brand
#     user_attribute: brand
#   }
#   access_filter: {
#     field: brand_vertical_mapping.vertical
#     user_attribute: vertical
#   }

#   join:brand_vertical_mapping {
#     type: left_outer
#     relationship: many_to_one
#     sql_on:
#     ${dm_dsp_shipping_data.brand_upper} = ${brand_vertical_mapping.bu_brand};;

#     }



#   }

#   explore: dm_dsp_shipping_data_updated {
#     label: "Logistics Data (BETA)"
#     view_label: "Logistics (BETA)"
#     hidden: yes

# #     join:date_master {
# #       type: left_outer
# #       relationship: many_to_one
# #       sql_on: 1=1
# #       ;;
# #     }



# #     access_filter: {
# #       field: brand_vertical_mapping.bu_brand
# #       user_attribute: brand
# #     }
# #     access_filter: {
# #       field: brand_vertical_mapping.vertical
# #       user_attribute: vertical
# #     }
# #
# #     join:brand_vertical_mapping {
# #       type: left_outer
# #       relationship: many_to_one
# #       sql_on:
# #           ${dm_dsp_shipping_data_updated.brand_upper} = ${brand_vertical_mapping.bu_brand};;
# #
# #       }
#   }

  explore: fact_soh_sales_poc {
    persist_for: "12 hours"
    always_filter: {
      filters: {
        field: business_unit
        value: "{{ _user_attributes['brand'] }}"
      }

      filters: {
        field: vertical
        value: "{{ _user_attributes['vertical'] }}"
      }
      filters: {
        field: org_num
        value: "%"
      }
    }
    conditionally_filter: {
      filters: [stock_date: "Last 1 months"]
      unless: [fact_soh_sales_poc.stock_month]
    }
    access_filter: {
      field: fact_soh_sales_poc.country_code
      user_attribute: country
    }
    access_filter: {
      field: fact_soh_sales_poc.vertical
      user_attribute: vertical
    }
    access_filter: {
      field: fact_soh_sales_poc.business_unit
      user_attribute: brand
    }
    label: "Sales and Stock Explore"
    view_label: "Sales and Stock Information"


    join: max_age {
      type: left_outer
      sql_on:
            ${fact_soh_sales_poc.prod_num} = ${max_age.prod_num}
            AND
            CAST(${fact_soh_sales_poc.store} AS STRING) = ${max_age.org_num};;
      relationship: one_to_one
    }

    join: fiscal_dates {
      view_label: "Fiscal Dates"
      type: left_outer
      sql_on:
          ${fact_soh_sales_poc.stock_date}= ${fiscal_dates.calendar_date};;
      relationship: one_to_one
    }
    join: item_hts {
      type: left_outer
      sql_on:
          ${fact_soh_sales_poc.prod_num}= ${item_hts.item};;
      relationship: one_to_many
    }
    join: stock_cover {
      type: left_outer
      sql_on:
            ${fact_soh_sales_poc.prod_num} = ${stock_cover.prod_num}
            AND
            ${fact_soh_sales_poc.org_num} = ${stock_cover.org_num};;
      relationship: many_to_one
    }

    join: sales_grading {
      type: left_outer
      sql_on:
            ${fact_soh_sales_poc.prod_num} = ${sales_grading.bk_productid}
            AND
            ${fact_soh_sales_poc.store} = ${sales_grading.bk_storeid}
            AND
            ${fact_soh_sales_poc.stock_year} = ${sales_grading.year}
            AND
            ${fact_soh_sales_poc.month_no} = ${sales_grading.month}
             ;;
      relationship: many_to_one
    }
    # ---------------------------------BR76 starts---------------------------------
    # join: ch_concession {
    #   type: left_outer
    #   sql_on:
    #         ${fact_soh_sales_poc.class_no} = ${ch_concession.class}
    #         AND
    #         ${fact_soh_sales_poc.department_id} = ${ch_concession.dept}
    #         AND
    #         ${fact_soh_sales_poc.org_num} = CAST(${ch_concession.store} AS STRING);;
    #   relationship: many_to_many
    # }

    join: dim_prod_org_concession {
      type: left_outer
      sql_on:
            ${fact_soh_sales_poc.prod_num} = ${dim_prod_org_concession.prod_num}
            AND
            ${fact_soh_sales_poc.org_num} = ${dim_prod_org_concession.org_num} ;;
      relationship: many_to_many
    }
        # ---------------------------------BR76 ends ---------------------------------



     join: dim_item_loc_traits {
      type: left_outer
      sql_on:
          ${fact_soh_sales_poc.prod_num} = ${dim_item_loc_traits.item}
         AND   ${fact_soh_sales_poc.store} = ${dim_item_loc_traits.loc};;
      relationship: one_to_one
    }
    join: dim_retail_product {
      type: left_outer
      sql_on:
      ${dim_retail_product.item} = ${fact_soh_sales_poc.prod_num};;
      relationship: one_to_many
    }
    join: weighted_osa {
      type: left_outer
      sql_on:
            ${weighted_osa.grading} = ${sales_grading.pareto_grade}
            AND
            ${weighted_osa.granularity_from_parameter} = ${fact_soh_sales_poc.granularity_from_parameter}
            AND
            ${weighted_osa.granularity_from_parameter_b} = ${fact_soh_sales_poc.granularity_from_parameter_b}
            ;;
      relationship: many_to_one
    }

    join: sell_through_on_stock {
      type: left_outer
      sql_on:
            ${sell_through_on_stock.last_date} = ${fact_soh_sales_poc.stock_date}
            ;;
      relationship: one_to_one
    }


    join: dm_soh_sales_last_day {
      type: left_outer
      sql_on:
            ${fact_soh_sales_poc.stock_date} = ${dm_soh_sales_last_day.stock_date}
            AND
            ${fact_soh_sales_poc.org_num} = ${dm_soh_sales_last_day.org_num}
            AND
            ${fact_soh_sales_poc.prod_num} = ${dm_soh_sales_last_day.prod_num}
            ;;
      relationship: one_to_one
    }

    join: dm_soh_sales_last_day_2 {
      type: left_outer
      sql_on:
            ${fact_soh_sales_poc.stock_date} = ${dm_soh_sales_last_day_2.stock_date}
            AND
            ${fact_soh_sales_poc.org_num} = ${dm_soh_sales_last_day_2.org_num}
            AND
            ${fact_soh_sales_poc.prod_num} = ${dm_soh_sales_last_day_2.prod_num}
            ;;
      relationship: one_to_one
    }
    join: budgetstarget {
      type: left_outer
      sql_on:
          cast(${fact_soh_sales_poc.org_num} as int64) = ${budgetstarget.bk_store}
          AND
          ${fact_soh_sales_poc.stock_date} = ${budgetstarget.business_date};;
      relationship: many_to_one
    }
    join: dim_conversion_daily_rates {
      type: left_outer
      sql_on:
          ${budgetstarget.business_date} = ${dim_conversion_daily_rates.conversion_date}
          AND
          ${budgetstarget.from_currency} = ${dim_conversion_daily_rates.from_currency}
          AND
          ${budgetstarget.to_currency} = ${dim_conversion_daily_rates.to_currency}
          AND
          ${budgetstarget.conversion_type} = ${dim_conversion_daily_rates.conversion_type}
           ;;
      relationship: many_to_one
    }
    join: average_inventory_on_hand {
      type: left_outer
      sql_on:
            ${average_inventory_on_hand.granularity_from_parameter} = ${fact_soh_sales_poc.granularity_from_parameter}
            AND
            ${average_inventory_on_hand.granularity_from_parameter_b} = ${fact_soh_sales_poc.granularity_from_parameter_b}
             AND
            DATE(${average_inventory_on_hand.stock_date}) = ${fact_soh_sales_poc.stock_date}
            ;;
      relationship: many_to_one
    }

    join: gmroi {
      type: left_outer
      sql_on:
            ${gmroi.granularity_from_parameter} = ${fact_soh_sales_poc.granularity_from_parameter}
            AND
            ${gmroi.granularity_from_parameter_b} = ${fact_soh_sales_poc.granularity_from_parameter_b}
            AND
            ${gmroi.stock_date_from_parameter} = ${fact_soh_sales_poc.stock_date_from_parameter}
            ;;
      relationship: many_to_one
    }

    join: dio_stock_cover {
      type: full_outer
      sql_on:
            ${dio_stock_cover.granularity_from_parameter} = ${fact_soh_sales_poc.granularity_from_parameter}
            AND
            ${dio_stock_cover.granularity_from_parameter_b} = ${fact_soh_sales_poc.granularity_from_parameter_b}
            AND
            ${dio_stock_cover.stock_raw_date} = ${fact_soh_sales_poc.stock_raw}
            ;;
      relationship: many_to_one
    }

    join: weightings {
      type: left_outer
      sql_on: ${weightings.pareto_grade} = ${weighted_osa.grading}
            AND
            ${weightings.granularity_from_parameter} = ${weighted_osa.granularity_from_parameter}
            AND
            ${weightings.granularity_from_parameter_b} = ${weighted_osa.granularity_from_parameter_b} ;;
      relationship: many_to_one
    }

#   SPD-201 changes in Average inventory calculation
    join: dm_sohsales_max_min_dates {
      type: left_outer
      sql_on: 1=1 ;;
      relationship: many_to_one
    }

#   Hidden images for developer use only
    join: level_vpn_images {
      view_label: "Level Shoes Website Images"
      relationship: many_to_one
      type: left_outer
      sql_on:  ${fact_soh_sales_poc.item_vpn} = ${level_vpn_images.vpn} ;;
    }




# -----------SPD-275 changes to include consignment WAC starts ----------------------------------------------



    join : ch_commission_valid_data {
      type: left_outer
      sql_on: ${fact_soh_sales_poc.department_id} =  ${ch_commission_valid_data.dept}
          and  case when  ${ch_commission_valid_data.class}  is null then 1 else ${fact_soh_sales_poc.class_no} end   = case when  ${ch_commission_valid_data.class}  is null then 1 else  ${ch_commission_valid_data.class}  end
          and  case when  ${ch_commission_valid_data.subclass}  is null then 1 else ${fact_soh_sales_poc.subclass_no} end   = case when  ${ch_commission_valid_data.subclass}  is null then 1 else  ${ch_commission_valid_data.subclass}  end
          and ${fact_soh_sales_poc.org_num}= cast(${ch_commission_valid_data.store} as string)
          and ${fact_soh_sales_poc.country_id} =  ${ch_commission_valid_data.country}
        ;;
      relationship: many_to_one
    }

# ------------------SPD-275 changes to include consignment WAC ends --------------------------------------


        # join:dim_dsp_attempt_forecast_delivery {
        #   type: left_outer
        #   relationship: many_to_one
        #   sql_on:
        #       ${carriyo_tracker_summary.pickup_country} = ${dim_dsp_attempt_forecast_delivery.country};;
        # }


        # join: dim_order_summary_forecast {
        #   type: left_outer
        #   relationship: many_to_one
        #   sql_on:
        #       ${carriyo_tracker_summary.pickup_country} = ${dim_order_summary_forecast.country}
        #       and ${carriyo_tracker_summary.merchant} = ${dim_order_summary_forecast.brand}
        #       and ${carriyo_date_master.date_date} = ${dim_order_summary_forecast.date_date}
        #       and ${carriyo_tracker_summary.service_type} = ${dim_order_summary_forecast.service_type};;
        # }

        # join: dim_dsp_share {
        #   type: left_outer
        #   relationship: many_to_one
        #   sql_on: ${carriyo_tracker_summary.carrier} = ${dim_dsp_share.provider}
        #             and ${carriyo_tracker_summary.pickup_country} = ${dim_dsp_share.country}
        #             and ${carriyo_tracker_summary.service_type} = ${dim_dsp_share.service_type};;
        # }

    # join: dm_dsp_forecasted_orders {
    #     type: left_outer
    #     relationship: many_to_one
    #     sql_on:  ${carriyo_tracker_summary.pickup_country} = ${dm_dsp_forecasted_orders.country}
    #           and ${carriyo_tracker_summary.merchant} = ${dm_dsp_forecasted_orders.brand}
    #           and ${carriyo_date_master.date_date} = ${dm_dsp_forecasted_orders.date}
    #           and ${carriyo_tracker_summary.service_type} = ${dm_dsp_forecasted_orders.service_type}
    #           and ${carriyo_tracker_summary.carrier} = ${dm_dsp_forecasted_orders.provider};;
    #   }


      }

    explore: dim_retail_prod_brand {
    hidden: yes
  }

  explore: dim_retail_prod_class {
    hidden: yes
  }

  explore: dim_retail_prod_colour {
    hidden: yes
  }

  explore: dim_retail_prod_division {
    hidden: yes
  }

  explore: dim_retail_prod_gender {
    hidden: yes
  }

  explore: dim_retail_prod_size {
    hidden: yes
  }

  explore: dim_retail_prod_sub_class {
    hidden: yes
  }

  explore: dim_retail_prod_taxo_class {
    hidden: yes
  }

  explore: dim_retail_prod_taxo_subclass {
    hidden: yes
  }

  explore: dim_retail_prod_season {
    hidden: yes
  }

  explore: dim_retail_loc_bu {
    hidden: yes
  }

  explore: dim_retail_loc_store {
    hidden: yes
  }

  explore: dim_retail_loc_country {
    hidden: yes
  }

  explore: dim_retail_loc_country_desc {
    hidden: yes
  }

  explore: dim_retail_loc_store_name {
    hidden: yes
  }

  explore: dim_retail_loc_district_name {
    hidden: yes
  }

  explore: dim_retail_loc_vertical {
    hidden: yes
  }

# explore: month_start_stock {
#   hidden: yes
# }

# explore: average_inventory_on_hand {}

  explore: usr_mgt_approved_requests {
    hidden: yes
  }

explore: sell_through_prototype {
  label: "Sell Through V2"
  always_filter: {
    filters: [
      sell_through_prototype.comparison_period: "WEEK",
      sell_through_prototype.number_of_periods_back: "1",
      sell_through_prototype.prototype_date_1: ""]
  }

  join: sell_through_prototype_sales_previous_period {
    type: left_outer
    sql_on:
            ${sell_through_prototype_sales_previous_period.location} = ${sell_through_prototype.location}
            AND
            ${sell_through_prototype_sales_previous_period.product} = ${sell_through_prototype.product}
            AND
            ${sell_through_prototype_sales_previous_period.date_date} = TIMESTAMP(${sell_through_prototype.date_date})



            ;;
    relationship: one_to_many
  }

  join: sell_through_last_first_values {
    type: full_outer
    sql_on:
        ${sell_through_last_first_values.location} = ${sell_through_prototype.location}
        AND
        ${sell_through_last_first_values.product} = ${sell_through_prototype.product}

    ;;
    relationship: one_to_one
  }

  join: dim_retail_product {
    type: left_outer
    sql_on: ${sell_through_prototype.product} = ${dim_retail_product.item}
    ;;
  }

  join: dim_alternate_bu_hierarchy {
    type: left_outer
    sql_on: ${sell_through_prototype.location} = ${dim_alternate_bu_hierarchy.store_code}  ;;
  }

  join: dim_item_loc_traits {
    type: left_outer
    sql_on:  ${sell_through_prototype.location} = CAST(${dim_item_loc_traits.loc} as STRING)
            AND
            ${sell_through_prototype.product} = ${dim_item_loc_traits.item}
            ;;
  }

     #   AND

      #   ${sell_through_last_first_values.date} =
      # DATE(${sell_through_prototype.date_date})

}
