connection: "chb-prod-data-cust"
include: "/customer_views/*.*"
include: "/customer_views/base_customers_metrics/*.*"
include: "/_common/*.*"
include: "../comm_views/*.view"
include: "../group_log_views/*.view"
include: "../sensitive_data_views/*.view.lkml"

datagroup: chb_customer_default_datagroup {
  # sql_trigger: SELECT MAX(id) FROM etl_log;;
  max_cache_age: "1 hour"
}


week_start_day: sunday




explore: cs_transactions {
  fields: [
    ALL_FIELDS*,
    -base_customers_factretailsales.is_muse_signup_txn,

    -base_customers_distinct.is_new_customer_universal,
    -base_customers_distinct.retained_customer_universal,
    -base_customers_factretailsales.acquisition_rate,
    -base_customers_factretailsales.new_customer_count,
    -base_customers_distinct.retained_customer,
    -base_customers_factretailsales.total_customers_prev_period,
    -base_customers_distinct.retained_customer_universal,
    -base_customers_factretailsales.total_customers_prev_period,
    -base_customers_distinct.ret_range_start,
    -base_customers_distinct.ret_range_end

    ]
  label: "CS Control Tower"

  view_name: cs_transactions

  join: base_customers_distinct {
    type: left_outer
    sql_on: ${cs_transactions.customer_unique_id}=
      ${base_customers_distinct.customer_unique_id};;
    relationship: many_to_many
  }

  join: base_customers_factretailsales {
    type: left_outer
    sql_on: ${cs_transactions.customer_unique_id}=
    ${base_customers_factretailsales.customer_unique_id};;
    relationship: many_to_many
  }

  join: oms_x_carriyo {
    fields: [oms_x_carriyo.order_id,
            oms_x_carriyo.latest_status,
            oms_x_carriyo.tracking_no,
            oms_x_carriyo.no_of_attempts,
            oms_x_carriyo.latest_undelivered_reason,
            oms_x_carriyo.latest_status_boolean,
            oms_x_carriyo.chb_status,
            oms_x_carriyo.merchant,
            oms_x_carriyo.retailunity_order_id,
            oms_x_carriyo.brand_url
            ]
    type: left_outer
    sql_on: ${base_customers_factretailsales.order_id_ecomm}=
    ${oms_x_carriyo.order_id};;
    relationship: many_to_many
  }

  access_filter: {
    field: cs_transactions.bu_brand_name
    user_attribute: brand
  }


  join:brand_vertical_mapping_log {
    type: left_outer
    relationship: many_to_one
    sql_on:
      ${oms_x_carriyo.merchant}=${brand_vertical_mapping_log.bu_brand};;
  }

  join: prev_month_tot_cust {
    type: left_outer
    sql_on: ${base_customers_factretailsales.min_brand_id}=${prev_month_tot_cust.min_brand_id}
          and ${base_customers_factretailsales.current_month}=${prev_month_tot_cust.next_month}
          and ${base_customers_factretailsales.current_year}=${prev_month_tot_cust.year_next_month}

          ;;
    relationship: many_to_one
  }

  join: prev_month_customers {
    type: left_outer
    sql_on: ${base_customers_factretailsales.current_month}= ${prev_month_customers.next_month}
        and ${base_customers_factretailsales.current_year}= ${prev_month_customers.year_next_month}
        and ${base_customers_factretailsales.brand_customer_unique_id}=${prev_month_customers.brand_customer_unique_id};;
    relationship: many_to_one



}
}


explore: ecomm_orders_new_vs_returning_pop{

  label: "Ecom Customer Growth Control Tower"
  view_name: ecomm_customer_transactional_pop

  access_filter: {
    field: dim_alternate_bu_hierarchy.country
    user_attribute: country
  }
  access_filter: {
    field: dim_alternate_bu_hierarchy.vertical
    user_attribute: vertical
  }
  access_filter: {
    field: dim_alternate_bu_hierarchy.brand
    user_attribute: brand
  }



  join: ecomm_customer_type_pop{
    type: inner
    relationship: many_to_one
    sql_on: ${ecomm_customer_type_pop.date_group} = ${ecomm_customer_transactional_pop.date_group}
           AND  ${ecomm_customer_type_pop.customer_key} = ${ecomm_customer_transactional_pop.customer_key}
          ;;
  }

  join: ecomm_customer_pop_pop{
    type: inner
    relationship: many_to_one
    sql_on:  ${ecomm_customer_pop_pop.brand} = ${ecomm_customer_transactional_pop.brand}
      AND  ${ecomm_customer_pop_pop.date_group} = ${ecomm_customer_transactional_pop.date_group}
      AND  ${ecomm_customer_pop_pop.customer_type} = ${ecomm_customer_type_pop.customer_type};;
  }

#   join: ecomm_brand_aggregates_pop{
#     type: inner
#     relationship: many_to_one
#     sql_on: ${ecomm_customer_transactional_pop.brand} = ${ecomm_brand_aggregates_pop.brand} ;;
#   }

  join: ecomm_customer_aggregates_pop{
    type: inner
    relationship: many_to_one
    sql_on: ${ecomm_customer_transactional_pop.customer_key} = ${ecomm_customer_aggregates_pop.customer_key} ;;
  }

  join: locations {
    type: inner
    sql_on: ${ecomm_customer_transactional_pop.store_id}=${locations.store};;
    relationship: :many_to_one
  }

  join: dim_alternate_bu_hierarchy {
    type:  inner
    sql_on: cast(${locations.store} as string)=${dim_alternate_bu_hierarchy.store_code} ;;
    relationship: one_to_one
  }


}


# join: cross_brand_adoption {
#   type:  left_outer
#   sql_on: {dim_retail_loc.district_name}=${cross_brand_adoption.brand} ;;
#
#   relationship: one_to_one
# }

# }
# explore: factretailsales_customer_unique_id{
#   label: "DEV Muse & CRM Customers"
#
#   join: base_customers_unique{
#     type: left_outer
#     sql_on:${factretailsales_customer_unique_id.brand_customer_unique_id} = ${base_customers_unique.brand_customer_unique_id}  ;;
#     relationship: one_to_many
#   }
#
#   join: locations {
#     type: inner
#     sql_on: ${factretailsales_customer_unique_id.bk_storeid}=${locations.store};;
#     relationship: :many_to_one
#   }

explore: base_customers_factretailsales {
  label: "Customer 360"
  always_filter: {
  filters: [is_generic_customer : "no", base_customers_distinct.is_generic_customer : "no", is_known_cust_txn : "yes"]}


  access_filter: {
    field: dim_alternate_bu_hierarchy.country
    user_attribute: country
  }
  access_filter: {
    field: dim_alternate_bu_hierarchy.vertical
    user_attribute: vertical
  }
  access_filter: {
    field: dim_alternate_bu_hierarchy.brand
    user_attribute: brand
  }


  join: dim_calendar {
    type:  inner
    sql_on:

      ${dim_calendar.dim_date_key}=${base_customers_factretailsales.bk_businessdate}
    ;;
    relationship: many_to_many
  }

  join: group_benchmarks {
    type:  left_outer
    sql_on:

      ${dim_calendar.dim_date_key}=${group_benchmarks.base_transaction_date_id}
    ;;
    relationship: many_to_one
  }

# join: returning_cust_dyn {
#   type: left_outer
#   sql_on:   ${dim_calendar.bk_businessdate} = ${returning_cust_dyn.bk_businessdate}
#   and
#   ${base_customers_factretailsales.customer_unique_id}=${returning_cust_dyn.customer_unique_id}
#       ;;
# }

join: returning_cust_dyn_pp {
  type: left_outer
  sql_on:    ${dim_calendar.dim_date_key }= ${returning_cust_dyn_pp.bk_businessdate}
  and
  ${base_customers_distinct.customer_unique_id}=${returning_cust_dyn_pp.customer_unique_id}
  and ${locations.store}=${returning_cust_dyn_pp.bk_storeid}
       ;;
}

join: new_to_store_pp_ndt  {
  type: left_outer
  sql_on: ${base_customers_factretailsales.customer_unique_id}=${new_to_store_pp_ndt.customer_unique_id}
       ;;
}



# join: new_to_store_ndt {
#   type: left_outer
#   sql_on:
#   ${base_customers_factretailsales.customer_unique_id}=${new_to_store_ndt.customer_unique_id} ;;
# }


join: customer_day_store {
  type: left_outer

  sql_on:
  ${locations.store}=${customer_day_store.bk_storeid}
and

case when ${dim_calendar.selected_granularity}=1 then

    ${dim_calendar.cal_year}=${customer_day_store.next_month_year}
    and
    ${dim_calendar.cal_month_no}=${customer_day_store.next_month}



when ${dim_calendar.selected_granularity}=2 then

    ${dim_calendar.cal_quarter_no}=${customer_day_store.next_quarter}
    and  ${dim_calendar.cal_year}=${customer_day_store.next_quarter_year}

  when ${dim_calendar.selected_granularity}=3 then

      case when extract(month from  (PARSE_DATE("%Y%m%d", cast(${dim_calendar.dim_date_key} as string))))<7 then 1 else 2 end  =${customer_day_store.next_half}
      and ${dim_calendar.cal_year}=${customer_day_store.next_half_year}

  when ${dim_calendar.selected_granularity}=4 then

    ${dim_calendar.cal_year} =${customer_day_store.next_year}

when ${dim_calendar.selected_granularity}=0 then

      ${customer_day_store.bk_businessdate}<${dim_calendar.ret_range_end_int}

      and ${dim_calendar.ret_range_start_int}<=${customer_day_store.bk_businessdate}

  else
        ${dim_calendar.ret_range_end_int}<${customer_day_store.bk_businessdate}

      and ${dim_calendar.ret_range_start_int}>=${customer_day_store.bk_businessdate}

  end
  ;;
}


join: retained_cust {
    type: left_outer


    sql_on:
    ${locations.store}=${customer_day_store.bk_storeid}
    and
    ${base_customers_distinct.customer_unique_id}=${retained_cust.customer_unique_id_prev}
      and

case when ${dim_calendar.selected_granularity}=1 then

    ${dim_calendar.cal_year}=${retained_cust.next_month_year}
    and
    ${dim_calendar.cal_month_no}=${retained_cust.next_month}



when ${dim_calendar.selected_granularity}=2 then

    ${dim_calendar.cal_quarter_no}=${retained_cust.next_quarter}
    and  ${dim_calendar.cal_year}=${retained_cust.next_quarter_year}

  when ${dim_calendar.selected_granularity}=3 then

      case when extract(month from  (PARSE_DATE("%Y%m%d", cast(${dim_calendar.dim_date_key} as string))))<7 then 1 else 2 end  =${retained_cust.next_half}
      and ${dim_calendar.cal_year}=${retained_cust.next_half_year}

  when ${dim_calendar.selected_granularity}=4 then

    ${dim_calendar.cal_year} =${retained_cust.next_year}

when ${dim_calendar.selected_granularity}=0 then

    ${retained_cust.bk_businessdate}<${dim_calendar.ret_range_end_int}

      and ${dim_calendar.ret_range_start_int}<=${retained_cust.bk_businessdate}

  else
        ${dim_calendar.ret_range_end_int}<${retained_cust.bk_businessdate}

      and ${dim_calendar.ret_range_start_int}>=${retained_cust.bk_businessdate}

  end
  ;;
  }

  join: prev_period_ndt {
    type: left_outer

    sql_on:
    ${base_customers_factretailsales.anykey}=${prev_period_ndt.anykey}

    and
    case when ${dim_calendar.selected_granularity}=1 then

        ${dim_calendar.cal_year}=${prev_period_ndt.next_month_year}
        and
        ${dim_calendar.cal_month_no}=${prev_period_ndt.next_month}



    when ${dim_calendar.selected_granularity}=2 then

        ${dim_calendar.cal_quarter_no}=${prev_period_ndt.next_quarter}
        and  ${dim_calendar.cal_year}=${prev_period_ndt.next_quarter_year}

      when ${dim_calendar.selected_granularity}=3 then

          case when extract(month from  (PARSE_DATE("%Y%m%d", cast(${dim_calendar.dim_date_key} as string))))<7 then 1 else 2 end  =${prev_period_ndt.next_half}
          and ${dim_calendar.cal_year}=${prev_period_ndt.next_half_year}

      when ${dim_calendar.selected_granularity}=4 then

        ${dim_calendar.cal_year} =${prev_period_ndt.next_year}

    when ${dim_calendar.selected_granularity}=0 then

        ${prev_period_ndt.bk_businessdate}<${dim_calendar.ret_range_end_int}

          and ${dim_calendar.ret_range_start_int}<=${prev_period_ndt.bk_businessdate}

      else
            ${dim_calendar.ret_range_end_int}<${prev_period_ndt.bk_businessdate}

          and ${dim_calendar.ret_range_start_int}>=${prev_period_ndt.bk_businessdate}

      end
      ;;
  }



#   join: retention_rate_ndt {
#     type: left_outer

#     sql_on:

#       ${base_customers_factretailsales.customer_unique_id}=${retention_rate_ndt.customer_unique_id_prev}

#       and

# case when ${dim_calendar.selected_granularity}=1 then

#     ${dim_calendar.cal_year}=${retention_rate_ndt.next_month_year}
#     and
#     ${dim_calendar.cal_month_no}=${retention_rate_ndt.next_month}



# when ${dim_calendar.selected_granularity}=2 then

#     ${dim_calendar.cal_quarter_no}=${retention_rate_ndt.next_quarter}
#     and  ${dim_calendar.cal_year}=${retention_rate_ndt.next_quarter_year}

#   when ${dim_calendar.selected_granularity}=3 then

#       case when extract(month from  (PARSE_DATE("%Y%m%d", cast(${dim_calendar.dim_date_key} as string))))<7 then 1 else 2 end  =${retention_rate_ndt.next_half}
#       and ${dim_calendar.cal_year}=${retention_rate_ndt.next_half_year}

#   when ${dim_calendar.selected_granularity}=4 then

#     ${dim_calendar.cal_year} =${retention_rate_ndt.next_year}

# when ${dim_calendar.selected_granularity}=0 then

#     ${retained_cust.bk_businessdate}<${dim_calendar.ret_range_end_int}

#       and ${dim_calendar.ret_range_start_int}<=${retention_rate_ndt.bk_businessdate}

#   else
#         ${dim_calendar.ret_range_end_int}<${retention_rate_ndt.bk_businessdate}

#       and ${dim_calendar.ret_range_start_int}>=${retention_rate_ndt.bk_businessdate}

#   end
#   ;;
#   }

  # join: date_mapper {
  #   type: left_outer
  #   sql_on: ${dim_calendar.date_mapper_source2}=${date_mapper.source_date} ;;
  #   relationship: many_to_one
  # }

  # join: sales_unified_comparative {
  #   type: left_outer
  #   sql_on: ${locations.store} =${sales_unified_comparative.bk_storeid};;
  #   relationship: one_to_many
  # }

# join: base_customers_offset_sales {
#   type: inner
#   sql_on: ${dim_calendar.businessdate_offset}=${base_customers_offset_sales.prev_bk_businessdate}
#   and ${locations.store}=${base_customers_offset_sales.prev_bk_storeid}
#   and ${dim_retail_product.item}=${base_customers_offset_sales.prev_bk_productid};;
#   relationship: one_to_many
# }

  join: base_customers_distinct{

    type: left_outer
    sql_on:${base_customers_factretailsales.brand_customer_unique_id} = ${base_customers_distinct.brand_customer_unique_id}  ;;
    relationship: one_to_many
  }

  join: locations {
    type: inner
    sql_on: ${base_customers_factretailsales.bk_storeid}=${locations.store};;
    relationship: :many_to_one
  }


  # join: dim_retail_product_unified {
  #   type: inner
  #   sql_on: cast(${base_customers_factretailsales.bk_productid} as string)=${dim_retail_product_unified.item} ;;
  #   relationship: many_to_one

  # }


  join: dim_retail_product {
    type: inner
    sql_on: cast(${base_customers_factretailsales.bk_productid} as string)=${dim_retail_product.item} ;;
    relationship: many_to_one

  }


  join: prev_month_tot_cust {
    type: left_outer
    sql_on: ${base_customers_factretailsales.min_brand_id}=${prev_month_tot_cust.min_brand_id}
    and ${base_customers_factretailsales.current_month}=${prev_month_tot_cust.next_month}
    and ${base_customers_factretailsales.current_year}=${prev_month_tot_cust.year_next_month}

    ;;
    relationship: many_to_one
  }

join: prev_month_customers {
  type: left_outer
  sql_on: ${base_customers_factretailsales.current_month}= ${prev_month_customers.next_month}
  and ${base_customers_factretailsales.current_year}= ${prev_month_customers.year_next_month}
  and ${base_customers_factretailsales.brand_customer_unique_id}=${prev_month_customers.brand_customer_unique_id};;
  relationship: many_to_one

}




  join: customer_360_facts {
    type: left_outer
    sql_on: ${base_customers_distinct.brand_customer_unique_id} =${customer_360_facts.brand_customer_unique_id} ;;
    relationship: one_to_one
  }


  join: customer_360_group_facts {
    type: left_outer
    sql_on: ${base_customers_distinct.customer_unique_id} =${customer_360_group_facts.customer_unique_id} ;;
    relationship: many_to_one
  }

  join: customer_360_facts_lifetime {
    type: left_outer
    sql_on: ${base_customers_distinct.brand_customer_unique_id} =${customer_360_facts_lifetime.brand_customer_unique_id} ;;
    relationship: one_to_one
  }

  join: transaction_attributes {
    type:  left_outer
    sql_on:  ${base_customers_factretailsales.id}= ${transaction_attributes.id};;
    relationship: one_to_one
  }

  join: c360_new_returning {
    type: left_outer
    sql_on:  ${base_customers_factretailsales.brand_customer_unique_id} =${c360_new_returning.brand_customer_unique_id}
      and ${base_customers_factretailsales.date_group}=${c360_new_returning.date_group};;
  }



join: customer_cross_brand {
  type: left_outer
  sql_on:  ${base_customers_factretailsales.customer_unique_id} =${customer_cross_brand.customer_unique_id}
   ;;
}



#   join: customer_facts_2 {
#     type: left_outer
#     sql_on: ${base_customers.bk_customer} =${customer_facts.bk_customer} ;;
#     relationship: one_to_one
#   }

  join: dim_alternate_bu_hierarchy {
    type:  inner
    sql_on: cast(${locations.store} as string)=${dim_alternate_bu_hierarchy.store_code} ;;
    relationship: one_to_one
  }

  # segmentation: lifecycle, value, experience(empty), vip
  join: current_segments {
    type:  left_outer
    sql_on: ${current_segments.brand_customer_unique_id}=${base_customers_factretailsales.brand_customer_unique_id}
      ;;
    relationship: many_to_one
  }

  join: segmentation_history {
    type:  left_outer
    sql_on: ${segmentation_history.brand_customer_unique_id}=${base_customers_factretailsales.brand_customer_unique_id}
      ;;
    relationship: many_to_many
  }

  join: base_customers_sf_contact {
    type:  left_outer
    sql_on: ${base_customers_sf_contact.brand_customer_unique_id}=${base_customers_factretailsales.brand_customer_unique_id}
      ;;
    relationship: many_to_many
  }

  join: communication_consent{
    type: left_outer
    sql_on:${base_customers_distinct.brand_customer_unique_id}= ${communication_consent.brand_customer_unique_id}    ;;
    relationship: one_to_many
  }

  join: brand_first_acquisition_channel{
    type: left_outer
    sql_on:${base_customers_distinct.brand_customer_unique_id}= ${brand_first_acquisition_channel.brand_customer_unique_id}    ;;
    relationship: one_to_one
  }

  join: group_first_acquisition_channel{
    type: left_outer
    sql_on:${base_customers_distinct.customer_unique_id}= ${group_first_acquisition_channel.customer_unique_id}    ;;
    relationship: many_to_one
  }

  join: brand_first_transaction_date_by_channel{
    type: left_outer
    sql_on:${base_customers_distinct.brand_customer_unique_id}= ${brand_first_transaction_date_by_channel.brand_customer_unique_id}    ;;
    relationship: one_to_one
  }

  join: group_first_transaction_date_by_channel{
    type: left_outer
    sql_on:${base_customers_distinct.customer_unique_id}= ${group_first_transaction_date_by_channel.customer_unique_id}    ;;
    relationship: many_to_one
  }

  join: customer_first_transaction{
    type: left_outer
    sql_on:${base_customers_distinct.brand_customer_unique_id}= ${customer_first_transaction.brand_customer_unique_id}    ;;
    relationship: many_to_one
  }
  join: customer_item_path{
    type: left_outer
    sql_on:${base_customers_distinct.brand_customer_unique_id}= ${customer_item_path.brand_customer_unique_id}    ;;
    relationship: many_to_one
  }
  join: customer_brand_last_transaction{
    type: left_outer
    sql_on:${base_customers_distinct.brand_customer_unique_id}= ${customer_brand_last_transaction.brand_customer_unique_id}    ;;
    relationship: many_to_one
  }
  join: rfm_scoring{
    type: left_outer
    sql_on:${base_customers_distinct.brand_customer_unique_id}= ${rfm_scoring.brand_customer_unique_id}    ;;
    relationship: many_to_one
  }

  join: group_lifetime_aggregations{
    type: left_outer
    sql_on:${base_customers_distinct.customer_unique_id}=
    ${group_lifetime_aggregations.customer_unique_id}    ;;
    relationship: many_to_one
  }

  join: brand_lifetime_aggregations{
    type: left_outer
    sql_on:${base_customers_distinct.brand_customer_unique_id}=
    ${brand_lifetime_aggregations.brand_customer_unique_id}    ;;
    relationship: many_to_one
  }

  join: customer_item_path_next_transaction {
    type: inner
    sql_on:${base_customers_distinct.brand_customer_unique_id}=
      ${customer_item_path_next_transaction.brand_customer_unique_id}    ;;
    relationship: many_to_one

}

  join: group_metrics {
    type: left_outer
    sql_on:${base_customers_factretailsales.customer_unique_id}=
      ${group_metrics.customer_unique_id} AND
      ${base_customers_factretailsales.atr_trandate_date}=
      ${group_metrics.atr_trandate};;
    relationship: many_to_one

  }

  join: brand_omnichannel {
    type: left_outer
    sql_on:${base_customers_distinct.brand_customer_unique_id}=
      ${brand_omnichannel.brand_customer_unique_id};;
    relationship: many_to_one

  }

  join: sales_omnichannel {
    type: left_outer
    sql_on:${base_customers_factretailsales.brand_customer_unique_id}=
      ${sales_omnichannel.brand_customer_unique_id}
      AND ${base_customers_factretailsales.bk_businessdate} = ${sales_omnichannel.bk_businessdate} ;;
    relationship: many_to_one

  }

  join: clientelling_segments {
    type: left_outer
    sql_on:${base_customers_distinct.brand_customer_unique_id}=
      ${clientelling_segments.brand_customer_unique_id};;
    relationship: many_to_one

  }


  join: dim_employee_details {
    fields: [dim_employee_details.salesperson_name]
    view_label: "Retail Sales"
    type: left_outer
    sql_on:${base_customers_factretailsales.atr_salesperson}=
    ${dim_employee_details.empcode} ;;
    relationship: many_to_one
  }

join: golden_circle_txn  {
  type: left_outer
  sql_on: ${base_customers_factretailsales.atr_tranno}=${golden_circle_txn.atr_tranno}
  and ${base_customers_factretailsales.bk_businessdate}=${golden_circle_txn.bk_businessdate}
    and ${base_customers_factretailsales.bk_storeid}=${golden_circle_txn.bk_storeid};;
}

join: cust_first_date_by_store {
  type: left_outer
  sql_on: ${locations.store}=${cust_first_date_by_store.bk_storeid}
  and  ${dim_calendar.dim_date_key}=${cust_first_date_by_store.first_date};;
}

join: vertical_nvr {
  type: left_outer
  sql_on: ${base_customers_factretailsales.id}=${vertical_nvr.surrogate_key_generated_uid};;
  relationship: one_to_one
}


}


explore: campaigns_eda {
  fields: [
    ALL_FIELDS*,
    -base_customers_distinct.group_total_customer_count,
    -base_customers_distinct.group_transacted_customer_count,
    -base_customers_distinct.birth_date,
    -base_customers_distinct.birth_month,
    -base_customers_distinct.birth_quarter,
    -base_customers_distinct.birth_week,
    -base_customers_distinct.birth_raw,
    -base_customers_distinct.birth_year,
    -base_customers_distinct.muse_registration_date,
    -base_customers_distinct.muse_registration_month,
    -base_customers_distinct.muse_registration_quarter,
    -base_customers_distinct.muse_registration_week,
    -base_customers_distinct.muse_registration_raw,
    -base_customers_distinct.muse_registration_year,
    -base_customers_distinct.Is_CRM_member,
    -base_customers_distinct.contactability,
    -base_customers_distinct.Top_customers_UAE,
    -base_customers_distinct.Top_customers_KSA,
    -base_customers_distinct.Top_customers_KUW,
    -base_customers_distinct.muse_retained_members,
    -base_customers_distinct.muse_reactivated_members,
    -base_customers_distinct.muse_retained_members_static,
    -base_customers_distinct.non_muse_retained_members_static,
    -base_customers_distinct.non_muse_retained_members,
    -base_customers_distinct.new_muse_exclusive,
    -base_customers_distinct.new_crm_exclusive,
    -base_customers_distinct.is_muse_elite,
    -dim_retail_product.Category_basedon_division_TB,
    -base_customers_distinct.is_generic_customer,
    -base_customers_distinct.Is_Muse_member,
    -base_customers_distinct.Is_CRM_member,
    -base_customers_distinct.group_total_customer_count,
    -base_customers_distinct.count_group_customers,
    -base_customers_distinct.retained_customer,
    -base_customers_distinct.is_new_customer_universal,
    -base_customers_distinct.retained_customer_universal,
    -base_customers_distinct.ret_range_start,
    -base_customers_distinct.ret_range_end

  ]
  label: "Campaigns analysis"
  view_name:  campaign_frs_sales_base

  join: prod_campaigns_sales_last_send_attribution {
    type: left_outer
    sql_on: ${prod_campaigns_sales_last_send_attribution.brand_id}=${campaign_frs_sales_base.brand_id}
      AND ${prod_campaigns_sales_last_send_attribution.email_date_sent} = ${campaign_frs_sales_base.frs_transaction_date}
      AND ${prod_campaigns_sales_last_send_attribution.country} = ${campaign_frs_sales_base.country};;
    relationship: one_to_many
  }

  access_filter: {
    field: campaign_frs_sales_base.bu_brand_name
    user_attribute: brand
  }

  join: base_customers_distinct {
    type: inner
    sql_on: ${prod_campaigns_sales_last_send_attribution.customer_unique_id}=${base_customers_distinct.customer_unique_id}
      AND ${prod_campaigns_sales_last_send_attribution.brand_name} = ${base_customers_distinct.brand_name};;
    relationship: many_to_one
  }

  join: brand_lifetime_aggregations {
    type: left_outer
    sql_on: ${prod_campaigns_sales_last_send_attribution.brand_customer_unique_id}=${base_customers_distinct.brand_customer_unique_id};;
    relationship: many_to_one
  }


  join: dim_retail_product {
    type: inner
    sql_on: ${prod_campaigns_sales_last_send_attribution.product_id}=${dim_retail_product.item};;
    relationship: many_to_one
  }

  join: stg_campaigns_sales_ga {
    type: left_outer
    sql_on: ${prod_campaigns_sales_last_send_attribution.campaign_name}=${stg_campaigns_sales_ga.ga_campaign_name}
      AND ${prod_campaigns_sales_last_send_attribution.email}=${stg_campaigns_sales_ga.ga_email};;
    relationship: many_to_many

  }

}

#######################################
#####            MUSE GA          #####
#######################################

explore: stg_comm_unified_ga_muse {
  label: "Muse GA"
  group_label: "Group eCommerce"
}



#######################################
#####     Data Validation         #####
#######################################

explore: data_validation_customer {
  label: "Data Validation"
  view_name: data_validation_customer
}







explore: prospects{
  from: base_customers_focus
  label: "Prospects 360 (BETA)"

  always_filter: {
    filters: [is_generic_customer : "no"]}



  join: min_brands  {

    type: inner
    sql_on: ${prospects.brand_id}=${min_brands.min_brand} ;;

  }



  join: locations {
    type: inner
    sql_on: ${min_brands.bk_brand}=${locations.district};;
    relationship: :many_to_one
  }



  join: base_customers_sf_contact {
    type:  left_outer
    sql_on: ${base_customers_sf_contact.brand_customer_unique_id}=${prospects.brand_customer_unique_id}
      ;;
    relationship: many_to_many
  }


join: base_customers_factretailsales_cx {
  type: left_outer
  sql_on: ${prospects.brand_customer_unique_id}=${base_customers_factretailsales_cx.brand_customer_unique_id}
  and ${locations.store}=${base_customers_factretailsales_cx.bk_storeid};;
}

  join: transaction_attributes {
    type:  left_outer
    sql_on:  ${base_customers_factretailsales_cx.id}= ${transaction_attributes.id};;
    relationship: one_to_one
  }



}



explore: base_customers_focus{
  label: "CX Focus (beta)"

  always_filter: {
    filters: [is_generic_customer : "no"]}


  access_filter: {
    field: dim_alternate_bu_hierarchy.brand
    user_attribute: brand
  }


  join: cx_responses_denorm {

    type: left_outer
    sql_on:
          ${base_customers_focus.brand_customer_unique_id}=${cx_responses_denorm.brand_customer_unique_id}
          ;;
    relationship: one_to_many
  }


  join: base_customers_factretailsales_cx {

    type: left_outer
    sql_on: ${base_customers_focus.brand_customer_unique_id}=${base_customers_factretailsales_cx.brand_customer_unique_id} ;;
    relationship: one_to_many
  }


  join: transaction_attributes {
    type:  left_outer
    sql_on:  ${base_customers_factretailsales_cx.id}= ${transaction_attributes.id};;
    relationship: one_to_one
  }


  join: locations {
    type: inner
    sql_on: ${cx_responses_denorm.store_id_c}=cast(${locations.store} as string);;
    relationship: :many_to_one
  }

  join: dim_alternate_bu_hierarchy {
    type:  inner
    sql_on: cast(${locations.store} as string)=${dim_alternate_bu_hierarchy.store_code} ;;
    relationship: one_to_one
  }


  join: dim_calendar {
    type:  inner
    sql_on:

      ${dim_calendar.dim_date_key}=${base_customers_factretailsales_cx.bk_businessdate}
      and  ${dim_calendar.dim_date_key}=${cx_responses_denorm.tran_date_key}
    ;;
  }

  join: cx_trainings {
    type: inner
    sql_on: ${cx_trainings.locations_district_name}=cast(${locations.district_name} as string);;
    relationship: :many_to_many
  }


}


explore: dim_calendar {
  label: "(dev) Customer 360 Time Series"


  join: sales_unified_no_item_rollup {
  type: :left_outer
    sql_on:${dim_calendar.dim_date_key}=${sales_unified_no_item_rollup.bk_businessdate}  ;;
    relationship: one_to_many
  }
  join: base_customers_distinct{

    type: left_outer
    sql_on:${sales_unified_no_item_rollup.brand_customer_unique_id} = ${base_customers_distinct.brand_customer_unique_id}  ;;
    relationship: many_to_one
  }

  join: locations {
    type: inner
    sql_on: ${sales_unified_no_item_rollup.bk_storeid}=${locations.store};;
    relationship: :many_to_one
  }





join: customer_day_store {
  type: left_outer

  sql_on:
   ${locations.store}=${customer_day_store.bk_storeid}

  and

 case when ${dim_calendar.selected_granularity}=1 then

    ${dim_calendar.cal_year}=${customer_day_store.next_month_year}
    and
    ${dim_calendar.cal_month_no}=${customer_day_store.next_month}



when ${dim_calendar.selected_granularity}=2 then

     ${dim_calendar.cal_quarter_no}=${customer_day_store.next_quarter}
    and  ${dim_calendar.cal_year}=${customer_day_store.next_quarter_year}

  when ${dim_calendar.selected_granularity}=3 then

      case when extract(month from  (PARSE_DATE("%Y%m%d", cast(${dim_calendar.dim_date_key} as string))))<7 then 1 else 2 end  =${customer_day_store.next_half}
      and ${dim_calendar.cal_year}=${customer_day_store.next_half_year}

  when ${dim_calendar.selected_granularity}=4 then

     ${dim_calendar.cal_year} =${customer_day_store.next_year}

 when ${dim_calendar.selected_granularity}=0 then

      ${customer_day_store.bk_businessdate}<${dim_calendar.ret_range_end_int}

      and ${dim_calendar.ret_range_start_int}<=${customer_day_store.bk_businessdate}

  else
         ${dim_calendar.ret_range_end_int}<${customer_day_store.bk_businessdate}

      and ${dim_calendar.ret_range_start_int}>=${customer_day_store.bk_businessdate}

  end
  ;;
}


join: retained_cust {
    type: left_outer

    sql_on:

   ${sales_unified_no_item_rollup.customer_unique_id}=${retained_cust.customer_unique_id_prev}
      and

 case when ${dim_calendar.selected_granularity}=1 then

    ${dim_calendar.cal_year}=${retained_cust.next_month_year}
    and
    ${dim_calendar.cal_month_no}=${retained_cust.next_month}



when ${dim_calendar.selected_granularity}=2 then

     ${dim_calendar.cal_quarter_no}=${retained_cust.next_quarter}
    and  ${dim_calendar.cal_year}=${retained_cust.next_quarter_year}

  when ${dim_calendar.selected_granularity}=3 then

      case when extract(month from  (PARSE_DATE("%Y%m%d", cast(${dim_calendar.dim_date_key} as string))))<7 then 1 else 2 end  =${retained_cust.next_half}
      and ${dim_calendar.cal_year}=${retained_cust.next_half_year}

  when ${dim_calendar.selected_granularity}=4 then

     ${dim_calendar.cal_year} =${retained_cust.next_year}

 when ${dim_calendar.selected_granularity}=0 then

     ${retained_cust.bk_businessdate}<${dim_calendar.ret_range_end_int}

      and ${dim_calendar.ret_range_start_int}<=${retained_cust.bk_businessdate}

  else
         ${dim_calendar.ret_range_end_int}<${retained_cust.bk_businessdate}

      and ${dim_calendar.ret_range_start_int}>=${retained_cust.bk_businessdate}

  end
  ;;
}






  join: cust_first_date_by_store {
    type: left_outer
    sql_on: ${locations.store}=${cust_first_date_by_store.bk_storeid}
      and  ${dim_calendar.dim_date_key}=${cust_first_date_by_store.first_date};;
  }

}
