connection: "chb-prod-data-cust"
include: "/customer_views/*.*"

include: "/customer_views/base_customers_metrics/*.*"
include: "/_common/*.*"
include: "../comm_views/*.view"

datagroup: chb_customer_default_datagroup {
  # sql_trigger: SELECT MAX(id) FROM etl_log;;
  max_cache_age: "1 hour"
}


week_start_day: sunday






# explore: base_cust_frs_unified {
#   label: "DEV Customer 360"
#   always_filter: {
#     filters: [is_generic_customer : "no", base_customers_distinct.is_generic_customer : "no"]}


#   access_filter: {
#     field: dim_alternate_bu_hierarchy.country
#     user_attribute: country
#   }
#   access_filter: {
#     field: dim_alternate_bu_hierarchy.vertical
#     user_attribute: vertical
#   }
#   access_filter: {
#     field: dim_alternate_bu_hierarchy.brand
#     user_attribute: brand
#   }


#   join: base_customers_distinct{

#     type: left_outer
#     sql_on:${base_customers_distinct.brand_customer_unique_id}= ${base_cust_frs_unified.brand_customer_unique_id}    ;;
#     relationship: one_to_many
#   }

#   join: cx_responses_denorm {

#     type: left_outer
#     sql_on: ${cx_responses_denorm.frs_tran_key} = ${base_cust_frs_unified.id}
#     and ${cx_responses_denorm.crm_customer_id}=${base_customers_distinct.speedbus_id}
#     ;;
#     relationship: one_to_many
#   }

#   join: locations {
#     type: inner
#     sql_on: ${base_cust_frs_unified.bk_storeid}=${locations.store};;
#     relationship: :many_to_one
#   }

#   join: dim_retail_product {
#     type: inner
#     sql_on: cast(${base_cust_frs_unified.bk_productid} as string)=${dim_retail_product.item} ;;
#     relationship: many_to_one

#   }


#   join: dev_c360_new_returning {
#     type: left_outer
#     sql_on:  ${base_cust_frs_unified.brand_customer_unique_id} =${dev_c360_new_returning.brand_customer_unique_id}
#       and ${base_cust_frs_unified.date_group}=${dev_c360_new_returning.date_group};;
#   }

  # join: customer_360_facts {
  #   type: left_outer
  #   sql_on: ${base_customers_distinct.brand_customer_unique_id} =${customer_360_facts.brand_customer_unique_id} ;;
  #   relationship: one_to_one
  # }


  # join: customer_360_group_facts {
  #   type: left_outer
  #   sql_on: ${base_customers_distinct.customer_unique_id} =${customer_360_group_facts.customer_unique_id} ;;
  #   relationship: many_to_one
  # }

  # join: customer_360_facts_lifetime {
  #   type: left_outer
  #   sql_on: ${base_customers_distinct.brand_customer_unique_id} =${customer_360_facts_lifetime.brand_customer_unique_id} ;;
  #   relationship: one_to_one
  # }

#   join: transaction_attributes {
#     type:  left_outer
#     sql_on:  ${base_cust_frs_unified.id}= ${transaction_attributes.id};;
#     relationship: one_to_one
#   }

# #   join: customer_facts_2 {
# #     type: left_outer
# #     sql_on: ${base_customers.bk_customer} =${customer_facts.bk_customer} ;;
# #     relationship: one_to_one
# #   }

#   join: dim_alternate_bu_hierarchy {
#     type:  inner
#     sql_on: cast(${locations.store} as string)=${dim_alternate_bu_hierarchy.store_code} ;;
#     relationship: one_to_one
#   }

#   # segmentation: lifecycle, value, experience(empty), vip
#   join: current_segments {
#     type:  left_outer
#     sql_on: ${current_segments.brand_customer_unique_id}=${base_cust_frs_unified.brand_customer_unique_id}
#       ;;
#     relationship: many_to_one
#   }

#   join: segmentation_history {
#     type:  left_outer
#     sql_on: ${segmentation_history.brand_customer_unique_id}=${base_cust_frs_unified.brand_customer_unique_id}
#       ;;
#     relationship: many_to_many
#   }


#   join : cs_base_customers {
#     type:  left_outer
#     sql_on: ${cs_base_customers.brand_customer_unique_id}=${base_cust_frs_unified.brand_customer_unique_id}
#       ;;
#     relationship: many_to_many
#   }

#   join: adjust_levelshoes{
#     type: left_outer
#     sql_on:${base_cust_frs_unified.brand_customer_unique_id} = ${adjust_levelshoes.brand_customer_unique_id}
#       AND ${base_cust_frs_unified.atr_cginvoiceno} = ${adjust_levelshoes.order_id};;
#     relationship: many_to_one
#   }

#   join: communication_consent{
#     type: left_outer
#     sql_on:${base_customers_distinct.brand_customer_unique_id}= ${communication_consent.brand_customer_unique_id}    ;;
#     relationship: one_to_many
#   }

#   join: brand_acquisition_channel{
#     type: left_outer
#     sql_on:${base_customers_distinct.brand_customer_unique_id}= ${brand_acquisition_channel.brand_customer_unique_id}    ;;
#     relationship: one_to_one
#   }

#   join: group_acquisition_channel{
#     type: left_outer
#     sql_on:${base_customers_distinct.customer_unique_id}= ${group_acquisition_channel.customer_unique_id}    ;;
#     relationship: many_to_one
#   }

#   join: customer_first_transaction{
#     type: left_outer
#     sql_on:${base_customers_distinct.brand_customer_unique_id}= ${customer_first_transaction.brand_customer_unique_id}    ;;
#     relationship: many_to_one
#   }
#   join: customer_lifetime_aggregations{
#     type: left_outer
#     sql_on:${base_customers_distinct.brand_customer_unique_id}= ${customer_lifetime_aggregations.brand_customer_unique_id}    ;;
#     relationship: many_to_one
#   }

# }





#######################################
#####     Cohorts Analysis        #####
#######################################

explore: cohorts_customer {
  label: "(DEV) Cohorts Analysis"

  view_name: cohorts
  join: segmentation_flow {
    type:  left_outer
    sql_on: ${cohorts.brand_id}=${segmentation_flow.brand_id} AND
          TRIM(${cohorts.period})=TRIM(${segmentation_flow.period}) AND ${cohorts.n} = 0
            ;;
    relationship: one_to_one
  }
}




#######################################
#####     DEV Campaign Analysis   #####
#######################################

explore: campaigns_eda {
  fields: [
    ALL_FIELDS*,
    -base_customers_distinct.group_total_customer_count,
    -base_customers_distinct.group_transacted_customer_count,
    -base_customers_distinct.birth_date,
    -base_customers_distinct.birth_month,
    -base_customers_distinct.birth_quarter,
    -base_customers_distinct.birth_week,
    -base_customers_distinct.birth_raw,
    -base_customers_distinct.birth_year,
    -base_customers_distinct.muse_registration_date,
    -base_customers_distinct.muse_registration_month,
    -base_customers_distinct.muse_registration_quarter,
    -base_customers_distinct.muse_registration_week,
    -base_customers_distinct.muse_registration_raw,
    -base_customers_distinct.muse_registration_year,
    -base_customers_distinct.Is_CRM_member,
    -base_customers_distinct.contactability,
    -base_customers_distinct.Top_customers_UAE,
    -base_customers_distinct.Top_customers_KSA,
    -base_customers_distinct.Top_customers_KUW,
    -base_customers_distinct.muse_retained_members,
    -base_customers_distinct.muse_reactivated_members,
    -base_customers_distinct.muse_retained_members_static,
    -base_customers_distinct.non_muse_retained_members_static,
    -base_customers_distinct.non_muse_retained_members,
    -base_customers_distinct.new_muse_exclusive,
    -base_customers_distinct.new_crm_exclusive,
    -base_customers_distinct.is_muse_elite,
    -dim_retail_product.Category_basedon_division_TB,
    -base_customers_distinct.is_generic_customer,
    -base_customers_distinct.Is_Muse_member,
    -base_customers_distinct.Is_CRM_member,
    -base_customers_distinct.group_total_customer_count,
    -base_customers_distinct.count_group_customers,
    -base_customers_distinct.retained_customer,
    -base_customers_distinct.is_new_customer_universal,
    -base_customers_distinct.retained_customer_universal,
    -base_customers_distinct.ret_range_start,
    -base_customers_distinct.ret_range_end

  ]
  label: "DEV Campaigns analysis"
  view_name:  campaign_frs_sales_base

  join: prod_campaigns_sales_last_send_attribution {
    type: left_outer
    sql_on: ${prod_campaigns_sales_last_send_attribution.brand_id}=${campaign_frs_sales_base.brand_id}
      AND ${prod_campaigns_sales_last_send_attribution.email_date_sent} = ${campaign_frs_sales_base.frs_transaction_date}
      AND ${prod_campaigns_sales_last_send_attribution.country} = ${campaign_frs_sales_base.country};;
    relationship: one_to_many
  }

  access_filter: {
    field: campaign_frs_sales_base.bu_brand_name
    user_attribute: brand
  }

  join: base_customers_distinct {
    type: inner
    sql_on: ${prod_campaigns_sales_last_send_attribution.customer_unique_id}=${base_customers_distinct.customer_unique_id}
    AND ${prod_campaigns_sales_last_send_attribution.brand_name} = ${base_customers_distinct.brand_name};;
    relationship: many_to_one
  }

  join: brand_lifetime_aggregations {
    type: left_outer
    sql_on: ${prod_campaigns_sales_last_send_attribution.brand_customer_unique_id}=${base_customers_distinct.brand_customer_unique_id};;
    relationship: many_to_one
  }


  join: dim_retail_product {
    type: inner
    sql_on: ${prod_campaigns_sales_last_send_attribution.product_id}=${dim_retail_product.item};;
    relationship: many_to_one
  }

  join: stg_campaigns_sales_ga {
    type: left_outer
    sql_on: ${prod_campaigns_sales_last_send_attribution.campaign_name}=${stg_campaigns_sales_ga.ga_campaign_name}
      AND ${prod_campaigns_sales_last_send_attribution.email}=${stg_campaigns_sales_ga.ga_email};;
    relationship: many_to_many

  }

}







# explore: base_customers_factretailsales_v2{

#   from: base_customers_factretailsales_v2
#   label: "Customer 360 V2"
#   always_filter: {
#     filters: [is_generic_customer : "no", base_customers_distinct_v2.is_generic_customer : "no", is_known_cust_txn : "yes"]}


#   access_filter: {
#     field: dim_alternate_bu_hierarchy.country
#     user_attribute: country
#   }
#   access_filter: {
#     field: dim_alternate_bu_hierarchy.vertical
#     user_attribute: vertical
#   }
#   access_filter: {
#     field: dim_alternate_bu_hierarchy.brand
#     user_attribute: brand
#   }

#   join: dim_calendar {
#     type:  inner
#     sql_on:

#       ${dim_calendar.dim_date_key}=${base_customers_factretailsales_v2.bk_businessdate}

#     ;;
#   }

# # join: rolling_retention_rate {
# #   type: left_outer
# #   sql_on: ${locations.district_name}=${rolling_retention_rate.district_name}
# # ;;
# # }

#   join: base_customers_distinct_v2{

#     type: left_outer
#     sql_on:${base_customers_factretailsales_v2.brand_customer_unique_id} = ${base_customers_distinct_v2.brand_customer_unique_id}  ;;
#     relationship: one_to_many
#   }

#   join: locations {
#     type: inner
#     sql_on: ${base_customers_factretailsales_v2.bk_storeid}=${locations.store};;
#     relationship: :many_to_one
#   }


#   join: dim_retail_product_unified {
#     type: inner
#     sql_on: cast(${base_customers_factretailsales_v2.bk_productid} as string)=${dim_retail_product_unified.item} ;;
#     relationship: many_to_one

#   }


#   join: dim_retail_product {
#     type: inner
#     sql_on: cast(${base_customers_factretailsales_v2.bk_productid} as string)=${dim_retail_product.item} ;;
#     relationship: many_to_one

#   }

#   join: customer_360_facts_v2 {
#     type: left_outer
#     sql_on: ${base_customers_distinct_v2.brand_customer_unique_id} =${customer_360_facts_v2.brand_customer_unique_id} ;;
#     relationship: one_to_one
#   }


#   join: customer_360_group_facts_v2 {
#     type: left_outer
#     sql_on: ${base_customers_distinct_v2.customer_unique_id} =${customer_360_group_facts_v2.customer_unique_id} ;;
#     relationship: many_to_one
#   }

#   join: customer_360_facts_lifetime_v2 {
#     type: left_outer
#     sql_on: ${base_customers_distinct_v2.brand_customer_unique_id} =${customer_360_facts_lifetime_v2.brand_customer_unique_id} ;;
#     relationship: one_to_one
#   }

#   join: transaction_attributes {
#     type:  left_outer
#     sql_on:  ${base_customers_factretailsales_v2.id}= ${transaction_attributes.id};;
#     relationship: one_to_one
#   }

#   join: c360_new_returning_v2 {
#     type: left_outer
#     sql_on:  ${base_customers_factretailsales_v2.brand_customer_unique_id} =${c360_new_returning_v2.brand_customer_unique_id}
#       and ${base_customers_factretailsales_v2.date_group}=${c360_new_returning_v2.date_group};;
#   }



#   join: customer_cross_brand_v2 {
#     type: left_outer
#     sql_on:  ${base_customers_factretailsales_v2.customer_unique_id} =${customer_cross_brand_v2.customer_unique_id}
#       ;;
#   }




#   join: dim_alternate_bu_hierarchy {
#     type:  inner
#     sql_on: cast(${locations.store} as string)=${dim_alternate_bu_hierarchy.store_code} ;;
#     relationship: one_to_one
#   }

#   # segmentation: lifecycle, value, experience(empty), vip
#   join: current_segments {
#     type:  left_outer
#     sql_on: ${current_segments.brand_customer_unique_id}=${base_customers_factretailsales_v2.brand_customer_unique_id}
#       ;;
#     relationship: many_to_one
#   }

#   join: segmentation_history {
#     type:  left_outer
#     sql_on: ${segmentation_history.brand_customer_unique_id}=${base_customers_factretailsales_v2.brand_customer_unique_id}
#       ;;
#     relationship: many_to_many
#   }

#   join: base_customers_sf_contact {
#     type:  left_outer
#     sql_on: ${base_customers_sf_contact.brand_customer_unique_id}=${base_customers_factretailsales_v2.brand_customer_unique_id}
#       ;;
#     relationship: many_to_many
#   }

#   join: communication_consent{
#     type: left_outer
#     sql_on:${base_customers_distinct_v2.brand_customer_unique_id}= ${communication_consent.brand_customer_unique_id}    ;;
#     relationship: one_to_many
#   }

#   join: brand_first_acquisition_channel{
#     type: left_outer
#     sql_on:${base_customers_distinct_v2.brand_customer_unique_id}= ${brand_first_acquisition_channel.brand_customer_unique_id}    ;;
#     relationship: one_to_one
#   }

#   join: group_first_acquisition_channel{
#     type: left_outer
#     sql_on:${base_customers_distinct_v2.customer_unique_id}= ${group_first_acquisition_channel.customer_unique_id}    ;;
#     relationship: many_to_one
#   }

#   join: brand_first_transaction_date_by_channel{
#     type: left_outer
#     sql_on:${base_customers_distinct_v2.brand_customer_unique_id}= ${brand_first_transaction_date_by_channel.brand_customer_unique_id}    ;;
#     relationship: one_to_one
#   }

#   join: group_first_transaction_date_by_channel{
#     type: left_outer
#     sql_on:${base_customers_distinct_v2.customer_unique_id}= ${group_first_transaction_date_by_channel.customer_unique_id}    ;;
#     relationship: many_to_one
#   }

#   join: customer_first_transaction{
#     type: left_outer
#     sql_on:${base_customers_distinct_v2.brand_customer_unique_id}= ${customer_first_transaction.brand_customer_unique_id}    ;;
#     relationship: many_to_one
#   }
#   join: customer_item_path{
#     type: left_outer
#     sql_on:${base_customers_distinct_v2.brand_customer_unique_id}= ${customer_item_path.brand_customer_unique_id}    ;;
#     relationship: many_to_one
#   }
#   join: customer_brand_last_transaction{
#     type: left_outer
#     sql_on:${base_customers_distinct_v2.brand_customer_unique_id}= ${customer_brand_last_transaction.brand_customer_unique_id}    ;;
#     relationship: many_to_one
#   }
#   join: rfm_scoring{
#     type: left_outer
#     sql_on:${base_customers_distinct_v2.brand_customer_unique_id}= ${rfm_scoring.brand_customer_unique_id}    ;;
#     relationship: many_to_one
#   }

#   join: group_lifetime_aggregations{
#     type: left_outer
#     sql_on:${base_customers_distinct_v2.customer_unique_id}=
#       ${group_lifetime_aggregations.customer_unique_id}    ;;
#     relationship: many_to_one
#   }

#   join: brand_lifetime_aggregations{
#     type: left_outer
#     sql_on:${base_customers_distinct_v2.brand_customer_unique_id}=
#       ${brand_lifetime_aggregations.brand_customer_unique_id}    ;;
#     relationship: many_to_one
#   }

#   join: customer_item_path_next_transaction {
#     type: inner
#     sql_on:${base_customers_distinct_v2.brand_customer_unique_id}=
#       ${customer_item_path_next_transaction.brand_customer_unique_id}    ;;
#     relationship: many_to_one

#   }

#   join: group_metrics {
#     type: left_outer
#     sql_on:${base_customers_factretailsales_v2.customer_unique_id}=
#       ${group_metrics.customer_unique_id} AND
#       ${base_customers_factretailsales_v2.atr_trandate_date}=
#       ${group_metrics.atr_trandate};;
#     relationship: many_to_one

#   }

#   join: brand_omnichannel {
#     type: left_outer
#     sql_on:${base_customers_distinct_v2.brand_customer_unique_id}=
#       ${brand_omnichannel.brand_customer_unique_id};;
#     relationship: many_to_one

#   }



# }
