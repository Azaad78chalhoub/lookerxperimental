connection: "chb-looker-user-mgt"

include: "../_common/*.view"               # include all views in the views/ folder in this project
# include: "/**/view.lkml"                   # include all views in this project
# include: "my_dashboard.dashboard.lookml"   # include a LookML dashboard called my_dashboard

# # Select the views that should be a part of this model,
# # and define the joins that connect them together.
#
explore: usr_mgt_approved_requests {
  hidden: yes

  label: "User Mgt - Explore"
  view_label: "User Mgt - Approved Requests"
}
