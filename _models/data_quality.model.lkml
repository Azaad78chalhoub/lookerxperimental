connection: "chb-exp-004"
# connection: "chb-prod-supplychain-data"
include: "../dq_views/*.view"
include: "../dq_views/MDM/*.view"
include: "../group_log_views/*"
include: "../_common/*.view"
include: "../_common/dim_suggest/*.view"
include: "../group_supply_chain_views/*"

label: "Data Quality"

#------------- OMS DQ Starts---------------------

#explore for rule%
explore: rule_score_agg{
  label: "Data Quality OMS Rule Scores(BETA)"
  view_label: "Data Quality OMS Rule Scores (BETA)"
  hidden: no

  access_filter: {
    field: bu_brand_for_access_filter
    user_attribute: brand
  }
  access_filter: {
    field: rule_score_agg.vertical_for_access_filter
    user_attribute: vertical
  }

  join:brand_vertical_mapping_log {
    type: left_outer
    relationship: many_to_one
    sql_where: ${rule_score_agg.vertical_for_access_filter} != 'COUNTRY MANAGEMENT' ;;
    sql_on:
      ${rule_score_agg.brand_map}=${brand_vertical_mapping_log.bu_brand};;
  }
  }
#main DQ explore
explore: oms_dq_last7_latest{
  label: "Data Quality OMS (BETA)"
  view_label: "OMS DQ by Rule OMS (BETA)"
  hidden: no

  access_filter: {
    field: bu_brand_for_access_filter
    user_attribute: brand
  }
  access_filter: {
    field: oms_dq_last7_latest.vertical_for_access_filter
    user_attribute: vertical
  }

  join:brand_vertical_mapping_log {
    type: left_outer
    relationship: many_to_one
    sql_where: ${oms_dq_last7_latest.vertical_for_access_filter} != 'COUNTRY MANAGEMENT' ;;
    sql_on:
      ${oms_dq_last7_latest.brand_map}=${brand_vertical_mapping_log.bu_brand};;
  }

}
#------------- OMS DQ Ends---------------------


#------------- MD DQ Starts---------------------
explore: vpn_brand_zone {
  label: "MD VPN Zone Quality"

  # join:dt_distinct_vpn_brand_zone {
  #   type: left_outer
  #   relationship: many_to_one
  #   sql_on:
  #     ${vpn_brand_zone.brand}=${dt_distinct_vpn_brand_zone.brand};;
  # }

join:vpn_brand_zone_agg {
    type: left_outer
    relationship: one_to_one
    sql_on:
      ${vpn_brand_zone.brand}=${vpn_brand_zone_agg.brand}
  and ${vpn_brand_zone.vpn}=${vpn_brand_zone_agg.vpn};;
   }
}
#------------- MD DQ Ends-----------------------



explore: c360_dq_final {
  label: "C360 Data Quality (BETA)"
}

explore: dq_rule_serving_layer1 {
  label: "C360 DQ Serving Layer "
  }
explore: dq_source_full {
    label: "C360 DQ Source"
  }

#------------- PIM - BrandQuad Starts---------------------
explore: dq_brandquad_row {
  label: "PIM-BrandQuad-Data Quality"
}

explore: dq_brandquad_agg {
  label: "PIM-BrandQuad-Data Quality Aggregates"
}
#------------- PIM - BrandQuad Ends---------------------
