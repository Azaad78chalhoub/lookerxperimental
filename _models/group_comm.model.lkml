connection: "chb-prod-stage-comm"
label: "Group Commercial"

include: "/comm_views/*.view.lkml"
include: "../_common/*.view"
include: "../_common/dim_suggest/*.view"
include: "../customer_views/*.view"
include: "../group_supply_chain_views/*.view"


week_start_day: sunday


  #######################################
  #####        SWAROVSKI            #####
  #######################################


explore: swarovski {
  from: swarovski_facts_union

#   access_filter: {
#     field: source
#     user_attribute: brand
#   }
  access_filter: {
    field: brand_security
    user_attribute: brand
  }
  access_filter: {
    field: vertical_security
    user_attribute: vertical
  }

  label: "Swarovski"
  view_label: "Sales, GA, Marketing"
  group_label: "Group eCommerce"

  join: dim_retail_product {
    relationship: one_to_one
    type: left_outer
    sql_on: ${swarovski.sku}  = ${dim_retail_product.item} ;;
  }

  join: budgetstarget {
    relationship: many_to_one
    type: left_outer
    sql_on: ${swarovski.common_date_date} = ${budgetstarget.business_date} and ${swarovski.store_id} = cast(${budgetstarget.bk_store} as string) ;;
  }

  join: ecomm_customer_transactional_pop {
    relationship: many_to_one
    type: left_outer
    sql_on: "SWAROVSKI" = upper(${ecomm_customer_transactional_pop.brand}) and ${swarovski.order_id} = ${ecomm_customer_transactional_pop.original_order_id}
      and ${swarovski.date_group} = ${ecomm_customer_transactional_pop.date_group} ;;
  }

  join: ecomm_customer_type_pop {
    type: left_outer
    relationship: many_to_one
    sql_on: ${swarovski.date_group} = ${ecomm_customer_transactional_pop.date_group}
      AND  ${ecomm_customer_type_pop.customer_key} = ${ecomm_customer_transactional_pop.customer_key} ;;
  }

  join: ecomm_customer_aggregates_pop{
    type: left_outer
    relationship: many_to_one
    sql_on: ${ecomm_customer_transactional_pop.customer_key} = ${ecomm_customer_aggregates_pop.customer_key} ;;
  }

  # join: swarovski_target_2020 {
  #   type: full_outer
  #   relationship: many_to_one
  #   sql_on: ${stg_comm_swarovski_combined.store_country} = ${swarovski_target_2020.country};;
  # }


  join: oms_manco {
    type: left_outer
    view_label: "Manco cancellations"
    relationship: one_to_many
    sql_on: ${swarovski.order_id} = ${oms_manco.order_id} and ${swarovski.detail_id} = ${oms_manco.detail_id}
      and "SWAROVSKI" = upper(${oms_manco.brand}) and ${swarovski.record_type} = "CANCELLED" ;;
  }

  join: dim_tender_types_comm {
    type: left_outer
    relationship: many_to_one
    sql_on: ${swarovski.payment_detail_tender_type_id} = ${dim_tender_types_comm.tender_type_id} ;;
  }


}


#######################################
######## SWAROVSKI TARGET #############
#######################################

explore: swarovski_target_2020 {
  # access_filter: {
  #   field: brand_security
  #   user_attribute: brand
  # }
  # access_filter: {
  #   field: vertical_security
  #   user_attribute: vertical
  # }
  label: "Swarovski Targets"
  group_label: "Group eCommerce"
  # join: stg_comm_swarovski_combined {
  #   type: left_outer
  #   relationship: one_to_many
  #   sql_on: ${swarovski_target_2020.country} = ${stg_comm_swarovski_combined.store_country} and ${swarovski_target_2020.date_raw} = ${stg_comm_swarovski_combined.common_date_raw} ;;
  # }
}
#######################################
#####      Swarovski Adjust     #####
#######################################

explore: swarovski_adjust {

  access_filter: {
    field: brand_security
    user_attribute: brand
  }

  label: "Swarovski - Mobile App (BETA)"
  group_label: "Mobile App"
}


  #######################################
  #####        LEVELSHOES           #####
  #######################################

explore: lvl_facts_union {

  hidden: no
  access_filter: {
    field: brand_security
    user_attribute: brand
  }
  access_filter: {
    field: vertical_security
    user_attribute: vertical
  }

  label: "Level Shoes"
  group_label: "Group eCommerce"

  view_label: "Sales, GA, Marketing"


  join: budgetstarget {
    relationship: many_to_one
    type: left_outer
    sql_on: ${lvl_facts_union.common_date} = ${budgetstarget.business_date} and ${budgetstarget.bk_store} = 8003 ;;
  }

  join: brandquad_retail_product_level {
    view_label: "Brandquad PIM"
    relationship: many_to_one
    type: left_outer
    sql_on:  ${lvl_facts_union.source_sku} = ${brandquad_retail_product_level.id} ;;
  }

  join: level_vpn_images {
    view_label: "Level Shoes Website Images"
    relationship: many_to_one
    type: left_outer
    sql_on:  ${lvl_facts_union.detail_sku_code} = ${level_vpn_images.vpn} ;;
  }



  join: ecomm_customer_transactional_pop {
    relationship: many_to_one
    type: left_outer
    sql_on: "LEVEL SHOES" = upper(${ecomm_customer_transactional_pop.brand}) and ${lvl_facts_union.order_id} = ${ecomm_customer_transactional_pop.original_order_id}
      and ${lvl_facts_union.date_group} = ${ecomm_customer_transactional_pop.date_group} ;;
  }
  join: ecomm_customer_type_pop {
    type: left_outer
    relationship: many_to_one
    sql_on: ${lvl_facts_union.date_group} = ${ecomm_customer_transactional_pop.date_group}
      AND  ${ecomm_customer_type_pop.customer_key} = ${ecomm_customer_transactional_pop.customer_key} ;;
  }

  join: ecomm_customer_aggregates_pop{
    type: left_outer
    relationship: many_to_one
    sql_on: ${ecomm_customer_transactional_pop.customer_key} = ${ecomm_customer_aggregates_pop.customer_key} ;;
  }

  join: level_shoes_coupon_codes{
    type: left_outer
    view_label: "Level Shoes Coupon Codes"
    relationship: many_to_one
    sql_on:   ${lvl_facts_union.coupon_code_fixed}=${level_shoes_coupon_codes.coupons} ;;
  }

  join: oms_manco {
    type: left_outer
    view_label: "Manco cancellations"
    relationship: one_to_many
    sql_on: ${lvl_facts_union.order_id} = ${oms_manco.order_id} and ${lvl_facts_union.detail_id} = ${oms_manco.detail_id}
      and "LEVEL SHOES" = upper(${oms_manco.brand}) and ${lvl_facts_union.record_type} = "CANCELLED" ;;
  }

  join: level_shoes_daily_targets {
    type: full_outer
    view_label: "Daily Targets"
    relationship: many_to_one
    sql_on: ${lvl_facts_union.common_raw} = ${level_shoes_daily_targets.budget_raw}
            and LOWER(${brandquad_retail_product_level.zone_code}) = LOWER(${level_shoes_daily_targets.zone})
            ;;
  }

  join: level_shoes_soh {
    type: left_outer
    view_label: "Stock On Hand"
    relationship: many_to_many
    sql_on: ${lvl_facts_union.sku_order} = ${level_shoes_soh.rms_sku};;
  }
}

explore: stg_comm_lvl_ga_sku {
  hidden: no
  access_filter: {
    field: brand_security
    user_attribute: brand
  }
  access_filter: {
    field: vertical_security
    user_attribute: vertical
  }

  label: "Level Shoes - VPN"
  group_label: "Group eCommerce"

  view_label: "GA - VPN"

  join: lvl_catalog_product_flat_1 {
    relationship: many_to_one
    type: left_outer
    sql_on: ${stg_comm_lvl_ga_sku.product_sku} = ${lvl_catalog_product_flat_1.primaryvpn} ;;
  }
}

######################################
#####      Level Shoes PIM       #####
######################################

explore: brandquad_retail_product_level {
  hidden: no

  access_filter: {
    field: brand_security
    user_attribute: brand
  }

  group_label: "Group eCommerce"
  label: "PIM Level Shoes"


  join: level_shoes_soh {
    type: full_outer
    view_label: "Stock On Hand"
    relationship: one_to_many
    sql_on: ${brandquad_retail_product_level.id} = ${level_shoes_soh.rms_sku};;
  }
}

#######################################
#####      Level Shoes Adjust     #####
#######################################

explore: lvl_adjust {

  access_filter: {
    field: brand_security
    user_attribute: brand
  }

  label: "Level Shoes - Mobile App (BETA)"
  group_label: "Mobile App"
}

  #######################################
  #####         Affinity Analysis   #####
  #######################################

explore: affinity {
  from: affinity_order_purchase_affinity
  hidden: yes

  join: total_order_product_A {
    from: affinity_total_order_product
    view_label: "Total Order Product A"
    relationship: many_to_one
    type: left_outer
    sql_on: ${affinity.common_date} = ${total_order_product_A.common_date}
        and ${affinity.brand} = ${total_order_product_A.brand}
        and ${affinity.product_a} = ${total_order_product_A.product_id}
    ;;
  }

  join: total_order_product_B {
    from: affinity_total_order_product
    view_label: "Total Order Product B"
    relationship: many_to_one
    type: left_outer
    sql_on: ${affinity.common_date} = ${total_order_product_B.common_date}
        and ${affinity.brand} = ${total_order_product_B.brand}
        and ${affinity.product_b} = ${total_order_product_B.product_id}
    ;;
  }

  join: basket_metrics_A {
    from: affinity_basket_metrics
    view_label: "Basket Metrics A"
    relationship: many_to_one
    type: left_outer
    sql_on: ${affinity.common_date} = ${basket_metrics_A.common_date}
        and ${affinity.brand} = ${basket_metrics_A.brand}
        and ${affinity.product_a} = ${basket_metrics_A.product_id};;
  }

  join: basket_metrics_B {
    from: affinity_basket_metrics
    view_label: "Basket Metrics B"
    relationship: many_to_one
    type: left_outer
    sql_on: ${affinity.common_date} = ${basket_metrics_B.common_date}
      and ${affinity.brand} = ${basket_metrics_B.brand}
      and ${affinity.product_b} = ${basket_metrics_B.product_id};;
  }

  join: total_orders {
    from: affinity_total_orders
    view_label: "Total orders"
    relationship: many_to_one
    type: left_outer
    sql_on: ${affinity.common_date} = ${total_orders.common_date}
        and ${affinity.brand} = ${total_orders.brand};;
  }

}

  #######################################
  #####         FACES               #####
  #######################################

#-------------------------------------------------------
# Unified view - Sales order - With optimization
#-------------------------------------------------------

explore: faces {
  from: faces_facts_union

  access_filter: {
    field: brand_security
    user_attribute: brand
  }

  access_filter: {
    field: vertical_security
    user_attribute: vertical
  }
  group_label: "Group eCommerce"

  join: akeneo_retail_product_faces {
    view_label: "Akeneo PIM"
    relationship: many_to_one
    type: left_outer
    sql_on: ${faces.sku}  = ${akeneo_retail_product_faces.id} ;;
  }

join: ecomm_customer_transactional_pop {
    relationship: many_to_one
    type: left_outer
    sql_on: "FACES" = upper(${ecomm_customer_transactional_pop.brand}) and ${faces.order_id} = ${ecomm_customer_transactional_pop.original_order_id}
      and ${faces.date_group} = ${ecomm_customer_transactional_pop.date_group} ;;
  }

  join: ecomm_customer_type_pop {
    type: left_outer
    relationship: many_to_one
    sql_on: ${faces.date_group} = ${ecomm_customer_transactional_pop.date_group}
      AND  ${ecomm_customer_type_pop.customer_key} = ${ecomm_customer_transactional_pop.customer_key} ;;
  }

  join: ecomm_customer_aggregates_pop{
    type: left_outer
    relationship: many_to_one
    sql_on: ${ecomm_customer_transactional_pop.customer_key} = ${ecomm_customer_aggregates_pop.customer_key} ;;
  }

  join: budgetstarget {
    relationship: many_to_one
    type: left_outer
    sql_on: ${faces.common_date} = ${budgetstarget.business_date}
        and ${budgetstarget.bk_store} =
          case
            when ${faces.country} = "United Arab Emirates" then 3085
            when ${faces.country} = "Saudi Arabia" then 3077
            when ${faces.country} = "Kuwait" then 3130
            when ${faces.country} = "Egypt" then 3128
            when ${faces.country} = "Qatar" then 3131
          end ;;
  }

  join: oms_manco {
    type: left_outer
    view_label: "Manco cancellations"
    relationship: one_to_many
    sql_on: ${faces.order_id} = ${oms_manco.order_id} and ${faces.detail_id} = ${oms_manco.detail_id}
      and "FACES" = upper(${oms_manco.brand}) and ${faces.record_type} = "CANCELLED" ;;
  }

  join: faces_coupon_code_affiliates {
    type: left_outer
    view_label: "Affiliates Coupon Code"
    relationship: many_to_one
    sql_on: ${faces.promotion_detail_coupon_code} = ${faces_coupon_code_affiliates.coupon_code};;
  }

  join: dim_tender_types_comm {
    #view_label: "SFCC Categories"
    type: left_outer
    relationship: many_to_one
    sql_on: ${faces.payment_detail_tender_type_id} = ${dim_tender_types_comm.tender_type_id} ;;
  }

}

#######################################
#####         TRYANO             #####
#######################################

explore: tryano {
  from: tryano_facts_union_new
  sql_always_where: not(${acq_campaign}="tryano ss21 campaign" and ${acq_source}="tiktok") ;;

  access_filter: {
    field: brand_security
    user_attribute: brand
  }

  access_filter: {
    field: vertical_security
    user_attribute: vertical
  }

  group_label: "Group eCommerce"

  # join: dim_retail_product {
  #   view_label: "Product"
  #   relationship: many_to_one
  #   type: left_outer
  #   sql_on: ${tryano.sku}  = ${dim_retail_product.item} ;;
  # }

  join: akeneo_retail_product_tryano {
    view_label: "Akeneo PIM"
    relationship: many_to_one
    type: left_outer
    sql_on: ${tryano.sku}  = ${akeneo_retail_product_tryano.id} ;;
  }

  join: tryano_sku_image_mapping {
    view_label: "Product Images"
    relationship: many_to_one
    type: left_outer
    sql_on: ${tryano.sku} = ${tryano_sku_image_mapping.sku} ;;
  }

  join: tryano_stock {
    view_label: "Stock"
    relationship: many_to_many
    type: left_outer
    sql_on: ${tryano.sku}  = ${tryano_stock.rms_item} ;;
  }

  join: ecomm_customer_transactional_pop {
    relationship: many_to_one
    type: left_outer
    sql_on: "TRYANO" = upper(${ecomm_customer_transactional_pop.brand})
      and   ${tryano.increment_id} = ${ecomm_customer_transactional_pop.original_order_id}
      and ${tryano.date_group} = ${ecomm_customer_transactional_pop.date_group} ;;
  }

  join: ecomm_customer_type_pop {
    type: left_outer
    relationship: many_to_one
    sql_on: ${tryano.date_group} = ${ecomm_customer_transactional_pop.date_group}
      AND  ${ecomm_customer_type_pop.customer_key} = ${ecomm_customer_transactional_pop.customer_key} ;;
  }

  join: ecomm_customer_aggregates_pop{
    type: left_outer
    relationship: many_to_one
    sql_on: ${ecomm_customer_transactional_pop.customer_key} = ${ecomm_customer_aggregates_pop.customer_key} ;;
  }

  join: budgetstarget {
    relationship: many_to_one
    type: left_outer
    sql_on: ${tryano.common_date} = ${budgetstarget.business_date}
      and ${tryano.rms_store_code} = ${budgetstarget.bk_store} ;;
  }

  join: oms_manco {
    type: left_outer
    view_label: "Manco cancellations"
    relationship: one_to_many
    sql_on: ${tryano.increment_id} = ${oms_manco.order_id} and ${tryano.detail_id} = ${oms_manco.detail_id}
      and "TRYANO" = upper(${oms_manco.brand}) and ${tryano.record_type} = "CANCELLED";;
  }

  join: tryano_coupon_code_affiliates {
    type: left_outer
    view_label: "Affiliates Coupon Code"
    relationship: many_to_one
    sql_on: ${tryano.order_promotion_detail_coupon_code} = ${tryano_coupon_code_affiliates.coupon_code}  ;;
  }

}
#######################################
#####      Tryano Adjust     #####
#######################################

explore: tryano_adjust {

  access_filter: {
    field: brand_security
    user_attribute: brand
  }
  always_filter: {
    filters: [tryano_adjust.created_date: "1 months"]
  }

  label: "Tryano - Mobile App (BETA)"
  group_label: "Mobile App"
}


#######################################
#####      Tryano PIM     #####
#######################################

explore: tryano_PIM {
  from: akeneo_retail_product_tryano
  label: "Tryano - PIM"

  access_filter: {
    field: brand_security
    user_attribute: brand
  }

  access_filter: {
    field: vertical_security
    user_attribute: vertical
  }

  group_label: "Group eCommerce"

  join: tryano_soh {
    view_label: "Stock"
    relationship: one_to_one
    type: left_outer
    sql_on: ${tryano_PIM.id} = ${tryano_soh.sku_soh} ;;
  }

  join: tryano_sku_image_mapping {
    view_label: "Product Images"
    relationship: one_to_one
    type: left_outer
    sql_on: ${tryano_PIM.id} = ${tryano_sku_image_mapping.sku} ;;
  }

  join: tryano_stock {
    view_label: "Stock - New"
    relationship: many_to_many
    type: left_outer
    sql_on: ${tryano_PIM.id}  = ${tryano_stock.rms_item} ;;
  }

  join: tryano_external_concession_stock {
    view_label: "EC Stock"
    relationship: one_to_many
    type: left_outer
    sql_on:  ${tryano_PIM.id} = ${tryano_external_concession_stock.sku} ;;
  }

  join: tryano_ga_product_performance {
    view_label: "GA Product Performance"
    relationship: one_to_many
    type: left_outer
    sql_on: ${tryano_PIM.id} = ${tryano_ga_product_performance.sku} ;;
  }

}

########################################
#####          SFCC                #####
########################################

explore: stg_comm_sfcc_combined_sales_orders_items {

  access_filter: {
    field: brand
    user_attribute: brand
  }

  label: "SFCC brands"
  group_label: "Group eCommerce"

  join: dim_retail_product {
    relationship: one_to_one
    type: left_outer
    sql_on: ${stg_comm_sfcc_combined_sales_orders_items.brand} = upper(${dim_retail_product.brand}) and ${stg_comm_sfcc_combined_sales_orders_items.sku} = ${dim_retail_product.vpn} ;;
  }
}

#######################################
#####         CHANEL - FACES      #####
#######################################


#-------------------------------------------------------
# Unified view - Chanel Performance
#-------------------------------------------------------

explore: stg_comm_chanel_faces_ga_landing_pages {

  view_label: "Landing pages"
  label: "Faces - Chanel"
  group_label: "Group eCommerce"

  access_filter: {
    field: brand_security
    user_attribute: brand
  }

  access_filter: {
    field: vertical_security
    user_attribute: vertical
  }

  join: stg_comm_chanel_faces_ga_brand_pages {
    view_label: "Brand pages"
    relationship: one_to_many
    type: full_outer
    sql_on: ${stg_comm_chanel_faces_ga_landing_pages.date_date} = ${stg_comm_chanel_faces_ga_brand_pages.date_date}
        and ${stg_comm_chanel_faces_ga_landing_pages.campaign} = ${stg_comm_chanel_faces_ga_brand_pages.campaign}
        and ${stg_comm_chanel_faces_ga_landing_pages.device_category} = ${stg_comm_chanel_faces_ga_brand_pages.device_category}
        and ${stg_comm_chanel_faces_ga_landing_pages.source_medium} = ${stg_comm_chanel_faces_ga_brand_pages.source_medium}
        and ${stg_comm_chanel_faces_ga_landing_pages.landing_page_path} = ${stg_comm_chanel_faces_ga_brand_pages.landing_page_path}
        ;;
  }

  join: stg_comm_chanel_faces_ga_transactions {
    view_label: "GA Transactions"
    relationship: one_to_many
    type: full_outer
    sql_on: ${stg_comm_chanel_faces_ga_landing_pages.date_date} = ${stg_comm_chanel_faces_ga_transactions.date_date}
        and ${stg_comm_chanel_faces_ga_landing_pages.campaign} = ${stg_comm_chanel_faces_ga_transactions.campaign}
        and ${stg_comm_chanel_faces_ga_landing_pages.device_category} = ${stg_comm_chanel_faces_ga_transactions.device_category}
        and ${stg_comm_chanel_faces_ga_landing_pages.source_medium} = ${stg_comm_chanel_faces_ga_transactions.source_medium}
        and ${stg_comm_chanel_faces_ga_landing_pages.landing_page_path} = ${stg_comm_chanel_faces_ga_transactions.landing_page_path}
        ;;
  }

  join: faces_chanel_facts_union{
    view_label: "Sales Order Items"
    relationship: one_to_many
    type: full_outer
    sql_on: ${stg_comm_chanel_faces_ga_transactions.transaction_id}=${faces_chanel_facts_union.order_id};;
  }

  join: dim_retail_product {
    view_label: "Product Details"
    relationship: many_to_one
    type: inner
    sql_on: ${faces_chanel_facts_union.sku} = ${dim_retail_product.item}
        and ${dim_retail_product.brand} = 'CHANEL'
        ;;
  }

  join: faces_ga_report_sku {
    view_label: "SKUs View, Cart, and Checkout"
    relationship: one_to_many
    type: left_outer
    sql_on: ${dim_retail_product.item} = ${faces_ga_report_sku.product_sku}
      AND ${faces_chanel_facts_union.common_date} = ${faces_ga_report_sku.date_date};;

  }

  join: ecomm_customer_new_returning {
    view_label: "Customers"
    relationship: many_to_one
    type: left_outer
    sql_on: concat('FACES_', ${faces_chanel_facts_union.order_id}) = ${ecomm_customer_new_returning.order_id}
      and ${ecomm_customer_new_returning.brand} = 'FACES' ;;
  }

  join: ecomm_customer_orders_derived_table {
    view_label: "Customers"
    relationship: many_to_many
    type: left_outer
    sql_on: ${ecomm_customer_orders_derived_table.customer_key} = ${ecomm_customer_new_returning.customer_key}
          {% if faces_chanel_facts_union.common_date._in_query %}
          AND ${faces_chanel_facts_union.common_date} = ${ecomm_customer_orders_derived_table.date}
          {% elsif faces_chanel_facts_union.common_month._in_query %}
          AND ${faces_chanel_facts_union.common_month} = ${ecomm_customer_orders_derived_table.month}
          {% elsif faces_chanel_facts_union.common_week._in_query %}
          AND ${faces_chanel_facts_union.common_week} = ${ecomm_customer_orders_derived_table.week}
          {% elsif faces_chanel_facts_union.common_quarter._in_query %}
          AND ${faces_chanel_facts_union.common_quarter} = ${ecomm_customer_orders_derived_table.quarter}
          {% else %}
          AND ${faces_chanel_facts_union.common_year} = ${ecomm_customer_orders_derived_table.year}
          {% endif %} ;;
  }

}

#######################################
#####     The Deal                #####
#######################################

# explore: the_deal {
#   from: thedeal_facts_union

#   access_filter: {
#     field: brand_security
#     user_attribute: brand
#   }

#   access_filter: {
#     field: vertical_security
#     user_attribute: vertical
#   }


#   join: budgetstarget {
#     relationship: many_to_one
#     type: left_outer
#     sql_on: ${the_deal.common_date} = ${budgetstarget.business_date}
#       and ${the_deal.rms_store_code} = ${budgetstarget.bk_store} ;;
#   }


  explore: the_deal {
    from: thedeal_facts_union

    access_filter: {
      field: brand_security
      user_attribute: brand
    }

    access_filter: {
      field: vertical_security
      user_attribute: vertical
    }

    group_label: "Group eCommerce"

    join: oms_manco {
      type: left_outer
      view_label: "Manco cancellations"
      relationship: one_to_many
      sql_on: ${the_deal.order_id} = ${oms_manco.order_id} and ${the_deal.detail_id} = ${oms_manco.detail_id}
        and "THE DEAL" = upper(${oms_manco.brand}) and ${the_deal.record_type} = "CANCELLED" ;;
    }

    join: dim_retail_product {
      relationship: one_to_one
      type: left_outer
      sql_on: ${the_deal.detail_external_id}  = ${dim_retail_product.item} ;;
    }

    join: budgetstarget {
      relationship: many_to_one
      type: left_outer
      sql_on: ${the_deal.common_datetime_date} = ${budgetstarget.business_date}
        and ${the_deal.store_id} = ${budgetstarget.bk_store} ;;
    }

}

#######################################
#####       Namshi                #####
#######################################

explore: namshi_sales {
  label: "Namshi (BETA)"
  group_label: "Group Commercial"

  access_filter: {
    field: vertical_security
    user_attribute: vertical
  }
  access_filter: {
    field: country_code
    user_attribute: country
  }

  join: namshi_stock {
    relationship: many_to_one
    type: full_outer
    sql_on: ${namshi_sales.sku} = ${namshi_stock.sku};;
  }

}

#######################################
#####        Jira                 #####
#######################################

explore: ecom_jira_epic {
  label: "Ecommerce Jira (BETA)"
  group_label: "Group eCommerce"

}

#######################################
#####       Ounass                #####
#######################################

explore: ounass_orders {
  label: "Ounass (BETA)"
  group_label: "Group Commercial"

  access_filter: {
    field: vertical_security
    user_attribute: vertical
  }
  access_filter: {
    field: country_code
    user_attribute: country
  }

  join: ounass_stock {
    relationship: many_to_one
    type: full_outer
    sql_on: ${ounass_orders.sku} = ${ounass_stock.sku};;
  }

}
#######################################
#####       Farfetch                #####
#######################################

explore: farfetch_sales {
  label: "Farfetch (BETA)"
  group_label: "Group Commercial"

  access_filter: {
    field: vertical_security
    user_attribute: vertical
  }
  access_filter: {
    field: country_code
    user_attribute: country
  }

  join: dim_retail_product {
    type: left_outer
    sql_on:
    ${farfetch_sales.item} = ${dim_retail_product.item};;
    relationship: one_to_one
    }
}



#######################################
#####         Group Overall       #####
#######################################

explore: commercial_unified_facts_union {
  label: "Group Level Report"
  group_label: "Group eCommerce"

  access_filter: {
    field: brand_security
    user_attribute: brand
  }

  access_filter: {
    field: vertical_security
    user_attribute: vertical
  }
  join: budgetstarget {
    relationship: many_to_one
    type: left_outer
    sql_on: ${commercial_unified_facts_union.common_date} = ${budgetstarget.business_date} AND cast(${commercial_unified_facts_union.store_id} as string) = cast(${budgetstarget.bk_store} as string);;
  }
  join: ecomm_customer_transactional_pop {
    relationship: many_to_one
    type: left_outer
    sql_on: CASE WHEN ${commercial_unified_facts_union.brand} = "TORY BURCH" THEN "TORY BURCH" = upper(${ecomm_customer_transactional_pop.brand})
    ELSE ${commercial_unified_facts_union.brand} = upper(${ecomm_customer_transactional_pop.brand}) END and ${commercial_unified_facts_union.order_id} = ${ecomm_customer_transactional_pop.original_order_id}
      and ${commercial_unified_facts_union.date_group} = ${ecomm_customer_transactional_pop.date_group} ;;
  }
  join: ecomm_customer_type_pop {
    type: left_outer
    relationship: many_to_one
    sql_on: ${commercial_unified_facts_union.date_group} = ${ecomm_customer_transactional_pop.date_group}
      AND  ${ecomm_customer_type_pop.customer_key} = ${ecomm_customer_transactional_pop.customer_key} ;;
  }

  join: ecomm_customer_aggregates_pop{
    type: left_outer
    relationship: many_to_one
    sql_on: ${ecomm_customer_transactional_pop.customer_key} = ${ecomm_customer_aggregates_pop.customer_key} ;;
  }

  join: oms_manco {
    type: left_outer
    view_label: "OMS Manco cancellations"
    relationship: one_to_many
    sql_on: ${commercial_unified_facts_union.order_id} = ${oms_manco.order_id}
    and cast(${commercial_unified_facts_union.detail_id} as int64) = ${oms_manco.detail_id}
    and ${commercial_unified_facts_union.brand} = upper(${oms_manco.brand})
    and ${commercial_unified_facts_union.record_type} = "CANCELLED" ;;
  }

  join: commercial_unified_daily_targets {
    type: full_outer
    view_label: "Daily Targets"
    relationship: many_to_one
    sql_on:
           ${commercial_unified_facts_union.store_country} = ${commercial_unified_daily_targets.country_code}
          and ${commercial_unified_facts_union.common_date} = ${commercial_unified_daily_targets.date_date}
          and ${commercial_unified_facts_union.brand} = ${commercial_unified_daily_targets.brand} ;;
  }

}

#########################
####    LOCCITANE    ####
#########################

explore: loccitane_facts_union {

#   access_filter: {
#     field: source
#     user_attribute: brand
#   }
  access_filter: {
    field: brand_security
    user_attribute: brand
  }
  access_filter: {
    field: vertical_security
    user_attribute: vertical
  }

  label: "Loccitane"
  group_label: "Group eCommerce"

  join: budgetstarget {
    relationship: many_to_one
    type: left_outer
    sql_on: ${loccitane_facts_union.common_date_date} = ${budgetstarget.business_date} and ${loccitane_facts_union.store_id} = ${budgetstarget.bk_store} ;;
  }

  join: dim_tender_types_comm {
    type: left_outer
    relationship: many_to_one
    sql_on: ${loccitane_facts_union.tender_type_id} = ${dim_tender_types_comm.tender_type_id} ;;
  }


#     join: ecomm_customer_transactional_pop {
#       relationship: many_to_one
#       type: left_outer
#       sql_on: "LOCCITANE" = upper(${ecomm_customer_transactional_pop.brand}) and ${loccitane_facts_union.order_id} = ${ecomm_customer_transactional_pop.original_order_id}
#         and ${loccitane_facts_union.date_group} = ${ecomm_customer_transactional_pop.date_group} ;;
#     }
#
#     join: ecomm_customer_type_pop {
#       type: left_outer
#       relationship: many_to_one
#       sql_on: ${loccitane_facts_union.date_group} = ${ecomm_customer_transactional_pop.date_group}
#         AND  ${ecomm_customer_type_pop.customer_key} = ${ecomm_customer_transactional_pop.customer_key} ;;
#     }
#
#     join: ecomm_customer_aggregates_pop{
#       type: left_outer
#       relationship: many_to_one
#       sql_on: ${ecomm_customer_transactional_pop.customer_key} = ${ecomm_customer_aggregates_pop.customer_key} ;;
#     }

}


##########################
####    TORY BURCH    ####
##########################

explore: toryburch_facts_union {

#   access_filter: {
#     field: source
#     user_attribute: brand
#   }
  access_filter: {
    field: brand_security
    user_attribute: brand
  }
  access_filter: {
    field: vertical_security
    user_attribute: vertical
  }

  label: "Tory Burch"
  view_label: "Sales, GA, Marketing"
  group_label: "Group eCommerce"

  join: budgetstarget {
    relationship: many_to_one
    type: left_outer
    sql_on: ${toryburch_facts_union.common_date_date} = ${budgetstarget.business_date} and ${toryburch_facts_union.store_id} = ${budgetstarget.bk_store} ;;
  }

  join: ecomm_customer_type_pop {
    type: left_outer
    relationship: many_to_one
    sql_on: ${toryburch_facts_union.date_group} = ${ecomm_customer_transactional_pop.date_group}
      AND  ${ecomm_customer_type_pop.customer_key} = ${ecomm_customer_transactional_pop.customer_key} ;;
  }
  join: ecomm_customer_aggregates_pop{
    type: left_outer
    relationship: many_to_one
    sql_on: ${ecomm_customer_transactional_pop.customer_key} = ${ecomm_customer_aggregates_pop.customer_key} ;;
  }
  join: ecomm_customer_transactional_pop {
    relationship: many_to_one
    type: left_outer
    sql_on: "TORY BURCH" = upper(${ecomm_customer_transactional_pop.brand}) and ${toryburch_facts_union.order_id} = ${ecomm_customer_transactional_pop.original_order_id}
      and ${toryburch_facts_union.date_group} = ${ecomm_customer_transactional_pop.date_group} ;;
  }

  join: dim_alternate_bu_hierarchy {
    type: left_outer
    sql_on:
    cast(${toryburch_facts_union.store_id} as string) = ${dim_alternate_bu_hierarchy.store_code};;
    relationship: many_to_one
  }

  join: dim_tender_types_comm {
    type: left_outer
    relationship: many_to_one
    sql_on: ${toryburch_facts_union.tender_type_id} = ${dim_tender_types_comm.tender_type_id} ;;
  }

  join: oms_manco {
    type: left_outer
    view_label: "OMS Manco cancellations"
    relationship: one_to_many
    sql_on: ${toryburch_facts_union.order_id} = ${oms_manco.order_id}
          and cast(${toryburch_facts_union.detail_id} as int64) = ${oms_manco.detail_id}
          and "TORY BURCH" = upper(${oms_manco.brand})
          and ${toryburch_facts_union.record_type} = "CANCELLED" ;;
  }

}

######################################
#######      Penhaligons      ########
######################################

explore: penhaligons_facts_union {
  access_filter: {
    field: brand_security
    user_attribute: brand
  }
  access_filter: {
    field: vertical_security
    user_attribute: vertical
  }

  label: "Penhaligon's"
  view_label: "Sales, GA, Marketing"
  group_label: "Group eCommerce"

  join: budgetstarget {
    relationship: many_to_one
    type: left_outer
    sql_on: ${penhaligons_facts_union.common_date_date} = ${budgetstarget.business_date} and ${penhaligons_facts_union.store_id} = ${budgetstarget.bk_store} ;;
  }

  join: dim_alternate_bu_hierarchy {
    type: left_outer
    sql_on:
    cast(${penhaligons_facts_union.store_id} as string) = ${dim_alternate_bu_hierarchy.store_code};;
    relationship: many_to_one
  }

}


######################################
#########      LACOSTE      ##########
######################################

explore: lacoste_facts_union {

  access_filter: {
    field: brand_security
    user_attribute: brand
  }
  access_filter: {
    field: vertical_security
    user_attribute: vertical
  }
  join: budgetstarget {
    relationship: many_to_one
    type: left_outer
    sql_on:  ${lacoste_facts_union.common_date} = ${budgetstarget.business_date} and ${lacoste_facts_union.store_id} = ${budgetstarget.bk_store};;
  }
  join: dim_retail_product {
    view_label: "Product"
    relationship: one_to_one
    type: left_outer
    sql_on: ${lacoste_facts_union.sku}  = ${dim_retail_product.item}
    AND ${lacoste_facts_union.brand} = ${dim_retail_product.brand};;
  }
  join: lacoste_coupon_code_affiliates {
    type: left_outer
    view_label: "Affiliates Coupon Code"
    relationship: many_to_one
    sql_on: ${lacoste_facts_union.order_promotion_detail_coupon_code} = ${lacoste_coupon_code_affiliates.coupon_code}  ;;
  }

  join: dim_tender_types_comm {
    type: left_outer
    relationship: many_to_one
    sql_on: ${lacoste_facts_union.tender_type_id} = ${dim_tender_types_comm.tender_type_id} ;;
  }

  label: "Lacoste"
  group_label: "Group eCommerce"

}


#######################################
#####           TUMI              #####
#######################################

explore: tumi {
  from: tumi_facts_union

  access_filter: {
    field: brand_security
    user_attribute: brand
  }
  access_filter: {
    field: vertical_security
    user_attribute: vertical
  }

  label: "Tumi"
  view_label: "Sales, GA, Marketing"
  group_label: "Group eCommerce"

  join: dim_retail_product {
    relationship: one_to_one
    type: left_outer
    sql_on: ${tumi.sku}  = ${dim_retail_product.item} ;;
  }

  join: budgetstarget {
    relationship: many_to_one
    type: left_outer
    sql_on: ${tumi.common_datetime_date} = ${budgetstarget.business_date} and cast(${tumi.store_id} as string) = cast(${budgetstarget.bk_store} as string);;
  }

  # join: ecomm_customer_transactional_pop {
  #   relationship: many_to_one
  #   type: left_outer
  #   sql_on: "TUMI" = upper(${ecomm_customer_transactional_pop.brand}) and ${tumi.order_id} = ${ecomm_customer_transactional_pop.original_order_id}
  #     and ${tumi.date_group} = ${ecomm_customer_transactional_pop.date_group} ;;
  # }

  # join: ecomm_customer_type_pop {
  #   type: left_outer
  #   relationship: many_to_one
  #   sql_on: ${tumi.date_group} = ${ecomm_customer_transactional_pop.date_group}
  #     AND  ${ecomm_customer_type_pop.customer_key} = ${ecomm_customer_transactional_pop.customer_key} ;;
  # }

  # join: ecomm_customer_aggregates_pop{
  #   type: left_outer
  #   relationship: many_to_one
  #   sql_on: ${ecomm_customer_transactional_pop.customer_key} = ${ecomm_customer_aggregates_pop.customer_key} ;;
  # }

}


#######################################
#####           GHAWALI           #####
#######################################

explore: ghawali {
  from: ghawali_facts_union

  access_filter: {
    field: brand_security
    user_attribute: brand
  }
  access_filter: {
    field: vertical_security
    user_attribute: vertical
  }

  label: "Ghawali"
  view_label: "Sales, GA, Marketing"
  group_label: "Group eCommerce"

  join: dim_retail_product {
    relationship: one_to_one
    type: left_outer
    sql_on: ${ghawali.sku}  = ${dim_retail_product.item} ;;
  }

  join: budgetstarget {
    relationship: many_to_one
    type: left_outer
    sql_on: ${ghawali.common_datetime_date} = ${budgetstarget.business_date} and cast(${ghawali.store_id} as string) = cast(${budgetstarget.bk_store} as string);;
  }

  # join: ecomm_customer_transactional_pop {
  #   relationship: many_to_one
  #   type: left_outer
  #   sql_on: "GHAWALI" = upper(${ecomm_customer_transactional_pop.brand}) and ${ghawali.order_id} = ${ecomm_customer_transactional_pop.original_order_id}
  #     and ${ghawali.date_group} = ${ecomm_customer_transactional_pop.date_group} ;;
  # }

  # join: ecomm_customer_type_pop {
  #   type: left_outer
  #   relationship: many_to_one
  #   sql_on: ${ghawali.date_group} = ${ecomm_customer_transactional_pop.date_group}
  #     AND  ${ecomm_customer_type_pop.customer_key} = ${ecomm_customer_transactional_pop.customer_key} ;;
  # }

  # join: ecomm_customer_aggregates_pop{
  #   type: left_outer
  #   relationship: many_to_one
  #   sql_on: ${ecomm_customer_transactional_pop.customer_key} = ${ecomm_customer_aggregates_pop.customer_key} ;;
  # }

}


#######################################
#####           TANAGRA           #####
#######################################

explore: tanagra {
  from: tanagra_facts_union

  access_filter: {
    field: brand_security
    user_attribute: brand
  }
  access_filter: {
    field: vertical_security
    user_attribute: vertical
  }

  label: "Tanagra"
  view_label: "Sales, GA, Marketing"
  group_label: "Group eCommerce"

  join: dim_retail_product {
    relationship: one_to_one
    type: left_outer
    sql_on: ${tanagra.sku}  = ${dim_retail_product.item} ;;
  }

  join: oms_manco_acceptance {
    type: left_outer
    view_label: "Manco cancellations"
    relationship: one_to_many
    sql_on: ${tanagra.order_id} = ${oms_manco_acceptance.order_id} and ${tanagra.detail_id} = ${oms_manco_acceptance.detail_id}
      and "TANAGRA" = upper(${oms_manco_acceptance.brand}) and ${tanagra.record_type} = "CANCELLED" ;;
  }

  # join: budgetstarget {
  #   relationship: many_to_one
  #   type: left_outer
  #   sql_on: ${tanagra.common_datetime_date} = ${budgetstarget.business_date} and cast(${tanagra.store_id} as string) = cast(${budgetstarget.bk_store} as string);;
  # }

  # join: ecomm_customer_transactional_pop {
  #   relationship: many_to_one
  #   type: left_outer
  #   sql_on: "GHAWALI" = upper(${ecomm_customer_transactional_pop.brand}) and ${ghawali.order_id} = ${ecomm_customer_transactional_pop.original_order_id}
  #     and ${ghawali.date_group} = ${ecomm_customer_transactional_pop.date_group} ;;
  # }

  # join: ecomm_customer_type_pop {
  #   type: left_outer
  #   relationship: many_to_one
  #   sql_on: ${ghawali.date_group} = ${ecomm_customer_transactional_pop.date_group}
  #     AND  ${ecomm_customer_type_pop.customer_key} = ${ecomm_customer_transactional_pop.customer_key} ;;
  # }

  # join: ecomm_customer_aggregates_pop{
  #   type: left_outer
  #   relationship: many_to_one
  #   sql_on: ${ecomm_customer_transactional_pop.customer_key} = ${ecomm_customer_aggregates_pop.customer_key} ;;
  # }

}



#######################################
#####    TORY BURCH ACCEPTANCE    #####
#######################################

explore: toryburch_acceptance {
  from: toryburch_acceptance_facts_union

  access_filter: {
    field: brand_security
    user_attribute: brand
  }
  access_filter: {
    field: vertical_security
    user_attribute: vertical
  }

  label: "Tory Burch OMS UAT"
  view_label: "Sales, GA, Marketing"
  group_label: "Group eCommerce"

  join: dim_retail_product {
    relationship: one_to_one
    type: left_outer
    sql_on: ${toryburch_acceptance.sku}  = ${dim_retail_product.item} ;;
  }

  join: oms_manco_acceptance {
    type: left_outer
    view_label: "Manco cancellations"
    relationship: one_to_many
    sql_on: ${toryburch_acceptance.order_id} = ${oms_manco_acceptance.order_id} and ${toryburch_acceptance.detail_id} = ${oms_manco_acceptance.detail_id}
      and "TORYBURCH" = upper(${oms_manco_acceptance.brand}) and ${toryburch_acceptance.record_type} = "CANCELLED" ;;
  }

  # join: budgetstarget {
  #   relationship: many_to_one
  #   type: left_outer
  #   sql_on: ${tanagra.common_datetime_date} = ${budgetstarget.business_date} and cast(${tanagra.store_id} as string) = cast(${budgetstarget.bk_store} as string);;
  # }

  # join: ecomm_customer_transactional_pop {
  #   relationship: many_to_one
  #   type: left_outer
  #   sql_on: "GHAWALI" = upper(${ecomm_customer_transactional_pop.brand}) and ${ghawali.order_id} = ${ecomm_customer_transactional_pop.original_order_id}
  #     and ${ghawali.date_group} = ${ecomm_customer_transactional_pop.date_group} ;;
  # }

  # join: ecomm_customer_type_pop {
  #   type: left_outer
  #   relationship: many_to_one
  #   sql_on: ${ghawali.date_group} = ${ecomm_customer_transactional_pop.date_group}
  #     AND  ${ecomm_customer_type_pop.customer_key} = ${ecomm_customer_transactional_pop.customer_key} ;;
  # }

  # join: ecomm_customer_aggregates_pop{
  #   type: left_outer
  #   relationship: many_to_one
  #   sql_on: ${ecomm_customer_transactional_pop.customer_key} = ${ecomm_customer_aggregates_pop.customer_key} ;;
  # }

}





######################################
######    ECOM TABLE TESTING    ######
######################################

explore: comm_dq_check_looker {
  group_label: "Group Commercial"
  label: "Test Results"
  hidden: yes
}

######################################
######    ECOM TABLE TESTING    ######
######################################

explore: akeneo_retail_product_faces {

  access_filter: {
    field: brand_security
    user_attribute: brand
  }

  group_label: "Group eCommerce"
  label: "PIM Faces UAT"
}

######################################
######    ECOM TABLE TESTING    ######
######################################

explore: akeneo_retail_product_tryano {
  hidden: yes
  # access_filter: {
  #   field: brand_security
  #   user_attribute: brand
  # }

  group_label: "Group eCommerce"
  label: "PIM Tryano UAT"
}


  ##############################################
  ### Hidden explores for dim filter suggestions
  ##############################################

explore: dim_retail_prod_brand {
  hidden: yes
}

explore: dim_retail_prod_class {
  hidden: yes
}

explore: dim_retail_prod_colour {
  hidden: yes
}

explore: dim_retail_prod_division {
  hidden: yes
}

explore: dim_retail_prod_gender {
  hidden: yes
}

explore: dim_retail_prod_size {
  hidden: yes
}

explore: dim_retail_prod_sub_class {
  hidden: yes
}

explore: dim_retail_prod_taxo_class {
  hidden: yes
}

explore: dim_retail_prod_taxo_subclass {
  hidden: yes
}

explore: dim_retail_prod_season {
  hidden: yes
}
