connection: "firebase"
label: "Mobile App"

include: "/comm_views/firebase/*.view.lkml"
include: "/comm_views/firebase/firebase.dashboard.lookml"   # include a LookML dashboard called my_dashboard

datagroup: daily {
  sql_trigger: SELECT CURRENT_DATE() ;;
  max_cache_age: "24 hours"
}

explore: tryano_events {
label: "Tryano - Firebase (BETA)"

## TO avoid querying the entire database by default, suggest setting up a filter like below, and perhaps limiting GB scanned with 'Max Billing Gigabytes' in the connection
  always_filter: {
    filters: {
      field: event_date
      value: "last 7 days"
    }
  }

  access_filter: {
    field: brand_security
    user_attribute: brand
  }

  join: tryano_events__event_params {
    view_label: "Event Properties"
    sql: LEFT JOIN UNNEST(tryano_events.event_params) as tryano_events__event_params ;;
    relationship: one_to_many
  }

  join: tryano_events__event_params__value {
    view_label: "Event Properties"
    sql: LEFT JOIN UNNEST([tryano_events__event_params.value]) as tryano_events__event_params__value ;;
    relationship: one_to_many
  }

  join: tryano_events__user_properties {
    sql: LEFT JOIN UNNEST(tryano_events.user_properties) as tryano_events__user_properties ;;
    view_label: "User Properties"
    relationship: one_to_many
  }

  join: tryano_events__user_properties__value {
    view_label: "User Properties"
    sql: LEFT JOIN UNNEST([tryano_events__user_properties.value]) as tryano_events__user_properties__value ;;
    relationship: one_to_many
  }
}
explore: swarovski_events {
  label: "Swarovski - Firebase (BETA)"

## TO avoid querying the entire database by default, suggest setting up a filter like below, and perhaps limiting GB scanned with 'Max Billing Gigabytes' in the connection
  always_filter: {
    filters: {
      field: event_date
      value: "last 7 days"
    }
  }

  access_filter: {
    field: brand_security
    user_attribute: brand
  }

  join: swarovski_events__event_params {
    view_label: "Event Properties"
    sql: LEFT JOIN UNNEST(swarovski_events.event_params) as swarovski_events__event_params ;;
    relationship: one_to_many
  }

  join: swarovski_events__event_params__value {
    view_label: "Event Properties"
    sql: LEFT JOIN UNNEST([swarovski_events__event_params.value]) as swarovski_events__event_params__value ;;
    relationship: one_to_many
  }

  join: swarovski_events__user_properties {
    sql: LEFT JOIN UNNEST(swarovski_events.user_properties) as swarovski_events__user_properties ;;
    view_label: "User Properties"
    relationship: one_to_many
  }

  join: swarovski_events__user_properties__value {
    view_label: "User Properties"
    sql: LEFT JOIN UNNEST([swarovski_events__user_properties.value]) as swarovski_events__user_properties__value ;;
    relationship: one_to_many
  }
}
