connection: "chb-prod-data-cust"
include: "/customer_views/*.*"
include: "/_common/*.*"
include: "../comm_views/*.view"

datagroup: chb_customer_default_datagroup {
  # sql_trigger: SELECT MAX(id) FROM etl_log;;
  max_cache_age: "1 hour"
}


week_start_day: sunday



#######################################
#####  Muse Control Tower Gravty  #####
#######################################




explore: muse_control_tower_gravty {
  label: "Muse Control Tower Gravty"

  view_name: muse_calendar

  join: muse_gravty {
    type: left_outer
    sql_on: ${muse_gravty.transaction_date_date}=${muse_calendar.transaction_date_date} AND
    ${muse_gravty.enrolling_location_id_joiner}=${muse_calendar.calendar_enrolling_location_id} ;;
    relationship: one_to_many
  }

  join: contest_offers {
    type: left_outer
    sql_on: ${muse_gravty.member_id}=${contest_offers.member_id};;
    relationship: many_to_many
  }

    join: unique_offers {
    type: left_outer
    sql_on: ${muse_gravty.member_id}=${unique_offers.member_id};;
    relationship: many_to_many
  }


  join: redemptions_experience {
    type: left_outer
    sql_on: ${muse_gravty.member_id}=${redemptions_experience.member_id};;
    relationship: many_to_many
  }

  join: redemptions_product {
    type: left_outer
    sql_on: ${muse_gravty.member_id}=${redemptions_product.member_id};;
    relationship: many_to_many
  }

  join: muse_gravty_aggregates {
    type: inner
    sql_on: ${muse_gravty.member_id}=${muse_gravty_aggregates.member_id};;
    relationship: many_to_one
  }

  join: members_ndt {
    type:  left_outer
    sql_on:  ${muse_gravty.member_id}=${members_ndt.member_id};;
    relationship: many_to_one
  }

  join: segments_ndt1  {
    type:  left_outer
    sql_on:  ${muse_gravty.member_id}=${segments_ndt1.member_id};;
    relationship: many_to_one
  }

  join: locations {
    type: inner
    sql_on: ${muse_calendar.calendar_enrolling_location_id}=CAST(${locations.store} AS STRING);;
    relationship: many_to_one
  }

  join: stg_muse_forecasts {
    type: left_outer
    sql_on: ${muse_calendar.cal_month}=${stg_muse_forecasts.cal_month} AND
    CAST(${muse_calendar.calendar_enrolling_location_id} AS STRING) = CAST(${stg_muse_forecasts.store_code} AS STRING)
      ;;
    relationship: one_to_one
  }


  join: segments_ndt {
    type:  left_outer
    sql_on:  ${muse_gravty.member_id}=${segments_ndt.member_id};;
    relationship: many_to_one
  }
  # join: dim_retail_product {
  #   type: inner
  #   sql_on: ${muse_customers_sales_test.bk_productid}=${dim_retail_product.item} ;;
  #   relationship: many_to_one
  # }

  # join: dim_alternate_bu_hierarchy {
  #   type:  inner
  #   sql_on: cast(${locations.store} as string)=${dim_alternate_bu_hierarchy.store_code} ;;
  #   relationship: one_to_one
  # }

  join: prod_braze_campaigns {
    type:  left_outer
    sql_on:  ${muse_gravty.uuid}=${prod_braze_campaigns.external_user_id};;
    relationship: many_to_many
  }

  # its muse model only anyway and this filter breaks frs
  # access_filter: {
  #   field: muse_gravty.vertical
  #   user_attribute: vertical
  # }

}



explore: factretailsales {
  label: "IR Muse & CRM Customers"
  always_filter: {
    filters: [is_generic_customer : "no", base_customers.is_generic_customer : "no"]}

  access_filter: {
    field: dim_alternate_bu_hierarchy.country
    user_attribute: country
  }
  access_filter: {
    field: dim_alternate_bu_hierarchy.vertical
    user_attribute: vertical
  }
  access_filter: {
    field: dim_alternate_bu_hierarchy.brand
    user_attribute: brand
  }


  join: base_customers{
    type: left_outer
    sql_on:${factretailsales.brand_customer_unique_id} = ${base_customers.brand_customer_unique_id}  ;;
    relationship: many_to_one
  }

  join: locations {
    type: inner
    sql_on: ${factretailsales.bk_storeid}=${locations.store};;
    relationship: :many_to_one
  }

  join: dim_retail_product {
    type: inner
    sql_on: cast(${factretailsales.bk_productid} as string)=${dim_retail_product.item} ;;
    relationship: many_to_one

  }

  join: customer_facts {
    type: left_outer
    sql_on: ${base_customers.brand_customer_unique_id} =${customer_facts.brand_customer_unique_id} ;;

    relationship: one_to_one
  }

  join: muse_facts {
    type: left_outer
    sql_on: ${factretailsales.atr_muse_id} =${muse_facts.atr_muse_id} ;;

    relationship: many_to_one
  }




  join: customer_facts_golden {
    type: left_outer
    sql_on: ${base_customers.customer_unique_id} =${customer_facts_golden.customer_unique_id} ;;

    relationship: many_to_one
  }

  join: customer_facts_lifetime {
    type: left_outer
    sql_on: ${base_customers.brand_customer_unique_id} =${customer_facts_lifetime.brand_customer_unique_id} ;;
    relationship: one_to_one
  }

#   join: customer_facts_2 {
#     type: left_outer
#     sql_on: ${base_customers.bk_customer} =${customer_facts.bk_customer} ;;
#     relationship: one_to_one
#   }

  join: dim_alternate_bu_hierarchy {
    type:  inner
    sql_on: cast(${locations.store} as string)=${dim_alternate_bu_hierarchy.store_code} ;;
    relationship: one_to_one
  }
}




# explore: muse_customers_sales_test {
#   label: "Muse Control Tower"

#   view_name: muse_customers_sales_test

#   join: muse_aggregates_test {
#     type: inner
#     sql_on: ${muse_customers_sales_test.atr_muse_id}=${muse_aggregates_test.atr_muse_id};;
#     relationship: :many_to_one
#   }

#   join: locations {
#     type: inner
#     sql_on: ${muse_customers_sales_test.bk_storeid}=CAST(${locations.store} AS STRING);;
#     relationship: :many_to_one
#   }

#   join: dim_retail_product {
#     type: inner
#     sql_on: ${muse_customers_sales_test.bk_productid}=${dim_retail_product.item} ;;
#     relationship: many_to_one

#   }

#   join: dim_alternate_bu_hierarchy {
#     type:  inner
#     sql_on: cast(${locations.store} as string)=${dim_alternate_bu_hierarchy.store_code} ;;
#     relationship: one_to_one
#   }

#   access_filter: {
#     field: muse_customers_sales_test.vertical
#     user_attribute: vertical
#   }

# }
#######################################
#####      MUSE Adjust     #####
#######################################

explore: muse_adjust {
  label: "MUSE - Mobile App (BETA)"
}
