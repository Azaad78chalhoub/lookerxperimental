connection: "chb-prod-supplychain-data"

include: "/finance_views/*.view.lkml"                # include all views in the views/ folder in this project
# include: "/**/view.lkml"                   # include all views in this project
# include: "my_dashboard.dashboard.lookml"   # include a LookML dashboard called my_dashboard

label: "Finance"

explore: dm_accounts_payable {
  label: "Days Payable Outstanding"
  group_label: "Finance"
  view_label: "Days Payable Outstanding"
  hidden: yes
  }

explore: dm_accounts_receivable {
  label: "Days Sales Outstanding"
  group_label: "Finance"
  view_label: "Days Sales Outstanding"
  hidden: yes
}

explore: dm_inventory_outstanding {
  label: "Days Inventory Outstanding"
  group_label: "Finance"
  view_label: "Days Inventory Outstanding"
  hidden: yes
}

explore: dm_accounts_payable_by_invoice {
  label: "Accounts Payable by Invoice"
  group_label: "Finance"
  view_label: "Accounts Payable by Invoice"
  hidden: yes
}



# # Select the views that should be a part of this model,
# # and define the joins that connect them together.
#
# explore: order_items {
#   join: orders {
#     relationship: many_to_one
#     sql_on: ${orders.id} = ${order_items.order_id} ;;
#   }
#
#   join: users {
#     relationship: many_to_one
#     sql_on: ${users.id} = ${orders.user_id} ;;
#   }
# }
