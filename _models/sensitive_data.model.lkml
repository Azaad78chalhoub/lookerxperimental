connection: "chb-prod-stage-oracle"

include: "/sensitive_data_views/*.view.lkml"

include: "/comm_views/*.*"

label: "Sensitive Data"

access_grant: access_sensitive_data {
  user_attribute: access_sensitive_data
  allowed_values: ["Yes"]
}

explore: dim_employee_details {
  label: "Employee Data"
  required_access_grants: [access_sensitive_data]}


explore: onboarded_users {
  hidden: no
  label: "Onboarded Users"
}


explore: looker_usage{

  join: dim_employee_details {
    type: left_outer
    sql_on: lower(${looker_usage.user_email_lower})=lower(${dim_employee_details.email}) ;;
  }

  join: onboarded_users2 {
    type: left_outer
    sql_on: lower(${looker_usage.user_email_lower})=lower(${onboarded_users2.email_lower}) ;;
  }


}
