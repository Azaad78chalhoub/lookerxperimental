connection: "chb-prod-stage-mdm-rdbs"

include: "/rdbs_views/*.view.lkml"                # include all views in the views/ folder in this project
# include: "/**/view.lkml"                   # include all views in this project
# include: "my_dashboard.dashboard.lookml"   # include a LookML dashboard called my_dashboard

label: "RDBS"
week_start_day: sunday

explore: dm_jira_issues {
  group_label: "RDBS"
  view_label: "Jira Issues"
  hidden: no
}
# explore: mdm_jira_issue {
#   label: "Jira Issues"
#   view_label: "Issue"
#   join: mdm_jira_project  {
#     view_label: "Project"
#     relationship: many_to_one
#     sql_on: ${mdm_jira_issue.project} = ${mdm_jira_project.id} ;;
#   }
#   join: mdm_jira_status {
#     view_label: "Status"
#     relationship: many_to_one
#     sql_on: ${mdm_jira_issue.status} = ${mdm_jira_status.id} ;;
#   }
#   join: mdm_jira_issue_type {
#     view_label: "Issue Type"
#     relationship: many_to_one
#     sql_on: ${mdm_jira_issue.issue_type} = ${mdm_jira_issue_type.id} ;;
#   }
#   join: mdm_jira_user_assignee {
#     from: mdm_jira_user
#     view_label: "Assignee"
#     relationship: many_to_one
#     sql_on: ${mdm_jira_issue.assignee} = ${mdm_jira_user_assignee.id} ;;
#   }
#   join: mdm_jira_user_creator {
#     from: mdm_jira_user
#     view_label: "Creator"
#     relationship: many_to_one
#     sql_on: ${mdm_jira_issue.creator} = ${mdm_jira_user_creator.id} ;;
#   }
#   join: mdm_jira_user_reporter {
#     from: mdm_jira_user
#     view_label: "Reporter"
#     relationship: many_to_one
#     sql_on: ${mdm_jira_issue.reporter} = ${mdm_jira_user_reporter.id} ;;
#   }
#   join: mdm_jira_division_boat {
#     from: mdm_jira_field_option
#     view_label: "Division Boat"
#     relationship: many_to_one
#     sql_on: ${mdm_jira_issue.division_boat_} = ${mdm_jira_division_boat.id} ;;
#   }
#   join: mdm_jira_legal_entity{
#     from: mdm_jira_field_option
#     view_label: "Legal Entity"
#     relationship: many_to_one
#     sql_on: ${mdm_jira_issue.legal_entity} = ${mdm_jira_legal_entity.id} ;;
#   }
#   join: mdm_jira_org_unit{
#     from: mdm_jira_field_option
#     view_label: "Organizational Unit"
#     relationship: many_to_one
#     sql_on: ${mdm_jira_issue.org_unit} = ${mdm_jira_org_unit.id} ;;
#   }
#   join: mdm_jira_pos_corporate{
#     from: mdm_jira_field_option
#     view_label: "Pos/Corporate"
#     relationship: many_to_one
#     sql_on: ${mdm_jira_issue.pos_corporate} = ${mdm_jira_pos_corporate.id} ;;
#   }
#   join: mdm_jira_store_wh{
#     from: mdm_jira_field_option
#     view_label: "Store/WH"
#     relationship: many_to_one
#     sql_on: ${mdm_jira_issue.store_wh} = ${mdm_jira_store_wh.id} ;;
#   }
#   join: mdm_jira_sub_vertical{
#     from: mdm_jira_field_option
#     view_label: "Subvertical"
#     relationship: many_to_one
#     sql_on: ${mdm_jira_issue.sub_vertical} = ${mdm_jira_store_wh.id} ;;
#   }
#   join: mdm_jira_vertical{
#     from: mdm_jira_field_option
#     view_label: "Vertical"
#     relationship: many_to_one
#     sql_on: ${mdm_jira_issue.vertical} = ${mdm_jira_store_wh.id} ;;
#   }
#
#
#
# }

# # Select the views that should be a part of this model,
# # and define the joins that connect them together.
#
# explore: order_items {
#   join: orders {
#     relationship: many_to_one
#     sql_on: ${orders.id} = ${order_items.order_id} ;;
#   }
#
#   join: users {
#     relationship: many_to_one
#     sql_on: ${users.id} = ${orders.user_id} ;;
#   }
# }
