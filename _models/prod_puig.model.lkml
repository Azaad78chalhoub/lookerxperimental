connection: "prod-puig"
label: "PUIG"

access_grant: puig {
  user_attribute: puig
  allowed_values: ["Yes"]
}

include: "/monetisation/puig_dm_serving.view.lkml"



week_start_day: sunday

explore: puig {
  from: puig_dm_serving

#   access_filter: {
#     field: source
#     user_attribute: brand
#   }

  label: "PUIG"
  view_label: "PUIG"
  group_label: "Monetisation"
}
