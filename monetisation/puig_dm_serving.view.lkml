view: puig_dm_serving {
  sql_table_name: `chb-dat-svc-puig-p001.prod_serving_dm.puig_dm_serving`
    ;;

  dimension: pk {
    primary_key: yes
    hidden: yes
    sql: CONCAT(${TABLE}.date, ${TABLE}.ean_code, ${TABLE}.masterfile_brand, ${TABLE}.category, ${TABLE}.source) ;;
  }

  dimension: axis {
    type: string
    sql: ${TABLE}.axis ;;
  }

  dimension: category {
    type: string
    sql: ${TABLE}.category ;;
  }

  dimension: country {
    type: string
    map_layer_name: countries
    sql: ${TABLE}.country ;;
  }

  dimension_group: date {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.date ;;
  }

  dimension: description {
    type: string
    sql: ${TABLE}.description ;;
  }

  dimension: ean_code {
    type: string
    sql: ${TABLE}.EAN_code ;;
  }

  dimension: format {
    type: string
    sql: ${TABLE}.format ;;
  }

  dimension: gender {
    type: string
    sql: ${TABLE}.gender ;;
  }

  dimension: local_currency {
    type: string
    sql: ${TABLE}.local_currency ;;
  }

  dimension: location {
    type: string
    sql: ${TABLE}.location ;;
  }

  dimension: masterfile_brand {
    type: string
    sql: ${TABLE}.masterfile_brand ;;
  }

  dimension: product_line {
    type: string
    sql: ${TABLE}.product_line ;;
  }

  dimension: promo_type {
    type: string
    sql: ${TABLE}.promo_type ;;
  }

  dimension: quantity {
    type: number
    sql: ${TABLE}.quantity ;;
  }

  dimension: reference {
    type: string
    sql: ${TABLE}.reference ;;
  }

  dimension: source {
    type: string
    sql: ${TABLE}.source ;;
  }

  dimension: universe {
    type: string
    sql: ${TABLE}.universe ;;
  }

  dimension: retailer {
    type: string
    sql: ${TABLE}.location_masterfile_2.Retailer ;;
  }

  dimension: location_masterfile {
    type: string
    sql: ${TABLE}.location_masterfile_2.Location ;;
  }

  dimension: marketsize_month {
    type: date
    sql: TIMESTAMP(${TABLE}.marketsize_masterfile.Month) ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }


  measure: total_value {
    type: sum
    value_format: "#,##0.00;(#,##0.00)"
    sql: ${TABLE}.local_value;;
  }

  measure: total_usd_value {
    type: sum
    value_format: "$#,##0.00;($#,##0.00)"
    sql: ${TABLE}.USD_value;;
  }

  measure: marketsize_net_usd {
    type: sum
    sql: ${TABLE}.marketsize_masterfile.Net_sales_amount_USD ;;
  }

  measure: marketshare {
    type: number
    value_format: "#.#%"
    sql: ${total_usd_value}/${marketsize_net_usd} ;;
  }


}
