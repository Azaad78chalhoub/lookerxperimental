view: dim_employee_details {
  label: "Employee Details"
  sql_table_name: `chb-prod-stage-oracle.prod_hr_employee_details.dim_employee_details`
    ;;

  dimension: _fivetran_deleted {
    hidden: yes
    type: yesno
    sql: ${TABLE}._fivetran_deleted ;;
  }

# dimension: is_looker_onboarded {
#   description: "Has the employee been on-boarded to Looker"
#   type: yesno
#   sql: case when ${onboarded_users2.email} is not null then true else false end ;;
# }

  dimension_group: _fivetran_synced {
    hidden: yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}._fivetran_synced ;;
  }

  dimension: address {
    hidden: yes
    type: string
    sql: ${TABLE}.address ;;
  }

  dimension: address_2 {
    hidden: yes
    type: string
    sql: ${TABLE}.address_2 ;;
  }

  dimension: buildingshop {
    type: string
    sql: ${TABLE}.buildingshop ;;
  }

  dimension: businessunit {
    type: string
    sql: ${TABLE}.businessunit ;;
  }

  dimension: captain {
    type: string
    sql: ${TABLE}.captain ;;
  }

  dimension: city {
    hidden: yes
    type: string
    sql: ${TABLE}.city ;;
  }

  dimension: city_code {
    hidden: yes
    type: string
    sql: ${TABLE}.city_code ;;
  }

  dimension: company {
    type: string
    sql: ${TABLE}.company ;;
  }

  dimension_group: company_joining {
    hidden: yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.company_joining_date ;;
  }

  dimension: costcenter {
    type: string
    sql: ${TABLE}.costcenter ;;
  }

  dimension: countrycode {
    type: string
    sql: ${TABLE}.countrycode ;;
  }

  dimension: custcountryphonecode {
    hidden: yes
    type: string
    sql: ${TABLE}.custcountryphonecode ;;
  }

  dimension: custphonenumber {
    hidden: yes
    type: string
    sql: ${TABLE}.custphonenumber ;;
  }

  dimension: custusedfor {
    hidden: yes
    type: string
    sql: ${TABLE}.custusedfor ;;
  }

  dimension_group: date_of_birth {
    hidden: yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.date_of_birth ;;
  }

  dimension: department {
    type: string
    sql: ${TABLE}.department ;;
  }

  dimension: department_code {
    type: string
    sql: ${TABLE}.department_code ;;
  }

  dimension: division {
    type: string
    sql: ${TABLE}.division ;;
  }

  dimension: division_code {
    type: string
    sql: ${TABLE}.division_code ;;
  }

  dimension: email {
    type: string
    sql: LOWER(${TABLE}.email) ;;
  }

  dimension: email_id {
    type: string
    sql: ${TABLE}.email_id ;;
  }

  dimension: empcode {
    type: string
    sql: ${TABLE}.empcode ;;
    label: "Employee-ID"
  }

  dimension: empid {
    type: string
    sql: ${TABLE}.empid ;;
  }

  dimension: employee_name {
    type: string
    sql: ${TABLE}.employee_name ;;
  }

  # Note (Patrick): renamed label requested by business
  dimension: salesperson_name {
    label: "SalesPerson Name"
    type: string
    sql: ${TABLE}.employee_name ;;
  }



  dimension: employee_satus {
    type: string
    sql: ${TABLE}.employee_satus ;;
  }

  dimension: employeecategory {
    type: string
    sql: ${TABLE}.employeecategory ;;
  }

  dimension: enableentertainer {
     hidden: yes
    type: string
    sql: ${TABLE}.enableentertainer ;;
  }

  dimension_group: eos {
    hidden: yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.eos_date ;;
  }

  dimension: fax {
    hidden: yes
    type: string
    sql: ${TABLE}.fax ;;
  }

  dimension: fax_no {
    hidden: yes
    type: string
    sql: ${TABLE}.fax_no ;;
  }

  dimension: fic {
    hidden: yes
    type: string
    sql: ${TABLE}.fic ;;
  }

  dimension: first_name {
    type: string
    sql: ${TABLE}.first_name ;;
  }

  dimension: formalname {
    type: string
    sql: ${TABLE}.formalname ;;
  }

  dimension: function {
    type: string
    sql: ${TABLE}.function ;;
  }

  dimension: gender {
    type: string
    sql: ${TABLE}.gender ;;
  }

  dimension_group: group_joining {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.group_joining_date ;;
  }

  dimension: holidaycalendarcode {
    hidden: yes
    type: string
    sql: ${TABLE}.holidaycalendarcode ;;
  }

  dimension: hrbp {
    hidden: yes
    type: string
    sql: ${TABLE}.hrbp ;;
  }

  dimension: isshiftemployee {
    hidden: yes
    type: string
    sql: ${TABLE}.isshiftemployee ;;
  }

  dimension: job_title {
    type: string
    sql: ${TABLE}.job_title ;;
    label: "Designation"
  }

  dimension: job_title_code {
    hidden: yes
    type: string
    sql: ${TABLE}.job_title_code ;;
  }

  dimension: jobcode {
    hidden: yes
    type: string
    sql: ${TABLE}.jobcode ;;
  }

  dimension: knownname {
    type: string
    sql: ${TABLE}.knownname ;;
  }

  dimension: last_name {
    type: string
    sql: ${TABLE}.last_name ;;
  }

  dimension_group: lastmodifieddatetime {
    hidden: yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.lastmodifieddatetime ;;
  }

  dimension_group: lastmodifiedon {
    hidden: yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.lastmodifiedon ;;
  }

  dimension: lastphysicaldate {
    hidden: yes
    type: string
    sql: ${TABLE}.lastphysicaldate ;;
  }

  dimension_group: lastupdate {
    hidden: yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.lastupdate ;;
  }

  dimension: local_address {
    hidden: yes
    type: string
    sql: ${TABLE}.local_address ;;
  }

  dimension: location {
    type: string
    sql: ${TABLE}.location ;;
  }

  dimension: location_1 {
    type: string
    sql: ${TABLE}.location_1 ;;
  }

  dimension: location_1_code {
    type: string
    sql: ${TABLE}.location_1_code ;;
  }

  dimension: managerid {
    type: string
    sql: ${TABLE}.managerid ;;
  }

  dimension: marital_status {
    hidden: yes
    type: string
    sql: ${TABLE}.marital_status ;;
  }

  dimension: middle_name {
    hidden: yes
    type: string
    sql: ${TABLE}.middle_name ;;
  }

  dimension: mobile_no {
    hidden: yes
    type: string
    sql: ${TABLE}.mobile_no ;;
  }

  dimension: nationality {
    type: string
    sql: ${TABLE}.nationality ;;
  }

  dimension: nationality_code {
    type: string
    sql: ${TABLE}.nationality_code ;;
  }

  dimension: off_phone_no {
    hidden: yes
    type: string
    sql: ${TABLE}.off_phone_no ;;
  }

  dimension: p_o_box {
    hidden: yes
    type: string
    sql: ${TABLE}.p_o_box ;;
  }

  dimension: passportname {
    hidden: yes
    type: string
    sql: ${TABLE}.passportname ;;
  }

  dimension: pay_grade {
    hidden: yes
    type: string
    sql: ${TABLE}.pay_grade ;;
  }

  dimension: personalemail {
    hidden: yes
    type: string
    sql: ${TABLE}.personalemail ;;
  }

  dimension: photo_file_name {
    hidden: yes
    type: string
    sql: ${TABLE}.photo_file_name ;;
  }

  dimension: photo_file_path {
    hidden: yes
    type: string
    sql: ${TABLE}.photo_file_path ;;
  }

  dimension: pobox {
    hidden: yes
    type: string
    sql: ${TABLE}.pobox ;;
  }

  dimension: position {
    type: string
    sql: ${TABLE}.position ;;
  }

  dimension_group: positionstartdate {
    hidden: yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.positionstartdate ;;
  }

  dimension_group: recorddate {
    hidden: yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.recorddate ;;
  }

  dimension: religion {
    hidden: yes
    type: string
    sql: ${TABLE}.religion ;;
  }

  dimension: religion_code {
    hidden: yes
    type: string
    sql: ${TABLE}.religion_code ;;
  }


  dimension: residence_no {
    hidden: yes
    type: string
    sql: ${TABLE}.residence_no ;;
  }

  dimension: salutation {
    type: string
    sql: ${TABLE}.salutation ;;
  }

  dimension: status {
    type: string
    sql: ${TABLE}.status ;;
  }

  dimension: subfunction {
    type: string
    sql: ${TABLE}.subfunction ;;
  }

  dimension: tas_id {
    hidden: yes
    type: string
    sql: ${TABLE}.tas_id ;;
  }

  dimension: terminationdate {
    hidden: yes
    type: string
    sql: ${TABLE}.terminationdate ;;
  }

  dimension: timetypeprofilecode {
    hidden: yes
    type: string
    sql: ${TABLE}.timetypeprofilecode ;;
  }

  dimension: title {
    type: string
    sql: ${TABLE}.title ;;
  }

  dimension: updated {
    type: string
    sql: ${TABLE}.updated ;;
  }

  dimension: visaprofession {
    hidden: yes
    type: string
    sql: ${TABLE}.visaprofession ;;
  }

  dimension: working_company {
    type: string
    sql: ${TABLE}.working_company ;;
  }

  dimension: working_company_code {
    type: string
    sql: ${TABLE}.working_company_code ;;
  }

  dimension: working_place {
    type: string
    sql: ${TABLE}.working_place ;;
  }

  dimension: working_place_code {
    hidden: yes
    type: string
    sql: ${TABLE}.working_place_code ;;
  }

  dimension: workschedulecode {
    hidden: yes
    type: string
    sql: ${TABLE}.workschedulecode ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      passportname,
      formalname,
      knownname,
      first_name,
      last_name,
      employee_name,
      middle_name,
      photo_file_name
    ]
  }
}
