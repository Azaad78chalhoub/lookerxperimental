view: mdm_jira_status {
  sql_table_name: `chb-prod-stage-mdm-rdbs.prod_mdm.mdm_jira_status`
    ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension_group: _fivetran_synced {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}._fivetran_synced ;;
  }

  dimension: description {
    type: string
    sql: ${TABLE}.description ;;
  }

  dimension: name {
    type: string
    sql: ${TABLE}.name ;;
  }

  dimension: status_category_id {
    type: number
    sql: ${TABLE}.status_category_id ;;
  }

  measure: count {
    type: count
    drill_fields: [id, name]
  }
}
