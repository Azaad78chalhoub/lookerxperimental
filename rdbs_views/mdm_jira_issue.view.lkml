view: mdm_jira_issue {
  sql_table_name: `chb-prod-stage-mdm-rdbs.prod_mdm.mdm_jira_issue`
    ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: _fivetran_deleted {
    type: yesno
    sql: ${TABLE}._fivetran_deleted ;;
  }

  dimension_group: _fivetran_synced {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}._fivetran_synced ;;
  }

  dimension: _original_estimate {
    type: number
    sql: ${TABLE}._original_estimate ;;
  }

  dimension: _remaining_estimate {
    type: number
    sql: ${TABLE}._remaining_estimate ;;
  }

  dimension: _time_spent {
    type: number
    sql: ${TABLE}._time_spent ;;
  }

  dimension_group: activate_deactivate {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.activate_deactivate_date ;;
  }

  dimension: assignee {
    type: string
    sql: ${TABLE}.assignee ;;
  }

  dimension: brand {
    type: string
    sql: ${TABLE}.brand ;;
  }

  dimension: business_unit {
    type: string
    sql: ${TABLE}.business_unit ;;
  }

  dimension: categories {
    type: number
    sql: ${TABLE}.categories ;;
  }

  dimension_group: change_completion {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.change_completion_date ;;
  }

  dimension: change_reason {
    type: number
    sql: ${TABLE}.change_reason ;;
  }

  dimension: change_risk {
    type: number
    sql: ${TABLE}.change_risk ;;
  }

  dimension_group: change_start {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.change_start_date ;;
  }

  dimension: change_type {
    type: number
    sql: ${TABLE}.change_type ;;
  }

  dimension: city_port {
    type: string
    sql: ${TABLE}.city_port ;;
  }

  dimension: country_name {
    type: string
    sql: ${TABLE}.country_name ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created ;;
  }

  dimension: creator {
    type: string
    sql: ${TABLE}.creator ;;
  }

  dimension: customer {
    type: string
    sql: ${TABLE}.customer ;;
  }

  dimension: customer_code {
    type: string
    sql: ${TABLE}.customer_code ;;
  }

  dimension: customer_new_name {
    type: string
    sql: ${TABLE}.customer_new_name ;;
  }

  dimension: customer_old_name {
    type: string
    sql: ${TABLE}.customer_old_name ;;
  }

  dimension_group: date {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.date ;;
  }

  dimension_group: date_of_join {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.date_of_join ;;
  }

  dimension: department {
    type: number
    sql: ${TABLE}.department ;;
  }

  dimension: dept_class_subclass {
    type: string
    sql: ${TABLE}.dept_class_subclass ;;
  }

  dimension: description {
    type: string
    sql: ${TABLE}.description ;;
  }

  dimension: development {
    type: string
    sql: ${TABLE}.development ;;
  }

  dimension: division_boat_ {
    type: number
    sql: ${TABLE}.division_boat_ ;;
  }

  dimension_group: due {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.due_date ;;
  }

  dimension_group: effective {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.effective_date ;;
  }

  dimension: emac_description {
    type: string
    sql: ${TABLE}.emac_description ;;
  }

  dimension: emac_link {
    type: string
    sql: ${TABLE}.emac_link ;;
  }

  dimension: emac_ref_no {
    type: number
    sql: ${TABLE}.emac_ref_no ;;
  }

  dimension: employee_code {
    type: string
    sql: ${TABLE}.employee_code ;;
  }

  dimension: employee_name {
    type: string
    sql: ${TABLE}.employee_name ;;
  }

  dimension: environment {
    type: string
    sql: ${TABLE}.environment ;;
  }

  dimension: epic_link {
    type: number
    sql: ${TABLE}.epic_link ;;
  }

  dimension: error_line_count {
    type: number
    sql: ${TABLE}.error_line_count ;;
  }

  dimension: from_location {
    type: string
    sql: ${TABLE}.from_location ;;
  }

  dimension: impact {
    type: number
    sql: ${TABLE}.impact ;;
  }

  dimension: investigation_reason {
    type: number
    sql: ${TABLE}.investigation_reason ;;
  }

  dimension: issue_color {
    type: string
    sql: ${TABLE}.issue_color ;;
  }

  dimension: issue_reported_by {
    type: string
    sql: ${TABLE}.issue_reported_by ;;
  }

  dimension: issue_type {
    type: number
    sql: ${TABLE}.issue_type ;;
  }

  dimension: it_incident_logged_by {
    type: string
    sql: ${TABLE}.it_incident_logged_by ;;
  }

  dimension_group: it_incident_logged {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.it_incident_logged_date ;;
  }

  dimension: it_incident_no {
    type: string
    sql: ${TABLE}.it_incident_no ;;
  }

  dimension: key {
    type: string
    sql: ${TABLE}.key ;;
  }

  dimension: last_contact {
    type: string
    sql: ${TABLE}.last_contact ;;
  }

  dimension_group: last_viewed {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.last_viewed ;;
  }

  dimension: legal_entity {
    type: number
    sql: ${TABLE}.legal_entity ;;
  }

  dimension: legal_entity_for_admin_training_porject {
    type: number
    sql: ${TABLE}.legal_entity_for_admin_training_porject ;;
  }

  dimension: line_count {
    type: number
    sql: ${TABLE}.line_count ;;
  }

  dimension: location_name {
    type: string
    sql: ${TABLE}.location_name ;;
  }

  dimension: locations {
    type: string
    sql: ${TABLE}.locations ;;
  }

  dimension: month_for_corporate_ {
    type: string
    sql: ${TABLE}.month_for_corporate_ ;;
  }

  dimension: name_in_template {
    type: string
    sql: ${TABLE}.name_in_template ;;
  }

  dimension: org_unit {
    type: number
    sql: ${TABLE}.org_unit ;;
  }

  dimension: org_unit_name {
    type: string
    sql: ${TABLE}.org_unit_name ;;
  }

  dimension: original_estimate {
    type: number
    sql: ${TABLE}.original_estimate ;;
  }

  dimension: parent_id {
    type: number
    sql: ${TABLE}.parent_id ;;
  }

  dimension: parent_link {
    type: number
    sql: ${TABLE}.parent_link ;;
  }

  dimension: pending_reason {
    type: number
    sql: ${TABLE}.pending_reason ;;
  }

  dimension: pos_corporate {
    type: number
    sql: ${TABLE}.pos_corporate ;;
  }

  dimension: priority {
    type: number
    sql: ${TABLE}.priority ;;
  }

  dimension: project {
    type: number
    sql: ${TABLE}.project ;;
  }

  dimension: qc_line_count {
    type: number
    sql: ${TABLE}.qc_line_count ;;
  }

  dimension: rdbs_comment {
    type: string
    sql: ${TABLE}.rdbs_comment ;;
  }

  dimension: remaining_estimate {
    type: number
    sql: ${TABLE}.remaining_estimate ;;
  }

  dimension: reporter {
    type: string
    sql: ${TABLE}.reporter ;;
  }

  dimension: resolution {
    type: number
    sql: ${TABLE}.resolution ;;
  }

  dimension_group: resolved {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.resolved ;;
  }

  dimension: root_cause {
    type: string
    sql: ${TABLE}.root_cause ;;
  }

  dimension: rpa_status {
    type: number
    sql: ${TABLE}.rpa_status ;;
  }

  dimension_group: satisfaction {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.satisfaction_date ;;
  }

  dimension: season {
    type: number
    sql: ${TABLE}.season ;;
  }

  dimension: so_number {
    type: string
    sql: ${TABLE}.so_number ;;
  }

  dimension: source {
    type: number
    sql: ${TABLE}.source ;;
  }

  dimension_group: start {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.start_date ;;
  }

  dimension: status {
    type: number
    sql: ${TABLE}.status ;;
  }

  dimension_group: status_category_changed {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.status_category_changed ;;
  }

  dimension: store_code {
    type: string
    sql: ${TABLE}.store_code ;;
  }

  dimension: store_wh {
    type: number
    sql: ${TABLE}.store_wh ;;
  }

  dimension: story_point_estimate {
    type: number
    sql: ${TABLE}.story_point_estimate ;;
  }

  dimension: sub_vertical {
    type: number
    sql: ${TABLE}.sub_vertical ;;
  }

  dimension: summary {
    type: string
    sql: ${TABLE}.summary ;;
  }

  dimension: supplier_code {
    type: string
    sql: ${TABLE}.supplier_code ;;
  }

  dimension: supplier_new_name {
    type: string
    sql: ${TABLE}.supplier_new_name ;;
  }

  dimension: supplier_old_name {
    type: string
    sql: ${TABLE}.supplier_old_name ;;
  }

  dimension: supplier_site_details {
    type: string
    sql: ${TABLE}.supplier_site_details ;;
  }

  dimension_group: target_end {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.target_end ;;
  }

  dimension_group: target_start {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.target_start ;;
  }

  dimension: ticket_no {
    type: string
    sql: ${TABLE}.ticket_no ;;
  }

  dimension: time_spent {
    type: number
    sql: ${TABLE}.time_spent ;;
  }

  dimension: to_location {
    type: string
    sql: ${TABLE}.to_location ;;
  }

  dimension: type {
    type: number
    sql: ${TABLE}.type ;;
  }

  dimension: update_required_for {
    type: number
    sql: ${TABLE}.update_required_for ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated ;;
  }

  dimension: urgency {
    type: number
    sql: ${TABLE}.urgency ;;
  }

  dimension: vendor {
    type: string
    sql: ${TABLE}.vendor ;;
  }

  dimension: vertical {
    type: number
    sql: ${TABLE}.vertical ;;
  }

  dimension: warehouse {
    type: string
    sql: ${TABLE}.warehouse ;;
  }

  dimension: work_ratio {
    type: number
    sql: ${TABLE}.work_ratio ;;
  }

  dimension: workaround {
    type: string
    sql: ${TABLE}.workaround ;;
  }

  dimension: working_hours {
    type: string
    sql: ${TABLE}.working_hours ;;
  }

  dimension: year {
    type: number
    sql: ${TABLE}.year ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      location_name,
      customer_new_name,
      supplier_new_name,
      employee_name,
      org_unit_name,
      country_name,
      customer_old_name,
      supplier_old_name
    ]
  }
}
