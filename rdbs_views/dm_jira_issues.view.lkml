view: dm_jira_issues {
  sql_table_name: `chb-prod-mdm-rdbs.prod_jira.dm_jira_issues`
    ;;
  drill_fields: [id]

  dimension: id {
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: pk {
    primary_key: yes
    type: string
    hidden:yes
    sql: CONCAT(${id}, ' - ' ,cast(${start_time} as string), ' - ', CAST(${remaining_time_hours} AS STRING)) ;;}


  dimension: _fivetran_deleted {
    type: yesno
    sql: ${TABLE}._fivetran_deleted ;;
  }

  dimension_group: _fivetran_synced {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}._fivetran_synced ;;
  }

  dimension: _original_estimate {
    type: number
    sql: ${TABLE}._original_estimate ;;
  }

  dimension: _remaining_estimate {
    type: number
    sql: ${TABLE}._remaining_estimate ;;
  }

  dimension: _time_spent {
    type: number
    sql: ${TABLE}._time_spent ;;
  }

  dimension_group: activate_deactivate {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.activate_deactivate_date ;;
  }

  dimension: assignee {
    type: string
    sql: ${TABLE}.assignee ;;
  }

  dimension: assignee_name {
    type: string
    sql: ${TABLE}.assignee_name ;;
  }

  dimension: brand {
    type: string
    sql: ${TABLE}.brand ;;
  }

  dimension: breached {
    type: yesno
    sql: ${TABLE}.breached ;;
  }

  dimension: business_unit {
    type: string
    sql: ${TABLE}.business_unit ;;
  }

  dimension: categories {
    type: number
    sql: ${TABLE}.categories ;;
  }

  dimension_group: change_completion {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.change_completion_date ;;
  }

  dimension: change_reason {
    type: number
    sql: ${TABLE}.change_reason ;;
  }

  dimension: change_risk {
    type: number
    sql: ${TABLE}.change_risk ;;
  }

  dimension_group: change_start {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.change_start_date ;;
  }

  dimension: change_type {
    type: number
    sql: ${TABLE}.change_type ;;
  }

  dimension: city_port {
    type: string
    sql: ${TABLE}.city_port ;;
  }

  dimension: country_name {
    type: string
    sql: ${TABLE}.country_name ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created ;;
  }

  dimension: creator {
    type: string
    sql: ${TABLE}.creator ;;
  }

  dimension: creator_name {
    type: string
    sql: ${TABLE}.creator_name ;;
  }

  dimension: customer {
    type: string
    sql: ${TABLE}.customer ;;
  }

  dimension: customer_code {
    type: string
    sql: ${TABLE}.customer_code ;;
  }

  dimension: customer_new_name {
    type: string
    sql: ${TABLE}.customer_new_name ;;
  }

  dimension: customer_old_name {
    type: string
    sql: ${TABLE}.customer_old_name ;;
  }

  dimension_group: date {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.date ;;
  }

  dimension_group: date_of_join {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.date_of_join ;;
  }

  dimension: department {
    type: number
    sql: ${TABLE}.department ;;
  }

  dimension: department_name {
    type: string
    sql: ${TABLE}.department_name ;;
  }

  dimension: dept_class_subclass {
    type: string
    sql: ${TABLE}.dept_class_subclass ;;
  }

  dimension: description {
    type: string
    sql: ${TABLE}.description ;;
  }

  dimension: development {
    type: string
    sql: ${TABLE}.development ;;
  }

  dimension: division_boat_ {
    type: number
    sql: ${TABLE}.division_boat_ ;;
  }

  dimension: division_name {
    type: string
    sql: ${TABLE}.division_name ;;
  }

  dimension_group: due {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.due_date ;;
  }

  dimension_group: effective {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.effective_date ;;
  }

  dimension: elapsed_time_hours {
    type: number
    sql: ${TABLE}.elapsed_time_hours ;;
  }

  dimension: elapsed_days {
    type: number
    sql: ${TABLE}.elapsed_time_hours/8 ;;
  }

  dimension: emac_description {
    type: string
    sql: ${TABLE}.emac_description ;;
  }

  dimension: emac_link {
    type: string
    sql: ${TABLE}.emac_link ;;
  }

  dimension: emac_ref_no {
    type: number
    sql: ${TABLE}.emac_ref_no ;;
  }

  dimension: employee_code {
    type: string
    sql: ${TABLE}.employee_code ;;
  }

  dimension: employee_name {
    type: string
    sql: ${TABLE}.employee_name ;;
  }

  dimension: environment {
    type: string
    sql: ${TABLE}.environment ;;
  }

  dimension: epic_link {
    type: number
    sql: ${TABLE}.epic_link ;;
  }

  dimension: error_line_count {
    type: number
    sql: ${TABLE}.error_line_count ;;
  }

  dimension: from_location {
    type: string
    sql: ${TABLE}.from_location ;;
  }

  dimension: goal_duration_hours {
    type: number
    sql: ${TABLE}.goal_duration_hours ;;
  }

  dimension: impact {
    type: number
    sql: ${TABLE}.impact ;;
  }

  dimension: investigation_reason {
    type: number
    sql: ${TABLE}.investigation_reason ;;
  }

  dimension: issue_color {
    type: string
    sql: ${TABLE}.issue_color ;;
  }

  dimension: issue_reported_by {
    type: string
    sql: ${TABLE}.issue_reported_by ;;
  }

  dimension: issue_type {
    type: number
    sql: ${TABLE}.issue_type ;;
  }

  dimension: issue_type_description {
    type: string
    sql: ${TABLE}.issue_type_description ;;
  }

  dimension: issue_type_name {
    type: string
    sql: ${TABLE}.issue_type_name ;;
  }

  dimension: issue_type_subtask {
    type: yesno
    sql: ${TABLE}.issue_type_subtask ;;
  }

  dimension: it_incident_logged_by {
    type: string
    sql: ${TABLE}.it_incident_logged_by ;;
  }

  dimension_group: it_incident_logged {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.it_incident_logged_date ;;
  }

  dimension: it_incident_no {
    type: string
    sql: ${TABLE}.it_incident_no ;;
  }

  dimension: key {
    type: string
    sql: ${TABLE}.key ;;
  }

  dimension: last_contact {
    type: string
    sql: ${TABLE}.last_contact ;;
  }

  dimension_group: last_viewed {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.last_viewed ;;
  }

  dimension: legal_entity {
    type: number
    sql: ${TABLE}.legal_entity ;;
  }

  dimension: legal_entity_for_admin_training_porject {
    type: number
    sql: ${TABLE}.legal_entity_for_admin_training_porject ;;
  }

  dimension: legal_entity_name {
    type: string
    sql: ${TABLE}.legal_entity_name ;;
  }

  dimension: line_count {
    type: number
    sql: ${TABLE}.line_count ;;
  }

  dimension: location_name {
    type: string
    sql: ${TABLE}.location_name ;;
  }

  dimension: locations {
    type: string
    sql: ${TABLE}.locations ;;
  }

  dimension: month_for_corporate_ {
    type: string
    sql: ${TABLE}.month_for_corporate_ ;;
  }

  dimension: name_in_template {
    type: string
    sql: ${TABLE}.name_in_template ;;
  }

  dimension: org_unit {
    type: number
    sql: ${TABLE}.org_unit ;;
  }

  dimension: org_unit_name {
    type: string
    sql: ${TABLE}.org_unit_name ;;
  }

  dimension: org_unit_name_2 {
    type: string
    sql: ${TABLE}.org_unit_name_2 ;;
  }

  dimension: original_estimate {
    type: number
    sql: ${TABLE}.original_estimate ;;
  }

  dimension: parent_id {
    type: number
    sql: ${TABLE}.parent_id ;;
  }

  dimension: parent_link {
    type: number
    sql: ${TABLE}.parent_link ;;
  }

  dimension: paused {
    type: yesno
    sql: ${TABLE}.paused ;;
  }

  dimension: pending_reason {
    type: number
    sql: ${TABLE}.pending_reason ;;
  }

  dimension: pos_corporate {
    type: number
    sql: ${TABLE}.pos_corporate ;;
  }

  dimension: priority {
    type: number
    sql: ${TABLE}.priority ;;
  }

  dimension: project {
    type: number
    sql: ${TABLE}.project ;;
  }

  dimension: project_key {
    type: string
    sql: ${TABLE}.project_key ;;
  }

  dimension: project_name {
    type: string
    sql: ${TABLE}.project_name ;;
  }

  dimension: project_type_key {
    type: string
    sql: ${TABLE}.project_type_key ;;
  }

  dimension: qc_line_count {
    type: number
    sql: ${TABLE}.qc_line_count ;;
  }

  dimension: rdbs_comment {
    type: string
    sql: ${TABLE}.rdbs_comment ;;
  }

  dimension: remaining_estimate {
    type: number
    sql: ${TABLE}.remaining_estimate ;;
  }

  dimension: remaining_time_hours {
    type: number
    sql: ${TABLE}.remaining_time_hours ;;
  }

  dimension: reporter {
    type: string
    sql: ${TABLE}.reporter ;;
  }

  dimension: reporter_name {
    type: string
    sql: ${TABLE}.reporter_name ;;
  }

  dimension: resolution {
    type: number
    sql: ${TABLE}.resolution ;;
  }

  dimension_group: resolved {
    type: time

    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]

    sql: ${TABLE}.resolved ;;
  }

  dimension: root_cause {
    type: string
    sql: ${TABLE}.root_cause ;;
  }

  dimension: rpa_status {
    type: number
    sql: ${TABLE}.rpa_status ;;
  }

  dimension_group: satisfaction {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.satisfaction_date ;;
  }

  dimension: season {
    type: number
    sql: ${TABLE}.season ;;
  }

  dimension: so_number {
    type: string
    sql: ${TABLE}.so_number ;;
  }

  dimension: source {
    type: number
    sql: ${TABLE}.source ;;
  }

#   dimension_group: start {
#     type: time
#     timeframes: [
#       raw,
#       date,
#       week,
#       month,
#       quarter,
#       year
#     ]
#     convert_tz: no
#     datatype: date
#     sql: ${TABLE}.start_date ;;
#   }

  dimension_group: start {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.start_time ;;
  }

  dimension: status {
    type: number
    sql: ${TABLE}.status ;;
  }

  dimension_group: status_category_changed {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.status_category_changed ;;
  }

  dimension: status_category_id {
    type: number
    sql: ${TABLE}.status_category_id ;;
  }

  dimension: status_description {
    type: string
    sql: ${TABLE}.status_description ;;
  }

  dimension: status_name {
    type: string
    sql: ${TABLE}.status_name ;;
  }

  dimension_group: stop {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.stop_time ;;
  }

  dimension: store_code {
    type: string
    sql: ${TABLE}.store_code ;;
  }

  dimension: store_wh {
    type: number
    sql: ${TABLE}.store_wh ;;
  }

  dimension: story_point_estimate {
    type: number
    sql: ${TABLE}.story_point_estimate ;;
  }

  dimension: sub_vertical {
    type: number
    sql: ${TABLE}.sub_vertical ;;
  }

  dimension: sub_vertical_name {
    type: string
    sql: ${TABLE}.sub_vertical_name ;;
  }

  dimension: summary {
    type: string
    sql: ${TABLE}.summary ;;
  }

  dimension: supplier_code {
    type: string
    sql: ${TABLE}.supplier_code ;;
  }

  dimension: supplier_new_name {
    type: string
    sql: ${TABLE}.supplier_new_name ;;
  }

  dimension: supplier_old_name {
    type: string
    sql: ${TABLE}.supplier_old_name ;;
  }

  dimension: supplier_site_details {
    type: string
    sql: ${TABLE}.supplier_site_details ;;
  }
  dimension: Oracle_RefId {
    type: string
    description: "Oracle RefId"
    sql: ${TABLE}.Oracle_Ref_Id  ;;
  }

  dimension_group: target_end {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.target_end ;;
  }

  dimension_group: target_start {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.target_start ;;
  }

  dimension: ticket_no {
    type: string
    sql: ${TABLE}.ticket_no ;;
  }

  dimension: time_spent {
    type: number
    sql: ${TABLE}.time_spent ;;
  }

  dimension: to_location {
    type: string
    sql: ${TABLE}.to_location ;;
  }

  dimension: type {
    type: number
    sql: ${TABLE}.type ;;
  }

  dimension: update_required_for {
    type: number
    sql: ${TABLE}.update_required_for ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated ;;
  }

  dimension: ProjectOrder {
    type:  number
    sql:CASE
          WHEN  ${project_name} = "PO,TRANSFERS & RETURN" THEN  1
          WHEN  ${project_name} = "PRICE MANAGEMENT" THEN  2
          WHEN  ${project_name} = "SALES ORDER" THEN 3
          WHEN  ${project_name} = "DROP SHIPMENT" THEN  4
          WHEN  ${project_name} = "INBOUND STOCK VARIANCE" THEN  5
          WHEN  ${project_name} = "STOCK READINESS" THEN  6
  WHEN  ${project_name} = "INVENTORY" THEN  7
           WHEN  ${project_name} = "ITEM" THEN 8
          WHEN  ${project_name} = "SUPPLIER" THEN   9

          WHEN  ${project_name} = "CUSTOMER" THEN   10
          WHEN  ${project_name} = "ACCOUNTING" THEN 11

          WHEN  ${project_name} = "ORG SET UP" THEN  12
          WHEN  ${project_name} = "NMP/ICSB" THEN  13
         WHEN  ${project_name} = "OPEN TEXT" THEN 14
      end;;
  }

  dimension: ProjectGroup{
    type:  string
    label: "Scope"
    sql:CASE
          WHEN  ${project_name} = "PO,TRANSFERS & RETURN" THEN  'Transactional'
          WHEN  ${project_name} = "PRICE MANAGEMENT" THEN  'Transactional'
          WHEN  ${project_name} = "SALES ORDER" THEN 'Transactional'
          WHEN  ${project_name} = "DROP SHIPMENT" THEN  'Transactional'
          WHEN  ${project_name} = "INBOUND STOCK VARIANCE" THEN  'Transactional'
          WHEN  ${project_name} = "INVENTORY" THEN  'Transactional'
          WHEN  ${project_name} = "STOCK READINESS" THEN   'Transactional'
           WHEN  ${project_name} = "ITEM" THEN  'MDM Scope'
          WHEN  ${project_name} = "SUPPLIER" THEN   'MDM Scope'

          WHEN  ${project_name} = "CUSTOMER" THEN   'MDM Scope'
          WHEN  ${project_name} = "ACCOUNTING" THEN 'MDM Scope'

          WHEN  ${project_name} = "ORG SET UP" THEN  'MDM Scope'
          WHEN  ${project_name} = "NMP/ICSB" THEN  'MDM Scope'
         WHEN  ${project_name} = "OPEN TEXT" THEN 'MDM Scope'
      end;;
  }

  dimension: VerticalGroup{
    type:  string
       sql:CASE
           WHEN  ${vertical_name} = "BEAUTY" THEN  'MANAGED COMPANIES'
           WHEN  ${vertical_name} = "FASHION" THEN  'MANAGED COMPANIES'
           WHEN  ${vertical_name} = "LEVEL" THEN  'MANAGED COMPANIES'
          WHEN  ${vertical_name} = "MANAGED COMPANIES" THEN  'MANAGED COMPANIES'
           WHEN  ${vertical_name} = "CHRISTOFLE" THEN  'CHRISTOFLE'
           WHEN  ${vertical_name} = "COUNTRY MANAGEMENT" THEN  'COUNTRY MANAGEMENT'
           WHEN  ${vertical_name} = "JOINT VENTURES" THEN  'JOINT VENTURES'
           WHEN  ${vertical_name} = "SUPPORT SERVICES" THEN  'ENABLERS'
           WHEN  ${vertical_name} = "DISCONTINUED" THEN  'COUNTRY MANAGEMENT'
           WHEN  ${vertical_name} = "OUT OF SCOPE" THEN  'COUNTRY MANAGEMENT'
          ELSE

          'COUNTRY MANAGEMENT'

         end;;
  }

  dimension: urgency {
    type: number
    sql: ${TABLE}.urgency ;;
  }

  dimension: vendor {
    type: string
    sql: ${TABLE}.vendor ;;
  }

  dimension: vertical {
    type: number
    sql: ${TABLE}.vertical ;;
  }

  dimension: vertical_name {
    type: string
    sql: ${TABLE}.vertical_name ;;
  }

  dimension: warehouse {
    type: string
    sql: ${TABLE}.warehouse ;;
  }

  dimension: within_calendar_hours {
    type: yesno
    sql: ${TABLE}.within_calendar_hours ;;
  }

  dimension: work_ratio {
    type: number
    sql: ${TABLE}.work_ratio ;;
  }

  dimension: workaround {
    type: string
    sql: ${TABLE}.workaround ;;
  }

  dimension: working_hours {
    type: string
    sql: ${TABLE}.working_hours ;;
  }

  dimension: year {
    type: number
    sql: ${TABLE}.year ;;
  }
  dimension: request_type {
    type: string
    sql: ${TABLE}.request_type ;;
  }

  measure: issue_count {
    type: count_distinct
    label: "Number of distinct issues"
    description: "Number of distinct issues"
    sql: ${id} ;;
  }

  measure: resolved_count {
    type: count_distinct
    label: "Number of resolved issues"
    description: "Count of resolved issues"
    sql: CASE WHEN ${TABLE}.resolved  is not null and  (${status_name} ='Completed' or ${status_name} ='Cancelled')
   then ${id} end;;
  }

  measure: Created_count {
    type: count_distinct
    label: "Number of Created issues"
    description: "Count of Created issues"
    sql: ${id} ;;
  }
  measure: Percentage_Resolved {
    type: percent_of_total
    label: "Percent of resolved requests"
    description: "Percent of resolved requests"
    sql:  ${resolved_count}/${Created_count} ;;
  }

  measure: sum_line_count {
    type: sum
    value_format: "[>=1000] 000.00,\" K\";000"
    sql: ${line_count} ;;
  }



measure: count_Ageing_1 {
  label: "Less than 1 Day"
  type: count_distinct
  sql:case when  ${elapsed_days} <= 1 then ${id}
  end ;;
}

  measure: count_Ageing_2 {
    label: "1-2 Days"
    type: count_distinct
    sql:case when  ${elapsed_days} > 1 and  ${elapsed_days} <= 2  then ${id}
      end ;;
  }
  measure: count_Ageing_3 {
    label: "2-3 Days"
    type: count_distinct
    sql:case when  ${elapsed_days} > 2 and  ${elapsed_days} <= 3  then ${id}
      end ;;
  }
  measure: count_Ageing_4 {
    label: "3-4 Days"
    type: count_distinct
    sql:case when  ${elapsed_days} > 3 and  ${elapsed_days} <= 4  then ${id}
      end ;;
  }
  measure: count_Ageing_5 {
    label: "4-5 Days"
    type: count_distinct
    sql:case when  ${elapsed_days} > 4 and  ${elapsed_days} <= 5  then ${id}
      end ;;
  }
  measure: count_Ageing_6 {
    label: "5-6 Days"
    type: count_distinct
    sql:case when  ${elapsed_days} > 5 and  ${elapsed_days} <= 6  then ${id}
      end ;;
  }
  measure: count_Ageing_7 {
    label: "6-7 Days"
    type: count_distinct
    sql:case when  ${elapsed_days} > 6 and  ${elapsed_days} <= 7  then ${id}
      end ;;
  }
  measure: count_Ageing_8 {
    label: "7-8 Days"
    type: count_distinct
    sql:case when  ${elapsed_days} > 7 and  ${elapsed_days} <= 8  then ${id}
      end ;;
  }
  measure: count_Ageing_9 {
    label: "8-9 Days"
    type: count_distinct
    sql:case when  ${elapsed_days} > 8 and  ${elapsed_days} <= 9  then ${id}
      end ;;
  }
   measure: count_Ageing_10 {
    label: "9-10 Days"
    type: count_distinct
    sql:case when  ${elapsed_days} > 9 and  ${elapsed_days} <= 10  then ${id}
      end ;;
  }
  measure: count_Ageing_15 {
    label: "10-15 Days"
    type: count_distinct
    sql:case when  ${elapsed_days} > 10 and  ${elapsed_days} <= 15  then ${id}
      end ;;
  }
  measure: count_Ageing_20 {
    label: "15-20 Days"
    type: count_distinct
    sql:case when  ${elapsed_days} > 15 and  ${elapsed_days} <= 20  then ${id}
      end ;;
  }
  measure: count_Ageing_Greater20 {
    label: ">20 Days"
    type: count_distinct
    sql:case when  ${elapsed_days} >  20   then ${id}
      end ;;
  }
  measure: 95Percntile {
    label: "95th Percentile"
    description: "To calculate 95th percetile of elapsed time"
    type: percentile_distinct
    percentile: 95
    sql: ${elapsed_time_hours}/8
    ;;
  }

measure: Datediff {
  label: "Calendar Days for Pending"
  description: "To calculate resolved date and created date difference"
  type: number
  sql: CURRENT_DATE-${created_date} ;;
}

measure: Businessdays{
  type: number
  sql:
  ${resolved_count};;
}

  dimension: Time_spent_with_Request{
    type: number
    sql:  DATETIME_DIFF(cast(${resolved_raw} as datetime), cast(${created_raw} as datetime), MINUTE)  ;;


  }
  measure: Total_Time_Spent {
    type: sum_distinct
    label: "Total Time spent"
    description: "Total Time spent in Minutes"
    sql: CASE WHEN ${TABLE}.resolved  is not null and  (${status_name} ='Completed')
      then ${Time_spent_with_Request} end;;
  }


  dimension: divisionName {
    type: string
    description: "Divison/Boat Name"
    sql: ${division_name} ;;
  }

  dimension:  Time_Minutes_Taken_per_category{
    type:  number
    sql:CASE
           WHEN  ${request_type} = "Barcode Sticker Issue" THEN  7
           WHEN  ${request_type} = "BI Mapping Internal Sellout" THEN  15
           WHEN  ${request_type} = "COA Addition" THEN  10
         WHEN  ${request_type} = "COA Deactivate/Reactivate" THEN  10
           WHEN  ${request_type} = "COA Updates" THEN  10
           WHEN  ${request_type} = "Commision Set Up / Update" THEN  1
          WHEN  ${request_type} = "Concession Set Up" THEN  8.3
           WHEN  ${request_type} = "Currency/Exchange Rate" THEN  45
          WHEN  ${request_type} = "Customer Creation" THEN  16
           WHEN  ${request_type} = "Customer Creation Data Collection" THEN  10
          WHEN  ${request_type} = "Customer Discount Group" THEN  14
           WHEN  ${request_type} = "Customer List of Values" THEN  6.3
          WHEN  ${request_type} = "Customer Update" THEN  6.3
           WHEN  ${request_type} = "Damages" THEN  10
          WHEN  ${request_type} = "Delivery Follow Up" THEN  10
           WHEN  ${request_type} = "Dept Loc Ranging" THEN  1.3
          WHEN  ${request_type} = "COA Updates" THEN  10
           WHEN  ${request_type} = "Discount & Promotions" THEN  25
          WHEN  ${request_type} = "Distribution Price Upload" THEN  4
           WHEN  ${request_type} = "Drop Shipment Order" THEN  30
          WHEN  ${request_type} = "Employee Segment" THEN  3.2
           WHEN  ${request_type} = "Expense Profile" THEN  6
          WHEN  ${request_type} = "GL Mapping – Department" THEN  8.3
           WHEN  ${request_type} = "GL Mapping – DepLocation" THEN  8.3
          WHEN  ${request_type} = "GL Mapping – TranCode" THEN  8.3
           WHEN  ${request_type} = "ICSB-Relationship" THEN  5
          WHEN  ${request_type} = "Inbound stock Variance" THEN  1.4
           WHEN  ${request_type} = "Internal Bank Account" THEN  45
           WHEN  ${request_type} = "Item Creation" THEN  1
           WHEN  ${request_type} = "Item Loc Traits" THEN  0.05
          WHEN  ${request_type} = "Item Reclassification" THEN  1
           WHEN  ${request_type} = "Item Supplier Ranging" THEN  0.5
           WHEN  ${request_type} = "Item Updates" THEN  0.4
           WHEN  ${request_type} = "Location Closure" THEN  15
          WHEN  ${request_type} = "Location Creation" THEN  15
           WHEN  ${request_type} = "Location Update" THEN  15
          WHEN  ${request_type} = "Merchandise Hierarchy" THEN  1.3
           WHEN  ${request_type} = "NMP/ICSB Item Creation" THEN  5
          WHEN  ${request_type} = "NMP/ICSB Update" THEN  5
           WHEN  ${request_type} = "NMP/ICSB Other" THEN  5
           WHEN  ${request_type} = "ICSB Relationship" THEN  5
           WHEN  ${request_type} = "Org Structure Closure" THEN  25
           WHEN  ${request_type} = "Org Structure Creation" THEN  25
          WHEN  ${request_type} = "Org Structure Update" THEN  25
           WHEN  ${request_type} = "PO Cancellation" THEN  1
           WHEN  ${request_type} = "PO Creation" THEN  3
           WHEN  ${request_type} = "PO Update" THEN  3
           WHEN  ${request_type} = "Queries" THEN  5
          WHEN  ${request_type} = "Retail Price Change" THEN  0.3
           WHEN  ${request_type} = "RPM Attachment" THEN  3.3
           WHEN  ${request_type} = "SO Cancellation" THEN  1
           WHEN  ${request_type} = "SO Creation" THEN  8
          WHEN  ${request_type} = "Emailed request" THEN  8
           WHEN  ${request_type} = "SO Queries" THEN  5
          WHEN  ${request_type} = "Special Order Inquiry" THEN  15
           WHEN  ${request_type} = "Special Order Placement" THEN  3
           WHEN  ${request_type} = "Stock Adjustments" THEN  1.4
           WHEN  ${request_type} = "Stock Return to Vendor" THEN  3
           WHEN  ${request_type} = "Stock Return to warehouse" THEN  3
           WHEN  ${request_type} = "Stock Take Readiness" THEN  10
           WHEN  ${request_type} = "StoreCustomerDiscount Group" THEN  15
           WHEN  ${request_type} = "Supplier Creation" THEN  16
          WHEN  ${request_type} = "Supplier Creation Data Collection" THEN 10
           WHEN  ${request_type} = "Supplier List of Values" THEN  6
           WHEN  ${request_type} = "Supplier Payment" THEN  5
           WHEN  ${request_type} = "Supplier Updates" THEN  6.3
           WHEN  ${request_type} = "Transfer Cancellation" THEN  2.5
           WHEN  ${request_type} = "Transfers" THEN  3
           WHEN  ${request_type} = "Unit Cost & Retail Price" THEN  0.4
           WHEN  ${request_type} = "Unit Cost Change" THEN  0.3
          WHEN  ${request_type} =  "Customer Creation Data Collection" THEN  10
           WHEN  ${request_type} = "E-Order Booking" THEN  5
           WHEN  ${request_type} = "E-Invoice Create" THEN  30
           WHEN  ${request_type} = "E-forwarder Booking" THEN  25
           WHEN  ${request_type} = "E-Invoice Cancel" THEN  10
           WHEN  ${request_type} = "Employee Supplier Creation" THEN  2
           WHEN  ${request_type} = "SUPPLIER DATA COLLECTION" THEN  0.4
          WHEN  ${request_type} = "Supplier Data Collection" THEN  0.4



        end;;

  }
  measure: Time_Spent_by_subvertical {
    type: sum_distinct
    label: "Time spent by subvertical"
    description: "Time spent by subvertical per category"
    sql: CASE WHEN ${TABLE}.resolved  is not null and  (${status_name} ='Completed')
      then  ( ${Time_Minutes_Taken_per_category} * ${line_count} )
      end;;
  }




  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      vertical_name,
      project_name,
      sub_vertical_name,
      location_name,
      customer_new_name,
      supplier_new_name,
      status_name,
      department_name,
      employee_name,
      division_name,
      reporter_name,
      issue_type_name,
      org_unit_name,
      assignee_name,
      country_name,
      customer_old_name,
      creator_name,
      supplier_old_name,
      legal_entity_name
    ]
  }

}
