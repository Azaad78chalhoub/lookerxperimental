view: mdm_jira_sla {
  sql_table_name: `chb-prod-stage-mdm-rdbs.prod_mdm.mdm_jira_sla`
    ;;

  dimension: _fivetran_id {
    type: string
    sql: ${TABLE}._fivetran_id ;;
  }

  dimension_group: _fivetran_synced {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}._fivetran_synced ;;
  }

  dimension: breached {
    type: yesno
    sql: ${TABLE}.breached ;;
  }

  dimension: elapsed_time {
    type: number
    sql: ${TABLE}.elapsed_time ;;
  }

  dimension: goal_duration {
    type: number
    sql: ${TABLE}.goal_duration ;;
  }

  dimension: is_ongoing_cycle {
    type: yesno
    sql: ${TABLE}.is_ongoing_cycle ;;
  }

  dimension: issue_id {
    type: number
    sql: ${TABLE}.issue_id ;;
  }

  dimension: paused {
    type: yesno
    sql: ${TABLE}.paused ;;
  }

  dimension: remaining_time {
    type: number
    sql: ${TABLE}.remaining_time ;;
  }

  dimension: sla_id {
    type: number
    sql: ${TABLE}.sla_id ;;
  }

  dimension_group: start {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.start_time ;;
  }

  dimension_group: stop {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.stop_time ;;
  }

  dimension: within_calendar_hours {
    type: yesno
    sql: ${TABLE}.within_calendar_hours ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
