view: dio_stock_cover {
  derived_table: {
    explore_source: fact_soh_sales_poc {
      column: stock_raw_date {}
      column: granularity_from_parameter {}
      column: granularity_from_parameter_b {}
      column: sum_inv_soh_qty {}
      column: average_stock_value {}
      column: avg_last30days_cogs_amountusd_average {}
      column: avg_last90days_cogs_amountusd_average {}
      column: avg_lastyear_cogs_amountusd_average {}
      column: rolling_sum_28_days {}
      column: total_quantity_sold {}
      column: in_stock_count {}
      bind_all_filters: yes
      # filters: {
      #   field: fact_soh_sales_poc.stock_date
      #   value: "12 months"
      # }
    }
  }

  dimension: pk {
    primary_key: yes
    hidden: yes
    type: string
    sql: CONCAT(${granularity_from_parameter}, ' - ', ${granularity_from_parameter_b}, ' - ', ${stock_raw_date}) ;;
  }


  dimension: granularity_from_parameter {hidden: yes}
  dimension: granularity_from_parameter_b {hidden: yes}
  dimension: stock_raw_date {hidden: yes}

  dimension: sum_inv_soh_qty {
    hidden: yes
    type: number
  }


  dimension: average_stock_value {
    hidden: yes
    type: number
  }


  dimension: avg_last30days_cogs_amountusd_average {
    hidden: yes
    type: number
  }
  dimension: avg_last90days_cogs_amountusd_average {
    hidden: yes
    type: number
  }
  dimension: avg_lastyear_cogs_amountusd_average {
    hidden: yes
    type: number
  }
  dimension: stock_cover_lm_cogs {
    type: number
    hidden: yes
    sql: SAFE_DIVIDE(${average_stock_value},${avg_last30days_cogs_amountusd_average}) ;;
    value_format_name: decimal_2
    html: {{rendered_value}} Days ;;
  }

  dimension: stock_cover_lq_cogs {
    type: number
    hidden: yes
    sql: SAFE_DIVIDE(${average_stock_value},${avg_last90days_cogs_amountusd_average}) ;;
    value_format_name: decimal_2
    html: {{rendered_value}} Days ;;
  }

  dimension: stock_cover_ly_cogs {
    type: number
    hidden:yes
    sql: SAFE_DIVIDE(${average_stock_value},${avg_lastyear_cogs_amountusd_average}) ;;
    value_format_name: decimal_2
  }



  measure: average_stock_cover_lm_cogs {
    type: average
    hidden: yes
    label: "Stock Cover by Value Monthy"
    view_label: "-- Stock Cover and GMROI BETA"
    value_format_name: decimal_0
    sql: 1.0*${stock_cover_lm_cogs};;
    html: {{rendered_value}} Days ;;
  }

  measure: average_stock_cover_lq_cogs {
    type: average
    hidden: yes
    label: "Stock Cover by Value Quartery"
    view_label: "-- Stock Cover and GMROI BETA"
    value_format_name: decimal_0
    sql: 1.0*${stock_cover_lq_cogs};;
    html: {{rendered_value}} Days ;;
  }

  measure: average_stock_cover_ly_cogs {
    type: average
    hidden: yes
    label: "Stock Cover by Value Yearly"
    view_label: "-- Stock Cover and GMROI BETA"
    value_format_name: decimal_0
    sql: 1.0*${stock_cover_ly_cogs};;
    html: {{rendered_value}} Days ;;
  }



}


# dimension: daily_quantity_sold {
#   type: number
#   sql: ${rolling_sum_28_days} / 28.0 ;;
# }
#
# dimension: stock_cover {
#   type: number
#   sql: 1.0 * ${in_stock_count} / NULLIF(${daily_quantity_sold},0) ;;
#   value_format_name: decimal_2
#   html: {{rendered_value}} Days ;;
# }
# measure: average_stock_cover {
#   type: average
#   value_format_name: decimal_2
#   sql: ${stock_cover};;
#   html: {{rendered_value}} Days ;;
# }
