view: dim_prod_org_concession {
  sql_table_name: `chb-prod-supplychain-data.prod_shared_dimensions.dim_prod_org_concession`
    ;;

  dimension: pk {
    type: number
    primary_key: yes
    hidden: yes
    sql: concat(${prod_num},${org_num}) ;;
  }

  dimension: class {
    type: number

    hidden: yes
    sql: ${TABLE}.class ;;
  }

  dimension: concession_bu {
    type: string
    hidden: yes
    sql: ${TABLE}.concession_bu ;;
  }

  dimension: concession_store {
    type: number
    hidden: yes
    sql: ${TABLE}.concession_store ;;
  }

  dimension: concessionstore_name {
    type: string
    label: "Concession Store"
    view_label: "Concession"
    sql: ${TABLE}.concessionstore_name ;;
  }

  dimension: dept {
    type: number
    hidden: yes
    sql: ${TABLE}.dept ;;
  }

  dimension: gender {
    type: number
    hidden: yes
    sql: ${TABLE}.gender ;;
  }

  dimension: line {
    type: number
    hidden: yes
    sql: ${TABLE}.line ;;
  }

  dimension: org_num {
    type: string
    hidden: yes
    sql: ${TABLE}.org_num ;;
  }

  dimension: prod_num {
    type: string
    hidden: yes
    sql: ${TABLE}.prod_num ;;
  }

  dimension: subclass {
    type: number
    hidden: yes
    sql: ${TABLE}.subclass ;;
  }

  dimension: concession_product {
    type: yesno
    hidden: no
    label: "Is Store Concession?"
    view_label: "Concession"
    sql: ${concession_store} IS NOT NULL AND ${concessionstore_name} != "Non-Chalhoub";;
  }

  dimension: is_ext_concession {
    type: yesno
    view_label: "Concession"
    label: "External Concession"
    description: "Flag to identify external concession items"
    sql: ${concessionstore_name} like "%EXTERNAL CONCESSION%";;
  }

  dimension: is_int_concession {
    type: yesno
    view_label: "Concession"
    label: "Internal Concession"
    description: "Flag to identify internal concession items"
    sql: ${concession_store} != 0 AND ${concessionstore_name} not like "%EXTERNAL CONCESSION%";;
  }

  measure: count {
    type: count
    hidden: yes
    drill_fields: [concessionstore_name]
  }
}
