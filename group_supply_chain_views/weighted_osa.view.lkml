view: weighted_osa {
  derived_table: {
    explore_source: fact_soh_sales_poc {
      column: granularity_from_parameter {}
      column: granularity_from_parameter_b {}
      column: grading { field: sales_grading.pareto_grade }
      column: on_shelf_availability {}
      bind_all_filters: yes
    }
  }

  dimension: pk {
    primary_key: yes
    hidden: yes
    type: string
    sql: CONCAT(${granularity_from_parameter}, ' - ', ${granularity_from_parameter_b}, ' - ', ${grading}) ;;
  }

  dimension: granularity_from_parameter {
    hidden: yes
    type: string
  }

  dimension: granularity_from_parameter_b {
    hidden: yes
    type: string
  }

  dimension: grading {
    hidden: yes
    type: string
  }

  dimension: on_shelf_availability {
    hidden: yes
    type: number
  }

#   dimension: osa_temp {
#     hidden: yes
#     type: number
#     sql:
#     CASE
#       WHEN ${grading} = 'A' THEN 0.605
#       WHEN ${grading} = 'B' THEN 0.185
#       WHEN ${grading} = 'C' THEN 0.210
#       ELSE 1
#     END * ${on_shelf_availability}
#     ;;
#   }

  dimension: osa_temp {
    hidden: yes
    type: number
    sql: 1.0 * ${weightings.total_sales_amt_usd_percent} * ${on_shelf_availability} ;;
  }

  measure: weighted_osa {
    view_label: "-- Supply Chain KPIs (use params!)"
    description: "Availability with stronger weights for grade A products"
    type: sum
    sql: ${osa_temp} ;;
    value_format_name: percent_0
  }

}


view: weightings {
  derived_table: {
    explore_source: fact_soh_sales_poc {
      column: granularity_from_parameter {}
      column: granularity_from_parameter_b {}
      column: pareto_grade { field: sales_grading.pareto_grade }
      column: on_shelf_availability {}
      column: total_sales_amt_usd {}
      derived_column: total_sales_amt_usd_percent {
        sql:  SAFE_DIVIDE(total_sales_amt_usd,SUM(total_sales_amt_usd) OVER (PARTITION BY granularity_from_parameter,granularity_from_parameter_b)) ;;
      }
      bind_all_filters: yes
    }
  }

  dimension: pk {
    primary_key: yes
    hidden: no
    sql: CONCAT(${granularity_from_parameter}, " - ", ${granularity_from_parameter_b}, " - ", ${pareto_grade}) ;;
  }

  dimension: pareto_grade {
    primary_key: no
    hidden: no
  }

  dimension: granularity_from_parameter {
    hidden: yes
    type: string
  }

  dimension: granularity_from_parameter_b {
    hidden: yes
    type: string
  }


  dimension: on_shelf_availability {
    hidden: yes
    type: number
  }

  dimension: total_sales_amt_usd {}

  dimension: total_sales_amt_usd_percent {
    label: "per Grade Contribution"
    hidden: no
    value_format_name: decimal_2
    type: number
  }


}
