view: reversals{
  derived_table: {
    sql:
SELECT *,
SUM(provisions_usd_amount) OVER (ORDER BY org_num,brand,date ROWS BETWEEN 1 PRECEDING AND 1 PRECEDING) AS provisions_usd_amount_lm,
SUM(provisions_local_amount) OVER (ORDER BY org_num,brand,date ROWS BETWEEN 1 PRECEDING AND 1 PRECEDING) AS provisions_local_amount_lm
FROM
(SELECT

  date,
  org_num,
  brand,
  SUM(provisions_usd_amount) as provisions_usd_amount,
  SUM(provisions_local_amount) as provisions_local_amount
FROM
(SELECT  *,
CASE WHEN ROUND(age/30) <= 6 AND entity_type != "COMMON" THEN 0
          WHEN ROUND(age/30) > 6 AND ROUND(age/30)  <= 26 AND (entity_type = "FASHION" OR entity_type = "EXPIRY (FASHION)" )
          THEN soh_av_cost_amt_lcl*prov_running_qty*0.05/conversion_rate
          WHEN ROUND(age/30) > 6 AND ROUND(age/30)  <= 46 AND (entity_type = "BEAUTY" OR entity_type = "GIFT" )
          THEN
          soh_av_cost_amt_lcl*prov_running_qty*0.025/conversion_rate
          WHEN entity_type = "COMMON" AND ROUND(age/30)= 0
          THEN soh_av_cost_amt_lcl*prov_running_qty/conversion_rate
          ELSE 0
          END AS provisions_monthly_usd_amount,
-- calculating the sum of provision in usd
CASE WHEN ROUND(age/30) <= 6 AND entity_type != "COMMON" THEN 0
    WHEN ROUND(age/30) > 6 AND ROUND(age/30)  <= 26 AND (entity_type = "FASHION" OR entity_type = "EXPIRY (FASHION)" )
    THEN soh_av_cost_amt_lcl*prov_running_qty*0.05*(ROUND(age/30)-6)/conversion_rate
    WHEN ROUND(age/30) > 26 AND (entity_type = "FASHION" OR entity_type = "EXPIRY (FASHION)" )
    THEN soh_av_cost_amt_lcl*prov_running_qty/conversion_rate
    WHEN ROUND(age/30) > 6 AND ROUND(age/30)  <= 46 AND (entity_type = "BEAUTY" OR entity_type = "GIFT" )
    THEN
    soh_av_cost_amt_lcl*prov_running_qty*0.025*(ROUND(age/30)-6)/conversion_rate
    WHEN ROUND(age/30) > 46 AND (entity_type = "BEAUTY" OR entity_type = "GIFT" )
    THEN soh_av_cost_amt_lcl*prov_running_qty/conversion_rate
    WHEN entity_type = "COMMON"
    THEN soh_av_cost_amt_lcl*prov_running_qty/conversion_rate
    ELSE 0
    END AS provisions_usd_amount,
-- calculating the sum of monthly provision in local currency
CASE WHEN ROUND(age/30) <= 6 AND entity_type != "COMMON" THEN 0
          WHEN ROUND(age/30) > 6 AND ROUND(age/30)  <= 26 AND (entity_type = "FASHION" OR entity_type = "EXPIRY (FASHION)" )
          THEN soh_av_cost_amt_lcl*prov_running_qty*0.05
          WHEN ROUND(age/30) > 6 AND ROUND(age/30)  <= 46 AND (entity_type = "BEAUTY" OR entity_type = "GIFT" )
          THEN
          soh_av_cost_amt_lcl*prov_running_qty*0.025
          WHEN entity_type = "COMMON" AND ROUND(age/30)= 0
          THEN soh_av_cost_amt_lcl*prov_running_qty
          ELSE 0
          END AS provisions_monthly_local_amount,
-- calculating the sum of provision in local currency
CASE WHEN ROUND(age/30) <= 6 AND entity_type != "COMMON" THEN 0
    WHEN ROUND(age/30) > 6 AND ROUND(age/30)  <= 26 AND (entity_type = "FASHION" OR entity_type = "EXPIRY (FASHION)" )
    THEN soh_av_cost_amt_lcl*prov_running_qty*0.05*(ROUND(age/30)-6)
    WHEN ROUND(age/30) > 26 AND (entity_type = "FASHION" OR entity_type = "EXPIRY (FASHION)" )
    THEN soh_av_cost_amt_lcl*prov_running_qty
    WHEN ROUND(age/30) > 6 AND ROUND(age/30)  <= 46 AND (entity_type = "BEAUTY" OR entity_type = "GIFT" )
    THEN
    soh_av_cost_amt_lcl*prov_running_qty*0.025*(ROUND(age/30)-6)
    WHEN ROUND(age/30) > 46 AND (entity_type = "BEAUTY" OR entity_type = "GIFT" )
    THEN soh_av_cost_amt_lcl*prov_running_qty
    WHEN entity_type = "COMMON"
    THEN soh_av_cost_amt_lcl*prov_running_qty
    ELSE 0
    END AS provisions_local_amount,
FROM
(SELECT
  date,
  org_num,
  prod_num,
  age,
  prov_running_qty,
  soh_av_cost_amt_lcl,
  conversion_rate,
  brand,
  entity_type
FROM
  `chb-prod-supplychain-data.prod_supply_chain.fact_provision_soh` prov
LEFT JOIN
  `chb-prod-supplychain-data.prod_shared_dimensions.dim_retail_product` products
ON
  prov.prod_num=products.item)
)
GROUP BY
  date,
  org_num,
  brand
)
;;
  }


  dimension_group: provision {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.date AS TIMESTAMP)  ;;
  }
  measure: last_updated_date {
    type: date
    label: "Last refresh date"
    sql: MAX(${provision_raw}) ;;
    convert_tz: no
  }



  dimension: org_num {
    type: string
    label: "Location No."
    view_label: "Locations"
    sql: ${TABLE}.org_num ;;
  }

  dimension: brand {
    type: string
    hidden: no
    label: "Product Brand"
    value_format_name: id
    sql: ${TABLE}.brand ;;
  }


  dimension_group: period_end_dt {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.period_end_dt AS TIMESTAMP) ;;
  }

  dimension: pr_num {
    type: string
    hidden: yes
    sql: ${TABLE}.pr_num ;;
  }

  dimension: pr_start_dt_wid {
    type: number
    hidden: yes
    value_format_name: id
    sql: ${TABLE}.pr_start_dt_wid ;;
  }

  dimension: pr_wid {
    type: number
    hidden: yes
    value_format_name: id
    sql: ${TABLE}.pr_wid ;;
  }


  dimension: provisions_local_amount_lm {
    type: number
    hidden: yes
    sql: ${TABLE}.provisions_local_amount_lm ;;
  }

  dimension: provisions_usd_amount_lm {
    type: number
    hidden: yes
    sql: ${TABLE}.provisions_usd_amount_lm ;;
  }


  dimension: provisions_usd_amount {
    type: number
    hidden: yes
    sql: ${TABLE}.provisions_usd_amount ;;
  }


  dimension: provisions_local_amount {
    type: number
    hidden: yes
    sql: ${TABLE}.provisions_local_amount ;;
  }


  measure: provision_usd_sum {
    type: sum
    label: "Provision USD"
    value_format_name: usd
    hidden: no
    sql: ${provisions_usd_amount};;
  }
  measure: provision_usd_sum_lm {
    type: sum
    label: "Provision USD Last Month"
    value_format_name: usd
    hidden: no
    sql: ${provisions_usd_amount_lm};;
  }
  measure: provision_local_sum {
    type: sum
    label: "Provision Local"
    value_format_name: decimal_2
    hidden: no
    sql: ${provisions_local_amount};;
  }
  measure: provision_local_sum_lm {
    type: sum
    label: "Provision Local Last Month"
    value_format_name: decimal_2
    hidden: no
    sql: ${provisions_local_amount_lm};;
  }

  measure: reversal_usd {
    type: number
    label: "Reversal Amount USD"
    value_format_name: usd
    hidden: no
    sql: CASE WHEN SUM(${provisions_usd_amount})-SUM(${provisions_usd_amount_lm}) >= 0 THEN 0 ELSE ABS(SUM(${provisions_usd_amount})-SUM(${provisions_usd_amount_lm})) END ;;
  }

  measure: reversal_local {
    type: number
    label: "Reversal Amount Local"
    value_format_name: decimal_2
    hidden: no
    sql: CASE WHEN SUM(${provisions_local_amount})-SUM(${provisions_local_amount_lm}) >= 0 THEN 0 ELSE ABS(SUM(${provisions_local_amount})-SUM(${provisions_local_amount_lm})) END;;
  }




  measure: count {
    type: count
    hidden: yes
  }
}
