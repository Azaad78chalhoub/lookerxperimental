include: "group_supply_chain.model.lkml"

view: employee_cross_sell {
  derived_table: {
    explore_source: fact_retail_sales {
      column: empcode { field: dim_employee_details.empcode }
      column: employee_name { field: dim_employee_details.employee_name }
      column: distinct_zone {}
      column: distinct_dept_zone {}
      column: atr_cginvoiceno {}
      column: quantity_sold {}
      column: net_sales_amount_local {}
      bind_all_filters: yes
    }
  }

  dimension: pk {
    primary_key: yes
    hidden: yes
    sql: CONCAT(${empcode},${employee_name},${atr_cginvoiceno}) ;;
  }

  dimension: empcode {
    label: "Employee Details Employee-ID"
    hidden: yes
    sql: ${TABLE}.empcode ;;
  }

  dimension: employee_name {
    label: "Employee Details Employee Name"
    hidden: yes
    sql: ${TABLE}.employee_name ;;
  }

  dimension: distinct_zone {
    label: "Sales Information Distinct Category"
    description: "Count of Distinct Category Sold"
    value_format: "#,##0"
    type: number
  }

  dimension: distinct_dept_zone {
    label: "Sales Information Distinct Dept Category"
    description: "Count of Distinct Dept Zone Sold"
    value_format: "#,##0"
    type: number
  }

  dimension: atr_cginvoiceno {
    label: "Sales Information Invoice Number"
  }

  dimension: quantity_sold {
    label: "Sales Information Quantity Sold"
    description: "Aggregate Quantity of Units Sold"
    value_format: "#,##0"
    type: number
  }

  dimension: net_sales_amount_local {
    label: "Sales Information Net Sales Amount Local"
    description: "Gross Sales - Returns - Discounts, use with Customer ID for LTV"
    value_format: "#,##0"
    type: number
  }

  measure: average_zone {
    label: "Average Number of Zones"
    type: average
    value_format_name: decimal_2
    sql:  ${distinct_zone};;
  }

  measure: average_dept_zone {
    label: "Average Number of Dept Zones"
    type: average
    value_format_name: decimal_2
    sql:  ${distinct_dept_zone};;
  }
}
