view: carolina_herrera_store_master {
  label: "Store Details"
  sql_table_name: `chb-prod-supplychain-data.prod_supply_chain.carolina_herrera_store_master`
    ;;

  dimension: ilion_store_code {
    type: string
    hidden: yes
    sql: ${TABLE}.ilion_store_code ;;
  }

  dimension: ilion_store_id {
    type: string
    hidden: yes
    sql: ${TABLE}.ilion_store_id ;;
  }

  dimension: rms_store_code {
    type: string
    hidden: yes
    sql: ${TABLE}.rms_store_code ;;
  }

  dimension: store_cd_name {
    type: string
    label: "Store Code and Name"
    hidden: no
    drill_fields: [carolina_herrera_category_mapping.category]
    sql: CONCAT(${TABLE}.ilion_store_code," - ",${TABLE}.store_name);;
  }

  dimension: store_closing_date {
    type: date
    label: "Store closing date"
    hidden: yes
    sql: ${TABLE}.store_closing_date ;;
  }

  dimension: store_name {
    type: string
    label: "Store Name"
    sql: ${TABLE}.store_name ;;
  }

  dimension: store_opening_date {
    type: date
    label: "Store opening date"
    sql: ${TABLE}.store_opening_date ;;
  }

  measure: count {
    type: count
    drill_fields: [store_name]
  }
}
