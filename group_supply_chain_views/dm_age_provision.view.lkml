view: dm_age_provision {
  sql_table_name: `chb-prod-supplychain-data.prod_supply_chain.age_provision`
    ;;

  dimension: last_date_2 {
    type: string
    hidden: yes
    sql: ${new_provision_last_date.last_date} ;;
  }

  dimension: is_last_day {
    type: yesno
    sql: ${last_date_2}=${date_date} ;;
  }

#   SPD-359 - Last Day of the Month

  dimension: is_last_day_of_month {
    hidden: yes
    type: yesno
    sql: EXTRACT( DAY FROM DATE_ADD(${date_raw}, INTERVAL 1 DAY)) = 1 ;;
  }

  #   SPD-359 - Last Day of the Month




#   SPD-328

  dimension: age_bucket {
    label: "Age Bucket"
    description: "Age in months grouped into Tiers"
    type: tier
    tiers: [6,12,18,24,36,48]
    value_format_name: decimal_0
    style: relational
    sql:ROUND(${age}/30);;
  }

  dimension: age_bucket_thedeal {
    label: "Age Bucket for The Deal"
    description: "Age in months grouped into Tiers for THE DEAL"
    type: tier
    tiers: [4,7,10]
    value_format_name: decimal_0
    style: relational
    sql:ROUND(${age}/30);;
  }

  dimension: age_bucket_level {
    label: "Age Bucket for Level"
    description: "Age in months grouped into Tiers for Level Shoes"
    type: tier
    tiers: [7,13,19,25,49]
    value_format_name: decimal_0
    style: relational
    sql:ROUND(${age}/30);;
  }

  #   SPD-328



# SPD - 386

  dimension_group: received_date {
    type: time
    label:"Received"
    timeframes: [
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date


    sql: DATE_ADD(${date_date}, INTERVAL -cast(${age} as int64) DAY)  ;;
  }

#######################################
#####          LOCS-DIMS          #####
#######################################

  dimension: boat{
    group_label: "Locations"
    description: "Boat is subvertical value from alternate hierarchy"
    type: string
    sql: ${TABLE}.dim_alternate_bu_hierarchy.sub_vertical  ;;
  }


  dimension: country_id{
    group_label: "Locations"
    description: "Country ID i.e. AE for United Arab Emirates"
    type: string
    suggest_explore: dim_retail_loc_country
    suggest_dimension: dim_retail_loc_country.country_id
    sql: ${TABLE}.dim_distr_ret_loc.country_id  ;;
  }

  dimension: country{
    group_label: "Locations"
    description: "Country value from Oracle i.e. United Arab Emirates"
    type: string
    suggest_explore: dim_retail_loc_country_desc
    suggest_dimension: dim_retail_loc_country_desc.country_desc
    sql: ${TABLE}.dim_distr_ret_loc.country_desc ;;
  }


  dimension: country_for_access_filter{
    group_label: "Locations"
    description: "Country value from Oracle i.e. United Arab Emirates"
    hidden: yes
    type: string
    suggest_explore: dim_retail_loc_country_desc
    suggest_dimension: dim_retail_loc_country_desc.country_desc
    sql: case when ${TABLE}.dim_alternate_bu_hierarchy.country  is null then 'x' else ${TABLE}.dim_alternate_bu_hierarchy.country  end ;;
  }


  dimension: vertical_for_access_filter{
    group_label: "Locations"
    label: "Vertical"
    description: "Vertical attribute in Oracle RMS"
    type: string
    hidden: yes

    suggest_explore: dim_retail_loc_vertical
    suggest_dimension: dim_retail_loc_vertical.vertical
    sql: case when ${vertical} is null then 'x' else ${vertical} end ;;
  }

  dimension: business_unit_for_access_filter{
    group_label: "Locations"
    description: "Business unit attribute in Oracle RMS"
    type: string
    hidden: yes
    suggest_explore: dim_retail_loc_bu
    suggest_dimension: dim_retail_loc_bu.business_unit
    sql: case when ${business_unit} is null then 'x' else ${business_unit} end ;;
  }





  dimension: store{
    group_label: "Locations"
    label: "Location ID"
    description: "Warehouse and Store IDs coming from Oracle RMS"
    type: number
    hidden: yes
    value_format: "0"
    suggest_explore: dim_retail_loc_store
    suggest_dimension: dim_retail_loc_store.location_id
    sql: ${TABLE}.dim_distr_ret_loc.country_desc   ;;
  }

# SPD-358 - changing reference to dim_distr_loc to show warehouse attributes as well
  dimension: district_name{
    group_label: "Locations"
    description: "District Name coming from Oracle RMS"
    type: string
    suggest_explore: dim_retail_loc_district_name
    suggest_dimension: dim_retail_loc_district_name.district_name
    sql: ${TABLE}.dim_distr_ret_loc.district_name   ;;
  }
# SPD-358 - changing reference to dim_distr_loc to show warehouse attributes as well

  dimension: region_name{
    group_label: "Locations"
    description: "Region Name coming from Oracle RMS"
    type: string

    sql: ${TABLE}.dim_distr_ret_loc.region_name   ;;
  }

  dimension: store_name{
    group_label: "Locations"
    label: "Location Name"
    description: "Name of location in Oracle RMS"
    type: string
    suggest_explore: dim_retail_loc_store_name
    suggest_dimension: dim_retail_loc_store_name.loc_name
    sql: ${TABLE}.dim_distr_ret_loc.loc_name   ;;
  }

# SPD-358 - changing reference to dim_distr_loc to show warehouse attributes as well
  dimension: country_code{
    group_label: "Locations"
    label: "Country Code"
    description: "Country code for location in Oracle RMS"
    type: string
    suggest_dimension: dim_retail_loc.country_code
    sql: ${TABLE}.dim_distr_ret_loc.country_desc  ;;
  }

  dimension: vertical{
    group_label: "Locations"
    label: "Vertical"
    description: "Vertical attribute in Oracle RMS"
    type: string
    suggest_explore: dim_retail_loc_vertical
    suggest_dimension: dim_retail_loc_vertical.vertical
    sql: ${TABLE}.dim_alternate_bu_hierarchy.vertical ;;
  }

  dimension: chain{
    group_label: "Locations"
    label: "Chain"
    description: "Chain name attribute in Oracle RMS"
    type: string
    sql: ${TABLE}.dim_distr_ret_loc.chain_name  ;;
  }

  dimension: department_id {
    group_label: "Locations"
    label: "Department No."
    description: "Department_number attribute in Oracle RMS"
    type: string
    sql: ${TABLE}.dim_retail_product.dept_no   ;;
  }

  dimension: department_name {
    group_label: "Locations"
    label: "Department "
    description: "Department name attribute in Oracle RMS"
    type: string
    sql: ${TABLE}.dim_retail_product ;;
  }


# SPD-358 - changing reference to dim_distr_loc to show warehouse attributes as well
  dimension: channel_name {
    type: string
    group_label: "Locations"
    label: "Channel"
    description: "Channel Name from RMS DPT STORE,FASHION OWN CONCEPT etc"
    hidden: no
    sql: ${TABLE}.dim_distr_ret_loc.channel_name  ;;
  }


  dimension: business_unit{
    group_label: "Locations"
    description: "Business unit attribute in Oracle RMS"
    type: string
    suggest_explore: dim_retail_loc_bu
    suggest_dimension: dim_retail_loc_bu.business_unit
    sql: ${TABLE}.dim_alternate_bu_hierarchy.brand  ;;
  }

  dimension: business_unit_code{
    group_label: "Locations"
    description: "Business unit code attribute in Oracle RMS"
    hidden: no
    label: "Business Unit Code"
    type: string
    sql: ${TABLE}.dim_alternate_bu_hierarchy.bu_code  ;;
  }

  dimension: bu_dep_code{
    group_label: "Locations"
    description: "BU Department Code from alternative hierarchy"
    hidden: no
    label: "Department Code"
    type: string
    sql: ${TABLE}.dim_alternate_bu_hierarchy.bu_dep_code  ;;
  }

  dimension: org_num {
    description: "Location number from Oracle RMS"
    hidden: no
    label: "Location Number"
    group_label: "Locations"
    suggest_explore: dim_retail_loc_store
    suggest_dimension: dim_retail_loc_store.store
    type: string
    sql: ${TABLE}.org_num ;;
  }


#######################################
#####         PROD DIMS           #####
#######################################

  dimension: age_months {
    type: number
    group_label: "Products"
    description: "Age in Months"
    sql:ROUND(${age}/30);;
  }
  dimension: item_style{
    group_label: "Products"
    label: "Product Style"
    description: "Style ID from Oracle RMS"
    type: string
    suggest_explore: fact_soh_sales_poc
    suggest_dimension: dim_retail_prod.item_style
    sql: ${TABLE}.dim_retail_product.item_style ;;
  }

  dimension: gwp_or_plv_item {
    description: "Yes if a GWP(Gift With Purchase) or PLV(Point of Sale Marketing Material)"
    type: yesno
    group_label: "Products"
    sql: ${TABLE}.dim_retail_product.division = "GWP" OR ${TABLE}.dim_retail_product.division = "PLV";;
  }

  dimension: class_name{
    group_label: "Products"
    description: "Class name attribute from Oracle RMS"
    label: "Class"
    type: string
    suggest_explore: dim_retail_prod_class
    suggest_dimension: class_name
    sql: ${TABLE}.dim_retail_product.class_name ;;
  }

  dimension: class_no{
    group_label: "Products"
    description: "Class id attribute from Oracle RMS"
    label: "Class ID"
    type: string
    sql: ${TABLE}.dim_retail_product.class ;;
  }

  dimension: subclass_name{
    group_label: "Products"
    label: "Sub Class"
    hidden: yes
    description: "Oracle RMS product subclass"
    type: string
    suggest_explore: dim_retail_prod_sub_class
    suggest_dimension: dim_retail_prod_sub_class.subclass_name
    sql: ${TABLE}.dim_retail_product.subclass_name ;;
  }

  dimension: taxo_class{
    group_label: "Products"
    label: "Taxonomy Class"
    description: "Taxonomy Class from Oracle RMS"
    type: string
    suggest_explore: dim_retail_prod_taxo_class
    suggest_dimension: dim_retail_prod_taxo_class.taxo_class
    sql: ${TABLE}.dim_retail_product.taxo_class ;;
  }

  dimension: item_desc{
    group_label: "Products"
    label: "Description"
    description: "Description from Oracle RMS"
    type: string
    suggest_dimension: dim_retail_prod.item_desc
    sql: ${TABLE}.dim_retail_product.item_desc ;;
  }

  dimension: brand{
    group_label: "Products"
    description: "Brand attribute from Oracle RMS"
    type: string
    hidden: yes
    suggest_explore: dim_retail_prod_brand
    suggest_dimension: dim_retail_prod_brand.brand
    sql: ${TABLE}.dim_retail_product.brand ;;
  }

  dimension: taxo_subclass{
    group_label: "Products"
    label: "Taxonomy Subclass"
    description: "Taxonomy subclass attribute from RMS"
    type: string
    hidden: yes
    suggest_explore: dim_retail_prod_taxo_subclass
    suggest_dimension: dim_retail_prod_taxo_subclass.taxo_subclass
    sql: ${TABLE}.dim_retail_product.taxo_subclass ;;
  }

  dimension: attrb_color{
    group_label: "Products"
    label: "Colour"
    description: "Colour attribute from Oracle RMS"
    type: string
    hidden: yes
    suggest_explore: dim_retail_prod_colour
    suggest_dimension: dim_retail_prod_colour.colour
    sql: ${TABLE}.dim_retail_product.attrb_color ;;
  }

  dimension: size_uda{
    group_label: "Products"
    description: "Size attribute from Oracle RMS"
    type: string
    hidden: yes
    label: "Size"
    suggest_explore: dim_retail_prod_size
    suggest_dimension: dim_retail_prod_size.size
    sql: ${TABLE}.dim_retail_product.size_uda;;
  }

  dimension: item_vpn{
    group_label: "Products"
    description: "Item VPN attribute from Oracle RMS"
    type: string
    hidden: yes
    label: "VPN"
    suggest_dimension: dim_retail_prod.item_vpn
    sql: ${TABLE}.dim_retail_product.vpn;;
  }

  dimension: gender{
    group_label: "Products"
    description: "Gender attribute from Oracle RMS"
    type: string
    hidden: yes
    suggest_explore: dim_retail_prod_gender
    suggest_dimension: dim_retail_prod_gender.gender
    sql: ${TABLE}.dim_retail_product.gender ;;
  }
  dimension: gender_brand_specific {
    group_label: "Products"
    description: "Gender with Kids, Newborn, Infant, Toddler, Layette under Kids"
    label: "Gender"
    hidden: yes
    type: string
    suggest_explore:  dim_retail_prod_gender
    suggest_dimension: dim_retail_prod_gender.gender
    sql: CASE WHEN ${gender}  LIKE "ANIMAL" THEN "ANIMAL" WHEN ${gender}  LIKE "MEN" THEN "MEN" WHEN ${gender}  LIKE "WOMEN" THEN "WOMEN" WHEN ${gender} LIKE "UNISEX" THEN "UNISEX" WHEN ${gender}  LIKE "NEUTRAL" THEN "NEUTRAL" WHEN ${gender}  LIKE "%KIDS%" or  ${gender} LIKE "%INFANT%"   or ${gender} LIKE "%TODDLER%" or  ${gender} LIKE "LAYETTE" or  ${gender} LIKE "NEWBORN" THEN "KIDS" WHEN ${gender} is Null  THEN "UNSPECIFIED" END;;
  }

  dimension: sep_category{
    group_label: "Products"
    type: string
    hidden: yes
    suggest_dimension: dim_retail_prod.gendersep_category
    sql: ${TABLE}.dim_retail_product.sep_category.SEP_CATEGORY  ;;
  }

  dimension: division{
    description: "Division attribute from Oracle RMS"
    group_label: "Products"
    type: string
    hidden: yes
    suggest_explore: dim_retail_prod_division
    suggest_dimension: dim_retail_prod_division.division
    sql: ${TABLE}.dim_retail_product.division  ;;
  }

  dimension: line{
    description: "Line attribute from Oracle RMS"
    group_label: "Products"
    type: string
    hidden: yes
    sql: ${TABLE}.dim_retail_product.line  ;;
  }

  dimension: sub_line{
    description: "Line attribute from Oracle RMS"
    group_label: "Products"
    type: string
    hidden: yes
    sql: ${TABLE}.dim_retail_product.sub_line  ;;
  }
  dimension: Category_basedon_division {
    group_label: "Products"
    description: "Product categories for Lacoste"
    label: "Category"
    hidden: yes
    type: string
    sql: CASE WHEN ${division}="APPAREL" AND  ${sub_line} LIKE "%Basic%" Then "BASIC"
              WHEN ${division}="APPAREL" AND  ${sub_line} LIKE "%BASIC%" Then "BASIC"
              WHEN ${division}="APPAREL" AND  ${sub_line} LIKE "%FASHION%" Then "FASHION"
              WHEN ${division}="APPAREL" Then "APPAREL"
              WHEN ${division}="APPAREL COMPLEMENTS" AND  ${sub_line} LIKE "%RC%" Then "BELTS"
              WHEN ${division}= "APPAREL COMPLEMENTS" THEN "ACCESSORIES"
              WHEN ${division}= "BATH & BEACHWEAR" Then "APPAREL"
              WHEN ${division}= "EYE WEAR" Then "EYE WEAR"
              WHEN ${division}= "FOOTWEAR" Then "FOOTWEAR"
              WHEN ${division}= "FRAGRANCES" Then "FRAGRANCES"
              WHEN ${division}= "GWP" Then "GWP"
              WHEN ${division}= "WATCHES" Then "WATCHES"
      WHEN ${division}= "SERVICE ITEMS" Then "SERVICE ITEMS"
      WHEN ${division}= "PLV" Then "PLV"
      WHEN ${division}= "LEATHER GOODS & LUGGAGE" AND  ${sub_line} LIKE "%BAG%" Then "BAGS"
      WHEN ${division}= "LEATHER GOODS & LUGGAGE" AND  ${sub_line} LIKE "%SLG%" Then "SLG"
      WHEN ${division}= "LEATHER GOODS & LUGGAGE" Then "LEATHER GOODS & LUGGAGE"
      WHEN ${division}= "MEDICARE" Then "MEDICARE"
      WHEN ${division}= "INTIMATE" Then "INTIMATE"
      WHEN ${division}= "CHARITY" Then "CHARITY" END;;
  }

  dimension: season_desc{
    group_label: "Products"
    description: "Season attribute from Oracle RMS"
    label: "Season"
    hidden: yes
    type: string
    suggest_explore: dim_retail_prod_season
    suggest_dimension: dim_retail_prod_season.season
    sql: ${TABLE}.dim_retail_product.season_desc   ;;
  }

  dimension: made_of {
    group_label: "Products"
    label: "Made of"
    hidden: yes
    type: string
    sql: ${TABLE}.dim_retail_product.attrb_made_of  ;;
  }

  dimension: prod_num {
    description: "Product number from Oracle RMS"
    hidden: no
    type: string
    label: "Product ID"

    suggest_dimension: dim_retail_prod.item
    group_label: "Products"
    sql: ${TABLE}.prod_num ;;
  }


  dimension: entity_type {
    description: "Entity Type specifies whether the product is Common,Fashion, Beauty , Gift etc"

    type: string
    hidden: yes
    group_label: "Products"
    sql: ${TABLE}.dim_retail_product.entity_type    ;;
  }

  dimension: age {
    description: "Age in Days"
    type: number
    sql: ${TABLE}.age ;;
  }

  dimension: conversion_rate {
    type: number
    hidden: yes
    sql: ${TABLE}.conversion_rate ;;
  }

# SPD-405 Displaying Currency COde
  dimension: currency_code {
    type: string
    hidden: no
    sql: ${TABLE}.currency_code ;;
  }

  dimension_group: date {
    type: time
    label:"Provision"
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.date ;;
  }
  measure: last_updated_date {
    type: date
    label: "Last refresh date"
    sql: MAX(${date_raw}) ;;
    convert_tz: no
  }


  dimension: dim_alternate_bu_hierarchy {
    hidden: yes
    sql: ${TABLE}.dim_alternate_bu_hierarchy ;;
  }

  dimension: dim_distr_ret_loc {
    hidden: yes
    sql: ${TABLE}.dim_distr_ret_loc ;;
  }

  dimension: dim_retail_loc {
    hidden: yes
    sql: ${TABLE}.dim_retail_loc ;;
  }

  dimension: dim_retail_product {
    hidden: yes
    sql: ${TABLE}.dim_retail_product ;;
  }


  dimension: status {
    description: "Item Status from Oracle RMS"
    type: string
    sql: ${TABLE}.status ;;
  }

  dimension: stock_type {
    description: "Stock Type(Ecom/WH/Store)"
    hidden: no
    label: "Stock Type(Ecom/WH/Store)"
    group_label: "Locations"
    type: string
    sql: CASE WHEN ${TABLE}.dim_distr_ret_loc.loc_name LIKE "%ECOMMERCE%" THEN "Ecom" WHEN BYTE_LENGTH(${org_num})>6 THEN "Warehouse" ELSE "Store" END ;;
  }

  dimension: is_ecommerce {
    description: "Is it an ecomm sale?"
    hidden: no
    label: "Is Ecommerce?"
    group_label: "Locations"
    type: yesno
    sql: ${TABLE}.dim_distr_ret_loc.loc_name LIKE "%ECOMMERCE%" ;;
  }

  dimension: store_name_swk {
    view_label: "Locations"
    label: "Location Name(Swarovski BU)"
    description: "Location Name combined for Ecom Stock and Sales location for Swarovski"
    type: string
    suggest_explore: dim_retail_loc_store_name
    suggest_dimension: dim_retail_loc_store_name.store_name
    sql: CASE WHEN ${TABLE}.dim_distr_ret_loc.loc_name LIKE '%ECOMMERCE%' THEN CONCAT(UPPER(${TABLE}.dim_alternate_bu_hierarchy.brand)," - ",UPPER(${TABLE}.dim_distr_ret_loc.country_desc)," - ","ECOMMERCE")
      ELSE ${TABLE}.dim_distr_ret_loc.loc_name END;;
  }

  dimension: prov_running_qty {
    type: number
    label: "Provisions Running Quantity"
    description: "Stock on Hand"
    sql: ${TABLE}.prov_running_qty ;;
  }

  dimension: soh_av_cost_amt_lcl {
    type: number
    label: "WAC Local"
    description: "Weighted Average Cost (WAC) Local"
    hidden: no
    sql: ${TABLE}.soh_av_cost_amt_lcl ;;
  }


  dimension: soh_av_cost_amt_usd {
    type: number
    label: "WAC USD"
    description: "Weighted Average Cost (WAC) USD"
    hidden: no
    sql: ${TABLE}.soh_av_cost_amt_usd ;;
  }


  dimension: provision_factor {
    description: "Depreciation Factor per month"
    type: number
    hidden: no
    sql: (CASE WHEN ${entity_type} = "EXPIRY (FASHION)" THEN 0.05
        WHEN ${entity_type} = "FASHION" THEN 0.05
        WHEN ${entity_type} = "BEAUTY" THEN 0.025
        WHEN ${entity_type} = "COMMON" THEN 1
        WHEN ${entity_type} = "GIFT" THEN 0.025
        ELSE 0
        END);;
  }

  dimension: provisions_local_amount {
    type: number
    hidden: yes
    sql:CASE WHEN ${age_months} <= 6 AND ${entity_type} != "COMMON" THEN 0
          WHEN ${age_months} > 6 AND ${age_months}  <= 26 AND (${entity_type} = "FASHION" OR ${entity_type} = "EXPIRY (FASHION)" )
          THEN ${soh_av_cost_amt_lcl}*${prov_running_qty}*${provision_factor}*(${age_months}-6)
          WHEN ${age_months} > 26 AND (${entity_type} = "FASHION" OR ${entity_type} = "EXPIRY (FASHION)" )
          THEN ${soh_av_cost_amt_lcl}*${prov_running_qty}
          WHEN ${age_months} > 6 AND ${age_months}  <= 46 AND (${entity_type} = "BEAUTY" OR ${entity_type} = "GIFT" )
          THEN
          ${soh_av_cost_amt_lcl}*${prov_running_qty}*${provision_factor}*(${age_months}-6)
          WHEN ${age_months} > 46 AND (${entity_type} = "BEAUTY" OR ${entity_type} = "GIFT" )
          THEN ${soh_av_cost_amt_lcl}*${prov_running_qty}
          WHEN ${entity_type} = "COMMON"
          THEN ${soh_av_cost_amt_lcl}*${prov_running_qty}
          ELSE 0
          END;;
  }

  dimension: provisions_monthly_local_amount {
    type: number
    hidden: yes
    sql:CASE WHEN ${age_months} <= 6 AND ${entity_type} != "COMMON" THEN 0
          WHEN ${age_months} > 6 AND ${age_months}  <= 26 AND (${entity_type} = "FASHION" OR ${entity_type} = "EXPIRY (FASHION)" )
          THEN ${soh_av_cost_amt_lcl}*${prov_running_qty}*${provision_factor}
          WHEN ${age_months} > 6 AND ${age_months}  <= 46 AND (${entity_type} = "BEAUTY" OR ${entity_type} = "GIFT" )
          THEN
          ${soh_av_cost_amt_lcl}*${prov_running_qty}*${provision_factor}
          WHEN ${entity_type} = "COMMON" AND ${age_months} = 0
          THEN ${soh_av_cost_amt_lcl}*${prov_running_qty}
          ELSE 0
          END;;
  }

  dimension: provisions_usd_amount {
    type: number
    hidden: yes
    sql:CASE WHEN ${age_months} <= 6 AND ${entity_type} != "COMMON" THEN 0
          WHEN ${age_months} > 6 AND ${age_months}  <= 26 AND (${entity_type} = "FASHION" OR ${entity_type} = "EXPIRY (FASHION)" )
          THEN ${soh_av_cost_amt_usd}*${prov_running_qty}*${provision_factor}*(${age_months}-6)
          WHEN ${age_months} > 26 AND (${entity_type} = "FASHION" OR ${entity_type} = "EXPIRY (FASHION)" )
          THEN ${soh_av_cost_amt_usd}*${prov_running_qty}
          WHEN ${age_months} > 6 AND ${age_months}  <= 46 AND (${entity_type} = "BEAUTY" OR ${entity_type} = "GIFT" )
          THEN
          ${soh_av_cost_amt_usd}*${prov_running_qty}*${provision_factor}*(${age_months}-6)
          WHEN ${age_months} > 46 AND (${entity_type} = "BEAUTY" OR ${entity_type} = "GIFT" )
          THEN ${soh_av_cost_amt_usd}*${prov_running_qty}
          WHEN ${entity_type} = "COMMON"
          THEN ${soh_av_cost_amt_usd}*${prov_running_qty}
          ELSE 0
          END;;
  }

  dimension: provisions_monthly_usd_amount {
    type: number
    hidden: yes
    sql:CASE WHEN ${age_months} <= 6 AND ${entity_type} != "COMMON" THEN 0
          WHEN ${age_months} > 6 AND ${age_months}  <= 26 AND (${entity_type} = "FASHION" OR ${entity_type} = "EXPIRY (FASHION)" )
          THEN ${soh_av_cost_amt_usd}*${prov_running_qty}*${provision_factor}
          WHEN ${age_months} > 6 AND ${age_months}  <= 46 AND (${entity_type} = "BEAUTY" OR ${entity_type} = "GIFT" )
          THEN
          ${soh_av_cost_amt_usd}*${prov_running_qty}*${provision_factor}
          WHEN ${entity_type} = "COMMON" AND ${age_months} = 0
          THEN ${soh_av_cost_amt_usd}*${prov_running_qty}
          ELSE 0
          END;;
  }

#-------------SPD-420 Item_Loc_traits from age_provision table starts---------------

  dimension: consignment_flag
  {
    type: string
    hidden: yes

    sql: ${TABLE}.dim_item_loc_traits.food_stamp_ind ;;
  }

  dimension: consignment
  {
    type: yesno
    view_label: "Location Traits"
    label: "Is Consignment?"
    sql: ${consignment_flag} = "Y" ;;
  }

  dimension: alt_storage_location {
    type: string
    hidden: no
    view_label: "Location Traits"
    label: "Season (Location)"
    description: "Season value specific for the location"
    sql: UPPER(${TABLE}.dim_item_loc_traits.alt_storage_location) ;;
  }

  dimension: elect_mtk_clubs {
    type: string
    view_label: "Location Traits"
    label: "Tryano Area Description"
    sql: ${TABLE}.dim_item_loc_traits.elect_mtk_clubs ;;
  }

  dimension: alt_storeage_location_bucket {
    type: string
    hidden: no
    view_label: "Location Traits"
    label: "Level Season Bucket"
    description: "Season bucket based on location trait"
    sql: CASE
          WHEN  REPLACE(${alt_storage_location}, ' ', '') IN ("SS21P", "SS21M", "SS21B") THEN "SS21"
          WHEN  REPLACE(${alt_storage_location}, ' ', '') IN ("FW21P", "FW21M", "FW21B") THEN "FW21"
          WHEN  REPLACE(${alt_storage_location}, ' ', '') IN ("SS22P", "SS22M", "SS22B") THEN "SS22"
          WHEN  REPLACE(${alt_storage_location}, ' ', '') IN ("FW22P", "FW22M", "FW22B") THEN "FW22"
          WHEN  ${alt_storage_location} LIKE "BASIC" THEN "Basic"
          WHEN  ${alt_storage_location} LIKE "REGULAR" THEN "Regular"
          WHEN  ${alt_storage_location} LIKE "%C" OR  ${alt_storage_location}  LIKE "%CO" THEN "Carry-over"
          WHEN  ${alt_storage_location} IS NULL THEN "No Season Info"
          ELSE "Previous Seasons"
         END;;
  }

  dimension: alt_storeage_location_bucket_2 {
    type: string
    hidden: no
    view_label: "Location Traits"
    label: "Level Season Bucket 2"
    description: "Season bucket based on location trait"
    sql: CASE WHEN  ${alt_storage_location} LIKE "BASIC" THEN "Basic"
          ELSE "Seasonal"
          END;;
  }

  dimension: report_code {
    type: string
    hidden: no
    view_label: "Location Traits"
    label: "Zone"
    description: "Report Code value"
    sql: ${TABLE}.dim_item_loc_traits.report_code ;;
  }

  dimension: zone_3 {
    type: string
    hidden: no
    view_label: "Location Traits"
    label: "Zone 3"
    description: "Zone 3 for Level shoes"
    sql: CASE WHEN ${report_code} LIKE "%ACC" Then "Accesories"
          WHEN ${report_code}="GIAROS" AND ${gender}="WOMEN" THEN "Direct Concession"
          ELSE "Multibrand"
          END;;
  }

  dimension: zone_category_sort {
    type: string
    hidden: yes
    view_label: "Location Traits"
    label: "Zone Category Sort"
    description: "Zone Category Sort for Level shoes"
    sql: CASE WHEN ${report_code} in ("CONTMP", "DESIGM", "DESIGW", "KIDS", "MENACC", "TRENDM", "TRENDW", "WMNACC", "KDACC") Then "1"
              WHEN ${report_code} in ("AXEARI", "CREPRO", "GIAROS", "SOLLOU", "STRFAR", "TORBUR", "VERSAC", "DOLCE") Then "2"
              WHEN ${report_code} in ("INTCON", "EXTCON") Then "3"
          ELSE "4"
          END;;
  }

  dimension: zone_category {
    type: string
    hidden: no
    view_label: "Location Traits"
    label: "Zone Category"
    order_by_field: zone_category_sort
    description: "Zone Category for Level shoes"
    sql: CASE WHEN ${report_code} in ("CONTMP", "DESIGM", "DESIGW", "KIDS", "MENACC", "TRENDM", "TRENDW", "WMNACC", "KDACC") Then "Multibrand"
              WHEN ${report_code} in ("AXEARI", "CREPRO", "GIAROS", "SOLLOU", "STRFAR", "TORBUR", "VERSAC", "DOLCE") Then "Shop in Shop"
              WHEN ${report_code} in ("INTCON", "EXTCON") Then "Concessions"
          ELSE null
          END;;
  }


#-------------SPD-420 Item_Loc_traits from age_provision table ends---------------


#-------------Accumulated Provision starts ----------------
  measure: sum_provisions_local_amount {
    type: sum
    description: "Cumulative Provision Amount in Local Currency"
    group_label: "Provision"

    label: "Provisions Amount Local"
    value_format_name: decimal_2
    sql:${provisions_local_amount};;
  }

  measure: provisions_amount_usd {
    group_label: "Provision"

    type: sum
    label: "Provisions Amount USD"
    description: "Cumulative Provision Amount in USD"
    value_format_name: usd
    sql:${provisions_usd_amount};;
  }
#-------------Accumulated Provision ends ----------------



#-------------Monthly Provision starts ----------------
  measure: sum_provisions_monthly_local_amount {
    type: sum
    group_label: "Provision"

    label: "Provisions Monthly Amount Local"
    description: "Monthly Provision Amount in Local Currency"
    value_format_name: decimal_2
    sql:${provisions_monthly_local_amount};;
  }

  measure: provisions_monthly_amount_usd {
    group_label: "Provision"
    label: "Provisions Monthly Amount USD"
    description: "Monthly Provision Amount in USD"

    type: sum
    value_format_name: usd
    sql:${provisions_monthly_usd_amount};;
  }
#-------------Monthly Provision ends ----------------


  measure: last_date {
    hidden: yes
    type: date
    sql:MAX(${date_date});;
  }


# SPD-359 changes Month end closing stock  starts..

#-------------Running Quantity Starts ----------------
  measure: month_closing_running_quantity {
    type: sum
    hidden: yes
    value_format_name: decimal_0
    label: "Month Closing Running Quantity"
    description: "Stock on Hand as of end of Month"

#     filters: {
#       field: is_last_day
#       value: "yes"
#     }

    filters: {
      field: is_last_day_of_month
      value: "yes"
    }

    sql: ${prov_running_qty};;
  }

  measure: aged_stock_month_closing_running_quantity {
    type: sum
    hidden: yes
    value_format_name: decimal_0
    label: "Aged Stock Month Closing Running Quantity"
    description: "Aged Stock on Hand (>=13 Months) as of end of Month"

    filters: {
      field: is_last_day_of_month
      value: "yes"
    }

    sql: case when ${age_months}>=13 then ${prov_running_qty} else 0 end;;
  }

  measure: last_day_running_quantity {
    type: sum
    hidden: yes
    value_format_name: decimal_0
    label: "Last Day Running Quantity"
    description: "Stock on Hand as of the day"

#     filters: {
#       field: is_last_day
#       value: "yes"
#     }

    filters: {
      field: is_last_day
      value: "yes"
    }

    sql: ${prov_running_qty};;
  }

  measure: aged_stock_last_day_running_quantity {
    type: sum
    hidden: yes
    value_format_name: decimal_0
    label: "Aged Stock Last Day Running Quantity"
    description: "Aged Stock on Hand (>=13 Months) as of the day"

    filters: {
      field: is_last_day
      value: "yes"
    }

    sql: case when ${age_months}>=13 then ${prov_running_qty} else 0 end;;
  }


#-------------Running Quantity ends ----------------



#   measure: last_day_provisions_monthly_local_amount {
#     type: sum
#     label: "Month Closing Provisions Monthly Amount Local"
#     description: "Cumulative Provision Amount as of end of Month in Local"
#
#     hidden: no
#     value_format_name: decimal_0
#     #     filters: {
# #       field: is_last_day
# #       value: "yes"
# #     }
#
#     filters: {
#       field: last_day_of_month
#       value: "yes"
#     }
#     sql: ${provisions_monthly_local_amount};;
#   }
#
#
#
#   measure: last_day_provisions_monthly_local_amount {
#     type: sum
#     label: "Month Closing Provisions Monthly Amount Local"
#     description: "Cumulative Provision Amount as of end of Month in Local"
#
#     hidden: no
#     value_format_name: decimal_0
#     #     filters: {
# #       field: is_last_day
# #       value: "yes"
# #     }
#
#     filters: {
#       field: last_day_of_month
#       value: "yes"
#     }
#     sql: ${provisions_monthly_local_amount};;
#   }

#   measure: last_day_provisions_monthly_usd {
#     type: sum
#     label: "Month Closing Provisions Monthly Amount USD"
#     description: "Cumulative Provision Amount as of end of Month in USD"
#     hidden: no
#     value_format_name: usd
#     #     filters: {
# #       field: is_last_day
# #       value: "yes"
# #     }
#
#     filters: {
#       field: last_day_of_month
#       value: "yes"
#     }
#     sql: ${provisions_monthly_local_amount}/${conversion_rate};;
#   }


#-------------Stock Value Starts ----------------

  measure: last_day_stock_value_local {
    type: sum
    label:  "Month Closing Stock Value Local"
    description: "Monthly CLosing Stock Value in Local Currency"

    hidden: yes


    filters: {
      field: is_last_day
      value: "yes"
    }
    sql: ${prov_running_qty}*${soh_av_cost_amt_lcl};;
  }

  measure: last_day_aged_stock_value_local {
    type: sum
    label:  "Month Closing Aged Stock Value Local"
    description: "Monthly Closing Aged Stock (>=13 Months) Value in Local Currency"

    hidden: yes


    filters: {
      field: is_last_day
      value: "yes"
    }
    sql: CASE WHEN ${age_months}>=13 THEN ${prov_running_qty}*${soh_av_cost_amt_lcl} else 0 END;;
  }

  measure: last_day_stock_value_usd {
    type: sum
    label: "Month Closing Stock Value USD"
    description: "Monthly CLosing Stock Value in USD"

    hidden: yes
        filters: {
      field: is_last_day
      value: "yes"
    }


    sql: ${prov_running_qty}*${soh_av_cost_amt_usd};;
  }

  measure: last_day_aged_stock_value_usd {
    type: sum
    label: "Month Closing Aged Stock Value USD"
    description: "Monthly Closing Aged Stock (>=13 Months) Value in USD"

    hidden: yes
    filters: {
      field: is_last_day
      value: "yes"
    }


    sql: CASE WHEN ${age_months}>=13 THEN ${prov_running_qty}*${soh_av_cost_amt_usd} else 0 END;;
  }


  measure: month_closing_stock_value_local {
    type: sum
    label:  "Month Closing Stock Value Local"
    description: "Monthly CLosing Stock Value in Local Currency"

    hidden: yes


    filters: {
      field: is_last_day_of_month
      value: "yes"
    }
    sql: ${prov_running_qty}*${soh_av_cost_amt_lcl};;
  }

  measure: month_closing_aged_stock_value_local {
    type: sum
    label:  "Month Closing Aged Stock Value Local"
    description: "Monthly Closing Stock (>=13) Value in Local Currency"

    hidden: yes


    filters: {
      field: is_last_day_of_month
      value: "yes"
    }
    sql: CASE WHEN ${age_months}>=13 THEN ${prov_running_qty}*${soh_av_cost_amt_lcl} else 0 END;;
  }

  measure: month_closing_stock_value_usd {
    type: sum
    label: "Month Closing Stock Value USD"
    description: "Monthly CLosing Stock Value in USD"

    hidden: yes
    filters: {
      field: is_last_day_of_month
      value: "yes"
    }


    sql: ${prov_running_qty}*${soh_av_cost_amt_usd};;
  }

  measure: month_closing_aged_stock_value_usd {
    type: sum
    label: "Month Closing Aged Stock Value USD"
    description: "Monthly Closing Aged Stock (>13 Months) Value in USD"

    hidden: yes
    filters: {
      field: is_last_day_of_month
      value: "yes"
    }


    sql: CASE WHEN ${age_months}>=13 THEN ${prov_running_qty}*${soh_av_cost_amt_usd} else 0 END;;
  }




  #-------------Stock Value ends ----------------



  # SPD-359

  parameter: param_prov_month_daily {
    label: "Calculate on"
    view_label: "Parameters"
    type: string


    default_value: "Month End"
    allowed_value: {
      label: "End of Month"
      value: "monthend"
    }
    allowed_value: {
      label: "End of Day"
      value: "daywise"
    }
  }



  measure: running_quantity_from_param{

    type: number
    sql:
    {% if param_prov_month_daily._parameter_value == "'monthend'" %}
     ${month_closing_running_quantity}
    {% else %}
    ${last_day_running_quantity}
    {% endif %}
    ;;
    label: "Running Quantity"
    description: "Closing Stock Qty based on End of Month or End of Day "
    group_label: "Closing Stock"

  }

  measure: aged_stock_running_quantity_from_param{

    type: number
    sql:
    {% if param_prov_month_daily._parameter_value == "'monthend'" %}
     ${aged_stock_month_closing_running_quantity}
    {% else %}
    ${aged_stock_last_day_running_quantity}
    {% endif %}
    ;;
    label: "Aged Stock Running Quantity"
    description: "Closing Aged Stock Qty (>=13 Months) based on End of Month or End of Day "
    group_label: "Closing Stock"

  }

  measure: stock_value_local_from_param{

    type: number
    sql:
    {% if param_prov_month_daily._parameter_value ==  "'monthend'" %}
    ${month_closing_stock_value_local}

    {% else %}
    ${last_day_stock_value_local}
    {% endif %}
    ;;
    label: "Closing Stock Value Local"
    value_format_name: decimal_2
    description: "Closing Stock Value based on End of Month or End of Day "

    group_label: "Closing Stock"

  }

  measure: aged_stock_value_local_from_param{

    type: number
    sql:
    {% if param_prov_month_daily._parameter_value ==  "'monthend'" %}
    ${month_closing_aged_stock_value_local}

    {% else %}
    ${last_day_aged_stock_value_local}
    {% endif %}
    ;;
    label: "Closing Aged Stock Value Local"
    value_format_name: decimal_2
    description: "Closing Aged Stock (>=13 Months) Value based on End of Month or End of Day "

    group_label: "Closing Stock"

  }


  measure: stock_value_usd_from_param{

    type: number
    sql:
    {% if param_prov_month_daily._parameter_value ==  "'monthend'" %}
    ${month_closing_stock_value_usd}
    {% else %}
    ${last_day_stock_value_usd}
    {% endif %}
    ;;
    value_format_name: usd

    description: "Closing Stock Value based on End of Month or End of Day "
    label: "Closing Stock Value USD"
    group_label: "Closing Stock"
  }

  measure: aged_value_usd_from_param{

    type: number
    sql:
    {% if param_prov_month_daily._parameter_value ==  "'monthend'" %}
    ${month_closing_aged_stock_value_usd}
    {% else %}
    ${last_day_aged_stock_value_usd}
    {% endif %}
    ;;
    value_format_name: usd

    description: "Closing Aged Stock (>=13 Months) Value based on End of Month or End of Day "
    label: "Closing Aged Stock Value USD"
    group_label: "Closing Stock"
  }


  # SPD-359



  measure: count {
    type: count
    drill_fields: []
  }
}

view: age_provision__dim_alternate_bu_hierarchy {
  dimension: brand {
    type: string
    sql: ${TABLE}.brand ;;
  }

  dimension: bu_code {
    type: string
    sql: ${TABLE}.bu_code ;;
  }

  dimension: bu_dep_code {
    type: string
    sql: ${TABLE}.bu_dep_code ;;
  }

  dimension: bu_dep_desc {
    type: string
    sql: ${TABLE}.bu_dep_desc ;;
  }

  dimension: bu_desc {
    type: string
    sql: ${TABLE}.bu_desc ;;
  }

  dimension: country {
    type: string
    map_layer_name: countries
    sql: ${TABLE}.country ;;
  }

  dimension: dim_bu_cdl_org_num {
    type: string
    sql: ${TABLE}.dim_bu_cdl_org_num ;;
  }

  dimension: dim_bu_cdl_prod_num {
    type: string
    sql: ${TABLE}.dim_bu_cdl_prod_num ;;
  }

  dimension: ownership {
    type: string
    sql: ${TABLE}.ownership ;;
  }

  dimension: store_code {
    type: string
    sql: ${TABLE}.store_code ;;
  }

  dimension: sub_vertical {
    type: string
    sql: ${TABLE}.sub_vertical ;;
  }

  dimension: vertical {
    type: string
    sql: ${TABLE}.vertical ;;
  }
}

view: age_provision__dim_distr_ret_loc {
  dimension: add_1 {
    type: string
    sql: ${TABLE}.add_1 ;;
  }

  dimension: add_2 {
    type: string
    sql: ${TABLE}.add_2 ;;
  }

  dimension: addr_key {
    type: number
    sql: ${TABLE}.addr_key ;;
  }

  dimension: addr_type {
    type: string
    sql: ${TABLE}.addr_type ;;
  }

  dimension: area {
    type: number
    sql: ${TABLE}.area ;;
  }

  dimension: area_name {
    type: string
    sql: ${TABLE}.area_name ;;
  }

  dimension: chain {
    type: number
    sql: ${TABLE}.chain ;;
  }

  dimension: chain_name {
    type: string
    sql: ${TABLE}.chain_name ;;
  }

  dimension: channel_id {
    type: number
    sql: ${TABLE}.channel_id ;;
  }

  dimension: channel_name {
    type: string
    sql: ${TABLE}.channel_name ;;
  }

  dimension: channel_type {
    type: string
    sql: ${TABLE}.channel_type ;;
  }

  dimension: city {
    type: string
    sql: ${TABLE}.city ;;
  }

  dimension: country_desc {
    type: string
    sql: ${TABLE}.country_desc ;;
  }

  dimension: country_id {
    type: string
    sql: ${TABLE}.country_id ;;
  }

  dimension: currency_code {
    type: string
    sql: ${TABLE}.currency_code ;;
  }

  dimension: description {
    type: string
    sql: ${TABLE}.description ;;
  }

  dimension: district {
    type: number
    sql: ${TABLE}.district ;;
  }

  dimension: district_name {
    type: string
    sql: ${TABLE}.district_name ;;
  }

  dimension: key_value_1 {
    type: string
    sql: ${TABLE}.key_value_1 ;;
  }

  dimension: key_value_2 {
    type: string
    sql: ${TABLE}.key_value_2 ;;
  }

  dimension: loc_code {
    type: number
    sql: ${TABLE}.loc_code ;;
  }

  dimension: loc_name {
    type: string
    sql: ${TABLE}.loc_name ;;
  }

  dimension: mall_name {
    type: string
    sql: ${TABLE}.mall_name ;;
  }

  dimension: module {
    type: string
    sql: ${TABLE}.module ;;
  }

  dimension: physical_wh {
    type: number
    sql: ${TABLE}.physical_wh ;;
  }

  dimension: post {
    type: string
    sql: ${TABLE}.post ;;
  }

  dimension: primary_addr_ind {
    type: string
    sql: ${TABLE}.primary_addr_ind ;;
  }

  dimension: primary_vwh {
    type: number
    sql: ${TABLE}.primary_vwh ;;
  }

  dimension: region {
    type: number
    sql: ${TABLE}.region ;;
  }

  dimension: region_name {
    type: string
    sql: ${TABLE}.region_name ;;
  }

  dimension: selling_square_ft {
    type: number
    sql: ${TABLE}.selling_square_ft ;;
  }

  dimension: state {
    type: string
    sql: ${TABLE}.state ;;
  }

  dimension_group: store_close {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.store_close_date ;;
  }

  dimension: store_format {
    type: number
    sql: ${TABLE}.store_format ;;
  }

  dimension: store_name10 {
    type: string
    sql: ${TABLE}.store_name10 ;;
  }

  dimension: store_name3 {
    type: string
    sql: ${TABLE}.store_name3 ;;
  }

  dimension_group: store_open {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.store_open_date ;;
  }

  dimension: total_square_ft {
    type: number
    sql: ${TABLE}.total_square_ft ;;
  }

  dimension: vat_region {
    type: number
    sql: ${TABLE}.vat_region ;;
  }

  dimension: wh_name_secondary {
    type: string
    sql: ${TABLE}.wh_name_secondary ;;
  }
}

view: age_provision__dim_retail_product {
  dimension: activity {
    type: string
    sql: ${TABLE}.activity ;;
  }

  dimension: age_of_skin {
    type: string
    sql: ${TABLE}.age_of_skin ;;
  }

  dimension: alcohol_content_gross {
    type: string
    sql: ${TABLE}.alcohol_content_gross ;;
  }

  dimension: alcohol_content_net {
    type: string
    sql: ${TABLE}.alcohol_content_net ;;
  }

  dimension: arabic_desc {
    type: string
    sql: ${TABLE}.arabic_desc ;;
  }

  dimension: atrb_boy_girl {
    type: string
    sql: ${TABLE}.atrb_boy_girl ;;
  }

  dimension: attrb_color {
    type: string
    sql: ${TABLE}.attrb_color ;;
  }

  dimension: attrb_made_of {
    type: string
    sql: ${TABLE}.attrb_made_of ;;
  }

  dimension: attrb_theme {
    type: string
    sql: ${TABLE}.attrb_theme ;;
  }

  dimension: barcode {
    type: string
    sql: ${TABLE}.barcode ;;
  }

  dimension: batch_ind {
    type: string
    sql: ${TABLE}.batch_ind ;;
  }

  dimension: brand {
    type: string
    sql: ${TABLE}.brand ;;
  }

  dimension: class {
    type: number
    sql: ${TABLE}.class ;;
  }

  dimension: class_name {
    type: string
    sql: ${TABLE}.class_name ;;
  }

  dimension: country_of_manu {
    type: string
    sql: ${TABLE}.country_of_manu ;;
  }

  dimension: dept_name {
    type: string
    sql: ${TABLE}.dept_name ;;
  }

  dimension: dept_no {
    type: number
    sql: ${TABLE}.dept_no ;;
  }

  dimension: dgr_ind {
    type: string
    sql: ${TABLE}.dgr_ind ;;
  }

  dimension: diff_1 {
    type: string
    sql: ${TABLE}.diff_1 ;;
  }

  dimension: diff_2 {
    type: string
    sql: ${TABLE}.diff_2 ;;
  }

  dimension: division {
    type: string
    sql: ${TABLE}.division ;;
  }

  dimension: division_no {
    type: number
    sql: ${TABLE}.division_no ;;
  }

  dimension: entity_type {
    type: string
    sql: ${TABLE}.entity_type ;;
  }

  dimension: format {
    type: string
    sql: ${TABLE}.format ;;
  }

  dimension: gender {
    type: string
    sql: ${TABLE}.gender ;;
  }

  dimension: grey_mkt_ind {
    type: string
    sql: ${TABLE}.grey_mkt_ind ;;
  }

  dimension: group_name {
    type: string
    sql: ${TABLE}.group_name ;;
  }

  dimension: group_no {
    type: number
    sql: ${TABLE}.group_no ;;
  }

  dimension: item {
    type: string
    sql: ${TABLE}.item ;;
  }

  dimension: item_desc {
    type: string
    sql: ${TABLE}.item_desc ;;
  }

  dimension: item_style {
    type: string
    sql: ${TABLE}.item_style ;;
  }

  dimension: line {
    type: string
    sql: ${TABLE}.line ;;
  }

  dimension: perishable_ind {
    type: string
    sql: ${TABLE}.perishable_ind ;;
  }

  dimension: recurrence {
    type: string
    sql: ${TABLE}.recurrence ;;
  }

  dimension: sap_item_code {
    type: string
    sql: ${TABLE}.sap_item_code ;;
  }

  dimension: saso_ind {
    type: string
    sql: ${TABLE}.saso_ind ;;
  }

  dimension: season_desc {
    type: string
    sql: ${TABLE}.season_desc ;;
  }

  dimension: sep_axis {
    type: string
    sql: ${TABLE}.sep_axis ;;
  }

  dimension: sep_category {
    hidden: yes
    sql: ${TABLE}.sep_category ;;
  }

  dimension: sep_market {
    type: string
    sql: ${TABLE}.sep_market ;;
  }

  dimension: sep_nature {
    type: string
    sql: ${TABLE}.sep_nature ;;
  }

  dimension: sep_range {
    type: string
    sql: ${TABLE}.sep_range ;;
  }

  dimension: size_uda {
    type: string
    sql: ${TABLE}.size_uda ;;
  }

  dimension: standard_uom {
    type: string
    sql: ${TABLE}.standard_uom ;;
  }

  dimension: sub_line {
    type: string
    sql: ${TABLE}.sub_line ;;
  }

  dimension: subclass {
    type: number
    sql: ${TABLE}.subclass ;;
  }

  dimension: subclass_name {
    type: string
    sql: ${TABLE}.subclass_name ;;
  }

  dimension: sup_class {
    type: string
    sql: ${TABLE}.sup_class ;;
  }

  dimension: sup_subclass {
    type: string
    sql: ${TABLE}.sup_subclass ;;
  }

  dimension: taxo_class {
    type: string
    sql: ${TABLE}.taxo_class ;;
  }

  dimension: taxo_subclass {
    type: string
    sql: ${TABLE}.taxo_subclass ;;
  }

  dimension: uc_code {
    type: string
    sql: ${TABLE}.uc_code ;;
  }

  dimension: uda_16_attribute {
    type: string
    sql: ${TABLE}.uda_16_attribute ;;
  }

  dimension: uda_zone {
    type: string
    sql: ${TABLE}.uda_zone ;;
  }

  dimension: usage_specificity {
    type: string
    sql: ${TABLE}.usage_specificity ;;
  }

  dimension: vpn {
    type: string
    sql: ${TABLE}.vpn ;;
  }
}

view: age_provision__dim_retail_product__sep_category {
  dimension: item {
    type: string
    sql: ${TABLE}.ITEM ;;
  }

  dimension: sep_category {
    type: string
    sql: ${TABLE}.SEP_CATEGORY ;;
  }

  dimension: uda_value {
    type: number
    sql: ${TABLE}.UDA_VALUE ;;
  }
}

view: age_provision__dim_retail_loc {
  dimension: add_1 {
    type: string
    sql: ${TABLE}.add_1 ;;
  }

  dimension: add_2 {
    type: string
    sql: ${TABLE}.add_2 ;;
  }

  dimension: addr_key {
    type: number
    sql: ${TABLE}.addr_key ;;
  }

  dimension: addr_type {
    type: string
    sql: ${TABLE}.addr_type ;;
  }

  dimension: area {
    type: number
    sql: ${TABLE}.area ;;
  }

  dimension: area_name {
    type: string
    sql: ${TABLE}.area_name ;;
  }

  dimension: chain {
    type: number
    sql: ${TABLE}.chain ;;
  }

  dimension: chain_name {
    type: string
    sql: ${TABLE}.chain_name ;;
  }

  dimension: channel_id {
    type: number
    sql: ${TABLE}.channel_id ;;
  }

  dimension: channel_name {
    type: string
    sql: ${TABLE}.channel_name ;;
  }

  dimension: channel_type {
    type: string
    sql: ${TABLE}.channel_type ;;
  }

  dimension: city {
    type: string
    sql: ${TABLE}.city ;;
  }

  dimension: country_desc {
    type: string
    sql: ${TABLE}.country_desc ;;
  }

  dimension: country_id {
    type: string
    sql: ${TABLE}.country_id ;;
  }

  dimension: currency_code {
    type: string
    sql: ${TABLE}.currency_code ;;
  }

  dimension: description {
    type: string
    sql: ${TABLE}.description ;;
  }

  dimension: district {
    type: number
    sql: ${TABLE}.district ;;
  }

  dimension: district_name {
    type: string
    sql: ${TABLE}.district_name ;;
  }

  dimension: key_value_1 {
    type: string
    sql: ${TABLE}.key_value_1 ;;
  }

  dimension: key_value_2 {
    type: string
    sql: ${TABLE}.key_value_2 ;;
  }

  dimension: mall_name {
    type: string
    sql: ${TABLE}.mall_name ;;
  }

  dimension: module {
    type: string
    sql: ${TABLE}.module ;;
  }

  dimension: post {
    type: string
    sql: ${TABLE}.post ;;
  }

  dimension: primary_addr_ind {
    type: string
    sql: ${TABLE}.primary_addr_ind ;;
  }

  dimension: region {
    type: number
    sql: ${TABLE}.region ;;
  }

  dimension: region_name {
    type: string
    sql: ${TABLE}.region_name ;;
  }

  dimension: selling_square_ft {
    type: number
    sql: ${TABLE}.selling_square_ft ;;
  }

  dimension: state {
    type: string
    sql: ${TABLE}.state ;;
  }

  dimension: store {
    type: number
    sql: ${TABLE}.store ;;
  }

  dimension_group: store_close {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.store_close_date ;;
  }

  dimension: store_format {
    type: number
    sql: ${TABLE}.store_format ;;
  }

  dimension: store_name {
    type: string
    sql: ${TABLE}.store_name ;;
  }

  dimension: store_name10 {
    type: string
    sql: ${TABLE}.store_name10 ;;
  }

  dimension: store_name3 {
    type: string
    sql: ${TABLE}.store_name3 ;;
  }

  dimension_group: store_open {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.store_open_date ;;
  }

  dimension: total_square_ft {
    type: number
    sql: ${TABLE}.total_square_ft ;;
  }

  dimension: vat_region {
    type: number
    sql: ${TABLE}.vat_region ;;
  }
}
