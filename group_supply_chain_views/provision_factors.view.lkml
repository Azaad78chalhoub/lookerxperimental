view: provision_factors {
  derived_table: {
  persist_for: "24 hours"
    sql:
SELECT
  item,
  store,
  entity_type,
  LAST_VALUE(unit_cost_usd) OVER (PARTITION BY stock_date ORDER BY stock_Date DESC) AS unit_cost_usd
FROM
  `chb-prod-supplychain-data.prod_supply_chain.dm_soh_sales`
WHERE
  stock_date = "2019-02-01";;
  }


  dimension: pk {
    primary_key: yes
    hidden: yes
    sql: CONCAT(${item}, "-",${store}) ;;
  }

  dimension: unit_cost_usd {
    type: number
    hidden: no
    sql: ${TABLE}.unit_cost_usd ;;
  }

  dimension: store {
    type: string
    hidden: no
    sql: ${TABLE}.store ;;
  }


  dimension: item {
    type: string
    hidden: no
    sql: ${TABLE}.item ;;
  }

  dimension: entity_type {
    type: string
    hidden: no
    sql: ${TABLE}.entity_type ;;
  }




}
