view: dm_soh_sales_last_day {
  label: "Sales and Stock Information"
  derived_table: {
    explore_source: fact_soh_sales_poc {
      column: stock_date {}
      column: org_num {}
      column: prod_num {}
      column: unit_cost_usd {}
      column: unit_cost {}
      column: inv_soh_qty {}
      column: sales_qty {}
      derived_column: running_sales_qty {
        sql: SUM(sales_qty) OVER (PARTITION BY org_num,prod_num ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW);;
      }

      bind_all_filters: yes
    }
  }

  dimension: pk {
    primary_key: yes
    hidden: yes
    sql: CONCAT(${stock_date}, " - ", ${org_num}, " - ", ${prod_num}) ;;
  }

  dimension: stock_date {
    type: date
    hidden: yes
  }


  dimension: is_last_date {
    type: yesno
    hidden: no
    label: "Is Last date test"
    sql: ${stock_date}=${dm_sohsales_max_min_dates.max_date} ;;
  }

  dimension: org_num {
    type: string
    hidden: yes
  }

  dimension: prod_num {
    type: string
    hidden: yes
  }


  dimension: inv_soh_qty {
    type: number
    hidden: yes
  }

  dimension: unit_cost {
    type: number
    hidden: yes
  }

  dimension: sales_qty {
    type: number
    hidden: yes
  }

  dimension: unit_cost_usd {
    type: number
    hidden: yes
  }

  dimension: running_sales_qty {
    type: number
    hidden: no
  }



  measure: sell_through_rate_on_stock_new {
    type: number
    label: "Sell Through Rate on Stock % NEW"
    value_format_name: percent_2
    sql:SAFE_DIVIDE(SUM(${sales_qty}),(SUM(${sales_qty})+SUM(CASE WHEN ${stock_date}=${dm_sohsales_max_min_dates.max_date} THEN ${inv_soh_qty} ELSE NULL END)));;
  }
    # sql:IFNULL(SAFE_DIVIDE(SUM(${sales_qty})/(SUM(${sales_qty}),SUM(CASE WHEN ${stock_date}=${dm_sohsales_max_min_dates.max_date} THEN ${inv_soh_qty} ELSE NULL END))),0);;


  measure: last_day_stock_quantity_new {
    type: sum
    hidden: no
    group_label: "Stock Metrics NEW"
    label: "Last Day Closing SOH Quantity New"
    value_format_name: decimal_0
    sql: CASE WHEN ${stock_date}=${dm_sohsales_max_min_dates.max_date} THEN ${inv_soh_qty} ELSE NULL END;;
  }

  measure: last_day_stock_value_2 {
    type: sum
    hidden: no
    group_label: "Stock Metrics NEW"
    label: "Last Day Closing SOH Value New"
    value_format_name: decimal_0
    sql: CASE WHEN ${stock_date}=${dm_sohsales_max_min_dates.max_date} THEN ${inv_soh_qty}*${unit_cost} ELSE NULL END;;
  }

  measure: last_day_stock_value_usd_2 {
    type: sum
    hidden: no
    group_label: "Stock Metrics NEW"
    label: "Last Day Closing SOH Value USD New"
    value_format_name: usd
    sql: CASE WHEN ${stock_date}=${dm_sohsales_max_min_dates.max_date} THEN ${inv_soh_qty}*${unit_cost_usd} ELSE NULL END;;
  }

}
