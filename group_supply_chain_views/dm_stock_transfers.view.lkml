view: dm_stock_transfers {
  sql_table_name: `chb-prod-supplychain-data.prod_supply_chain.dm_stock_transfers`
    ;;

  dimension_group: approval {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.approval_date AS TIMESTAMP) ;;
  }

  dimension: approval_id {
    type: string
    sql: ${TABLE}.approval_id ;;
  }

  measure: last_updated_date {
    type: date
    label: "Last refresh date"
    sql: MAX(${create_raw}) ;;
    convert_tz: no
  }

  dimension: cancelled_qty {
    type: number
    hidden: yes
    sql: ${TABLE}.cancelled_qty ;;
  }

  dimension: ch_av_cost {
    type: number
    hidden: yes
    sql: ${TABLE}.ch_av_cost ;;
  }

  dimension: ch_customer {
    type: number
    sql: ${TABLE}.ch_customer ;;
  }

  dimension_group: close {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.close_date AS TIMESTAMP) ;;
  }

  dimension: comment_desc {
    type: string
    sql: ${TABLE}.comment_desc ;;
  }

  dimension: context_type {
    type: string
    sql: ${TABLE}.context_type ;;
  }

  dimension: context_value {
    type: string
    sql: ${TABLE}.context_value ;;
  }

  dimension_group: create {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.create_date AS TIMESTAMP) ;;
  }

  dimension: create_id {
    type: string
    sql: ${TABLE}.create_id ;;
  }

  dimension: default_chrgs_2_leg_ind {
    type: string
    hidden: yes
    sql: ${TABLE}.default_chrgs_2_leg_ind ;;
  }

  dimension_group: delivery {
    type: time
    label: "Delivery date"
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.delivery_date AS TIMESTAMP) ;;
  }

  dimension: delivery_slot_id {
    type: string
    hidden: yes
    sql: ${TABLE}.delivery_slot_id ;;
  }

  dimension: dept {
    type: number
    hidden: yes
    sql: ${TABLE}.dept ;;
  }

  dimension: distro_qty {
    type: number
    hidden: yes
    sql: ${TABLE}.distro_qty ;;
  }
  dimension: stock_on_hand_from_loc {
    label: "Stock on hand in source location"
    type: number
    hidden: yes
    sql: ${TABLE}.stock_on_hand_from_loc ;;
  }
  dimension: stock_on_hand_to_loc {
    label: "Stock on hand in destination location"
    type: number
    hidden: yes
    sql: ${TABLE}.stock_on_hand_to_loc ;;
  }
  dimension: unit_cost_usd_from_loc {
    label: "Unit Cost in source location"
    type: number
    hidden: yes
    sql: ${TABLE}.unit_cost_usd_from_loc  ;;
  }
  dimension: unit_cost_usd_to_loc {
    label: "Unit Cost in destination location"
    type: number
    hidden: yes
    sql: ${TABLE}.unit_cost_usd_to_loc  ;;
  }

  dimension_group: exp_dc {
    type: time
    hidden: yes
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.exp_dc_date ;;
  }

  dimension_group: exp_dc_eow {
    type: time

    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.exp_dc_eow_date ;;
  }

  dimension: ext_ref_no {
    type: string
    hidden: yes
    sql: ${TABLE}.ext_ref_no ;;
  }

  dimension: fill_qty {
    type: number
    hidden: yes
    sql: ${TABLE}.fill_qty ;;
  }

  dimension: finisher_av_retail {
    type: number
    hidden: yes
    sql: ${TABLE}.finisher_av_retail ;;
  }

  dimension: finisher_units {
    type: number
    hidden: yes
    sql: ${TABLE}.finisher_units ;;
  }

  dimension: freight_code {
    type: string
    hidden: yes
    sql: ${TABLE}.freight_code ;;
  }

  dimension: from_loc {
    type: number
    group_label: "Locations"
    label: "From Location ID"
    sql: ${TABLE}.from_loc ;;
    value_format: "0"
  }

  dimension: from_loc_type {
    type: string
    group_label: "Locations"
    label: "From Location Type"
    sql: ${TABLE}.from_loc_type ;;
  }

  dimension: from_loc_name {
    type: string
    group_label: "Locations"
    label: "From Location Name"
    sql: ${TABLE}.from_loc_name;;
  }

  dimension: to_loc_name {
    type: string
    group_label: "Locations"
    label: "To Location Name"
    sql: ${TABLE}.to_loc_name;;
  }

  dimension: inv_status {
    type: number
    hidden: yes
    sql: ${TABLE}.inv_status ;;
  }

  dimension: inventory_type {
    type: string
    sql: ${TABLE}.inventory_type ;;
  }

  dimension: item {
    type: string
    group_label: "Products"
    label: "Product ID"
    sql: ${TABLE}.item ;;
  }

  dimension: mbr_processed_ind {
    type: string
    hidden: yes
    sql: ${TABLE}.mbr_processed_ind ;;
  }

  dimension: mrt_no {
    type: number
    hidden: yes
    sql: ${TABLE}.mrt_no ;;
  }

  dimension_group: not_after {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    hidden: yes
    datatype: date
    sql: ${TABLE}.not_after_date ;;
  }

  dimension: order_no {
    type: number
    sql: ${TABLE}.order_no ;;
  }

  dimension: publish_ind {
    hidden: yes
    type: string
    sql: ${TABLE}.publish_ind ;;
  }

  dimension: received_qty {
    type: number
    hidden: yes
    sql: ${TABLE}.received_qty ;;
  }

  dimension: reconciled_qty {
    type: number
    hidden: yes
    sql: ${TABLE}.reconciled_qty ;;
  }

  dimension: repl_tsf_approve_ind {
    hidden: yes
    type: string
    sql: ${TABLE}.repl_tsf_approve_ind ;;
  }

  dimension: restock_pct {
    type: number
    hidden: yes
    sql: ${TABLE}.restock_pct ;;
  }

  dimension: routing_code {
    type: string
    hidden: yes
    sql: ${TABLE}.routing_code ;;
  }

  dimension: selected_qty {
    type: number
    hidden: yes
    sql: ${TABLE}.selected_qty ;;
  }

  dimension: ship_qty {
    type: number
    hidden: yes
    sql: ${TABLE}.ship_qty ;;
  }

  dimension: status {
    type: string
    sql: ${TABLE}.status ;;
  }

  dimension: supp_pack_size {
    type: number
    hidden: yes
    sql: ${TABLE}.supp_pack_size ;;
  }

  dimension: to_loc {
    type: number
    group_label: "Locations"
    label: "To Location ID"
    sql: ${TABLE}.to_loc ;;
    value_format: "0"
  }

  dimension: to_loc_type {
    type: string
    group_label: "Locations"
    label: "To Location Type"
    sql: ${TABLE}.to_loc_type ;;
  }

  dimension: tsf_cost {
    type: number
    sql: ${TABLE}.tsf_cost ;;
  }

  measure: tsf_cost_m {
    type: sum
    label: "Total Transfer cost"
    sql: ${tsf_cost} ;;
  }


  dimension: tsf_no {
    type: number
    label: "Transfer Number"
    sql: ${TABLE}.tsf_no ;;
    value_format: "0"
  }
  measure: tsf_no_count {
    type: count_distinct
    label: "Distinct Transfer Numbers"
    sql: ${tsf_no} ;;
    value_format: "0"
  }
  dimension: tsf_parent_no {
    type: number
    hidden: yes
    sql: ${TABLE}.tsf_parent_no ;;
  }

  dimension: tsf_po_link_no {
    hidden: yes
    type: number
    sql: ${TABLE}.tsf_po_link_no ;;
  }

  dimension: tsf_price {
    hidden: no
    type: number
    sql: ${TABLE}.tsf_price ;;
  }

  dimension: tsf_qty {
    type: number
    hidden: no
    sql: ${TABLE}.tsf_qty ;;
  }

  dimension: tsf_seq_no {
    type: number
    sql: ${TABLE}.tsf_seq_no ;;
  }

  dimension: tsf_type {
    type: string
    sql: ${TABLE}.tsf_type ;;
  }

  dimension: updated_by_rms_ind {
    type: string
    sql: ${TABLE}.updated_by_rms_ind ;;
  }

  dimension_group: wf_need {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.wf_need_date AS TIMESTAMP) ;;
  }

  measure: count {
    type: count
    hidden: yes
    drill_fields: []
  }

  measure: ship_qty_sum {
    type: sum
    group_label: "Quantity metrics"
    label: "Shipped Quantity"
    sql:${ship_qty} ;;
  }

  measure: cancelled_qty_sum {
    type: sum
    group_label: "Quantity metrics"
    label: "Cancelled Quantity"
    sql:${cancelled_qty} ;;
  }

  measure: tsf_qty_sum {
    type: sum
    group_label: "Quantity metrics"
    label: "Transferred Quantity"
    sql: ${tsf_qty} ;;
  }

  measure: received_qty_sum {
    type: sum
    group_label: "Quantity metrics"
    label: "Received Quantity"
    sql: ${received_qty} ;;
  }

  dimension: discrepancy_qty {
    type: number
    hidden: yes
    label: "Discrepancy Quantity"
    sql: ${ship_qty}-${received_qty} ;;
  }
  measure: discrepancy_qty_sum {
    type: sum
    group_label: "Quantity metrics"
    label: "Discrepancy Quantity"
    sql: ${discrepancy_qty} ;;
  }

  measure: distro_qty_sum {
    type: sum
    group_label: "Quantity metrics"
    label: "Distro Quantity"
    sql:${distro_qty} ;;
  }

  measure: fill_qty_sum {
    type: sum
    group_label: "Quantity metrics"
    label: "Fill Quantity"
    sql:${fill_qty} ;;
  }

  measure: reconciled_qty_sum {
    type: sum
    group_label: "Quantity metrics"
    label: "Reconciled Quantity"
    sql:${reconciled_qty} ;;
  }

  measure: stock_on_hand_from_loc_sum {
    label: "SOH in Source"
    description: "Stock on hand in source location"
    type: sum
    hidden: yes
    sql: ${stock_on_hand_from_loc} ;;
  }
  measure: stock_on_hand_to_sum {
    label: "SOH in Destination"
    description: "Stock on hand in destination location"
    type: sum
    hidden: yes
    sql: ${stock_on_hand_to_loc} ;;
  }
  measure: unit_cost_usd_from_loc_sum {
    label: "Unit Cost in Source"
    description: "Unit Cost in source location"
    type: sum
    hidden: yes
    sql: ${unit_cost_usd_from_loc};;
    value_format: "$0.00"
  }
  measure: unit_cost_usd_to_loc_sum {
    label: "Unit Cost in Destination"
    description: "Unit Cost in destination location"
    type: sum
    hidden: yes
    sql: ${unit_cost_usd_to_loc}  ;;
    value_format: "$0.00"
  }

  measure: is_received_in_full {
    type: yesno
    hidden: yes
    sql: (${tsf_qty_sum}-${cancelled_qty_sum})-${received_qty_sum}=0 ;;
  }
  dimension: from_chain_name {
    type: string
    group_label: "Locations"
    label: "From Chain Name"
    sql: ${TABLE}.from_chain_name ;;
  }
  dimension: from_area_name {
    type: string
    group_label: "Locations"
    label: "From Area Name"
    sql: ${TABLE}.from_area_name ;;
  }
  dimension: from_region_name {
    type: string
    group_label: "Locations"
    label: "From Region Name"
    sql: ${TABLE}.from_region_name ;;
  }
  dimension: from_district_name {
    type: string
    group_label: "Locations"
    label: "From District Name"
    sql: ${TABLE}.from_district_name ;;
  }
  dimension: from_channel_name {
    type: string
    group_label: "Locations"
    label: "From Channel Name"
    sql: ${TABLE}.from_channel_name ;;
  }
  dimension: from_currency_code {
    type: string
    group_label: "Locations"
    label: "From Currency code"
    sql: ${TABLE}.from_currency_code ;;
  }
  dimension: from_city {
    type: string
    group_label: "Locations"
    label: "From City Name"
    sql: ${TABLE}.from_city;;
  }
  dimension: from_state {
    type: number
    hidden: yes
    sql: ${TABLE}.from_state;;
  }
  dimension: from_country_id {
    type: string
    group_label: "Locations"
    label: "From Country ID"
    sql: ${TABLE}.from_country_id;;
  }
  dimension: from_country_desc {
    type: string
    group_label: "Locations"
    label: "From Country Name"
    sql: ${TABLE}.from_country_desc;;
  }
  dimension:  to_chain_name {
    type: string
    group_label: "Locations"
    label: "To Chain Name"
    sql: ${TABLE}.to_chain_name ;;
  }
  dimension: to_area_name {
    type: string
    group_label: "Locations"
    label: "To Area Name"
    sql: ${TABLE}.to_area_name ;;
  }
  dimension: to_region_name {
    type: string
    group_label: "Locations"
    label: "To Region Name"
    sql: ${TABLE}.to_region_name ;;
  }
  dimension: to_district_name {
    type: string
    group_label: "Locations"
    label: "To District Name"
    sql: ${TABLE}.to_district_name ;;
  }
  dimension: to_channel_name {
    type: string
    group_label: "Locations"
    label: "To Channel Name"
    sql: ${TABLE}.to_channel_name ;;
  }
  dimension: to_currency_code {
    type: string
    group_label: "Locations"
    label: "To Currency Code"
    sql: ${TABLE}.to_currency_code ;;
  }
  dimension: to_city {
    type: string
    group_label: "Locations"
    label: "To City name"
    sql: ${TABLE}.to_city;;
  }
  dimension: to_state {
    type: string
    group_label: "Locations"
    label: "To State Name"
    sql: ${TABLE}.to_state;;
  }
  dimension: to_country_id {
    group_label: "Locations"
    label: "To Country ID"
    sql: ${TABLE}.to_country_id;;
  }
  dimension: to_country_desc {
    group_label: "Locations"
    label: "To Country Name"
    sql: ${TABLE}.to_country_desc;;
  }
  dimension: from_business_unit {
    group_label: "Locations"
    label: "From Business Unit"
    sql: ${TABLE}.from_business_unit;;
  }
  dimension: to_business_unit {
    group_label: "Locations"
    label: "To Business Unit"
    sql: ${TABLE}.to_business_unit;;
  }
  dimension: from_vertical {
    group_label: "Locations"
    label: "From Vertical"
    sql: case when ${from_business_unit}=${dim_alternate_bu_hierarchy.brand} then ${dim_alternate_bu_hierarchy.vertical} end;;
  }
  dimension: to_vertical {
    group_label: "Locations"
    label: "To Vertical"
    sql: case when ${to_business_unit}=${dim_alternate_bu_hierarchy.brand} then ${dim_alternate_bu_hierarchy.vertical} end;;
  }
  dimension: from_boat {
    group_label: "Locations"
    label: "From Boat"
    sql: case when ${from_business_unit}=${dim_alternate_bu_hierarchy.brand} then ${dim_alternate_bu_hierarchy.sub_vertical} end;;
  }
  dimension: to_boat {
    group_label: "Locations"
    label: "To Boat"
    sql: case when ${to_business_unit}=${dim_alternate_bu_hierarchy.brand} then ${dim_alternate_bu_hierarchy.sub_vertical} end;;
  }
  dimension: item_desc {
    type: string
    group_label: "Products"
    label: "Item Description"
    sql: ${TABLE}.item_desc;;
  }
  dimension: vpn {
    type: string
    group_label: "Products"
    sql: ${TABLE}.vpn;;
  }
  dimension: division {
    type: string
    group_label: "Products"
    sql: ${TABLE}.division;;
  }
  dimension: dept_name {
    type: string
    group_label: "Products"
    sql: ${TABLE}.dept_name;;
  }
  dimension: class_name {
    type: string
    group_label: "Products"
    sql: ${TABLE}.class_name;;
  }
  dimension: subclass_name {
    type: string
    group_label: "Products"
    sql: ${TABLE}.subclass_name;;
  }
  dimension: taxo_class {
    type: string
    group_label: "Products"
    sql: ${TABLE}.taxo_class;;
  }
  dimension: taxo_subclass {
    type: string
    group_label: "Products"
    sql: ${TABLE}.taxo_subclass;;
  }
  dimension: gender {
    type: string
    group_label: "Products"
    sql: ${TABLE}.gender;;
  }
  dimension: line {
    type: string
    group_label: "Products"
    sql: ${TABLE}.line;;
  }
  dimension: sub_line {
    type: string
    group_label: "Products"
    sql: ${TABLE}.sub_line;;
  }
  dimension: pk {
    primary_key: yes
    type: string
    hidden:yes
    sql: CONCAT(${tsf_no}, ' - ' ,${tsf_seq_no}, ' - ', ${item}) ;;
  }
  measure: cancelled_val {
    type: sum
    group_label: "Value metrics USD"
    label: "Cancelled Value USD"
    sql:${cancelled_qty}* ${cost_facts.unit_cost_usd} ;;
    value_format: "$0.00"
  }

  measure: received_val {
    type: sum
    group_label: "Value metrics USD"
    label: "Received Value USD"
    sql: ${received_qty} * ${cost_facts.unit_cost_usd} ;;
    value_format: "$0.00"
  }
  measure: transferred_val {
    type: sum
    group_label: "Value metrics USD"
    label: "Transferred Value USD"
    sql: ${tsf_qty} * ${cost_facts.unit_cost_usd} ;;
    value_format: "$0.00"
  }
  measure: shipped_val {
    type: sum
    group_label: "Value metrics USD"
    label: "Shipped Value USD"
    sql: ${ship_qty} * ${cost_facts.unit_cost_usd} ;;
    value_format: "$0.00"
  }
  measure: discrepancy_val {
    type: sum
    group_label: "Value metrics USD"
    label: "Discrepancy Value USD"
    sql: ${discrepancy_qty} * ${cost_facts.unit_cost_usd} ;;
    value_format: "$0.00"
  }
  measure: cancelled_val_lc {
    type: sum
    group_label: "Value metrics Local Currency"
    label: "Cancelled Value (Local Currency)"
    sql:${cancelled_qty}* ${cost_facts.unit_cost} ;;
    value_format: "#,##0.00"
  }

  measure: received_val_lc {
    type: sum
    group_label: "Value metrics Local Currency"
    label: "Received Value (Local Currency)"
    sql: ${received_qty} * ${cost_facts.unit_cost} ;;
    value_format: "#,##0.00"
  }
  measure: transferred_val_lc {
    type: sum
    group_label: "Value metrics Local Currency"
    label: "Transferred Value (Local Currency)"
    sql: ${tsf_qty} * ${cost_facts.unit_cost} ;;
    value_format: "#,##0.00"
  }
  measure: shipped_val_lc {
    type: sum
    group_label: "Value metrics Local Currency"
    label: "Shipped Value (Local Currency)"
    sql: ${ship_qty} * ${cost_facts.unit_cost} ;;
    value_format: "#,##0.00"
  }
  measure: discrepancy_val_lc {
    type: sum
    group_label: "Value metrics Local Currency"
    label: "Discrepancy Value (Local Currency)"
    sql: ${discrepancy_qty} * ${cost_facts.unit_cost} ;;
    value_format: "#,##0.00"
  }
}
