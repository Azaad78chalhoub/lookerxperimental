view: gmroi {
  derived_table: {
    explore_source: fact_soh_sales_poc {
      column: stock_date_from_parameter {}
      column: granularity_from_parameter {}
      column: granularity_from_parameter_b {}
      column: gross_margin {}
      column: total_stock_value {}
      column: month_opening_stock_value {}
      column: month_closing_stock_value {}
      column: quarter_closing_stock_value {}
      column: year_closing_stock_value {}
      bind_all_filters: yes

    }
  }

  dimension: pk {
    primary_key: yes
    hidden: yes
    type: string
    sql: CONCAT(${granularity_from_parameter}, ' - ', ${granularity_from_parameter_b}, ' - ',${stock_date_from_parameter}) ;;
  }

  dimension: granularity_from_parameter {hidden: yes}
  dimension: granularity_from_parameter_b {hidden: yes}
  dimension: stock_date_from_parameter {
    hidden: no
    type:string
    view_label: "-- GMROI"
    }


  dimension: month_opening_stock_value {
    hidden: yes
    type: number
  }
  dimension: month_closing_stock_value {
    hidden: yes
    value_format_name: decimal_2
    type: number
  }
  dimension: quarter_closing_stock_value {
    hidden: yes
    value_format_name: decimal_2
    type: number
  }
  dimension: year_closing_stock_value {
    hidden: yes
    value_format_name: decimal_2
    type: number
  }

  dimension: gross_margin {
    hidden: yes
    value_format_name: decimal_2
    type: number
  }


  dimension: gmroi_parameter {
    hidden: yes
    value_format_name: decimal_0
    type: number
  }

  dimension: timeframe_parameter_value {
    hidden: yes
    value_format_name: decimal_0
    type: string
  }

  measure: avg_gmroi_parameter {
    hidden: yes
    type: average
    sql: ${gmroi_parameter} ;;
  }

  dimension: total_stock_value {
    type: number
    hidden: yes
    value_format_name: usd
  }

  measure: month_opening_stock_value_sum {
    hidden: yes
    type: sum
    sql: ${month_opening_stock_value} ;;
  }

  measure: month_closing_stock_value_sum {
    hidden: yes
    type: sum
    sql: ${month_closing_stock_value} ;;
  }

  measure: quarter_closing_stock_value_sum {
    hidden: yes
    type: sum
    sql: ${quarter_closing_stock_value} ;;
  }

  measure: year_closing_stock_value_sum {
    hidden: yes
    type: sum
    sql: ${year_closing_stock_value} ;;
  }

  measure: average_month_inventory_Value {
    hidden: yes
    type: sum
    sql: ( ${month_opening_stock_value}+${month_closing_stock_value})/2 ;;
  }

  measure: average_quarter_inventory_value {
    hidden: yes
    type: sum
    sql: (${month_opening_stock_value}+${quarter_closing_stock_value})/4 ;;
  }

  measure: average_yearly_inventory_value {
    hidden: yes
    type: sum
    sql: (${month_opening_stock_value}+${year_closing_stock_value})/13 ;;
  }

  measure: gross_margin_sum {
    hidden: yes
    type: sum
    value_format_name: usd
    sql: ${gross_margin} ;;
  }

#   measure: daily_stock_value {
#     value_format_name: usd
#     type: sum
#     hidden: yes
#     sql:  SAFE_DIVIDE(${total_stock_value},${count_of_unique_dates}) ;;
#   }
  measure: gmroi_month {
    value_format_name: decimal_2
    type: sum
    hidden: yes
    sql:SAFE_DIVIDE(${gross_margin},((${month_opening_stock_value}+${month_closing_stock_value})/2));;
  }
  measure: gmroi_quarter {
    value_format_name: decimal_2
    type: sum
    hidden: yes
    sql:SAFE_DIVIDE(${gross_margin},((${month_opening_stock_value}+${quarter_closing_stock_value})/4));;
  }

  measure: gmroi_year {
    value_format_name: decimal_2
    type: sum
    hidden: yes
    sql:SAFE_DIVIDE(${gross_margin},((${month_opening_stock_value}+${year_closing_stock_value})/13));;
  }

  measure: dynamic_gmroi {
    value_format_name: decimal_2
    type: number
    label: "GMROI"
    view_label: "-- GMROI"
    hidden: no
    sql:
    {% if fact_soh_sales_poc.timeframe_parameter._parameter_value == 'month' %}
    SAFE_DIVIDE(SUM(${gross_margin}),((SUM(${month_opening_stock_value})+SUM(${month_closing_stock_value}))/2))
    {% elsif fact_soh_sales_poc.timeframe_parameter._parameter_value == 'quarter' %}
    SAFE_DIVIDE(SUM(${gross_margin}),((SUM(${month_opening_stock_value})+SUM(${quarter_closing_stock_value}))/4))
    {% elsif fact_soh_sales_poc.timeframe_parameter._parameter_value == 'year' %}
    SAFE_DIVIDE(SUM(${gross_margin}),((SUM(${month_opening_stock_value})+SUM(${year_closing_stock_value}))/13))
    {% else %}
    SAFE_DIVIDE(SUM(${gross_margin}),((SUM(${month_opening_stock_value})+SUM(${month_closing_stock_value}))/2))
    {% endif %};;
  }

  measure: annualized_dynamic_gmroi {
    value_format_name: decimal_2
    type: number
    label: "Annualized GMROI"
    view_label: "-- GMROI"
    hidden: no
    sql:
    {% if fact_soh_sales_poc.timeframe_parameter._parameter_value == 'month' %}
    SAFE_DIVIDE(12*SUM(${gross_margin}),((SUM(${month_opening_stock_value})+SUM(${month_closing_stock_value}))/2))
    {% elsif fact_soh_sales_poc.timeframe_parameter._parameter_value == 'quarter' %}
    SAFE_DIVIDE(4*SUM(${gross_margin}),((SUM(${month_opening_stock_value})+SUM(${quarter_closing_stock_value}))/4))
    {% elsif fact_soh_sales_poc.timeframe_parameter._parameter_value == 'year' %}
    SAFE_DIVIDE(SUM(${gross_margin}),((SUM(${month_opening_stock_value})+SUM(${year_closing_stock_value}))/13))
    {% else %}
    SAFE_DIVIDE(SUM(${gross_margin}),((SUM(${month_opening_stock_value})+SUM(${month_closing_stock_value}))/2))
    {% endif %};;
  }

#   measure: gmroi_new {
#     type: number
#     label: "GMROI"
#     view_label: "-- Stock Cover and GMROI BETA"
#     value_format_name: decimal_2
#
#     sql: SAFE_DIVIDE(${gross_margin},SAFE_DIVIDE(${total_stock_value},${count_of_unique_dates})) ;;
#   }
#
#   measure: gmroi_new_v2 {
#     type: number
#     label: "GMROI Weighted"
#     view_label: "-- Stock Cover and GMROI BETA"
#     value_format_name: decimal_2
#     sql: ${avg_gmroi_parameter}*SAFE_DIVIDE(${gross_margin},SAFE_DIVIDE(${total_stock_value},${count_of_unique_dates})) ;;
#   }






}
