view: out_of_stock_days_bucket {
  label: "Out of stock analysis"
  derived_table: {
    sql: SELECT CONCAT(carolina_herrera_store_master.ilion_store_code," - ",carolina_herrera_store_master.store_name) AS carolina_herrera_store_master_store_cd_name,
                carolina_herrera_theoretical_stock.sku_externalcode  AS carolina_herrera_theoretical_stock_sku_externalcode,
                COALESCE(SUM((case when carolina_herrera_theoretical_stock.availablestock >0 then 0 else 1 end) ), 0) AS carolina_herrera_theoretical_stock_total_days_oos

        FROM `chb-prod-supplychain-data.prod_supply_chain.carolina_herrera_theoretical_stock` carolina_herrera_theoretical_stock

        LEFT JOIN `chb-prod-supplychain-data.prod_supply_chain.carolina_herrera_store_master` AS carolina_herrera_store_master
        ON cast(carolina_herrera_theoretical_stock.store as string) = cast(carolina_herrera_store_master.ilion_store_id as string)

WHERE ((case when carolina_herrera_theoretical_stock.sku_externalcode like 'A%' then "Accessories"
          when carolina_herrera_theoretical_stock.sku_externalcode like '12%' then "FW21"
          when carolina_herrera_theoretical_stock.sku_externalcode like '11%' then "SS21"
          when carolina_herrera_theoretical_stock.sku_externalcode like '01%' then "SS20"
          when carolina_herrera_theoretical_stock.sku_externalcode like '02%' then "FW20"
          when carolina_herrera_theoretical_stock.sku_externalcode like '91%' then "SS19"
          when carolina_herrera_theoretical_stock.sku_externalcode like '92%' then "FW19"
          when carolina_herrera_theoretical_stock.sku_externalcode like '81%' then "SS18"
          when carolina_herrera_theoretical_stock.sku_externalcode like '82%' then "FW18"

          else " Pre FW18" end = {% parameter Season %})) AND cast(carolina_herrera_theoretical_stock.stockdate as timestamp) >= IFNULL({% date_start stock_date %},CURRENT_TIMESTAMP())  AND cast(carolina_herrera_theoretical_stock.stockdate as timestamp) < IFNULL({% date_end stock_date %},CURRENT_TIMESTAMP())
GROUP BY 1,2
HAVING
  NOT (COALESCE(SUM((case when carolina_herrera_theoretical_stock.availablestock >0 then 0 else 1 end) ), 0) = 0)
ORDER BY 3 DESC
 ;;
  }

  filter: stock_date {
    type: date
    label: "Stock Date for OOS"
    default_value: "after 2021/01/01"
  }

  parameter: Season {
    type: string
    allowed_value: {
      label: "FW20"
      value: "FW20"
    }
    allowed_value: {
      label: "SS21"
      value: "SS21"
    }
    allowed_value: {
      label: "FW21"
      value: "FW21"
    }
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  dimension: carolina_herrera_store_master_store_cd_name {
    label: "Store code- Name"
    type: string
    hidden: yes
    sql: ${TABLE}.carolina_herrera_store_master_store_cd_name ;;
  }

  dimension: carolina_herrera_theoretical_stock_sku_externalcode {
    label: "sku code"
    type: string
    hidden: yes
    sql: ${TABLE}.carolina_herrera_theoretical_stock_sku_externalcode ;;
  }

  dimension: carolina_herrera_theoretical_stock_total_days_oos {
    type: number
    hidden: yes
    sql: ${TABLE}.carolina_herrera_theoretical_stock_total_days_oos ;;
  }

  dimension: total_days_oos_bucket {
    type: string
    label: "OOS days bucket"
    hidden: yes
    sql: case when ${TABLE}.carolina_herrera_theoretical_stock_total_days_oos in (1,3) then "1-3 days"
              when ${TABLE}.carolina_herrera_theoretical_stock_total_days_oos in (4,6) then "4-6 days"
              when ${TABLE}.carolina_herrera_theoretical_stock_total_days_oos in (7,9) then "7-9 days"
              else ">9 days" end;;
  }

  measure: count_items_oos {
    type: count_distinct
    label: "Total items OOS"
    hidden: yes
    sql:  ${TABLE}.carolina_herrera_theoretical_stock_sku_externalcode;;
  }

  set: detail {
    fields: [carolina_herrera_store_master_store_cd_name, carolina_herrera_theoretical_stock_sku_externalcode, carolina_herrera_theoretical_stock_total_days_oos]
  }
}
