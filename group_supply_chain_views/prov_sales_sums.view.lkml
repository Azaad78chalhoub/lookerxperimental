view: prov_sales_sums {
  derived_table: {
    persist_for: "24 hours"
    sql:
SELECT
    REGEXP_EXTRACT(CAST(sales.bk_businessdate AS STRING), r"(.{6})") AS bk_month,
  CAST(sales.bk_storeid AS STRING) AS store,
  CAST(sales.bk_productid AS STRING) AS item,
  SUM(sales.mea_quantity) AS sales_qty
FROM
  `chb-prod-supplychain-data.prod_facts.factretailsales` sales
GROUP BY
  1,
  2,
  3;;
  }


  dimension: pk {
    type: string
    primary_key: yes
    sql: CONCAT ${bk_month}, " - ", ${bk_storeid}, " - ", ${item})  ;;
  }

  dimension: item {
    type: string
    primary_key: no
    sql: ${TABLE}.item ;;
  }

   dimension: bk_storeid {
    type: string
    primary_key: no
    sql: ${TABLE}.store ;;
  }


  dimension: bk_month {
    type: string
    primary_key: no
    sql: ${TABLE}.bk_month ;;
  }



  dimension: sales_qty {
    type: number
    hidden: yes
    sql: ${TABLE}.sales_qty ;;
  }

  measure: total_sales_quantity {
    type: sum
    value_format_name: decimal_0
    sql: ${sales_qty} ;;
  }



}
