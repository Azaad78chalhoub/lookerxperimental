view: dm_provision_vs_sales {
  sql_table_name: `chb-prod-supplychain-data.prod_supply_chain.dm_provision_vs_sales`
    ;;

  dimension_group: date {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    group_label: "Provision date"
    label: "Provision"
    description: "Provision amount at start of the month"
    convert_tz: no
    datatype: date
    sql: ${TABLE}.date ;;
  }
  dimension_group: purchase_date {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    group_label: "Received date"
    label: "Received"
    description: "Received date of provision product"
    convert_tz: no
    hidden: no
    datatype: date
    sql: ${TABLE}.purchase_date ;;
  }

  dimension: depreciated_unit_value {
    type: number
    hidden: yes
    sql: ${TABLE}.depreciated_unit_value ;;
  }

  dimension: org_num {
    type: string
    hidden: yes
    sql: ${TABLE}.org_num ;;
  }

  dimension: original_unit_stock_value {
    type: number
    hidden: yes
    sql: ${TABLE}.original_unit_stock_value ;;
  }

  dimension: prod_num {
    type: string
    hidden: yes
    sql: ${TABLE}.prod_num ;;
  }

  dimension: prov_quantity {
    type: number
    hidden: yes
    sql: ${TABLE}.prov_quantity ;;
  }

  dimension: prov_value_sold_prov_product {
    type: number
    hidden: yes
    sql: ${TABLE}.prov_value_sold_prov_product ;;
  }

  dimension: sales_qty {
    type: number
    hidden: yes
    sql: ${TABLE}.sales_qty ;;
  }

  dimension: sales_revenue {
    type: number
    hidden: yes
    sql: ${TABLE}.sales_revenue ;;
  }

  dimension: sales_unit_value {
    type: number
    hidden: yes
    sql: ${TABLE}.sales_unit_value ;;
  }

  dimension: year_month {
    type: string
    hidden: yes
    sql: ${TABLE}.year_month ;;
  }

  measure: count {
    type: count
    hidden: yes
    drill_fields: []
  }


  measure: sales_qty_sum {
    type: sum
    hidden: no
    group_label: "Sold Item"
    label: "Total Provision Item Selling"
    description: "Total Items Sold Out of Provision Product"
    sql:sales_qty;;
    value_format_name: decimal_0
  }

  measure: prov_quantity_sum {
    type: sum
    hidden: no
    group_label: "Provision Item"
    label: "Total Provision Item"
    description: "Total item in provision product"
    sql:prov_quantity;;
    value_format_name: decimal_0
  }
  # measure: sales_revenue{
  #   type: number
  #   hidden: yes
  #   group_label: "Sales revenue from provision item"
  #   label: "Total revenue from sales of provision items"
  #   description: "Average unit cost (in USD) multiplied by quantity sold"
  #   sql:SUM(sales_revenue);;
  #   value_format_name: decimal_2
  # }
  measure: quantity_sold_from_provision {
    type: number
    hidden: no
    group_label: "Sold Item"
    label: "Percantage Items Sold From Provision Items"
    description: "Item Level Sales % From Provision"
    sql: ${sales_qty_sum}/${prov_quantity_sum} ;;
    value_format_name: percent_1
  }
  measure: revenue_from_provision {
    type: sum
    hidden: no
    group_label: "Sold Item"
    label: "USD Revenue From Provision Sales"
    description: "Revenue coming from sales of provision product"
    sql: ${sales_revenue} ;;

    value_format_name: usd_0
  }
  measure: sales_unit_value_num {
    type: number
    hidden:yes
    group_label: "Sales revenue from provision items"
    label: "USD Sales unit value of provision items"
    description: ""
    sql:${sales_unit_value} ;;
  }
  measure: depreciated_unit_value_num {
    type: number
    hidden: yes
    group_label: "Current provision value of items"
    label: "Depreciated value of provision items"
    sql: ${depreciated_unit_value} ;;
  }



  measure: product_sold_below_provison_value{
    type: sum
    hidden: yes
    group_label: "Sold Item"
    label: "Product Sold Below Book Value"
    description: "Items Selling Below Current Book Value"
    sql: case when sales_unit_value < depreciated_unit_value then sales_qty
      else 0 end;;

  }


  measure: percentage_product_sold_below_provision_value {
    type: number
    hidden: no
    group_label: "Provision Item"
    label: "Product Sold Below Provision(%)"
    description: "Percantage of product sold below provison value"
    sql: safe_divide(${product_sold_below_provison_value},${sales_qty_sum}) ;;
    value_format_name: percent_1
  }
  measure: current_stock_value {
    type: sum
    hidden: no
    group_label: "Provision Item"
    label: "Current book value of stocks"
    description: "Present book value of items in provision"
    sql: ${depreciated_unit_value}*${prov_quantity} ;;
    value_format_name: usd_0
  }

  measure: purchase_stock_value {
    type: sum
    hidden: no
    group_label: "Provision Item"
    label: "Purchase Value Of Provision Item"
    description: "Original purchase value of current provision product"
    sql: ${original_unit_stock_value}*${prov_quantity} ;;
    value_format_name: usd_0
  }
  measure: average_provision_percentage {
    type: number
    hidden: no
    group_label: "Provision Item"
    label: "Purchase Value Provisioned(%)"
    description: "Current book value as percentage of purchase value"
    sql: 1-safe_divide(${current_stock_value},${purchase_stock_value}) ;;
    value_format_name: percent_1
  }

}
