view: age_provision {
  sql_table_name: `chb-prod-supplychain-data.prod_supply_chain.fact_provision_soh`
    ;;

  dimension: last_date_2 {
    type: string
    hidden: yes
    sql: ${provision_last_date.last_date} ;;
  }

  dimension: is_last_day {
    type: yesno
    sql: ${last_date_2}=${provision_date} ;;
  }

  dimension: pk {
    type: string
    primary_key: yes
    hidden: yes
   sql: CONCAT(${provision_date}, " - ", ${org_num}, " - ", ${prod_num}, " - ", ${age}) ;;
  }

  dimension_group: provision {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.date AS TIMESTAMP) ;;
  }
  measure: last_updated_date {
    type: date
    label: "Last refresh date"
    sql: MAX(${provision_raw}) ;;
    convert_tz: no
  }


  dimension: age {
    type: number
    label: "Age Days"
    sql: ${TABLE}.age ;;
  }

  dimension: age_months {
    type: number
    sql:ROUND(${age}/30);;
  }

  dimension: age_bucket {
    type: tier
    tiers: [6,12,18,24,36,48]
    value_format_name: decimal_0
    style: relational
    sql:ROUND(${age}/30);;
  }

  dimension: to_currency {
    hidden: yes
    sql:  "USD" ;;
  }




  dimension: global1_exchange_rate {
    type: number
    hidden: yes
    sql: ${TABLE}.conversion_rate ;;
  }

  dimension: loc_curr_code {
    type: string
    hidden: yes
    sql: ${TABLE}.loc_curr_code ;;
  }

  dimension: status {
    type: string
    sql: ${TABLE}.status ;;
  }

  dimension: org_num {
    type: string
    label: "Location No."
    sql: ${TABLE}.org_num ;;
  }


  dimension: prod_num {
    type: string
    label: "Product No."
    sql: ${TABLE}.prod_num ;;
  }

  dimension: prov_running_qty {
    type: number
    label: "Provisions Running Quantity"
    sql: ${TABLE}.prov_running_qty ;;
  }

  dimension: soh_av_cost_amt_lcl {
    type: number
    label: "WAC Local"
    description: "Weighted Average Cost (WAC) Local"
    hidden: no
    sql: ${TABLE}.soh_av_cost_amt_lcl ;;
  }


  dimension: soh_av_cost_amt_usd {
    type: number
    label: "WAC USD"
    description: "Weighted Average Cost (WAC) USD"
    hidden: no
    sql: ${TABLE}.soh_av_cost_amt_usd ;;
  }


  dimension: provision_factor {
    type: number
    hidden: no
    sql: (CASE WHEN ${dim_retail_product.entity_type} = "EXPIRY (FASHION)" THEN 0.05
        WHEN ${dim_retail_product.entity_type} = "FASHION" THEN 0.05
        WHEN ${dim_retail_product.entity_type} = "BEAUTY" THEN 0.025
        WHEN ${dim_retail_product.entity_type} = "COMMON" THEN 1
        WHEN ${dim_retail_product.entity_type} = "GIFT" THEN 0.025
        ELSE 0
        END);;
  }

  dimension: provisions_local_amount {
    type: number
    hidden: yes
    sql:CASE WHEN ${age_months} <= 6 AND ${dim_retail_product.entity_type} != "COMMON" THEN 0
    WHEN ${age_months} > 6 AND ${age_months}  <= 26 AND (${dim_retail_product.entity_type} = "FASHION" OR ${dim_retail_product.entity_type} = "EXPIRY (FASHION)" )
    THEN ${soh_av_cost_amt_lcl}*${prov_running_qty}*${provision_factor}*(${age_months}-6)
    WHEN ${age_months} > 26 AND (${dim_retail_product.entity_type} = "FASHION" OR ${dim_retail_product.entity_type} = "EXPIRY (FASHION)" )
    THEN ${soh_av_cost_amt_lcl}*${prov_running_qty}
    WHEN ${age_months} > 6 AND ${age_months}  <= 46 AND (${dim_retail_product.entity_type} = "BEAUTY" OR ${dim_retail_product.entity_type} = "GIFT" )
    THEN
    ${soh_av_cost_amt_lcl}*${prov_running_qty}*${provision_factor}*(${age_months}-6)
    WHEN ${age_months} > 46 AND (${dim_retail_product.entity_type} = "BEAUTY" OR ${dim_retail_product.entity_type} = "GIFT" )
    THEN ${soh_av_cost_amt_lcl}*${prov_running_qty}
    WHEN ${dim_retail_product.entity_type} = "COMMON"
    THEN ${soh_av_cost_amt_lcl}*${prov_running_qty}
    ELSE 0
    END;;
  }

  dimension: provisions_monthly_local_amount {
    type: number
    hidden: yes
    sql:CASE WHEN ${age_months} <= 6 AND ${dim_retail_product.entity_type} != "COMMON" THEN 0
          WHEN ${age_months} > 6 AND ${age_months}  <= 26 AND (${dim_retail_product.entity_type} = "FASHION" OR ${dim_retail_product.entity_type} = "EXPIRY (FASHION)" )
          THEN ${soh_av_cost_amt_lcl}*${prov_running_qty}*${provision_factor}
          WHEN ${age_months} > 6 AND ${age_months}  <= 46 AND (${dim_retail_product.entity_type} = "BEAUTY" OR ${dim_retail_product.entity_type} = "GIFT" )
          THEN
          ${soh_av_cost_amt_lcl}*${prov_running_qty}*${provision_factor}
          WHEN ${dim_retail_product.entity_type} = "COMMON" AND ${age_months} = 0
          THEN ${soh_av_cost_amt_lcl}*${prov_running_qty}
          ELSE 0
          END;;
  }

  dimension: provisions_usd_amount {
    type: number
    hidden: yes
    sql:CASE WHEN ${age_months} <= 6 AND ${dim_retail_product.entity_type} != "COMMON" THEN 0
          WHEN ${age_months} > 6 AND ${age_months}  <= 26 AND (${dim_retail_product.entity_type} = "FASHION" OR ${dim_retail_product.entity_type} = "EXPIRY (FASHION)" )
          THEN ${soh_av_cost_amt_usd}*${prov_running_qty}*${provision_factor}*(${age_months}-6)
          WHEN ${age_months} > 26 AND (${dim_retail_product.entity_type} = "FASHION" OR ${dim_retail_product.entity_type} = "EXPIRY (FASHION)" )
          THEN ${soh_av_cost_amt_usd}*${prov_running_qty}
          WHEN ${age_months} > 6 AND ${age_months}  <= 46 AND (${dim_retail_product.entity_type} = "BEAUTY" OR ${dim_retail_product.entity_type} = "GIFT" )
          THEN
          ${soh_av_cost_amt_usd}*${prov_running_qty}*${provision_factor}*(${age_months}-6)
          WHEN ${age_months} > 46 AND (${dim_retail_product.entity_type} = "BEAUTY" OR ${dim_retail_product.entity_type} = "GIFT" )
          THEN ${soh_av_cost_amt_usd}*${prov_running_qty}
          WHEN ${dim_retail_product.entity_type} = "COMMON" AND ${age_months} > 6
          THEN ${soh_av_cost_amt_usd}*${prov_running_qty}
          ELSE 0
          END;;
  }

  dimension: provisions_monthly_usd_amount {
    type: number
    hidden: yes
    sql:CASE WHEN ${age_months} <= 6 AND ${dim_retail_product.entity_type} != "COMMON" THEN 0
          WHEN ${age_months} > 6 AND ${age_months}  <= 26 AND (${dim_retail_product.entity_type} = "FASHION" OR ${dim_retail_product.entity_type} = "EXPIRY (FASHION)" )
          THEN ${soh_av_cost_amt_usd}*${prov_running_qty}*${provision_factor}
          WHEN ${age_months} > 6 AND ${age_months}  <= 46 AND (${dim_retail_product.entity_type} = "BEAUTY" OR ${dim_retail_product.entity_type} = "GIFT" )
          THEN
          ${soh_av_cost_amt_usd}*${prov_running_qty}*${provision_factor}
          WHEN ${dim_retail_product.entity_type} = "COMMON" AND ${age_months} = 0
          THEN ${soh_av_cost_amt_usd}*${prov_running_qty}
          ELSE 0
          END;;
  }


  measure: sum_provisions_local_amount {
    type: sum
    label: "Provisions Amount Local"
    value_format_name: decimal_2
    sql:${provisions_local_amount};;
  }

  measure: provisions_amount_usd {
    type: sum
    value_format_name: usd
    sql:${provisions_usd_amount};;
  }

  measure: sum_provisions_monthly_local_amount {
    type: sum
    label: "Provisions Monthly Amount Local"
    value_format_name: decimal_2
    sql:${provisions_monthly_local_amount};;
  }

  measure: provisions_monthly_amount_usd {
    type: sum
    value_format_name: usd
    sql:${provisions_monthly_usd_amount};;
  }

  measure: last_date {
    hidden: yes
    type: date
    sql:MAX(${provision_date});;
  }

  measure: last_day_running_quantity {
    type: sum
    hidden: no
    value_format_name: decimal_0
    label: "Month Closing Running Quantity"
    filters: {
      field: is_last_day
      value: "yes"
    }
    sql: ${prov_running_qty};;
  }

  measure: last_day_provisions_monthly_local_amount {
    type: sum
    label: "Month Closing Monthly Amount Local"
    hidden: no
    value_format_name: decimal_0
    filters: {
      field: is_last_day
      value: "yes"
    }
    sql: ${provisions_monthly_local_amount};;
  }

  measure: last_day_provisions_monthly_usd {
    type: sum
    label: "Month Closing Monthly Amount USD"
    hidden: no
    value_format_name: usd
    filters: {
      field: is_last_day
      value: "yes"
    }
    sql: ${provisions_monthly_local_amount}/${global1_exchange_rate};;
  }


  measure: last_day_stock_value_local {
    type: sum
    label:  "Month Closing Stock Value Local"
    hidden: no
    value_format_name: decimal_0
    filters: {
      field: is_last_day
      value: "yes"
    }
    sql: ${prov_running_qty}*${soh_av_cost_amt_lcl};;
  }

  measure: last_day_stock_value_usd {
    type: sum
    label: "Month Closing Stock Value USD"
    hidden: no
    value_format_name: usd
    filters: {
      field: is_last_day
      value: "yes"
    }
    sql: ${prov_running_qty}*${soh_av_cost_amt_usd};;
  }

  measure: count {
    type: count
    hidden: yes
    drill_fields: [pk]
  }
}
