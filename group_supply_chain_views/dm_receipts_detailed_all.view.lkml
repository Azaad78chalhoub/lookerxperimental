view: dm_receipts_detailed_all {
  sql_table_name: `chb-prod-supplychain-data.prod_warehouse_logistics_supply_chain.dm_receipts_detailed_all`
    ;;

  dimension: pk {
    type: string
    primary_key: yes
    hidden: yes
    sql:concat(${item}, "-", ${order_no}, "-", ${location}, "-", ${receipt_date})
      ;;
  }

  dimension: item {
    type: string
    hidden: yes
    sql: ${TABLE}.item ;;
  }

  dimension: location {
    type: string
    hidden: yes
    sql: ${TABLE}.location ;;
  }

  dimension: order_no {
    type: string
    hidden: yes
    sql: ${TABLE}.order_no ;;
  }

  dimension: physical_warehouse {
    type: string
    hidden: yes
    sql: ${TABLE}.physical_warehouse ;;
  }

  dimension: qty_received {
    type: number
    hidden: yes
    sql: ${TABLE}.qty_received ;;
  }

  dimension_group: receipt {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.receipt_date ;;
  }

  dimension: ltl_till_date_received {
    type: string
    view_label: "Like to Like Timeframe comparison for Receipt Date"
    label: "LTL Timeframe- Receipt Date"
    description: "For Like to like timeframe comparison for Receipt Date"
    sql: CASE WHEN EXTRACT(MONTH FROM ${TABLE}.receipt_date) > EXTRACT(MONTH FROM CURRENT_TIMESTAMP) THEN "NO"
              WHEN EXTRACT(MONTH FROM ${TABLE}.receipt_date) < EXTRACT(MONTH FROM CURRENT_TIMESTAMP) THEN "YES"
              WHEN EXTRACT(MONTH FROM ${TABLE}.receipt_date) = EXTRACT(MONTH FROM CURRENT_TIMESTAMP) AND
              EXTRACT(DAY FROM ${TABLE}.receipt_date) >= EXTRACT(DAY FROM CURRENT_TIMESTAMP) THEN "NO"
              ELSE "YES" END;;
  }

  measure: count {
    type: count
    hidden: yes
    drill_fields: [order_no]
  }

  measure: qty_received_reciept {
    type: sum
    hidden: no
    label: "Receipt Quantity"
    group_label: "Receipt Metrics"
    description: "Total quantity received based on receipt date"
    value_format_name: decimal_0
    sql: ${qty_received} ;;
  }

  measure: qty_received_value_reciept_wac_local {
    label: "Receipt Value Local"
    type: sum
    group_label: "Receipt Metrics"
    description: "Total value received based on receipt date and WAC"
    value_format_name: decimal_0
    sql: ${qty_received}* ${dm_supply_orders_detailed.item_unit_cost} ;;
  }

}
