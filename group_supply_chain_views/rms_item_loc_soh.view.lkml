view: rms_item_loc_soh {
  sql_table_name: `chb-prod-stage-oracle.prod_oracle_rms.rms_item_loc_soh`
    ;;

  dimension: av_cost {
    type: number
    sql: ${TABLE}.AV_COST ;;
  }

  dimension: average_weight {
    type: number
    sql: ${TABLE}.AVERAGE_WEIGHT ;;
  }

  dimension_group: create_datetime {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.CREATE_DATETIME ;;
  }

  dimension: customer_backorder {
    type: number
    sql: ${TABLE}.CUSTOMER_BACKORDER ;;
  }

  dimension: customer_resv {
    type: number
    sql: ${TABLE}.CUSTOMER_RESV ;;
  }

  dimension: finisher_av_retail {
    type: number
    sql: ${TABLE}.FINISHER_AV_RETAIL ;;
  }

  dimension: finisher_units {
    type: number
    sql: ${TABLE}.FINISHER_UNITS ;;
  }

  dimension_group: first_received {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.FIRST_RECEIVED ;;
  }

  dimension: first_sold {
    type: string
    sql: ${TABLE}.FIRST_SOLD ;;
  }

  dimension: in_transit_qty {
    type: number
    sql: ${TABLE}.IN_TRANSIT_QTY ;;
  }

  dimension: item {
    type: string
    sql: ${TABLE}.ITEM ;;
  }

  dimension: item_grandparent {
    type: string
    sql: ${TABLE}.ITEM_GRANDPARENT ;;
  }

  dimension: item_parent {
    type: string
    sql: ${TABLE}.ITEM_PARENT ;;
  }

  dimension_group: last_hist_export {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.LAST_HIST_EXPORT_DATE ;;
  }

  dimension_group: last_received {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.LAST_RECEIVED ;;
  }

  dimension: last_sold {
    type: string
    sql: ${TABLE}.LAST_SOLD ;;
  }

  dimension_group: last_update_datetime {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.LAST_UPDATE_DATETIME ;;
  }

  dimension: last_update_id {
    type: string
    sql: ${TABLE}.LAST_UPDATE_ID ;;
  }

  dimension: loc {
    type: number
    sql: ${TABLE}.LOC ;;
  }

  dimension: loc_type {
    type: string
    sql: ${TABLE}.LOC_TYPE ;;
  }

  dimension: non_sellable_qty {
    type: number
    sql: ${TABLE}.NON_SELLABLE_QTY ;;
  }

  dimension: pack_comp_cust_back {
    type: number
    sql: ${TABLE}.PACK_COMP_CUST_BACK ;;
  }

  dimension: pack_comp_cust_resv {
    type: number
    sql: ${TABLE}.PACK_COMP_CUST_RESV ;;
  }

  dimension: pack_comp_exp {
    type: number
    sql: ${TABLE}.PACK_COMP_EXP ;;
  }

  dimension: pack_comp_intran {
    type: number
    sql: ${TABLE}.PACK_COMP_INTRAN ;;
  }

  dimension: pack_comp_non_sellable {
    type: number
    sql: ${TABLE}.PACK_COMP_NON_SELLABLE ;;
  }

  dimension: pack_comp_resv {
    type: number
    sql: ${TABLE}.PACK_COMP_RESV ;;
  }

  dimension: pack_comp_soh {
    type: number
    sql: ${TABLE}.PACK_COMP_SOH ;;
  }

  dimension: primary_cntry {
    type: string
    sql: ${TABLE}.PRIMARY_CNTRY ;;
  }

  dimension: primary_supp {
    type: number
    sql: ${TABLE}.PRIMARY_SUPP ;;
  }

  dimension: qty_received {
    type: number
    sql: ${TABLE}.QTY_RECEIVED ;;
  }

  dimension: qty_sold {
    type: number
    sql: ${TABLE}.QTY_SOLD ;;
  }

  dimension: rtv_qty {
    type: number
    sql: ${TABLE}.RTV_QTY ;;
  }

  dimension_group: soh_update_datetime {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.SOH_UPDATE_DATETIME ;;
  }

  dimension: stock_on_hand {
    type: number
    sql: ${TABLE}.STOCK_ON_HAND ;;
  }

  dimension: tsf_expected_qty {
    type: number
    sql: ${TABLE}.TSF_EXPECTED_QTY ;;
  }

  dimension: tsf_reserved_qty {
    type: number
    sql: ${TABLE}.TSF_RESERVED_QTY ;;
  }

  dimension: unit_cost {
    type: number
    sql: ${TABLE}.UNIT_COST ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
