view: carolina_herrera_staff_ilion_codes {
  sql_table_name: `chb-prod-supplychain-data.prod_supply_chain.carolina_herrera_staff_ilion_codes`
    ;;

  dimension: chalhoub_code {
    type: string
    sql: ${TABLE}.chalhoub_code ;;
  }

  dimension: country {
    type: string
    map_layer_name: countries
    sql: ${TABLE}.country ;;
  }

  dimension: ilion_code {
    type: string
    sql: ${TABLE}.ilion_code ;;
  }

  dimension: job_title {
    type: string
    sql: ${TABLE}.job_title ;;
  }

  dimension: staff_name {
    type: string
    sql: ${TABLE}.staff_name ;;
  }

  dimension: staff_code_name {
    type: string
    label: "Ilion code - Staff Name - Emp Code - Designation"
    sql: CONCAT(${TABLE}.ilion_code,"-",${TABLE}.staff_name,"-",${TABLE}.chalhoub_code,"-",${TABLE}.job_title) ;;
  }

  dimension: store_code {
    type: string
    sql: ${TABLE}.store_code ;;
  }

  measure: count {
    type: count
    drill_fields: [staff_name]
  }
}
