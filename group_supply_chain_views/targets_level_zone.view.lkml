view: targets_level_zone {
    derived_table: {
      sql:
with subtable AS(
  SELECT
    * EXCEPT(str_target, type)
    ,null as str_target_fp
    ,str_target as str_target_md
  FROM
    `chb-prod-stage-oracle.prod_supply_chain.targets_level_zone`
  WHERE
    type ="MD"
 UNION ALL
   SELECT
    * EXCEPT(str_target, type)
    ,str_target as str_target_fp
    ,null as str_target_md
  FROM
    `chb-prod-stage-oracle.prod_supply_chain.targets_level_zone`
  WHERE
    type ="FP"
 )
 SELECT
 year
 ,weeknum
 ,zone
 ,bu_code
 ,loc_season
 ,avg(str_target_fp) as str_target_fp
 ,avg(str_target_md) as str_target_md
 FROM subtable
 GROUP BY
   year
 ,weeknum
 ,zone
 ,bu_code
 ,loc_season
      ;;
    }

  dimension: pk {
    primary_key: yes
    type: string
    sql: CONCAT(${year}, " - "${weeknum}, " - "${bu_code}, " - "${zone}, " - "${loc_season}) ;;
  }

  dimension: bu_code {
    type: string
    hidden: yes
    sql: ${TABLE}.bu_code ;;
  }

  dimension: loc_season {
    type: string
    hidden: no
    sql: ${TABLE}.loc_season ;;
  }

  dimension: str_target_fp {
    type: number
    hidden: no
    sql: ${TABLE}.str_target_fp ;;
  }

  dimension: str_target_md {
    type: number
    hidden: no
    sql: ${TABLE}.str_target_md ;;
  }


  dimension: weeknum {
    type: number
    hidden: yes
    sql: CAST(${TABLE}.weeknum AS INT64) ;;
  }

  dimension: col_type {
    type: string
    hidden: yes
  }

  dimension: year {
    type: number
    hidden: yes
    sql: ${TABLE}.year ;;
  }

  dimension: zone {
    type: string
    hidden: yes
    sql: ${TABLE}.zone ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
