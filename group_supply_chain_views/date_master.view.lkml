view: date_master  {
    derived_table: {
      sql: select timestamp(mydate) as date from
(
select * from  UNNEST(GENERATE_DATE_ARRAY('2018-10-01', CURRENT_DATE(), INTERVAL 1 DAY)) as mydate

) ;;
    }

    dimension: primary_key {
      primary_key: yes
      hidden: yes
      type: string
      sql: ${TABLE}.date ;;
    }

    dimension_group: date {
      label: "Master Date"
      type: time
      timeframes: [date, week, month]
      convert_tz: no
      sql: ${TABLE}.date ;;
    }
  }
