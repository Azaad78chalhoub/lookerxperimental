view: rso_suppliesrequestsv2 {
  label: "RSO detils"
  sql_table_name: `chb-prod-supplychain-data.prod_supply_chain.rso_suppliesrequestsv2`
    ;;

  dimension: adminincharge {
    type: string
    sql: ${TABLE}.adminincharge ;;
  }

  dimension_group: approveddate {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.approveddate ;;
  }

  dimension: approvercomments {
    type: string
    sql: ${TABLE}.approvercomments ;;
  }

  dimension: brandid {
    type: number
    value_format_name: id
    sql: ${TABLE}.brandid ;;
  }

  dimension: categoryid {
    type: number
    value_format_name: id
    sql: ${TABLE}.categoryid ;;
  }

  dimension: cityid {
    type: number
    value_format_name: id
    sql: ${TABLE}.cityid ;;
  }

  dimension_group: closeddate {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.closeddate ;;
  }

  dimension: comments {
    type: string
    sql: ${TABLE}.comments ;;
  }

  dimension: countryid {
    type: number
    value_format_name: id
    sql: ${TABLE}.countryid ;;
  }

  dimension_group: delivereddate {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.delivereddate ;;
  }

  dimension: employeeid {
    type: string
    sql: ${TABLE}.employeeid ;;
  }

  dimension: guid {
    type: string
    sql: ${TABLE}.guid ;;
  }

  dimension: mallid {
    type: number
    value_format_name: id
    sql: ${TABLE}.mallid ;;
  }

  dimension: month {
    type: number
    sql: ${TABLE}.month ;;
  }

  dimension: monthbudget {
    type: number
    sql: ${TABLE}.monthbudget ;;
  }

  dimension: postedby {
    type: string
    sql: ${TABLE}.postedby ;;
  }

  dimension_group: posteddate {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.posteddate ;;
  }

  dimension: referencenumber {
    type: string
    sql: ${TABLE}.referencenumber ;;
  }

  dimension: requestid {
    type: number
    value_format_name: id
    sql: ${TABLE}.requestid ;;
  }

  dimension: state {
    type: string
    sql: ${TABLE}.state ;;
  }

  dimension: status {
    type: string
    sql: ${TABLE}.status ;;
  }

  dimension: storeid {
    type: number
    value_format_name: id
    sql: ${TABLE}.storeid ;;
  }

  dimension: totalamountordered {
    type: number
    sql: ${TABLE}.totalamountordered ;;
  }

  dimension: totalamountreceived {
    type: number
    sql: ${TABLE}.totalamountreceived ;;
  }

  dimension: year {
    type: number
    sql: ${TABLE}.year ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
