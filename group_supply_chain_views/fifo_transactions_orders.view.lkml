view: fifo_transactions_orders {
  label: "Cost Table"
  derived_table: {
    explore_source: fifo_transactions {
      column: bk_productid {}
      column: total_qty_received  {}
      column: total_value_received {}
      derived_column: avg_unit_cost {
        sql: SAFE_DIVIDE(total_value_received,total_qty_received)  ;;
      }
    bind_all_filters: yes
  }
  }

  dimension: bk_productid {
    hidden: yes
    primary_key: yes
  }

  dimension: total_qty_received {
    type: number
    hidden: yes
  }

  dimension: avg_unit_cost {
    type: number
    value_format_name: usd
    label: "Average Unit Cost"
    hidden: no

  }


}
