view: provision_last_date {
  derived_table: {
    explore_source: age_provision {
      column: last_date {}
      bind_all_filters: yes
    }
  }

  dimension: last_date {
    type: date
    hidden: yes
  }

}
