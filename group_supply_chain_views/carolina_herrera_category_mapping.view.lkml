view: carolina_herrera_category_mapping {
  label: "Category Details"
  sql_table_name: `chb-prod-supplychain-data.prod_supply_chain.carolina_herrera_category_mapping`
    ;;

  dimension: category {
    type: string
    label: "Category"
    drill_fields: [family,carolina_herrera_sales_at_time_interval.sku_size]
    sql: ${TABLE}.category ;;
  }

  dimension: family {
    type: string
    label: "Family"
    drill_fields: [carolina_herrera_sales_at_time_interval.sku_externalcode,carolina_herrera_sales_at_time_interval.sku_size,carolina_herrera_sales_at_time_interval.sku_season]
    sql: ${TABLE}.family ;;
  }

  dimension: identifier {
    type: string
    hidden: yes
    sql: ${TABLE}.identifier ;;
  }

  dimension: reference_code {
    type: string
    hidden: yes
    sql: ${TABLE}.reference_code ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
