view: rso_suppliesrequesteditemsv2 {
  sql_table_name: `chb-prod-supplychain-data.prod_supply_chain.rso_suppliesrequesteditemsv2`
    ;;

  dimension: itemid {
    type: number
    value_format_name: id
    sql: ${TABLE}.itemid ;;
  }

  dimension: quantitydelivered {
    type: number
    sql: ${TABLE}.quantitydelivered ;;
  }

  dimension: quantityreceived {
    type: number
    sql: ${TABLE}.quantityreceived ;;
  }

  dimension: quantityrequired {
    type: number
    sql: ${TABLE}.quantityrequired ;;
  }

  dimension: requesteditemid {
    type: number
    value_format_name: id
    sql: ${TABLE}.requesteditemid ;;
  }

  dimension: requestid {
    type: number
    value_format_name: id
    sql: ${TABLE}.requestid ;;
  }

  measure: quantity_required {
    label: "Quantity Required by Store"
    type: sum
    drill_fields: []
    sql: ${quantityrequired} ;;
  }

  measure: quantity_ordered {
    label: "Quantity Ordered by Admin"
    type: sum
    drill_fields: []
    sql: ${quantitydelivered} ;;
  }

  measure: quantity_received {
    label: "Quantity Received by Store"
    type: sum
    drill_fields: []
    sql: ${quantityreceived} ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
