view: sellthrough_ch_pg {
  derived_table: {
    sql: with sub_query1 as (
      select
            store,
             store_code,
            sku_externalcode,
            stockdate,
            availablestock,
            quantity,
            quantity_fp,
            quantity_md,
            totalnet_usd_fp,
            totalnet_usd_md,
            totalnet_fp,
            totalnet_md
      from

      (select
                        case when stocktbl.stockdate is null then sales.dateop else stocktbl.stockdate end as stockdate,
                        case when stocktbl.country is null then sales.country else stocktbl.country end as country,
                        case when stocktbl.store is null then sales.store else stocktbl.store end as store,
                        case when stocktbl.store_code is null then sales.store_code else stocktbl.store_code end as store_code,
                        case when stocktbl.sku_externalcode is null then sales.sku_externalcode else stocktbl.sku_externalcode end as sku_externalcode,
                        availablestock,
                        sales.quantity,
                        sales.quantity_fp,
                        sales.quantity_md,
                        sales.totalnet_usd_fp,
                        sales.totalnet_usd_md,
                        sales.totalnet_fp,
                        sales.totalnet_md

                        from `chb-prod-supplychain-data.prod_supply_chain.carolina_herrera_theoretical_stock` stocktbl
                  FULL OUTER JOIN
                  (
                 select store,
                        store_code,
                        dateop,
                        sku_externalcode,
                        country,
                        sum(quantity_fp) as quantity_fp,
                        sum(quantity_md) as quantity_md,
                        sum(quantity) as quantity,
                        sum(totalnet_usd_fp) as totalnet_usd_fp,
                        sum(totalnet_usd_md) as totalnet_usd_md,
                        sum(totalnet_fp) as totalnet_fp,
                        sum(totalnet_md) as totalnet_md
                 from
                 (
                  select  store,
                          store_code,
                          dateop,
                          sku_externalcode,
                          ticketnumber,
                          country,
                          case when (ticket_price is null or updated_seasonprice is null or ticket_price > updated_seasonprice) then quantity
                          when ticket_price = updated_seasonprice then quantity else 0 end as quantity_fp,
                          case when (ticket_price is null or  updated_seasonprice is null or ticket_price > updated_seasonprice) then 0
                          when ticket_price < updated_seasonprice then quantity else 0 end as quantity_md,
                          case when (ticket_price is null or updated_seasonprice is null or ticket_price > updated_seasonprice) then totalnet_usd
                          when ticket_price = updated_seasonprice then totalnet_usd else 0 end as totalnet_usd_fp,
                          case when (ticket_price is null or  updated_seasonprice is null or ticket_price > updated_seasonprice) then 0
                          when ticket_price < updated_seasonprice then totalnet_usd else 0 end as totalnet_usd_md,
                           case when (ticket_price is null or updated_seasonprice is null or ticket_price > updated_seasonprice) then totalnet
                          when ticket_price = updated_seasonprice then totalnet else 0 end as totalnet_fp,
                          case when (ticket_price is null or  updated_seasonprice is null or ticket_price > updated_seasonprice) then 0
                          when ticket_price < updated_seasonprice then totalnet else 0 end as totalnet_md,
                          ticket_price,
                          updated_seasonprice,
                          quantity

                 from (
                  select  a.store,
                          a.store_code,
                          a.dateop,
                          a.sku_externalcode,
                          a.ticketnumber,
                          a.country,
                          a.ticket_price,
                          IFNULL(b.seasonprice,c.seasonprice) AS updated_seasonprice,
                          a.quantity,
                          a.totalnet,
                          a.totalnet_usd

                  from `chb-prod-supplychain-data.prod_supply_chain.carolina_herrera_sales_at_time_interval` a

                  left join (select a.*,b.min_update_date, cast(dbt_updated_at as date) as update_date,
                            IFNULL(LEAD(cast(dbt_updated_at as date)) OVER(PARTITION BY a.store,a.sku_code order by cast(dbt_updated_at as date) asc),CURRENT_DATE()) AS next_updated_on

                              from `chb-prod-supplychain-data.prod_supply_chain.carolina_herrera_prices_quotation_at_given_date_daily_snapshots` a

                              join (select store,sku_code,min(cast(dbt_updated_at as date)) as min_update_date from `chb-prod-supplychain-data.prod_supply_chain.carolina_herrera_prices_quotation_at_given_date_daily_snapshots`
                                    group by 1,2) b
                              on a.store = b.store and
                                  a.sku_code = b.sku_code) b
                  on a.store = b.store and
                    a.sku_externalcode = b.sku_code and
                    (a.dateop >= update_date AND a.dateop < next_updated_on)

                  left join (select * from
                        (
                        select a.*,b.min_update_date, cast(dbt_updated_at as date) as update_date,
                                  IFNULL(LEAD(cast(dbt_updated_at as date)) OVER(PARTITION BY a.store,a.sku_code order by cast(dbt_updated_at as date) asc),CURRENT_DATE()) AS next_updated_on

                                    from `chb-prod-supplychain-data.prod_supply_chain.carolina_herrera_prices_quotation_at_given_date_daily_snapshots` a

                              join (select store,sku_code,min(cast(dbt_updated_at as date)) as min_update_date from `chb-prod-supplychain-data.prod_supply_chain.carolina_herrera_prices_quotation_at_given_date_daily_snapshots`
                              group by 1,2) b
                                   on a.store = b.store and
                                    a.sku_code = b.sku_code
                  )
                  where update_date = min_update_date
                  ) c

                  on a.store = c.store and
                      a.sku_externalcode = c.sku_code
                  ))


                  where cast(dateop as timestamp) >= ifnull({% date_start timeframe %},CURRENT_TIMESTAMP()) and cast(dateop as timestamp) <= ifnull({% date_end timeframe %},CURRENT_TIMESTAMP())
                    group by 1,2,3,4,5) sales


                  on cast(stocktbl.store as string) = cast(sales.store as string)
                  and stocktbl.sku_externalcode = sales.sku_externalcode
                  and stocktbl.stockdate = sales.dateop

                  where cast(stockdate as timestamp) >= ifnull({% date_start timeframe %},CURRENT_TIMESTAMP()) and cast(stockdate as timestamp) <= ifnull({% date_end timeframe %},CURRENT_TIMESTAMP())


                  order by store,sku_externalcode,stockdate)
                  order by 1,2,3,4 asc),
              --opening stock
              sub_query2 as (select a.store,a.store_code,a.sku_externalcode,a.availablestock as opening_stock,b.total_quantity,b.total_quantity_fp,b.total_quantity_md,b.totalnet_usd_fp,b.totalnet_usd_md,b.totalnet_fp,b.totalnet_md
              from sub_query1 a
              join
              (select store_code,sku_externalcode, min(stockdate) as min_date,sum(quantity) as total_quantity,sum(quantity_fp) as total_quantity_fp, sum(quantity_md) as total_quantity_md,sum(totalnet_usd_fp) as totalnet_usd_fp ,sum(totalnet_usd_md) as totalnet_usd_md,sum(totalnet_fp) as totalnet_fp, sum(totalnet_md) as totalnet_md
              from sub_query1
              group by 1,2) b
              on a.store_code = b.store_code and
                a.sku_externalcode = b.sku_externalcode and
                 a.stockdate = b.min_date),

              sub_query3 as (
              select a.store,a.store_code,a.sku_externalcode,(a.availablestock- ifnull(a.quantity,0)) as closing_stock
              from sub_query1 a
              join
              (select store_code,sku_externalcode, max(stockdate) as max_date
              from sub_query1
              group by 1,2) b
              on a.store_code = b.store_code and
                a.sku_externalcode = b.sku_externalcode and
                 a.stockdate = b.max_date)

                 select a.store,
                        a.store_code,
                        a.sku_externalcode,
                        a.opening_stock,
                        a.total_quantity,
                        a.total_quantity_fp,
                        a.total_quantity_md,
                        a.totalnet_usd_fp,
                        a.totalnet_usd_md,
                        a.totalnet_fp,
                        a.totalnet_md,
                        b.closing_stock
                from sub_query2 a
                join sub_query3 b
                on a.store_code = b.store_code and
                   a.sku_externalcode = b.sku_externalcode
       ;;
  }

  filter: timeframe {
    type: date
    label: "Time frame"
    default_value: "after 2021/04/01"
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  dimension: store {
    type: string
    hidden: yes
    sql: ${TABLE}.store ;;
  }
  dimension: store_code {
    type: string
    hidden: yes
    sql: ${TABLE}.store_code ;;
  }

  dimension: sku_externalcode {
    type: string
    hidden: yes
    sql: ${TABLE}.sku_externalcode ;;
  }

  dimension: opening_stock {
    type: number
    hidden: yes
    sql: ${TABLE}.opening_stock ;;
  }

  dimension: total_quantity {
    type: number
    hidden: yes
    sql: ${TABLE}.total_quantity ;;
  }

  dimension: total_quantity_fp {
    type: number
    hidden: yes
    sql: ${TABLE}.total_quantity_fp ;;
  }

  dimension: total_quantity_md {
    type: number
    hidden: yes
    sql: ${TABLE}.total_quantity_md ;;
  }

  dimension: totalnet_fp {
    type: number
    hidden: yes
    sql: ${TABLE}.totalnet_fp ;;
  }

  dimension: totalnet_md {
    type: number
    hidden: yes
    sql: ${TABLE}.totalnet_md ;;
  }

  dimension: totalnet_usd_fp {
    type: number
    hidden: yes
    sql: ${TABLE}.totalnet_usd_fp ;;
  }

  dimension: totalnet_usd_md {
    type: number
    hidden: yes
    sql: ${TABLE}.totalnet_usd_md ;;
  }

  dimension: closing_stock {
    type: number
    hidden: yes
    sql: ${TABLE}.closing_stock ;;
  }


  set: detail {
    fields: [store,store_code, sku_externalcode, opening_stock, total_quantity,total_quantity_fp,total_quantity_md,totalnet_fp,totalnet_md,totalnet_usd_fp,totalnet_usd_md, closing_stock]
  }
}
