view: str_age {
  derived_table: {
persist_for: "24 hours"
    sql:
WITH
  buckets AS(
  SELECT
    *
  FROM
    UNNEST(["0","1","2","3","4","5","6","7","10","12"]) AS age_bucket),
  distincts AS(
  SELECT
    DISTINCT item,
    org_num
  FROM
    `chb-prod-supplychain-data.prod_supply_chain.dm_soh_sales`
  WHERE
    DATE_DIFF(IFNULL(DATE({% parameter str_age.report_date %}), CURRENT_DATE()), stock_date, MONTH) <=12
    AND business_unit="THE DEAL" and country_desc={% parameter str_age.report_country %}),
  stock_received AS(
SELECT
  org_num,
  item,
  inv_soh_qty as qty_left
FROM (
  SELECT
    org_num,
    item,
    stock_date,
    inv_soh_qty,
    LAST_VALUE(stock_date) OVER(PARTITION BY org_num, item ORDER BY stock_date ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) AS last_date
  FROM
    `chb-prod-supplychain-data.prod_supply_chain.dm_soh_sales`
  WHERE
    DATE_DIFF(IFNULL(DATE({% parameter str_age.report_date %}), CURRENT_DATE()), stock_date, MONTH) <=12
    AND business_unit="THE DEAL" and country_desc={% parameter str_age.report_country %})
WHERE
  last_date= stock_date


    ),
  sales AS(
  SELECT
    CASE
      WHEN DATE_DIFF(IFNULL(DATE({% parameter str_age.report_date %}), CURRENT_DATE()), stock_date, MONTH)  <7 THEN DATE_DIFF(IFNULL(DATE({% parameter str_age.report_date %}), CURRENT_DATE()), stock_date, MONTH)
      WHEN DATE_DIFF(IFNULL(DATE({% parameter str_age.report_date %}), CURRENT_DATE()), stock_date, MONTH)  >=7 AND DATE_DIFF(IFNULL(DATE({% parameter str_age.report_date %}), CURRENT_DATE()), stock_date, MONTH)  < 10 THEN 7
      WHEN DATE_DIFF(IFNULL(DATE({% parameter str_age.report_date %}), CURRENT_DATE()), stock_date, MONTH)  >=10 AND DATE_DIFF(IFNULL(DATE({% parameter str_age.report_date %}), CURRENT_DATE()), stock_date, MONTH)  < 12 THEN 10
    ELSE
    12
  END
    AS age_bucket,
    org_num,
    item,
    SUM(sales_qty) AS sales_qty
  FROM
    `chb-prod-supplychain-data.prod_supply_chain.dm_soh_sales`
  WHERE
    DATE_DIFF(IFNULL(DATE({% parameter str_age.report_date %}), CURRENT_DATE()), stock_date, MONTH) <=12
     AND business_unit="THE DEAL" and country_desc={% parameter str_age.report_country %}
  GROUP BY
    1,
    2,
    3)
, final_table AS(
SELECT
  distincts.org_num,
  distincts.item,
  CAST(buckets.age_bucket AS INT64) as age_bucket,
  SUM(qty_left) AS qty_left,
  SUM(sales_qty) AS sales_qty
FROM
  distincts
CROSS JOIN
  buckets
LEFT JOIN
  sales
ON
  distincts.item=sales.item
  AND distincts.org_num=sales.org_num
  AND CAST(buckets.age_bucket AS INT64)=sales.age_bucket
LEFT JOIN
  stock_received
ON
  distincts.item=stock_received.item
  AND distincts.org_num=stock_received.org_num
GROUP BY 1,2,3
ORDER BY age_bucket ASC
)
SELECT *,
  SUM(sales_qty) OVER( PARTITION BY org_num,item ORDER BY age_bucket) as running_sales_qty,
  SUM(sales_qty) OVER( PARTITION BY org_num,item) as total_sales_qty
FROM final_table

    ;;
  }

  parameter: report_date {
    type: date
  }

  dimension: age_bucket_2 {
    label: "Age Bucket"
    case: {
      when: {
        sql: ${age_bucket} = 0 ;;
        label: "0"
      }
      when: {
        sql: ${age_bucket} = 1 ;;
        label: "1"
      }
      when: {
        sql: ${age_bucket} = 2 ;;
        label: "2"
      }
      when: {
        sql: ${age_bucket} = 3 ;;
        label: "3"
      }
      when: {
        sql: ${age_bucket} = 4 ;;
        label: "4"
      }      when: {
        sql: ${age_bucket} = 5 ;;
        label: "5"
      }
      when: {
        sql: ${age_bucket} = 6 ;;
        label: "6"
      }
      when: {
        sql: ${age_bucket} = 7 ;;
        label: "7-9"
      }
      when: {
        sql: ${age_bucket} = 10 ;;
        label: "10-12"
      }
      else: "12+"
    }
  }

  parameter: report_country {
    allowed_value: {
      label: "UAE"
      value: "United Arab Emirates"
    }
    allowed_value: {
      label: "KSA"
      value: "Saudi Arabia"
    }
    allowed_value: {
      label: "EGY"
      value: "Egypt"
    }
    allowed_value: {
      label: "QAT"
      value: "Qatar"
    }
    allowed_value: {
      label: "KWT"
      value: "Kuwait"
    }
    default_value: "United Arab Emirates"
  }

  dimension: item {
    label: "Product ID"
    view_label: "Products"
  }
  dimension: age_bucket {
    type: number
    hidden: yes
  }
  dimension: org_num {
    label: "Location Number"
    view_label: "Locations"
    sql: CAST(${TABLE}.org_num AS STRING) ;;
  }

  dimension: qty_left {
    type: number
    hidden: yes
  }
  dimension: sales_qty {
    type: number
    hidden: yes
  }
  dimension: running_sales_qty {
    type: number
    hidden: yes
  }

  dimension: total_sales_qty {
    hidden: yes
    type: number
  }

  dimension: count_of_age {
    type: number
    hidden: yes
    sql: COUNT(DISTINCT ${age_bucket}) ;;
  }

  dimension: pk {
    primary_key: yes
    hidden: yes
    sql: CONCAT(${org_num}, " - " ,${item}, " - " ,${age_bucket} ;;
  }

  measure: str {
    label: "Sell Through Rate %"
    type: number
    value_format_name: percent_2
    sql: SAFE_DIVIDE(SUM(IFNULL(running_sales_qty,0)),SUM(IFNULL(total_sales_qty,0)+qty_left)) ;;
  }

  measure: total_running_sales_qty {
    label: "Running Sales Quantity"
    type: number
    value_format_name: decimal_0
    sql: SUM(${running_sales_qty}) ;;
  }

  measure: quantity_left {
    label: "Quantity Left"
    type: number
    value_format_name: decimal_0
    sql: SUM(${qty_left}) ;;
  }


}

# view: str_age {
#   # Or, you could make this view a derived table, like this:
#   derived_table: {
#     sql: SELECT
#         user_id as user_id
#         , COUNT(*) as lifetime_orders
#         , MAX(orders.created_at) as most_recent_purchase_at
#       FROM orders
#       GROUP BY user_id
#       ;;
#   }
#
#   # Define your dimensions and measures here, like this:
#   dimension: user_id {
#     description: "Unique ID for each user that has ordered"
#     type: number
#     sql: ${TABLE}.user_id ;;
#   }
#
#   dimension: lifetime_orders {
#     description: "The total number of orders for each user"
#     type: number
#     sql: ${TABLE}.lifetime_orders ;;
#   }
#
#   dimension_group: most_recent_purchase {
#     description: "The date when each user last ordered"
#     type: time
#     timeframes: [date, week, month, year]
#     sql: ${TABLE}.most_recent_purchase_at ;;
#   }
#
#   measure: total_lifetime_orders {
#     description: "Use this for counting lifetime orders across many users"
#     type: sum
#     sql: ${lifetime_orders} ;;
#   }
# }
