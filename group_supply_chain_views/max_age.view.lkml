# If necessary, uncomment the line below to include explore_source.
# include: "group_supply_chain.model.lkml"

view: max_age {
  derived_table: {
    explore_source: fact_soh_sales_poc {
      column: org_num {}
      column: prod_num {}
      column: max_stock_age {}
bind_all_filters: yes
    }
  }
  dimension: org_num {
    hidden: yes
  }
  dimension: prod_num {
    hidden: yes
  }
  dimension: max_stock_age {
    hidden: yes
  }
}
