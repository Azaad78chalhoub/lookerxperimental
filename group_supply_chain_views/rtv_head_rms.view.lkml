view: rtv_head_rms {
  sql_table_name: `chb-prod-supplychain-data.prod_supply_chain.rtv_head_rms`
    ;;

  dimension: comment_desc {
    type: string
    sql: ${TABLE}.comment_desc ;;
  }

  dimension_group: completed {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.completed_date ;;
  }

  dimension: courier {
    type: string
    sql: ${TABLE}.courier ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_date ;;
  }

  dimension: dept {
    type: string
    sql: ${TABLE}.dept ;;
  }

  dimension: ext_ref_no {
    type: string
    sql: ${TABLE}.ext_ref_no ;;
  }

  dimension: freight {
    type: string
    sql: ${TABLE}.freight ;;
  }

  dimension: item {
    type: string
    sql: ${TABLE}.item ;;
  }

  dimension: mrt_no {
    type: number
    sql: ${TABLE}.mrt_no ;;
  }

  dimension_group: not_after {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.not_after_date ;;
  }

  dimension: reason {
    type: string
    sql: ${TABLE}.reason ;;
  }

  dimension: restock_cost {
    type: number
    sql: ${TABLE}.restock_cost ;;
  }

  dimension: restock_pct {
    type: number
    sql: ${TABLE}.restock_pct ;;
  }

  dimension: ret_auth_num {
    type: string
    sql: ${TABLE}.ret_auth_num ;;
  }

  dimension: rtv_order_no {
    primary_key: yes
    type: number
    sql: ${TABLE}.rtv_order_no ;;
  }

  dimension: ship_to_add_1 {
    type: string
    sql: ${TABLE}.ship_to_add_1 ;;
  }

  dimension: ship_to_add_2 {
    type: string
    sql: ${TABLE}.ship_to_add_2 ;;
  }

  dimension: ship_to_add_3 {
    type: string
    sql: ${TABLE}.ship_to_add_3 ;;
  }

  dimension: ship_to_city {
    type: string
    sql: ${TABLE}.ship_to_city ;;
  }

  dimension: ship_to_country_id {
    type: string
    sql: ${TABLE}.ship_to_country_id ;;
  }

  dimension: ship_to_jurisdiction_code {
    type: string
    sql: ${TABLE}.ship_to_jurisdiction_code ;;
  }

  dimension: ship_to_pcode {
    type: string
    sql: ${TABLE}.ship_to_pcode ;;
  }

  dimension: state {
    type: string
    sql: ${TABLE}.state ;;
  }

  dimension: status_ind {
    type: number
    sql: ${TABLE}.status_ind ;;
  }

  dimension: store {
    type: number
    sql: ${TABLE}.store;;
  }

  dimension: supplier {
    type: number
    sql: ${TABLE}.supplier ;;
  }

  dimension: total_order_amt {
    type: number
    hidden: yes
    sql: ${TABLE}.total_order_amt ;;
  }

  dimension: wh {
    type: number
    sql: ${TABLE}.wh ;;
  }

  measure: RTV_Amount{
    label: "RTV Amount"
    description: "RTV Amount"
    type: sum
    sql: ${total_order_amt} ;;
  }
}
