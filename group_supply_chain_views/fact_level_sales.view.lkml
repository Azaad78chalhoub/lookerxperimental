include: "../_common/period_over_period.view"
include: "ch_commission_valid_data.view"

view: fact_level_sales {
  label: "Level Shoes Sales"
  derived_table: {
    sql_trigger_value: SELECT FLOOR(((TIMESTAMP_DIFF(CURRENT_TIMESTAMP(),'1970-01-01 00:00:00',SECOND)) - 60*60*5)/(60*60*24)) ;;

    sql: SELECT
  sales.* EXCEPT (av_cost_usd, av_cost_local),
  IFNULL(stock.unit_cost_usd, 0) AS av_cost_usd,
  IFNULL(unit_cost,0) AS av_cost_local,
  dims.class,
  dims.dept_no,
  traits.report_code AS zone,
  traits.food_stamp_ind AS consigment_flag,
  (CASE WHEN traits.food_stamp_ind = "Y" THEN TRUE ELSE FALSE END) AS consigment,
  (CASE WHEN concession_store IS NOT NULL AND concessionstore_name != "Non-Chalhoub" THEN TRUE ELSE FALSE END) as concession
FROM
  `chb-prod-supplychain-data.prod_supply_chain.factretailsales` sales
      LEFT JOIN
      `chb-prod-supplychain-data.prod_supply_chain.dm_soh_sales` stock
      ON
        stock_date=atr_trandate
        AND (CASE WHEN bk_storeid = 8003 THEN 8001 ELSE bk_storeid END) = CAST(org_num AS NUMERIC)
        AND item=bk_productid
LEFT JOIN
  `chb-prod-supplychain-data.prod_shared_dimensions.dim_retail_product` dims
ON
  dims.item=sales.bk_productid

------------------------ BR 76 fixes starts--------------------------
/* LEFT JOIN `chb-prod-supplychain-data.prod_shared_dimensions.dim_ch_concession` concession
ON
dims.class=concession.class
AND
dims.dept_no=concession.dept
AND
sales.bk_storeid=concession.store */

LEFT JOIN `chb-prod-supplychain-data.prod_shared_dimensions.dim_prod_org_concession` concession
ON
dims.item =concession.prod_num
AND
cast(sales.bk_storeid as string) =concession.org_num

------------------------ BR 76 fixes ends--------------------------

LEFT JOIN `chb-prod-supplychain-data.prod_shared_dimensions.dim_item_loc_traits` traits
ON traits.item=bk_productid
AND
traits.loc = bk_storeid
LEFT JOIN `chb-prod-supplychain-data.prod_shared_dimensions.dim_retail_loc` locs
ON locs.store=sales.bk_storeid
  WHERE
  atr_trandate > "2020-01-01"
  AND bk_storeid IN (8001,
    8003,
    8005)
   AND (stock_date >= "2020-01-01" OR stock_date IS NULL ) AND stock.org_num IN ("8001","8003","8005")
  AND (CASE WHEN traits.food_stamp_ind = "Y" THEN TRUE ELSE FALSE END) IS FALSE
  AND (CASE WHEN concession_store IS NOT NULL AND concessionstore_name != "Non-Chalhoub" THEN TRUE ELSE FALSE END) IS FALSE
    ;;

  }

#   sql_table_name: `chb-prod-supplychain-data.prod_supply_chain.factretailsales` ;;

  extends: [period_over_period]

  drill_fields: [id]

#   filter: customer_purchase_date {
#     description: "Choose the date range that defines the period for the date filtered measures"
#     type: date
#   }

##################################
#####       EXTRA DIMS      #####
##################################

  dimension: class {
    type: number
    hidden: yes
    sql: ${TABLE}.class ;;
  }

  parameter: exclude_consignment {
    type: string
    allowed_value: {
      label: "Yes"
      value: "yes_exclude"
    }
    allowed_value: {
      label: "No"
      value: "no_keep"
    }
  }

  parameter: exclude_concession {
    type: string
    allowed_value: {
      label: "Yes"
      value: "yes_exclude"
    }
    allowed_value: {
      label: "No"
      value: "no_keep"
    }
  }



  dimension: dept_no {
    type: number
    hidden: yes
    sql: ${TABLE}.dept_no ;;
  }


  dimension: consignment_flag
  {
    type: string
    hidden: yes

    sql: ${TABLE}.food_stamp_ind ;;
  }


  dimension: report_code {
    type: string
    hidden: yes
    view_label: "Location Traits"
    label: "Zone"
    description: "Report Code value"
    sql: ${TABLE}.zone ;;
  }

  dimension: consignment
  {
    type: yesno
    view_label: "Location Traits"
    label: "Is Consignment?"
    sql:  ${TABLE}.consigment  ;;
  }

  dimension: concession
  {
    type: yesno
    view_label: "Location Traits"
    label: "Is Concession?"
    sql:  ${TABLE}.concession  ;;
  }



##################################
#####      DIMENSIONS       #####
##################################


  parameter: customer_granularity_parameter {
    label: "Brand / Group"
    view_label: "Muse Customer Information"
    description: "Choose the granularity you need the customer table to be"
    type: unquoted
    default_value: "overall"
    allowed_value: {
      label: "Group"
      value: "overall"
    }
    allowed_value: {
      label: "Brand"
      value: "brand"
    }
  }

  dimension: customer_granularity {
    hidden: yes
    type: string
    sql:
    {% if customer_granularity_parameter._parameter_value == 'overall' %}
      true
    {% elsif customer_granularity_parameter._parameter_value == 'brand' %}
      ${dim_alternate_bu_hierarchy.brand}
    {% else %}
      true
    {% endif %}
    ;;
#     label: "@{granularity_dimension_label}"
    }

    dimension: id {
      primary_key: yes
      type: number
      hidden: yes
      sql: ${TABLE}.id ;;
    }

#   dimension: customer_fact_key {
#     type: string
#     sql: CONCAT(CAST(${atr_muse_id} AS STRING), CAST(${customer_granularity} AS STRING)) ;;
#   }

    dimension: muse_customer_fact_key {
      type: string
      sql: CONCAT(CAST(${atr_muse_id} AS STRING), CAST(${customer_granularity} AS STRING)) ;;
    }

    dimension_group: pop_no_tz {
      sql: ${business_raw} ;;
    }

    dimension: transaction_id {
      type: string
      hidden: no
      sql: CONCAT(${atr_cginvoiceno}, ${bk_storeid}) ;;
    }

    dimension: month_no {
      type: string
      hidden:yes
      sql: EXTRACT(MONTH FROM DATE ${business_raw}) ;;
    }

    dimension: month_name {
      case: {
        when: {
          sql: ${month_no}= 1 ;;
          label: "January"
        }
        when: {
          sql: ${month_no}= 2 ;;
          label: "February"
        }
        when: {
          sql: ${month_no}= 3 ;;
          label: "March"
        }
        when: {
          sql: ${month_no}= 4 ;;
          label: "April"
        }
        when: {
          sql: ${month_no}= 5 ;;
          label: "May"
        }
        when: {
          sql: ${month_no}= 6 ;;
          label: "June"
        }
        when: {
          sql: ${month_no}= 7 ;;
          label: "July"
        }
        when: {
          sql: ${month_no}= 8 ;;
          label: "August"
        }
        when: {
          sql: ${month_no}= 9 ;;
          label: "September"
        }
        when: {
          sql: ${month_no}= 10 ;;
          label: "October"
        }
        when: {
          sql: ${month_no}= 11 ;;
          label: "November"
        }
        when: {
          sql: ${month_no}= 12 ;;
          label: "December"
        }
      }
    }


    dimension_group: business{
      type: time
      timeframes: [
        raw,
        time,
        date,
        week,
        month,
        quarter,
        year
      ]
      sql: TIMESTAMP(PARSE_DATE('%Y%m%d', CAST(${TABLE}.bk_businessdate AS STRING))) ;;
    }

    dimension: business_week_no{
      type: number
      description: "Week No. extracted from the business date"
      sql: EXTRACT( WEEK FROM(PARSE_DATE('%Y%m%d', CAST(${TABLE}.bk_businessdate AS STRING)))) ;;
    }

    dimension: business_month_no{
      type: number
      description: "Month No. extracted from the business date"
      sql: EXTRACT( MONTH FROM(PARSE_DATE('%Y%m%d', CAST(${TABLE}.bk_businessdate AS STRING)))) ;;
    }


    dimension: _fivetran_deleted {
      type: yesno
      hidden: yes
      sql: ${TABLE}._fivetran_deleted ;;
    }



    dimension_group: _fivetran_synced {
      type: time
      hidden: yes
      timeframes: [
        raw,
        time,
        date,
        week,
        month,
        quarter,
        year
      ]
      sql: ${TABLE}._fivetran_synced ;;
    }

    dimension: amountlocal_beforetax {
      type: number
      hidden: yes
      sql: ${TABLE}.amountlocal_beforetax ;;
    }

    dimension: amountusd_beforetax {
      type: number
      hidden: yes
      sql: ${TABLE}.amountusd_beforetax ;;
    }

    dimension: atr_action {
      type: string
      hidden: yes
      sql: ${TABLE}.atr_action ;;
    }

    dimension: atr_activitytype {
      type: string
      hidden: yes
      sql: ${TABLE}.atr_activitytype ;;
    }

    dimension: atr_batchid {
      type: string
      hidden: yes
      sql: ${TABLE}.atr_batchid ;;
    }

    dimension: atr_cginvoiceno {
      type: string
      hidden: no
      label: "Invoice Number"
      sql: ${TABLE}.atr_cginvoiceno ;;
    }

    dimension: atr_cginvoicenoline {
      type: string
      hidden: yes
      sql: ${TABLE}.atr_cginvoicenoline ;;
    }

    dimension_group: atr_createdate {
      type: time
      hidden: yes
      label: "CreateDate Timestamp"
      timeframes: [
        raw,
        time,
        date,
        week,
        month,
        quarter,
        year
      ]
      sql: ${TABLE}.atr_createdate ;;
    }

    dimension: atr_day {
      type: number
      hidden: yes
      sql: ${TABLE}.atr_day ;;
    }

    dimension_group: atr_documentdate {
      type: time
      hidden: yes
      timeframes: [
        raw,
        time,
        date,
        week,
        month,
        quarter,
        year
      ]
      sql: ${TABLE}.atr_documentdate ;;
    }

    dimension: atr_errordesc {
      type: string
      hidden: yes
      sql: ${TABLE}.atr_errordesc ;;
    }

    dimension: atr_homecurrency {
      type: string
      hidden: yes
      sql: ${TABLE}.atr_homecurrency ;;
    }

    dimension: atr_itemseqno {
      type: number
      hidden: yes
      sql: ${TABLE}.atr_itemseqno ;;
    }

    dimension: atr_itemstatus {
      type: string
      hidden: yes
      sql: ${TABLE}.atr_itemstatus ;;
    }

    dimension: atr_membershipid {
      type: string
      hidden: yes
      sql: ${TABLE}.atr_membershipid ;;
    }

    dimension: atr_muse_id {
      view_label: "Muse Customer Information"
#     group_label: "ID Information"
      label: "-- Muse Customer ID"
      type: string
#     hidden: yes
      sql: ${TABLE}.atr_muse_id ;;
    }

    dimension: atr_promotionid {
      type: number
      hidden: yes
      value_format_name: id
      sql: ${TABLE}.atr_promotionid ;;
    }

    dimension: atr_promotionid2 {
      type: number
      hidden: yes
      sql: ${TABLE}.atr_promotionid2 ;;
    }

    dimension: atr_salesperson {
      type: string
      hidden: yes
      sql: ${TABLE}.atr_salesperson ;;
    }

    dimension: atr_stagestatus {
      type: string
      hidden: yes
      sql: ${TABLE}.atr_stagestatus ;;
    }

    dimension: atr_status {
      type: string
      hidden: yes
      sql: ${TABLE}.atr_status ;;
    }

    dimension: atr_storedayseqno {
      type: number
      hidden: yes
      sql: ${TABLE}.atr_storedayseqno ;;
    }

    dimension: atr_time {
      type: number
      hidden: yes
      sql: ${TABLE}.atr_time ;;
    }

    dimension: atr_timezone {
      type: string
      hidden: yes
      sql: ${TABLE}.atr_timezone ;;
    }

    dimension_group: atr_trandate {
      type: time
      label: "Transaction Timestamp"
      # hidden: yes
      timeframes: [
        raw,
        time,
        date,
        week,
        month,
        quarter,
        year
      ]
      sql: CAST(${TABLE}.atr_trandate AS TIMESTAMP) ;;
    }

    measure: last_updated_date {
      type: date
      label: "Last refresh date"
      sql: MAX(${atr_trandate_raw}) ;;
      convert_tz: no
    }


    measure: first_atr_trandate {
      hidden: yes
      type: date
      sql: min(CAST(fact_retail_sales.atr_trandate  AS DATE)) ;;
    }

    measure: last_atr_trandate {
      hidden: yes
      type: date
      sql: max(CAST(fact_retail_sales.atr_trandate  AS DATE)) ;;
    }

    dimension: atr_tranno {
      type: number
      hidden: yes
      sql: ${TABLE}.atr_tranno ;;
    }


    dimension: atr_transeqno {
      type: number
      hidden: yes
      sql: ${TABLE}.atr_transeqno ;;
    }

    dimension: atr_trantype {
      type: string
      hidden: yes
      sql: ${TABLE}.atr_trantype ;;
    }

    dimension: is_refund {
      type: yesno
      hidden: no
      sql: ${TABLE}.mea_amountusd < 0 ;;
    }

    dimension_group: atr_updatedate {
      type: time
      hidden: yes
      timeframes: [
        raw,
        time,
        date,
        week,
        month,
        quarter,
        year
      ]
      sql: ${TABLE}.atr_updatedate ;;
    }

    dimension: av_cost_local {
      type: number
      hidden: yes
      sql: ${TABLE}.av_cost_local ;;
    }

    dimension: av_cost_usd {
      type: number
      hidden: yes
      sql: ${TABLE}.av_cost_usd ;;
    }

    dimension: bk_brand {
      type: number
      hidden: yes
      sql: ${TABLE}.bk_brand ;;
    }

    dimension: bk_businessdate {
      type: number
      hidden: yes
      sql: ${TABLE}.bk_businessdate;;
    }

    dimension: bk_businessmonth {
      type: string
      hidden: yes
      sql: REGEXP_EXTRACT(CAST(${bk_businessdate} AS STRING), r"(.{6})") as bk_month  ;;
    }

    dimension: md_tran_id {
      type: string
      hidden:  yes
      sql: CASE WHEN ${discountamt_usd} > 0 THEN CONCAT(${atr_cginvoiceno}, ${bk_storeid})
        ELSE null END;;
    }

    dimension: fp_tran_id {
      type: string
      hidden:  yes
      sql: CASE WHEN ${discountamt_usd} IS NULL THEN CONCAT(${atr_cginvoiceno}, ${bk_storeid})
            WHEN ${discountamt_usd} = 0  THEN CONCAT(${atr_cginvoiceno}, ${bk_storeid})
            ELSE null END;;
    }

    dimension: bk_customer {
      group_label: "ID Information"
      label: "CRM Customer ID"
      type: string
      hidden: yes
      sql: ${TABLE}.bk_customer ;;
    }

    dimension: bk_loyalty {
      type: string
      hidden: yes

      sql: ${TABLE}.bk_loyalty ;;
    }

    dimension: bk_loyaltyprogram {
      type: string
      hidden: yes
      sql: ${TABLE}.bk_loyaltyprogram ;;
    }

    dimension: bk_productid {
      type: string
      label: "Product ID"
      view_label: "Products"
      sql: ${TABLE}.bk_productid ;;
    }

    dimension: bk_salestype {
      type: number
      hidden: yes
      sql: ${TABLE}.bk_salestype ;;
    }

    dimension: bk_storeid {
      type: number
      hidden: yes
      value_format_name: id
      sql: ${TABLE}.bk_storeid ;;
    }

    dimension: location_number {
      type: string
      hidden: no
      label: "Location Number"
      value_format_name: id
      sql: CAST(${TABLE}.bk_storeid as STRING) ;;
    }

    dimension: bk_top20 {
      type: string
      hidden: yes
      sql: ${TABLE}.bk_top20 ;;
    }

    dimension_group: creationdatetime {
      type: time
      hidden: yes
      label: "Creation Timestamp"
      timeframes: [
        raw,
        time,
        date,
        week,
        month,
        quarter,
        year
      ]
      sql: ${TABLE}.creationdatetime ;;
    }

    dimension: customer_id {
#     view_label: "Customer Information"
#     group_label: "ID Information"
    type: string
    sql: CONCAT(ifnull(${atr_muse_id},'no-muse-id'), '-', ifnull(${bk_customer},'no-crm-id')) ;;
  }

  dimension: customer_type {
    hidden: yes
#     view_label: "Customer Information"
    group_label: "Customer Type Information"
    type: string
    sql:
    CASE
      WHEN ${atr_muse_id} IS NOT NULL AND ${bk_customer} IS NOT NULL AND ${bk_customer} NOT LIKE 'UNK%'
        THEN 'Muse and CRM'
      WHEN ${atr_muse_id} IS NOT NULL AND ${bk_customer} IS NOT NULL AND ${bk_customer} LIKE 'UNK%'
        THEN 'Muse'
      WHEN ${bk_customer} IS NOT NULL AND ${bk_customer} NOT LIKE 'UNK%'
        THEN 'CRM'
      WHEN ${atr_muse_id} IS NOT NULL
        THEN 'Muse'
      ELSE
        'Unkown'
      END
    ;;
  }

  dimension: is_muse {
  type: yesno
  sql: ${atr_muse_id} IS NOT NULL ;;
}

dimension: is_crm {
#     view_label: "Customer Information"
group_label: "Customer Type Information"
type: yesno
hidden: yes
sql: ${bk_customer} IS NOT NULL AND ${bk_customer} NOT LIKE 'UNK%' ;;
}


dimension: customer_golden_id {
  type: string
  hidden: yes
  sql: ${TABLE}.customer_golden_id ;;
}


dimension: discountamt_local {
  type: number
  hidden: yes
  sql: ${TABLE}.discountamt_local ;;
}

dimension: discountamt_usd {
  type: number
  hidden: yes
  sql: ${TABLE}.discountamt_usd ;;
}

dimension: sales_tran {
  type: yesno
  label: "Sales Transaction"
  sql: ${TABLE}.discountamt_usd IS NOT NULL and ${TABLE}.discountamt_usd <> 0;;
}

dimension: mea_amountlocal {
  type: number
  hidden: yes
  sql: ${TABLE}.mea_amountlocal ;;
}

dimension: mea_amountusd {
  type: number
  hidden: yes
  sql: ${TABLE}.mea_amountusd ;;
}

dimension: mea_quantity {
  type: number
  hidden: yes
  sql: ${TABLE}.mea_quantity ;;
}

dimension: pos_customer_id {
  type: string
  hidden: yes
  sql: ${TABLE}.pos_customer_id ;;
}

dimension: taxamt_local {
  type: number
  hidden: yes
  sql: ${TABLE}.taxamt_local ;;
}

dimension: taxamt_usd {
  type: number
  hidden: yes
  sql: ${TABLE}.taxamt_usd ;;
}

dimension: gender_brand_specific {
  group_label: "Products"
  description: "Gender with Kids, Newborn, Infant, Toddler, Layette under Kids"
  label: "Gender"
  type: string
  sql: CASE WHEN ${dim_retail_product.gender}  LIKE "ANIMAL" THEN "ANIMAL" WHEN ${dim_retail_product.gender}  LIKE "MEN" THEN "MEN" WHEN ${dim_retail_product.gender}  LIKE "WOMEN" THEN "WOMEN" WHEN ${dim_retail_product.gender} LIKE "UNISEX" THEN "UNISEX" WHEN ${dim_retail_product.gender}  LIKE "NEUTRAL" THEN "NEUTRAL" WHEN ${dim_retail_product.gender}  LIKE "%KIDS%" or  ${dim_retail_product.gender} LIKE "%INFANT%"   or ${dim_retail_product.gender} LIKE "%TODDLER%" or  ${dim_retail_product.gender} LIKE "LAYETTE" or  ${dim_retail_product.gender} LIKE "NEWBORN" THEN "KIDS" WHEN ${dim_retail_product.gender} is Null  THEN "UNSPECIFIED" END;;
}

dimension: unit_retail_usd {
  type: number
  value_format_name: usd
  group_label: "Products"
  label: "Retail Selling Price USD"
  description: "Unit Retail price in USD. Only exists in the datamart as of April' 2020."
  hidden: no
  sql: ${TABLE}.unit_retail_usd;;
}
dimension: unit_retail_lc {
  type: number
  value_format_name: decimal_2
  group_label: "Products"
  label: "Retail Selling Price Local Currency"
  description: "Unit Retail price in Local Currency. Only exists in the datamart as of April' 2020."
  hidden: no
  sql: ${TABLE}.unit_retail_local;;
}
measure: average_retail_selling_price_local {
  group_label: "Sales Metrics"
  description: "The average of Unit Retail price in local currency. Only exists in the datamart as of April' 2020."
  type: average
  hidden:no
  sql: ${unit_retail_lc} ;;
  value_format_name: decimal_2
}

measure: markdown_investment {
  type: sum
  label: "Markdown Investment USD"
  description: "Total discount given in USD"
  group_label: "Markdown metrics"
  value_format_name: usd
  sql: ${discountamt_usd};;
}

measure: markdown_investment_lc {
  type: sum
  label: "Markdown Investment Local"
  description: "Total discount given in Local currency"
  group_label: "Markdown metrics"
  value_format_name: decimal_2
  sql: ${discountamt_local};;
}


measure: average_retail_selling_price_usd {
  group_label: "Sales Metrics"
  description: "The average of Unit Retail price in USD. Only exists in the datamart as of April' 2020."
  type: average
  hidden:no
  sql: ${unit_retail_usd} ;;
  value_format_name: usd
}

dimension: rsp_buckets {
  group_label: "Products"
  description: "Segregation of Retail Selling Price into buckets. Only exists in the datamart as of April' 2020."
  label: "RSP Buckets"
  type: string
  sql: CASE WHEN ${unit_retail_usd} >= 0 AND ${unit_retail_usd} < 100 THEN '0 to 99'
              WHEN ${unit_retail_usd} >= 100 AND ${unit_retail_usd} < 200 THEN '100 to 199'
              WHEN ${unit_retail_usd}>= 200 AND ${unit_retail_usd}< 300 THEN '200 to 299'
              WHEN ${unit_retail_usd}>= 300 AND ${unit_retail_usd} < 400 THEN '300 to 399'
              WHEN ${unit_retail_usd} >= 400 AND ${unit_retail_usd} < 500 THEN '400 to 499'
              WHEN ${unit_retail_usd} >= 500 AND ${unit_retail_usd} < 600 THEN '500 to 599'
              WHEN ${unit_retail_usd} >= 600 AND ${unit_retail_usd} < 700 THEN '600 to 699'
              WHEN ${unit_retail_usd} >= 700 AND ${unit_retail_usd} < 800 THEN '700 to 799'
              WHEN ${unit_retail_usd} >= 800 AND ${unit_retail_usd} < 900 THEN '800 to 899'
              WHEN ${unit_retail_usd} >= 900 AND ${unit_retail_usd} < 1000 THEN '900 to 999'
              WHEN ${unit_retail_usd} >= 1000 AND ${unit_retail_usd} < 1500 THEN '1000 to 1499'
              WHEN ${unit_retail_usd} >= 1500 THEN '>= 1500'
              ELSE 'Undefined'
END;;
}
#   measure: max_markdown_investment {
#     type: sum
#     label: "Max Markdown Investment USD"
#     value_format_name: usd
#     sql: ${max_discount_amount_usd};;
#   }



dimension: count_markdown_items {
  type: number
  hidden: yes
  sql:  CASE WHEN ${discountamt_usd} <> 0 THEN ${mea_quantity} ELSE 0 END;;
}

dimension: markdown_transaction {
  type: yesno
  hidden: no
  sql:  ${discountamt_usd} <> 0 ;;
}


measure: markdown_depth {
  type: number
  hidden: no
  label: "Markdown Depth %"
  group_label: "Markdown metrics"
  description: "Total discount given divided by the original selling price (before the discount)"
  value_format_name: percent_2
  sql: SUM(${discountamt_usd})/NULLIF(SUM(${mea_amountusd}+${discountamt_usd}),0) ;;
}

measure: markdown_transactions {
  group_label: "Transaction metrics"
  description: "Distinct number of transactions with a discount"
  type: count_distinct
  value_format_name: decimal_0
  sql: ${md_tran_id} ;;
}

measure: full_price_transactions {
  group_label: "Transaction metrics"
  description: "Distinct number of transactions at full price(no discount)"
  type: count_distinct
  value_format_name: decimal_0
  sql: ${fp_tran_id};;
}


measure: share_of_markdown_transactions {
  group_label: "Transaction metrics"
  description: "Percentage of transactions with a discount vs total number of transactions"
  type: number
  value_format_name: percent_2
  sql: COUNT(DISTINCT ${md_tran_id})/COUNT (DISTINCT CONCAT(${atr_cginvoiceno}, ${bk_storeid}));;
}

measure: share_of_markdown_items {
  group_label: "Markdown metrics"
  description: "Percentage of total number of items sold a discount vs total number of items sold"
  type: number
  value_format_name: percent_2
  sql: SUM(${count_markdown_items})/ NULLIF(SUM(${TABLE}.mea_quantity),0) ;;
}



measure: cross_brand_adoption {
  hidden: yes
  description: "The number of brands that single customer has shopped in."
  type: count_distinct
  sql: ${dim_retail_loc.district_name} ;;
}

measure: transaction_count {
  type: count_distinct
  group_label: "Transaction metrics"
  description: "Total number of transactions"
  value_format_name: decimal_0
  sql: ${transaction_id};;
  drill_fields: [dim_retail_product.vpn, transaction_count]
}

measure: total_muse_mall_visits {
  label: "Visits per Mall"
  type: count_distinct
  hidden: yes
  sql: CONCAT(${atr_muse_id}, CAST(${atr_trandate_time} AS STRING), ${dim_retail_loc.mall_name}) ;;
}

measure: total_muse_brand_visits {
  label: "Visits per Brand"
  type: count_distinct
  hidden: yes
  sql: CONCAT(${atr_muse_id}, CAST(${atr_trandate_time} AS STRING), ${dim_retail_loc.mall_name}, ${dim_alternate_bu_hierarchy.brand}) ;;
}

measure: muse_transaction_count {
  type: count_distinct
  group_label: "Transaction metrics"
  value_format_name: decimal_0
  sql: ${transaction_id};;
  filters: [is_muse: "yes"]
}




measure: cogs_usd {
  type: sum
  hidden: no
  group_label: "Cost of Goods Sold"
  label: "Cost of Goods Sold USD"
  description: "Average unit cost (in USD) multiplied by quantity sold"
  sql:ABS(${av_cost_usd})*${mea_quantity} ;;
  value_format_name: usd
}

measure: cogs_local {
  type: sum
  hidden: no
  group_label: "Cost of Goods Sold"
  label: "Cost of Goods Sold Local"
  description: "Average unit cost (in local currency) multiplied by quantity sold"
  sql:ABS(${av_cost_local})*${mea_quantity} ;;
  value_format_name: usd
}

measure: fp_net_sales_usd {
  type :  sum
  hidden : yes
  filters: {
    field: markdown_transaction
    value: "no"
  }
  sql: ${amountusd_beforetax} ;;
  value_format_name: usd
}

measure: md_net_sales_usd {
  type :  sum
  hidden : yes
  filters: {
    field: markdown_transaction
    value: "yes"
  }
  sql: ${amountusd_beforetax} ;;
  value_format_name: usd
}



measure: fp_cogs_usd {
  type :  sum
  hidden : yes
  filters: {
    field: markdown_transaction
    value: "no"
  }
  sql: ${av_cost_usd}*${mea_quantity} ;;
  value_format_name: usd
}

measure: md_cogs_usd {
  type :  sum
  hidden : yes
  filters: {
    field: markdown_transaction
    value: "yes"
  }
  sql: ${av_cost_usd}*${mea_quantity} ;;
  value_format_name: usd
}

measure: gross_margin {
  type: number
  hidden: no
  label: "Gross Margin USD"
  group_label: "Gross Margin"
  description: "Net Sales (USD) minus Cost of Goods Sold (USD)"
  sql: ${net_sales_amount_usd} - ${cogs_usd} ;;
  value_format_name: usd
}

measure: gross_margin_share {
  type: number
  hidden: no
  label: "Gross Margin %"
  group_label: "Gross Margin"
  description: "Percentage of Gross Margin (USD) vs Net Sales (USD)"
  value_format_name: percent_2
  sql: SAFE_DIVIDE(${gross_margin},${net_sales_amount_usd}) ;;
}



measure: full_price_gross_margin {
  type: number
  hidden: yes
  label: "FP Gross Margin USD"
  sql: ${fp_net_sales_usd} - ${fp_cogs_usd} ;;
  value_format_name: usd
}

measure: md_price_gross_margin {
  type: number
  hidden: yes
  label: "MD Gross Margin USD"
  sql: ${md_net_sales_usd} -${md_cogs_usd} ;;
  value_format_name: usd
}

measure: gross_margin_local {
  type: number
  hidden: no
  label: "Gross Margin Local"
  description: "Net Sales (local currency) minus Cost of Goods Sold (local currency)"
  group_label: "Gross Margin"
  sql: ${net_sales_amount_local} -${cogs_local} ;;
  value_format_name: decimal_0
}

measure: total_net_sales_local {
  type: sum
  hidden: yes
  sql: ${amountlocal_beforetax} ;;
  value_format_name: decimal_2
}

measure: net_sales_amount_usd {
  description: "Gross Sales - Returns - Discounts, use with Customer ID for LTV"
  type: sum
  group_label: "Net Sales"
  value_format_name: usd
  sql: ${amountusd_beforetax} ;;
}

measure: net_sales_amount_local {
  description: "Gross Sales - Returns - Discounts, use with Customer ID for LTV"
  type: sum
  group_label: "Net Sales"
  value_format_name: decimal_0
  sql: ${amountlocal_beforetax} ;;
}

measure: running_net_sales_amount_usd {
  description: "Gross Sales - Returns - Discounts, use with Customer ID for LTV"
  group_label: "Net Sales"
  type: running_total
  value_format_name: usd
  sql: SUM(${amountusd_beforetax}) ;;
}

measure: customer_count {
  hidden: yes
#     view_label: "Customer Information"
  description: "The number of customers that have bought in filters applied"
  type: count_distinct
  sql: ${customer_id} ;;
}

measure: muse_customer_count {
  view_label: "Muse Customer Information"
  description: "The number of muse customers that have bought in filters applied"
  type: count_distinct
  sql: ${atr_muse_id} ;;
}

measure: frequency {
  hidden: yes
  view_label: "Customer Information"
  description: "Number of transactions / number of customers that purchased in applied filters"
  type: number
  value_format_name: decimal_2
  sql: 1.0 * ${transaction_count} / NULLIF(${customer_count},0) ;;
}

measure: muse_frequency {
  view_label: "Muse Customer Information"
  description: "Number of transactions / number of customers that purchased in applied filters"
  type: number
  value_format_name: decimal_2
  sql: 1.0 * ${muse_transaction_count} / NULLIF(${muse_customer_count},0) ;;
}

measure: average_order_value_usd {
  description: "Net Sales / Transactions"
  type: number
  sql: 1.0 * ${net_sales_amount_usd} / NULLIF(${transaction_count},0) ;;
  value_format_name: usd
}

measure: quantity_sold {
  group_label: "Quantity metrics"
  description: "Aggregate Quantity of Units Sold"
  type: sum
  value_format_name: decimal_0
  sql: ${TABLE}.mea_quantity ;;
}

measure: UPT {
  label: "UPT"
  description: "Units Sold per transaction"
  group_label: "Transaction metrics"
  type: number
  value_format_name: decimal_2
  sql: ${quantity_sold}/${transaction_count} ;;
}

measure: VPT {
  label: "VPT"
  description: "Sales made per transaction"
  group_label: "Transaction metrics"
  type: number
  value_format_name: usd
  sql: ${net_sales_amount_usd}/${transaction_count} ;;
}

measure: markdown_quantity_sold {
  group_label: "Quantity metrics"
  description: "Aggregate Quantity of Units Sold at Discount"
  type: sum
  value_format_name: decimal_0
  sql: ${count_markdown_items} ;;
}

measure: NetSalesUSD_onFP{
  label: "Net Sales Amount USD on Full Price"
  description: "Net Sales (USD) made on items sold at full price (no discount)"
  group_label: "Net Sales"
  type: sum
  value_format_name: usd
  sql: CASE WHEN ${discountamt_usd} = 0 or  ${discountamt_usd} is NULL THEN (${amountusd_beforetax})
    ELSE null END ;;
}

measure: NetSalesUSD_onMD{
  label: "Net Sales Amount USD on Markdown"
  description: "Net Sales (USD) made on items sold at discount"
  group_label: "Net Sales"
  type: sum
  value_format_name: usd
  sql: CASE WHEN ${discountamt_usd} <> 0 THEN (${amountusd_beforetax})
    ELSE null END ;;
}

measure: COGSUSD_onFP{
  group_label: "Cost of Goods Sold"
  description: "Cost (USD) of items sold at full price (no discount)"
  label: "COGS USD on Full Price"
  type: sum
  value_format_name: usd
  sql: CASE WHEN ${discountamt_usd} = 0 or  ${discountamt_usd} is NULL THEN ABS(${av_cost_usd})*${mea_quantity}
    ELSE null END ;;
}

measure: COGSUSD_onMD{
  group_label: "Cost of Goods Sold"
  description: "Cost (USD) of items sold at discount"
  label: "COGS USD on Markdown"
  type: sum
  value_format_name: usd
  sql: CASE WHEN ${discountamt_usd} <> 0 THEN ABS(${av_cost_usd})*${mea_quantity}
    ELSE null END ;;
}

measure: quantity_sold_onFP {
  group_label: "Quantity metrics"
  label: "Full Price Quantity Sold"
  description: "Aggregate Quantity of Units Sold on FP"
  type: sum
  value_format_name: decimal_0
  sql: CASE WHEN ${discountamt_usd} = 0 THEN (${mea_quantity})
    ELSE null END ;;
}

measure: GrossMarginUSD_onFP{
  label: "Gross Margin USD on Full Price"
  description: "Net Sales (USD) minus Cost of Goods Sold (USD) of items sold at full price"
  group_label: "Gross Margin"
  type: number
  value_format_name: usd
  sql: ${NetSalesUSD_onFP}-${COGSUSD_onFP}
    ;;
}

measure: GrossMarginUSD_onMD{
  label: "Gross Margin USD on Markdown"
  description: "Net Sales (USD) minus Cost of Goods Sold (USD) of items sold at a discount"
  group_label: "Gross Margin"
  type: number
  value_format_name: usd
  sql: ${NetSalesUSD_onMD}-${COGSUSD_onMD}
    ;;
}

measure: ReturnQauntity {
  group_label: "Quantity metrics"
  description: "Aggregate Quantity of Units Returned"
  type: sum
  label: "Returned Quantity"
  hidden: no
  sql:case when ${mea_quantity} <0 then ABS(${mea_quantity}) else null end;;
}

measure: TotalQuantity_beforeReturn{
  type: sum
  hidden: yes
  sql:case when ${mea_quantity} >=0 then ${mea_quantity} else null end  ;;
}

measure:ReturnRate  {
  label: "Return Rate"
  description: "Percentage of total number of items returned vs total number of items sold"
  type: number
  value_format_name: percent_2
  sql: SAFE_DIVIDE(cast(${ReturnQauntity} as INT64),cast(${TotalQuantity_beforeReturn} as INT64));;
}

measure: item_count {
  type: count_distinct
  label: "Count of Distinct Items"
  value_format_name: decimal_0
  sql: ${bk_productid} ;;
}


}
