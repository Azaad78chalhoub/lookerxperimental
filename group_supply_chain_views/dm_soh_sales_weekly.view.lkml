view: dm_soh_sales_weekly {
  sql_table_name: `chb-prod-supplychain-data.prod_supply_chain.dm_soh_sales_weekly`
    ;;

  dimension: arabic_desc {
    type: string
    label: "Arabic Description"
    sql: ${TABLE}.arabic_desc ;;
  }

  dimension: area {
    type: number
    label: "Area Code"
    sql: ${TABLE}.area ;;
  }

  dimension: area_name {
    type: string
    label: "Area Name"
    sql: ${TABLE}.area_name ;;
  }

  dimension: atr_homecurrency {
    type: string
    label: "Currency Code"
    sql: ${TABLE}.atr_homecurrency ;;
  }

  dimension: atrb_boy_girl {
    type: string
    label: "Gender"
    sql: ${TABLE}.atrb_boy_girl ;;
  }

  dimension: attrb_color {
    type: string
    label: "Colour"
    sql: ${TABLE}.attrb_color ;;
  }

  dimension: attrb_made_of {
    type: string
    label: "Made of"
    sql: ${TABLE}.attrb_made_of ;;
  }

  dimension: attrb_theme {
    type: string
    label: "Theme"
    sql: ${TABLE}.attrb_theme ;;
  }

  dimension: avg_last13weeks_cogs_amountusd {
    type: number
    hidden: yes
    sql: ${TABLE}.avg_last13weeks_cogs_amountusd ;;
  }

  dimension: avg_last26weeks_cogs_amountusd {
    type: number
    hidden: yes
    sql: ${TABLE}.avg_last26weeks_cogs_amountusd ;;
  }

  dimension: avg_last4weeks_cogs_amountusd {
    type: number
    hidden: yes
    sql: ${TABLE}.avg_last4weeks_cogs_amountusd ;;
  }

  dimension: avg_last52weeks_cogs_amountusd {
    type: number
    hidden: yes
    sql: ${TABLE}.avg_last52weeks_cogs_amountusd ;;
  }

  dimension: avg_last7days_cogs_amountusd {
    type: number
    hidden: yes
    sql: ${TABLE}.avg_last7days_cogs_amountusd ;;
  }

  dimension: barcode {
    type: string
    label: "Barcode"
    sql: ${TABLE}.barcode ;;
  }

  dimension: brand {
    type: string
    label: "Brand"
    sql: ${TABLE}.brand ;;
  }

  dimension: bu_dep_code {
    type: string
    label: "BU Department Code"
    sql: ${TABLE}.bu_dep_code ;;
  }

  dimension: bu_dep_desc {
    type: string
    label: "BU Department Description"
    sql: ${TABLE}.bu_dep_desc ;;
  }

  dimension: bu_desc {
    type: string
    label: "BU Description"
    sql: ${TABLE}.bu_desc ;;
  }

  dimension: business_unit {
    type: string
    label: "Business Unit"
    sql: ${TABLE}.business_unit ;;
  }

  dimension: business_unit_code {
    type: string
    label: "BU Code"
    sql: ${TABLE}.business_unit_code ;;
  }

  dimension: chain {
    type: number
    label: "Chain Code"
    sql: ${TABLE}.chain ;;
  }

  dimension: chain_name {
    type: string
    label: "Chain"
    sql: ${TABLE}.chain_name ;;
  }

  dimension: channel_id {
    type: number
    label: "Channel ID"
    sql: ${TABLE}.channel_id ;;
  }

  dimension: channel_name {
    type: string
    label: "Channel Name"
    sql: ${TABLE}.channel_name ;;
  }

  dimension: channel_type {
    type: string
    label: "Channel Type"
    sql: ${TABLE}.channel_type ;;
  }

  dimension: city {
    type: string
    label: "City"
    sql: ${TABLE}.city ;;
  }

  dimension: class {
    type: number
    label: "Class"
    sql: ${TABLE}.class ;;
  }

  dimension: class_name {
    type: string
    label: "Class Name"
    sql: ${TABLE}.class_name ;;
  }

  dimension: consignment_flag {
    type: string
    label: "Consignment Flag"
    sql: ${TABLE}.consignment_flag ;;
  }

  dimension: conversion_rate {
    type: number
    hidden: yes
    sql: ${TABLE}.CONVERSION_RATE ;;
  }

  dimension: country_desc {
    type: string
    label: "Country"
    sql: ${TABLE}.country_desc ;;
  }

  dimension: country_id {
    type: string
    label: "Country Code"
    sql: ${TABLE}.country_id ;;
  }

  dimension: country_code{
    group_label: "Locations"
    label: "Country Code"
    description: "Country code for location in Oracle RMS"
    type: string
    suggest_dimension: dim_retail_loc.country_code
    sql: CASE
        WHEN ${country_id} = "BH" THEN 'BAH'
        WHEN ${country_id} = "EG" THEN 'EGY'
        WHEN ${country_id} = "KW" THEN 'KWT'
        WHEN ${country_id} = "SA" THEN 'KSA'
        WHEN ${country_id} = "QA" THEN 'QAT'
        WHEN ${country_id} = "EF" THEN 'EGY'
        WHEN ${country_id} = "MA" THEN 'MAR'
        WHEN ${country_id} = "AE" THEN 'UAE'
        ELSE 'UAE'
        END ;;
  }


  dimension: country_of_manu {
    type: string
    label: "Country of Manufacturer"
    sql: ${TABLE}.country_of_manu ;;
  }

  dimension: day_of_week {
    type: number
    label: "Day of Week"
    sql: ${TABLE}.day_of_week ;;
  }

  dimension: dept_name {
    type: string
    label: "Department Name"
    sql: ${TABLE}.dept_name ;;
  }

  dimension: dept_no {
    type: number
    label: "Department No."
    sql: ${TABLE}.dept_no ;;
  }

  dimension: description {
    type: string
    label: "Region Descrption"
    sql: ${TABLE}.description ;;
  }

  dimension: dgr_ind {
    type: string
    hidden: yes
    sql: ${TABLE}.dgr_ind ;;
  }

  dimension: distr_sales_ind {
    type: number
    hidden: yes
    sql: ${TABLE}.distr_sales_ind ;;
  }

  dimension: district {
    type: number
    label: "District"
    sql: ${TABLE}.district ;;
  }

  dimension: district_name {
    type: string
    label: "District Name"
    sql: ${TABLE}.district_name ;;
  }

  dimension: division {
    type: string
    label: "Division"
    sql: ${TABLE}.division ;;
  }

  dimension: division_no {
    type: number
    label: "Division No."
    sql: ${TABLE}.division_no ;;
  }

  dimension: entity_type {
    type: string
    label: "Entity Type"
    sql: ${TABLE}.entity_type ;;
  }

  dimension_group: first_received {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.first_received ;;
  }

  dimension_group: first_sold {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.first_sold ;;
  }

  dimension: gender {
    type: string
    label: "Gender 2"
    sql: ${TABLE}.gender ;;
  }

  dimension: grey_mkt_ind {
    type: string
    hidden: yes
    sql: ${TABLE}.grey_mkt_ind ;;
  }

  dimension: group_name {
    type: string
    label: "Group Name"
    sql: ${TABLE}.group_name ;;
  }

  dimension: group_no {
    type: number
    label: "Group No."
    sql: ${TABLE}.group_no ;;
  }

  dimension: inv_soh_qty {
    type: number
    label: "SOH Quantity"
    sql: ${TABLE}.inv_soh_qty ;;
  }

  dimension: item {
    type: string
    label: "Item No."
    sql: ${TABLE}.item ;;
  }

  dimension: item_desc {
    type: string
    label: "Item Description"
    sql: ${TABLE}.item_desc ;;
  }

  dimension: item_style {
    type: string
    label: "Item Style"
    sql: ${TABLE}.item_style ;;
  }

  dimension: last13weeks_cogs_amountusd {
    type: number
    hidden: yes
    sql: ${TABLE}.last13weeks_cogs_amountusd ;;
  }

  dimension: last26weeks_cogs_amountusd {
    type: number
    hidden: yes
    sql: ${TABLE}.last26weeks_cogs_amountusd ;;
  }

  dimension: last4weeks_cogs_amountusd {
    type: number
    hidden: yes
    sql: ${TABLE}.last4weeks_cogs_amountusd ;;
  }

  dimension: last52weeks_cogs_amountusd {
    type: number
    hidden: yes
    sql: ${TABLE}.last52weeks_cogs_amountusd ;;
  }

  dimension: last7days_cogs_amountusd {
    type: number
    hidden: yes
    sql: ${TABLE}.last7days_cogs_amountusd ;;
  }

  dimension_group: last_update_datetime {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.last_update_datetime ;;
  }

  dimension: line {
    type: string
    label: "Line"
    sql: ${TABLE}.line ;;
  }

  dimension: loc_code {
    type: number
    label: "Location Code"
    sql: ${TABLE}.loc_code ;;
  }

  dimension: loc_name {
    type: string
    label: "Location Name"
    sql: ${TABLE}.loc_name ;;
  }

  dimension: loc_season {
    type: string
    label: "Location Season"
    sql: ${TABLE}.loc_season ;;
  }

  dimension: mall_name {
    type: string
    label: "Mall Name"
    sql: ${TABLE}.mall_name ;;
  }

  dimension: max_price_local_by_country_day {
    type: number
    label: "Max Price Local By Country"
    sql: ${TABLE}.max_price_local_by_country_day ;;
  }

  dimension: max_price_usd_by_country {
    type: number
    label: "Max Price USD By Country"
    sql: ${TABLE}.max_price_usd_by_country ;;
  }

  dimension: module {
    type: string
    label: "Module"
    sql: ${TABLE}.module ;;
  }

  dimension: month {
    type: number
    label: "Month"
    sql: ${TABLE}.month ;;
  }

  dimension: org_num {
    type: string
    hidden: yes
    sql: ${TABLE}.org_num ;;
  }

  dimension: ownership {
    type: string
    label: "Ownership"
    sql: ${TABLE}.ownership ;;
  }

  dimension: physical_wh {
    type: number
    label: "Physical Warehouse"
    sql: ${TABLE}.physical_wh ;;
  }

  dimension: previous_week_stock_qty {
    type: number
    label: "Previous Week Stock Quantity"
    sql: ${TABLE}.previous_week_stock_qty ;;
  }

  dimension: previous_week_unit_cost_usd {
    type: number
    label: "Previous Week Unit Cost USD"
    sql: ${TABLE}.previous_week_unit_cost_usd ;;
  }

  dimension: price_local {
    type: number
    label: "Price Local"
    sql: ${TABLE}.price_local ;;
  }

  dimension: prod_num {
    type: string
    label: "Prod No."
    sql: ${TABLE}.prod_num ;;
  }

  dimension: recurrence {
    type: string
    label: "Recurrence"
    sql: ${TABLE}.recurrence ;;
  }

  dimension: region {
    type: number
    label: "Region"
    sql: ${TABLE}.region ;;
  }

  dimension: region_name {
    type: string
    label: "Region"
    sql: ${TABLE}.region_name ;;
  }

  dimension: report_code {
    type: string
    label: "Report Code"
    sql: ${TABLE}.report_code ;;
  }

  dimension: sales_amountlocal_beforetax {
    type: number
    label: "Net Sales Amount Local"
    hidden: yes
    sql: ${TABLE}.sales_amountlocal_beforetax ;;
  }

  dimension: sales_amountusd_beforetax {
    type: number
    label: "Net Sales Amount Local"
    hidden: yes
    sql: ${TABLE}.sales_amountusd_beforetax ;;
  }

  dimension: sales_qty {
    type: number
    hidden: yes
    sql: ${TABLE}.sales_qty ;;
  }

  dimension: sap_item_code {
    type: string
    hidden: yes
    sql: ${TABLE}.sap_item_code ;;
  }

  dimension: season_desc {
    type: string
    label: "Season"
    sql: ${TABLE}.season_desc ;;
  }

  dimension: selling_square_ft {
    type: number
    label: "Selling Square Foot"
    sql: ${TABLE}.selling_square_ft ;;
  }

  dimension: sep_category {
    hidden: yes
    label: "SEP Category"
    sql: ${TABLE}.sep_category ;;
  }

  dimension: sep_market {
    type: string
    label: "SEP Market"
    sql: ${TABLE}.sep_market ;;
  }

  dimension: size_uda {
    type: string
    label: "Size UDA"
    sql: ${TABLE}.size_uda ;;
  }

  dimension: standard_uom {
    type: string
    label: "Standard UOM"
    sql: ${TABLE}.standard_uom ;;
  }

  dimension: state {
    type: string
    label: "State"
    sql: ${TABLE}.state ;;
  }

  dimension: status {
    type: string
    label: "Status"
    sql: ${TABLE}.status ;;
  }

  dimension_group: stock {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.stock_date ;;
  }

  dimension_group: store_close {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.store_close_date ;;
  }

  dimension: store_code {
    type: string
    sql: ${TABLE}.store_code ;;
  }

  dimension: store_format {
    type: number
    label: "Store Code"
    sql: ${TABLE}.store_format ;;
  }

  dimension_group: store_open {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.store_open_date ;;
  }

  dimension_group: week_start_date {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.week_start_date ;;
  }


  dimension: sub_line {
    type: string
    label: "Sub Line"
    sql: ${TABLE}.sub_line ;;
  }

  dimension: sub_vertical {
    type: string
    label: "Sub Vertical"
    sql: ${TABLE}.sub_vertical ;;
  }

  dimension: subclass {
    type: number
    label: "Sub Class"
    sql: ${TABLE}.subclass ;;
  }

  dimension: subclass_name {
    type: string
    label: "Subclass Name"
    sql: ${TABLE}.subclass_name ;;
  }

  dimension: sup_class {
    type: string
    label: "SUP Class"
    sql: ${TABLE}.sup_class ;;
  }

  dimension: sup_subclass {
    type: string
    label: "SUP Cubclass"
    sql: ${TABLE}.sup_subclass ;;
  }

  dimension: taxo_class {
    type: string
    label: "Taxonomy Class"
    sql: ${TABLE}.taxo_class ;;
  }

  dimension: taxo_subclass {
    type: string
    label: "Taxonomy Subclass"
    sql: ${TABLE}.taxo_subclass ;;
  }

  dimension: total_square_ft {
    type: number
    label: "Total Square Foot"
    sql: ${TABLE}.total_square_ft ;;
  }

  dimension: uda_16_attribute {
    type: string
    hidden: yes
    sql: ${TABLE}.uda_16_attribute ;;
  }

  dimension: uda_zone {
    type: string
    label: "Zone"
    sql: ${TABLE}.uda_zone ;;
  }

  dimension: unit_cost {
    type: number
    hidden: yes
    sql: ${TABLE}.unit_cost ;;
  }

  dimension: unit_cost_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.unit_cost_usd ;;
  }

  dimension: unit_retail_local {
    type: number
    hidden: yes
    sql: ${TABLE}.unit_retail_local ;;
  }

  dimension: unit_retail_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.unit_retail_usd ;;
  }

  dimension: unit_selling_price_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.unit_selling_price_usd ;;
  }

  dimension: usage_specificity {
    type: string
    hidden: yes
    sql: ${TABLE}.usage_specificity ;;
  }

  dimension: vertical {
    type: string
    label: "Vertical"
    sql: ${TABLE}.vertical ;;
  }

  dimension: vpn {
    type: string
    label: "VPN"
    sql: ${TABLE}.vpn ;;
  }

  dimension: week_number {
    type: number
    label: "Week Number"
    sql: ${TABLE}.week_number ;;
  }

  dimension: year {
    type: number
    label:"Year"
    sql: ${TABLE}.year ;;
  }

  measure: net_sales_local {
    type: sum
    label: "Net Sales Local"
    value_format_name: decimal_2
    sql:  ${sales_amountlocal_beforetax};;
  }

  measure: net_sales_usd {
    type: sum
    label: "Net Sales USD"
    value_format_name: usd
    sql:  ${sales_amountusd_beforetax};;
  }

  measure: avg_inv_soh_qty{
    type: average
    label: "Average SOH Quantity"
    value_format_name: decimal_0
    sql: ${inv_soh_qty} ;;
  }

  measure: avg_unit_cost_local{
    type: average
    label: "Average Unit Cost Local"
    value_format_name: decimal_2
    sql: ${unit_cost} ;;
  }

  measure: avg_unit_cost_usd{
    type: average
    value_format_name: usd
    label: "Average Unit Cost USD"
    sql: ${unit_cost_usd} ;;
  }


  measure: avg_retail_cost_local{
    type: average
    value_format_name: decimal_2
    label: "Average Retail Cost Local"
    sql: ${unit_retail_local} ;;
  }

  measure: avg_retail_cost_usd{
    type: average
    value_format_name: usd
    label: "Average Retail Cost USD"
    sql: ${unit_retail_usd} ;;
  }


  # measure: count {
  #   type: count
  #   hidden:yes
  #   drill_fields: [detail*]
  # }

  # ----- Sets of fields for drilling ------
  # set: detail {
  #   fields: [
  #     area_name,
  #     loc_name,
  #     group_name,
  #     region_name,
  #     class_name,
  #     chain_name,
  #     channel_name,
  #     subclass_name,
  #     mall_name,
  #     district_name,
  #     dept_name
  #   ]
  # }
}

# view: dm_soh_sales_weekly__sep_category {
#   dimension: item {
#     type: string
#     hidden:yes
#     sql: ${TABLE}.ITEM ;;
#   }

#   dimension: sep_category {
#     type: string
#     hidden:yes
#     sql: ${TABLE}.SEP_CATEGORY ;;
#   }

#   dimension: uda_value {
#     type: number
#     hidden:yes
#     sql: ${TABLE}.UDA_VALUE ;;
#   }
# }
