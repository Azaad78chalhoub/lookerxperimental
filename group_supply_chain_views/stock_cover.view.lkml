# If necessary, uncomment the line below to include explore_source.
# include: "group_supply_chain.model.lkml"

view: stock_cover {
  derived_table: {
    explore_source: fact_soh_sales_poc {
      column: stock_date {}
      column: org_num {}
      column: prod_num {}
      column: avg_last30days_cogs_amountusd {}
      column: avg_last90days_cogs_amountusd {}
      column: avg_last182days_cogs_amountusd {}
      column: avg_lastyear_cogs_amountusd {}
      column: cost_of_inventory {}
      derived_column: last_day_avg_last30days_cogs_amountusd {
        sql: LAST_VALUE(avg_last30days_cogs_amountusd) OVER(PARTITION BY org_num, prod_num ORDER BY stock_date ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING);;
      }
      derived_column: last_day_avg_last90days_cogs_amountusd {
        sql: LAST_VALUE(avg_last90days_cogs_amountusd) OVER(PARTITION BY org_num, prod_num ORDER BY stock_date ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING);;
      }
      derived_column: last_day_avg_last182days_cogs_amountusd {
        sql: LAST_VALUE(avg_last182days_cogs_amountusd) OVER(PARTITION BY org_num, prod_num ORDER BY stock_date ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING);;
      }
      derived_column: last_day_avg_lastyear_cogs_amountusd{
        sql: LAST_VALUE(avg_lastyear_cogs_amountusd) OVER(PARTITION BY org_num, prod_num ORDER BY stock_date ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING);;
      }
      derived_column: last_day_cost_of_inventory {
        sql: LAST_VALUE(cost_of_inventory) OVER(PARTITION BY org_num, prod_num ORDER BY stock_date ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING);;
      }

      bind_all_filters: yes
    }
  }
  dimension: stock_date {
    label: "Sales and Stock Information Stock Date"
    type: date
    hidden: yes
  }

  measure: count_of_days {
    type: number
    hidden: yes
    sql: COUNT(DISTINCT${stock_date}) ;;
  }
  dimension: org_num {
    hidden: yes
  }
  dimension: prod_num {
    hidden: yes
  }

  dimension: pk {
    hidden: yes
    primary_key: yes
    sql: CONCAT(${prod_num}, " - ", ${org_num}) ;;
  }

  dimension: last_day_avg_last30days_cogs_amountusd {
    hidden: yes
  }

  dimension: last_day_avg_last90days_cogs_amountusd {
    hidden: yes
  }

  dimension: last_day_avg_last182days_cogs_amountusd {
    hidden: yes
  }


  dimension: last_day_avg_lastyear_cogs_amountusd {
    hidden: yes
  }
  dimension: last_day_cost_of_inventory {
    hidden: yes
  }

  dimension: total_stock_cover_new_on_30_days {
    type: number
     hidden: yes
    value_format_name: decimal_0
    sql: IFNULL(SAFE_DIVIDE(SUM(${last_day_cost_of_inventory}),SUM(${last_day_avg_last30days_cogs_amountusd})),0) ;;
  }

  ## unhide the metrics below

  measure: stock_cover_new_on_30_days {
    type: number
  #  hidden: yes
    label: "Last Day Stock Cover on 1 Month"
    description: "Stock Cover on 1 Month of historical sales"
    group_label: "Stock Cover"
    value_format_name: decimal_0
    sql: IFNULL(SAFE_DIVIDE(SUM(${last_day_cost_of_inventory}),SUM(${last_day_avg_last30days_cogs_amountusd})),0) ;;
    html: {{ rendered_value }} Days ;;
  }

  measure: stock_cover_new_on_90_days {
    type: number
  #  hidden: yes
    label: "Last Day Stock Cover on 3 Months"
    description: "Stock Cover on 3 Months of historical sales"
    group_label: "Stock Cover"
        value_format_name: decimal_0
    sql: IFNULL(SAFE_DIVIDE(SUM(${last_day_cost_of_inventory}),SUM(${last_day_avg_last90days_cogs_amountusd})),0) ;;
    html: {{ rendered_value }} Days ;;
  }

  measure: stock_cover_new_on_182_days {
    type: number
  #  hidden: yes
    label: "Last Day Stock Cover on 6 Months"
    group_label: "Stock Cover"
    description: "Stock Cover on 6 Months of historical sales"
    value_format_name: decimal_0
    sql: IFNULL(SAFE_DIVIDE(SUM(${last_day_cost_of_inventory}),SUM(${last_day_avg_last182days_cogs_amountusd})),0) ;;
    html: {{ rendered_value }} Days ;;
  }

  measure: stock_cover_new_on_last_year {
    type: number
  #  hidden: yes
    label: "Last Day Stock Cover on 1 Year"
    group_label: "Stock Cover"
    description: "Stock Cover on 1 Year of historical sales"
    value_format_name: decimal_0
    sql: IFNULL(SAFE_DIVIDE(SUM(${last_day_cost_of_inventory}),SUM(${last_day_avg_lastyear_cogs_amountusd})),0) ;;
    html: {{ rendered_value }} Days ;;
  }
}
