view: ch_age_prov_stock {
  sql_table_name: `chb-prod-supplychain-data.prod_supply_chain.ch_age_prov_stock`
    ;;

  dimension: _fivetran_deleted {
    type: yesno
    hidden: yes
    sql: ${TABLE}._fivetran_deleted ;;
  }

  dimension: pk {
    primary_key: yes
    type: string
    hidden:yes
    sql: CONCAT(${item}, ' - ' ,${location}) ;;
  }

  dimension: _fivetran_id {
    type: string
    hidden: yes
    sql: ${TABLE}._fivetran_id ;;
  }

  dimension_group: _fivetran_synced {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}._fivetran_synced ;;
  }

  dimension: age {
    type: number
    sql: ${TABLE}.age ;;
  }

  dimension: item {
    type: string
    hidden: no
    sql: ${TABLE}.item ;;
  }

  dimension: location {
    type: string
    hidden: no
    sql: ${TABLE}.location ;;
  }

  dimension: original_quantity {
    type: number
    sql: ${TABLE}.original_quantity ;;
  }

  dimension: provision {
    type: number
    hidden: yes
    sql: ${TABLE}.provision ;;
  }


  dimension: provision_factor {
    type: number
    hidden: yes
    sql: CASE WHEN ${provision_factors.entity_type} = "EXPIRY (FASHION)" THEN 0.05
        WHEN ${provision_factors.entity_type} = "FASHION" THEN 0.05
        WHEN ${provision_factors.entity_type} = "BEAUTY" THEN 0.025
        WHEN ${provision_factors.entity_type} = "COMMON" THEN 1
        WHEN ${provision_factors.entity_type} = "GIFT" THEN 0.025
        ELSE 0
        END;;
  }

  measure: provision_value_usd {
    type: sum
    hidden: no
    value_format_name: usd
    sql: CASE WHEN ${age} <= 6 THEN 0
    WHEN ${age}  > 6 AND ${age}   = 46 THEN
    ${provision_factors.unit_cost_usd} * ${running_quantity}*((${age}-6)*${provision_factor})
    ELSE ${provision_factors.unit_cost_usd} * ${running_quantity}
    END;;
  }

  dimension: running_quantity {
    type: number
    sql: ${TABLE}.running_quantity ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
