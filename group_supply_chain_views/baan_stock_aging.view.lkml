view: baan_stock_aging {
  sql_table_name: `chb-prod-supplychain-data.prod_supply_chain.baan_stock_aging`
    ;;

  dimension: _1080_days {
    type: number
    hidden: yes
    sql: ${TABLE}._1080_days ;;
  }
  measure: sum_1080_days {
    type: sum
    group_label: "Quantity by Age"
    hidden: no
    label: ">= 36 and <48"
    sql: ifnull(${_1080_days},0) ;;
  }
  measure: sum_1080_days_value {
    type: sum
    group_label: "Value by Age USD"
    hidden: no
    label: ">= 36 and <48"
    value_format: "$#,##0.00"
    sql: cast(${TABLE}._1080_days as INT64)*cast(${TABLE}.avg_cost as FLOAT64)*1.41 ;;
  }
  measure: sum_1080_days_value_lc {
    type: sum
    group_label: "Value by Age in Local Currency"
    hidden: no
    label: ">= 36 and <48"
    value_format: "#,##0.00"
    sql: cast(${TABLE}._1080_days as INT64)*cast(${TABLE}.avg_cost as FLOAT64) ;;
  }
  dimension: pk {
    primary_key: yes
    type: string
    hidden:yes
    sql: CONCAT(${item}, ' - ' ,${sheet_name});;
  }
  dimension: _180_days {
    type: number
    hidden: yes
    sql: ${TABLE}._180_days ;;
  }
  measure: sum_180_days_value {
    type: sum
    group_label: "Value by Age USD"
    hidden: no
    label: ">= 6 and <12"
    value_format: "$#,##0.00"
    sql:cast(${TABLE}._180_days as INT64)*cast(${TABLE}.avg_cost as FLOAT64)*1.41  ;;
  }
  measure: sum_180_days_value_lc {
    type: sum
    group_label: "Value by Age in Local Currency"
    hidden: no
    label: ">= 6 and <12"
    value_format: "#,##0.00"
    sql:cast(${TABLE}._180_days as INT64)*cast(${TABLE}.avg_cost as FLOAT64)  ;;
  }
  measure: sum_180_days {
    type: sum
    group_label: "Quantity by Age"
    hidden: no
    label: ">= 6 and <12"
    sql: ${_180_days} ;;
  }
  dimension: _360_days {
    type: number
    hidden: yes
    sql: ${TABLE}._360_days ;;
  }
  measure: sum_360_days {
    type: sum
    group_label: "Quantity by Age"
    hidden: no
    label:">= 12 and <24"
    sql: ${_360_days} ;;
  }
  measure: sum_360_days_value {
    type: sum
    group_label: "Value by Age USD"
    hidden: no
    label:">= 12 and <24"
    value_format: "$#,##0.00"
    sql: cast(${TABLE}._360_days as INT64)*cast(${TABLE}.avg_cost as FLOAT64)*1.41  ;;
  }
  measure: sum_360_days_value_lc {
    type: sum
    group_label: "Value by Age in Local Currency"
    hidden: no
    label:">= 12 and <24"
    value_format: "#,##0.00"
    sql: cast(${TABLE}._360_days as INT64)*cast(${TABLE}.avg_cost as FLOAT64)  ;;
  }
  dimension: _60_days {
    type: number
    hidden: yes
    sql: ${TABLE}._60_days ;;
  }
  measure: sum_60_days {
    hidden: no
    group_label: "Quantity by Age"
    label: ">= 2 and <3"
    type: sum
    sql: ${_60_days} ;;
  }
  measure: sum_60_days_value {
    hidden: no
    group_label: "Value by Age USD"
    label: ">= 2 and <3"
    type: sum
    value_format: "$#,##0.00"
    sql: cast(${TABLE}._60_days as INT64)*cast(${TABLE}.avg_cost as FLOAT64)*1.41 ;;
  }
  measure: sum_60_days_value_lc {
    hidden: no
    group_label: "Value by Age in Local Currency"
    label: ">= 2 and <3"
    type: sum
    value_format: "#,##0.00"
    sql: cast(${TABLE}._60_days as INT64)*cast(${TABLE}.avg_cost as FLOAT64);;
  }
  dimension: _720_days {
    type: number
    hidden: yes
    sql: ${TABLE}._720_days ;;
  }
  measure: sum_720_days {
    type: sum
    group_label: "Quantity by Age"
    hidden: no
    label: ">= 24 and <36"
    sql: ${_720_days} ;;
  }
  measure: sum_720_days_value {
    type: sum
    group_label: "Value by Age USD"
    hidden: no
    label: ">= 24 and <36"
    value_format: "$#,##0.00"
    sql: cast(${TABLE}._720_days as INT64)*cast(${TABLE}.avg_cost as FLOAT64)*1.41 ;;
  }
  measure: sum_720_days_value_lc {
    type: sum
    group_label: "Value by Age in Local Currency"
    hidden: no
    label: ">= 24 and <36"
    value_format: "#,##0.00"
    sql: cast(${TABLE}._720_days as INT64)*cast(${TABLE}.avg_cost as FLOAT64) ;;
  }
  dimension: _90_days {
    type: number
    hidden: yes
    sql: ${TABLE}._90_days ;;
  }
  measure: sum_90_days {
    type: sum
    group_label: "Quantity by Age"
    hidden: no
    label: ">= 3 and <6"
    sql: ${_90_days} ;;
  }
  measure: sum_90_days_value {
    type: sum
    group_label: "Value by Age USD"
    hidden: no
    label: ">= 3 and <6"
    value_format: "$#,##0.00"
    sql: cast(${TABLE}._90_days as INT64)*cast(${TABLE}.avg_cost as FLOAT64)*1.41 ;;
  }
  measure: sum_90_days_value_lc {
    type: sum
    group_label: "Value by Age in Local Currency"
    hidden: no
    label: ">= 3 and <6"
    value_format: "#,##0.00"
    sql: cast(${TABLE}._90_days as INT64)*cast(${TABLE}.avg_cost as FLOAT64) ;;
  }
  dimension: avg_cost {
    type: string
    hidden: yes
    sql: ${TABLE}.avg_cost ;;
  }

  dimension: description {
    type: string
    hidden: yes
    sql: ${TABLE}.description ;;
  }

  dimension: item {
    type: string
    label: "VPN"
    sql: ${TABLE}.item ;;
  }

  dimension: Barcode {
    type: string
    label: "Barcode"
    sql: ${TABLE}.barcode ;;
  }

  dimension: lesser_than_60_days {
    type: number
    hidden: yes
    sql: ${TABLE}.lesser_than_60_days ;;
  }
  measure: sum_lesser_than_60_days {
    label: "< 2"
    hidden: no
    group_label: "Quantity by Age"
    type: sum
    sql: ${lesser_than_60_days};;
  }
  measure: sum_lesser_than_60_days_value {
    label: "< 2"
    hidden: no
    group_label: "Value by Age USD"
    type: sum
    value_format: "$#,##0.00"
    sql: cast(${TABLE}.lesser_than_60_days as INT64)*cast(${TABLE}.avg_cost as FLOAT64)*1.41;;
  }
  measure: sum_lesser_than_60_days_value_lc {
    label: "< 2"
    hidden: no
    group_label: "Value by Age in Local Currency"
    type: sum
    value_format: "#,##0.00"
    sql: cast(${TABLE}.lesser_than_60_days as INT64)*cast(${TABLE}.avg_cost as FLOAT64);;
  }
  dimension: more_than_1440_days {
    type: number
    hidden: yes
    sql: ${TABLE}.more_than_1440_days ;;
  }
  measure: sum_more_than_1440_days {
    type: sum
    group_label: "Quantity by Age"
    hidden: no
    label:  ">= 48"
    sql: ${more_than_1440_days} ;;
  }
  measure: sum_more_than_1440_days_value {
    type: sum
    group_label: "Value by Age USD"
    hidden: no
    label:  ">= 48"
    value_format: "$#,##0.00"
    sql: cast(${TABLE}.more_than_1440_days as INT64)*cast(${TABLE}.avg_cost as FLOAT64)*1.41 ;;
  }
  measure: sum_more_than_1440_days_value_lc {
    type: sum
    group_label: "Value by Age in Local Currency"
    hidden: no
    label:  ">= 48"
    value_format: "#,##0.00"
    sql: cast(${TABLE}.more_than_1440_days as INT64)*cast(${TABLE}.avg_cost as FLOAT64) ;;
  }
  dimension: on_hand {
    type: number
    hidden: yes
    sql: ${TABLE}.on_hand ;;
  }

  dimension: value {
    type: string
    hidden: yes
    sql: ${TABLE}.value ;;
  }
  dimension: sheet_name {
    type: string
    hidden: yes
    sql: ${TABLE}.sheet_name ;;
  }
  dimension: Location_Name{
    label: "Location Name"
    type: string
    sql: CASE WHEN ${TABLE}.sheet_name  LIKE "MK%" THEN "MICHAEL KORS"
          WHEN  ${TABLE}.sheet_name  LIKE "LA%"   THEN "LACOSTE"
          WHEN ${TABLE}.sheet_name LIKE "LO1%" or ${TABLE}.sheet_name LIKE "LO-Taj" THEN "LOCCITANE-Taj"
          WHEN ${TABLE}.sheet_name LIKE "LO3%" or ${TABLE}.sheet_name LIKE "LO-Abdali" THEN "LOCCITANE-Abdali"
           WHEN ${TABLE}.sheet_name LIKE "LO-Mecca" THEN "LOCCITANE-Mecca"
          WHEN ${TABLE}.sheet_name  LIKE "TB%" THEN "TORY BURCH"
          WHEN ${TABLE}.sheet_name  LIKE "F02%" or ${TABLE}.sheet_name LIKE "FA-Mecca" THEN "FACES-Mecca"
          WHEN ${TABLE}.sheet_name  LIKE "F03%" or ${TABLE}.sheet_name LIKE "FA-Taj" THEN "FACES-Taj"
          WHEN ${TABLE}.sheet_name  LIKE "F04%" or ${TABLE}.sheet_name  LIKE "FA-Abdali" THEN "FACES-Abdali"
          END;;
  }
  dimension: business_unit {
    label: "Business Unit"
    type: string
    sql: CASE WHEN ${TABLE}.sheet_name  LIKE "MK%" THEN "MICHAEL KORS"
    WHEN  ${TABLE}.sheet_name  LIKE "LA%"   THEN "LACOSTE"
    WHEN ${TABLE}.sheet_name LIKE "LO%" THEN "LOCCITANE"
    WHEN ${TABLE}.sheet_name  LIKE "TB%" THEN "TORY BURCH"
    WHEN ${TABLE}.sheet_name  LIKE "F%" THEN "FACES"
    END;;
    }
  dimension: vertical {
    label: "Vertical"
    type: string
    sql: CASE WHEN ${TABLE}.sheet_name  LIKE "MK%" THEN "FASHION"
          WHEN  ${TABLE}.sheet_name  LIKE "LA%"   THEN "FASHION"
          WHEN ${TABLE}.sheet_name LIKE "LO%" THEN "JOINT VENTURES"
          WHEN ${TABLE}.sheet_name  LIKE "TB%" THEN "FASHION"
          WHEN ${TABLE}.sheet_name  LIKE "F%" THEN "BEAUTY"
          END;;
  }


  dimension: Age_Buckets {
    case: {
      when: {
        label: "< 2"
        sql: 1=1 ;;
      }
      when: {
        label: ">= 2 and <3"
        sql: 1=1 ;;
      }
      when: {
        label: ">= 3 and <6"
        sql: 1=1 ;;
      }
      when: {
        label: ">= 6 and <12"
        sql: 1=1 ;;
      }
      when: {
        label: ">= 12 and <24"
        sql: 1=1 ;;
      }
      when: {
        label: ">= 24 and <36"
        sql: 1=1 ;;
      }
      when: {
        label: ">= 36 and <48"
        sql: 1=1 ;;
      }
      when: {
        label: ">= 48"
        sql: 1=1 ;;
      }
    }
  }

  dimension: country {
    label: "Country Name"
    type: string
    sql: CASE WHEN ${TABLE}.sheet_name  is not null THEN "Jordan"
        END;;
  }
  dimension_group: stock {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.stock_date AS TIMESTAMP);;
  }
  measure: month_closing_stock_value {
    type: sum
    hidden: no
    group_label: "Stock Metrics"
    label: "Month Closing SOH Value in Local Currency"
    value_format: "#,##0.00"
    sql: cast(${TABLE}.on_hand as INT64)*cast(${TABLE}.avg_cost as FLOAT64);;
  }

  measure: month_closing_stock_value_usd {
    type: sum
    hidden: no
    group_label: "Stock Metrics"
    label: "Month Closing SOH Value USD"
    value_format: "$#,##0.00"
    sql: cast(${TABLE}.on_hand as INT64)*cast(${TABLE}.avg_cost as FLOAT64)*1.41;;
  }

  measure: month_closing_stock_quantity {
    type: sum
    hidden: no
    group_label: "Stock Metrics"
    label: "Month Closing SOH Quantity"
    value_format_name: decimal_0
    sql: cast(${on_hand} as INT64);;
  }
  measure: count {
    hidden: yes
    type: count
    drill_fields: []
  }
}
