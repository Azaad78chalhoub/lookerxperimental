view: baan_stock {
  sql_table_name: `chb-prod-supplychain-data.prod_supply_chain.baan_stock`
    ;;
  dimension: pk {
    primary_key: yes
    hidden: yes
    type: string
    sql: CONCAT(${item},${store},${ingestion_time}) ;;
  }
  dimension: _1440days_qty {
    hidden: yes
    type: number
    sql: CAST(${TABLE}._1440days_qty AS NUMERIC) ;;
  }
  measure: sum_1440_days_qty {
    hidden: no
    group_label: "Quantity by Age"
    label: "48"
    type: sum
    sql: ${_1440days_qty} ;;
  }

  dimension: _1440days_value {
    hidden: yes
    type: number
    sql: CAST(${TABLE}._1440days_value AS NUMERIC);;
  }
  measure: sum_1440_days_value {
    type: sum
    group_label: "Value by Age"
    hidden: no
    label: "48"
    sql: ${_1440days_value};;
  }


  dimension: _180days_qty {
    hidden: yes
    type: number
    sql: CAST(${TABLE}._180days_qty AS NUMERIC) ;;
  }
  measure: sum_180_days_qty {
    type: sum
    group_label: "Quantity by Age"
    hidden: no
    label: ">= 6 and <12"
    sql: ${_180days_qty} ;;
  }

  dimension: _180days_value {
    hidden: yes
    type: number
    sql: CAST(${TABLE}._180days_value AS NUMERIC) ;;
  }
  measure: sum_180_days_value {
    type: sum
    group_label: "Value by Age"
    hidden: no
    label: ">= 6 and <12"
    sql:${_180days_value};;
  }

  dimension: _360days_qty {
    hidden: yes
    type: number
    sql: CAST(${TABLE}._360days_qty AS NUMERIC) ;;
  }
  measure: sum_360_days_qty {
    type: sum
    group_label: "Quantity by Age"
    hidden: no
    label:">= 12 and <24"
    sql: ${_360days_qty} ;;
  }

  dimension: _360days_value {
    hidden: yes
    type: number
    sql: CAST(${TABLE}._360days_value AS NUMERIC) ;;
  }
  measure: sum_360_days_value {
    type: sum
    group_label: "Value by Age"
    hidden: no
    label:">= 12 and <24"
    sql: ${_360days_value};;
  }

  dimension: _60days_qty {
    hidden: yes
    type: number
    sql: CAST(${TABLE}._60days_qty AS NUMERIC);;
  }
  measure: sum_60_days_qty {
    hidden: no
    group_label: "Quantity by Age"
    label: ">= 2 and <3"
    type: sum
    sql: ${_60days_qty} ;;
  }

  dimension: _60days_value {
    hidden: yes
    type: number
    sql: CAST(${TABLE}._60days_value AS NUMERIC);;
  }
  measure: sum_60_days_value {
    hidden: no
    group_label: "Value by Age"
    label: ">= 2 and <3"
    type: sum
    sql: ${_60days_value};;
  }

  dimension: _720days_qty {
    hidden: yes
    type: number
    sql: CAST(${TABLE}._720days_qty AS NUMERIC);;
  }
  measure: sum_720_days_qty {
    type: sum
    group_label: "Quantity by Age"
    hidden: no
    label: ">= 24 and <36"
    sql: ${_720days_qty} ;;
  }

  dimension: _720days_value {
    hidden: yes
    type: number
    sql: CAST(${TABLE}._720days_value AS NUMERIC) ;;
  }
  measure: sum_720_days_value {
    type: sum
    group_label: "Value by Age"
    hidden: no
    label: ">= 24 and <36"
    sql: ${_720days_value};;
  }

  dimension: _90days_qty {
    hidden: yes
    type: number
    sql: CAST(${TABLE}._90days_qty AS NUMERIC) ;;
  }
  measure: sum_90_days_qty {
    type: sum
    group_label: "Quantity by Age"
    hidden: no
    label: ">= 3 and <6"
    sql: ${_90days_qty} ;;
  }

  dimension: _90days_value {
    hidden: yes
    type: number
    sql: CAST(${TABLE}._90days_value AS NUMERIC) ;;
  }
  measure: sum_90_days_value {
    type: sum
    group_label: "Value by Age"
    hidden: no
    label: ">= 3 and <6"
    sql: ${_90days_value};;
  }

  dimension: barcode {
    type: string
    sql: ${TABLE}.barcode ;;
  }

  dimension: brand {
    type: string
    sql: ${TABLE}.brand ;;
  }

  dimension: business_unit {
    type: string
    sql: ${TABLE}.business_unit ;;
  }

  dimension: color_uda {
    type: string
    sql: ${TABLE}.color_uda ;;
  }

  dimension: cost_price {
    hidden: yes
    type: number
    sql: CAST(${TABLE}.cost_price AS NUMERIC) ;;
  }

  measure: sum_cost_price {
    label: "Cost Price"
    type: sum
    hidden: no
    sql: ${cost_price};;
  }

  dimension: country {
    type: string
    map_layer_name: countries
    sql: ${TABLE}.country ;;
  }

  dimension: division_description {
    type: string
    sql: ${TABLE}.division_description ;;
  }

  dimension: file_name {
    hidden: yes
    type: string
    sql: ${TABLE}.file_name ;;
  }

  dimension: gt_1440days_qty {
    hidden: yes
    type: number
    sql: CAST(${TABLE}.gt_1440days_qty AS NUMERIC) ;;
  }
  measure: sum_more_than_1440_days_qty {
    type: sum
    group_label: "Quantity by Age"
    hidden: no
    label:  ">= 48"
    sql: ${gt_1440days_qty} ;;
  }

  dimension: gt_1440days_value {
    hidden: yes
    type: number
    sql: CAST(${TABLE}.gt_1440days_value AS NUMERIC) ;;
  }
  measure: sum_more_than_1440_days_value {
    type: sum
    group_label: "Value by Age"
    hidden: no
    label:  ">= 48"
    sql: ${gt_1440days_value};;
  }

  dimension: ingestion_time {
    hidden: yes
    type: string
    sql: ${TABLE}.ingestion_time ;;
  }

  dimension: item {
    type: string
    label: "VPN"
    sql: ${TABLE}.item ;;
  }

  dimension: item_description {
    type: string
    sql: ${TABLE}.item_description ;;
  }

  dimension: lt_60days_qty {
    hidden: yes
    type: number
    sql: CAST(${TABLE}.lt_60days_qty AS NUMERIC) ;;
  }
  measure: sum_lesser_than_60_days_qty {
    label: "< 2"
    hidden: no
    group_label: "Quantity by Age"
    type: sum
    sql: ${lt_60days_qty};;
  }

  dimension: lt_60days_value {
    hidden: yes
    type: number
    sql: CAST(${TABLE}.lt_60days_value AS NUMERIC) ;;
  }
  measure: sum_lesser_than_60_days_value {
    label: "< 2"
    hidden: no
    group_label: "Value by Age"
    type: sum
    sql: ${lt_60days_value};;
  }

  dimension: prod__category {
    type: string
    sql: ${TABLE}.prod__category ;;
  }

  dimension: product_division {
    type: string
    sql: ${TABLE}.product_division ;;
  }

  dimension: quantity_on_hand {
    hidden: yes
    type: number
    sql: CAST(${TABLE}.quantity_on_hand AS NUMERIC);;
  }
  measure: month_closing_stock_quantity {
    type: sum
    hidden: no
    group_label: "Stock Metrics"
    label: "Month Closing SOH Quantity"
    sql: ${quantity_on_hand};;
  }

  dimension: season {
    type: string
    sql: ${TABLE}.season ;;
  }

  dimension_group: stock {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(PARSE_DATE('%d-%m-%Y', ${TABLE}.stock_date) AS TIMESTAMP);;
  }

  dimension: store {
    type: string
    sql: ${TABLE}.store ;;
  }

  dimension: store_name {
    type: string
    sql: ${TABLE}.store_name ;;
  }

  dimension: taxonomy_1 {
    type: string
    sql: ${TABLE}.taxonomy_1 ;;
  }

  dimension: taxonomy_1_1 {
    type: string
    sql: ${TABLE}.taxonomy_1_1 ;;
  }

  dimension: value {
    hidden: yes
    type: number
    sql: CAST(${TABLE}.value AS NUMERIC) ;;
  }
  measure: month_closing_stock_value {
    type: sum
    hidden: no
    group_label: "Stock Metrics"
    label: "Month Closing SOH Value"
    sql: ${value};;
  }

   dimension: vpn {
    hidden: yes
    label: "VPN"
    type: string
    sql: ${TABLE}.vpn ;;
  }

  dimension: Age_Buckets {
    case: {
      when: {
        label: "< 2"
        sql: 1=1 ;;
      }
      when: {
        label: ">= 2 and <3"
        sql: 1=1 ;;
      }
      when: {
        label: ">= 3 and <6"
        sql: 1=1 ;;
      }
      when: {
        label: ">= 6 and <12"
        sql: 1=1 ;;
      }
      when: {
        label: ">= 12 and <24"
        sql: 1=1 ;;
      }
      when: {
        label: ">= 24 and <36"
        sql: 1=1 ;;
      }
      when: {
        label: ">= 36 and <48"
        sql: 1=1 ;;
      }
      when: {
        label: ">= 48"
        sql: 1=1 ;;
      }
    }
  }
}
