view: tryano_ranks {
  derived_table: {
    explore_source: fact_retail_sales {
      column: code_desc { field: ch_trait_attribute.code_desc }
      column: brand { field: dim_retail_product.brand }
      column: net_sales_amount_local {}
      column: concession_product { field: ch_concession.concession_product }
      derived_column: sales_rank {
        sql: RANK() OVER (PARTITION BY concession_product, code_desc ORDER BY  net_sales_amount_local DESC);;
      }
      bind_all_filters: yes
    }
  }
  dimension: pk {
    primary_key: yes
    hidden: yes
    sql: CONCAT(${code_desc}, " - ", ${brand}, " - ", ${concession_product}) ;;
  }

  dimension: concession_product {
    label: "Concession"
    hidden: yes
    description: "Concession for Tryano"
  }

  dimension: code_desc {
    label: "Ch Trait Attribute Zone Description"
    hidden: yes
    description: "Zone Description for Tryano"
  }
  dimension: brand {
    label: "Products Brand"
    hidden: yes
    description: "Brand attribute from Oracle RMS"
  }
  dimension: net_sales_amount_local {
    label: "Sales Information Net Sales Amount Local"
    description: "Gross Sales - Returns - Discounts, use with Customer ID for LTV"
    hidden: yes
    value_format: "#,##0"
    type: number
  }
  dimension: sales_rank {
    type: number
    label: "Brand Rank"
  }

  dimension: concession_ownbought {
    label: "Business Type"
    type: string
    description: "Concession or Own Bought"
    sql: CASE WHEN ${concession_product} THEN "CONCESSION" ELSE "OWN BOUGHT" END ;;
  }

}
