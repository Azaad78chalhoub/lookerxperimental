view: awac_new {
  derived_table: {
    explore_source: dm_supply_orders_detailed {
      bind_all_filters: yes
      column: item_unit_cost {}
      column: item_unit_cost_usd {}
      column: loc_code {}
      column: item {}
      derived_column: awac {
        sql: (CASE WHEN item_unit_cost = 0 THEN NULL ELSE AVG(item_unit_cost) END ) ;;
      }
      derived_column: awac_usd {
        sql: ( CASE WHEN item_unit_cost = 0 THEN NULL ELSE AVG(item_unit_cost_usd) END ) ;;
      }
    }

  }

  dimension: pk {
    primary_key: yes
    hidden: yes
    sql:  CONCAT(${loc_code}, " - ", ${item}) ;;
  }

  dimension: loc_code {
    label: "Product Data in RMS Location ID"
    type:  number
    hidden: yes
  }
  dimension: item {
    label: "Product Data in RMS Product ID"
    type :  number
    hidden: yes
  }
  dimension: unit_cost {
    label: "Product Data in RMS WAC Local"
    description: "Weighted Average Unit Cost (WAC) Local"
    hidden: yes
    value_format: "#,##0.00"
    type: number
  }
  dimension: unit_cost_usd {
    label: "Product Data in RMS WAC USD"
    description: "Weighted Average Unit Cost (WAC) USD"
    hidden: yes
    value_format: "$#,##0.00"
    type: number
  }
  dimension: awac {
    view_label: "Product Data in RMS"
    description: "Aproximate Weighted Average Cost"
    label: "AWAC"
    hidden: no
    value_format_name:  decimal_2
    type: number
  }
  dimension: awac_usd {
    view_label: "Product Data in RMS"
    description: "Aproximate Weighted Average Cost USD"
    label: "AWAC USD"
    hidden: no
    value_format_name: usd
    type: number
  }

}
