view: sups {
  sql_table_name: `chb-prod-supplychain-data.prod_supply_chain.sups`
    ;;

  dimension: _fivetran_deleted {
    type: yesno
    hidden: yes
    sql: ${TABLE}._fivetran_deleted ;;
  }

  dimension_group: _fivetran_synced {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}._fivetran_synced ;;
  }

  dimension: addinvc_gross_net {
    type: string
    hidden: yes
    sql: ${TABLE}.addinvc_gross_net ;;
  }

  dimension: auto_appr_dbt_memo_ind {
    type: string
    hidden: yes
    sql: ${TABLE}.auto_appr_dbt_memo_ind ;;
  }

  dimension: auto_appr_invc_ind {
    type: string
    hidden: yes
    sql: ${TABLE}.auto_appr_invc_ind ;;
  }

  dimension: backorder_ind {
    type: string
    hidden: yes
    sql: ${TABLE}.backorder_ind ;;
  }

  dimension: bracket_costing_ind {
    type: string
    hidden: yes
    sql: ${TABLE}.bracket_costing_ind ;;
  }

  dimension: comment_desc {
    type: string
    hidden: yes
    sql: ${TABLE}.comment_desc ;;
  }

  dimension: contact_email {
    type: string
    hidden: yes
    sql: ${TABLE}.contact_email ;;
  }

  dimension: contact_fax {
    type: string
    hidden: yes
    sql: ${TABLE}.contact_fax ;;
  }

  dimension: contact_name {
    type: string
    sql: ${TABLE}.contact_name ;;
  }

  dimension: contact_pager {
    type: string
    hidden: yes
    sql: ${TABLE}.contact_pager ;;
  }

  dimension: contact_phone {
    type: string
    hidden: yes
    sql: ${TABLE}.contact_phone ;;
  }

  dimension: contact_telex {
    type: string
    hidden: yes
    sql: ${TABLE}.contact_telex ;;
  }

  dimension: cost_chg_amt_var {
    type: number
    hidden: yes
    sql: ${TABLE}.cost_chg_amt_var ;;
  }

  dimension: cost_chg_pct_var {
    type: number
    hidden: yes
    sql: ${TABLE}.cost_chg_pct_var ;;
  }

  dimension: currency_code {
    type: string
    hidden: yes
    sql: ${TABLE}.currency_code ;;
  }

  dimension: dbt_memo_code {
    type: string
    hidden: yes
    sql: ${TABLE}.dbt_memo_code ;;
  }

  dimension: default_item_lead_time {
    type: number
    hidden: yes
    sql: ${TABLE}.default_item_lead_time ;;
  }

  dimension: delivery_policy {
    type: string
    hidden: yes
    sql: ${TABLE}.delivery_policy ;;
  }

  dimension: dsd_ind {
    type: string
    hidden: yes
    sql: ${TABLE}.dsd_ind ;;
  }

  dimension: duns_loc {
    type: string
    hidden: yes
    sql: ${TABLE}.duns_loc ;;
  }

  dimension: duns_number {
    type: string
    hidden: yes
    sql: ${TABLE}.duns_number ;;
  }

  dimension: edi_asn {
    type: string
    hidden: yes
    sql: ${TABLE}.edi_asn ;;
  }

  dimension: edi_channel_id {
    type: number
    hidden: yes
    sql: ${TABLE}.edi_channel_id ;;
  }

  dimension: edi_contract_ind {
    type: string
    hidden: yes
    sql: ${TABLE}.edi_contract_ind ;;
  }

  dimension: edi_invc_ind {
    type: string
    hidden: yes
    sql: ${TABLE}.edi_invc_ind ;;
  }

  dimension: edi_po_chg {
    type: string
    hidden: yes
    sql: ${TABLE}.edi_po_chg ;;
  }

  dimension: edi_po_confirm {
    type: string
    hidden: yes
    sql: ${TABLE}.edi_po_confirm ;;
  }

  dimension: edi_po_ind {
    type: string
    hidden: yes
    sql: ${TABLE}.edi_po_ind ;;
  }

  dimension: edi_sales_rpt_freq {
    type: string
    hidden: yes
    sql: ${TABLE}.edi_sales_rpt_freq ;;
  }

  dimension: edi_supp_available_ind {
    type: string
    hidden: yes
    sql: ${TABLE}.edi_supp_available_ind ;;
  }

  dimension: final_dest_ind {
    type: string
    hidden: yes
    sql: ${TABLE}.final_dest_ind ;;
  }

  dimension: freight_charge_ind {
    type: string
    hidden: yes
    sql: ${TABLE}.freight_charge_ind ;;
  }

  dimension: freight_terms {
    type: string
    hidden: yes
    sql: ${TABLE}.freight_terms ;;
  }

  dimension: handling_pct {
    type: number
    hidden: yes
    sql: ${TABLE}.handling_pct ;;
  }

  dimension: inv_mgmt_lvl {
    type: string
    hidden: yes
    sql: ${TABLE}.inv_mgmt_lvl ;;
  }

  dimension: invc_pay_loc {
    type: string
    hidden: yes
    sql: ${TABLE}.invc_pay_loc ;;
  }

  dimension: invc_receive_loc {
    type: string
    hidden: yes
    sql: ${TABLE}.invc_receive_loc ;;
  }

  dimension: lang {
    type: number
    hidden: yes
    sql: ${TABLE}.lang ;;
  }

  dimension: payment_method {
    type: string
    hidden: yes
    sql: ${TABLE}.payment_method ;;
  }

  dimension: pre_mark_ind {
    type: string
    hidden: yes
    sql: ${TABLE}.pre_mark_ind ;;
  }

  dimension: prepay_invc_ind {
    type: string
    hidden: yes
    sql: ${TABLE}.prepay_invc_ind ;;
  }

  dimension: qc_freq {
    type: number
    hidden: yes
    sql: ${TABLE}.qc_freq ;;
  }

  dimension: qc_ind {
    type: string
    hidden: yes
    sql: ${TABLE}.qc_ind ;;
  }

  dimension: qc_pct {
    type: number
    hidden: yes
    sql: ${TABLE}.qc_pct ;;
  }

  dimension: replen_approval_ind {
    type: string
    hidden: yes
    sql: ${TABLE}.replen_approval_ind ;;
  }

  dimension: ret_allow_ind {
    type: string
    hidden: yes
    sql: ${TABLE}.ret_allow_ind ;;
  }

  dimension: ret_auth_req {
    type: string
    hidden: yes
    sql: ${TABLE}.ret_auth_req ;;
  }

  dimension: ret_courier {
    type: string
    hidden: yes
    sql: ${TABLE}.ret_courier ;;
  }

  dimension: ret_min_dol_amt {
    type: number
    hidden: yes
    sql: ${TABLE}.ret_min_dol_amt ;;
  }

  dimension: scale_aip_orders {
    type: string
    hidden: yes
    sql: ${TABLE}.scale_aip_orders ;;
  }

  dimension: service_perf_req_ind {
    type: string
    hidden: yes
    sql: ${TABLE}.service_perf_req_ind ;;
  }

  dimension: settlement_code {
    type: string
    hidden: yes
    sql: ${TABLE}.settlement_code ;;
  }

  dimension: ship_method {
    type: string
    hidden: yes
    sql: ${TABLE}.ship_method ;;
  }

  dimension: sup_name {
    type: string
    label: "Supplier Name"
    sql: ${TABLE}.sup_name ;;
  }

  dimension: sup_name_secondary {
    type: string
    hidden: yes
    sql: ${TABLE}.sup_name_secondary ;;
  }

  dimension: sup_qty_level {
    type: string
    hidden: yes
    sql: ${TABLE}.sup_qty_level ;;
  }

  dimension: sup_status {
    type: string
    hidden: yes
    sql: ${TABLE}.sup_status ;;
  }

  dimension: supplier {
    type: number
    primary_key: yes
    sql: ${TABLE}.supplier ;;
  }

  dimension: supplier_parent {
    type: number
    hidden: yes
    sql: ${TABLE}.supplier_parent ;;
  }

  dimension: terms {
    type: string
    hidden: yes
    sql: ${TABLE}.terms ;;
  }

  dimension: vat_region {
    type: number
    hidden: yes
    sql: ${TABLE}.vat_region ;;
  }

  dimension: vc_freq {
    type: number
    hidden: yes
    sql: ${TABLE}.vc_freq ;;
  }

  dimension: vc_ind {
    type: string
    hidden: yes
    sql: ${TABLE}.vc_ind ;;
  }

  dimension: vc_pct {
    type: number
    hidden: yes
    sql: ${TABLE}.vc_pct ;;
  }

  dimension: vmi_order_status {
    type: string
    hidden: yes
    sql: ${TABLE}.vmi_order_status ;;
  }

  measure: count {
    type: count
    hidden: yes
    drill_fields: [sup_name, contact_name]
  }
}
