view: awac_str {
  label: "Aproximate Cost Table"
  derived_table: {
    explore_source: order_sums2 {
      bind_all_filters: yes
      column: item {}
      column: sum_cost_received {}
      column: sum_qty_received {}
    }
  }

  dimension: location {
    label: "Sell Throughs Location Code"
    value_format: "0"
    hidden: yes
    type: number
  }
  dimension: item {
    label: "Sell Throughs Item No"
    hidden: yes
    primary_key: yes
    description: "Product number from Oracle RMS"
  }
  dimension: sum_cost_received {
    label: "Sell Throughs Total Cost Received"
    value_format: "#,##0"
    hidden: yes
    type: number
  }
  dimension: sum_qty_received {
    label: "Sell Throughs Total Quantity Received"
    value_format: "#,##0"
    hidden: yes
    type: number
  }
  dimension: awac_usd {
    type: number
    label: "Aproximate Cost"
    value_format_name: usd
    sql: SAFE_DIVIDE(${sum_cost_received},${sum_qty_received}) ;;
  }
}
