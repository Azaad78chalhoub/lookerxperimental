view: sell_through_last_first_values {
label: "Sell Through Last Values"
    derived_table: {
    sql:
        WITH
        base as (
        SELECT

                DATE_TRUNC(stock_date, {% parameter sell_through_prototype.comparison_period %}) as truncated_date,
                stock_date,
                org_num,
                prod_num,
                LAST_VALUE(inv_soh_qty) OVER (PARTITION BY

                                                                  org_num,
                                                                  prod_num
                                                ORDER BY stock_date
                                                ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as last_val_soh,
                FIRST_VALUE(inv_soh_qty) OVER (PARTITION BY

                                                                  org_num,
                                                                  prod_num
                                                ORDER BY stock_date
                                                ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as first_val_soh,
                SUM(sales_qty) OVER (PARTITION BY
                                                                  org_num,
                                                                  prod_num
                                                ) as sales_over_period,
                SUM(qty_ordered) OVER (PARTITION BY
                                                                  org_num,
                                                                  prod_num
                                                ) as ordered_over_period





              FROM
                `chb-prod-supplychain-data.prod_supply_chain.dm_sell_through` stp

              WHERE {% condition sell_through_prototype.prototype_date_1 %} TIMESTAMP(stp.stock_date) {% endcondition %}

        ),

        compare as (
        SELECT
                stock_date,
                TIMESTAMP(DATE_ADD(stock_date, INTERVAL
                                        {% parameter sell_through_prototype.number_of_periods_back %}
                                        {% parameter sell_through_prototype.comparison_period %})) as base_date, -- This date is needed to join on that date with base period to have calculate metrices be overlapped on the chart and in the query result
                DATETIME_TRUNC(TIMESTAMP(DATE_ADD(stock_date, INTERVAL
                                              {% parameter sell_through_prototype.number_of_periods_back %}
                                            {% parameter sell_through_prototype.comparison_period %})),
                                          {% parameter sell_through_prototype.comparison_period %}) as truncated_base_date,
                org_num,
                prod_num,
                LAST_VALUE(inv_soh_qty) OVER (PARTITION BY

                                                                  org_num,
                                                                  prod_num
                                                ORDER BY  stock_date
                                                ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as last_val_soh_previous_period,
                FIRST_VALUE(inv_soh_qty) OVER (PARTITION BY

                                                                  org_num,
                                                                  prod_num
                                                ORDER BY  stock_date
                                                ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING) as first_val_soh_previous_period,
                SUM(sales_qty) OVER (PARTITION BY
                                                                  org_num,
                                                                  prod_num
                                                ) as sales_over_period_previous_period,
                SUM(qty_ordered) OVER (PARTITION BY
                                                                  org_num,
                                                                  prod_num
                                                ) as ordered_over_period_previous_period



              FROM
                 `chb-prod-supplychain-data.prod_supply_chain.dm_sell_through` stp

              WHERE {% condition sell_through_prototype.prototype_date_1 %}
                         TIMESTAMP(DATE_ADD(stock_date, INTERVAL
                                                    {% parameter sell_through_prototype.number_of_periods_back %}
                                                    {% parameter sell_through_prototype.comparison_period %}))
                    {% endcondition %}

        ),



        aggr as (

        SELECT
        DISTINCT

        --truncated_date,

        org_num,
        prod_num,
        last_val_soh as last_value_soh,
        first_val_soh as first_value_soh,
        sales_over_period,
        ordered_over_period
        from base
        ),

        aggr_compare as (
        SELECT
        DISTINCT

          --truncated_base_date,

         org_num,
        prod_num,
        last_val_soh_previous_period as last_value_soh_previous_period,
        first_val_soh_previous_period as first_value_soh_previous_period,
        sales_over_period_previous_period,
        ordered_over_period_previous_period
        from compare
        )

        SELECT aggr.*,
              aggr_compare.last_value_soh_previous_period,
              aggr_compare.first_value_soh_previous_period,
              aggr_compare.sales_over_period_previous_period,
              aggr_compare.ordered_over_period_previous_period
        FROM aggr
        LEFT JOIN aggr_compare -- Left join is To only have periodwhic is chosen in base table
        ON aggr.org_num = aggr_compare.org_num
        AND aggr.prod_num = aggr_compare.prod_num

        --AND TIMESTAMP(aggr.truncated_date) = TIMESTAMP(aggr_compare.truncated_base_date)




    ;;
  }



  dimension: pk {
    primary_key: yes
    type: string
    hidden: yes
    sql:

    CONCAT( ${location},${product})

    ;;
  }
# ${date},

  dimension: location {
    type: string
    hidden: yes
    sql: CAST(${TABLE}.org_num as STRING) ;;
  }

  dimension: product {
    type: string
    hidden: yes
    sql: ${TABLE}.prod_num ;;
  }

  # dimension: date {
  #   type: date
  #   sql: ${TABLE}.truncated_date ;;
  # }


  # Current period metrics
  measure: soh_last_value {
    type: sum_distinct
    sql: ${TABLE}.last_value_soh ;;
  }

  measure: soh_first_value {
    type: sum_distinct
    sql: ${TABLE}.first_value_soh ;;
  }

  measure: sales_over_period {
    type: sum_distinct
    # type: sum
    sql: ${TABLE}.sales_over_period ;;
  }

  measure: ordered_over_period {
    type: sum_distinct
    sql: ${TABLE}.ordered_over_period ;;
  }

  # Previous period metrics
  measure: soh_last_value_previous_period{
    type: sum_distinct
    sql: ${TABLE}.last_value_soh_previous_period ;;
  }

  measure: soh_first_value_previous_period {
    type: sum_distinct
    sql: ${TABLE}.first_value_soh_previous_period ;;
  }

  measure: sales_over_period_previous_period {
    type: sum_distinct
    sql: ${TABLE}.sales_over_period_previous_period ;;
  }

  measure: ordered_over_period_previous_period {
    type: sum_distinct
    sql: ${TABLE}.ordered_over_period_previous_period ;;
  }

 # ST metrics for current period
  # measure: sell_through_received {
  #   type: number
  #   value_format: "0.00%"
  #   sql: ${sell_through_prototype.sales}/
  #                 (
  #                 {% if sell_through_prototype.date_date._is_selected %}
  #                   ${sales_over_period}
  #                 {% else %}
  #                   ${sell_through_prototype.sales}
  #                 {% endif %}
  #                 +

  #                 ${soh_last_value}) ;;
  # }

  # measure: sell_through_ordered{
  #   type: number
  #   value_format: "0.00%"
  #   sql: ${sell_through_prototype.sales}/
  #                   (
  #                   {% if sell_through_prototype.date_date._is_selected %}
  #                   ${sales_over_period}
  #                   {% else %}
  #                   ${sell_through_prototype.sales}
  #                   {% endif %}

  #                   + ${soh_ordered_last_value}) ;;
  # }

 # ST metrics for previous period


  }

  # measure: last_value_1 {
  #   type: number
  #   sql: LAST_VALUE(${TABLE}.soh) OVER (PARTITION BY ${location}, ${product} ORDER BY ${date}
  #             ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING);;
  # }
