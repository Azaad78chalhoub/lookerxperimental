view: sell_through_on_date_cost {
  derived_table: {
    explore_source: sell_through_on_date {
      column: item {}
      column: unit_value_received {}
      bind_all_filters: yes
}
    }
    dimension: item {
      label: "Orders Item"
    }
    dimension: unit_value_received {
      label: "Orders Unit Value Received"
      value_format: "#,##0.00"
      type: number
    }
  }
