view: average_inventory_on_hand {
  derived_table: {
    explore_source: fact_soh_sales_poc {
     column: stock_date {}
     column: granularity_from_parameter {}
      column: granularity_from_parameter_b {}
      column: total_stock_value {}
      column: total_cost_of_goods_sold {}
      column: gross_margin {}
#       filters: {
#         field: fact_soh_sales_poc.stock_date
#         value: "1 months ago for 1 months"
#       }
#       filters: {
#         field: dim_retail_product.brand
#         value: "CLARINS"
#       }
#       filters: {
#         field: fact_soh_sales_poc.granularity_of_analysis_parameter
#         value: "overall"
#       }
#       filters: {
#         field: fact_soh_sales_poc.granularity_of_analysis_parameter_b
#         value: "overall"
#       }
      bind_all_filters: yes
    }
  }

  dimension: pk {
    primary_key: yes
    hidden: yes
    type: string
    sql: CONCAT(${granularity_from_parameter}, ' - ', ${granularity_from_parameter_b}, ' - ', ${stock_date}) ;;
  }

#   dimension: stock_date {
#     label: "Sales and Stock Information Stock Date"
#     type: date
#   }

  dimension: granularity_from_parameter {hidden: yes}
  dimension: granularity_from_parameter_b {hidden: yes}
  dimension: stock_date {hidden: yes}
  dimension: gross_margin {hidden: yes}
  dimension: total_stock_value {
    hidden: yes
    type: number
  }

  dimension: total_cost_of_goods_sold {
    hidden: yes
    type: number
  }

  measure: average_inventory_on_hand {
    type: average
    hidden: yes
    label: "Average Inventory on Hand USD"
    view_label: "-- Supply Chain KPIs (use params!)"
    value_format_name: usd
    sql: ${total_stock_value} ;;
  }
  dimension: yearly_gross_margin {
    type: number
    hidden: yes
    sql: 365*${gross_margin} ;;
  }

  dimension: yearly_cogs_sum {
    type: number
    hidden: yes
    sql: 365*${total_cost_of_goods_sold} ;;
  }

#   SPD-201 Change Average Inventory Calculation

  measure: average_inventory_turnover {
    type: average
    hidden: yes

    label: "Average Yearly Inventory Turnover"
    view_label: "-- Supply Chain KPIs (use params!)"
    value_format_name: decimal_2
    sql: ${yearly_cogs_sum}/NULLIF(${total_stock_value},0) ;;
  }

}





# view: month_start_stock {
#   derived_table: {
#     explore_source: fact_soh_sales_poc {
#       column: stock_date {}
#       column: org_num {}
#       column: prod_num {}
#       column: inv_soh_qty {}
#       column: unit_cost {}
#       filters: {
#         field: fact_soh_sales_poc.stock_date
#         value: "12 months"
#       }
#       # keep this filter for first day of month
#       filters: {
#         field: fact_soh_sales_poc.stock_day_of_month
#         value: "1"
#       }
#     }
#   }
#
#   dimension: pk {
#     primary_key: yes
#     hidden: yes
#     type: string
#     sql: CONCAT(CAST(${stock_month} AS string), " - ", ${org_num}, " - ", ${prod_num} ;;
#   }
#
#   dimension_group: stock {
#     type: time
#     sql: ${TABLE}.stock_date ;;
#     timeframes: [raw, month]
#   }
#
#   dimension: org_num {
#     description: "The key to join the location table"
#     type: number
#   }
#
#   dimension: prod_num {
#     description: "The key to join the product table"
#   }
#
#   dimension: inv_soh_qty {
#     label: "Sales and Stock Information Inv Soh Qty"
#     type: number
#   }
#
#   dimension: unit_cost {
#     label: "Sales and Stock Information Inv Soh Qty"
#     type: number
#   }
#
#   measure: count_beginning_on_hand_inventory {
#     type: sum
#     sql: ${inv_soh_qty} ;;
#   }
#
#   measure: value_beginning_on_hand_inventory {
#     type: sum
#     sql: ${unit_cost} ;;
#   }
#
# }
#
#
#
#


























# view: average_inventory_on_hand {
#   derived_table: {
#     explore_source: fact_soh_sales_poc {
#       column: stock_date_from_parameter {}
#       column: granularity_from_parameter {}
#       column: stock_count {}
#       column: stock_value {}
#       column: average_inventory_on_hand {}
#       column: average_inventory_value_on_hand {}
#       bind_all_filters: yes
#     }
#   }
#
#   dimension: pk {
#     primary_key: yes
#     hidden: yes
#     type: string
#     sql: CONCAT(${stock_date_from_parameter}, ' - ', ${granularity_from_parameter}) ;;
#   }
#
#   dimension: stock_date_from_parameter {
#     hidden: yes
#     type: string
#     sql: ${TABLE}.stock_date_from_parameter ;;
#   }
#
#   dimension: granularity_from_parameter {
#     hidden: yes
#     type: string
#     sql: ${TABLE}.granularity_from_parameter ;;
#   }
#
# #   dimension: stock_count {
# #     hidden: yes
# #     type: number
# #   }
# #
# #   dimension: stock_value {
# #     hidden: yes
# #     type: number
# #   }
#
#   measure: average_inventory_on_hand {
#     type: max
#     sql: ${TABLE}.average_inventory_on_hand ;;
#     value_format_name: decimal_0
#   }
#
#   measure: average_inventory_value_on_hand {
#     type: max
#     sql: ${TABLE}.average_inventory_value_on_hand ;;
#     value_format_name: usd
#   }
#
# }
