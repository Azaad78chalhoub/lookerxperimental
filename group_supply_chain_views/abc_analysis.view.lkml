view: abc_analysis {
  derived_table: {
    sql:
With subquery1 as (
 SELECT brand,
       bk_productid,
       Netsales,
       SUM(Netsales) OVER(PARTITION BY brand ORDER BY Netsales desc) AS RUNNING_SALES
       FROM
       (
        SELECT brand,
        bk_productid,
        SUM(amountusd_beforetax) AS Netsales
        FROM
          `chb-prod-supplychain-data.prod_supply_chain.factretailsales` sales
           LEFT JOIN
              `chb-prod-supplychain-data.prod_shared_dimensions.dim_bu_cdl` dims
            ON
              cast(sales.bk_productid as string)=dims.prod_num
              AND CAST(sales.bk_storeid AS STRING)=dims.org_num

          LEFT JOIN
            `chb-prod-supplychain-data.prod_shared_dimensions.dim_retail_loc` diml
             ON CAST(sales.bk_storeid AS STRING)=CAST(diml.store as STRING)

          -- joining to get the right BU for the item
            LEFT JOIN
              `chb-prod-supplychain-data.prod_shared_dimensions.dim_alternate_bu_hierarchy` bu
            ON
              dims.business_unit_final=bu.bu_code

          WHERE atr_trandate  >= IFNULL(DATE({% date_start timeframe %}),CURRENT_DATE()) AND
                atr_trandate <=   IFNULL(DATE({% date_end timeframe %}),CURRENT_DATE())  AND
                diml.country_desc = {% parameter country%}
                AND brand = {%parameter bu_brand %}
          GROUP BY 1,2)
         WHERE Netsales>=0
          GROUP BY 1,2,3),

subquery2 as (

    SELECT SUM(Netsales) AS TOTAL_SALES
    FROM subquery1

    )

    SELECT
       bk_productid,
       SUM(Netsales) as Netsales,
       SAFE_DIVIDE(RUNNING_SALES,TOTAL_SALES) AS PARETO_PERC,
       SAFE_DIVIDE(Netsales,TOTAL_SALES) AS Sales_contribution_perc,
      CASE WHEN SAFE_DIVIDE(RUNNING_SALES,TOTAL_SALES) <= {% parameter a_cutoff%} THEN 'A'
          WHEN  SAFE_DIVIDE(RUNNING_SALES,TOTAL_SALES) <= {% parameter b_cutoff%} THEN 'B'
          ELSE 'C' END AS ABC_CLASS,
       SUM(RUNNING_SALES) RUNNING_SALES,
       SUM(TOTAL_SALES) TOTAL_SALES
    FROM

    (
      SELECT brand,
       bk_productid,
       Netsales,
       RUNNING_SALES,
       TOTAL_SALES
       FROM subquery1
       CROSS JOIN subquery2
  )
  GROUP BY 1,3,4,5;;

    }


    filter: timeframe {
      type: date
      label: "Time frame ABC analysis"
      default_value: "after 2019/12/01"
    }

  parameter: bu_brand {
    label: "Business Unit"
    type: string
    allowed_value: {
      label: "SWAROVSKI"
      value: "SWAROVSKI"
    }
    allowed_value: {
      label: "LACOSTE"
      value: "LACOSTE"
    }
    allowed_value: {
      label: "FILA"
      value: "FILA"
    }
    allowed_value: {
      label: "LEVEL SHOES"
      value: "LEVEL SHOES"
    }
    allowed_value: {
      label: "D&G"
      value: "D&G"
    }
    allowed_value: {
      label: "LOCCITANE"
      value: "LOCCITANE"
    }
    allowed_value: {
      label: "MICHAEL KORS"
      value: "MICHAEL KORS"
    }
    allowed_value: {
      label: "RALPH LAUREN"
      value: "RALPH LAUREN"
    }
    allowed_value: {
      label: "SAKS"
      value: "SAKS"
    }
    allowed_value: {
      label: "TRYANO"
      value: "TRYANO"
    }

    allowed_value: {
    label: "THE DEAL"
    value: "THE DEAL"
      }
    default_value: "SWAROVSKI"
  }


  parameter: country {
    label: "Market"
    type: string
    allowed_value: {
      label: "United Arab Emirates"
      value: "United Arab Emirates"
    }
    allowed_value: {
      label: "Kuwait"
      value: "Kuwait"
    }
    allowed_value: {
      label: "Saudi Arabia"
      value: "Saudi Arabia"
    }
    allowed_value: {
      label: "Qatar"
      value: "Qatar"
    }
    allowed_value: {
      label: "Bahrain"
      value: "Bahrain"
    }
    allowed_value: {
      label: "Morocco"
      value: "Morocco"
    }
    allowed_value: {
      label: "Egypt"
      value: "Egypt"
    }
    allowed_value: {
      label: "Egypt Free Zone"
      value: "Egypt Free Zone"
    }
    default_value: "United Arab Emirates"
  }

 parameter: a_cutoff {
  type: number
  label: "Cutoff perc for class A"
  default_value: "0.8"
}

  parameter: b_cutoff {
    type: number
    label: "Cutoff perc for class B"
    default_value: "0.95"
  }

    dimension: item {
      type: string
      hidden: no
      label: "item"
      sql: ${TABLE}.bk_productid;;

    }


  measure: sales {
    type: sum
    hidden: no
    label: "Net Sales in USD"
    sql:  ${TABLE}.Netsales;;
    value_format: "$#,##0"
    }

    dimension: pareto_perc {
      type: number
      hidden: no
      label: "Pareto percentage%"
      sql: ${TABLE}.PARETO_PERC;;
      value_format: "0.00%"
      }
  dimension: Sales_contribution_perc {
      type: number
      hidden: no
      label: "Sales contribution percentage%"
      sql: ${TABLE}.Sales_contribution_perc;;
      value_format: "0.00%"
      }

      dimension: classification {
        type: string
        hidden: no
        label: "Classification(A/B/C)"
        sql: ${TABLE}.ABC_CLASS;;
    }
  measure: count {
    hidden: no
    type: count
    drill_fields: []
  }
  }
