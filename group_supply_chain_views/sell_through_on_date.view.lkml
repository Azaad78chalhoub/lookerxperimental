view: sell_through_on_date {
  derived_table: {
    sql:
    WITH
  orders_table AS(
  SELECT
    orders.item,
    SUM(qty_received) AS total_received,
    SUM(qty_received*unit_cost_usd) AS value_received
  FROM
    `chb-prod-supplychain-data.prod_supply_chain.orders` orders
  LEFT JOIN
    `chb-prod-supplychain-data.prod_shared_dimensions.dim_bu_cdl` dims

  ON
    orders.item=dims.prod_num
 AND
 CAST(orders.location AS STRING)=dims.org_num
  LEFT JOIN
    `chb-prod-supplychain-data.prod_shared_dimensions.dim_alternate_bu_hierarchy` bu
    ON dims.business_unit_final=bu.bu_code
      WHERE
        written_date  >= {% date_start season %} AND written_date <=  {% date_end season %}
        AND qty_received IS NOT NULL


        {% if bu_brand._parameter_value == "'LACOSTE'" %}
         AND brand="LACOSTE"

        {% else if bu_brand._parameter_value == "'SWAROVSKI'" %}
         AND brand="SWAROVSKI"
        {% else %}
         AND brand="SWAROVSKI"
        {% endif %}

        {% if exclude_previously_purchased._parameter_value == "'exclude'" %}
        AND orders.item NOT IN(SELECT item FROM `chb-prod-supplychain-data.prod_supply_chain.orders` orders   WHERE
        written_date <= {% date_start season %})
            {% else %}
        AND orders.item IS NOT NULL
        {% endif %}

      GROUP BY
    1),
  calendar_table AS(
  SELECT
    *
  FROM
UNNEST(GENERATE_DATE_ARRAY( DATE({% date_start sales_season %}) ,IFNULL(DATE({% date_end sales_season %}),current_date()), INTERVAL 1 MONTH)) AS cal_date ),
  sales_table AS(
  SELECT
    year_month,
    item,
    SUM(qty_sold) AS qty_sold,
    SUM(qty_sold_md) AS qty_sold_md,
    SUM(qty_sold_fp) AS qty_sold_fp
  FROM (
    SELECT
      bk_productid AS item,
      DATE_TRUNC(atr_trandate, MONTH) AS year_month,
      SUM(mea_quantity) AS qty_sold,
      SUM(CASE WHEN discountamt_usd <> 0 THEN mea_quantity ELSE 0 END) AS qty_sold_md,
      SUM(CASE WHEN discountamt_usd = 0 THEN mea_quantity ELSE 0 END) AS qty_sold_fp
    FROM
      `chb-prod-supplychain-data.prod_supply_chain.factretailsales` sales
   LEFT JOIN
    `chb-prod-supplychain-data.prod_shared_dimensions.dim_bu_cdl` dims

  ON
    sales.bk_productid=dims.prod_num
 AND
 CAST(sales.bk_storeid AS STRING)=dims.org_num
  LEFT JOIN
    `chb-prod-supplychain-data.prod_shared_dimensions.dim_alternate_bu_hierarchy` bu
    ON dims.business_unit_final=bu.bu_code
 WHERE
atr_trandate >= DATE({% date_start sales_season %}) AND atr_trandate <= IFNULL(DATE({% date_end sales_season %}),current_date())
        {% if bu_brand._parameter_value == "'LACOSTE'" %}
     AND brand="LACOSTE"

    {% else if bu_brand._parameter_value == "'SWAROVSKI'" %}
     AND brand="SWAROVSKI"
    {% else %}
     AND brand="SWAROVSKI"
    {% endif %}
    GROUP BY
      1,
      2 )
  GROUP BY
    year_month,
    item),
  stagging_table AS(
  SELECT
    cal_date,
    item,
    SUM(total_received) AS total_received,
    SUM(value_received) AS value_received,
    SUM(qty_sold) AS qty_sold,
    SUM(qty_sold_md) AS qty_sold_md,
    SUM(qty_sold_fp) AS qty_sold_fp,
  FROM (
    SELECT
      agg_table.cal_date,
      agg_table.item,
      agg_table.total_received,
      agg_table.value_received,
      ifnull(sales_table.qty_sold,
        0) AS qty_sold,
      ifnull(sales_table.qty_sold_md,
        0) AS qty_sold_md,
      ifnull(sales_table.qty_sold_fp,
        0) AS qty_sold_fp
    FROM (
      SELECT
        DISTINCT item,
        total_received,
        value_received,
        cal_date
      FROM
        orders_table
      CROSS JOIN
        calendar_table) AS agg_table
    LEFT JOIN
      sales_table
    ON
      agg_table.item=sales_table.item
      AND agg_table.cal_date=sales_table.year_month)
  GROUP BY
    cal_date,
    item)
SELECT
  cal_date,
  item,
  SUM(total_received) AS total_received,
  SUM(value_received) AS value_received,
  SUM(stagging_table.qty_sold) AS qty_sold,
  SUM(stagging_table.qty_sold_md) AS qty_sold_md,
  SUM(stagging_table.qty_sold_fp) AS qty_sold_fp,
  SUM(SUM(qty_sold)) OVER(PARTITION BY item ORDER BY cal_date ASC ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS running_qty_sold,
  SUM(SUM(qty_sold_md)) OVER(PARTITION BY item ORDER BY cal_date ASC ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS running_qty_sold_md,
  SUM(SUM(qty_sold_fp)) OVER(PARTITION BY item ORDER BY cal_date ASC ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS running_qty_sold_fp,
  CASE
    WHEN SUM(SUM(qty_sold)) OVER(PARTITION BY item ORDER BY cal_date ASC ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) <= SUM(total_received) THEN SUM(SUM(qty_sold)) OVER(PARTITION BY item ORDER BY cal_date ASC ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)
  ELSE
  SUM(total_received)
END
  AS approximate_running_qty_sold,
  CASE
    WHEN
    SUM(SUM(qty_sold_fp)) OVER(PARTITION BY item ORDER BY cal_date ASC ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) +
    SUM(SUM(qty_sold_md)) OVER(PARTITION BY item ORDER BY cal_date ASC ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)  <= SUM(total_received)
    THEN    SUM(SUM(qty_sold_md)) OVER(PARTITION BY item ORDER BY cal_date ASC ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)
    WHEN
   ( SUM(SUM(qty_sold_fp)) OVER(PARTITION BY item ORDER BY cal_date ASC ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) +
    SUM(SUM(qty_sold_md)) OVER(PARTITION BY item ORDER BY cal_date ASC ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)  > SUM(total_received) ) AND
    SUM(SUM(qty_sold_fp)) OVER(PARTITION BY item ORDER BY cal_date ASC ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) +
    SUM(SUM(qty_sold_md)) OVER(PARTITION BY item ORDER BY cal_date ASC ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)  < SUM(total_received)
    THEN
      SUM(total_received) -  SUM(SUM(qty_sold_fp)) OVER(PARTITION BY item ORDER BY cal_date ASC ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)
  ELSE
  SUM(SUM(qty_sold_md)) OVER(PARTITION BY item ORDER BY cal_date ASC ROWS BETWEEN UNBOUNDED PRECEDING AND 1 PRECEDING)
END
  AS approximate_running_qty_sold_mp,
  CASE
    WHEN
    SUM(SUM(qty_sold_fp)) OVER(PARTITION BY item ORDER BY cal_date ASC ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) +
    SUM(SUM(qty_sold_md)) OVER(PARTITION BY item ORDER BY cal_date ASC ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)  <= SUM(total_received)
    THEN    SUM(SUM(qty_sold_fp)) OVER(PARTITION BY item ORDER BY cal_date ASC ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)
    WHEN
   ( SUM(SUM(qty_sold_fp)) OVER(PARTITION BY item ORDER BY cal_date ASC ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) +
    SUM(SUM(qty_sold_md)) OVER(PARTITION BY item ORDER BY cal_date ASC ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)  > SUM(total_received) ) AND
    SUM(SUM(qty_sold_fp)) OVER(PARTITION BY item ORDER BY cal_date ASC ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) +
    SUM(SUM(qty_sold_md)) OVER(PARTITION BY item ORDER BY cal_date ASC ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)  < SUM(total_received)
    THEN
      SUM(total_received) -  SUM(SUM(qty_sold_md)) OVER(PARTITION BY item ORDER BY cal_date ASC ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)
  ELSE
  SUM(total_received) -  SUM(SUM(qty_sold_md)) OVER(PARTITION BY item ORDER BY cal_date ASC ROWS BETWEEN UNBOUNDED PRECEDING AND 1 PRECEDING)
END
  AS approximate_running_qty_sold_fp
FROM
  stagging_table
GROUP BY
  cal_date,
  item


      ;;
  }

  parameter: exclude_previously_purchased {
    type: string
    allowed_value: {
      label: "Yes"
      value: "exclude"
    }
    allowed_value: {
      label: "No"
      value: "no"
    }
    default_value: "exclude"
  }

  filter: sales_season {
    type: date
    label: "Sales Season"
    default_value: "after 2012/05/01"
  }


  filter: season {
    type: date
    label: "Purchase Season"
    default_value: "after 2016/12/01"
  }

  parameter: bu_brand {
    label: "Business Unit"
    type: string
    allowed_value: {
      label: "Swarovski"
      value: "SWAROVSKI"
    }
    allowed_value: {
      label: "Lacoste"
      value: "LACOSTE"
    }
    default_value: "SWAROVSKI"
  }

  dimension: item {
    type: string
    sql: ${TABLE}.item ;;
  }


  dimension: pk {
    type: string
    primary_key: yes
    hidden: yes
    sql: CONCAT(${order_date}, " - ", ${item}) ;;
  }


  dimension_group: order {
    type: time
    timeframes: [
      raw,
      date,
      day_of_month,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.cal_date ;;
  }

  dimension: qty_sold {
    type: number
    label: "Quantity Sold"
    sql: ${TABLE}.qty_sold ;;
  }

  dimension: qty_sold_md {
    type: number
    label: "Quantity Sold on MD"
    sql: ${TABLE}.qty_sold_md ;;
  }

  dimension: qty_sold_fp {
    type: number
    label: "Quantity Sold on FP"
    sql: ${TABLE}.qty_sold_fp ;;
  }

  dimension: total_received {
    type: number
    label: "Total Received"
    sql: ${TABLE}.total_received ;;
  }

  dimension: value_received {
    type: number
    value_format_name: usd
    label: "Value Received"
    sql: ${TABLE}.value_received ;;
  }

  dimension: running_qty_sold {
    type: number
    label: "Running Quantity Sold"
    sql: ${TABLE}.running_qty_sold ;;
  }

  dimension: running_qty_sold_md {
    type: number
    label: "Running Quantity Sold MD"
    sql: ${TABLE}.running_qty_sold_md ;;
  }

  dimension: running_qty_sold_fp {
    type: number
    label: "Running Quantity Sold FP"
    sql: ${TABLE}.running_qty_sold_fp ;;
  }

  dimension: app_running_qty_sold {
    type: number
    label: "Running Quantity Sold from PO"
    sql: ${TABLE}.approximate_running_qty_sold ;;
  }


  dimension: app_running_qty_sold_md {
    type: number
    label: "Running Quantity Sold MD from PO"
    sql: ${TABLE}.approximate_running_qty_sold_mp ;;
  }


  dimension: app_running_qty_sold_fp {
    type: number
    label: "Running Quantity Sold FP from PO"
    sql: ${TABLE}.approximate_running_qty_sold_fp ;;
  }

  measure: total_quantity_received {
    type: number
    sql: SAFE_DIVIDE(SUM(${total_received}),COUNT(DISTINCT ${order_month})) ;;
  }

  measure: unit_value_received {
    type: number
    value_format_name: usd
    label: "Average Unit Cost USD"
    sql: SAFE_DIVIDE(SUM(SAFE_DIVIDE(${value_received},${total_received})),COUNT(DISTINCT ${order_month})) ;;
  }

  measure: total_qty_sold_from_po {
    type: sum
    label: "Total Quantity Sold"
    sql: ${qty_sold} ;;
  }

  measure: running_qty_sold_from_po {
    type: number
    label: "Running Quantity Sold per Month"
    sql: SAFE_DIVIDE(SUM(${app_running_qty_sold}),COUNT(DISTINCT ${order_month})) ;;
  }



  measure: running_qty_sold_fp_from_po {
    type: number
    label: "Running Quantity Sold FP per Month"
    sql: SAFE_DIVIDE(SUM(${app_running_qty_sold_fp}),COUNT(DISTINCT ${order_month})) ;;
  }


  measure: running_qty_sold_nd_from_po {
    type: number
    label: "Running Quantity Sold MD per Month"
    sql: SAFE_DIVIDE(SUM(${app_running_qty_sold_md}),COUNT(DISTINCT ${order_month})) ;;
  }

  measure: total_value_received {
    type: number
    value_format_name: usd
    sql: SAFE_DIVIDE(SUM(${total_received}*${sell_through_on_date_cost.unit_value_received}),COUNT(DISTINCT ${order_month})) ;;
  }

  measure: total_value_sold {
    type: number
    value_format_name: usd
    sql: SAFE_DIVIDE(SUM(${app_running_qty_sold}*${sell_through_on_date_cost.unit_value_received}),COUNT(DISTINCT ${order_month})) ;;
  }

  measure: total_value_sold_full_price {
    type: number
    value_format_name: usd
    sql: SAFE_DIVIDE(SUM(${app_running_qty_sold_fp}*${sell_through_on_date_cost.unit_value_received}),COUNT(DISTINCT ${order_month})) ;;
  }


  measure: total_value_sold_markdown {
    type: number
    value_format_name: usd
    sql: SAFE_DIVIDE(SUM(${app_running_qty_sold_md}*${sell_through_on_date_cost.unit_value_received}),COUNT(DISTINCT ${order_month})) ;;
    }


}
