view: sales_test {
  sql_table_name: `chb-prod-supplychain-data.prod_supply_chain.factretailsales`
    ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: _fivetran_deleted {
    type: yesno
    sql: ${TABLE}._fivetran_deleted ;;
  }

  dimension_group: _fivetran_synced {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}._fivetran_synced ;;
  }

  dimension: amountlocal_beforetax {
    type: number
    sql: ${TABLE}.amountlocal_beforetax ;;
  }

  dimension: amountusd_beforetax {
    type: number
    sql: ${TABLE}.amountusd_beforetax ;;
  }

  dimension: atr_action {
    type: string
    sql: ${TABLE}.atr_action ;;
  }

  dimension: atr_activitytype {
    type: string
    sql: ${TABLE}.atr_activitytype ;;
  }

  dimension: atr_batchid {
    type: string
    sql: ${TABLE}.atr_batchid ;;
  }

  dimension: atr_cginvoiceno {
    type: string
    sql: ${TABLE}.atr_cginvoiceno ;;
  }

  dimension: atr_cginvoicenoline {
    type: string
    sql: ${TABLE}.atr_cginvoicenoline ;;
  }

  dimension_group: atr_createdate {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.atr_createdate ;;
  }

  dimension: atr_day {
    type: number
    sql: ${TABLE}.atr_day ;;
  }

  dimension_group: atr_documentdate {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.atr_documentdate ;;
  }

  dimension: atr_errordesc {
    type: string
    sql: ${TABLE}.atr_errordesc ;;
  }

  dimension: atr_homecurrency {
    type: string
    sql: ${TABLE}.atr_homecurrency ;;
  }

  dimension: atr_itemseqno {
    type: number
    sql: ${TABLE}.atr_itemseqno ;;
  }

  dimension: atr_itemstatus {
    type: string
    sql: ${TABLE}.atr_itemstatus ;;
  }

  dimension: atr_membershipid {
    type: string
    sql: ${TABLE}.atr_membershipid ;;
  }

  dimension: atr_muse_id {
    type: string
    sql: ${TABLE}.atr_muse_id ;;
  }

  dimension: atr_promotionid {
    type: number
    value_format_name: id
    sql: ${TABLE}.atr_promotionid ;;
  }

  dimension: atr_promotionid2 {
    type: number
    sql: ${TABLE}.atr_promotionid2 ;;
  }

  dimension: atr_salesperson {
    type: string
    sql: ${TABLE}.atr_salesperson ;;
  }

  dimension: atr_stagestatus {
    type: string
    sql: ${TABLE}.atr_stagestatus ;;
  }

  dimension: atr_status {
    type: string
    sql: ${TABLE}.atr_status ;;
  }

  dimension: atr_storedayseqno {
    type: number
    sql: ${TABLE}.atr_storedayseqno ;;
  }

  dimension: atr_time {
    type: number
    sql: ${TABLE}.atr_time ;;
  }

  dimension: atr_timezone {
    type: string
    sql: ${TABLE}.atr_timezone ;;
  }

  dimension_group: atr_trandate {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.atr_trandate ;;
  }

  dimension: atr_tranno {
    type: number
    sql: ${TABLE}.atr_tranno ;;
  }

  dimension: atr_transeqno {
    type: number
    sql: ${TABLE}.atr_transeqno ;;
  }

  dimension: atr_trantype {
    type: string
    sql: ${TABLE}.atr_trantype ;;
  }

  dimension_group: atr_updatedate {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.atr_updatedate ;;
  }

  dimension: av_cost_local {
    type: number
    sql: ${TABLE}.av_cost_local ;;
  }

  dimension: av_cost_usd {
    type: number
    sql: ${TABLE}.av_cost_usd ;;
  }

  dimension: bk_brand {
    type: number
    sql: ${TABLE}.bk_brand ;;
  }

  dimension: bk_businessdate {
    type: number
    hidden: no
    sql: ${TABLE}.bk_businessdate ;;
  }

  dimension_group: business {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: TIMESTAMP(PARSE_DATE('%Y%m%d', CAST(${bk_businessdate} AS STRING))) ;;
  }

  dimension: bk_customer {
    type: string
    sql: ${TABLE}.bk_customer ;;
  }

  dimension: bk_loyalty {
    type: string
    sql: ${TABLE}.bk_loyalty ;;
  }

  dimension: bk_loyaltyprogram {
    type: string
    sql: ${TABLE}.bk_loyaltyprogram ;;
  }

  dimension: bk_productid {
    type: string
    sql: ${TABLE}.bk_productid ;;
  }

  dimension: bk_salestype {
    type: number
    sql: ${TABLE}.bk_salestype ;;
  }

  dimension: bk_storeid {
    type: number
    value_format_name: id
    sql: ${TABLE}.bk_storeid ;;
  }

  dimension: bk_top20 {
    type: string
    sql: ${TABLE}.bk_top20 ;;
  }

  dimension: conversion_rate {
    type: number
    sql: ${TABLE}.CONVERSION_RATE ;;
  }

  dimension_group: creationdatetime {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.creationdatetime ;;
  }

  dimension: crm_customer_id {
    type: string
    sql: ${TABLE}.crm_customer_id ;;
  }

  dimension: customer_golden_id {
    type: string
    sql: ${TABLE}.customer_golden_id ;;
  }

  dimension: discountamt_local {
    type: number
    sql: ${TABLE}.discountamt_local ;;
  }

  dimension: discountamt_usd {
    type: number
    sql: ${TABLE}.discountamt_usd ;;
  }

  dimension: mea_amountlocal {
    type: number
    sql: ${TABLE}.mea_amountlocal ;;
  }

  dimension: mea_amountusd {
    type: number
    sql: ${TABLE}.mea_amountusd ;;
  }

  dimension: mea_quantity {
    type: number
    sql: ${TABLE}.mea_quantity ;;
  }

  dimension: pos_customer_id {
    type: string
    sql: ${TABLE}.pos_customer_id ;;
  }

  dimension: taxamt_local {
    type: number
    sql: ${TABLE}.taxamt_local ;;
  }

  dimension: taxamt_usd {
    type: number
    sql: ${TABLE}.taxamt_usd ;;
  }

  measure: net_sales {
    type: sum
    label: "Net Revenue USD Before Tax"
    group_label: "Revenue KPIs"
    view_label: "KPIs View"
    sql: ${amountusd_beforetax} ;;
  }

  measure: count {
    type: count
    drill_fields: [id]
  }
}
