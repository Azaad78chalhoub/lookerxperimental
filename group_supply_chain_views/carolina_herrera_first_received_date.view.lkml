view: carolina_herrera_first_received_date {
  derived_table: {
    sql: select            country,
                  store,
                  store_code,
                  sku_externalcode,
                  min(stockdate) as stockdate

           from
           (
            select
                  stockdate,
                  country,
                  store,
                  store_code,
                  sku_externalcode,
                  availablestock

                  from `chb-prod-supplychain-data.prod_supply_chain.carolina_herrera_theoretical_stock` stocktbl
                  where availablestock>0)

                  group by 1,2,3,4
 ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  dimension: country {
    type: string
    hidden: yes
    sql: ${TABLE}.country ;;
  }

  dimension: store {
    type: number
    hidden: yes
    sql: ${TABLE}.store ;;
  }

  dimension: store_code {
    type: string
    hidden: yes
    sql: ${TABLE}.store_code ;;
  }

  dimension: sku_externalcode {
    type: string
    hidden: yes
    sql: ${TABLE}.sku_externalcode ;;
  }

  dimension: stockdate {
    type: date
    label: "First Received Date"
    datatype: date
    sql: ${TABLE}.stockdate ;;
  }

  set: detail {
    fields: [country, store, store_code, sku_externalcode, stockdate]
  }
}
