# test: fact_soh_sales_poc_test {
#   explore_source: fact_soh_sales_poc {
#     column: inv_soh_qty {}
#     sort: {
#       field: inv_soh_qty
#       desc: yes
#     }
#     limit: 1
#   }
#   assert: qty_is_not_null {
#     expression: NOT is_null(${fact_soh_sales_poc.inv_soh_qty}) ;;
#   }
# }


include: "../_common/period_over_period.view"

view: fact_soh_sales_poc {
  sql_table_name: `chb-prod-supplychain-data.prod_supply_chain.dm_soh_sales` ;;
  extends: [period_over_period]

  parameter: timeframe_parameter {
    label: "GMROI Timeframe Parameter"
    view_label: "- Parameters"
    type: unquoted
    default_value: "year"
    allowed_value: {
      label: "Yearly"
      value: "year"
    }
    allowed_value: {
      label: "Quarterly"
      value: "quarter"
    }
    allowed_value: {
      label: "Monthly"
      value: "month"
    }
  }

  parameter: sell_through_granularity {
    label: "Sell Through Parameter"
    view_label: "- Parameters"
    type: unquoted
    default_value: "year"
    allowed_value: {
      label: "Yearly"
      value: "year"
    }
    allowed_value: {
      label: "Quarterly"
      value: "quarter"
    }
    allowed_value: {
      label: "Monthly"
      value: "month"
    }
    allowed_value: {
      label: "Daily"
      value: "day"
    }
    allowed_value: {
      label: "Overall(use parameter)"
      value: "overall"
    }
  }

  parameter: granularity_of_analysis_parameter {
    view_label: "- Parameters"
    type: unquoted
    default_value: "overall"
    allowed_value: {
      label: "Overall"
      value: "overall"
    }
    allowed_value: {
      label: "Date"
      value: "date"
    }
    allowed_value: {
      label: "Month"
      value: "month"
    }
    allowed_value: {
      label: "Week"
      value: "week"
    }
    allowed_value: {
      label: "Product ID"
      value: "item_no"
    }
    allowed_value: {
      label: "Item VPN"
      value: "item_vpn"
    }
    allowed_value: {
      label: "Item Style ID"
      value: "item_style_number"
    }
    allowed_value: {
      label: "Class"
      value: "class_name"
    }
    allowed_value: {
      label: "Sub Class"
      value: "subclass_name"
    }
    allowed_value: {
      label: "Taxonomy 1"
      value: "taxonomy_1"
    }
    allowed_value: {
      label: "Taxonomy 2"
      value: "taxonomy_2"
    }
    allowed_value: {
      label: "Colour"
      value: "colour"
    }
    allowed_value: {
      label: "Size"
      value: "size"
    }
    allowed_value: {
      label: "Gender"
      value: "gender"
    }
    allowed_value: {
      label: "Brand"
      value: "brand"
    }
    allowed_value: {
      label: "Division"
      value: "division"
    }
    allowed_value: {
      label: "Season"
      value: "season"
    }
    allowed_value: {
      label: "Business Unit"
      value: "business_unit"
    }
    allowed_value: {
      label: "Country"
      value: "country"
    }
    allowed_value: {
      label: "Boat"
      value: "boat"
    }
    allowed_value: {
      label: "District"
      value: "district"
    }
    allowed_value: {
      label: "Store"
      value: "store"
    }
  }

  parameter: granularity_of_analysis_parameter_b {
    view_label: "- Parameters"
    type: unquoted
    default_value: "overall"
    allowed_value: {
      label: "Overall"
      value: "overall"
    }
    allowed_value: {
      label: "Date"
      value: "date"
    }
    allowed_value: {
      label: "Month"
      value: "month"
    }
    allowed_value: {
      label: "Week"
      value: "week"
    }
    allowed_value: {
      label: "Item No."
      value: "item_no"
    }
    allowed_value: {
      label: "Item VPN"
      value: "item_vpn"
    }
    allowed_value: {
      label: "Item Style Number"
      value: "item_style_number"
    }
    allowed_value: {
      label: "Class"
      value: "class_name"
    }
    allowed_value: {
      label: "Sub Class"
      value: "subclass_name"
    }
    allowed_value: {
      label: "Taxonmy 1"
      value: "taxonomy_1"
    }
    allowed_value: {
      label: "Taxonmy 2"
      value: "taxonomy_2"
    }
    allowed_value: {
      label: "Colour"
      value: "colour"
    }
    allowed_value: {
      label: "Size"
      value: "size"
    }
    allowed_value: {
      label: "Gender"
      value: "gender"
    }
    allowed_value: {
      label: "Brand"
      value: "brand"
    }
    allowed_value: {
      label: "Division"
      value: "division"
    }
    allowed_value: {
      label: "Season"
      value: "season"
    }
    allowed_value: {
      label: "Business Unit"
      value: "business_unit"
    }
    allowed_value: {
      label: "Country"
      value: "country"
    }
    allowed_value: {
      label: "Boat"
      value: "boat"
    }
    allowed_value: {
      label: "District"
      value: "district"
    }
    allowed_value: {
      label: "Store"
      value: "store"
    }
  }

##################################
#####         DATES          #####
##################################


  dimension: stock_raw_date {
    type: string
    hidden: yes
    sql: ${TABLE}.stock_date ;;
  }

  dimension_group: stock {
    type: time
    timeframes: [
      raw,
      date,
      day_of_month,
      day_of_week,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.stock_date ;;
#     sql: ${TABLE}.date_mc ;;
  }

  dimension: stockdate_week_no{
    type: number
    description: "Week No. extracted from the Stock date"
    sql:EXTRACT(WEEK FROM(${TABLE}.stock_date))+1  ;;
  }

  measure: last_updated_date {
    type: date
    label: "Last Sales refresh date"
    sql: case when sum(${sales_amt_usd}) >=0 then MAX(${stock_raw_date}) end ;;
    convert_tz: no
  }
  dimension_group: stock_snapshot_update {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.stock_snapshot_update_ts ;;
  }
  measure: stock_snapshot_refresh_date {
    type: date_time
    label: "Last Stock refresh timestamp"
    sql:CAST(MAX(${stock_snapshot_update_time}) AS TIMESTAMP)  ;;
    convert_tz: no
  }

  dimension_group: store_close {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.STORE_CLOSE_DATE ;;
  }

  dimension_group: pop_no_tz {
    sql: ${stock_raw} ;;
  }


  dimension: stock_date_from_parameter {
    hidden: yes
    type: string
    sql:
    {% if timeframe_parameter._parameter_value == 'year' %}
      ${stock_year}
    {% elsif timeframe_parameter._parameter_value == 'quarter' %}
      ${stock_quarter}
    {% elsif timeframe_parameter._parameter_value == 'month' %}
      ${stock_month}
    {% else %}
      ${stock_month}
    {% endif %}
    ;;
  }

  dimension: gmroi_parameter {
    hidden: yes
    type: number
    sql:
    {% if timeframe_parameter._parameter_value == 'year' %}
      1
    {% elsif timeframe_parameter._parameter_value == 'quarter' %}
      4
    {% elsif timeframe_parameter._parameter_value == 'month' %}
      12
    {% else %}
      12
    {% endif %}
    ;;
  }

#######################################
#####             TIME            #####
#######################################

  dimension_group: first_received {
    hidden: no
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.first_received AS TIMESTAMP) ;;
  }

  dimension_group: first_sold {
    hidden: yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.first_sold AS TIMESTAMP) ;;
  }


#######################################
#####         DIMENSIONS          #####
#######################################
  dimension: bu_code_final {
    type: string
    label: "BU Code"
    sql: ${TABLE}.business_unit_code ;;
  }

  dimension: is_closed {
    type: yesno
    label: "Is Closed Now?"
    sql: ${store_close_date} IS NOT NULL ;;
  }

  dimension: is_closed_store{
    type: yesno
    label: "Is Closed?"
    sql: ${store_close_date} <= ${stock_date};;
  }

  dimension: is_invalid_store{
    type: yesno
    label: "Is Valid Store?"
    sql: ${store_name} NOT LIKE "%PODIUM%" AND ${store_name} NOT LIKE "%KIOSK%" AND ${store_name} NOT LIKE "%STAFF%"
      ;;
  }


  dimension: is_valid_stock_date {
    hidden: yes
    description: "Checks that First Received Date is before the Stock Date"
    type: yesno
    sql: ${first_received_date} < ${stock_date} ;;
  }

  dimension: pk {
    primary_key: yes
    type: string
    hidden:yes
    sql: CONCAT(${prod_num}, ' - ' ,${org_num}, ' - ', CAST(${stock_date} AS STRING)) ;;
  }

  dimension: month_no {
    hidden: yes
    type: number
    sql: EXTRACT(MONTH FROM ${stock_date})
      ;;
  }
  dimension: selling_sqm {
    group_label: "Locations"
    label: "Selling sqm"
    hidden: no
    type: number
    value_format_name: decimal_0
    sql: ${TABLE}.selling_square_ft
      ;;
  }

  measure: sales_productivity {
    type: sum
    group_label: "Sales Metrics"
    sql: ${sales_amt_usd}/(${TABLE}.selling_square_ft) ;;
    label: "Sales Productivity per sqm"
    value_format_name: usd
  }
  measure: sales_avg {
    type: average
    group_label: "Sales Metrics"
    sql: ${sales_amt_usd} ;;
    label: "Sales avg"
    value_format_name: usd
  }
  dimension: granularity_from_parameter {
    view_label: "- Parameters"
    type: string
    sql:
    {% if granularity_of_analysis_parameter._parameter_value == 'overall' %}
      true
    {% elsif granularity_of_analysis_parameter._parameter_value == 'brand' %}
      ${brand}
    {% elsif granularity_of_analysis_parameter._parameter_value == 'district' %}
      ${district_name}
    {% elsif granularity_of_analysis_parameter._parameter_value == 'month' %}
      ${stock_month}
    {% elsif granularity_of_analysis_parameter._parameter_value == 'week' %}
      ${stock_week}
    {% elsif granularity_of_analysis_parameter._parameter_value == 'date' %}
      ${stock_date}
    {% elsif granularity_of_analysis_parameter._parameter_value == 'item' %}
      ${prod_num}
    {% elsif granularity_of_analysis_parameter._parameter_value == 'item_no' %}
      ${prod_num}
    {% elsif granularity_of_analysis_parameter._parameter_value == 'item_vpn' %}
      ${item_vpn}
    {% elsif granularity_of_analysis_parameter._parameter_value == 'item_style_number' %}
      ${item_style}
    {% elsif granularity_of_analysis_parameter._parameter_value == 'class_name' %}
      ${class_name}
    {% elsif granularity_of_analysis_parameter._parameter_value == 'subclass_name' %}
      ${subclass_name}
    {% elsif granularity_of_analysis_parameter._parameter_value == 'taxonomy_1' %}
      ${taxo_class}
    {% elsif granularity_of_analysis_parameter._parameter_value == 'taxonomy_2' %}
      ${taxo_subclass}
    {% elsif granularity_of_analysis_parameter._parameter_value == 'colour' %}
      ${attrb_color}
    {% elsif granularity_of_analysis_parameter._parameter_value == 'size' %}
      ${size_uda}
    {% elsif granularity_of_analysis_parameter._parameter_value == 'gender' %}
      ${gender}
    {% elsif granularity_of_analysis_parameter._parameter_value == 'division' %}
      ${division}
    {% elsif granularity_of_analysis_parameter._parameter_value == 'season' %}
      ${season_desc}
    {% elsif granularity_of_analysis_parameter._parameter_value == 'business_unit' %}
      ${business_unit}
    {% elsif granularity_of_analysis_parameter._parameter_value == 'country' %}
      ${country}
    {% elsif granularity_of_analysis_parameter._parameter_value == 'boat' %}
      ${boat}
    {% elsif granularity_of_analysis_parameter._parameter_value == 'store' %}
      ${store_name}
    {% else %}
      true
    {% endif %}
    ;;
    label: "@{granularity_dimension_label}"
  }

  dimension: granularity_from_parameter_b {
    view_label: "- Parameters"
    type: string
    sql:
    {% if granularity_of_analysis_parameter_b._parameter_value == 'overall' %}
      true
    {% elsif granularity_of_analysis_parameter_b._parameter_value == 'brand' %}
      ${brand}
    {% elsif granularity_of_analysis_parameter_b._parameter_value == 'district' %}
      ${district_name}
    {% elsif granularity_of_analysis_parameter_b._parameter_value == 'month' %}
      ${stock_month}
    {% elsif granularity_of_analysis_parameter_b._parameter_value == 'week' %}
      ${stock_week}
    {% elsif granularity_of_analysis_parameter_b._parameter_value == 'date' %}
      ${stock_date}
    {% elsif granularity_of_analysis_parameter_b._parameter_value == 'item' %}
      ${prod_num}
    {% elsif granularity_of_analysis_parameter_b._parameter_value == 'item_no' %}
      ${prod_num}
    {% elsif granularity_of_analysis_parameter_b._parameter_value == 'item_vpn' %}
      ${item_vpn}
    {% elsif granularity_of_analysis_parameter_b._parameter_value == 'item_style_number' %}
      ${item_style}
    {% elsif granularity_of_analysis_parameter_b._parameter_value == 'class_name' %}
      ${class_name}
    {% elsif granularity_of_analysis_parameter_b._parameter_value == 'subclass_name' %}
      ${subclass_name}
    {% elsif granularity_of_analysis_parameter_b._parameter_value == 'taxonomy_1' %}
      ${taxo_class}
    {% elsif granularity_of_analysis_parameter_b._parameter_value == 'taxonomy_2' %}
      ${taxo_subclass}
    {% elsif granularity_of_analysis_parameter_b._parameter_value == 'colour' %}
      ${attrb_color}
    {% elsif granularity_of_analysis_parameter_b._parameter_value == 'size' %}
      ${size_uda}
    {% elsif granularity_of_analysis_parameter_b._parameter_value == 'gender' %}
      ${gender}
    {% elsif granularity_of_analysis_parameter_b._parameter_value == 'division' %}
      ${division}
    {% elsif granularity_of_analysis_parameter_b._parameter_value == 'season' %}
      ${season_desc}
    {% elsif granularity_of_analysis_parameter_b._parameter_value == 'business_unit' %}
      ${business_unit}
    {% elsif granularity_of_analysis_parameter_b._parameter_value == 'country' %}
      ${country}
    {% elsif granularity_of_analysis_parameter_b._parameter_value == 'boat' %}
      ${boat}
    {% elsif granularity_of_analysis_parameter_b._parameter_value == 'store' %}
      ${store_name}
    {% else %}
      true
    {% endif %}
    ;;
    label: "@{granularity_dimension_label_b}"
  }

#######################################
#####          LOCS-DIMS          #####
#######################################

  dimension: boat{
    group_label: "Locations"
    description: "Boat is subvertical value from alternate hierarchy"
    type: string
    sql: ${TABLE}.sub_vertical ;;
  }

  dimension: alt_storeage_location_bucket {
    type: string
    hidden: yes
    view_label: "Location Traits"
    label: "Level Season Bucket"
    description: "Season bucket based on location trait"
    sql: CASE
          WHEN  REPLACE(${loc_season}, ' ', '') IN ("SS21P", "SS21M", "SS21B") THEN "SS21"
          WHEN  REPLACE(${loc_season}, ' ', '') IN ("FW21P", "FW21M", "FW21B") THEN "FW21"
          WHEN  REPLACE(${loc_season}, ' ', '') IN ("SS22P", "SS22M", "SS22B") THEN "SS22"
          WHEN  REPLACE(${loc_season}, ' ', '') IN ("FW22P", "FW22M", "FW22B") THEN "FW22"
          WHEN  ${loc_season} LIKE "BASIC" THEN "Basic"
          WHEN  ${loc_season} LIKE "REGULAR" THEN "Regular"
          WHEN  ${loc_season} LIKE "%C" OR  ${loc_season}  LIKE "%CO" THEN "Carry-over"
          WHEN  ${loc_season} IS NULL THEN "No Season Info"
          ELSE "Previous Seasons"
          END;;
  }

  dimension: alt_storeage_location_bucket_2 {
    type: string
    hidden: yes
    view_label: "Location Traits"
    label: "Level Season Bucket 2"
    description: "Season bucket based on location trait"
    sql: CASE WHEN  ${loc_season} LIKE "BASIC" THEN "Basic"
          ELSE "Seasonal"
          END;;
  }


  dimension: country_id{
    group_label: "Locations"
    description: "Country ID i.e. AE for United Arab Emirates"
    type: string
    suggest_explore: dim_retail_loc_country
    suggest_dimension: dim_retail_loc_country.country_id
    sql: ${TABLE}.country_id ;;
  }

  dimension: is_last_day {
    type: yesno
    sql: ${last_date_2}=${stock_date} ;;
  }

  dimension: country{
    group_label: "Locations"
    description: "Country value from Oracle i.e. United Arab Emirates"
    type: string
    suggest_explore: dim_retail_loc_country_desc
    suggest_dimension: dim_retail_loc_country_desc.country_desc
    sql: ${TABLE}.country_desc ;;
  }

  dimension: store{
    group_label: "Locations"
    label: "Location ID"
    description: "Warehouse and Store IDs coming from Oracle RMS"
    type: number
    hidden: yes
    value_format: "0"
    suggest_explore: dim_retail_loc_store
    suggest_dimension: dim_retail_loc_store.location_id
    sql: ${TABLE}.loc_code ;;
  }

  dimension: district_name{
    group_label: "Locations"
    description: "District Name coming from Oracle RMS"
    type: string
    suggest_explore: dim_retail_loc_district_name
    suggest_dimension: dim_retail_loc_district_name.district_name
    sql: ${TABLE}.district_name ;;
  }

# SPD-329
  dimension: region_name{
    group_label: "Locations"
    description: "Region Name coming from Oracle RMS"
    type: string
    sql: ${TABLE}.region_name ;;
  }
# SPD-329
  dimension: store_name{
    group_label: "Locations"
    label: "Location Name"
    description: "Name of location in Oracle RMS"
    type: string
    suggest_explore: dim_retail_loc_store_name
    suggest_dimension: dim_retail_loc_store_name.loc_name
    sql: ${TABLE}.loc_name ;;
  }
  dimension: mall_name {
    group_label: "Locations"
    label: "Mall Name"
    description: "Name of Mall in Oracle RMS"
    type: string
    sql: ${TABLE}.mall_name ;;
  }
  dimension: Store1_name_segregation {
    group_label: "Locations"
    description: "Segregation of Offline-Mkt place - Ecom for FF1"
    label: "Channel FF1"
    type: string
    sql: CASE WHEN ${TABLE}.loc_name LIKE "%FARFETCH%" THEN "MKT PLACE"
    WHEN ${TABLE}.loc_name  LIKE "%ECOMMERCE%" THEN "ECOM"
    WHEN ${TABLE}.loc_name LIKE "%OUTLET%" THEN "OUTLET"    else "STORE" END;;
    }



  dimension: store_name_swk {
    view_label: "Locations"
    label: "Location Name(Swarovski BU)"
    description: "Location Name combined for Ecom Stock and Sales location for Swarovski"
    type: string
    suggest_explore: dim_retail_loc_store_name
    suggest_dimension: dim_retail_loc_store_name.store_name
    sql: CASE WHEN ${TABLE}.loc_name LIKE '%ECOMMERCE%' THEN CONCAT(UPPER(${TABLE}.business_unit)," - ",UPPER(${TABLE}.country_desc)," - ","ECOMMERCE")
      ELSE ${TABLE}.loc_name END;;
  }
  dimension: store_name_thedeal {
    view_label: "Locations"
    label: "Location Segregations (THE DEAL BU)"
    description: "Location Names segregated for THE DEAL into Ecom, Brick and Mortar and Liquidation"
    type: string
    suggest_explore: dim_retail_loc_store_name
    suggest_dimension: dim_retail_loc_store_name.store_name
    sql: CASE WHEN ${TABLE}.loc_name LIKE 'THE DEAL - ECOMMERCE%' THEN "ECOMMERCE"
              WHEN ${TABLE}.loc_name LIKE 'BEAUTY STAFF SALE' or  ${TABLE}.loc_name  LIKE 'FASHION STAFF SALE'  or  ${TABLE}.loc_name  LIKE 'ALLIED RETAIL THE DEAL WHOLESALE NORMAL STOCK' or  ${TABLE}.loc_name  LIKE 'THE DEAL - STAFF SALE'or  ${TABLE}.loc_name  LIKE 'THE DEAL - STAFF SALE (RAJAAN)'THEN "LIQUIDATION"
              WHEN ${TABLE}.loc_name LIKE 'THE SHOE DEAL - DUBAI OUTLET MALL' or  ${TABLE}.loc_name  LIKE 'THE KIDS DEAL - DUBAI OUTLET MALL' or ${TABLE}.loc_name  LIKE 'THE DEAL -%' THEN "BRICK & MORTAR"
              END;;
  }

#SPD-296
  dimension: store_name_segregation {
    group_label: "Locations"
    description: "Segregation of retail stores into channels"
    label: "Channel"
    type: string
    suggest_explore:  dim_retail_loc_store_name
    suggest_dimension: dim_retail_loc_store_name.store_name
    sql: CASE WHEN ${TABLE}.loc_name  LIKE "%LACOSTE - TRYANO%" THEN "TRYANO"
          WHEN  ${TABLE}.loc_name  LIKE "%LACOSTE - ECOMMERCE%" or  ${TABLE}.loc_name  LIKE "ALLIED RETAIL FASHION NORMAL STOCK" or  ${TABLE}.loc_name  LIKE "QATAR LUXURY FASHION WH NORMAL STOCK" or  ${TABLE}.loc_name  LIKE "FAROUK TRADING RETAIL FASHION RIYADH NORMAL STOCK" or ${TABLE}.loc_name  LIKE "FAROUK TRADING RETAIL FASHION JEDDAH NORMAL STOCK" THEN "ECOMMERCE"
          WHEN ${TABLE}.loc_name  LIKE "%LACOSTE - MERAAS OUTLET VILLAGE%" or  ${TABLE}.loc_name  LIKE "LACOSTE - NORTH COAST – ALEXANDRIA" THEN "OUTLET"
          WHEN ${TABLE}.loc_name  LIKE "LACOSTE -%"
          or  ${TABLE}.loc_name  LIKE "H&C RETAIL FASHION NORMAL STOCK"
          or  ${TABLE}.loc_name  LIKE "ASHRAF-BGDC FASHION WH NORMAL STOCK"
          or  ${TABLE}.loc_name  LIKE "ASHRAF-BGDC FASHION WH BOX DAMAGED"
          or  ${TABLE}.loc_name  LIKE "ASHRAF-BGDC FASHION WH OLD SEASON"
          or  ${TABLE}.loc_name  LIKE "ALLIED RETAIL FASHION HEAD OFFICE"
          or  ${TABLE}.loc_name  LIKE "ASHRAF-BGDC FASHION WH FULL DAMAGED"
          or  ${TABLE}.loc_name  LIKE "MAC FOR PROMOTING COMMERCIAL RETAIL FASHION CAIRO NORMAL STOCK"
          or  ${TABLE}.loc_name  LIKE "MAC FOR PROMOTING COMMERCIAL RETAIL FASHION CAIRO CROSS-DOCK"
          or  ${TABLE}.loc_name  LIKE "ALLIED RETAIL FASHION CONSIGNMENT FOL"
          or  ${TABLE}.loc_name  LIKE "MAC FOR PROMOTING COMMERCIAL RETAIL FASHION CAIRO OLD SEASON"
          or  ${TABLE}.loc_name  LIKE "ALLIED RETAIL FASHION PREVIOUS SEASON"
          or  ${TABLE}.loc_name  LIKE "FAROUK TRADING RETAIL FASHION JEDDAH CROSS-DOCK"
          or  ${TABLE}.loc_name  LIKE "FAROUK TRADING RETAIL FASHION RIYADH OLD SEASON"
          or  ${TABLE}.loc_name  LIKE "ALLIED RETAIL FASHION OLD SEASON"
          or  ${TABLE}.loc_name  LIKE "H&C RETAIL FASHION OLD SEASON"
          or  ${TABLE}.loc_name  LIKE "MAC FOR PROMOTING COMMERCIAL RETAIL  HEAD OFFICE"
          or  ${TABLE}.loc_name  LIKE "QATAR LUXURY FASHION WH OLD SEASON"
          or  ${TABLE}.loc_name  LIKE "FAROUK TRADING RETAIL FASHION JEDDAH OLD SEASON"
          or  ${TABLE}.loc_name  LIKE "FAROUK TRADING RETAIL FASHION RIYADH CROSS-DOCK" THEN "STORE"
          WHEN ${TABLE}.loc_name  LIKE "FACTORY OUTLET%" THEN "FACTORY OUTLET"
          WHEN ${TABLE}.loc_name  LIKE "THE DEAL - ECOMMERCE%" THEN "ECOMMERCE"
          WHEN ${TABLE}.loc_name  LIKE "THE DEAL -%" or ${TABLE}.loc_name  LIKE "THE SHOE DEAL -%" or ${TABLE}.loc_name  LIKE "THE KIDS DEAL -%" or ${TABLE}.loc_name  LIKE "RAJAAN RETAIL THE DEAL WHOLESALE NORMAL STOCK" or ${TABLE}.loc_name  LIKE "ALLIED RETAIL THE DEAL WHOLESALE NORMAL STOCK" THEN "STORE" END;;
  }
  dimension: country_code{
    group_label: "Locations"
    label: "Country Code"
    description: "Country code for location in Oracle RMS"
    type: string
    suggest_dimension: dim_retail_loc.country_code
    sql: CASE
        WHEN ${country_id} = "BH" THEN 'BAH'
        WHEN ${country_id} = "EG" THEN 'EGY'
        WHEN ${country_id} = "KW" THEN 'KWT'
        WHEN ${country_id} = "SA" THEN 'KSA'
        WHEN ${country_id} = "QA" THEN 'QAT'
        WHEN ${country_id} = "EF" THEN 'EGY'
        WHEN ${country_id} = "MA" THEN 'MAR'
        WHEN ${country_id} = "AE" THEN 'UAE'
        ELSE 'UAE'
        END ;;
  }

  dimension: vertical{
    group_label: "Locations"
    label: "Vertical"
    description: "Vertical attribute in Oracle RMS"
    type: string
    suggest_explore: dim_retail_loc_vertical
    suggest_dimension: dim_retail_loc_vertical.vertical
    sql: ${TABLE}.vertical ;;
  }

  dimension: chain{
    group_label: "Locations"
    label: "Chain"
    description: "Chain name attribute in Oracle RMS"
    type: string
    sql: ${TABLE}.chain_name ;;
  }

  dimension: department_id {
    group_label: "Locations"
    label: "Department No."
    description: "Department_number attribute in Oracle RMS"
    type: string
    sql: ${TABLE}.dept_no ;;
  }

  dimension: department_name {
    group_label: "Locations"
    label: "Department "
    description: "Department name attribute in Oracle RMS"
    type: string
    sql: ${TABLE}.dept_name ;;
  }

  dimension: channel_name {
    type: string
    group_label: "Locations"
    label: "Channel"
    hidden: no
    sql: ${TABLE}.channel_name ;;
  }


  dimension: business_unit{
    group_label: "Locations"
    description: "Business unit attribute in Oracle RMS"
    type: string
    suggest_explore: dim_retail_loc_bu
    suggest_dimension: dim_retail_loc_bu.business_unit
    sql: ${TABLE}.business_unit ;;
  }

  dimension: org_num {
    description: "Location number from Oracle RMS"
    hidden: no
    label: "Location Number"
    group_label: "Locations"
    suggest_explore: dim_retail_loc_store
    suggest_dimension: dim_retail_loc_store.store
    type: string
    sql: ${TABLE}.org_num ;;
  }

  dimension: is_warehouse {
    description: "Location number from Oracle RMS"
    hidden: no
    label: "Is Warehouse?"
    group_label: "Locations"
    type: yesno
    sql: BYTE_LENGTH(${org_num})>6;;
  }

  dimension: is_dept_store {
    description: "Is the store a department store?"
    hidden: no
    label: "Is Department Store?"
    group_label: "Locations"
    type: yesno
    sql: ${business_unit} IN ("LEVEL SHOES", "TRYANO", "SAKS");;
  }

  dimension: is_ecommerce {
    description: "Is it an ecomm sale?"
    hidden: no
    label: "Is Ecommerce?"
    group_label: "Locations"
    type: yesno
    sql: ${TABLE}.loc_name LIKE "%ECOMMERCE%" ;;
  }

  dimension: stock_type {
    description: "Stock Type(Ecom/WH/Store)"
    hidden: no
    label: "Stock Type(Ecom/WH/Store)"
    group_label: "Locations"
    type: string
    sql: CASE WHEN ${TABLE}.loc_name LIKE "%ECOMMERCE%" THEN "Ecom" WHEN BYTE_LENGTH(${org_num})>6 THEN "Warehouse" ELSE "Store" END ;;
  }
#######################################
#####          PROD-DIMS          #####
#######################################

  dimension: inv_soh_qty {
    type: number
    hidden: no
    label: "Inventory Quantity"
    description: "Daily Value of Iventory Stock on Hand(works only with date dimension)"
    group_label: "Non-Aggregate Dimensions"
    sql: ${TABLE}.inv_soh_qty ;;
  }

  dimension: is_novelty {
    group_label: "Products"
    label: "Is Novelty?"
    description: "Did we receive it more than 6 months ago(from the current date)?"
    type: yesno
    sql: date_diff(${first_received_date}, ${stock_date}, month) < 6 ;;
  }

  dimension: max_age {
    type: string
    group_label: "Stock Age"
    label: "Stock Age as per Last Date"
    sql: ${max_age.max_stock_age} ;;
  }

  dimension: age_bucket {
    group_label: "Stock Age"
    label: "First Received Age Bucket "
    type: tier
    tiers: [6,12,18,24,36,48]
    value_format_name: decimal_0
    style: relational
    sql:${max_age.max_stock_age};;
  }

  dimension: age_2 {
    type: number
    hidden: yes
    sql:  DATE_DIFF(CURRENT_DATE(), ${first_received_date}, MONTH) ;;
  }

  dimension: age_bucket_2 {
    type: tier
    group_label: "Stock Age"
    label: "STR % Age"
    tiers: [1,2,3,4,5,6,7,8,9]
    value_format_name: decimal_0
    style: relational
    sql:  ${age_2};;
  }

  dimension: previous_day_stock_qty {
    type: number
    hidden: yes
    group_label: "Products"
    sql: ${TABLE}.previous_day_stock_qty ;;
  }

  dimension: item_style{
    group_label: "Products"
    label: "Product Style"
    description: "Style ID from Oracle RMS"
    type: string
    suggest_explore: fact_soh_sales_poc
    suggest_dimension: dim_retail_prod.item_style
    sql: ${TABLE}.item_style ;;
  }

  dimension: gwp_or_plv_item {
    type: yesno
    group_label: "Products"
    sql: ${TABLE}.division = "GWP" OR ${TABLE}.division = "PLV";;
  }

  dimension: class_name{
    group_label: "Products"
    description: "Class name attribute from Oracle RMS"
    label: "Class"
    type: string
    suggest_explore: dim_retail_prod_class
    suggest_dimension: class_name
    sql: ${TABLE}.class_name ;;
  }

  dimension: class_no{
    group_label: "Products"
    description: "Class id attribute from Oracle RMS"
    label: "Class ID"
    type: string
    sql: ${TABLE}.class ;;
  }

  dimension: class_no_2{
    group_label: "Products"
    description: "Class id when BU is not a department store (Level Shoes, Tryano, Saks)"
    label: "Class ID 2"
    hidden: no
    type: string
    sql: CASE WHEN ${is_dept_store} = TRUE THEN NULL ELSE ${class_no} END ;;
  }

  dimension: subclass_name{
    group_label: "Products"
    label: "Sub Class"
    description: "Oracle RMS product subclass"
    type: string
    suggest_explore: dim_retail_prod_sub_class
    suggest_dimension: dim_retail_prod_sub_class.subclass_name
    sql: ${TABLE}.subclass_name ;;
  }


  dimension: subclass_no{
    group_label: "Products"
    label: "Sub Class No"
    description: "Oracle RMS product subclass no"
    hidden: yes
    type: string
    sql: ${TABLE}.subclass ;;
  }


  dimension: subclass_no_2{
    hidden: yes
    type: string
    sql: CASE WHEN ${is_dept_store} = TRUE THEN NULL ELSE ${subclass_no} END ;;
  }

  dimension: taxo_class{
    group_label: "Products"
    label: "Taxonomy Class"
    description: "Taxonomy Class from Oracle RMS"
    type: string
    suggest_explore: dim_retail_prod_taxo_class
    suggest_dimension: dim_retail_prod_taxo_class.taxo_class
    sql: ${TABLE}.taxo_class ;;
  }

  dimension: item_desc{
    group_label: "Products"
    label: "Description"
    description: "Description from Oracle RMS"
    type: string
    suggest_dimension: dim_retail_prod.item_desc
    sql: ${TABLE}.item_desc ;;
  }

  dimension: brand{
    group_label: "Products"
    description: "Brand attribute from Oracle RMS"
    type: string
    suggest_explore: dim_retail_prod_brand
    suggest_dimension: dim_retail_prod_brand.brand
    sql: ${TABLE}.brand ;;
  }

  dimension: taxo_subclass{
    group_label: "Products"
    label: "Taxonomy Subclass"
    description: "Taxonomy subclass attribute from RMS"
    type: string
    suggest_explore: dim_retail_prod_taxo_subclass
    suggest_dimension: dim_retail_prod_taxo_subclass.taxo_subclass
    sql: ${TABLE}.taxo_subclass ;;
  }

  dimension: attrb_color{
    group_label: "Products"
    label: "Colour"
    description: "Colour attribute from Oracle RMS"
    type: string
    suggest_explore: dim_retail_prod_colour
    suggest_dimension: dim_retail_prod_colour.colour
    sql: ${TABLE}.attrb_color ;;
  }

  dimension: size_uda{
    group_label: "Products"
    description: "Size attribute from Oracle RMS"
    type: string
    label: "Size"
    suggest_explore: dim_retail_prod_size
    suggest_dimension: dim_retail_prod_size.size
    sql: ${TABLE}.size_uda ;;
  }

  dimension: item_vpn{
    group_label: "Products"
    description: "Item VPN attribute from Oracle RMS"
    type: string
    label: "VPN"
    suggest_dimension: dim_retail_prod.item_vpn
    sql: ${TABLE}.vpn ;;
  }

  dimension: gender{
    group_label: "Products"
    description: "Gender attribute from Oracle RMS"
    type: string
    suggest_explore: dim_retail_prod_gender
    suggest_dimension: dim_retail_prod_gender.gender
    sql: ${TABLE}.gender ;;
  }
  dimension: gender_brand_specific {
    group_label: "Products"
    description: "Gender with Kids, Newborn, Infant, Toddler, Layette under Kids"
    label: "Gender"
    type: string
    suggest_explore:  dim_retail_prod_gender
    suggest_dimension: dim_retail_prod_gender.gender
    sql: CASE WHEN ${TABLE}.gender  LIKE "ANIMAL" THEN "ANIMAL" WHEN ${TABLE}.gender  LIKE "MEN" THEN "MEN" WHEN ${TABLE}.gender  LIKE "WOMEN" THEN "WOMEN" WHEN ${TABLE}.gender  LIKE "UNISEX" THEN "UNISEX" WHEN ${TABLE}.gender  LIKE "NEUTRAL" THEN "NEUTRAL" WHEN ${TABLE}.gender  LIKE "%KIDS%" or  ${TABLE}.gender LIKE "%INFANT%"   or ${TABLE}.gender LIKE "%TODDLER%" or  ${TABLE}.gender LIKE "LAYETTE" or  ${TABLE}.gender LIKE "NEWBORN" THEN "KIDS" WHEN ${TABLE}.gender is Null  THEN "UNSPECIFIED" END;;
  }

  dimension: sep_category{
    group_label: "Products"
    type: string
    hidden: yes
    suggest_dimension: dim_retail_prod.gendersep_category
    sql: ${TABLE}.sep_category ;;
  }

  dimension: division{
    description: "Division attribute from Oracle RMS"
    group_label: "Products"
    type: string
    suggest_explore: dim_retail_prod_division
    suggest_dimension: dim_retail_prod_division.division
    sql: ${TABLE}.division ;;
  }
  parameter: budget_selection {
    label: "Budget selection (USD)"
    view_label: "Parameters"
    type: string
    default_value: "R2"
    allowed_value: {
      label: "R0"
      value: "R0"
    }
    allowed_value: {
      label: "R1"
      value: "R1"
    }
    allowed_value: {
      label: "R2"
      value: "R2"
    }
  }
  measure: budgets_from_param2{
    type: number
    label: "Budget (USD) based on user requirement"
    value_format_name: usd
    description: "Sales budget based on input from the user (R0,R1 or R2) "
    group_label: "Budget"
    sql:
    {% if budget_selection._parameter_value == "'R2'" %}
    ${budgetstarget.budget_r2_usd}
    {% elsif budget_selection._parameter_value == "'R1'" %}
    ${budgetstarget.budget_r1_usd}
    {% else %}
    ${budgetstarget.budget_r0_usd}
    {% endif %};;
  }
 dimension: month_name {
    case: {
      when: {
        sql: ${month_no}= 1 ;;
        label: "January"
      }
      when: {
        sql: ${month_no}= 2 ;;
        label: "February"
      }
      when: {
        sql: ${month_no}= 3 ;;
        label: "March"
      }
      when: {
        sql: ${month_no}= 4 ;;
        label: "April"
      }
      when: {
        sql: ${month_no}= 5 ;;
        label: "May"
      }
      when: {
        sql: ${month_no}= 6 ;;
        label: "June"
      }
      when: {
        sql: ${month_no}= 7 ;;
        label: "July"
      }
      when: {
        sql: ${month_no}= 8 ;;
        label: "August"
      }
      when: {
        sql: ${month_no}= 9 ;;
        label: "September"
      }
      when: {
        sql: ${month_no}= 10 ;;
        label: "October"
      }
      when: {
        sql: ${month_no}= 11 ;;
        label: "November"
      }
      when: {
        sql: ${month_no}= 12 ;;
        label: "December"
      }
    }
  }
  dimension: line{
    description: "Line attribute from Oracle RMS"
    group_label: "Products"
    type: string
    sql: ${TABLE}.line ;;
  }

  dimension: sub_line{
    description: "Line attribute from Oracle RMS"
    group_label: "Products"
    type: string
    sql: ${TABLE}.sub_line ;;
  }
  dimension: Category_basedon_division {
    group_label: "Products"
    description: "Product categories for Lacoste"
    label: "Category"
    type: string
    sql: CASE WHEN ${TABLE}.division ="APPAREL" AND  ${TABLE}.sub_line LIKE "%Basic%" Then "BASIC"
              WHEN ${TABLE}.division ="APPAREL" AND  ${TABLE}.sub_line LIKE "%BASIC%" Then "BASIC"
              WHEN ${TABLE}.division ="APPAREL" AND  ${TABLE}.sub_line LIKE "%FASHION%" Then "FASHION"
              WHEN ${TABLE}.division ="APPAREL" Then "APPAREL"
              WHEN ${TABLE}.division ="APPAREL COMPLEMENTS" AND  ${TABLE}.sub_line LIKE "%RC%" Then "BELTS"
              WHEN ${TABLE}.division = "APPAREL COMPLEMENTS" THEN "ACCESSORIES"
              WHEN ${TABLE}.division = "BATH & BEACHWEAR" Then "APPAREL"
              WHEN ${TABLE}.division = "EYE WEAR" Then "EYE WEAR"
              WHEN ${TABLE}.division = "FOOTWEAR" Then "FOOTWEAR"
              WHEN ${TABLE}.division = "FRAGRANCES" Then "FRAGRANCES"
              WHEN ${TABLE}.division = "GWP" Then "GWP"
              WHEN ${TABLE}.division = "WATCHES" Then "WATCHES"
      WHEN ${TABLE}.division = "SERVICE ITEMS" Then "SERVICE ITEMS"
      WHEN ${TABLE}.division = "PLV" Then "PLV"
      WHEN ${TABLE}.division = "LEATHER GOODS & LUGGAGE" AND  ${TABLE}.sub_line LIKE "%BAG%" Then "BAGS"
      WHEN ${TABLE}.division = "LEATHER GOODS & LUGGAGE" AND  ${TABLE}.sub_line LIKE "%SLG%" Then "SLG"
      WHEN ${TABLE}.division = "LEATHER GOODS & LUGGAGE" Then "LEATHER GOODS & LUGGAGE"
      WHEN ${TABLE}.division = "MEDICARE" Then "MEDICARE"
      WHEN ${TABLE}.division = "INTIMATE" Then "INTIMATE"
      WHEN ${TABLE}.division = "CHARITY" Then "CHARITY" END;;
  }
  dimension: Category_basedon_division_TB {
    group_label: "Products"
    description: "Product categories for Tory Burch"
    label: "Category"
    type: string
    sql:     CASE WHEN fact_soh_sales_poc.division ="APPAREL" AND  fact_soh_sales_poc.sub_line LIKE "%Basic%" Then "BASIC"
              WHEN fact_soh_sales_poc.division ="APPAREL" AND  fact_soh_sales_poc.sub_line LIKE "%BASIC%" Then "BASIC"
              WHEN fact_soh_sales_poc.division ="APPAREL" AND  fact_soh_sales_poc.sub_line LIKE "%FASHION%" Then "FASHION"
              WHEN fact_soh_sales_poc.division ="APPAREL" Then "RTW"
              WHEN fact_soh_sales_poc.division ="APPAREL COMPLEMENTS" AND  fact_soh_sales_poc.sub_line LIKE "%RC%" Then "BELTS"
              WHEN fact_soh_sales_poc.division = "APPAREL COMPLEMENTS" THEN "ACC"
              WHEN fact_soh_sales_poc.division = "COSTUME JEWELRY" THEN "ACC"
              WHEN fact_soh_sales_poc.division = "JEWELRY" THEN "ACC"
              WHEN fact_soh_sales_poc.division = "DECORATION" THEN "ACC"
              WHEN fact_soh_sales_poc.division = "TOYS & GAMES" THEN "ACC"
              WHEN fact_soh_sales_poc.division = "BATH & BEACHWEAR" Then "RTW"
              WHEN fact_soh_sales_poc.division = "EYE WEAR" Then "EYE WEAR"
              WHEN fact_soh_sales_poc.division = "FOOTWEAR" Then "SHOES"
              WHEN fact_soh_sales_poc.division = "FRAGRANCES" Then "FRAGRANCES"
              WHEN fact_soh_sales_poc.division = "AMBIENT SCENTS" Then "FRAGRANCES"
              WHEN fact_soh_sales_poc.division = "GWP" Then "GWP"
              WHEN fact_soh_sales_poc.division = "WATCHES" Then "WATCHES"
              WHEN fact_soh_sales_poc.division = "LIGHT ELECTRONICS" Then "WATCHES"
              WHEN fact_soh_sales_poc.division = "SERVICE ITEMS" Then "SERVICE ITEMS"
              WHEN fact_soh_sales_poc.division = "PLV" Then "PLV"
              WHEN fact_soh_sales_poc.division = "LEATHER GOODS & LUGGAGE" AND  fact_soh_sales_poc.taxo_class LIKE "%BAG%" Then "HANDBAGS"
              WHEN fact_soh_sales_poc.division = "LEATHER GOODS & LUGGAGE" AND  fact_soh_sales_poc.taxo_class LIKE "%SMALL LEATHER GOODS%" Then "SLG"
              WHEN fact_soh_sales_poc.division = "LEATHER GOODS & LUGGAGE" Then "HANDBAGS"
              WHEN fact_soh_sales_poc.division = "MEDICARE" Then "MEDICARE"
              WHEN fact_soh_sales_poc.division = "INTIMATE" Then "INTIMATE"
              WHEN fact_soh_sales_poc.division = "CHARITY" Then "CHARITY"
              WHEN fact_soh_sales_poc.division = "SERVICE MATERIAL" Then "SERVICE MATERIAL"
              END;; }

      dimension: season_desc{
        group_label: "Products"
        description: "Season attribute from Oracle RMS"
        label: "Season"
        type: string
        suggest_explore: dim_retail_prod_season
        suggest_dimension: dim_retail_prod_season.season
        sql: ${TABLE}.season_desc ;;
      }

      dimension: loc_season{
        group_label: "Products"
        description: "Season attribute from Oracle RMS"
        label: "Season(location)"
        type: string
        suggest_explore: dim_retail_prod_season
        suggest_dimension: dim_retail_prod_season.season
        sql: ${TABLE}.loc_season ;;
      }

      dimension: season_desc_for_reporting {
        group_label: "Products"
        description: "Season attribute from Oracle RMS"
        label: "Season Buckets"
        hidden: yes
        type: string
        suggest_explore: dim_retail_prod_season
        suggest_dimension: dim_retail_prod_season.season
        sql: CASE WHEN ${TABLE}.season_desc  LIKE "%BASIC%" OR  ${TABLE}.season_desc  LIKE "%REGULAR%"   THEN "Basic/Regular" WHEN ${TABLE}.season_desc  LIKE "%2021%" THEN "2021"  WHEN ${TABLE}.season_desc  LIKE "%2020%" THEN "Current" WHEN ${TABLE}.season_desc  LIKE "%2019%" THEN "Last Year" ELSE "Older" END;;
      }

      dimension: is_in_stock {
        type: yesno
        description: "Checks if the value of stock is greater than 0 and if product status is active"
        group_label: "Products"
        sql: ${inv_soh_qty} > 0 AND ${status} = 'A';;
      }

      dimension: price_local {
        type: number
        value_format_name: decimal_2
        group_label: "Products"
        label: "Retail Selling Price"
        hidden: yes
        sql: ${TABLE}.price_local;;
      }


      dimension: price_usd {
        type: number
        value_format_name: usd
        group_label: "Products"
        label: "Retail Selling Price USD"
        hidden: yes
        sql: ${price_local}*${conversion_rate};;
      }

      #SPD-332
      dimension: unit_retail_usd {
        type: number
        value_format_name: usd
        group_label: "Products"
        label: "Retail Selling Price USD"
        description: "Unit Retail price in USD. Only exists in the datamart as of April' 2020."
        hidden: no
        sql: ${TABLE}.unit_retail_usd;;
      }

      # SPD -346
      dimension: rsp_buckets_fashion {
        group_label: "Products"
        description: "Segregation of Retail Selling Price into buckets. Only exists in the datamart as of April' 2020."
        label: "RSP Buckets - Fashion"
        type: string
        sql: CASE WHEN ${unit_retail_usd} >= 0 AND ${unit_retail_usd} < 100 THEN '0 to 99'
              WHEN ${unit_retail_usd} >= 100 AND ${unit_retail_usd} < 200 THEN '100 to 199'
              WHEN ${unit_retail_usd}>= 200 AND ${unit_retail_usd}< 300 THEN '200 to 299'
              WHEN ${unit_retail_usd}>= 300 AND ${unit_retail_usd} < 400 THEN '300 to 399'
              WHEN ${unit_retail_usd} >= 400 AND ${unit_retail_usd} < 500 THEN '400 to 499'
              WHEN ${unit_retail_usd} >= 500 AND ${unit_retail_usd} < 600 THEN '500 to 599'
              WHEN ${unit_retail_usd} >= 600 AND ${unit_retail_usd} < 700 THEN '600 to 699'
              WHEN ${unit_retail_usd} >= 700 AND ${unit_retail_usd} < 800 THEN '700 to 799'
              WHEN ${unit_retail_usd} >= 800 AND ${unit_retail_usd} < 900 THEN '800 to 899'
              WHEN ${unit_retail_usd} >= 900 AND ${unit_retail_usd} < 1000 THEN '900 to 999'
              WHEN ${unit_retail_usd} >= 1000 AND ${unit_retail_usd} < 1500 THEN '1000 to 1499'
              WHEN ${unit_retail_usd} >= 1500 THEN '1500+'
              ELSE 'Undefined'
END;;
      }

      dimension: rsp_buckets_beauty {
        group_label: "Products"
        description: "Segregation of Retail Selling Price into buckets. Only exists in the datamart as of April' 2020."
        label: "RSP Buckets - Beauty"
        type: string
        sql: CASE WHEN ${unit_retail_usd} >= 0 AND ${unit_retail_usd} < 25 THEN '0 to 24'
              WHEN ${unit_retail_usd} >= 25 AND ${unit_retail_usd} < 50 THEN '25 to 49'
              WHEN ${unit_retail_usd}>= 50 AND ${unit_retail_usd}< 75 THEN '50 to 74'
              WHEN ${unit_retail_usd}>= 75 AND ${unit_retail_usd} < 100 THEN '75 to 99'
              WHEN ${unit_retail_usd} >= 100 AND ${unit_retail_usd} < 125 THEN '100 to 124'
              WHEN ${unit_retail_usd} >= 125 AND ${unit_retail_usd} < 150 THEN '125 to 149'
              WHEN ${unit_retail_usd} >= 150 AND ${unit_retail_usd} < 175 THEN '150 to 174'
              WHEN ${unit_retail_usd} >= 175 AND ${unit_retail_usd} < 200 THEN '175 to 199'
              WHEN ${unit_retail_usd} >= 200 AND ${unit_retail_usd} < 225 THEN '200 to 224'
              WHEN ${unit_retail_usd} >= 225 AND ${unit_retail_usd} < 250 THEN '225 to 249'
              WHEN ${unit_retail_usd} >= 250 AND ${unit_retail_usd} < 275 THEN '250 to 274'
              WHEN ${unit_retail_usd} >= 275 AND ${unit_retail_usd} < 300 THEN '275 to 299'
              WHEN ${unit_retail_usd} >= 300 THEN '300+'
              ELSE 'Undefined'
END;;
      }

      dimension: consignment
      {
        type: yesno
        label: "Is Consignment?"
        group_label: "Products"
        sql: ${consignment_flag} = "Y" ;;
      }

      dimension: currency {
        group_label: "Products"
        label: "Currency"
        type: string
        sql: ${TABLE}.atr_homecurrency;;
      }

      dimension: made_of {
        group_label: "Products"
        label: "Made of"
        type: string
        sql: ${TABLE}.attrb_made_of;;
      }


      dimension: is_novelty_now {
        group_label: "Products"
        label: "Is Novelty Now?"
        description: "Did we receive it more than 6 months ago(from the current date)?"
        type: yesno
        sql: date_diff(${first_received_date}, current_date, month) < 6 ;;
      }

      dimension: sales_qty {
        hidden: yes
        type: number
        group_label: "Products"
        sql: ${TABLE}.sales_qty ;;
      }

      dimension: status {
        description: "The status of the product in Oracle"
        group_label: "Products"
        type: string
        suggestions: ["A", "C", "I", "D"]
        sql: ${TABLE}.status ;;
      }

      dimension: prod_num {
        description: "Product number from Oracle RMS"
#     hidden: yes
        type: string
        label: "Product ID"
        suggest_dimension: dim_retail_prod.item
        group_label: "Products"
        sql: ${TABLE}.prod_num ;;
#     sql: ${TABLE}.item ;;
      }

      dimension: unit_cost_usd {
        type: number
        group_label: "Products"
        label: "WAC in USD"
        description: "Average Unit Cost (WAC) in USD"
        hidden: no
        value_format_name: decimal_2
        sql: ${TABLE}.unit_cost_usd ;;
      }

      dimension: unit_cost {
        type: number
        group_label: "Products"
        label: "WAC in local currency"
        description: "Average Unit Cost (WAC) in local currency"
        hidden: no
        value_format_name: decimal_2
        sql: ${TABLE}.unit_cost ;;
      }

# was Table.prev before - changed to previous_day_unit_cost_usd to fix the issue in dashboard
      dimension: previous_day_unit_cost_usd {
        type: number
        group_label: "Products"
        hidden: yes
        value_format_name: decimal_2
        sql: ${TABLE}.previous_day_unit_cost_usd;;
      }
  dimension: previous_day_unit_cost {
    type: number
    group_label: "Products"
    hidden: yes
    value_format_name: decimal_2
    sql: ${TABLE}.previous_day_unit_cost;;
  }
#######################################
#####         OTHER-DIMS          #####
#######################################

      dimension: entity_type {
        type: string
        hidden: yes
        sql: ${TABLE}.entity_type  ;;
      }

      dimension: provision_factor {
        type: number
        hidden: yes
        sql: CASE WHEN ${entity_type} = "EXPIRY (FASHION)" THEN 0.05
            WHEN ${entity_type} = "FASHION" THEN 0.05
            WHEN ${entity_type} = "BEAUTY" THEN 0.025
            WHEN ${entity_type} = "COMMON" THEN 1
            WHEN ${entity_type} = "GIFT" THEN 0.025
            ELSE 0
            END;;
      }

      dimension: max_price_usd_by_country {
        type: number
        hidden: yes
        sql: ${TABLE}.max_price_usd_by_country;;
      }


      dimension: conversion_rate {
        type: number
        hidden: yes
        sql: ${TABLE}.conversion_rate;;
      }


      dimension: first_day_of_month {
        hidden: yes
        type: yesno
        sql: ${stock_day_of_month}=1 ;;
      }

      dimension: last_day_of_month {
        hidden: yes
        type: yesno
        sql: EXTRACT( DAY FROM DATE_ADD(${stock_raw}, INTERVAL 1 DAY)) = 1 ;;
      }

      dimension: last_date_2 {
        type: string
        hidden: yes
        sql: ${sell_through_on_stock.last_date} ;;
      }

      dimension: last_month_of_quarter {
        hidden: yes
        type: yesno
        sql: ${month_no} IN (3,6,9,12) ;;
      }

      measure: last_date {
        hidden: yes
        type: date
        sql:MAX(${stock_raw_date});;
      }

      dimension: last_month_of_year {
        hidden: yes
        type: yesno
        sql: ${month_no} = 12 ;;
      }

      dimension: sales_amt_usd {
        hidden: yes
        type: number
        sql: ${TABLE}.sales_amountusd_beforetax ;;
      }

      dimension: zone_3 {
        group_label: "Products"
        label: "Zone 3"
        description: "Zone 3 for Level Shoes"
        type: string
        sql: CASE WHEN ${brand}="GIANVITO ROSSI" and ${gender}="WOMEN" THEN "Direct Concession"
              WHEN ${dim_item_loc_traits.report_code} LIKE "%ACC%" AND ${dim_prod_org_concession.concession_product} IS FALSE THEN "Accessories"
              ELSE "Multibrand"
              END;;
      }

      dimension: uda_zone
      {
        type: string
        group_label: "Products"
        label: "UDA Zone"
        hidden: no
        sql: ${TABLE}.uda_zone ;;
      }

      dimension: trait_zone
      {
        type: string
        group_label: "Products"
        label: "Zone"
        description: "Zone value from location trait"
        hidden: no
        sql: ${TABLE}.report_code ;;
      }

      dimension: consignment_flag
      {
        type: string
        hidden: yes
        sql: ${TABLE}.consignment_flag ;;
      }

      measure: count_of_days
      {
        type: number
        sql: COUNT(DISTINCT${stock_date}) ;;
      }


  dimension: ltl_till_date {
    type: string
    view_label: "Like to Like Timeframe comparison"
    label: "LTL Timeframe"
    description: "For Like to like timeframe comparison"
    sql: CASE WHEN EXTRACT(MONTH FROM ${stock_date}) > EXTRACT(MONTH FROM CURRENT_TIMESTAMP) THEN "NO"
              WHEN EXTRACT(MONTH FROM ${stock_date}) < EXTRACT(MONTH FROM CURRENT_TIMESTAMP) THEN "YES"
              WHEN EXTRACT(MONTH FROM ${stock_date}) = EXTRACT(MONTH FROM CURRENT_TIMESTAMP) AND
              EXTRACT(DAY FROM ${stock_date}) >= EXTRACT(DAY FROM CURRENT_TIMESTAMP) THEN "NO"
              ELSE "YES" END;;
  }



#####################################
#####         MEASURES          #####
#####################################



      measure: month_opening_stock_value {
        type: sum
        hidden: no
        group_label: "Stock Metrics"
        label: "Month Opening SOH USD Value"
        value_format_name: usd
        filters: {
          field: first_day_of_month
          value: "yes"
        }
        sql: ${previous_day_unit_cost_usd} * ${previous_day_stock_qty};;
      }

      measure: month_opening_stock_value_loc {
        type: sum
        hidden: no
        group_label: "Stock Metrics"
        label: "Month Opening SOH Local Value"
        value_format_name: decimal_2
        filters: {
          field: first_day_of_month
          value: "yes"
        }
        sql: ${previous_day_unit_cost} * ${previous_day_stock_qty};;
      }


      measure: month_opening_stock_quantity {
        type: sum
        hidden: no
        group_label: "Stock Metrics"
        label: "Month Opening SOH quantity"
        value_format_name: decimal_0
        filters: {
          field: first_day_of_month
          value: "yes"
        }
        sql: ${previous_day_stock_qty};;
      }

      measure: month_closing_stock_value {
        type: sum
        hidden: no
        group_label: "Stock Metrics"
        label: "Month Closing SOH USD Value"
        value_format_name: usd
        filters: {
          field: last_day_of_month
          value: "yes"
        }
        sql: ${unit_cost_usd} * ${inv_soh_qty};;
      }

      measure: month_closing_stock_value_loc {
        type: sum
        hidden: no
        group_label: "Stock Metrics"
        label: "Month Closing SOH Local Value"
        value_format_name: decimal_2
        filters: {
          field: last_day_of_month
          value: "yes"
        }
        sql: ${unit_cost} * ${inv_soh_qty};;
      }

  measure: week_opening_stock_value {
    type: sum
    hidden: no
    label: "Week Opening SOH USD Value"
    group_label: "Stock Metrics"
    value_format_name: usd
    filters: {
      field: stock_day_of_week
      value: "Sunday"
    }
    sql: ${unit_cost_usd} * ${inv_soh_qty};;
  }

  measure: week_opening_stock_value_loc {
    type: sum
    hidden: no
    value_format_name: decimal_2
    label: "Week Opening SOH Local Value"
    group_label: "Stock Metrics"
    filters: {
      field: stock_day_of_week
      value: "Sunday"
    }
    sql: ${unit_cost} * ${inv_soh_qty};;
  }



      measure: week_closing_stock_value {
        type: sum
        hidden: no
        label: "Week Closing SOH USD Value"
        group_label: "Stock Metrics"
        value_format_name: usd
        filters: {
          field: stock_day_of_week
          value: "Saturday"
        }
        sql: ${unit_cost_usd} * ${inv_soh_qty};;
      }

      measure: week_closing_stock_value_loc {
        type: sum
        hidden: no
        value_format_name: decimal_2
        label: "Week Closing SOH Local Value"
        group_label: "Stock Metrics"
        filters: {
          field: stock_day_of_week
          value: "Saturday"
        }
        sql: ${unit_cost} * ${inv_soh_qty};;
      }


      measure: month_closing_stock_quantity {
        type: sum
        hidden: no
        group_label: "Stock Metrics"
        label: "Month Closing SOH quantity"
        value_format_name: decimal_0
        filters: {
          field: last_day_of_month
          value: "yes"
        }
        sql: ${inv_soh_qty};;
      }

      measure: week_closing_stock_quantity {
        type: sum
        hidden: no
        group_label: "Stock Metrics"
        label: "Week Closing SOH quantity"
        value_format_name: decimal_0
        filters: {
          field: stock_day_of_week
          value: "Saturday"
        }
        sql: ${inv_soh_qty};;
      }

      measure: last_day_stock_quantity {
        type: sum
        hidden: no
        group_label: "Stock Metrics"
        label: "Last Day closing SOH quantity"
        value_format_name: decimal_0
        filters: {
          field: is_last_day
          value: "yes"
        }
        sql: ${inv_soh_qty};;
      }

      measure: last_day_stock_value {
        type: sum
        hidden: no
        group_label: "Stock Metrics"
        label: "Last Day closing SOH USD Value"
        value_format_name: usd
        filters: {
          field: is_last_day
          value: "yes"
        }
        sql: ${unit_cost_usd} * ${inv_soh_qty};;
      }

      measure: last_day_stock_value_loc {
        type: sum
        hidden: no
        group_label: "Stock Metrics"
        label: "Last Day closing SOH Local Value"
        value_format_name: decimal_2
        filters: {
          field: is_last_day
          value: "yes"
        }
        sql: ${unit_cost} * ${inv_soh_qty};;
      }

      measure: daily_stock_quantity {
        type: sum
        hidden: no
        group_label: "Stock Metrics"
        label: "Daily closing SOH quantity"
        value_format_name: decimal_0
        sql: ${inv_soh_qty};;
      }

      measure: daily_stock_value {
        type: sum
        hidden: no
        group_label: "Stock Metrics"
        label: "Daily closing SOH USD Value"
        value_format_name: usd_0
        sql: ${unit_cost_usd} * ${inv_soh_qty};;
      }
  measure: daily_stock_retail_value {
    type: sum
    hidden: no
    group_label: "Stock Metrics"
    label: "Daily closing SOH USD Retail Value"
    value_format_name: usd_0
    sql: ${unit_retail_usd} * ${inv_soh_qty};;
  }
      measure: sell_through_rate_on_stock {
        type: number
        hidden:yes
        label: "Sell Through Rate on Stock % Old"
        value_format_name: percent_2
        sql: SAFE_DIVIDE(sum(${sales_qty}),(sum(${dm_soh_sales_last_day.last_day_stock_quantity_new})));;
      }

      measure: year_closing_stock_value {
        type: sum
        hidden: no
        group_label: "Stock Metrics"
        label: "Year closing SOH USD Value"
        value_format_name: usd
        filters: {
          field: last_day_of_month
          value: "yes"
        }
        filters: {
          field: last_month_of_year
          value: "yes"
        }
        sql: ${unit_cost_usd} * ${inv_soh_qty};;
      }

      measure: year_closing_stock_value_loc {
        type: sum
        hidden: no
        group_label: "Stock Metrics"
        label: "Year closing SOH Local Value"
        value_format_name: decimal_2
        filters: {
          field: last_day_of_month
          value: "yes"
        }
        filters: {
          field: last_month_of_year
          value: "yes"
        }
        sql: ${unit_cost} * ${inv_soh_qty};;
      }

      measure: year_closing_stock_quantity {
        type: sum
        hidden: no
        label: "Year closing SOH quantity"
        group_label: "Stock Metrics"
        value_format_name: decimal_0
        filters: {
          field: last_day_of_month
          value: "yes"
        }
        filters: {
          field: last_month_of_year
          value: "yes"
        }
        sql: ${inv_soh_qty};;
      }

      measure: quarter_closing_stock_value {
        type: sum
        hidden: no
        group_label: "Stock Metrics"
        label: "Quarter closing SOH USD Value"
        value_format_name: usd
        filters: {
          field: last_day_of_month
          value: "yes"
        }
        filters: {
          field: last_month_of_quarter
          value: "yes"
        }
        sql: ${unit_cost_usd} * ${inv_soh_qty};;
      }

      measure: quarter_closing_stock_value_loc {
        type: sum
        hidden: no
        group_label: "Stock Metrics"
        label: "Quarter closing SOH Local Value"
        value_format_name: decimal_2
        filters: {
          field: last_day_of_month
          value: "yes"
        }
        filters: {
          field: last_month_of_quarter
          value: "yes"
        }
        sql: ${unit_cost} * ${inv_soh_qty};;
      }


      measure: quarter_closing_stock_quantity {
        type: sum
        hidden: no
        group_label: "Stock Metrics"
        label: "Quarter closing SOH quantity"
        value_format_name: decimal_0
        filters: {
          field: last_day_of_month
          value: "yes"
        }
        filters: {
          field: last_month_of_quarter
          value: "yes"
        }
        sql: ${inv_soh_qty};;
      }


      measure: sum_inv_soh_qty {
        type: sum
        hidden: yes
        value_format_name: decimal_2
        sql: ${inv_soh_qty} ;;
      }


      measure: total_cost_of_goods_sold {
        type: sum
        sql: ${cost_of_goods_sold} ;;
        label: "Total Cost of Goods Sold USD"
        group_label: "Cost of Goods Sold"
        description: "Average unit cost in USD multiplied by quantity sold"
        value_format_name: usd
      }

      measure: total_cost_of_goods_sold_local {
        type: sum
        sql: ${cost_of_goods_sold_local} ;;
        group_label: "Cost of Goods Sold"
        label: "Total Cost of Goods Sold Local"
        description: "Average unit cost (in local currency) multiplied by quantity sold"
        value_format_name: decimal_2
      }

      measure: maximum_stock_date {
        type: max
        hidden: yes
        sql: ${stock_date} ;;
      }


# measure: gross_margin {
#   type: sum
#   hidden: no
#   label: "Gross Margin USD"
#   sql: ${sales_amt_usd}-${unit_cost_usd}*ABS(${sales_qty}) ;;
#   value_format_name: usd
# }
#
# measure: gross_margin_share {
#     type: number
#     hidden: no
#     label: "Gross Margin %"
#     value_format_name: percent_2
#     sql: SAFE_DIVIDE(SUM(${sales_amt_usd}-${unit_cost_usd}*${sales_qty}),SUM(${sales_amt_usd})) ;;
#   }

# coalesce issue raised on 14th july 2020 #  SPD-257 changes starts
      measure: gross_margin {
        type: number
        hidden: no
        label: "Gross Margin USD"
        group_label: "Gross Margin"
        sql: ${total_sales_amt_usd} - ${total_cost_of_goods_sold} ;;
        value_format_name: usd
      }

      measure: gross_margin_share {
        type: number
        hidden: no
        label: "Gross Margin %"
        group_label: "Gross Margin"
        value_format_name: percent_2
        sql: SAFE_DIVIDE(${gross_margin},${total_sales_amt_usd}) ;;
      }

#  SPD-257 changes ends

      measure: total_quantity_sold {
        type: sum
        hidden: no
        description: "Sum of sales quantity"
        sql: ${sales_qty} ;;
        value_format_name: decimal_0
      }

      measure: total_quantity_sold_last_28_days {
        type: sum
        hidden: yes
        description: "Sum of sales quantity"
        sql: ${sales_qty} ;;
        value_format_name: decimal_0
        filters: {
          field: stock_date
          value: "last 28 days"
        }
      }


      measure: rolling_sum_28_days {
        type: number

        hidden: yes
        sql: SUM(${total_quantity_sold}) OVER(ORDER BY ${stock_raw} ROWS BETWEEN 27 PRECEDING AND CURRENT ROW) ;;
      }


      measure: total_sales_amt_usd {
        type: sum
        group_label: "Sales Metrics"
        label: "Net Sales Amount USD"
        description: "Sum of sales amount in USD"
        sql: ${sales_amt_usd} ;;
        value_format_name: usd
      }


      measure: highest_price_markdown {
        type: number
        value_format_name: percent_2
        group_label: "Highest Price Markdown"
        label: "Highest Price Markdown %"
        sql: 1-(SAFE_DIVIDE(SUM(${sales_amt_usd}),SUM(${sales_qty}*${max_price_usd_by_country}))) ;;

      }

      measure: highest_price_investment {
        type: number
        value_format_name: usd
        group_label: "Highest Price Markdown"
        label: "Highest Price Markdown USD"
        sql: ABS(SUM(${sales_amt_usd})-SUM(${sales_qty}*${max_price_usd_by_country}));;

      }



      measure: in_stock_sku_span {
        hidden: no
        label: "In Stock SKUs SPAN"
        type: count
        filters: {
          field: status
          value: "A"
        }
        filters: {
          field: inv_soh_qty
          value: ">0"
        }
        filters: {
          field: first_received_date
          value: "-null"
        }
        filters: {
          field: is_valid_stock_date
          value: "Yes"
        }
      }

      measure: active_sku_span {
        hidden: no
        label: "Active SKUs SPAN"
        type: count
        filters: {
          field: status
          value: "A"
        }
        filters: {
          field: first_received_date
          value: "-null"
        }
        filters: {
          field: is_valid_stock_date
          value: "Yes"
        }
      }

      measure: on_shelf_availability {
        view_label: "-- Supply Chain KPIs (use params!)"
        description: "OSA is the availability of products for sale to a shopper"
        type: number
        sql: SAFE_DIVIDE(${in_stock_sku_span},${active_sku_span}) ;;
        value_format_name: percent_0
        drill_fields: [prod_num, org_num, on_shelf_availability]
      }

      measure: out_of_stock_rate {
        description: "OOS Rate is 100% - On Shelf Availability"
        label: "Out of Stock Rate %"
        group_label: "Stock Metrics"
        type: number
        sql: 1.0 - ${on_shelf_availability} ;;
        value_format_name: percent_2
      }

      measure: in_stock_count {
        description: "The count of distinct SKUs that are in stock for at least 1 day"
        type: count_distinct
        group_label: "Stock Metrics"
        label: "Count of distinct SKUs in Stock"
        sql: ${prod_num} ;;
        filters: {
          field: is_in_stock
          value: "yes"
        }
      }

      measure: in_stock_vpn_count {
        group_label: "Stock Metrics"
        description: "The count of distinct VPNs that are in stock for at least 1 day"
        type: count_distinct
        label: "Count of distinct VPNs in Stock"
        sql: ${item_vpn} ;;
        filters: {
          field: is_in_stock
          value: "yes"
        }
      }

      measure: in_stock_count_yesterday {
        description: "The count of distinct SKUs that are in stock for at least 1 day"
        type: count_distinct
        label: "Count of distinct SKUs in Stock Yday"
        hidden: yes
        sql: ${prod_num} ;;
        filters: {
          field: is_in_stock
          value: "yes"
        }
        filters: {
          field: stock_date
          value: "yesterday"
        }
      }

      measure: opening_stock_value {
        type:  sum
        hidden: no
        label: "Opening SOH USD Value"
        group_label: "Stock Metrics"
        value_format_name: usd
        sql: ${opening_stock} ;;
      }

      measure: opening_stock_value_loc {
        type:  sum
        hidden: no
        label: "Opening SOH Local Value"
        group_label: "Stock Metrics"
        value_format_name: decimal_2
        sql: ${opening_stock_loc} ;;
      }

      measure: closing_stock_value {
        type:  sum
        hidden: no
        group_label: "Stock Metrics"
        label: "Closing SOH USD Value"
        value_format_name: usd
        sql: ${closing_stock} ;;
      }

      measure: closing_stock_value_loc {
        type:  sum
        hidden: no
        group_label: "Stock Metrics"
        label: "Closing SOH Local Value"
        value_format_name: decimal_2
        sql: ${closing_stock_loc} ;;
      }

      # measure: average_inventory  {
      #   label: "Average SOH USD"
      #   group_label: "Stock Metrics"
      #   type: sum
      #   hidden: no
      #   value_format_name: usd
      #   sql: (${opening_stock} +${closing_stock})/((${no_of_months}+1)*2) ;;
      # }

  measure: num_months {
    type: number
    hidden: yes
    sql: date_diff(max(${stock_date}), min(${stock_date}),month)+1  ;;
  }

  measure: num_weeks {
    type: number
    hidden: no
    sql: date_diff(max(${stock_date}), min(${stock_date}),week)+1  ;;
  }

  measure: month_op_cl_sum {
    type: sum
    hidden: yes
    value_format_name: usd
    sql: (${opening_stock} +${closing_stock}) ;;
      }

  measure: average_inventory_month  {
    label: "Month Average SOH USD"
    group_label: "Stock Metrics"
    type: number
    hidden: no
    value_format_name: usd
    sql: (${month_op_cl_sum})/(${num_months} * 2) ;;
  }

  measure: month_op_cl_sum_loc {
    type: sum
    hidden: yes
    value_format_name: usd
    sql: (${opening_stock_loc} +${closing_stock_loc}) ;;
  }


      measure: average_inventory_loc  {
        label: "Month Average SOH Local"
        group_label: "Stock Metrics"
        value_format_name: decimal_2
        type: number
        hidden: no
        sql: (${month_op_cl_sum_loc})/(${num_months} * 2) ;;
      }

  measure: week_op_cl_sum {
    type: number
    hidden: yes
    value_format_name: usd
    sql: (${week_opening_stock_value} +${week_closing_stock_value}) ;;
  }

  measure: week_op_cl_sum_loc {
    type: number
    hidden: yes
    value_format_name: usd
    sql: (${week_opening_stock_value_loc} +${week_closing_stock_value_loc}) ;;
  }
  measure: average_inventory_week  {
    label: "Week Average SOH USD"
    group_label: "Stock Metrics"
    type: number
    hidden: no
    value_format_name: usd
    sql: (${week_op_cl_sum})/(${num_weeks} * 2) ;;
  }


  measure: average_inventory_week_loc  {
    label: "Week Average SOH Local"
    group_label: "Stock Metrics"
    value_format_name: decimal_2
    type: number
    hidden: no
    sql: (${week_op_cl_sum_loc})/(${num_weeks} * 2) ;;
  }



      measure: inventory_turnover  {
        label: "Inventory Turnover"
        group_label: "Stock Metrics"
        description: "Based on Monthly Average Inventory"
        type: number
        hidden: no
        value_format_name: decimal_2
        sql: safe_divide(${total_cost_of_goods_sold},${average_inventory_month}) ;;
      }

      measure: turn_to_earn{
        label: "Turn to Earn"
        type: number
        hidden: no
        value_format_name: decimal_2
        sql: ${inventory_turnover} * ${gross_margin_share} * 100  ;;
      }


############################################
#####         HIDDEN MEASURES          #####
############################################


      measure: avg_last30days_cogs_amountusd_average {
        type: average
        hidden:yes
        sql:  ${avg_last30days_cogs_amountusd};;
      }

      measure: avg_last90days_cogs_amountusd_average {
        type: average
        hidden:yes
        sql: ${avg_last90days_cogs_amountusd};;
      }

      measure: avg_lastyear_cogs_amountusd_average {
        type: average
        hidden:yes
        sql: ${avg_lastyear_cogs_amountusd} ;;
      }

      measure: daily_quantity_sold_last_28_days {
        type: number
        hidden: yes
        sql: ${total_quantity_sold_last_28_days} / 28.0 ;;
        value_format_name: decimal_0
      }

      measure: average_stock_value {
        type: average
        hidden:yes
        sql: ${cost_of_inventory} ;;
        value_format_name: decimal_2
      }

      measure: average_selling_price_local {
        group_label: "Sales Metrics"
        type: average
        hidden:yes
        sql: ${price_local} ;;
        value_format_name: decimal_2
      }


# ORACLE - RECONNECT To be hidden until the OBIEE data is brought in
      measure: average_retail_selling_price_usd {
        group_label: "Sales Metrics"
        description: "The average of Unit Retail price in USD. Only exists in the datamart as of April' 2020."
        type: average
        hidden:yes
        sql: ${unit_retail_usd} ;;
        value_format_name: usd
      }
# ORACLE - RECONNECT To be hidden until the OBIEE data is brought in


      measure: total_stock_value {
        type: sum
        hidden:yes
        sql: ${cost_of_inventory} ;;
        value_format_name: decimal_0
      }


##############################################
#####           UNUSED MEASURES          #####
##############################################


      measure: total_sales_amt_usd_over {
        group_label: "Sales Over Measures - HIDDEN"
        hidden: yes
        type: number
        sql: SUM(${total_sales_amt_usd}) OVER() ;;
        value_format_name: usd
      }

      measure: total_sales_amt_usd_percent {
        group_label: "Sales Over Measures - HIDDEN"
        hidden: yes
        description: "Percentage of Total Sales"
        type: number
        sql: SAFE_DIVIDE(${total_sales_amt_usd}, NULLIF(${total_sales_amt_usd_over},0)) ;;
        value_format_name: percent_1
      }

      measure: total_sales_amt_usd_over_percent_rank_store {
        group_label: "Store Sales Over Measures - HIDDEN"
        hidden: yes
        type: number
        sql: SAFE_DIVIDE(${total_sales_amt_usd_sum_over_order_store},${total_sales_amt_usd_sum_over_store}) ;;
        value_format_name: percent_2
      }

      measure: total_sales_amt_usd_sum_over_order_store {
        group_label: "Store Sales Over Measures - HIDDEN"
        hidden: yes
        type: number
        sql: SUM(${total_sales_amt_usd}) OVER(PARTITION BY ${store} ORDER BY ${total_sales_amt_usd}) ;;
        value_format_name: usd
      }

      measure: total_sales_amt_usd_sum_over_store {
        group_label: "Store Sales Over Measures - HIDDEN"
        hidden: yes
        type: number
        sql: SUM(${total_sales_amt_usd}) OVER(PARTITION BY ${store}) ;;
        value_format_name: usd
      }

      measure: average_inventory_on_hand {
        description: "To only be used for average_inventory_on_hand NDT"
        hidden: yes
        type: number
        sql: AVG(${stock_count}) OVER(PARTITION BY ${granularity_from_parameter}) ;;
        required_fields: [granularity_from_parameter]
      }

      measure: average_inventory_value_on_hand {
        description: "To only be used for average_inventory_on_hand NDT"
        hidden: yes
        type: number
        sql: AVG(${stock_value}) OVER(PARTITION BY ${granularity_from_parameter}) ;;
        required_fields: [granularity_from_parameter]
      }

      measure: stock_count {
#     NEED TO CHANGE!!!
      type: average
      hidden: yes
      sql: ${inv_soh_qty} ;;
    }

    measure: stock_value {
#     NEED TO CHANGE!!!
    type: average
    hidden: yes
    sql: ${cost_of_inventory} ;;
    value_format_name: decimal_0
  }

##############################################
#####            STOCK COVER             #####
##############################################

  measure: month_closing_stock_cover_new_on_7_days {
    type: number
    label: "Month Closing Stock Cover on 1 Week"
    description: "Stock Cover on 1 Month of historical sales"
    group_label: "Stock Cover"
    value_format_name: decimal_0
    sql: SAFE_DIVIDE(SUM(CASE WHEN EXTRACT( DAY FROM DATE_ADD(${stock_raw}, INTERVAL 1 DAY)) = 1  THEN ${cost_of_inventory} ELSE 0 END),SUM(CASE WHEN   EXTRACT( DAY FROM DATE_ADD(${stock_raw}, INTERVAL 1 DAY)) = 1   THEN ${avg_last7days_cogs_amountusd} ELSE 0 END)) ;;
    html: {{ rendered_value }} Days ;;
  }

  measure: month_closing_stock_cover_new_on_30_days {
    type: number
    label: "Month Closing Stock Cover on 1 Month"
    description: "Stock Cover on 1 Month of historical sales"
    group_label: "Stock Cover"
    value_format_name: decimal_0
    sql: SAFE_DIVIDE(SUM(CASE WHEN EXTRACT( DAY FROM DATE_ADD(${stock_raw}, INTERVAL 1 DAY)) = 1  THEN ${cost_of_inventory} ELSE 0 END),SUM(CASE WHEN   EXTRACT( DAY FROM DATE_ADD(${stock_raw}, INTERVAL 1 DAY)) = 1   THEN ${avg_last30days_cogs_amountusd} ELSE 0 END)) ;;
    html: {{ rendered_value }} Days ;;
  }

  measure: month_closing_stock_cover_new_on_90_days {
    type: number
    label: "Month Closing Stock Cover on 3 Months"
    description: "Stock Cover on 3 Months of historical sales"
    group_label: "Stock Cover"
    value_format_name: decimal_0
    sql: SAFE_DIVIDE(SUM(CASE WHEN EXTRACT( DAY FROM DATE_ADD(${stock_raw}, INTERVAL 1 DAY)) = 1  THEN ${cost_of_inventory} ELSE 0 END),SUM(CASE WHEN   EXTRACT( DAY FROM DATE_ADD(${stock_raw}, INTERVAL 1 DAY)) = 1   THEN ${avg_last90days_cogs_amountusd} ELSE 0 END)) ;;
    html: {{ rendered_value }} Days ;;
  }

  measure: month_closing_stock_cover_new_on_182_days {
    type: number
    label: "Month Closing Stock Cover on 6 Months"
    group_label: "Stock Cover"
    description: "Stock Cover on 6 Months of historical sales"
    value_format_name: decimal_0
    sql: SAFE_DIVIDE(SUM(CASE WHEN EXTRACT( DAY FROM DATE_ADD(${stock_raw}, INTERVAL 1 DAY)) = 1  THEN ${cost_of_inventory} ELSE 0 END),SUM(CASE WHEN   EXTRACT( DAY FROM DATE_ADD(${stock_raw}, INTERVAL 1 DAY)) = 1   THEN ${avg_last182days_cogs_amountusd} ELSE 0 END)) ;;
    html: {{ rendered_value }} Days ;;
  }

  measure: month_closing_stock_cover_new_on_last_year {
    type: number
    label: "Month Closing Stock Cover on 1 Year"
    group_label: "Stock Cover"
    description: "Stock Cover on 1 Year of historical sales"
    value_format_name: decimal_0
    sql: SAFE_DIVIDE(SUM(CASE WHEN EXTRACT( DAY FROM DATE_ADD(${stock_raw}, INTERVAL 1 DAY)) = 1  THEN ${cost_of_inventory} ELSE 0 END),SUM(CASE WHEN   EXTRACT( DAY FROM DATE_ADD(${stock_raw}, INTERVAL 1 DAY)) = 1   THEN ${avg_lastyear_cogs_amountusd} ELSE 0 END)) ;;
    html: {{ rendered_value }} Days ;;
  }



##############################################
#####            HIDDEN DIMS             #####
##############################################


  dimension: opening_stock {
    type: number
    hidden: yes
    group_label: "Stock Metrics"
    value_format_name: usd

    sql: case when ${min_stock_date}=${stock_date} or ${stock_day_of_month}=1  then ${previous_day_unit_cost_usd} * ${previous_day_stock_qty}  else 0 end ;;
  }

  dimension: opening_stock_loc {
    type: number
    hidden: yes
    group_label: "Stock Metrics"
    sql: case when ${min_stock_date}=${stock_date} or ${stock_day_of_month}=1  then ${previous_day_unit_cost} * ${previous_day_stock_qty}  else 0 end ;;
  }

  dimension: closing_stock {
    type: number
    hidden: yes
    group_label: "Stock Metrics"
    value_format_name: usd
    sql: case when ${max_stock_date}=${stock_date} or EXTRACT( DAY FROM DATE_ADD(${stock_raw}, INTERVAL 1 DAY)) = 1  then ${unit_cost_usd} * ${inv_soh_qty} else 0 end ;;
  }

  dimension: closing_stock_loc {
    type: number
    hidden: yes
    group_label: "Stock Metrics"
    sql: case when ${max_stock_date}=${stock_date} or EXTRACT( DAY FROM DATE_ADD(${stock_raw}, INTERVAL 1 DAY)) = 1  then ${unit_cost} * ${inv_soh_qty} else 0 end ;;
  }

  dimension: cost_of_goods_sold {
    type: number
    hidden: yes
    label: "Cost of Goods Sold USD"
    sql: ABS(${unit_cost_usd})*${sales_qty} ;;
    value_format_name: usd
  }

  dimension: cost_of_goods_sold_local {
    type: number
    hidden: yes
    label: "Cost of Goods Sold USD"
    sql: ABS(${unit_cost})*${sales_qty} ;;
    value_format_name: usd
  }

  dimension: cost_of_inventory {
    type: number
    hidden: yes
    group_label: "Non-Aggregate Dimensions"
    sql: ${unit_cost_usd} * ${inv_soh_qty} ;;
    value_format_name: usd
  }

##############################################
#####         UNUSED DIMENSIONS          #####
##############################################



  dimension: global1_exchange_rate {
    hidden: yes
    type: number
    sql: ${TABLE}.global1_exchange_rate ;;
  }

  dimension: inv_soh_cost_amt_lcl {
    hidden: yes
    type: number
    sql: ${TABLE}.inv_soh_cost_amt_lcl ;;
  }

  dimension: doc_curr_code {
    hidden: yes
    type: string
    sql: ${TABLE}.doc_curr_code ;;
  }

  dimension: inv_soh_rtl_amt_lcl {
    hidden: yes
    type: number
    sql: ${TABLE}.inv_soh_rtl_amt_lcl ;;
  }

  dimension: inv_soh_rtl_wot_amt_lcl {
    hidden: yes
    type: number
    sql: ${TABLE}.inv_soh_rtl_wot_amt_lcl ;;
  }

  dimension: loc_curr_code {
    hidden: yes
    type: string
    sql: ${TABLE}.loc_curr_code ;;
  }

  dimension: total_price_sold {
    type: number
    hidden: yes
    sql: ${unit_cost_usd} * ${sales_qty} ;;
    value_format_name: usd
  }


  dimension: avg_last7days_cogs_amountusd {
    type: number
    hidden:yes
    sql: ${TABLE}.avg_last7days_cogs_amountusd ;;
  }

  dimension: avg_last30days_cogs_amountusd {
    type: number
    hidden:yes
    sql: ${TABLE}.avg_last30days_cogs_amountusd ;;
  }

  dimension: avg_last90days_cogs_amountusd {
    type: number
    hidden:yes
    sql: ${TABLE}.avg_last90days_cogs_amountusd ;;
  }

  dimension: avg_last182days_cogs_amountusd {
    type: number
    hidden:yes
    sql: ${TABLE}.avg_last182days_cogs_amountusd ;;
  }

  dimension: avg_lastyear_cogs_amountusd {
    type: number
    hidden:yes
    sql: ${TABLE}.avg_lastyear_cogs_amountusd ;;
  }


  measure: max_date {
    type:  date
    hidden: no
    sql: max(${stock_date});;
  }

  measure: min_date {
    type:  date
    hidden: no
    sql: min(${stock_date}) ;;
  }

  dimension: max_stock_date {
    type: date
    hidden: no
    sql: ${dm_sohsales_max_min_dates.max_date} ;;
  }

  dimension: min_stock_date {
    type: date
    hidden: no
    sql: ${dm_sohsales_max_min_dates.min_date} ;;
  }

  dimension: no_of_months {
    type: number
    hidden: yes
    sql: ${dm_sohsales_max_min_dates.no_of_months} ;;
  }
  dimension: no_of_days {
    type: number
    hidden: no
    sql: ${dm_sohsales_max_min_dates.no_of_days} ;;
  }

  measure: max_stock_age {
    type: max
    value_format_name: decimal_0
    hidden: yes
    sql: date_diff(${stock_date}, ${first_received_date}, month) ;;
  }

  # stock cover - Start - SPD 261

  dimension: is_max_date {
    type:  yesno
    hidden: yes
    sql: ${stock_date}=${max_stock_date} ;;
  }

  dimension: last7days_cogs_amountusd {
    type: number
    hidden: yes
    sql: ${TABLE}.last7days_cogs_amountusd ;;
  }

  dimension: last30days_cogs_amountusd {
    type: number
    hidden: yes
    sql: ${TABLE}.last30days_cogs_amountusd ;;
  }

  dimension: last90days_cogs_amountusd {
    type: number
    hidden: yes
    sql: ${TABLE}.last90days_cogs_amountusd ;;
  }

  dimension: last182days_cogs_amountusd {
    type: number
    hidden: yes
    sql: ${TABLE}.last182days_cogs_amountusd ;;
  }

  dimension: lastyear_cogs_amountusd {
    type: number
    hidden: yes
    sql: ${TABLE}.lastyear_cogs_amountusd ;;
  }

  measure: sum_last7days_cogs_amountusd {
    type: sum
    filters: {
      field: is_max_date
      value: "yes"
    }
    hidden: yes
    sql: ${last7days_cogs_amountusd} ;;
  }

  measure: sum_last30days_cogs_amountusd {
    type: sum
    filters: {
      field: is_max_date
      value: "yes"
    }
    hidden: yes
    sql: ${last30days_cogs_amountusd} ;;
  }

  measure: sum_last90days_cogs_amountusd {
    type: sum
    filters: {
      field: is_max_date
      value: "yes"
    }
    hidden: yes
    sql: ${last90days_cogs_amountusd} ;;
  }

  measure: sum_last182days_cogs_amountusd {
    type: sum
    filters: {
      field: is_max_date
      value: "yes"
    }
    hidden: yes
    sql: ${last182days_cogs_amountusd} ;;
  }

  measure: sum_lastyear_cogs_amountusd {
    type: sum
    filters: {
      field: is_max_date
      value: "yes"
    }
    hidden: yes
    sql: ${lastyear_cogs_amountusd} ;;
  }

  measure: stock_cover_bw_7days{
    type: number
    label: "Stock Cover on 1 Week"
    description: "Stock Cover on 1 week historical sales"
    group_label: "Stock Cover"
    value_format:  "0 \" Days\""
    sql: safe_divide(${last_day_stock_value},(${sum_last7days_cogs_amountusd}/7)) ;;
  }

  measure: stock_cover_bw_1month  {
    type: number
    label: "Stock Cover on 1 Month"
    description: "Stock Cover on 1 month historical sales"
    group_label: "Stock Cover"
    value_format: "0 \" Days\""
    sql: safe_divide(${last_day_stock_value},(${sum_last30days_cogs_amountusd}/30)) ;;
  }

  measure: stock_cover_bw_3month  {
    type: number
    label: "Stock Cover on 3 Month"
    description: "Stock Cover on 3 month historical sales"
    group_label: "Stock Cover"
    value_format: "0 \" Days\""
    sql: safe_divide(${last_day_stock_value},(${sum_last90days_cogs_amountusd}/90)) ;;
  }

  measure: stock_cover_bw_6month  {
    type: number
    label: "Stock Cover on 6 Month"
    description: "Stock Cover on 6 month historical sales"
    group_label: "Stock Cover"
    value_format: "0 \" Days\""
    sql: safe_divide(${last_day_stock_value},(${sum_last182days_cogs_amountusd}/182)) ;;
  }

  measure: stock_cover_bw_1year  {
    type: number
    label: "Stock Cover on 1 Year"
    description: "Stock Cover on 1 year historical sales"
    group_label: "Stock Cover"
    value_format: "0 \" Days\""
    sql: safe_divide(${last_day_stock_value},(${sum_lastyear_cogs_amountusd}/365)) ;;
  }

  # stock cover- End - SPD 261


# ---------------------SPD-275  Changes to include WAC for consignment starts -----------------------



  dimension: consignment_cogs_usd {
    type: number
    hidden:  yes
    value_format_name: decimal_2
    sql: ${sales_amt_usd}  * (1-${ch_commission_valid_data.commission}/100) ;;
  }

  dimension: consignment_cost_usd {
    type: number
    hidden:  yes
    value_format_name: decimal_2
    sql: safe_divide(${consignment_cogs_usd},${sales_qty}) ;;
  }

  dimension: unit_cost_usd_adj {
    type: number
    description: "Adjusted Average Unit Cost (WAC) in USD considering consignments"
    group_label: "Consignment"
    label: "Adj WAC USD"
    hidden: no
    value_format_name: decimal_2
    sql: case when  ${consignment_flag}= "Y" then
          ${consignment_cost_usd}
    else   ${unit_cost_usd} end ;;
  }

  dimension: sales_amt_lcl {
    type: number
    hidden: yes
    value_format_name : decimal_2
    sql: ${TABLE}.sales_amountlocal_beforetax ;;
  }


  dimension: consignment_cogs_lcl {
    type: number
    hidden:  yes
    value_format_name: decimal_2
    sql: ${sales_amt_lcl} * (1-${ch_commission_valid_data.commission}/100) ;;
  }

  dimension: consignment_cost_lcl {
    type: number
    hidden:  yes
    value_format_name: decimal_2
    sql: safe_divide(${consignment_cogs_lcl},${sales_qty}) ;;
  }

  dimension: unit_cost_lcl_adj {
    type: number
    description: "Adjusted Average Unit Cost (WAC) in Local Currency considering consignments"
    group_label: "Consignment"
    label: "Adj WAC Local"
    hidden: no
    value_format_name: decimal_2
    sql: case when  ${consignment_flag}= "Y" then
          ${consignment_cost_lcl}
    else   ${unit_cost} end ;;
  }

  # adjusted COGS

  dimension: cost_of_goods_sold_local_adj {
    type: number
    group_label: "Consignment"
    hidden: yes

    sql: ABS(${unit_cost_lcl_adj})*${sales_qty} ;;
    value_format_name: usd
  }

  dimension: cost_of_goods_sold_usd_adj {
    type: number
    group_label: "Consignment"
    hidden: yes

    sql: ABS(${unit_cost_usd_adj})*${sales_qty} ;;
    value_format_name: usd
  }

  measure: cogs_usd_adj {
    type: sum
    group_label: "Consignment"
    value_format_name: usd
    sql_distinct_key: concat(${pk},${ch_commission_valid_data.pk}) ;;

    label: "Cost of Goods Sold Adj USD"
    sql:  ${cost_of_goods_sold_usd_adj};;
  }



  measure: cogs_lcl_adj {
    type: sum
    group_label: "Consignment"
    label: "Cost of Goods Sold Adj Local"
    description: "Adjusted considering consignments"
    value_format_name: decimal_2
    sql_distinct_key: concat(${pk},${ch_commission_valid_data.pk}) ;;


    sql:  ${cost_of_goods_sold_local_adj};;
  }

  measure: gross_margin_usd_adj {
    type: number
    value_format_name: usd
    group_label: "Consignment"
    label: "Gross Margin Adj. USD"
    description: "Adjusted considering consignments"
    sql_distinct_key: concat(${pk},${ch_commission_valid_data.pk}) ;;

    sql: ${total_sales_amt_usd} -${cogs_usd_adj} ;;
  }

  measure: gross_margin_local_adj {
    type: number
    value_format_name: decimal_2
    group_label: "Consignment"
    label: "Gross Margin Adj Local"
    description: "Adjusted considering consignments"
    sql_distinct_key: concat(${pk},${ch_commission_valid_data.pk}) ;;


    sql: ${total_sales_amt_loc} -${cogs_lcl_adj} ;;
  }

  measure: gross_margin_share_adj {
    type: number
    value_format_name: percent_2
    group_label: "Consignment"
    label: "Gross Margin Share Adj"
    description: "Adjusted considering consignments"
    sql_distinct_key: concat(${pk},${ch_commission_valid_data.pk}) ;;


    sql:  safe_divide(${gross_margin_usd_adj},${total_sales_amt_usd});;
  }

  # adjusted COGS

# ---------------------SPD-275  Changes to include WAC for consignment ends -----------------------


#   SPD 314 Start

  measure: total_sales_amt_loc {
    type: sum
    label: "Net Sales Amount Local"
    group_label: "Sales Metrics"
    description: "Sum of sales amount in Local Currency"
    value_format_name: decimal_2
    sql: ${sales_amt_lcl} ;;
  }

  measure: gross_margin_loc {
    type: number
    hidden: no
    label: "Gross Margin Local"
    value_format_name: decimal_2
    group_label: "Gross Margin"
    sql: ${total_sales_amt_loc} - ${total_cost_of_goods_sold_local} ;;
  }

#   SPD 314 End

#   SPD 339 Start
  dimension: sep_market {
    group_label: "Products"
    label: "Market"
    type: string
    sql: ${TABLE}.sep_market ;;
  }
# SPD 339 End


}
