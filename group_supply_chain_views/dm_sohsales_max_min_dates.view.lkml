# View Created to get the maximum and minimum sales and stock date based on the user inputs

# include: "group_supply_chain.model.lkml"

view: dm_sohsales_max_min_dates {
  derived_table: {
    explore_source: fact_soh_sales_poc {
      column: max_date {}
      column: min_date {}
      bind_all_filters: yes
    }
  }
  dimension: max_date {
    label: "Sales and Stock Max Date"
    hidden: yes
    type: date
  }
  dimension: min_date {
    label: "Sales and Stock Min Date"
    hidden: yes
    type: date
  }

  dimension: no_of_months {
    type:  number
    hidden: no
    sql: date_diff(${max_date},${min_date},month) ;;
  }
  dimension: no_of_days {
    type:  number
    hidden: yes
    sql: date_diff(${max_date},${min_date},day) ;;
  }

}
