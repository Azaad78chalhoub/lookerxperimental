view: ch_supplier_commission_info {

  derived_table: {
    sql_trigger_value: SELECT CURRENT_DATE ;;
    sql:
    select distinct store,
    country,
    dept,
    class,
    subclass,
    supplier,
    effective_date from (SELECT
    store,
    commission,
    country,
    dept,
    class,
    subclass,
    effective_date,
    supplier_cost_flag,
    supplier,
    row_number () OVER (PARTITION BY country, dept, class, subclass,store  ORDER BY effective_date desc ,commission desc) rank
    FROM
    `chb-prod-supplychain-data.prod_shared_dimensions.dim_ch_supp_loc_commission`
    where supplier_cost_flag = "NO"
    and commission <> 0
    and store in (11001, 11002, 11003, 11004, 11008, 11011, 11023, 11033, 11043)
    ) where rank = 1;;
  }
##Filtered the data only for Tryano as there were duplicates in the commission table for other brands

  dimension: pk {
    primary_key: yes
    hidden: yes
    sql: CONCAT(${class},${dept},${store},${subclass}) ;;
  }

  dimension: class {
    hidden: yes
  }

  dimension: country {
    hidden: yes
  }

  dimension: dept {
    hidden: yes
  }

  dimension: store {
    hidden: yes
  }

  dimension: subclass {
    hidden: yes
  }

  dimension: effective_date {
    hidden: yes
  }

  dimension: supplier {
    label: "Supplier (for Tryano)"
    description: "Supplier code from Oracle RMS"
    sql: ${TABLE}.supplier ;;
  }

}
