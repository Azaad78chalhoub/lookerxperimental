view: rtv_detail_rms {
  sql_table_name: `chb-prod-supplychain-data.prod_supply_chain.rtv_detail_rms`
    ;;
  dimension: pk {
    primary_key: yes
    hidden: yes
    type: string
    sql: CONCAT(${rtv_order_no},${item},${seq_no}) ;;
  }

  dimension: inv_status {
    type: number
    sql: ${TABLE}.inv_status ;;
  }

  dimension: item {
    type: string
    sql: CAST(${TABLE}.item AS STRING) ;;
  }

  dimension: original_unit_cost {
    type: number
    hidden: yes
    sql: ${TABLE}.original_unit_cost ;;
  }
  measure: original_cost_per_unit {
    label: "Orginal Unit Cost"
    description: "Orginal Unit Cost"
    type: sum
    sql: ${original_unit_cost} ;;
  }


  dimension: publish_ind {
    type: yesno
    sql: ${TABLE}.publish_ind ;;
  }

  dimension: qty_cancelled {
    type: number
    hidden: yes
    sql: ${TABLE}.qty_cancelled ;;
  }
  measure: Total_qty_cancelled {
    label: "Total qty cancelled"
    description: "Total qty cancelled"
    type: sum
    sql: ${qty_cancelled} ;;
  }
  dimension: qty_requested {
    type: number
    hidden: yes
    sql: ${TABLE}.qty_requested ;;
  }

  measure: Total_qty_requested {
    label: "Total qty requested"
    description: "Total qty requested"
    type: sum
    sql: ${qty_requested} ;;
  }

  dimension: qty_returned {
    type: number
    hidden: yes
    sql: ${TABLE}.qty_returned ;;
  }

  measure: Total_qty_returned {
    label: "Total qty returned"
    description: "Total qty returned"
    type: sum
    sql: ${qty_returned} ;;
  }


  dimension: reason {
    type: string
    sql: ${TABLE}.reason ;;
  }

  dimension: restock_pct {
    type: number
    sql: ${TABLE}.restock_pct ;;
  }

  dimension: rtv_order_no {
    type: string
    sql: ${TABLE}.rtv_order_no ;;
  }

  dimension: seq_no {
    type: number
    sql: ${TABLE}.seq_no ;;
  }

  dimension: shipment {
    type: string
    sql: ${TABLE}.shipment ;;
  }

  dimension: unit_cost {
    type: number
    sql: ${TABLE}.unit_cost ;;
  }

  dimension: updated_by_rms_ind {
    type: yesno
    sql: ${TABLE}.updated_by_rms_ind ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
