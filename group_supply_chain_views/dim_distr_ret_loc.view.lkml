view: dim_distr_ret_loc {
  sql_table_name: `chb-prod-supplychain-data.prod_shared_dimensions.dim_distr_ret_loc`
    ;;

  dimension: add_1 {
    type: string
    view_label: "Retail Locations"
    label: "Address 1"
    sql: ${TABLE}.add_1 ;;
  }

  dimension: add_2 {
    type: string
    view_label: "Retail Locations"
    label: "Address "
    sql: ${TABLE}.add_2 ;;
  }

  dimension: addr_key {
    type: number
    view_label: "Retail Locations"
    label: "Address Key"
    sql: ${TABLE}.addr_key ;;
  }

  dimension: addr_type {
    type: string
    view_label: "Retail Locations"
    label: "Address Type"
    sql: ${TABLE}.addr_type ;;
  }

  dimension: area {
    type: number
    view_label: "Retail Locations"
    label: "Area"
    sql: ${TABLE}.area ;;
  }

  dimension: area_name {
    type: string
    view_label: "Retail Locations"
    label: "Area Name"
    sql: ${TABLE}.area_name ;;
  }

  dimension: chain {
    type: number
    view_label: "Retail Locations"
    label: "Chain"
    sql: ${TABLE}.chain ;;
  }

  dimension: chain_name {
    type: string
    view_label: "Retail Locations"
    label: "Chain Name"
    sql: ${TABLE}.chain_name ;;
  }

  dimension: channel_id {
    type: number
    view_label: "Retail Locations"
    label: "Channel ID"
    sql: ${TABLE}.channel_id ;;
  }

  dimension: channel_name {
    type: string
    view_label: "Retail Locations"
    label: "Channel Name"
    sql: ${TABLE}.channel_name ;;
  }

  dimension: channel_type {
    type: string
    view_label: "Retail Locations"
    label: "Channel Type"
    sql: ${TABLE}.channel_type ;;
  }

  dimension: city {
    type: string
    view_label: "Retail Locations"
    label: "City"
    sql: ${TABLE}.city ;;
  }

  dimension: country_desc {
    type: string
    view_label: "Retail Locations"
    label: "Country Description"
    sql: ${TABLE}.country_desc ;;
  }

  dimension: country_id {
    type: string
    view_label: "Retail Locations"
    label: "Country ID"
    sql: ${TABLE}.country_id ;;
  }

  dimension: currency_code {
    type: string
    view_label: "Retail Locations"
    label: "Currency Code"
    sql: ${TABLE}.currency_code ;;
  }

  dimension: description {
    type: string
    view_label: "Retail Locations"
    label: "Description"
    sql: ${TABLE}.description ;;
  }

  dimension: district {
    type: number
    view_label: "Retail Locations"
    label: "Disrict"
    sql: ${TABLE}.district ;;
  }

  dimension: district_name {
    type: string
    view_label: "Retail Locations"
    label: "District Name"
    sql: ${TABLE}.district_name ;;
  }

  dimension: key_value_1 {
    type: string
    view_label: "Retail Locations"
    label: "Key Value"
    sql: ${TABLE}.key_value_1 ;;
  }

  dimension: key_value_2 {
    type: string
    view_label: "Retail Locations"
    label: "Key Value 2"
    sql: ${TABLE}.key_value_2 ;;
  }

  dimension: loc_code {
    type: number
    view_label: "Retail Locations"
    label: "Location Code"
    sql: ${TABLE}.loc_code ;;
  }

  dimension: loc_name {
    type: string
    view_label: "Retail Locations"
    label: "Location Name"
    sql: ${TABLE}.loc_name ;;
  }

  dimension: mall_name {
    type: string
    view_label: "Retail Locations"
    label: "Mall Name"
    sql: ${TABLE}.mall_name ;;
  }

  dimension: module {
    type: string
    view_label: "Retail Locations"
    label: "Module"
    sql: ${TABLE}.module ;;
  }

  dimension: physical_wh {
    type: number
    view_label: "Retail Locations"
    label: "Physical Warehouse"
    sql: ${TABLE}.physical_wh ;;
  }

  dimension: post {
    type: string
    view_label: "Retail Locations"
    label: "Post"
    sql: ${TABLE}.post ;;
  }

  dimension: primary_addr_ind {
    type: string
    view_label: "Retail Locations"
    label: "Primary Address"
    sql: ${TABLE}.primary_addr_ind ;;
  }

  dimension: primary_vwh {
    type: number
    view_label: "Retail Locations"
    label: "Primary Warehouse"
    sql: ${TABLE}.primary_vwh ;;
  }

  dimension: region {
    type: number
    view_label: "Retail Locations"
    label: "Region"
    sql: ${TABLE}.region ;;
  }

  dimension: region_name {
    type: string
    view_label: "Retail Locations"
    label: "Region Name"
    sql: ${TABLE}.region_name ;;
  }

  dimension: selling_square_ft {
    type: number
    view_label: "Retail Locations"
    label: "Selling Square FT"
    sql: ${TABLE}.selling_square_ft ;;
  }

  dimension: state {
    type: string
    view_label: "Retail Locations"
    label: "State"
    sql: ${TABLE}.state ;;
  }

  dimension_group: store_close {
    type: time
    view_label: "Retail Locations"
    label: "Store Close"
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.store_close_date ;;
  }

  dimension: store_format {
    type: number
    view_label: "Retail Locations"
    label: "Store Format"
    sql: ${TABLE}.store_format ;;
  }

  dimension: store_name10 {
    type: string
    view_label: "Retail Locations"
    label: "Store Name"
    sql: ${TABLE}.store_name10 ;;
  }

  dimension: store_name3 {
    type: string
    view_label: "Retail Locations"
    label: "Store Name 3"
    sql: ${TABLE}.store_name3 ;;
  }

  dimension_group: store_open {
    type: time
    view_label: "Retail Locations"
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.store_open_date ;;
  }

  dimension: total_square_ft {
    type: number
    view_label: "Retail Locations"
    sql: ${TABLE}.total_square_ft ;;
  }

  dimension: vat_region {
    type: number
    view_label: "Retail Locations"
    sql: ${TABLE}.vat_region ;;
  }

  dimension: wh_name_secondary {
    type: string
    view_label: "Retail Locations"
    sql: ${TABLE}.wh_name_secondary ;;
  }

  measure: count {
    type: count
    view_label: "Retail Locations"
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      area_name,
      district_name,
      mall_name,
      region_name,
      loc_name,
      chain_name,
      channel_name
    ]
  }
}
