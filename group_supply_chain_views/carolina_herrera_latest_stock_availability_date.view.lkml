view: carolina_herrera_latest_stock_availability_date {
  derived_table: {
    sql: select store,sku_externalcode,max(stockdate) as stockdate
      from `chb-prod-supplychain-data.prod_supply_chain.carolina_herrera_theoretical_stock`
      where availablestock > 0
      group by 1,2
      order by 1,2,3 asc
       ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  dimension: store {
    type: number
    hidden: yes
    sql: ${TABLE}.store ;;
  }

  dimension: sku_externalcode {
    type: string
    hidden: yes
    sql: ${TABLE}.sku_externalcode ;;
  }

  dimension: latestst_stock_availability_date {
    type: date
    datatype: date
    sql: ${TABLE}.stockdate ;;
  }

  set: detail {
    fields: [store, sku_externalcode, latestst_stock_availability_date]
  }
}
