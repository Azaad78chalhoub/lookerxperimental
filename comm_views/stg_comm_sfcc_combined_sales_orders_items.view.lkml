include: "../_common/period_over_period.view"
view: stg_comm_sfcc_combined_sales_orders_items {
  sql_table_name: `chb-prod-stage-comm.prod_comm.stg_comm_sfcc_combined_sales_orders_items`
    ;;


  extends: [period_over_period]

  dimension_group: pop_no_tz {
    sql: ${record_datetime_raw} ;;
  }


  dimension: amount_refunded {
    type: number
    hidden: yes
    sql: ${TABLE}.amount_refunded ;;
  }

  dimension: brand {
    type: string
    sql: ${TABLE}.brand ;;
  }

  dimension: currency {
    type: string
    sql: ${TABLE}.currency ;;
  }

  dimension: canceled_amount_local {
    type: number
    hidden: yes
    sql: ${TABLE}.canceled_amount_local ;;
  }

  dimension: canceled_amount_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.canceled_amount_usd ;;
  }

  dimension: city {
    type: string
    sql: ${TABLE}.city ;;
  }

  dimension: customer_email {
    type: string
    sql: ${TABLE}.customer_email ;;
  }

  dimension: discount_amount {
    type: number
    hidden: yes
    sql: ${TABLE}.discount_amount ;;
  }

  dimension: discount_refunded {
    type: number
    hidden: yes
    sql: ${TABLE}.discount_refunded ;;
  }

  dimension: free_gift_bag {
    type: string
    sql: ${TABLE}.Free_Gift_Bag ;;
  }

  dimension: free_greeting_card {
    type: string
    sql: ${TABLE}.Free_Greeting_Card ;;
  }

  dimension: gross_local {
    type: number
    hidden: yes
    sql: ${TABLE}.gross_local ;;
  }

  dimension: gross_net_sales {
    type: number
    hidden: yes
    sql: ${TABLE}.gross_net_sales ;;
  }

  dimension: gross_net_sales_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.gross_net_sales_usd ;;
  }

  dimension: gross_revenue_amount_local {
    type: number
    hidden: yes
    sql: ${TABLE}.gross_revenue_amount_local ;;
  }

  dimension: gross_revenue_amount_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.gross_revenue_amount_usd ;;
  }

  dimension: gross_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.gross_usd ;;
  }

  dimension: item_comments {
    type: string
    sql: ${TABLE}.item_comments ;;
  }

  dimension_group: order {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year,
      month_num,
      month_name
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.order_date ;;
  }

  dimension_group: order_datetime {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year,
      month_num,
      month_name
    ]
    sql: CAST(${TABLE}.order_datetime AS TIMESTAMP) ;;
  }

  dimension: order_id {
    type: string
    sql: ${TABLE}.order_id ;;
  }

  dimension: price {
    type: number
    hidden: yes
    sql: ${TABLE}.price ;;
  }

  dimension: primary_key {
    type: string
    sql: ${TABLE}.primary_key ;;
  }

  dimension: qty_canceled {
    type: number
    hidden: yes
    sql: ${TABLE}.qty_canceled ;;
  }

  dimension: qty_ordered {
    type: number
    hidden: yes
    sql: ${TABLE}.qty_ordered ;;
  }

  dimension: qty_refunded {
    type: number
    hidden: yes
    sql: ${TABLE}.qty_refunded ;;
  }

  dimension: qty_shipped {
    type: number
    hidden: yes
    sql: ${TABLE}.qty_shipped ;;
  }

  dimension_group: record {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year,
      month_num,
      month_name
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.record_date ;;
  }

  dimension_group: record_datetime {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year,
      month_num,
      month_name
    ]
    sql: CAST(${TABLE}.record_datetime AS TIMESTAMP) ;;
  }

  dimension: record_type {
    type: string
    sql: ${TABLE}.record_type ;;
  }

  dimension: refund_amount_local {
    type: number
    hidden: yes
    sql: ${TABLE}.refund_amount_local ;;
  }

  dimension: refund_amount_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.refund_amount_usd ;;
  }

  dimension: shipment_net_sales {
    type: number
    hidden: yes
    sql: ${TABLE}.shipment_net_sales ;;
  }

  dimension: shipment_net_sales_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.shipment_net_sales_usd ;;
  }

  dimension: shipped_local {
    type: number
    hidden: yes
    sql: ${TABLE}.shipped_local ;;
  }

  dimension: shipped_rev_amount_local {
    type: number
    hidden: yes
    sql: ${TABLE}.shipped_rev_amount_local ;;
  }

  dimension: shipped_rev_amount_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.shipped_rev_amount_usd ;;
  }

  dimension: shipped_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.shipped_usd ;;
  }

  dimension: sku {
    type: string
    sql: ${TABLE}.sku ;;
  }

  dimension: source {
    type: string
    sql: ${TABLE}.source ;;
  }

  dimension: status {
    type: string
    sql: ${TABLE}.status ;;
  }

  dimension: store_country {
    type: string
    sql: ${TABLE}.store_country ;;
  }

  dimension: store_id {
    type: string
    sql: ${TABLE}.store_id ;;
  }

  dimension: tender_type_group {
    type: string
    sql: ${TABLE}.tender_type_group ;;
  }

  dimension: vertical {
    type: string
    sql: ${TABLE}.vertical ;;
  }

  dimension: promotion_detail_coupon_code {
    type: string
    sql: ${TABLE}.promotion_detail_coupon_code ;;
  }

  dimension: promotion_detail_coupon_id {
    type: string
    sql: ${TABLE}.promotion_detail_coupon_id ;;
  }

  dimension: promotion_detail_promotion_id {
    type: string
    sql: ${TABLE}.promotion_detail_promotion_id ;;
  }

  dimension: promotion_detail_campaign_id {
    type: string
    sql: ${TABLE}.promotion_detail_campaign_id ;;
  }

  dimension: promotion_detail_discount {
    type: number
    hidden: yes
    sql: ${TABLE}.promotion_detail_discount ;;
  }

  dimension: promotion_detail_promotion_text {
    type: string
    sql: ${TABLE}.promotion_detail_promotion_text ;;
  }

  dimension: order_promotion_detail_promotion_text {
    type: string
    sql: ${TABLE}.order_promotion_detail_promotion_text ;;
  }

  dimension: order_promotion_detail_promotion_id {
    type: string
    sql: ${TABLE}.order_promotion_detail_promotion_id ;;
  }

  dimension: order_promotion_detail_campaign_id {
    type: string
    sql: ${TABLE}.order_promotion_detail_campaign_id ;;
  }

  dimension: order_promotion_detail_coupon_id {
    type: string
    sql: ${TABLE}.order_promotion_detail_coupon_id ;;
  }

  dimension: order_promotion_detail_coupon_code {
    type: string
    sql: ${TABLE}.order_promotion_detail_coupon_code ;;
  }

  dimension: order_promotion_detail_discount {
    type: number
    hidden: yes
    sql: ${TABLE}.order_promotion_detail_discount ;;
  }

  #####################################
  #####         MEASURES          #####
  #####################################


  measure: sfcc_gross_revenue_usd {
    type: sum
    label: "Gross Revenue (USD)"
    value_format_name: usd
    description: "Total value of incoming orders on the website (excluding failed credit card attempts, VAT, customs and shipping fees and COD fees) in USD"
    sql:  ${gross_revenue_amount_usd} ;;
    filters: [record_type: "SALE"]
  }

  measure: sfcc_gross_revenue {
    type: sum
    label: "Gross Revenue (Local)"
    value_format_name: decimal_2
    description: "Total value of incoming orders on the website (excluding failed credit card attempts, VAT, customs and shipping fees and COD fees) in local currency"
    sql:  ${gross_revenue_amount_local} ;;
  }

  measure: sfcc_gross_cancelled_usd{
    type: sum
    label: "Cancelled orders (USD)"
    value_format_name: usd
    description: "Total value of cancelled orders on the website"
    sql: ${canceled_amount_usd};;
    filters: [record_type: "CANCELLED"]
  }

  measure: sfcc_gross_cancelled{
    type: sum
    label: "Cancelled orders (Local)"
    value_format_name: decimal_2
    description: "Total value of cancelled orders on the website in local currency"
    sql: ${canceled_amount_local};;
    filters: [record_type: "CANCELLED"]
  }


  measure: sfcc_gross_returns_usd {
    type: sum
    label: "Returned orders ($)"
    value_format_name: usd
    description: "Total value of returned orders on the website"
    sql: ${refund_amount_usd};;
    filters: [record_type: "REFUND"]
  }

  measure: sfcc_gross_returns {
    type: sum
    label: "Returned orders (Local)"
    value_format_name: decimal_2
    description: "Total value of returned orders on the website in local currency"
    sql: ${refund_amount_local};;
    filters: [record_type: "REFUND"]
  }

  measure: sfcc_gross_revenue_less_cancellations_usd {
    type: number
    label: "Gross Revenue w/o Cancellations (USD)"
    value_format_name: usd
    description: "Total value of incoming orders on the website excluding cancellations (excluding failed credit card attempts, VAT, customs and shipping fees and COD fees)"
    sql: ${sfcc_gross_revenue_usd} - ${sfcc_gross_cancelled_usd} ;;
  }

  measure: sfcc_gross_revenue_less_cancellations {
    type: number
    label: "Gross Revenue w/o Cancellations (Local)"
    value_format_name: decimal_2
    description: "Total value of incoming orders on the website excluding cancellations (excluding failed credit card attempts, VAT, customs and shipping fees and COD fees) in local currency"
    sql: ${sfcc_gross_revenue} - ${sfcc_gross_cancelled} ;;
  }

  measure: sfcc_gross_revenue_less_cancellations_less_returns_usd {
    type: number
    label: "Gross Revenue w/o cancellation and returns  (USD)"
    value_format_name: usd
    description: "Total value of incoming orders on the website excluding cancellations and returns (excluding failed credit card attempts, VAT, customs and shipping fees and COD fees)"
    sql: ${sfcc_gross_revenue_usd} - ${sfcc_gross_cancelled_usd} - ${sfcc_gross_returns_usd} ;;
  }

  measure: sfcc_gross_revenue_less_cancellations_less_returns {
    type: number
    label: "Gross Revenue w/o cancellation and returns  (Local)"
    value_format_name: decimal_2
    description: "Total value of incoming orders on the website excluding cancellations and returns (excluding failed credit card attempts, VAT, customs and shipping fees and COD fees) in local currency"
    sql: ${sfcc_gross_revenue} - ${sfcc_gross_cancelled} - ${refund_amount_local} ;;
  }

  measure: sfcc_shipped_sales {
    type: sum
    label: "Shipped Sales (USD)"
    value_format_name: usd
    description: "Total value of shipped orders (excluding failed credit card attempts, VAT and cancellations but including customs and shipping fees)"
    sql: ${shipped_usd};;
    filters: [record_type: "SHIPMENT"]
  }

  measure: sfcc_shipped_sales_local {
    type: sum
    label: "Shipped Sales (Local)"
    value_format_name: decimal_2
    description: "Total value of shipped orders (excluding failed credit card attempts, VAT and cancellations but including customs and shipping fees) in local currency"
    sql: ${shipped_local};;
    filters: [record_type: "SHIPMENT"]
  }

#     measure: sfcc_gross_net_sales{
#       type: sum
#       label: "Gross Net Sales USD"
#       value_format_name: usd
#       sql: ${gross_net_sales_usd} ;;
#     }

  measure: sfcc_shipped_net_sales {
    type: sum
    label: "Shipped Net Sales (USD)"
    value_format_name: usd
    description: "Total value of shipped orders excluding returns (excluding failed credit card attempts, VAT and cancellations but including customs and shipping fees)"
    sql: ${shipment_net_sales_usd};;
  }

  measure: sfcc_shipped_net_sales_local {
    type: sum
    label: "Shipped Net Sales (Local)"
    value_format_name: decimal_2
    description: "Total value of shipped orders excluding returns (excluding failed credit card attempts, VAT and cancellations but including customs and shipping fees) in local currency"
    sql: ${shipment_net_sales};;
  }

  measure: sfcc_number_of_orders  {
    type: count_distinct
    label: "Number of Orders"
    description: "Total number of orders placed on the website"
    sql: ${order_id} ;;
    filters: [record_type: "SALE"]
  }

#     measure: sfcc_conversion_rate {
#       type: number
#       label: "Conversion Rate"
#       value_format_name: percent_2
#       sql: ${sfcc_number_of_orders} / NULLIF(${unified_ga.session_count},0) ;;
#     }

  measure: count_cancelled_orders {
    type: count_distinct
    label: "# Cancelled Orders"
    description: "Total number of cancelled orders placed on the website"
    sql: ${order_id};;
    filters: [record_type: "CANCELLED"]
  }

  measure: count_returned_orders {
    type: count_distinct
    label: "# Returned Orders"
    description: "Total number of returned orders placed on the website"
    sql: ${order_id};;
    filters: [record_type: "REFUND"]
  }

  measure: units_sold {
    type: sum
    label: "Units Sold"
    description: "Total number of items ordered on the website"
    sql: ${qty_ordered};;
    filters: [record_type: "SALE"]
  }

  measure: units_shipped {
    type: sum
    label: "Units Shipped"
    description: "Total number of items shipped"
    sql: ${qty_shipped};;
  }

  measure: units_returned {
    type: sum
    label: "Units Returned"
    description: "Total number of items returned"
    sql: ${qty_refunded};;
  }

  measure: net_units {
    type: number
    label: "Net Units"
    description: "Total number of items shipped that were not returned"
    sql: ${units_shipped} - ${units_returned};;
  }

  measure: upt {
    type: number
    label: "Unit Per Transaction"
    description: "Average number of units within each order"
    value_format_name: decimal_2
    sql:  ${units_sold}/${sfcc_number_of_orders};;
  }

  measure: aov {
    type: number
    label: "Average Order Value (USD)"
    description: "Average Value of each order"
    value_format_name: decimal_2
    sql: ${sfcc_gross_revenue_usd}/${sfcc_number_of_orders};;
  }

  measure: unit_selling_price {
    type:  number
    label: "Unit Selling Price"
    description: "Average price of each item ordered"
    value_format_name: usd
    sql: NULLIF(${sfcc_gross_revenue_usd} / NULLIF(${units_sold},0),0) ;;
  }

  measure: sfcc_return_rate {
    type: number
    label: "Returned Order (%)"
    description: "Percentage of shipped orders that were returned"
    value_format_name: percent_2
    sql: NULLIF(${sfcc_gross_returns_usd}/NULLIF(${sfcc_gross_revenue_usd},0),0) ;;
  }

  measure: sfcc_cancellation_rate{
    type: number
    label: "Cancelled Order (%)"
    description: "Percentage of incoming orders that were cancelled"
    value_format_name: percent_2
    sql: NULLIF(${sfcc_gross_cancelled_usd}/NULLIF(${sfcc_gross_revenue_usd},0),0) ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
