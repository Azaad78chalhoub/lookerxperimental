view: swarovski_adjust {
  sql_table_name: `chb-prod-data-comm.prod_commercial.swarovski_adjust`
    ;;

  dimension: brand_security{
    type: string
    hidden: yes
    sql: "SWAROVSKI" ;;
  }

  dimension: activity_kind {
    type: string
    sql: ${TABLE}.activity_kind ;;
  }

  dimension: adgroup_name {
    type: string
    sql: ${TABLE}.adgroup_name ;;
  }

  dimension: adid {
    type: string
    sql: ${TABLE}.adid ;;
    primary_key: yes
  }

  dimension: android_id {
    type: string
    sql: ${TABLE}.android_id ;;
  }

  dimension: app_id {
    type: string
    sql: ${TABLE}.app_id ;;
  }

  dimension: app_version {
    type: string
    sql: ${TABLE}.app_version ;;
  }

  dimension: campaign_name {
    type: string
    sql: ${TABLE}.campaign_name ;;
  }

  dimension: city {
    type: string
    sql: ${TABLE}.city ;;
  }

  dimension: click_referer {
    type: string
    sql: ${TABLE}.click_referer ;;
  }

  dimension_group: click {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.click_time AS TIMESTAMP) ;;
  }

  dimension: connection_type {
    type: string
    sql: ${TABLE}.connection_type ;;
  }

  dimension: cost_amount {
    type: number
    sql: ${TABLE}.cost_amount ;;
    hidden: yes
  }

  dimension: cost_currency {
    type: string
    sql: ${TABLE}.cost_currency ;;
    hidden: yes
  }

  dimension: cost_type {
    type: string
    sql: ${TABLE}.cost_type ;;
    hidden: yes
  }

  dimension: country {
    type: string
    map_layer_name: countries
    sql: ${TABLE}.country ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.created_at AS TIMESTAMP) ;;
  }

  dimension: creative_name {
    type: string
    sql: ${TABLE}.creative_name ;;
  }

  dimension: currency {
    type: string
    sql: ${TABLE}.currency ;;
  }

  dimension: deeplink {
    type: string
    sql: ${TABLE}.deeplink ;;
  }

  dimension: device_name {
    type: string
    sql: ${TABLE}.device_name ;;
  }

  dimension: environment {
    type: string
    sql: ${TABLE}.environment ;;
  }

  dimension: event_name {
    type: string
    sql: ${TABLE}.event_name ;;
  }

  dimension: first_tracker_name {
    type: string
    sql: ${TABLE}.first_tracker_name ;;
  }

  dimension: idfa {
    type: string
    sql: ${TABLE}.idfa ;;
  }

  dimension: idfv {
    type: string
    sql: ${TABLE}.idfv ;;
  }

  dimension_group: installed {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.installed_at AS TIMESTAMP);;
  }

  dimension: ip_address {
    type: string
    sql: ${TABLE}.ip_address ;;
  }

  dimension: is_reattributed {
    type: string
    sql: ${TABLE}.is_reattributed ;;
  }

  dimension: isp {
    type: string
    sql: ${TABLE}.isp ;;
  }

  dimension: language {
    type: string
    sql: ${TABLE}.language ;;
  }

  dimension: last_time_spent {
    type: number
    sql: ${TABLE}.last_time_spent ;;
    hidden: yes
  }

  dimension: last_tracker_name {
    type: string
    sql: ${TABLE}.last_tracker_name ;;
  }

  dimension: match_type {
    type: string
    sql: ${TABLE}.match_type ;;
  }

  dimension: network_name {
    type: string
    sql: ${TABLE}.network_name ;;
  }

  dimension: number_of_sessions {
    type: number
    sql: ${TABLE}.number_of_sessions ;;
  }
  dimension: reattribution_count {
    type: number
    sql: ${TABLE}.reattribution_count ;;
    hidden: yes

  }
  dimension: purchase_count {
    type: number
    sql: ${TABLE}.purchase_count ;;
    hidden: yes

  }
  dimension: install_count {
    type: number
    sql: ${TABLE}.install_count ;;
    hidden: yes

  }
  dimension: add_to_cart_count {
    type: number
    sql: ${TABLE}.add_to_cart_count ;;
    hidden: yes

  }
  dimension: product_view_count {
    type: number
    sql: ${TABLE}.product_view_count ;;
    hidden: yes

  }
  dimension: remove_from_cart_count {
    type: number
    sql: ${TABLE}.remove_from_cart_count ;;
    hidden: yes

  }

  dimension: os_name {
    type: string
    sql: ${TABLE}.os_name ;;
  }

  dimension: os_version {
    type: string
    sql: ${TABLE}.os_version ;;
  }

  dimension: partner_parameters {
    type: string
    sql: ${TABLE}.partner_parameters ;;
  }

  dimension: publisher_parameters {
    type: string
    sql: ${TABLE}.publisher_parameters ;;
  }

  dimension: rejection_reason {
    type: string
    sql: ${TABLE}.rejection_reason ;;
  }

  dimension: reporting_cost {
    type: number
    sql: ${TABLE}.reporting_cost ;;
    hidden: yes
  }

  dimension: reporting_currency {
    type: string
    sql: ${TABLE}.reporting_currency ;;
    hidden: yes
  }

  dimension: reporting_revenue {
    type: number
    sql: ${TABLE}.reporting_revenue ;;
    hidden: yes
  }

  dimension: revenue_float_local {
    type: number
    sql: ${TABLE}.revenue_float_local ;;
    hidden: yes
  }
  dimension: revenue_float_aed {
    type: number
    sql: ${TABLE}.revenue_float_aed ;;
    hidden: yes
  }

  dimension: session_count {
    type: number
    sql: ${TABLE}.session_count ;;
    hidden: yes
  }

  dimension: time_spent {
    type: number
    sql: ${TABLE}.time_spent ;;
    hidden: yes
  }

  dimension: tracker {
    type: string
    sql: ${TABLE}.tracker ;;
  }

  dimension: user_agent {
    type: string
    sql: ${TABLE}.user_agent ;;
  }

 measure: aed_revenue_float {
  label: "Revenue (AED)"
  description: "Revenue Float converted to AED"
  type: sum
  sql: ${revenue_float_aed} ;;
  filters: [event_name: "purchase"]
}
  measure: local_revenue_float{
    label: "Revenue (Local)"
    type: sum
    sql: ${revenue_float_local} ;;
    filters: [event_name: "purchase"]
  }
  measure: number_of_sessions_measure {
  label: "Session Count"
  description: "Count of session/install/reattribution activity_kind"
  type: sum
  sql: ${number_of_sessions};;
  }
  measure: reattribution_count_measure {
    description: "Count of reattribution activity_kind"
    label: "Reattribution Count"
    hidden: yes
    type: sum
    sql: ${reattribution_count};;
  }

  measure: install_count_measure {
    description: "Count of install activity_kind"
    label: "Install Count"
    type: sum
    sql: ${install_count};;
  }
  measure: order_count {
    description: "Count of purchase event_name"
    label: "Order Count"
    type: sum
    sql: ${purchase_count};;
  }
measure: reporting_revenue_amount {
  label: "Reporting Revenue (AED)"
  type:  sum
  sql: ${reporting_revenue} ;;
  filters: [event_name: "purchase"]
}
measure: active_user_count {
  label: "Active User Count"
  description: "Count distinct of IDFA"
  type: count_distinct
  hidden: yes
  sql: ${idfa};;
  }
measure: add_to_cart_count_measure {
  description: "Count of add_to_cart event_name"
  label: "Add to Cart Count"
  type: sum
  sql: ${add_to_cart_count};;
  }

  measure: remove_from_cart_count_measure {
  description: "Count of add_to_cart event_name"
  label: "Remove from Cart Count"
  type: sum
  sql: ${remove_from_cart_count};;
  }
  measure: product_view_count_measure {
    description: "Count of product_view event_name"
    label: "Product View Count"
    type: sum
    sql: ${product_view_count};;
  }
  measure: conversion_rate {
    description: "Order Count/Session Count"
    label: "Conversion Rate (%)"
    type: number
    value_format_name: percent_2
    sql: ${order_count}/NULLIF(${number_of_sessions_measure},0);;
  }
  measure: average_order_value {
    description: "Reporting Revenue/Order Count in AED"
    label: "Average Order Value (AED)"
    type: number
    sql: ${reporting_revenue_amount}/NULLIF(${order_count}, 0);;
  }
measure: reporting_cost_amount {
  type:  sum
  hidden: yes
  sql: ${reporting_cost} ;;
}
measure: cost {
  type: sum
  hidden: yes
  sql: ${cost_amount} ;;
}
measure: time_spent_amount {
  type: sum
  hidden: yes
  sql: ${time_spent} ;;
}


  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      creative_name,
      campaign_name,
      device_name,
      last_tracker_name,
      first_tracker_name,
      network_name,
      event_name,
      os_name,
      adgroup_name
    ]
  }
}
