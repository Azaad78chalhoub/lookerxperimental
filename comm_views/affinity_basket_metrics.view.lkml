view: affinity_basket_metrics {
  sql_table_name: `chb-prod-data-comm.prod_commercial.affinity_basket_metrics`  ;;

  dimension: common_date {
    sql: ${TABLE}.common_date ;;
  }

  dimension: product_id {
    sql: ${TABLE}.product_id ;;
  }

  dimension: basket_sales {
    type: number
    hidden: yes
    sql: ${TABLE}.basket_sales ;;
  }

  dimension:  basket_margin {
    type: number
    hidden: yes
    sql: ${TABLE}.basket_margin ;;
  }

  dimension: brand {
    sql: ${TABLE}.brand ;;
  }

  dimension: primary_key {
    type: number
    primary_key: yes
    hidden: yes
    sql: concat(${TABLE}.common_date,${TABLE}.brand,${TABLE}.product_id) ;;
  }

  measure:  sum_basket_margin {
    label: "Basket Margin"
    type: sum
    sql: ${TABLE}.basket_margin ;;
  }

  measure: product_total_rest_of_basket_margin_A {
    label: "Rest of basket margin - Product A"
    type: number
    sql: ${sum_basket_margin}-IFNULL(${total_order_product_A.m_product_margin},0) ;;
    value_format_name: decimal_2
  }

  measure: product_total_rest_of_basket_margin_B {
    label: "Rest of basket margin - Product B"
    type: number
    sql: ${sum_basket_margin}-IFNULL(${total_order_product_B.m_product_margin},0) ;;
    value_format_name: decimal_2
  }

  measure: product_average_rest_of_basket_margin_A {
    label: "Average Rest of basket margin - Product A"
    type: number
    sql: ${product_total_rest_of_basket_margin_A}/${total_order_product_A.m_product_order_count} ;;
    value_format_name: decimal_2
  }

}
