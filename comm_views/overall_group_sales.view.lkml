include: "../_common/period_over_period.view"

view: overall_group_sales {
  sql_table_name: `chb-prod-data-comm.prod_commercial.sales_orders_items_unified_transposed_looker`
    ;;

  dimension: vertical_security {
    type: string
    hidden: yes
    sql: ${TABLE}.vertical ;;
  }

  dimension: brand_security {
    type: string
    hidden: yes
    sql: ${TABLE}.brand ;;
  }

  dimension: amount_refunded {
    type: number
    hidden: yes
    sql: ${TABLE}.amount_refunded ;;
  }

  dimension: canceled_amount_local {
    type: number
    hidden: yes
    sql: ${TABLE}.canceled_amount_local ;;
  }

  dimension: canceled_amount_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.canceled_amount_usd ;;
  }

  dimension: discount_amount {
    type: number
    hidden: yes
    sql: ${TABLE}.discount_amount ;;
  }

  dimension: discount_refunded {
    type: number
    hidden: yes
    sql: ${TABLE}.discount_refunded ;;
  }

  dimension: gross_net_sales {
    type: number
    hidden: yes
    sql: ${TABLE}.gross_net_sales ;;
  }

  dimension: gross_net_sales_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.gross_net_sales_usd ;;
  }

  dimension: gross_revenue_local {
    type: number
    hidden: yes
    sql: ${TABLE}.gross_revenue_local ;;
  }

  dimension: gross_revenue_local_minus {
    type: number
    hidden: yes
    sql: ${TABLE}.gross_revenue_local_minus ;;
  }

  dimension: gross_revenue_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.gross_revenue_usd ;;
  }

  dimension: gross_revenue_usd_minus {
    type: number
    hidden: yes
    sql: ${TABLE}.gross_revenue_usd_minus ;;
  }

  dimension_group: order {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.order_date ;;
  }


  dimension_group: order_datetime {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.order_datetime AS TIMESTAMP) ;;
  }

  dimension: order_id {
    type: string
    sql: ${TABLE}.order_id ;;
  }

  dimension: payment_method {
    type: string
    sql: ${TABLE}.payment_method ;;
  }

  dimension: price {
    type: number
    sql: ${TABLE}.price ;;
  }

  dimension: qty_canceled {
    type: number
    hidden: yes
    sql: ${TABLE}.qty_canceled ;;
  }

  dimension: qty_ordered {
    type: number
    hidden: yes
    sql: ${TABLE}.qty_ordered ;;
  }

  dimension: qty_refunded {
    type: number
    hidden: yes
    sql: ${TABLE}.qty_refunded ;;
  }

  dimension: qty_shipped {
    type: number
    hidden: yes
    sql: ${TABLE}.qty_shipped ;;
  }

  dimension_group: record {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.record_date ;;
  }

  dimension_group: record_datetime {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      day_of_week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.record_datetime AS TIMESTAMP) ;;
  }

  dimension: business_week_no{
    type: number
    description: "Week No. extracted from the business date"
    sql: EXTRACT( WEEK FROM(PARSE_DATE('%Y-%m-%d', CAST(${TABLE}.record_datetime AS STRING))))+1 ;;
  }

  extends: [period_over_period]
  dimension_group: pop_no_tz {
    sql: ${record_date} ;;
  }

  dimension: record_type {
    type: string
    sql: ${TABLE}.record_type ;;
  }

  dimension: refund_amount_local {
    type: number
    hidden: yes
    sql: ${TABLE}.refund_amount_local ;;
  }

  dimension: refund_amount_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.refund_amount_usd ;;
  }

  dimension: shipment_net_sales {
    type: number
    hidden: yes
    sql: ${TABLE}.shipment_net_sales ;;
  }

  dimension: shipment_net_sales_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.shipment_net_sales_usd ;;
  }

  dimension: shipment_revenue_local {
    type: number
    hidden: yes
    sql: ${TABLE}.shipment_revenue_local ;;
  }

  dimension: shipment_revenue_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.shipment_revenue_usd ;;
  }

  dimension: shipped_revenue_local_minus {
    type: number
    hidden: yes
    sql: ${TABLE}.shipped_revenue_local_minus ;;
  }

  dimension: shipped_revenue_usd_minus {
    type: number
    hidden: yes
    sql: ${TABLE}.shipped_revenue_usd_minus ;;
  }

  dimension: sku {
    type: string
    sql: ${TABLE}.sku ;;
  }

  dimension: source {
    type: string
    sql: ${TABLE}.source ;;
  }

  dimension: status {
    type: string
    sql: ${TABLE}.status ;;
  }

  dimension: store_country {
    type: string
    sql: ${TABLE}.store_country ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }

  dimension: store_id {
    type: number
    sql: ${TABLE}.store_id ;;
  }

  dimension: primary_key {
    type:  string
    primary_key:  yes
    hidden: yes
    sql:  ${TABLE}.primary_key ;;
  }

  dimension: brand {
    type: string
    sql: CASE
            WHEN ${TABLE}.brand='LEVELSHOES' then 'LEVEL SHOES'
            WHEN ${TABLE}.brand='THE_DEAL' then 'THE DEAL'
            ELSE UPPER(${TABLE}.brand)
          END ;;
  }

  dimension: ship_net_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.ship_net_usd ;;
  }

  dimension: ship_net_local {
    type: number
    hidden: yes
    sql: ${TABLE}.ship_net_local ;;
  }

  ###########################
  ##### Sales Orders ########
  ###########################

  measure: all_orders {
    type: count_distinct
    group_label: "Sales Order"
    label: "# Orders"
    description: "Count of Distinct Order IDs Gross"
    sql: ${order_id} ;;
    filters: [record_type: "SALE"]
  }

  measure: units_ordered {
    type: sum
    group_label: "Sales Order"
    label: "Units Ordered"
    description: "Sum of Qty Ordered"
    sql: ${TABLE}.qty_ordered;;
    filters: [record_type: "SALE"]
  }

  measure: gross_revenue_local_currency {
    type: sum
    group_label: "Sales Order"
    label: "Gross Revenue Local"
    description: "Gross Revenue Local"
    value_format: "#,##0.00"
    sql: ${TABLE}.gross_revenue_local;;
    filters: [record_type: "SALE"]
  }

  measure: gross_revenue_usd_ {
    type: sum
    group_label: "Sales Order"
    label: "Gross Revenue USD"
    description: "Gross Revenue USD"
    value_format: "$#,##0.00"
    sql: ${TABLE}.gross_revenue_usd;;
    filters: [record_type: "SALE"]
  }

  measure: gross_revenue_percent {
    type: percent_of_total
    group_label: "Sales Order"
    label: "% Gross Revenue"
    description: "% of Gross Revenue in selected dimension"
    value_format_name: decimal_2
    sql: ${gross_revenue_usd_} ;;
  }

  measure: upt {
    type: number
    group_label: "Sales Order"
    label: "UPT"
    description: "Average number of units per order/transaction"
    value_format_name: decimal_2
    sql:${units_ordered}/nullif(${all_orders}, 0);;
  }

  measure: aov_local {
    type: number
    group_label: "Sales Order"
    label: "Average Order Value Local"
    description: "Average of Grand Total"
    value_format: "#,##0.00"
    sql: ${gross_revenue_local_currency}/${all_orders};;
  }

  measure: aov_usd {
    type: number
    group_label: "Sales Order"
    label: "Average Order Value USD"
    description: "Average of Grand Total USD"
    value_format: "#,##0.00"
    sql: ${gross_revenue_usd_}/${all_orders};;
  }

  measure: unit_selling_price {
    type:  number
    label: "USP"
    group_label: "Sales Order"
    description: "Unit Selling Price - Gross revenue / Units ordered"
    value_format: "#,##0.00"
    sql: ${gross_revenue_usd_} / NULLIF(${units_ordered},0) ;;
  }


##############################
###### Cancelled Orders ######
##############################

  measure: count_cancelled_orders {
    type: count_distinct
    hidden:  yes
    group_label: "Cancelled Order"
    label: "# Cancelled Orders"
    description: "Count of Cancelled Orders"
    sql: ${order_id};;
    filters: [record_type: "CANCELLED"]
  }

  measure: unit_canceled {
    type: sum
    group_label: "Cancelled Order"
    label: "Units Canceled"
    description: "Sum of Qty Ordered"
    sql: ${TABLE}.qty_canceled;;
  }

  measure: cancelled_order_ratio {
    type: number
    group_label: "Cancelled Order"
    label: "Cancelled Order % by units"
    description: "Cancelled Order %"
    value_format_name: percent_2
    sql: ${count_cancelled_orders}/NULLIF(${all_orders},0) ;;
  }

  measure: canceled_amount_local_currency {
    type: sum
    group_label: "Cancelled Order"
    label: "Cancelled amount Local"
    description: "Cancelled amount Local"
    value_format: "#,##0.00"
    sql: ${TABLE}.canceled_amount_local;;
    filters: [record_type: "CANCELLED"]
  }

  measure: canceled_amount_usd_ {
    type: sum
    group_label: "Cancelled Order"
    label: "Cancelled amount USD"
    description: "Cancelled amount USD"
    value_format: "$#,##0.00"
    sql: ${TABLE}.canceled_amount_usd;;
    filters: [record_type: "CANCELLED"]
  }

  measure: gross_revenue_minus_cancelled_local {
    type: number
    group_label: "Cancelled Order"
    label: "Gross Rev Minus Cancelled Local"
    description: "Gross Revenue Minus Cancelled Local"
    value_format: "#,##0.00"
    sql: ${gross_revenue_local_currency} - ${canceled_amount_local_currency};;
  }

  measure: gross_revenue_minus_cancelled_usd {
    type: number
    group_label: "Cancelled Order"
    label: "Gross Rev Minus Cancelled USD"
    description: "Gross Revenue Minus Cancelled USD"
    value_format: "$#,##0.00"
    sql: ${gross_revenue_usd_} - ${canceled_amount_usd_};;
  }

  measure: cancelled_item_ratio {
    type: number
    group_label: "Cancelled Order"
    label: "Cancelled Item %"
    description: "Cancelled Item %"
    value_format_name: percent_2
    sql: ${unit_canceled}/NULLIF(${units_ordered},0) ;;
  }

  measure: cancelled_rate {
    type: number
    group_label: "Cancelled Order"
    label: "Cancellation Rate %"
    description: "% of shipped orders that were returned based on value"
    value_format_name: percent_2
    sql: ${canceled_amount_usd_}/nullif(${gross_revenue_usd_}, 0);;
  }


  ### End of Cancelled orders measures

##############################
######  Shipped Orders  ######
##############################

  measure: shipped_orders {
    type: count_distinct
    group_label: "Shipped Order"
    label: "# Shipped Orders"
    description: "Count of Distinct Order IDs that were shipped"
    sql: ${order_id} ;;
    filters: [record_type: "SHIPMENT"]
  }

  measure: units_shipped {
    type: sum
    group_label: "Shipped Order"
    label: "Units Shipped"
    description: "Sum of Qty Shipped"
    sql: ${TABLE}.qty_shipped;;
    filters: [record_type: "SHIPMENT"]
  }

  measure: shipment_revenue_local_currency {
    type: sum
    group_label: "Shipped Order"
    label: "Shipped Revenue Local"
    description: "Shipped Revenue Local"
    value_format: "#,##0.00"
    sql: ${TABLE}.shipment_revenue_local;;
    filters: [record_type: "SHIPMENT"]
  }

  measure: shipment_revenue_usd_ {
    type: sum
    group_label: "Shipped Order"
    label: "Shipped Revenue USD"
    description: "Shipped Revenue USD"
    value_format: "$#,##0.00"
    sql: ${TABLE}.shipment_revenue_usd;;
    filters: [record_type: "SHIPMENT"]
  }

  measure: shipment_revenue_minus_cancelled_local_currency {
    type: number
    group_label: "Cancelled Order"
    label: "Shipped Rev Minus Cancelled Local"
    description: "Shipped Revenue Minus Cancelled Local"
    hidden: yes
    value_format: "#,##0.00"
    sql: ${shipment_revenue_local_currency} - ${canceled_amount_local_currency};;
  }

  measure: shipment_revenue_minus_cancelled_usd_ {
    type: number
    group_label: "Cancelled Order"
    label: "Shipped Rev Minus Cancelled USD"
    description: "Shipped Revenue Minus Cancelled USD"
    hidden: yes
    value_format: "$#,##0.00"
    sql: ${shipment_revenue_usd_} - ${canceled_amount_usd_};;
  }

### End of Shipped orders measures

##############################
######  Refund measures ######
##############################

  measure: units_refunded {
    type: sum
    group_label: "Returned Order"
    label: "Units Refunded"
    description: "Sum of Qty Refunded"
    sql: ${TABLE}.qty_refunded;;
    filters: [record_type: "REFUND"]
  }
  measure: refund_amount_local_currency {
    type: sum
    group_label: "Returned Order"
    label: "Refund Amount Local"
    description: "Refund Amount Local"
    value_format: "#,##0.00"
    sql: ${TABLE}.refund_amount_local;;
    filters: [record_type: "REFUND"]
  }

  measure: refund_amount_usd_ {
    type: sum
    group_label: "Returned Order"
    label: "Refund Amount USD"
    description: "Refund Amount USD"
    value_format: "$#,##0.00"
    sql: ${TABLE}.refund_amount_usd;;
    filters: [record_type: "REFUND"]
  }


  measure: gross_revenue_minus_cancelled_minus_returns_local {
    type: number
    group_label: "Returned Order"
    label: "Gross Rev Minus Cancelled Minus Returns Local"
    description: "Gross Rev Minus Cancelled Minus Returns Local"
    value_format: "#,##0.00"
    sql: ${gross_revenue_local_currency} - ${canceled_amount_local_currency} - ${refund_amount_local_currency};;
  }

  measure: gross_revenue_minus_cancelled_minus_returns_usd {
    type: number
    group_label: "Returned Order"
    label: "Gross Rev Minus Cancelled Minus Returns USD"
    description: "Gross Rev Minus Cancelled Minus Returns USD"
    value_format: "$#,##0.00"
    sql: ${gross_revenue_usd_} - ${canceled_amount_usd_} - ${refund_amount_usd_};;
  }

  measure: shipment_revenue_minus_cancelled_minus_returns_local {
    type: number
    group_label: "Returned Order"
    label: "Shipped Rev Minus Cancelled Minus Returns Local"
    description: "Shipped Rev Minus Cancelled Minus Returns Local"
    hidden: yes
    value_format: "#,##0.00"
    sql: ${shipment_revenue_local_currency} - ${canceled_amount_local_currency} - ${refund_amount_local_currency};;
  }

  measure: shipment_revenue_minus_cancelled_minus_returns_usd {
    type: sum
    group_label: "Shipped Order"
    label: "Net Shipped Sales USD"
    description: "Shipped Rev Net USD"
    value_format: "$#,##0.00"
    sql: ${ship_net_usd};;
  }

  measure: shipment_revenue_minus_cancelled_minus_returns_lcl {
    type: sum
    group_label: "Shipped Order"
    label: "Net Shipped Sales Local"
    description: "Shipped Rev Net Local"
    value_format: "#,##0.00"
    sql: ${ship_net_local};;
  }

  measure: return_rate {
    type: number
    group_label: "Returned Order"
    label: "Return Rate (%)"
    description: "% of shipped orders that were returned based on value"
    value_format_name: percent_2
    sql: ${refund_amount_usd_}/nullif(${shipment_revenue_usd_}, 0);;
  }

  measure: refunded_items {
    type: number
    group_label: "Returned Order"
    label: "Refunded Items (%)"
    description: "% of "
    value_format_name: percent_2
    sql: ${units_refunded}/nullif(${units_shipped}, 0);;
  }

}
