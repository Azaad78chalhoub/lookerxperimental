view: level_shoes_soh {
  sql_table_name: `chb-prod-data-comm.prod_commercial.fact_soh_lvl_shoes`
    ;;


  #######################################
  #####         DIMENSIONS          #####
  #######################################

  dimension: primary_key {
    primary_key: yes
    hidden:  yes
    type: number
    sql: CONCAT(${rms_sku}, ${rms_loc})  ;;
  }

  dimension: rms_sku {
    type: string
    label: "SKU"
    description: "Stock Keeping Unit"
    sql: ${TABLE}.RMS_ITEM ;;
  }

  dimension: rms_loc {
    type: number
    label: "Stock Location"
    description: "Stock Location Number"
    value_format: "##0"
    sql: ${TABLE}.RMS_LOC ;;
  }

  dimension: rms_status {
    type: string
    label: "Status"
    description: "Stock Status: Active/Inactive/Discontinued"
    sql: ${TABLE}.RMS_STATUS ;;
  }

  dimension: rms_stock_on_hand {
    hidden: no
    type: number
    label: "Item Stock On Hand"
    description: "Non-Aggregate Stock on Hand per item"
    group_label: "Non-Aggregate Dimensions"
    sql: ${TABLE}.RMS_STOCK_ON_HAND ;;
  }

  dimension: rms_qty_received {
    hidden: yes
    type: number
    sql: ${TABLE}.RMS_QTY_RECEIVED ;;
  }

  dimension: rms_unit_cost {
    hidden: yes
    type: number
    sql: ${TABLE}.RMS_UNIT_COST ;;
  }

  dimension_group: rms_soh_update_datetime {
    type: time
    label: "Last Update"
    description: "Time the stock value was last updated"
    datatype: datetime
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.RMS_SOH_UPDATE_DATETIME ;;
  }

  dimension_group: rms_first_received {
    type: time
    label: "First Received"
    description: "Time the stock was first received"
    datatype: datetime
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.RMS_FIRST_RECEIVED ;;
  }

  dimension_group: rms_last_received {
    type: time
    label: "Last Received"
    description: "Time the stock was last received"
    datatype: datetime
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.RMS_LAST_RECEIVED ;;
  }

  dimension_group: rms_first_sold {
    type: time
    label: "First Sold"
    description: "Date the stock was first sold"
    datatype: date
    timeframes: [
      raw,
    # time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.RMS_FIRST_SOLD ;;
  }


  #######################################
  #####          MEASURES           #####
  #######################################


  measure: count {
    hidden: yes
    type: count
    label: "Count"
    description: "Total number of rows"
    drill_fields: [stock_details*]
  }

  measure: count_sku {
    type: count_distinct
    label: "# of SKUs"
    description: "Total number of SKUs"
    sql: ${rms_sku} ;;
    drill_fields: [stock_details*]
  }

  measure: total_stock_on_hand {
    type: sum
    label: "Stock On Hand"
    description: "Total amount of stock on hand"
    value_format_name: decimal_0
    sql: ${rms_stock_on_hand} ;;
    drill_fields: [stock_details*]
  }

  measure: total_qty_received {
    type: sum
    label: "Quanitity Last Received"
    description: "Total amount of stock on last received"
    value_format_name: decimal_0
    sql: ${rms_qty_received} ;;
    drill_fields: [stock_details*]
  }

  measure: Total_unit_cost {
    type: sum
    label: "Unit Cost"
    description: "Total cost for each unit of unique stock"
    value_format_name: decimal_2
    sql: ${rms_unit_cost} ;;
    drill_fields: [stock_details*]
  }

  set: stock_details {
    fields: [rms_sku, rms_loc, rms_soh_update_datetime_date, rms_status, rms_stock_on_hand]
  }
}
