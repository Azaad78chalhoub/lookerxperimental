view: faces_ga_report_sku {
  sql_table_name: `chb-prod-stage-comm.prod_comm.stg_comm_faces_ga_report_sku`;;

  dimension: pk {
    type:  string
    primary_key:  yes
    sql:  concat(${product_sku}, ${campaign}, ${source_medium}, cast(${date_date} as string)) ;;
  }

  dimension_group: date {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.date ;;
  }

  dimension: product_sku {
    type: string
    description: "SKU of the item"
    sql: ${TABLE}.product_sku ;;
  }

  dimension: campaign {
    type: string
    description: "Campaign"
    sql: ${TABLE}.campaign ;;
  }

  dimension: source_medium {
    type: string
    description: "Source / Medium"
    sql: ${TABLE}.source_medium ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }

  measure: product_detail_views {
    type: sum
    label: "# of Product Views"
    description: "Total number of product views"
    sql: ${TABLE}.product_detail_views ;;
  }

  measure: product_adds_to_cart {
    type: sum
    label: "# of Product Add to Carts"
    description: "Sum of products aded to cart"
    sql: ${TABLE}.product_adds_to_cart ;;
  }

  measure: product_checkouts {
    type: sum
    label: "# of Product Checkouts"
    description: "Sum of product checkouts"
    sql: ${TABLE}.product_checkouts ;;
  }

  measure: cart_to_detail_rate {
    type: number
    label: "Cart to Detail rate"
    description: "Sum of products added to cart / Sum of product detail views"
    sql: ${product_adds_to_cart}/NULLIF(${product_detail_views},0) ;;
  }

  measure: checkout_to_detail_rate {
    type: number
    label: "Checkout to Detail rate"
    description: "Sum of product checkouts / Sum of product detail views"
    sql: ${product_checkouts}/NULLIF(${product_detail_views},0) ;;
  }

}
