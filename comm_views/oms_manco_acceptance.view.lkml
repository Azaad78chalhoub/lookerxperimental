view: oms_manco_acceptance {
  sql_table_name: `chb-prod-data-comm.prod_commercial.oms_manco_acceptance`
    ;;

  dimension: pk {
    type: string
    hidden: yes
    primary_key: yes
    sql: concat(${TABLE}.order_id, ${TABLE}.detail_id, ${TABLE}.manco_location_id, cast(${TABLE}.manco_date as string));;
  }

  dimension: brand {
    type: string
    hidden: yes
    sql: ${TABLE}.brand ;;
  }

  dimension_group: manco {
    type: time
    description: "Local datetime by market"
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    datatype: datetime
    sql: ${TABLE}.manco_date ;;
  }

  dimension: manco_location_id {
    type: string
    sql: ${TABLE}.manco_location_id ;;
  }

  dimension: manco_location_internal_id {
    type: string
    sql: ${TABLE}.manco_location_internal_id ;;
  }

  dimension: manco_reason {
    type: string
    sql: ${TABLE}.manco_reason ;;
  }

  dimension: order_id {
    type: string
    hidden: yes
    sql: ${TABLE}.order_id ;;
  }

  dimension: detail_id {
    type: number
    sql: cast(${TABLE}.detail_id as int64) ;;
  }

  dimension_group: original_manco_date {
    type: time
    description: "Original datetime coming from OMS, in CET"
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.DATETIME(original_manco_date) ;;
  }

  dimension: sku {
    type: string
    hidden: yes
    sql: ${TABLE}.sku ;;
  }

  measure: number_of_manco {
    type: count_distinct
    label: "# of Manco orders"
    description: "Number of cancelled orders with Manco"
    sql: ${order_id} ;;
  }

  measure: number_of_manco_item {
    type: count_distinct
    label: "# of Manco items"
    description: "Number of cancelled items with Manco"
    sql: ${sku} ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
