view: tryano_stock {
  sql_table_name: `chb-prod-data-comm.prod_commercial.oracle_x_pim_tryano`
    ;;
  drill_fields: [id]

  dimension: key {
    primary_key: yes
    type: string
    sql: concat(${TABLE}.rms_item,${TABLE}.rms_loc) ;;
  }

  dimension: id {
    type: string
    sql: ${TABLE}.id ;;
  }

  dimension: rms_av_cost {
    type: number
    group_label: "RMS"
    sql: ${TABLE}.RMS_AV_COST ;;
  }

  dimension: rms_average_weight {
    type: number
    group_label: "RMS"
    sql: ${TABLE}.RMS_AVERAGE_WEIGHT ;;
  }

  dimension_group: rms_create_datetime {
    type: time
    group_label: "RMS Dates"
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.RMS_CREATE_DATETIME ;;
  }

  dimension: rms_customer_backorder {
    type: number
    group_label: "RMS"
    sql: ${TABLE}.RMS_CUSTOMER_BACKORDER ;;
  }

  dimension: rms_customer_resv {
    type: number
    group_label: "RMS"
    sql: ${TABLE}.RMS_CUSTOMER_RESV ;;
  }

  dimension: rms_finisher_av_retail {
    type: number
    group_label: "RMS"
    sql: ${TABLE}.RMS_FINISHER_AV_RETAIL ;;
  }

  dimension: rms_finisher_units {
    type: number
    group_label: "RMS"
    sql: ${TABLE}.RMS_FINISHER_UNITS ;;
  }

  dimension_group: rms_first_received {
    type: time
    group_label: "RMS Dates"
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.RMS_FIRST_RECEIVED ;;
  }

  dimension: rms_first_sold {
    type: string
    group_label: "RMS"
    sql: ${TABLE}.RMS_FIRST_SOLD ;;
  }

  dimension: rms_in_transit_qty {
    type: number
    group_label: "RMS"
    sql: ${TABLE}.RMS_IN_TRANSIT_QTY ;;
  }

  dimension: rms_item {
    type: string
    group_label: "RMS"
    sql: ${TABLE}.RMS_ITEM ;;
  }

  dimension: rms_item_grandparent {
    type: string
    group_label: "RMS"
    sql: ${TABLE}.RMS_ITEM_GRANDPARENT ;;
  }

  dimension: rms_item_parent {
    type: string
    group_label: "RMS"
    sql: ${TABLE}.RMS_ITEM_PARENT ;;
  }

  dimension_group: rms_last_hist_export {
    type: time
    group_label: "RMS Dates"
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.RMS_LAST_HIST_EXPORT_DATE ;;
  }

  dimension_group: rms_last_received {
    type: time
    group_label: "RMS Dates"
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.RMS_LAST_RECEIVED ;;
  }

  dimension: rms_last_sold {
    type: string
    group_label: "RMS"
    sql: ${TABLE}.RMS_LAST_SOLD ;;
  }

  dimension_group: rms_last_update_datetime {
    type: time
    group_label: "RMS Dates"
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.RMS_LAST_UPDATE_DATETIME ;;
  }

  dimension: rms_last_update_id {
    type: string
    group_label: "RMS"
    sql: ${TABLE}.RMS_LAST_UPDATE_ID ;;
  }

  dimension: rms_loc {
    type: number
    group_label: "RMS"
    sql: ${TABLE}.RMS_LOC ;;
  }

  dimension: rms_loc_type {
    type: string
    group_label: "RMS"
    sql: ${TABLE}.RMS_LOC_TYPE ;;
  }

  dimension: rms_non_sellable_qty {
    type: number
    group_label: "RMS"
    sql: ${TABLE}.RMS_NON_SELLABLE_QTY ;;
  }

  dimension: rms_pack_comp_cust_back {
    type: number
    group_label: "RMS"
    sql: ${TABLE}.RMS_PACK_COMP_CUST_BACK ;;
  }

  dimension: rms_pack_comp_cust_resv {
    type: number
    group_label: "RMS"
    sql: ${TABLE}.RMS_PACK_COMP_CUST_RESV ;;
  }

  dimension: rms_pack_comp_exp {
    type: number
    group_label: "RMS"
    sql: ${TABLE}.RMS_PACK_COMP_EXP ;;
  }

  dimension: rms_pack_comp_intran {
    type: number
    group_label: "RMS"
    sql: ${TABLE}.RMS_PACK_COMP_INTRAN ;;
  }

  dimension: rms_pack_comp_non_sellable {
    type: number
    group_label: "RMS"
    sql: ${TABLE}.RMS_PACK_COMP_NON_SELLABLE ;;
  }

  dimension: rms_pack_comp_resv {
    type: number
    group_label: "RMS"
    sql: ${TABLE}.RMS_PACK_COMP_RESV ;;
  }

  dimension: rms_pack_comp_soh {
    type: number
    group_label: "RMS"
    sql: ${TABLE}.RMS_PACK_COMP_SOH ;;
  }

  dimension: rms_primary_cntry {
    type: string
    group_label: "RMS"
    sql: ${TABLE}.RMS_PRIMARY_CNTRY ;;
  }

  dimension: rms_primary_supp {
    type: number
    group_label: "RMS"
    sql: ${TABLE}.RMS_PRIMARY_SUPP ;;
  }

  dimension: rms_qty_received {
    type: number
    group_label: "RMS"
    sql: ${TABLE}.RMS_QTY_RECEIVED ;;
  }

  dimension: rms_qty_sold {
    type: number
    group_label: "RMS"
    sql: ${TABLE}.RMS_QTY_SOLD ;;
  }

  dimension: rms_rtv_qty {
    type: number
    group_label: "RMS"
    sql: ${TABLE}.RMS_RTV_QTY ;;
  }

  dimension_group: rms_soh_update_datetime {
    type: time
    group_label: "RMS Dates"
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.RMS_SOH_UPDATE_DATETIME ;;
  }

  dimension: rms_status {
    type: string
    group_label: "RMS"
    sql: ${TABLE}.RMS_STATUS ;;
  }

  dimension: rms_tsf_expected_qty {
    type: number
    group_label: "RMS"
    sql: ${TABLE}.RMS_TSF_EXPECTED_QTY ;;
  }

  dimension: rms_tsf_reserved_qty {
    type: number
    group_label: "RMS"
    sql: ${TABLE}.RMS_TSF_RESERVED_QTY ;;
  }

  dimension: rms_unit_cost {
    type: number
    group_label: "RMS"
    sql: ${TABLE}.RMS_UNIT_COST ;;
  }

  dimension: rms_stock_on_hand {
    type: number
    group_label: "RMS"
    sql: ${TABLE}.RMS_STOCK_ON_HAND ;;
  }

  measure: stock_yasmall {
    type: sum
    group_label: "Stock"
    label: "Stock - Yas Mall"
    sql: ${rms_stock_on_hand} ;;
    filters: [rms_loc: "11001"]
  }

  measure: stock_dubaimall {
    type: sum
    group_label: "Stock"
    label: "Stock - Dubai Mall"
    sql: ${rms_stock_on_hand} ;;
    filters: [rms_loc: "76407"]
  }

  measure: stock_saks {
    type: sum
    group_label: "Stock"
    label: "Stock - SAKS"
    sql: ${rms_stock_on_hand} ;;
    filters: [rms_loc: "15001"]
  }

  measure: stock_yasmall_mk {
    type: sum
    group_label: "Stock"
    label: "Stock - Yas Mall - Michael Kors"
    sql: ${rms_stock_on_hand} ;;
    filters: [rms_loc: "60017"]
  }

  measure: stock_mallofqatar_tb {
    type: sum
    group_label: "Stock"
    label: "Stock - Mall of Qatar - Tory Burch"
    sql: ${rms_stock_on_hand} ;;
    filters: [rms_loc: "63017"]
  }

  measure: stock_avenues3_lacoste {
    type: sum
    group_label: "Stock"
    label: "Stock - Avenues III - Lacoste"
    sql: ${rms_stock_on_hand} ;;
    filters: [rms_loc: "52019"]
  }

  measure: stock_nakheelmall_lacoste {
    type: sum
    group_label: "Stock"
    label: "Stock - Nakheel Mall - Lacoste"
    sql: ${rms_stock_on_hand} ;;
    filters: [rms_loc: "52035"]
  }

  measure: stock_dohafestivalcity_lacoste {
    type: sum
    group_label: "Stock"
    label: "Stock - Doha Festival City - Lacoste"
    sql: ${rms_stock_on_hand} ;;
    filters: [rms_loc: "52045"]
  }
}
