view: tryano_adjust {
  sql_table_name: `chb-prod-data-comm.prod_commercial.tryano_adjust`
    ;;

  dimension: brand_security{
    type: string
    hidden: yes
    sql: "TRYANO" ;;
  }

  dimension: activity_kind {
    type: string
    sql: ${TABLE}.activity_kind ;;
  }

  dimension: add_to_cart_count {
    hidden: yes
    type: number
    sql: ${TABLE}.add_to_cart_count ;;
  }

  dimension: adgroup_name {
    type: string
    sql: ${TABLE}.adgroup_name ;;
  }

  dimension: adid {
    type: string
    sql: ${TABLE}.adid ;;
  }

  dimension: android_id {
    type: string
    sql: ${TABLE}.android_id ;;
  }

  dimension: app_id {
    type: string
    sql: ${TABLE}.app_id ;;
  }

  dimension: app_version {
    type: string
    sql: ${TABLE}.app_version ;;
  }

  dimension: campaign_name {
    type: string
    sql: ${TABLE}.campaign_name ;;
  }

  dimension: city {
    type: string
    sql: ${TABLE}.city ;;
  }

  dimension: click_referer {
    type: string
    sql: ${TABLE}.click_referer ;;
  }

  dimension_group: click {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.click_time AS TIMESTAMP) ;;
  }

  dimension: connection_type {
    type: string
    sql: ${TABLE}.connection_type ;;
  }

  dimension: cost_amount {
    hidden: yes
    type: number
    sql: ${TABLE}.cost_amount ;;
  }

  dimension: cost_currency {
    hidden: yes
    type: string
    sql: ${TABLE}.cost_currency ;;
  }

  dimension: cost_type {
    hidden: yes
    type: string
    sql: ${TABLE}.cost_type ;;
  }

  dimension: country {
    type: string
    map_layer_name: countries
    sql: ${TABLE}.country ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.created_at AS TIMESTAMP) ;;
  }

  dimension: creative_name {
    type: string
    sql: ${TABLE}.creative_name ;;
  }

  dimension: currency {
    type: string
    sql: ${TABLE}.currency ;;
  }

  dimension: deeplink {
    type: string
    sql: ${TABLE}.deeplink ;;
  }

  dimension: device_name {
    type: string
    sql: ${TABLE}.device_name ;;
  }

  dimension: environment {
    type: string
    sql: ${TABLE}.environment ;;
  }

  dimension: event_name {
    type: string
    sql: ${TABLE}.event_name ;;
  }

  dimension: first_tracker_name {
    type: string
    sql: ${TABLE}.first_tracker_name ;;
  }

  dimension: idfa {
    type: string
    sql: ${TABLE}.idfa ;;
  }

  dimension: idfv {
    type: string
    sql: ${TABLE}.idfv ;;
  }

  dimension: install_count {
    hidden: yes
    type: number
    sql: ${TABLE}.install_count ;;
  }

  dimension_group: installed {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.installed_at AS TIMESTAMP) ;;
  }

  dimension: ip_address {
    type: string
    sql: ${TABLE}.ip_address ;;
  }

  dimension: is_reattributed {
    type: string
    sql: ${TABLE}.is_reattributed ;;
  }

  dimension: isp {
    type: string
    sql: ${TABLE}.isp ;;
  }

  dimension: language {
    type: string
    sql: ${TABLE}.language ;;
  }

  dimension: last_time_spent {
    type: number
    sql: ${TABLE}.last_time_spent ;;
  }

  dimension: last_tracker_name {
    type: string
    sql: ${TABLE}.last_tracker_name ;;
  }

  dimension: match_type {
    type: string
    sql: ${TABLE}.match_type ;;
  }

  dimension: network_name {
    type: string
    sql: ${TABLE}.network_name ;;
  }

  dimension: number_of_sessions {
    hidden: yes
    type: number
    sql: ${TABLE}.number_of_sessions ;;
  }

  dimension: order_id {
    type: string
    sql: ${TABLE}.order_id ;;
  }

  dimension: os_name {
    type: string
    sql: ${TABLE}.os_name ;;
  }

  dimension: os_version {
    type: string
    sql: ${TABLE}.os_version ;;
  }

  dimension: partner_parameters {
    type: string
    sql: ${TABLE}.partner_parameters ;;
  }

  dimension: product_detail_view_count {
    hidden: yes
    type: number
    sql: ${TABLE}.product_detail_view_count ;;
  }
  dimension: checkout_step1_count {
    hidden: yes
    type: number
    sql: ${TABLE}.checkout_step1_count ;;
  }
  dimension: checkout_step2_count {
    hidden: yes
    type: number
    sql: ${TABLE}.checkout_step2_count ;;
  }
  dimension: login_count {
    hidden: yes
    type: number
    sql: ${TABLE}.login_count ;;
  }


  dimension: product_listing_view_count {
    hidden: yes
    type: number
    sql: ${TABLE}.product_listing_view_count ;;
  }

  dimension: publisher_parameters {
    type: string
    sql: ${TABLE}.publisher_parameters ;;
  }

  dimension: purchase_count {
    hidden: yes
    type: number
    sql: ${TABLE}.purchase_count ;;
  }

  dimension: reattribution_count {
    hidden: yes
    type: number
    sql: ${TABLE}.reattribution_count ;;
  }

  dimension: rejection_reason {
    type: string
    sql: ${TABLE}.rejection_reason ;;
  }

  dimension: reporting_cost {
    hidden: yes
    type: number
    sql: ${TABLE}.reporting_cost ;;
  }

  dimension: reporting_currency {
    type: string
    sql: ${TABLE}.reporting_currency ;;
  }

  dimension: reporting_revenue {
    hidden: yes
    type: number
    sql: ${TABLE}.reporting_revenue ;;
  }

  dimension: revenue_float {
    hidden: yes
    type: number
    sql: ${TABLE}.revenue_float ;;
  }

  dimension: session_count {
    hidden: yes
    type: number
    sql: ${TABLE}.session_count ;;
  }

  dimension: time_spent {
    hidden: yes
    type: number
    sql: ${TABLE}.time_spent ;;
  }

  dimension: tracker {
    type: string
    sql: ${TABLE}.tracker ;;
  }

  dimension: user_agent {
    type: string
    sql: ${TABLE}.user_agent ;;
  }
  measure: revenue_amount_aed {
    label: "Revenue (AED)"
    description: "Revenue in AED"
    type: sum
    sql: ${reporting_revenue} ;;
    filters: [event_name: "purchaseCompleted"]
  }
  measure: revenue_amount_local {
    label: "Revenue (Local)"
    description: "Revenue in local currency"
    type: sum
    sql: ${revenue_float} ;;
    filters: [event_name: "purchaseCompleted"]
  }
  measure: reattribution_count_measure {
    description: "Count of reattribution activity_kind"
    label: "Reattribution Count"
    type: sum
    sql: ${reattribution_count};;
  }
  measure: install_count_measure {
    description: "Count of install activity_kind"
    label: "Install Count"
    type: sum
    sql: ${install_count};;
  }
  measure: purchase_count_measure {
    description: "Count of purchase event_name"
    label: "Purchase Count"
    type: sum
    sql: ${purchase_count};;
  }
  measure: product_list_view_measure {
    description: "Count of plpview event_name"
    label: "Product Listing View Count"
    type: sum
    sql: ${product_listing_view_count};;
  }
  measure: product_detail_view_measure {
    description: "Count of pdpview event_name"
    label: "Product Detail View Count"
    type: sum
    sql: ${product_detail_view_count};;
  }
  measure: checkout_step1_count_measure {
    description: "Count of checkout_step1 event_name"
    label: "Checkout Step 1 Count"
    type: sum
    sql: ${checkout_step1_count};;
  }
  measure: checkout_step2_count_measure {
    description: "Count of checkout_step2 event_name"
    label: "Checkout Step 2 Count"
    type: sum
    sql: ${checkout_step2_count};;
  }
  measure: login_count_measure{
    description: "Count of login_count event_name"
    label: "Login Count"
    type: sum
    sql: ${login_count};;
  }
  measure: add_to_cart_measure {
    description: "Count of addToCart event_name"
    label: "Add to Cart Count"
    type: sum
    sql: ${add_to_cart_count};;
  }
  measure: session_count_measure {
    description: "Count of install/reattribution/session/reattribution_update/install_update activity_kind"
    label: "Session Count"
    type: sum
    sql: ${number_of_sessions};;
  }


  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      creative_name,
      last_tracker_name,
      first_tracker_name,
      network_name,
      event_name,
      adgroup_name,
      campaign_name,
      device_name,
      os_name
    ]
  }
}
