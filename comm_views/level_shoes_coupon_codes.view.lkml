view: level_shoes_coupon_codes {
  sql_table_name: `chb-prod-data-comm.prod_commercial.level_shoes_coupon_codes`
    ;;

  dimension: coupons {
    type: string
    label: "Affiliate Coupons"
    primary_key:yes
    sql: UPPER(${TABLE}.coupons) ;;
  }

  dimension: rules {
    type: string
    label: "Rules"
    sql: ${TABLE}.rules ;;
  }

  dimension: vendors {
    type: string
    label: "Vendors"
    sql: ${TABLE}.vendors ;;
  }

  dimension: coupon_discount {
    type: number
    hidden: yes
    label: "Coupon Discount"
    description: "Integer value of the coupon discount %"
    sql: CAST(REGEXP_EXTRACT(${rules}, r"^([0-9]+)%") AS NUMERIC);;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
