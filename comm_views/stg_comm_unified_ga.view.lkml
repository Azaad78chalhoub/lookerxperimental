include: "../_common/period_over_period.view"

view: unified_ga {
  sql_table_name: `chb-prod-stage-comm.prod_comm.stg_comm_unified_ga`
    ;;

  dimension: bounces {
    type: number
    sql: ${TABLE}.bounces ;;
  }

  dimension: brand_name {
    type: string
    sql: ${TABLE}.brand_name ;;
  }

  dimension: campaign {
    type: string
    sql: ${TABLE}.campaign ;;
  }

  dimension_group: date {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.date ;;
  }

  extends: [period_over_period]
  dimension_group: pop_no_tz {
    sql: ${date_date} ;;
  }

  dimension: device_category {
    type: string
    sql: ${TABLE}.device_category ;;
  }

  dimension: keyword {
    type: string
    sql: ${TABLE}.keyword ;;
  }

  dimension: medium {
    type: string
    sql: ${TABLE}.medium ;;
  }

  dimension: order_count {
    type: number
    sql: ${TABLE}.order_count ;;
  }

  dimension: pageviews {
    type: number
    sql: ${TABLE}.pageviews ;;
  }

  dimension: product_adds_to_cart {
    type: number
    sql: ${TABLE}.product_adds_to_cart ;;
  }

  dimension: product_checkouts {
    type: number
    sql: ${TABLE}.product_checkouts ;;
  }

  dimension: sessions {
    type: number
    sql: ${TABLE}.sessions ;;
  }

  dimension: session_duration {
    type: number
    sql: ${TABLE}.session_duration ;;
  }

  dimension: source {
    type: string
    sql: ${TABLE}.source ;;
  }

  dimension: store_country {
    type: string
    sql: upper(${TABLE}.ecom_site) ;;
  }

  dimension: unique_pageviews {
    type: number
    sql: ${TABLE}.unique_pageviews ;;
  }


  dimension: users {
    type: number
    sql: ${TABLE}.users ;;
  }

  dimension: user_type {
    type: string
    sql: ${TABLE}.user_type ;;
  }

  dimension: pk {
    primary_key: yes
    hidden: yes
    sql: concat(${TABLE}.brand_name, ${TABLE}.ecom_site, ${TABLE}.date, ${TABLE}.device_category, ${TABLE}.campaign, ${TABLE}.keyword, ${TABLE}.source, ${TABLE}.medium) ;;
  }


############################
####      MEASURES      ####
############################

  measure: count {
    type: count
    drill_fields: [brand_name]
  }

  measure: add_to_basket_rate {
    type: number
    label: "Add to Basket %"
    group_label: "GA Metrics"
    description: "Percentage of product detail views that have added the product to their basket over a period of time"
    value_format_name: percent_2
    sql: ${sum_product_adds_to_cart}/NULLIF(${number_of_pageviews},0) ;;
  }

  measure: avg_time_on_site {
    type: number
    group_label: "GA Metrics"
    label: "Avg Time On Site"
    description: "Average of time spend on the site per session"
    value_format: "h:mm:ss"
    sql: ${sum_session_duration}/NULLIF(${session_count},0)/86400 ;;
  }

  measure: bounce_count {
    label: "# of bounces"
    group_label: "GA Metrics"
    type: sum
    description: "Total number of visitors who leave the website after only one page view"
    sql: CASE when ${bounces} is null then 0
          Else ${bounces}
          END;;
  }

  measure: bounce_rate {
    label: "Bounce Rate %"
    group_label: "GA Metrics"
    type: number
    description: "Percentage of visitors who leave the website after only one page view"
    value_format_name: percent_2
    sql: NULLIF(${bounce_count},0)/NULLIF(${session_count},0) ;;
  }

  measure: cart_abandonment_rate {
    type: number
    label: "Cart Abandonment %"
    group_label: "GA Metrics"
    description: "Number of cart created that did not convert into an order (1-% cart abandonment = cart completion rate)"
    value_format_name: percent_2
    sql: (${sum_product_adds_to_cart}-${sum_product_checkouts})/NULLIF(${sum_product_adds_to_cart},0) ;;
  }

  measure: checkout_to_add_rate {
    type: number
    label: "Checkout to Add to Basket %"
    group_label: "GA Metrics"
    description: "Percentage of products that were added to cart and converted into an order"
    value_format_name: percent_2
    sql: ${sum_product_checkouts}/NULLIF(${sum_product_adds_to_cart},0) ;;
  }

  measure: conversion_rate {
    label: "Conversion Rate %"
    group_label: "GA Metrics"
    type: number
    description: "Percentage of sessions that make an order"
    value_format_name: percent_2
    sql: NULLIF(${orders_count},0) / NULLIF(${session_count},0) ;;
  }

  measure: number_of_pageviews {
    type: sum
    label: "# of Page views"
    group_label: "GA Metrics"
    sql: ${TABLE}.pageviews ;;
  }

  measure: orders_count {
    type: sum
    sql: CASE When ${order_count} is null then 0
        Else ${order_count}
        END;;
  }

  measure: pageviews_per_session {
    type: number
    label: "Pageviews per Session"
    group_label: "GA Metrics"
    description: "Average number of pages viewed per session"
    value_format_name: decimal_2
    sql: ${number_of_pageviews}/NULLIF(${session_count},0) ;;
  }

  measure: percent_of_sessions {
    type: percent_of_total
    group_label: "GA Metrics"
    label: "% of Total Sessions"
    value_format_name: decimal_2
    sql: ${session_count} ;;
  }

  measure: session_count{
    label: "# of Sessions"
    group_label: "GA Metrics"
    description: "Total number of sessions on the website"
    type: sum
    sql: CASE WHEN ${sessions} is null then 0
        ELSE ${sessions}
        END;;
  }

  measure: sum_product_adds_to_cart {
    type: sum
    label: "# of Products Added to Cart"
    group_label: "GA Metrics"
    description: "Total number of products added to cart"
    sql: ${TABLE}.product_adds_to_cart ;;
  }

  measure: sum_product_checkouts {
    type: sum
    label: "# of Product Checkouts"
    group_label: "GA Metrics"
    description: "Total number of product that were bought"
    sql: ${TABLE}.product_checkouts ;;
  }

  measure: sum_session_duration {
    type: sum
    label: "Sessions duration"
    group_label: "GA Metrics"
    sql: ${TABLE}.session_duration ;;
  }

  measure: sum_unique_pageviews {
    type: sum
    label: "# of Unique Page views"
    group_label: "GA Metrics"
    description: "Total number of unique pages viewed"
    sql: ${TABLE}.unique_pageviews ;;
  }

  measure: sum_users {
    type: sum
    label: "# of Users"
    group_label: "GA Metrics"
    description: "Total number of users"
    sql: ${TABLE}.users ;;
  }

  ### End of GA Measures
}
