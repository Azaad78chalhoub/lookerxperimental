view: tryano_coupon_code_affiliates {
  sql_table_name: `chb-prod-data-comm.prod_commercial.tryano_coupon_code_affiliates`
    ;;

  dimension: commission_percentage {
    type: number
    value_format_name: percent_0
    sql: (${TABLE}.commission_percentage)/100 ;;
  }

  dimension: pk {
    type: string
    primary_key: yes
    hidden: yes
    sql: concat(${coupon_code}, ${type}, ${vendor}) ;;
  }

  dimension: coupon_code {
    type: string
    sql: ${TABLE}.coupon_code ;;
  }

  dimension: type {
    type: string
    sql: ${TABLE}.type ;;
  }

  dimension: vendor {
    type: string
    sql: ${TABLE}.vendor ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }



}
