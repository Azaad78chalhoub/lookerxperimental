view: tryano_soh {

  sql_table_name: `chb-prod-data-comm.prod_commercial.tryano_soh_weekly`;;

  dimension: sku_soh {
    type: string
    primary_key: yes
    sql: ${TABLE}.sku_soh ;;
  }

  dimension: has_stock_0_or_1 {
    label: "Has stock in UAE? (0=no and 1=yes)"
    description: "Whether stock is available in UAE? 0-no and 1=yes"
    type: string
    sql: ${TABLE}.has_stock_0_or_1 ;;
  }

  measure: soh_ae {
    label: "Stock in UAE"
    type: sum
    sql: ${TABLE}.soh_ae ;;
  }

  measure: soh_sa {
    label: "Stock in KSA"
    type: sum
    sql: ${TABLE}.soh_sa ;;
  }

  measure: soh_kw {
    label: "Stock in KWT"
    type: sum
    sql: ${TABLE}.soh_kw ;;
  }

  measure: soh_bh {
    label: "Stock in BAH"
    type: sum
    sql: ${TABLE}.soh_bh ;;
  }

  measure: soh_qa {
    label: "Stock in QAT"
    type: sum
    sql: ${TABLE}.soh_qa ;;
  }

  measure: soh_in {
    label: "Stock in INT"
    type: sum
    sql: ${TABLE}.soh_in ;;
  }

}
