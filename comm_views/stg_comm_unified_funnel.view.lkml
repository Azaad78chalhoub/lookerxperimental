include: "../_common/period_over_period.view"
include: "../comm_views/overall_group_sales.view"
include: "../customer_views/ecomm_customer_new_returning.view"
include: "../comm_views/stg_comm_unified_ga.view"

view: stg_comm_unified_funnel {
  sql_table_name: `chb-prod-stage-comm.prod_comm.stg_comm_unified_funnel`
    ;;

  dimension: brand_name {
    type: string
    sql: ${TABLE}.brand_name ;;
  }

  dimension: campaign {
    type: string
    sql: ${TABLE}.campaign ;;
  }

  dimension: common_clicks {
    type: number
    sql: ${TABLE}.common_clicks ;;
  }

  dimension: common_cost {
    type: number
    sql: ${TABLE}.common_cost ;;
  }

  dimension: common_cost_usd {
    type: number
    sql: ${TABLE}.common_cost_usd ;;
  }

  dimension: common_impressions {
    type: number
    sql: ${TABLE}.common_impressions ;;
  }

  dimension: country {
    type: string
    sql: ${TABLE}.country ;;
  }

  dimension: currency {
    type: string
    sql: ${TABLE}.currency ;;
  }

  dimension_group: date {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.date ;;
  }

  dimension: media_type {
    type: string
    sql: ${TABLE}.media_type ;;
  }

  dimension: source_id {
    type: string
    sql: ${TABLE}.sourceId ;;
  }

  dimension: source_type {
    type: string
    sql: ${TABLE}.sourceType ;;
  }

  dimension: traffic_source {
    type: string
    sql: ${TABLE}.traffic_source ;;
  }

  measure: count {
    hidden: yes
    type: count
    drill_fields: [brand_name]
  }

  dimension: pk {
    type:  string
    primary_key:  yes
    hidden: yes
    sql:  concat(${brand_name},${campaign},${media_type},${date_date},${traffic_source},${currency},${source_type},${source_id}) ;;
  }

###################################
####   Funnel - Measures       ####
###################################

  measure: total_ad_spend_usd {
    type: sum
    group_label: "Funnel Metrics"
    label: "Total Ad Spend (USD)"
    value_format: "$#,##0.00"
    sql: ${common_cost_usd} ;;
  }

  measure: cost_per_order {
    type: number
    group_label: "Funnel Metrics"
    label: "CPO"
    description: "Cost Per Order"
    value_format: "$#,##0.00"
    sql: ${total_ad_spend_usd}/NULLIF(${group_level_report.all_orders}, 0) ;;
  }

  measure: total_clicks {
    type: sum
    group_label: "Funnel Metrics"
    label: "# Clicks"
    sql: ${common_clicks} ;;
    }

  measure: total_impressions {
    type: sum
    group_label: "Funnel Metrics"
    label: "# Impressions"
    sql:  ${common_impressions};;
    }

  measure: cost_per_click {
    type: number
    group_label: "Funnel Metrics"
    label: "CPC"
    description: "Cost per click"
    value_format_name: usd
    sql: ${total_ad_spend_usd} / NULLIF(${total_clicks},0) ;;
  }

  measure: click_through_rate {
    type: number
    group_label: "Funnel Metrics"
    label: "Click Through Rate %"
    description: "% of impressions that led to clicks"
    value_format_name: percent_2
    sql: NULLIF(${total_clicks},0) / NULLIF(${total_impressions},0);;
  }

  measure: cost_per_mill {
    type: number
    group_label: "Funnel Metrics"
    label: "CPM"
    description: "Cost per Thousand Impressions"
    value_format_name: usd
    sql: NULLIF(NULLIF(${total_ad_spend_usd},0) / NULLIF(${total_impressions}/1000,0),0) ;;
  }

  measure: cost_of_acquisition{
    type: number
    value_format_name: usd
    group_label: "Funnel Metrics"
    label: "Cost of Acquisition"
    sql: NULLIF(NULLIF(${total_ad_spend_usd},0) / NULLIF(${ecomm_customer_new_returning.new_cust_count},0),0) ;;
  }

  measure: roas_gross_rev {
    type: number
    group_label: "Funnel Metrics"
    label: "ROAS (on Gross Rev)"
    value_format: "#,##0.00"
    sql: ${group_level_report.gross_revenue_usd_}/nullif(${total_ad_spend_usd}, 0) ;;
  }

  measure: roi_gross_rev {
    type: number
    group_label: "Funnel Metrics"
    label: "ROI (on Gross Rev)"
    value_format: "#,##0.00"
    sql: (${group_level_report.gross_revenue_usd_}-${total_ad_spend_usd})/nullif(${total_ad_spend_usd}, 0) ;;
  }

  ### End of Funnel Measures
}
