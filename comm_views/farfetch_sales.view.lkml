view: farfetch_sales {
  sql_table_name: `chb-prod-data-comm.prod_commercial.farfetch_sales`
    ;;

  dimension: vertical_security{
    type: string
    hidden: yes
    sql: "FASHION" ;;
  }

  dimension: country_code{
    type: string
    hidden: yes
    sql: "UAE" ;;
  }

  dimension: aed_conversion_rate {
    type: number
    sql: ${TABLE}.aed_conversion_rate ;;
  }

  dimension: cancelled_order_value {
    type: number
    hidden: yes
    sql: ${TABLE}.cancelled_order_value ;;
  }
  measure: cancelled_ov {
    label: "Cancelled Order Value"
    type: sum
    sql: ${cancelled_order_value} ;;
  }

  dimension: cancelled_order_value_aed {
    type: number
    hidden: yes
    sql: ${TABLE}.cancelled_order_value_aed ;;
  }
  measure: cancelled_ov_aed {
    label: "Cancelled Order Value (AED)"
    type: sum
    sql: ${cancelled_order_value_aed} ;;
  }

  dimension: cancelled_order_value_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.cancelled_order_value_usd ;;
  }

  measure: cancelled_ov_usd {
    label: "Cancelled Order Value (USD)"
    type: sum
    sql: ${cancelled_order_value_usd} ;;
  }

  dimension: customer_city {
    type: string
    sql: ${TABLE}.customer_city ;;
  }

  dimension: customer_country {
    type: string
    sql: ${TABLE}.customer_country ;;
  }

  dimension: customer_first_name {
    type: string
    sql: ${TABLE}.customer_first_name ;;
  }

  dimension: customer_last_name {
    type: string
    sql: ${TABLE}.customer_last_name ;;
  }

  dimension: customer_order_id {
    type: string
    sql: ${TABLE}.customer_order_id ;;
  }

  dimension: discount_amount {
    type: number
    hidden: yes
    sql: ${TABLE}.discount_amount ;;
  }
  measure: disc_amt {
    label: "Discount Amount"
    type: sum
    sql: ${discount_amount} ;;
  }

  dimension: fulfillment_store {
    type: string
    sql: ${TABLE}.fulfillment_store ;;
  }

  dimension: gross_order_value {
    type: number
    hidden: yes
    sql: ${TABLE}.gross_order_value ;;
  }
  measure: gross_ov {
    label: "Gross Order Value"
    type: sum
    sql: ${gross_order_value} ;;
  }

  dimension: gross_order_value_aed {
    type: number
    hidden: yes
    sql: ${TABLE}.gross_order_value_aed ;;
  }
  measure: gross_ov_aed {
    label: "Gross Order Value (AED)"
    type: sum
    sql: ${gross_order_value_aed} ;;
  }

  dimension: gross_order_value_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.gross_order_value_usd ;;
  }
  measure: gross_ov_usd {
    label: "Gross Order Value (USD)"
    type: sum
    sql: ${gross_order_value_usd} ;;
  }

  dimension: item {
    type: string
    sql: ${TABLE}.item ;;
  }

  dimension: item_description {
    type: string
    sql: ${TABLE}.item_description ;;
  }

  dimension_group: order {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.order_date ;;
  }

  dimension: original_currency {
    type: string
    sql: ${TABLE}.original_currency ;;
  }

  dimension: product_id {
    type: string
    sql: ${TABLE}.product_id ;;
  }

  dimension: promotion_id {
    type: string
    sql: ${TABLE}.promotion_id ;;
  }

  dimension: quantity {
    type: number
    hidden: yes
    sql: ${TABLE}.quantity ;;
  }
  measure: qty {
    label: "Quantity"
    type: sum
    sql: ${quantity} ;;
  }

  dimension: quantity_sign {
    type: string
    sql: ${TABLE}.quantity_sign ;;
  }

  dimension: return_order_value {
    type: number
    hidden: yes
    sql: ${TABLE}.return_order_value ;;
  }
  measure: return_ov {
    label: "Return Order Value"
    type: sum
    sql: ${return_order_value} ;;
  }

  dimension: return_order_value_aed {
    type: number
    hidden: yes
    sql: ${TABLE}.return_order_value_aed ;;
  }
  measure: return_ov_aed {
    label: "Return Order Value (AED)"
    type: sum
    sql: ${return_order_value_aed} ;;
  }

  dimension: return_order_value_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.return_order_value_usd ;;
  }
  measure: return_ov_usd {
    label: "Return Order Value (USD)"
    type: sum
    sql: ${return_order_value_usd} ;;
  }

  dimension: shipped_order_value {
    type: number
    hidden: yes
    sql: ${TABLE}.shipped_order_value ;;
  }
  measure: shipped_ov {
    label: "Shipped Order Value"
    type: sum
    sql: ${shipped_order_value} ;;
  }

  dimension: shipped_order_value_aed {
    type: number
    hidden: yes
    sql: ${TABLE}.shipped_order_value_aed ;;
  }
  measure: shipped_ov_aed {
    label: "Shipped Order Value (AED)"
    type: sum
    sql: ${shipped_order_value_aed} ;;
  }

  dimension: shipped_order_value_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.shipped_order_value_usd ;;
  }
  measure: shipped_ov_usd {
    label: "Shipped Order Value (USD)"
    type: sum
    sql: ${shipped_order_value_usd} ;;
  }

  dimension: tender_type_amount {
    type: number
    hidden: yes
    sql: ${TABLE}.tender_type_amount ;;
  }
  measure: amt_tender_type {
    label: "Tender Type Amount"
    type: sum
    sql: ${tender_type_amount} ;;
  }

  dimension: tender_type_group {
    type: string
    sql: ${TABLE}.tender_type_group ;;
  }

  dimension: tender_type_id {
    type: string
    sql: CAST(${TABLE}.tender_type_id AS STRING) ;;
  }

  dimension: transaction_number {
    type: string
    sql: ${TABLE}.transaction_number ;;
  }

  dimension_group: transaction {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.transaction_time ;;
  }

  dimension: transaction_type {
    type: string
    sql: ${TABLE}.transaction_type ;;
  }

  dimension: unit_retail {
    type: number
    hidden: yes
    sql: ${TABLE}.unit_retail ;;
  }
  measure: retail_unit {
    label: "Unit Retail"
    type: sum
    sql: ${unit_retail} ;;
  }

  dimension: usd_conversion_rate {
    type: number
    sql: ${TABLE}.usd_conversion_rate ;;
  }
  dimension: vat_amount {
    type: number
    hidden: yes
    sql: ${TABLE}.vat_amount ;;
  }
  measure: vat_amt {
    label: "VAT Amount"
    type: sum
    sql: ${vat_amount} ;;
  }
}
