include: "../_common/period_over_period.view"
include: "../customer_views/ecomm_customer_new_returning.view"
include: "./lvl_catalog_flat_1.view"

view: lvl_facts_union {

  sql_table_name: `chb-prod-data-comm.prod_commercial.lvl_facts_union`
    ;;


  dimension: brand_security{
    type: string
    hidden: yes
    sql: "LEVEL SHOES" ;;
  }

  dimension: vertical_security{
    type: string
    hidden: yes
    sql: "MANAGED COMPANIES" ;;
  }

  extends: [period_over_period]

  parameter: date_type {
    view_label: "-- Period over Period"
    description: "Choose the type of Date you would like to use for the comparison. Default: Date Date"
    label: "Date Type:"
    type: unquoted
    allowed_value: {
      label: "Date Date"
      value: "date_date"
    }
    allowed_value: {
      label: "Order Date"
      value: "order_date"
    }
    default_value: "date_date"
  }

  dimension_group: pop_no_tz {
    sql:{% if date_type._parameter_value == "date_date" %}
            ${common_raw}
        {% elsif date_type._parameter_value == "order_date" %}
            ${sales_order_datetime_raw}
        {% endif %} ;;
  }

  dimension: source {
    type: string
    label: "Data Source"
    sql: ${TABLE}.source ;;
  }

  dimension: primary_key {
    type: number
    hidden: yes
    primary_key: yes
    sql: ${TABLE}.primary_key ;;
  }


  dimension_group: common {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    label: "Date"
    sql: ${TABLE}.common_date ;;
  }

  dimension: business_week_no{
    type: number
    description: "Week No. extracted from the business date"
    sql: EXTRACT( WEEK FROM(PARSE_DATE('%Y-%m-%d', CAST(${TABLE}.common_date AS STRING))))+1 ;;
  }
  dimension: month_name{
    type: string
    label: "Month Name"
    group_label: "Sales Order"
    description: "MM"
    sql: format_date("%b", ${TABLE}.common_date);;
  }
  # dimension_group: common {
  #   type: time
  #   timeframes: [
  #     raw,
  #     date,
  #     week,
  #     time,
  #     month,
  #     quarter,
  #     year
  #   ]
  #   convert_tz: no
  #   datatype: timestamp
  #   label: "Date"
  #   description: "Date when a specific even (Record Type) took place"
  #   sql: cast(${TABLE}.record_datetime as timestamp) ;;
  # }

  dimension: date_group {
    label: "Date Group"
    hidden: yes
    sql: {% if lvl_facts_union.common_date._in_query %} CAST(${TABLE}.common_date  AS DATE)
      {% elsif lvl_facts_union.common_month._in_query %} FORMAT_DATE('%Y-%m', ${TABLE}.common_date )
      {% elsif lvl_facts_union.common_quarter._in_query %} (FORMAT_TIMESTAMP('%Y-%m', TIMESTAMP_TRUNC(CAST(${TABLE}.common_date AS TIMESTAMP), QUARTER)))
      {% elsif lvl_facts_union.common_week._in_query %} CONCAT(CAST(EXTRACT(YEAR FROM ${TABLE}.common_date ) AS STRING),'-', CAST(EXTRACT(WEEK(SUNDAY) FROM ${TABLE}.common_date) AS STRING))
      {% elsif lvl_facts_union.common_year._in_query %} EXTRACT(YEAR FROM ${TABLE}.common_date )
      {% else %} CAST(${TABLE}.common_date  AS DATE)
      {% endif %} ;;
  }

  dimension: measure_label {

    label: "Measure Name"

    description: "Dummy dimensions shwoing measure label (used to transpose table)"

    case: {

      when: {

        label: "# of Sessions"

        sql: 1=1;;

      }

      when: {

        label: "# of Sales Orders"

        sql: 1=1;;

      }

      when: {

        label: "Conversion Rate (%)"

        sql: 1=1;;

      }

      when: {

        label: "Average Order Value (USD)"

        sql: 1=1;;

      }

      when: {

        label: "UPT"

        sql: 1=1;;

      }

      when: {

        label: "USP"

        sql: 1=1;;

      }

      when: {

        label: "Gross Revenue (USD)"

        sql: 1=1;;

      }

      when: {

        label: "Cancelled Rate (% value)"

        sql: 1=1;;

      }

      when: {

        label: "Returned Rate (% Shipped value)"

        sql: 1=1;;

      }

      when: {

        label: "ROAS (on Gross Rev)"

        sql: 1=1;;

      }

      when: {

        label: "Net Sales (USD)"

        sql: 1=1;;

      }

    }

  }

###################################
##     Sales Order Dimensions    ##
###################################{

  dimension: order_id {
    type: string
    label: "Order ID"
    group_label: "Sales Order"
    sql: ${TABLE}.order_id ;;
    link: {
      label: "View Order in OMS"
      url: "https://lvs.retailunity.com/cpanel/cpanel_includes/inc_datatables/orders/orders_all.php7?action=edit&id={{lvl_facts_union.entity_id._value}}"
    }
  }

  dimension: sales_channel {
    type: string
    label: "Channel"
    description: "Web or App"
    sql:  ${TABLE}.sales_channel;;
  }

  dimension: app_channel {
    type: string
    label: "App Channel"
    description: "Android or iOS"
    sql:  case left(${order_id}, 3)
            when 'APP' then 'iOS'
            when 'AND' then 'Android'
            else 'Web'
          end;;
  }

  dimension: language {
    type: string
    label: "Language"
    group_label: "Sales Order"
    sql:  case left(${order_id},5)
            when 'APPOR' then 'Arabic'
            when 'APPEN' then 'English'
            else null
          end;;
  }

  dimension_group: sales_order_datetime {
    type: time
    label: "Order"
    group_label: "Sales Order"
    hidden: no
    timeframes: [
      raw,
      time,
      date
    ]
    sql: CAST(${TABLE}.order_datetime AS TIMESTAMP) ;;
  }

  dimension: country {
    type: string
    label: "Country"
    group_label: "Sales Order"
    #map_layer_: countries
    sql: ${TABLE}.store_country ;;
  }

  dimension: order_currency {
    type: string
    label: "Order Currency"
    group_label: "Sales Order"
    sql: ${TABLE}.order_currency ;;
  }

  dimension: order_custom_duty_fee_aed {
    type: number
    hidden: no
    label: "Order Custom Duty Fee (Local)"
    description: "In AED; only available for OMS data"
    group_label: "Sales Order"
    sql: ${TABLE}.order_custom_duty_fee ;;
  }


  dimension: order_payment_fee_aed {
    type: number
    label: "Order Payment Fee (Local)"
    description: "In AED"
    group_label: "Sales Order"
    sql: ${TABLE}.order_payment_fee ;;
  }

  dimension: order_shipping_aed {
    type: number
    label: "Order Shipping Fee (AED)"
    description: "In AED"
    group_label: "Sales Order"
    sql: ${TABLE}.order_shipping ;;
  }

  dimension: order_total_aed {
    type: number
    hidden: no
    label: "Order Total (AED)"
    description: "Actual total paid by customer in AED including all fees"
    group_label: "Sales Order"
    sql: ${TABLE}.order_total_aed ;;
  }

  dimension: order_total_usd {
    type: number
    hidden: yes
    label: "Order Total (USD)"
    description: "Actual total paid by customer in USD including all fees"
    group_label: "Sales Order"
    sql: ${TABLE}.order_total*${TABLE}.currency_rate_usd ;;
  }

  dimension: payment_method {
    type: string
    label: "Payment Method"
    group_label: "Sales Order"
    sql: ${TABLE}.payment_method ;;
  }

  dimension: magento_payment_method {
    type: string
    label: "Magento Payment Method"
    description: "Payment Method value in Magento; to identify Tabby orders, Chechout.com, etc."
    group_label: "Sales Order"
    sql: ${TABLE}.magento_payment_method ;;
  }
  dimension: payment_status {
    type: string
    label: "Payment Status"
    group_label: "Sales Order"
    sql: ${TABLE}.payment_status ;;
  }

  dimension: customer_email {
    type: string
    group_label: "Sales Order"
    hidden: yes
    sql: ${TABLE}.customer_email ;;
  }
  dimension: customer_phonenumber {
    type: string
    group_label: "Sales Order"
    hidden: yes
    sql: ${TABLE}.customer_phonenumber ;;
  }

  dimension: xstore_invoice_number {
    type: string
    group_label: "Sales Order"
    sql: ${TABLE}.xstore_invoice_number ;;
  }

  dimension: sales_origin_internal_id {
    type: string
    group_label: "Sales Order"
    label: "Sales Origin Store ID"
    sql: ${TABLE}.sales_origin_internal_id ;;
  }

  dimension: giftwrapping {
    type: string
    label: "Gift Wrapping (Y/N)"
    description: "Shows Y (Yes) if a Gift Wrapping service item was part of the order, else N (No)"
    group_label: "Sales Order"
    sql: ${TABLE}.giftwrapping ;;
  }

## End of Sales Order Dimensions ##}

###################################
##  Sales Order Item Dimensions  ##
###################################{

  dimension: record_type {
    type: string
    label: "Record Type"
    description: "For sales related types only. Use Item Status for other statuses. "
    group_label: "Sales Order Item"
    sql: ${TABLE}.record_type ;;
  }


  dimension: detail_id {
    type: string
    label: "Detail ID"
    description: "ID of each individual item sold"
    group_label: "Sales Order Item"
    sql: ${TABLE}.detail_id ;;
  }

  #dimension: SKU1{
  # type: string
  #sql:IFNULL( ${sku_order},${lvl_catalog_product_flat_1.sku}) ;;
  #}

  dimension: detail_brand {
    type: string
    label:"Source Brand"
    description: "Item Brand from OMS"
    group_label: "Sales Order Item"
    sql: ${TABLE}.detail_brand ;;
  }

  dimension: detail_sku_code {
    type: string
    label:"Source VPN"
    description: "Item VPN from OMS"
    group_label: "Sales Order Item"
    sql: ${TABLE}.detail_sku_code ;;
  }

  # dimension: Product_Type {
  #   type: string
  #   sql: case when
  #         ${Concession_Brands} = 'MultiBrand' then "Own Bought"
  #         else "Concession"
  #         end ;;
  # }

  # dimension: Concession_Brands{
  #   hidden: yes
  #   type:  string
  #   sql:Case
  #     when  (${detail_brand} = 'SAINT LAURENT') then "Saint Laurent"
  #     when  (${detail_brand} = 'BALENCIAGA') then " Balenciaga"
  #     when  (${detail_brand} = 'MIU MIU') then " Miu Miu"
  #     when  (${detail_brand} = 'PRADA') then " Prada"
  #     When  (${detail_brand} = 'DOLCE & GABBANA') then " Dolce & Gabbana"
  #     when  (${detail_brand} = 'BOTTEGA VENETA') then "Bottega Vaneta"
  #     when  (${detail_brand} = 'TORY BURCH') then " Tory Burch"
  #     when  (${detail_brand} = 'JIMMY CHOO' and ${lvl_catalog_product_flat_1.gender_value} = 'WOMEN') then " Jimmy Choo"
  #     when  (${detail_brand} = 'GUCCI') then " Gucci"
  #     when  (${detail_brand} = 'SALVATORE FERRAGAMO') then "Salvatore Ferragamo"
  #     when  (${detail_brand} = 'MARGARET DABBS LONDON') then "Margret Dabbs"
  #     when  (${detail_brand} = 'MICHAEL KORS' and ${lvl_catalog_product_flat_1.gender_value} ='WOMEN') then "Michael Kors"
  #     when  (${detail_brand} = 'VALENTINO GARAVANI') then "Valentino Garavani"
  #     when  (${detail_brand} = 'SALVATORE FERRAGAMO') then  " Salvatore Ferragamo"
  #     when  (${detail_brand} = 'MANOLO BLAHNIK') then "Manolo Blahnik"
  #     when  (${detail_brand} = "TOD'S") then " Tod's"
  #     when  (${detail_brand} = 'PRIVATE COLLECTION') then "Private Collection"
  #     when  (${detail_brand} = 'MICHEAL KORS') then " Micheal kors"
  #     when  (${detail_brand} = 'CHRISTIAN LOUBOUTIN') then "Christian Louboutin"
  #     when  (${detail_brand} = 'FALKE') then " Falke"
  #     when  (${detail_brand} = 'BERLUTI') then "Berluti"
  #     when  (${detail_brand} = 'GIVENCHY' and ${lvl_catalog_product_flat_1.gender_value} IN ('MEN','WOMEN')) then "GIVENCHY"
  #     else "MultiBrand"
  #     end
  #     ;;
  # }


  dimension: sku_order {
    type: string
    hidden: yes
    label: "SKU PIM lookup value"
    group_label: "Sales Order Item"
    sql: ${TABLE}.sku ;;
  }

  dimension: source_barcode {
    type: string
    label: "Source Barcode"
    group_label: "Sales Order Item"
    sql: ${TABLE}.barcode ;;
  }

  dimension: source_sku {
    type: string
    label: "Source SKU"
    group_label: "Sales Order Item"
    sql: ${TABLE}.detail_external_id ;;
  }

  dimension_group: trigger {
    type: time
    hidden: no
    label: "Action Trigger "
    group_label: "Sales Order Item"
    description: "Displays the time of each detail status for each item"
    timeframes: [
      raw,
      time,
      hour_of_day
    ]
    sql: CAST(${TABLE}.record_datetime AS TIMESTAMP)  ;;
  }





  dimension: price_original {
    type: number
    hidden: yes
    label: "Item Price Original"
    description: "Local with tax and before discount in original currency"
    group_label: "Sales Order Item"
    sql: ${TABLE}.detail_price_original ;;
  }

  dimension: price_original_aed {
    type: number
    hidden: no
    label: "Item Price Original (AED)"
    description: "Local with tax and before discount in AED"
    group_label: "Sales Order Item"
    sql: ${TABLE}.detail_price_original_aed ;;
  }

  dimension: price {
    type: number
    hidden: yes
    label: "Item Price Actual"
    description: "Local without tax after discount in original currency"
    group_label: "Sales Order Item"
    sql: ${TABLE}.price ;;
  }

  dimension: price_aed {
    type: number
    label: "Item Price Actual (AED)"
    description: "Without tax after discount in AED"
    group_label: "Sales Order Item"
    sql: ${TABLE}.price_aed ;;
  }

  dimension: price_usd {
    type: number
    label: "Item Price Actual (USD)"
    description: "Without tax after discount in USD"
    value_format: "$#,##0.00"
    group_label: "Sales Order Item"
    sql: ${TABLE}.price_usd ;;
  }

  dimension: detail_status {
    type: string
    label: "Item Status"
    group_label: "Sales Order Item"
    sql: ${TABLE}.status ;;
  }


  dimension: tax_rate {
    type: number
    label: "Item Tax Rate"
    description: "VAT rate for OMS data; both VAT and Custom Duty rate for Magento data"
    group_label: "Sales Order Item"
    sql: ${TABLE}.detail_tax;;
  }

  dimension: tax_amount {
    type: number
    hidden: yes
    label: "Item Tax Amount"
    description: "Local tax of price after discount in original currency"
    group_label: "Sales Order Item"
    sql: ${TABLE}.detail_tax_amount ;;
  }

  dimension: tax_amount_aed {
    type: number
    hidden: no
    label: "Item Tax Amount"
    description: "Local tax of price after discount in AED"
    group_label: "Sales Order Item"
    sql: ${TABLE}.detail_tax_amount_aed ;;
  }

  dimension: discount_amount {
    type: number
    hidden: yes
    label: "Item Discount Amount"
    description: "Local including tax in original currency"
    group_label: "Sales Order Item"
    sql: ${TABLE}.discount_amount ;;
  }

  dimension: discount_amount_aed {
    type: number
    hidden: no
    label: "Item Discount Amount (AED)"
    description: "Local including tax in AED"
    group_label: "Sales Order Item"
    sql: ${TABLE}.discount_amount_aed ;;
  }

  dimension: magento_discount_invoiced {
    type: number
    hidden: yes
    label: "Magento Discount Invoiced"
    description: "Discount invoiced from Magento Coupon Code"
    group_label: "Sales Order Item"
    sql: ${TABLE}.magento_discount_invoiced ;;
  }

  dimension: magento_discount_refunded {
    type: number
    hidden: yes
    label: "Magento Discount Refunded"
    description: "Discount Refunded from Magento Coupon Code"
    group_label: "Sales Order Item"
    sql: ${TABLE}.magento_discount_refunded ;;
  }


  dimension: coupon_code {
    type: string
    label: "Coupon Code"
    hidden: yes
    group_label: "Sales Order Item"
    sql: UPPER(${TABLE}.coupon_code) ;;
  }

  dimension: voucher_code {
    type: string
    label: "E-gift card"
    hidden: no
    group_label: "Sales Order Item"
    sql: ${TABLE}.voucher_code ;;
  }

  dimension: magento_coupon_code {
    type: string
    label: "Magento Coupon Code"
    hidden: yes
    group_label: "Sales Order Item"
    sql: UPPER(${TABLE}.magento_coupon_code) ;;
  }
  dimension: coupon_code_fixed {
    type: string
    label: "Coupon Code Fixed"
    hidden: no
    group_label: "Sales Order Item"
    sql: coalesce(UPPER(${coupon_code}),UPPER(${magento_coupon_code})) ;;
  }
  dimension: magento_coupon_rule_name {
    type: string
    label: "Magento Coupon Rule Name"
    hidden: no
    group_label: "Sales Order Item"
    sql: ${TABLE}.magento_coupon_rule_name ;;
  }

  dimension: magento_discount_description {
    type: string
    label: "Magento Discount Description"
    hidden: no
    group_label: "Sales Order Item"
    sql: ${TABLE}.magento_discount_description ;;
  }


  dimension: employee_id {
    type: string
    label: "Employee ID"
    description: "Employee handling the status update"
    hidden: no
    group_label: "Sales Order Item"
    sql: ${TABLE}.employee_id ;;
  }


  dimension: unit_cost {
    type: number
    hidden: yes
    value_format: "#,##0.00"
    group_label: "Sales Order"
    #hidden: yes
    sql: ${TABLE}.unit_cost ;;
  }

  dimension: unit_cost_usd {
    type: number
    value_format: "$#,##0.00"
    label: "Unit cost (USD)"
    group_label: "Sales Order"
    #hidden: yes
    sql: ${TABLE}.unit_cost_usd ;;
  }

  dimension: unit_cost_aed {
    type: number
    value_format: "#,##0.00"
    label: "Unit cost (AED)"
    group_label: "Sales Order"
    #hidden: yes
    sql: ${TABLE}.unit_cost_usd/${currency_rate_usd}*${currency_rate_aed} ;;
  }

  dimension: org_num {
    type: number
    hidden: yes
    sql: ${TABLE}.org_num ;;
  }

  dimension: return_cancel_reason {
    type: string
    label: "Customer Item Return/Cancel Reason"
    group_label: "Sales Order Item"
    sql: ${TABLE}.return_cancel_reason ;;
  }

  dimension: cancellation_reason {
    type: string
    label: "Internal Item Cancel Reason"
    group_label: "Sales Order Item"
    #sql: case when nullif(trim(replace(${TABLE}.cancellation_reason, '-', '')), '') is null then 'Unknown' else ${TABLE}.cancellation_reason end  ;;
    sql: ${TABLE}.cancellation_reason ;;
  }

  dimension: fulfilment_employee_id {
    type: string
    label: "fulfilment_employee_id"
    group_label: "Sales Order Item"
    sql: ${TABLE}.fulfilment_employee_id ;;
  }


  dimension: final_record_status {
    type: string
    label: "Last Item Status"
    description: "Shows the last known status of the item on this order"
    group_label: "Sales Order Item"
    sql: ${TABLE}.final_record_status ;;
  }


  dimension_group: final_record_date {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    label: "Last Item Status Date"
    description: "Shows the last date of the known status of the item on this order"
    group_label: "Sales Order Item"
    sql: ${TABLE}.final_status_datetime ;;
  }

  dimension: return_location_internal_id {
    type: string
    label: "Returned to Shop/WH ID"
    hidden: no
    group_label: "Sales Order Item"
    sql: ${TABLE}.return_location_internal_id ;;
  }

  dimension: return_location_name {
    type: string
    label: "Returned to Shop/WH Name"
    hidden: no
    group_label: "Sales Order Item"
    sql: ${TABLE}.return_location_name ;;
  }

  dimension: cc_location_internal_id {
    type: string
    label: "Click & Collect Shop/WH ID"
    hidden: no
    group_label: "Sales Order Item"
    sql: ${TABLE}.cc_location_internal_id ;;
  }

  dimension: cc_location_name {
    type: string
    label: "Click & Collect Shop/WH Name"
    hidden: no
    group_label: "Sales Order Item"
    sql: ${TABLE}.cc_location_name ;;
  }


#End of Sales Order Item Dimensions#}

#######################################
### Sales Order Shipment dimensions ###
#######################################{

  dimension: shipment_country {
    type: string
    label: "Shipment Country"
    hidden: no
    group_label: "Sales Order Shipment"
    sql: ${TABLE}.delivery_country ;;
  }

  dimension: shipment_city {
    type: string
    label: "Shipment City"
    hidden: no
    group_label: "Sales Order Shipment"
    sql: ${TABLE}.delivery_city ;;
  }

  dimension: shipment_type {
    type: string
    label: "Shipment Type"
    group_label: "Sales Order Shipment"
    sql: ${TABLE}.delivery_type ;;
  }

  dimension: parcel_id {
    type: string
    label: "Parcel ID"
    group_label: "Sales Order Shipment"
    sql: ${TABLE}.parcel_id ;;
  }

  dimension: tracking_id {
    type: string
    label: "Shipment Tracking ID"
    group_label: "Sales Order Shipment"
    sql: ${TABLE}.tracking_id ;;
  }

  dimension: shipping_location_id {
    type: string
    hidden: yes
    group_label: "Sales Order Shipment"
    sql: ${TABLE}.shipping_location_id ;;
  }

  dimension: shipping_location_internal_id {
    type: string
    hidden: no
    group_label: "Sales Order Shipment"
    label: "Shipment Store/WH ID"
    sql: ${TABLE}.shipping_location_internal_id ;;
  }

  dimension: shipping_location_name {
    type: string
    hidden: no
    group_label: "Sales Order Shipment"
    label: "Shipment Store/WH name"
    sql: ${TABLE}.shipping_location_name ;;
  }

  dimension: shipping_original_amount {
    type: number
    group_label: "Sales Order Shipment"
    description: "Shipping fees in order currency, before discount"
    sql: ${TABLE}.shipping_original_amount ;;
  }

  dimension: shipping_discount_amount {
    type: number
    group_label: "Sales Order Shipment"
    description: "Discount amount applied on shipping fees in order currency"
    sql: ${TABLE}.shipping_discount_amount ;;
  }



  ### End of Sales Order Shipment dimensions  #}

###################################
##     Sales Order Measures      ##
###################################{


##################### SALE #####################


  measure: count_sales_orders {
    type: count_distinct
    group_label: "Sales Metrics"
    label: "# of Sales Orders"
    description: "Total number of orders placed on the website"
    sql: ${order_id};;
    filters: [record_type: "SALE"]
  }

  measure: units_ordered {
    type: sum
    group_label: "Sales Metrics"
    label: "Units Ordered"
    description: "Total number of items ordered on the website"
    sql: ${qty_ordered};;
  }

  measure: aov_usd {
    type: number
    group_label: "Sales Metrics"
    label: "Average Order Value (USD)"
    description: "Average Value of each order in USD"
    value_format: "$#,##0.00"
    sql:${gross_rev_usd}/nullif(${count_sales_orders}, 0);;
  }

  measure: aov_local {
    type: number
    group_label: "Sales Metrics"
    label: "Average Order Value (AED)"
    description: "Average Value of each order in local currency"
    value_format: "#,##0.00"
    sql: ${gross_rev_local}/nullif(${count_sales_orders}, 0);;
  }

  measure: aov_original {
    type: number
    group_label: "Sales Metrics"
    label: "Average Order Value (Local)"
    description: "Average Value of each order in local currency"
    value_format: "#,##0.00"
    sql: ${gross_rev_original}/nullif(${count_sales_orders}, 0);;
  }

  measure: gross_net_local {
    type: sum
    group_label: "Sales Metrics"
    label: "Gross Revenue w/o cancellations & returns (AED)"
    description: "In AED"
    value_format: "#,##0.00"
    sql: ${gross_net_sales_local};;
  }

  measure: gross_net_original {
    type: sum
    group_label: "Sales Metrics"
    label: "Gross Revenue w/o cancellations & returns (Local)"
    description: "In Local currency"
    value_format: "#,##0.00"
    sql: ${gross_net_sales_original};;
  }

  measure: gross_net_usd {
    type: sum
    group_label: "Sales Metrics"
    label: "Gross Revenue w/o cancellations & returns (USD)"
    description: "Total value of incoming orders on the website excluding cancellations and returns (excluding failed credit card attempts, VAT, customs and shipping fees and COD fees). To be computed based on order date"
    value_format: "$#,##0.00"
    sql:${gross_net_sales_usd};;
  }

  measure: gross_minus_cancelled_local {
    type: sum
    group_label: "Sales Metrics"
    label: "Gross Revenue w/o cancellations (AED)"
    description: "Total value of incoming orders on the website excluding cancellations (excluding failed credit card attempts, VAT, customs and shipping fees and COD fees). To be computed based on order date"
    value_format: "#,##0.00"
    sql: ${gross_minus_cancel_local};;
  }

  measure: gross_minus_cancelled_original {
    type: sum
    group_label: "Sales Metrics"
    label: "Gross Revenue w/o cancellations (Local)"
    description: "Total value of incoming orders on the website excluding cancellations (excluding failed credit card attempts, VAT, customs and shipping fees and COD fees). To be computed based on order date"
    value_format: "#,##0.00"
    sql: ${gross_minus_cancel_original};;
  }

  measure: gross_minus_cancelled_usd {
    type: sum
    group_label: "Sales Metrics"
    label: "Gross Revenue w/o cancellations (USD)"
    description: "Total value of incoming orders on the website excluding cancellations (excluding failed credit card attempts, VAT, customs and shipping fees and COD fees). To be computed based on order date"
    value_format: "$#,##0.00"
    sql: ${gross_minus_cancel_usd};;
  }

  measure: gross_rev_local {
    type: sum
    group_label: "Sales Metrics"
    label: "Gross Revenue (AED)"
    description: "Total value of incoming orders on the website in AED (excluding VAT, discount, customs and shipping fees and COD fees)"
    value_format: "#,##0.00"
    sql:  ${gross_revenue_amount_local};;
  }

  measure: gross_rev_original {
    type: sum
    group_label: "Sales Metrics"
    label: "Gross Revenue (Local)"
    description: "Total value of incoming orders on the website in Local currency (excluding VAT, discount, customs and shipping fees and COD fees)"
    value_format: "#,##0.00"
    sql:  ${gross_revenue_amount_original};;
  }

  measure: gross_rev_usd {
    type: sum
    group_label: "Sales Metrics"
    label: "Gross Revenue (USD)"
    description: "Total value of incoming orders on the website (excluding VAT, discount, customs and shipping fees and COD fees)"
    value_format: "$#,##0.00"
    sql: ${gross_revenue_amount_usd};;
  }

  measure: gross_rev_tax_local {
    type: sum
    group_label: "Sales Metrics"
    label: "Gross Revenue with Tax (AED)"
    description: "Total value of incoming orders on the website (excluding discount and shipping fees and COD fees)"
    value_format: "#,##0.00"
    sql:  ${detail_price_with_tax_minus_discount_aed};;
    filters: [record_type: "SALE"]
  }

  measure: gross_rev_tax_usd {
    type: sum
    group_label: "Sales Metrics"
    label: "Gross Revenue with Tax (USD)"
    description: "Total value of incoming orders on the website (excluding discount and shipping fees and COD fees)"
    value_format: "$#,##0.00"
    sql:  ${detail_price_with_tax_minus_discount_usd};;
    filters: [record_type: "SALE"]
  }

  measure: gross_rev_all_local {
    type: sum_distinct
    group_label: "Sales Metrics"
    label: "Gross Revenue with Tax and Fee (AED)"
    description: "Including both VAT and Custom Duty and all fee in AED, order level only"
    value_format: "#,##0.00"
    sql_distinct_key: ${order_id} ;;
    sql:  ${order_total_aed};;
    filters: [record_type: "SALE"]
  }

  measure: gross_rev_all_usd {
    type: sum_distinct
    group_label: "Sales Metrics"
    label: "Gross Revenue with Tax and Fee (USD)"
    description: "Including both VAT and Custom Duty and all fee, order level only"
    value_format: "$#,##0.00"
    sql_distinct_key: ${order_id} ;;
    sql:  ${order_total_usd};;
    filters: [record_type: "SALE"]
  }

  measure: upt {
    type: number
    group_label: "Sales Metrics"
    label: "UPT"
    description: "Average number of units within each order"
    value_format_name: decimal_2
    sql:${units_ordered}/nullif(${count_sales_orders}, 0);;
  }

  measure: usp {
    type:  number
    label: "USP (USD)"
    group_label: "Sales Metrics"
    description: "Unit Selling Price = Average price of each item ordered (in USD)"
    value_format: "$#,##0.00"
    sql: ${gross_rev_usd} / NULLIF(${units_ordered},0) ;;
  }

  measure: usp_local {
    type:  number
    label: "USP (AED)"
    group_label: "Sales Metrics"
    description: "Unit Selling Price = Average price of each item ordered (in AED)"
    value_format: "#,##0.00"
    sql: ${gross_rev_local} / NULLIF(${units_ordered},0) ;;
  }

  measure: usp_original {
    type:  number
    label: "USP (Local)"
    group_label: "Sales Metrics"
    description: "Unit Selling Price = Average price of each item ordered (in Local currency)"
    value_format: "#,##0.00"
    sql: ${gross_rev_original} / NULLIF(${units_ordered},0) ;;
  }

  measure: units_ordered_minus_cancelled {
    type: number
    group_label: "Sales Metrics"
    label: "Units Ordered minus Cancelled"
    description: "Total number of ordered items excluding cancelled items"
    sql: ${units_ordered} - ${unit_canceled} ;;
  }

  measure: units_ordered_minus_cancelled_minus_returned{
    type: number
    group_label: "Sales Metrics"
    label: "Units Ordered minus Cancelled & Returns"
    description: "Total number of ordered items excluding cancelled and returned items"
    sql: ${units_ordered} - ${units_returned} - ${unit_canceled} ;;
  }

##################### CANCELLATIONS #####################


  measure: total_cancelled_orders {
    type: count_distinct
    hidden:  yes
    group_label: "Sales Metrics"
    label: "# of Cancelled Orders"
    description: "Total number of orders that are partially or fully cancelled"
    sql: ${order_id};;
    filters: [record_type: "CANCELLED"]
  }

  measure: unit_canceled {
    type: sum
    group_label: "Sales Metrics"
    label: "Units Canceled"
    description: "Total number of canceled items"
    sql: ${qty_canceled};;
  }

  measure: unit_canceled_custom_req {
    type: sum
    group_label: "Sales Metrics"
    label: "Units Canceled - Customer Requeted"
    description: "Total number of canceled items"
    sql: ${qty_canceled};;
    filters: [cancellation_reason: "Customer Request"]
  }

  measure: cancel_rate_item_test {
    type: number
    group_label: "Sales Metrics"
    label: "Cancellation Rate (% item) - Customer Requested"
    description: "Percentage of incoming items quantity that were cancelled out of total incoming items quantity"
    value_format_name: percent_2
    sql: nullif(${unit_canceled_custom_req}/nullif(${units_ordered}, 0),0);;
  }

  measure: cancel_amount_local {
    type: sum
    group_label: "Sales Metrics"
    label: "Cancelled Amount (AED)"
    description: "Total value of incoming orders that are cancelled in AED"
    value_format: "#,##0.00"
    sql: ${cancel_amount};;
  }

  measure: cancel_amount_usd {
    type: sum
    group_label: "Sales Metrics"
    label: "Cancelled Amount (USD)"
    description: "Total value of incoming orders that are cancelled in USD"
    value_format: "$#,##0.00"
    sql: ${cancel_usd};;
  }

  measure: cancel_amount_original {
    type: sum
    group_label: "Sales Metrics"
    label: "Cancelled Amount (Local)"
    description: "Total value of incoming orders that are cancelled in Local currency"
    value_format: "#,##0.00"
    sql: ${cancel_original};;
  }

  measure: cancel_rate_item {
    type: number
    group_label: "Sales Metrics"
    label: "Cancellation Rate (% item)"
    description: "Percentage of incoming items quantity that were cancelled out of total incoming items quantity"
    value_format_name: percent_2
    sql: nullif(${unit_canceled}/nullif(${units_ordered}, 0),0);;
  }

  measure: cancel_rate_value {
    type: number
    group_label: "Sales Metrics"
    label: "Cancellation Rate (% value)"
    description: "Percentage of incoming items that were cancelled"
    value_format_name: percent_2
    sql: ${cancel_amount_usd}/nullif(${gross_rev_usd}, 0);;
  }


##################### SHIPPED #####################


  measure: total_shipped_orders {
    type: count_distinct
    group_label: "Sales Metrics"
    label: "# of Shipped Orders"
    description: "Total number of orders that are partially or fully shipped"
    sql: ${order_id};;
    filters: [record_type: "SHIPMENT"]
  }

  measure: units_shipped {
    type: sum
    group_label: "Sales Metrics"
    label: "Units Shipped"
    description: "Total number of shipped items"
    sql: ${qty_shipped};;
  }

  measure: net_units_shipped {
    type: number
    group_label: "Sales Metrics"
    label: "Units Shipped minus Returns"
    description: "Total number of shipped items excluding returned items"
    sql: ${units_shipped} - ${units_returned} ;;
  }

  measure: shipped_amount_local {
    type: sum
    group_label: "Sales Metrics"
    label: "Shipped Revenue (AED)"
    description: "Total value of shipped orders (excluding failed credit card attempts, VAT and cancellations but including customs and shipping fees)"
    value_format: "#,##0.00"
    sql: ${shipment_revenue_amount_local};;
  }

  measure: shipped_amount_original {
    type: sum
    group_label: "Sales Metrics"
    label: "Shipped Revenue (Local)"
    description: "Total value of shipped orders (excluding failed credit card attempts, VAT and cancellations but including customs and shipping fees)"
    value_format: "#,##0.00"
    sql: ${shipment_revenue_amount_original};;
  }

  measure: shipped_amount_usd {
    type: sum
    group_label: "Sales Metrics"
    label: "Shipped Revenue (USD)"
    description: "Total value of shipped orders (excluding failed credit card attempts, VAT and cancellations but including customs and shipping fees)"
    value_format: "$#,##0.00"
    sql: ${shipment_revenue_amount_usd};;
  }

  measure: net_sales_local {
    type: sum
    group_label: "Sales Metrics"
    label: "Shipped Net Sales (AED)"
    description: "Total value of incoming orders on the website excluding returns (excluding failed credit card attempts, VAT, customs and shipping fees and COD fees). To be computed based on shipping date"
    value_format: "#,##0.00"
    sql:  ${shipment_net_sales_local};;
  }

  measure: net_sales_usd {
    type: sum
    group_label: "Sales Metrics"
    label: "Shipped Net Sales (USD)"
    description: "Total value of incoming orders on the website excluding returns (excluding failed credit card attempts, VAT, customs and shipping fees and COD fees). To be computed based on shipping date"
    value_format: "$#,##0.00"
    sql: ${shipment_net_sales_usd};;
  }

  measure: net_sales_original {
    type: sum
    group_label: "Sales Metrics"
    label: "Shipped Net Sales (Local)"
    description: "Total value of incoming orders on the website excluding returns (excluding failed credit card attempts, VAT, customs and shipping fees and COD fees). To be computed based on shipping date"
    value_format: "#,##0.00"
    sql: ${shipment_net_sales_original};;
  }


##################### RETURNS #####################


  measure: units_returned {
    type: sum
    group_label: "Sales Metrics"
    label: "Units Returned"
    description: "Total numer of returned items"
    sql: ${qty_returned};;
  }

  measure: total_return_orders {
    type: count_distinct
    hidden:  yes
    group_label: "Sales Metrics"
    label: "# of Returned Orders"
    description: "Total value of orders that are partially or fully returned"
    sql: ${order_id};;
    filters: [record_type: "REFUND"]
  }

  measure: refund_amount_local {
    type: sum
    group_label: "Sales Metrics"
    label: "Returned Amount (AED)"
    description: "Total value of orders that are returned in local currency"
    value_format: "#,##0.00"
    sql: ${refund_amount};;
  }

  measure: refund_amount_usd {
    type: sum
    group_label: "Sales Metrics"
    label: "Returned Amount (USD)"
    description: "Total value of orders that are returned in USD"
    value_format: "$#,##0.00"
    sql: ${refund_usd};;
  }

  measure: refund_amount_original {
    type: sum
    group_label: "Sales Metrics"
    label: "Returned Amount (Local)"
    description: "Total value of orders that are returned in USD"
    value_format: "#,##0.00"
    sql: ${refund_original};;
  }

  measure: return_rate_value {
    type: number
    group_label: "Sales Metrics"
    label: "Returned Rate (% Shipped value)"
    description: "Percentage of shipped items that were returned"
    value_format_name: percent_2
    sql: ${refund_amount_usd}/nullif(${shipped_amount_usd}, 0);;
  }

  measure: return_rate_item {
    type: number
    group_label: "Sales Metrics"
    label: "Returned Rate (% items)"
    description: "Percentage of shipped items quantity that were returned out of total shipped items quantity"
    value_format_name: percent_2
    sql: nullif(${units_returned}/nullif(${units_shipped}, 0),0);;
  }

  measure: return_rate_value_packed {
    type: number
    group_label: "Sales Metrics"
    label: "Returned Rate (% Packed value)"
    description: "Percentage of packed items that were returned"
    value_format_name: percent_2
    sql: ${refund_amount_usd}/nullif(${packed_amount_usd}, 0);;
  }

  measure: return_rate_item_packed {
    type: number
    group_label: "Sales Metrics"
    label: "Returned Item (% packed items)"
    description: "Percentage of packed items quantity that were returned out of total packed items quantity"
    value_format_name: percent_2
    sql: nullif(${units_returned}/nullif(${units_packed}, 0),0);;
  }


##################### PACKED #####################


  measure: total_packed_orders {
    type: count_distinct
    hidden:  yes
    group_label: "Sales Metrics"
    label: "# of Packed Orders"
    description: "Total number of orders that are partially or fully packed"
    sql: ${order_id};;
    filters: [record_type: "PACKED"]
  }

  measure: units_packed {
    type: sum
    group_label: "Sales Metrics"
    label: "Units Packed"
    description: "Total number of packed items"
    sql: ${qty_packed};;
  }

  measure: net_units_packed {
    type: number
    group_label: "Sales Metrics"
    label: "Units Packed minus Returns"
    description: "Total number of packed items excluding returned items"
    sql: ${units_packed} - ${units_returned} ;;
  }

  measure: packed_amount_local {
    type: sum
    group_label: "Sales Metrics"
    label: "Packed Sales (AED)"
    description: "Total value of shipped orders (excluding failed credit card attempts, VAT and cancellations but including customs and shipping fees)"
    value_format: "#,##0.00"
    sql: ${packed_revenue_local};;
  }

  measure: packed_amount_usd {
    type: sum
    group_label: "Sales Metrics"
    label: "Packed Sales (USD)"
    description: "Total value of shipped orders (excluding failed credit card attempts, VAT and cancellations but including customs and shipping fees)"
    value_format: "$#,##0.00"
    sql: ${packed_revenue_usd};;
  }

  measure: packed_amount_original {
    type: sum
    group_label: "Sales Metrics"
    label: "Packed Sales (Local)"
    description: "Total value of shipped orders (excluding failed credit card attempts, VAT and cancellations but including customs and shipping fees)"
    value_format: "#,##0.00"
    sql: ${packed_revenue_original};;
  }

  measure: packed_net_sales_local {
    type: sum
    group_label: "Sales Metrics"
    label: "Packed Sales minus Returns (AED)"
    description: "Total value of incoming orders on the website excluding returns (excluding failed credit card attempts, VAT, customs and shipping fees and COD fees). To be computed based on packing date"
    value_format: "#,##0.00"
    sql: ${packed_sales_minus_return_local};;
  }

  measure: packed_net_sales_usd {
    type: sum
    group_label: "Sales Metrics"
    label: "Packed Sales minus Returns (USD)"
    description: "Total value of incoming orders on the website excluding returns (excluding failed credit card attempts, VAT, customs and shipping fees and COD fees). To be computed based on packing date"
    value_format: "$#,##0.00"
    sql: ${packed_sales_minus_return_usd};;
  }

  measure: packed_net_sales_original {
    type: sum
    group_label: "Sales Metrics"
    label: "Packed Sales minus Returns (Local)"
    description: "Total value of incoming orders on the website excluding returns (excluding failed credit card attempts, VAT, customs and shipping fees and COD fees). To be computed based on packing date"
    value_format: "#,##0.00"
    sql: ${packed_sales_minus_return_original};;
  }

##################### Gross Margin #####################


  measure: cogs_local {
    type: sum
    hidden:  yes
    group_label: "Sales Metrics"
    value_format: "#,##0.00"
    description: "Cost of Good Sold (Local Currency). Only available for items with cost under store 8003"
    sql: case when ${unit_cost} <> 0 then ${qty_ordered}*${unit_cost} else 0 end;;
    filters: [record_type: "SALE"]
  }

  measure: cogs_usd {
    type: sum
    group_label: "Sales Metrics"
    value_format: "$#,##0.00"
    label: "COGS (USD)"
    description: "Cost of Good Sold (USD). Only available for items with cost under store 8003"
    sql: case when ${unit_cost_usd} <> 0 then ${qty_ordered}*${unit_cost_usd} else 0 end;;
    filters: [record_type: "SALE"]
  }

  measure: cogs_aed {
    type: sum
    group_label: "Sales Metrics"
    value_format: "$#,##0.00"
    label: "COGS (AED)"
    description: "Cost of Good Sold (AED). Only available for items with cost under store 8003"
    sql: case when ${unit_cost_usd} <> 0 then ${qty_ordered}*${unit_cost_usd} else 0 end;;
    filters: [record_type: "SALE"]
  }

  measure: gross_margin_local {
    type: number
    hidden:  yes
    group_label: "Sales Metrics"
    label: "Gross Margin (Local Currency)"
    value_format: "#,##0.00"
    description: "Gross margin is a company's net sales revenue minus its cost of goods sold (COGS) in local currency"
    sql: case when ${unit_cost} <> 0 then nullif(${gross_revenue_amount_local}, 0)-nullif(${qty_ordered}*${unit_cost}, 0) else 0 end;;
  }

  measure: gross_margin_usd {
    type: sum
    group_label: "Sales Metrics"
    label: "Gross Margin (USD)"
    value_format: "$#,##0.00"
    description: "Gross margin is a company's net sales revenue minus its cost of goods sold (COGS) in USD"
    sql: case when ${unit_cost_usd} <> 0 then nullif(${gross_revenue_amount_usd}, 0)-nullif(${qty_ordered}*${unit_cost_usd}, 0) else 0 end;;
  }

  measure: gross_margin_aed {
    type: sum
    group_label: "Sales Metrics"
    label: "Gross Margin (AED)"
    value_format: "#,##0.00"
    description: "Gross margin is a company's net sales revenue minus its cost of goods sold (COGS) in local currency"
    sql: case when ${unit_cost_aed} <> 0 then nullif(${gross_revenue_amount_local}, 0)-nullif(${qty_ordered}*${unit_cost_aed}, 0) else 0 end;;
  }

  measure: gross_margin_ratio {
    type: number
    group_label: "Sales Metrics"
    label: "Gross Margin (%). Only available for items with cost under store 8003"
    description: "Gross margin is a company's net sales revenue minus its cost of goods sold (COGS)"
    value_format_name: percent_2
#     sql:case when ${cogs_local} <> 0 then 1-(${cogs_local}/nullif(${gross_rev_local}, 0)) else 0 end;;
    sql: sum(case when ${unit_cost_usd} <> 0 then nullif(${gross_revenue_amount_usd}, 0)-nullif(${qty_ordered}*${unit_cost_usd}, 0) else 0 end)/sum(case when ${unit_cost_usd} <> 0 then nullif(${gross_revenue_amount_usd}, 0) end);;
  }

  measure: sum_magento_discount_invoiced {
    type: sum
    group_label: "Sales Metrics"
    label: "Magento Discount Invoiced"
    description: "Discount invoiced from Magento coupon code"
    value_format_name: usd
    sql: ${magento_discount_invoiced} ;;
  }

  measure: sum_magento_discount_refunded {
    type: sum
    group_label: "Sales Metrics"
    label: "Magento Discount Refunded"
    description: "Discount refunded from Magento coupon code"
    value_format_name: usd
    sql: ${magento_discount_refunded} ;;
  }


################### DISCOUNT ###################

  measure: total_discount_aed {
    type: sum
    hidden: no
    label: "Discount Amount Total (AED)"
    description: "Total Discount applied on incoming orders on the website in AED"
    group_label: "Sales Metrics"
    sql: ${discount_amount_aed} ;;
    filters: [record_type: "SALE"]
  }

  measure: discount_percentage {
    type: number
    label: "Discount %"
    description: "Percentage Discount applied on incoming order"
    group_label: "Sales Metrics"
    value_format_name: percent_2
    sql:  IF(${total_discount_aed} = 0,
            0,
            ${total_discount_aed}/(${total_discount_aed} + ${gross_rev_tax_local})
          );;
  }

  measure: discount_reason {
    type: string
    label: "Discount Reason"
    description: "Reason why discount is being given: Coupon, Markdown, Coupon + Markdown, Full Price"
    group_label: "Sales Metrics"
    sql:  IF(${total_discount_aed} = 0,

            "Full Price",
            if(${level_shoes_coupon_codes.coupon_discount} IS NULL,

              "Markdown",
              if( ((${discount_percentage} * 100) - ${level_shoes_coupon_codes.coupon_discount}) > 1,

                "Coupon + Markdown",
                "Coupon"
              )
            )
          )
        ;;
  }

  dimension: item_discount_percentage {
    type: number
    label: "Item Discount %"
    description: "Percentage Discount applied on incoming order. Filter Record Type by 'SALE'."
    group_label: "Sales Order Item"
    value_format_name: percent_2
    sql:  IF(${record_type} = "SALE",
            IF(${discount_amount_aed} = 0,
              0,
              ${discount_amount_aed}/(${discount_amount_aed} + ${detail_price_with_tax_minus_discount_aed})
            ),
            NULL
          );;
  }

  dimension: item_discount_reason {
    type: string
    label: "Item Discount Reason"
    description: "Reason why discount is being given: Coupon, Markdown, Coupon + Markdown, Full Price. Filter Record Type by 'SALE'."
    group_label: "Sales Order Item"
    sql:  IF(${record_type} = "SALE",
            IF(${discount_amount_aed} = 0,

              "Full Price",
              IF(${level_shoes_coupon_codes.coupon_discount} IS NULL,

                "Markdown",
                IF( ((${item_discount_percentage} * 100) - ${level_shoes_coupon_codes.coupon_discount}) > 1,

                  "Coupon + Markdown",
                  "Coupon"
                )
              )
            ),
            NULL
          )
        ;;
  }


##################### UNUSED MEASURES #####################


#   measure: total_order_shipping_cost_local {
#     type: sum_distinct
#     hidden: yes
#     group_label: "Sales Metrics"
#     label: "Order Shipping Cost"
#     description: "In AED"
#     value_format: "$#,##0.00"
#     sql_distinct_key: ${order_id} ;;
#     sql:${order_shipping_aed};;
#   }
#
#   measure: total_order_payment_fee_local {
#     type: sum_distinct
#     hidden: yes
#     group_label: "Sales Metrics"
#     label: "Order Payment Fee"
#     description: "In AED"
#     value_format: "$#,##0.00"
#     sql_distinct_key: ${order_id} ;;
#     sql:${order_payment_fee_aed};;
#   }
#
#   measure: total_order_custom_duty_local {
#     type: sum_distinct
#     hidden: yes
#     group_label: "Sales Metrics"
#     label: "Order Custom Duty"
#     description: "In AED"
#     value_format: "$#,##0.00"
#     sql_distinct_key: ${order_id} ;;
#     sql:${order_custom_duty_fee_aed};;
#   }
#
#   measure: total_order_paid_local {
#     type: sum_distinct
#     hidden: yes
#     group_label: "Sales Metrics"
#     label: "Order Total"
#     description: "In AED"
#     value_format: "$#,##0.00"
#     sql_distinct_key: ${order_id} ;;
#     sql:${TABLE}.order_total_aed;;
#   }
#
#   measure: cancelled_order_ratio {
#     type: number
#     group_label: "Sales Metrics"
#     label: "Cancelled Order %"
#     description: "Cancelled Order %"
#     hidden:  yes
#     value_format_name: percent_2
#     sql: ${count_cancelled_orders}/NULLIF(${count_sales_orders},0) ;;
#   }
#
#   measure: returned_order_ratio {
#     type: number
#     group_label: "Sales Metrics"
#     label: "Returned Order %"
#     description: "Returned Order %"
#     value_format_name: percent_2
#     sql: ${count_returned_orders}/NULLIF(${count_sales_orders},0) ;;
#   }
#
#   measure: cancelled_item_ratio {
#     type: number
#     group_label: "Sales Metrics"
#     label: "Cancelled Item %"
#     description: "Cancelled item % of orders in the selected period"
#     value_format_name: percent_2
#     sql: ${unit_canceled}/NULLIF(${units_ordered},0) ;;
#   }
#
#   measure: return_rate_item {
#     type: number
#     group_label: "Sales Metrics"
#     label: "Returned Rate (% item)"
#     description: "# of items returned / total items ordered"
#     value_format_name: percent_2
#     sql: ${units_returned}/nullif(${units_ordered}, 0);;
#   }


## End of Sales Order Measures   ##}


###################################
##     Acq/Mkting Dimensions     ##
###################################{


  dimension: acq_campaign {
    type: string
    group_label: "Acquisition / Marketing"
    label: "Campaign"
    sql: ${TABLE}.acq_campaign ;;
  }

  dimension: acq_device {
    type: string
    group_label: "Acquisition / Marketing"
    label: "Device"
    sql: ${TABLE}.acq_device ;;
  }

  dimension: acq_keyword {
    type: string
    group_label: "Acquisition / Marketing"
    label: "Keyword"
    sql: ${TABLE}.acq_keyword ;;
  }

  dimension: acq_medium {
    type: string
    group_label: "Acquisition / Marketing"
    label: "Medium"
    sql: ${TABLE}.acq_medium ;;
  }

  dimension: acq_source {
    type: string
    group_label: "Acquisition / Marketing"
    label: "Source"
    sql: ${TABLE}.acq_source ;;
  }


  dimension: acq_country {
    type: string
    group_label: "Acquisition / Marketing"
    label: "Marketing Country"
    hidden: no
    sql: ${TABLE}.acq_country ;;
  }

  dimension: marketing_source {
    type: string
    group_label: "Acquisition / Marketing"
    description: "Acquisition Platform"
    label: "Marketing Source"
    sql: ${TABLE}.marketing_source ;;
  }

##  End of Acq/Mkting Dimensions ##}


###################################
##            GA Measures        ##
###################################{

  measure: number_of_sessions {
    type: sum
    group_label: "GA Metrics"
    label: "# of Sessions"
    description: "Total number of sessions on the website"
    sql: ${TABLE}.sessions ;;
  }

  measure: sum_session_duration {
    type: sum
    hidden: yes
    group_label: "GA Metrics"
    label: "Sessions duration"
    sql: ${TABLE}.session_duration ;;
  }

  measure: sum_bounces {
    type: sum
    group_label: "GA Metrics"
    label: "# of Bounces"
    description: "Total number of visitors who leave the website after only one visit"
    sql: ${TABLE}.bounces ;;
  }

  measure: number_of_pageviews {
    type: sum
    group_label: "GA Metrics"
    label: "# of Page views"
    description: "Total number of pages viewed"
    sql: ${TABLE}.pageviews ;;
  }

  measure: sum_unique_pageviews {
    type: sum
    group_label: "GA Metrics"
    label: "# of Unique Page views"
    description: "Total number of unique pages viewed"
    sql: ${TABLE}.unique_pageviews ;;
  }

  measure: sum_product_adds_to_cart {
    type: sum
    group_label: "GA Metrics"
    label: "# of Products Added to Cart"
    description: "Total number of products added to cart"
    sql: ${TABLE}.product_adds_to_cart ;;
  }

  measure: sum_product_checkouts {
    type: sum
    group_label: "GA Metrics"
    label: "# of Product Checkouts"
    description: "Total number of product that were bought"
    sql: ${TABLE}.product_checkouts ;;
  }

  measure: sum_users {
    type: sum
    group_label: "GA Metrics"
    label: "# of Users"
    description: "Total number of users"
    sql: ${TABLE}.users ;;
  }

  measure: sum_new_users {
    type: sum
    group_label: "GA Metrics"
    label: "# of New Users"
    description: "Total number of new users"
    sql: ${TABLE}.new_users ;;
  }

  measure: sum_product_list_views {
    type: sum
    group_label: "GA Metrics"
    label: "# of Product List Views"
    description: "Total number of Product List Views"
    sql: ${TABLE}.product_list_views ;;
  }

  measure: sum_product_detail_views {
    type: sum
    group_label: "GA Metrics"
    label: "# of Product Detail Views"
    description: "Total number of Product Detail Views"
    sql: ${TABLE}.product_detail_views ;;
  }

  measure: pageviews_per_session {
    type: number
    group_label: "GA Metrics"
    label: "Pageviews per Session"
    description: "Average number of pages viewed per session"
    value_format_name: decimal_2
    sql: ${number_of_pageviews}/NULLIF(${number_of_sessions},0) ;;
  }


  measure: bounce_rate {
    type: number
    group_label: "GA Metrics"
    label: "Bounce Rate %"
    description: "Percentage of visitors who leave the website after only one page view"
    value_format_name: percent_2
    sql: ${sum_bounces}/NULLIF(${number_of_sessions},0) ;;
  }

  measure: avg_time_on_site {
    type: number
    group_label: "GA Metrics"
    label: "Avg Time On Site"
    description: "Average of time spend on the site per session"
    value_format: "h:mm:ss"
    sql: ${sum_session_duration}/NULLIF(${number_of_sessions},0) / 86400 ;;
  }


  measure: add_to_basket_rate {
    type: number
    group_label: "GA Metrics"
    label: "Add to Basket %"
    description: "Percentage of product detail views that have added the product to their basket over a period of time"
    value_format_name: percent_2
    sql: ${sum_product_adds_to_cart}/NULLIF(${number_of_pageviews},0) ;;
  }

  measure: cart_abandonment_rate {
    type: number
    group_label: "GA Metrics"
    label: "Cart Abandonment %"
    description: "Number of cart created that did not convert into an order (1-% cart abandonment = cart compeltion rate)"
    value_format_name: percent_2
    sql: (${sum_product_adds_to_cart}-${sum_product_checkouts})/NULLIF(${sum_product_adds_to_cart},0) ;;
  }

  measure: checkout_to_add_rate {
    type: number
    group_label: "GA Metrics"
    label: "Checkout to Add to Basket %"
    description: "Percentage of products that were added to cart and converted into an order"
    value_format_name: percent_2
    sql: ${sum_product_checkouts}/NULLIF(${sum_product_adds_to_cart},0) ;;
  }


  measure: conversion_rate {
    type: number
    group_label: "GA Metrics"
    label: "Conversion Rate (%)"
    description: "Percentage of sessions that make an order"
    value_format_name: percent_2
    sql: ${count_sales_orders}/NULLIF(${number_of_sessions},0) ;;
  }




##       End of GA Measures      ##}



###################################
##         Funnel Measures       ##
###################################{



  measure: total_ad_spend_usd {
    type: sum
    group_label: "Marketing Metrics"
    label: "Total Ad Spend (USD)"
    value_format: "$#,##0.00"
    description: "Total value of marketing ads spent across all different channels"
    sql: ${common_cost_usd} ;;
  }

  measure: performance_roas_gross_rev {
    type: number
    group_label: "Marketing Metrics"
    label: "Performance ROAS (on Gross Rev)"
    description: "The Return in Gross Sales from Performance Ad Spend"
    value_format: "#,##0.00"
    sql: ${gross_rev_usd}/(nullif(${total_ad_spend_usd}, 0) * 0.75) ;;
  }

  measure: roas_gross_rev {
    type: number
    group_label: "Marketing Metrics"
    label: "ROAS (on Gross Rev)"
    description: "The Return in Gross Sales from Performance Ad Spend"
    value_format: "#,##0.00"
    sql: ${gross_rev_usd}/(nullif(${total_ad_spend_usd}, 0)) ;;
  }

  measure: roi_gross_rev {
    type: number
    group_label: "Marketing Metrics"
    label: "ROI (on Gross Rev)"
    description: "The Return in Gross Sales from Digital Ad Spend"
    value_format: "#,##0.00"
    sql: ${gross_rev_usd}/nullif(${total_ad_spend_usd}, 0) ;;
  }

  measure: cost_per_order {
    type: number
    group_label: "Marketing Metrics"
    label: "CPO"
    description: "The marketing cost of one order"
    value_format: "$#,##0.00"
    sql: ${total_ad_spend_usd}/nullif(${count_sales_orders}, 0) ;;
  }

  measure: total_clicks {
    type: sum
    group_label: "Marketing Metrics"
    label: "# Clicks"
    description: "Total number of times an ad is clicked by a user"
    sql: ${common_clicks} ;;
  }

  measure: total_impressions {
    type: sum
    group_label: "Marketing Metrics"
    label: "# Impressions"
    description: "Total number of times an ad is seen"
    sql:  ${common_impressions};;
  }

  measure: cost_per_click {
    type: number
    group_label: "Marketing Metrics"
    label: "CPC"
    description: "The marketing cost of one ad click"
    value_format_name: usd
    sql: ${total_ad_spend_usd} / NULLIF(${total_clicks},0) ;;
  }


  measure: click_through_rate {
    type: number
    group_label: "Marketing Metrics"
    label: "Click Through Rate"
    description: "Percentage ad impressions that have led to a click"
    value_format_name: percent_2
    sql: NULLIF(${total_clicks},0) / NULLIF(${total_impressions},0);;
  }


  measure: cost_per_mill {
    type: number
    group_label: "Marketing Metrics"
    label: "CPM"
    description: "The marketing cost of 1000 impressions"
    value_format_name: usd
    sql: NULLIF(${total_ad_spend_usd},0) / NULLIF(${total_impressions}/1000,0) ;;
  }

  measure: number_of_days {
    type: count_distinct
    hidden: yes
    sql: ${common_date} ;;
  }

  measure: clicks_per_day {
    type: number
    group_label: "Marketing Metrics"
    value_format_name: decimal_2
    label: "Clicks per Day"
    description: "Average number of clicks in a day for a given period"
    sql: ${total_clicks} / NULLIF(${number_of_days},0) ;;
  }

  measure: cost_per_day {
    type: number
    group_label: "Marketing Metrics"
    label: "Cost per Day"
    description: "Average ad spend in value in a day for a given period in USD"
    value_format_name: usd
    sql: ${total_ad_spend_usd} / NULLIF(${number_of_days},0) ;;
  }

  measure: cost_of_acquisition{
    type: number
    value_format_name: decimal_2
    group_label: "Marketing Metrics"
    label: "Cost of Acquisition"
    description: "The marketing cost of one acquired customer"
    sql: NULLIF(${total_ad_spend_usd} / NULLIF(${ecomm_customer_type_pop.new_cust_count_fixed},0),0) ;;
  }


## End of  Measures ##}


###################################
##        Hidden Dimensions      ##
###################################{

## Hidden Sales Order Dimensions ##


  dimension: qty_canceled {
    type: number
    group_label: "Sales Order"
    hidden: yes
    sql: ${TABLE}.qty_canceled ;;
  }

  dimension: qty_packed {
    type: number
    group_label: "Sales Order"
    hidden: yes
    sql: ${TABLE}.qty_packed ;;
  }

  dimension: qty_ordered {
    type: number
    group_label: "Sales Order"
    hidden: yes
    sql: ${TABLE}.qty_ordered ;;
  }

  dimension: qty_returned {
    type: number
    group_label: "Sales Order"
    hidden: yes
    sql: ${TABLE}.qty_returned ;;
  }

  dimension: qty_shipped {
    type: number
    group_label: "Sales Order"
    hidden: yes
    sql: ${TABLE}.qty_shipped ;;
  }

  dimension: cancel_amount {
    type: number
    group_label: "Sales Order"
    hidden: yes
    sql: ${TABLE}.cancel_amount_local ;;
  }

  dimension: cancel_usd {
    type: number
    group_label: "Sales Order"
    hidden: yes
    sql: ${TABLE}.cancel_amount_usd ;;
  }

  dimension: amount_refunded {
    type: number
    group_label: "Sales Order"
    hidden: yes
    sql: ${TABLE}.amount_refunded ;;
  }

  dimension: refund_amount {
    type: number
    group_label: "Sales Order"
    hidden: yes
    sql: ${TABLE}.refund_amount_local ;;
  }

  dimension: refund_usd {
    type: number
    group_label: "Sales Order"
    hidden: yes
    sql: ${TABLE}.refund_amount_usd ;;
  }

  dimension: shipment_revenue_amount_usd {
    type: number
    group_label: "Sales Order"
    hidden: yes
    sql: ${TABLE}.shipment_revenue_amount_usd ;;
  }

  dimension: shipment_revenue_amount_local {
    type: number
    group_label: "Sales Order"
    hidden: yes
    sql: ${TABLE}.shipment_revenue_amount_local ;;
  }

  dimension: shipped_minus_cancel_local {
    type: number
    group_label: "Sales Order"
    hidden: yes
    sql: ${TABLE}.shipped_minus_cancel_local ;;
  }

  dimension: shipped_minus_cancel_usd {
    type: number
    group_label: "Sales Order"
    hidden: yes
    sql: ${TABLE}.shipped_minus_cancel_usd ;;
  }

  dimension: gross_revenue_amount_local {
    type: number
    group_label: "Sales Order"
    hidden: yes
    sql: ${TABLE}.gross_revenue_amount_local;;
  }

  dimension: gross_revenue_amount_usd {
    type: number
    group_label: "Sales Order"
    hidden: yes
    sql: ${TABLE}.gross_revenue_amount_usd;;
  }

  dimension: gross_minus_cancel_local {
    type: number
    group_label: "Sales Order"
    hidden: yes
    sql: ${TABLE}.gross_minus_cancel_local;;
  }

  dimension: gross_minus_cancel_usd {
    type: number
    group_label: "Sales Order"
    hidden: yes
    sql: ${TABLE}.gross_minus_cancel_usd;;
  }

  dimension: gross_net_sales_local {
    type: number
    group_label: "Sales Order"
    hidden: yes
    sql: ${TABLE}.gross_net_sales_local;;
  }

  dimension: gross_net_sales_usd {
    type: number
    group_label: "Sales Order"
    hidden: yes
    sql: ${TABLE}.gross_net_sales_usd;;
  }

  dimension: cancel_original {
    type: number
    group_label: "Sales Order"
    hidden: yes
    sql: ${TABLE}.cancel_amount_original ;;
  }

  dimension: refund_original {
    type: number
    group_label: "Sales Order"
    hidden: yes
    sql: ${TABLE}.refund_amount_original ;;
  }

  dimension: shipment_revenue_amount_original {
    type: number
    group_label: "Sales Order"
    hidden: yes
    sql: ${TABLE}.shipment_revenue_amount_original ;;
  }

  dimension: shipped_minus_cancel_original {
    type: number
    group_label: "Sales Order"
    hidden: yes
    sql: ${TABLE}.shipped_minus_cancel_original ;;
  }

  dimension: gross_revenue_amount_original {
    type: number
    group_label: "Sales Order"
    hidden: yes
    sql: ${TABLE}.gross_revenue_amount_original;;
  }


  dimension: gross_minus_cancel_original {
    type: number
    group_label: "Sales Order"
    hidden: yes
    sql: ${TABLE}.gross_minus_cancel_original;;
  }

  dimension: gross_net_sales_original {
    type: number
    group_label: "Sales Order"
    hidden: yes
    sql: ${TABLE}.gross_net_sales_original;;
  }


  dimension: shipment_net_sales_original {
    type: number
    group_label: "Sales Order"
    hidden: yes
    sql: ${TABLE}.shipment_net_sales_original ;;
  }

  dimension: shipment_net_sales_local {
    type: number
    group_label: "Sales Order"
    hidden: yes
    sql: ${TABLE}.shipment_net_sales_local ;;
  }

  dimension: shipment_net_sales_usd {
    type: number
    group_label: "Sales Order"
    hidden: yes
    sql: ${TABLE}.shipment_net_sales_usd ;;
  }

  dimension: packed_sales_minus_return_usd {
    type: number
    group_label: "Sales Order"
    hidden: yes
    sql: ${TABLE}.packed_sales_minus_return_usd ;;
  }

  dimension: packed_revenue_original {
    type: number
    group_label: "Sales Order"
    hidden: yes
    sql: ${TABLE}.packed_revenue_original ;;
  }

  dimension: packed_revenue_local {
    type: number
    group_label: "Sales Order"
    hidden: yes
    sql: ${TABLE}.packed_revenue_local ;;
  }

  dimension: packed_revenue_usd {
    type: number
    group_label: "Sales Order"
    hidden: yes
    sql: ${TABLE}.packed_revenue_usd ;;
  }

  dimension: packed_revenue_original_minus {
    type: number
    group_label: "Sales Order"
    hidden: yes
    sql: ${TABLE}.packed_revenue_original_minus ;;
  }

  dimension: packed_revenue_local_minus {
    type: number
    group_label: "Sales Order"
    hidden: yes
    sql: ${TABLE}.packed_revenue_local_minus ;;
  }

  dimension: packed_revenue_usd_minus {
    type: number
    group_label: "Sales Order"
    hidden: yes
    sql: ${TABLE}.packed_revenue_usd_minus ;;
  }

  dimension: packed_sales_minus_return_original {
    type: number
    group_label: "Sales Order"
    hidden: yes
    sql: ${TABLE}.packed_sales_minus_return_original ;;
  }

  dimension: packed_sales_minus_return_local {
    type: number
    group_label: "Sales Order"
    hidden: yes
    sql: ${TABLE}.packed_sales_minus_return_local ;;
  }

  dimension: packed_sales_minus_return_usd_incl_tax {
    type: number
    group_label: "Sales Order"
    hidden: yes
    sql: ${TABLE}.packed_sales_minus_return_usd_incl_tax ;;
  }

  dimension: packed_sales_minus_return_local_incl_tax {
    type: number
    group_label: "Sales Order"
    hidden: yes
    sql: ${TABLE}.packed_sales_minus_return_local_incl_tax ;;
  }

  dimension: currency_rate_usd {
    type: number
    group_label: "Sales Order"
    hidden: yes
    sql: ${TABLE}.currency_rate_usd;;
  }

  dimension: currency_rate_aed {
    type: number
    group_label: "Sales Order"
    hidden: yes
    sql: ${TABLE}.currency_rate_aed;;
  }


  dimension: products_in_order {
    type: number
    group_label: "Sales Order"
    hidden: yes
    sql: ${TABLE}.products_in_order ;;
  }

  dimension: discount_refunded{
    type: number
    group_label: "Sales Order"
    hidden: yes
    sql: ${TABLE}.discount_refunded ;;
  }

  dimension: discount_refunded_aed{
    type: number
    group_label: "Sales Order"
    hidden: yes
    sql: ${TABLE}.discount_refunded_aed ;;
  }

  dimension: order_custom_duty_fee {
    type: number
    hidden: yes
    sql: ${TABLE}.order_custom_duty_fee ;;
  }


  dimension: order_payment_fee {
    type: number
    hidden: yes
    sql: ${TABLE}.order_payment_fee ;;
  }

  dimension: order_shipping {
    type: number
    hidden: yes
    sql: ${TABLE}.order_shipping ;;
  }

  dimension: order_total {
    type: number
    hidden: yes
    sql: ${TABLE}.order_total ;;
  }

  dimension: detail_price_with_tax_minus_discount {
    type: number
    hidden: yes
    sql: (${TABLE}.detail_price_original - ${TABLE}.discount_amount -${TABLE}.discount_refunded) * ${TABLE}.qty_ordered  ;;
  }

  dimension: detail_price_with_tax_minus_discount_aed {
    type: number
    hidden: yes
    sql: ${detail_price_with_tax_minus_discount}*${currency_rate_aed} ;;
  }


  dimension: detail_price_with_tax_minus_discount_usd {
    type: number
    hidden: yes
    sql: ${detail_price_with_tax_minus_discount}*${currency_rate_usd} ;;
  }

  dimension: entity_id {
    type: number
    hidden: yes
    sql: ${TABLE}.entity_id ;;
  }

  dimension: sales_origin_id {
    type: number
    hidden: yes
    sql: ${TABLE}.sales_origin_id ;;
  }

  dimension: sales_origin_name {
    type: string
    hidden: yes
    sql: ${TABLE}.sales_origin_name ;;
  }

  dimension: return_location_id {
    type: string
    hidden: yes
    sql: ${TABLE}.return_location_id ;;
  }



  dimension_group: update_timestamp {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.update_timestamp ;;
  }

##     Hidden GA Dimensions    ##

  dimension: session_duration {
    type: number
    group_label: "GA Dimensions"
    hidden: yes
    sql: ${TABLE}.session_duration ;;
  }

  dimension: sessions {
    type: number
    group_label: "GA Dimensions"
    hidden: yes
    sql: ${TABLE}.sessions ;;
  }

  dimension: new_users {
    type: number
    group_label: "GA Dimensions"
    hidden: yes
    sql: ${TABLE}.new_users ;;
  }


  dimension: bounces {
    type: number
    group_label: "GA Dimensions"
    hidden: yes
    sql: ${TABLE}.bounces ;;
  }


  dimension: pageviews {
    type: number
    group_label: "GA Dimensions"
    hidden: yes
    sql: ${TABLE}.pageviews ;;
  }

  dimension: product_adds_to_cart {
    type: number
    group_label: "GA Dimensions"
    hidden: yes
    sql: ${TABLE}.product_adds_to_cart ;;
  }

  dimension: product_checkouts {
    type: number
    group_label: "GA Dimensions"
    hidden: yes
    sql: ${TABLE}.product_checkouts ;;
  }

  dimension: users {
    type: number
    group_label: "GA Dimensions"
    hidden: yes
    sql: ${TABLE}.users ;;
  }


##   Hidden Funnel Dimensions    ##


  dimension: common_clicks {
    type: number
    group_label: "Funnel Dimensions"
    hidden: yes
    sql: ${TABLE}.common_clicks ;;
  }

  dimension: common_cost_usd {
    type: number
    group_label: "Funnel Dimensions"
    hidden: yes
    sql: ${TABLE}.common_cost_usd ;;
  }


  dimension: common_impressions {
    type: number
    group_label: "Funnel Dimensions"
    hidden: yes
    sql: ${TABLE}.common_impressions ;;
  }


  dimension: unique_pageviews {
    type: number
    group_label: "GA Dimensions"
    hidden: yes
    sql: ${TABLE}.unique_pageviews ;;
  }

  dimension: product_list_views {
    type: number
    group_label: "GA Dimensions"
    hidden: yes
    sql: ${TABLE}.product_list_views ;;
  }

  dimension: product_detail_views {
    type: number
    group_label: "GA Dimensions"
    hidden: yes
    sql: ${TABLE}.product_detail_views ;;
  }


##    End of Hidden Dimensions   ##}

  ##########################
  #######   ALERTS   #######
  ##########################

  dimension_group: record {
    type: time
    timeframes: [
      raw,
      second,
      minute,
      hour,
      date,
      week,
      month,
      quarter,
      year,
      week_of_year,
      day_of_week
    ]
    convert_tz: no
    datatype: datetime
    label: "Record"
    sql: ${TABLE}.record_datetime ;;
  }


  measure: minutes_since_last_update {
    type: min
    group_label: "Alerts"
    sql: DATETIME_DIFF(current_datetime(),${record_raw},minute)+240 ;;
  }
  measure: actual_cost {
    type: number
    value_format: "$#,##0.00"
    group_label: "Sales Metrics"
    sql: ${packed_net_sales_usd} * 0.15 ;;
  }
  measure: assumed_cost {
    type: number
    value_format: "$#,##0.00"
    group_label: "Sales Metrics"
    sql: ${gross_rev_usd} * 0.15 * 0.8 ;;
  }

}
