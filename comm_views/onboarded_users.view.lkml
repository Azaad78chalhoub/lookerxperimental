view: onboarded_users2 {
  view_label: "Onboarded Users"
  sql_table_name: `chb-prod-stage-oracle.prod_onboarded_users.onboarded_users`
    ;;

  dimension: brand_team {
    type: string
    sql: ${TABLE}.brand_team ;;
  }

  dimension: comments {
    type: string
    sql: ${TABLE}.comments ;;
  }

  dimension: email {
    type: string
    sql: ${TABLE}.email ;;
  }

  measure: count_distinct_users {
    type: count_distinct
    sql: ${email_lower} ;;
  }

  dimension: email_lower {
    type: string
    sql: ${TABLE}.email_lower ;;
  }

  dimension: onboarded_by {
    type: string
    sql: ${TABLE}.onboarded_by ;;
  }

  dimension: onboarding_type {
    type: string
    sql: ${TABLE}.onboarding_type ;;
  }

  dimension_group: phase_1_interest {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.phase_1_interest ;;
  }

  dimension_group: phase_2_scope {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.phase_2_scope ;;
  }

  dimension_group: phase_3_access {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.phase_3_access ;;
  }

  dimension_group: phase_4_1_to_1 {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.phase_4_1_to_1 ;;
  }

  dimension_group: phase_5_catchup {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.phase_5_catchup ;;
  }

  dimension_group: phase_6_further_catchup {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.phase_6_further_catchup ;;
  }

  dimension: stream {
    type: string
    sql: ${TABLE}.stream ;;
  }

  dimension: user {
    type: string
    sql: ${TABLE}.user ;;
  }

  measure: count {
    hidden: yes
    type: count
    drill_fields: []
  }
}
