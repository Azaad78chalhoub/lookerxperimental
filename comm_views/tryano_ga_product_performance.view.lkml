view: tryano_ga_product_performance {

    sql_table_name: `chb-prod-stage-comm.prod_comm.stg_comm_tryano_ga_report_sku_performance`;;

    dimension: sku {
      type: string
      primary_key: yes
      sql: ${TABLE}.sku ;;
    }

  dimension_group: common {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    label: "Date"
    sql: ${TABLE}.date ;;
  }

    measure: product_detail_views {
      type: sum
      sql: ${TABLE}.product_detail_views ;;
    }

  measure: product_adds_to_cart {
    type: sum
    sql: ${TABLE}.product_adds_to_cart ;;
  }

  measure: quantity_added_to_cart {
    type: sum
    sql: ${TABLE}.quantity_added_to_cart ;;
  }

  measure: unique_purchases {
    type: sum
    sql: ${TABLE}.unique_purchases ;;
  }

  measure: item_quantity {
    type: sum
    sql: ${TABLE}.item_quantity ;;
  }

  measure: item_revenue {
    type: sum
    value_format_name: usd
    sql: ${TABLE}.item_revenue ;;
  }

  }
