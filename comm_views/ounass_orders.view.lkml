view: ounass_orders {
  sql_table_name: `chb-prod-data-comm.prod_commercial.ounass_orders`
    ;;
  label: "Sales"
  dimension: pk {
    primary_key: yes
    hidden: yes
    type: string
    sql:CONCAT(${order_id},${sku}) ;;
  }
  dimension: vertical_security{
    type: string
    hidden: yes
    sql: "FASHION" ;;
  }

  dimension: country_code{
    type: string
    hidden: yes
    sql: "UAE" ;;
  }


  dimension: barcode {
    type: string
    sql: ${TABLE}.barcode ;;
  }

  dimension: brand {
    type: string
    sql: ${TABLE}.brand ;;
  }

  dimension: category {
    type: string
    sql: ${TABLE}.category ;;
  }

  dimension: color {
    type: string
    sql: ${TABLE}.color ;;
  }

  dimension: currency_rate {
    type: string
    sql: ${TABLE}.currency_rate ;;
  }

  dimension: customer_id {
    type: string
    sql: ${TABLE}.customer_id ;;
  }

  dimension: discount_amount {
    hidden: yes
    type: number
    sql: CAST(${TABLE}.discount_amount AS NUMERIC);;
  }
  measure: discount_amt {
    label: "Discount Amount"
    type: sum
    sql: ${discount_amount} ;;
  }

  dimension: gender {
    type: string
    sql: ${TABLE}.gender ;;
  }

  dimension: gross_revenue_aed {
    hidden: yes
    type: number
    sql: CAST(${TABLE}.gross_revenue_aed AS NUMERIC) ;;
  }
  measure: gross_rev_aed {
    label: "Gross Revenue (AED)"
    type: sum
    sql: ${gross_revenue_aed} ;;
  }

  dimension_group: ingestion {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.ingestion_date ;;
  }

  dimension: item_name {
    type: string
    sql: ${TABLE}.item_name ;;
  }

  dimension: order_creation_date {
    type: string
    sql: ${TABLE}.order_creation_date ;;
  }

  dimension: order_id {
    type: string
    sql: ${TABLE}.order_id ;;
  }

  dimension: order_status {
    type: string
    sql: ${TABLE}.order_status ;;
  }

  dimension: order_status_seq {
    type: string
    sql: ${TABLE}.order_status_seq ;;
  }

  dimension: pricing_type {
    type: string
    sql: ${TABLE}.pricing_type ;;
  }

  dimension: reason {
    type: string
    sql: ${TABLE}.reason ;;
  }

  dimension: retail_price {
    hidden: yes
    type: number
    sql: CAST(${TABLE}.retail_price AS NUMERIC) ;;
  }
  measure: retail_amt {
    label: "Retail Price"
    type: sum
    sql: ${retail_price} ;;
  }

  dimension: retail_price_aed {
    hidden: yes
    type: number
    sql: CAST(${TABLE}.retail_price_aed AS NUMERIC) ;;
  }
  measure: retail_amt_aed {
    label: "Retail Price (AED)"
    type: sum
    sql: ${retail_price_aed} ;;
  }

  dimension: season {
    type: string
    sql: ${TABLE}.season ;;
  }

  dimension: shipment_city {
    type: string
    sql: ${TABLE}.shipment_city ;;
  }

  dimension: shipment_country {
    type: string
    sql: ${TABLE}.shipment_country ;;
  }

  dimension: size {
    type: string
    sql: ${TABLE}.size ;;
  }

  dimension: sku {
    type: string
    sql: ${TABLE}.sku ;;
  }

  dimension: sku_quantity {
    type: string
    sql: ${TABLE}.sku_quantity ;;
  }

  dimension: status_record_date {
    type: string
    sql: ${TABLE}.status_record_date ;;
  }

  dimension: vendor_id {
    type: string
    sql: ${TABLE}.vendor_id ;;
  }

  dimension: vpn {
    type: string
    sql: ${TABLE}.vpn ;;
  }

  dimension: website {
    type: string
    sql: ${TABLE}.website ;;
  }

}
