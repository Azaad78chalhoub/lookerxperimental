view: brandquad_retail_product_level {
  sql_table_name: `chb-prod-data-comm.prod_commercial.brandquad_retail_product_level`
    ;;
  drill_fields: [id]

  dimension: brand_security {
    type: string
    hidden: yes
    sql:"LEVEL SHOES";;
  }

  dimension: id {
    primary_key: yes
    type: string
    sql: ${TABLE}.id ;;
    link: {
      label: "View Product in Brandquad"
      url: "https://level.brandquad.io/catalog/item/{{value}}/attributes"
    }
  }

  dimension: abstract_level {
    type: string
    sql: ${TABLE}.abstract_level ;;
  }

  dimension: cover {
    type: string
    sql: ${TABLE}.cover ;;
    html: <img src={{value}}> ;;
  }

  dimension: infomodel {
    type: number
    sql: ${TABLE}.infomodel ;;
  }


  dimension: name {
    type: string
    sql: ${TABLE}.name ;;
  }


  dimension: parent_product {
    type: string
    sql: ${TABLE}.parent_product ;;
  }


  dimension: product_model {
    type: string
    sql: ${TABLE}.product_model ;;
  }


  dimension: progress_value {
    type: number
    value_format: "0.00%"
    sql: ${TABLE}.progress_value ;;
  }

  dimension: status {
    type: string
    sql: ${TABLE}.status ;;
  }

  dimension: taxonomy {
    type: string
    sql: ${TABLE}.taxonomy ;;
  }

  dimension: timestamp {
    type: number
    sql: ${TABLE}.timestamp ;;
  }

  dimension: type {
    type: string
    sql: ${TABLE}.type ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated ;;
  }


  dimension: version {
    type: number
    sql: ${TABLE}.version ;;
  }

###################################
### Category                    ###
###################################{

  dimension: category_depth_level0 {
    type: string
    group_label: "Category"
    label: "Level 0"
    sql: ${TABLE}.category_depth_level0 ;;
  }

  dimension: category_depth_level1 {
    type: string
    group_label: "Category"
    label: "Level 1"
    sql: ${TABLE}.category_depth_level1 ;;
  }

  dimension: category_depth_level2 {
    type: string
    group_label: "Category"
    label: "Level 2"
    sql: ${TABLE}.category_depth_level2 ;;
  }

  dimension: category_depth_level3 {
    type: string
    group_label: "Category"
    label: "Level 3"
    sql: ${TABLE}.category_depth_level3 ;;
  }

  ### End of Category
#}

###################################
### Product information         ###
###################################{

  dimension: vpn {
    group_label: "Product information"
    type: string
    sql: ${TABLE}.vpn ;;
    link: {
      label: "View Product in Brandquad"
      url: "https://level.brandquad.io/catalog/item/{{value}}/attributes"
    }
    drill_fields: [id]
  }

  dimension: brand {
    type: string
    group_label: "Product information"
    sql: ${TABLE}.brand ;;
  }

  dimension: Mutibrand_vs_concessions {
    type: string
    label: "Multibrand vs Concessions"
    group_label: "Product information"
    sql: case when
          ${zone_code} IN ('EXTERNAL CONCESSION', 'INTERNAL CONCESSION') then "Concession"
          else "Multibrand"
        end ;;
  }

  dimension: product_name {
    type: string
    group_label: "Product information"
    sql: ${TABLE}.product_name ;;
  }

  dimension: short_description {
    type: string
    group_label: "Product information"
    sql: ${TABLE}.short_description ;;
  }

  dimension: long_description {
    type: string
    group_label: "Product information"
    sql: ${TABLE}.long_description ;;
  }

  dimension: gender {
    type: string
    group_label: "Product information"
    sql: ${TABLE}.gender ;;
  }

  dimension: taxonomy_1 {
    type: string
    group_label: "Product information"
    sql: ${TABLE}.taxonomy_1 ;;
  }

  dimension: taxonomy_2 {
    type: string
    group_label: "Product information"
    sql: ${TABLE}.taxonomy_2 ;;
  }

  dimension: color {
    type: string
    group_label: "Product information"
    sql: ${TABLE}.color ;;
  }

  dimension: material {
    group_label: "Product information"
    type: string
    sql: ${TABLE}.material ;;
  }

  dimension: season {
    group_label: "Product information"
    type: string
    sql: ${TABLE}.season ;;
  }

  dimension: division {
    group_label: "Product information"
    type: string
    sql: ${TABLE}.division ;;
  }

  dimension: zone_code {
    group_label: "Product information"
    type: string
    sql: ${TABLE}.zone_code ;;
  }

  dimension: std_size {
    group_label: "Product information"
    type: string
    sql: ${TABLE}.std_size ;;
  }

  dimension: std_size_temp {
    group_label: "Product information"
    type: string
    sql: ${TABLE}.std_size_temp ;;
  }

  dimension: heel_hight {
    group_label: "Product information"
    type: string
    sql: ${TABLE}.heel_hight ;;
  }

  dimension: heel_size {
    group_label: "Product information"
    type: string
    sql: ${TABLE}.heel_size ;;
  }

  dimension: size_and_fit_information {
    group_label: "Product information"
    type: string
    sql: ${TABLE}.size_and_fit_information ;;
  }

  dimension: usage_specificity {
    group_label: "Product information"
    type: string
    sql: ${TABLE}.usage_specificity ;;
  }

  dimension: consignment_flag {
    group_label: "Product information"
    type: string
    sql: ${TABLE}.consignment_flag ;;
  }

### End of Product information
#}

###################################
### Product card information         ###
###################################{

  dimension_group: create_date_bq {
    group_label: "Product card information"
    label: "Bq Create"
    type: time
    timeframes: [
      raw,
      date,
      time
    ]
    convert_tz: no
    datatype: datetime
    sql: SAFE.parse_datetime("%d-%b-%y %T", ${TABLE}.create_date_bq) ;;
  }

  dimension: create_datetime {
    group_label: "Product card information"
    type: string
    sql: ${TABLE}.create_datetime ;;
  }

  dimension: last_update_date {
    group_label: "Product card information"
    type: string
    sql: ${TABLE}.last_update_date ;;
  }

  dimension_group: received {
    group_label: "Product card information"
    label: "Received"
    type: time
    timeframes: [
      raw,
      date,
      time
    ]
    convert_tz: no
    datatype: datetime
    sql: SAFE.parse_datetime("%d-%b-%y %T", ${TABLE}.received_date) ;;
  }

  dimension: retail_price_uae {
    group_label: "Product card information"
    type: string
    sql: ${TABLE}.retail_price_uae ;;
  }

  dimension: retail_price_usd {
    group_label: "Product card information"
    type: string
    sql: ${TABLE}.retail_price_usd ;;
  }

  dimension: oracle_store {
    group_label: "Product card information"
    type: string
    sql: ${TABLE}.oracle_store ;;
  }

  dimension: in_stock {
    group_label: "Product card information"
    type: string
    sql: ${TABLE}.in_stock ;;
  }

  dimension: return_policy {
    group_label: "Product card information"
    type: string
    sql: ${TABLE}.return_policy ;;
  }

  dimension_group: untouched_images_completed {
    group_label: "Product card information"
    label: "Untouched Images Completed"
    type: time
    timeframes: [
      raw,
      date
    ]
    convert_tz: no
    datatype: datetime
    # sql: SAFE.parse_datetime("%d-%b-%y %T", ${TABLE}.untouched_images_completed) ;;
    sql: SAFE_CAST(${TABLE}.untouched_images_completed AS DATETIME) ;;
  }

  dimension_group: product_images_completed {
    group_label: "Product card information"
    label: "Product Images Completed"
    type: time
    timeframes: [
      raw,
      date
    ]
    convert_tz: no
    datatype: datetime
    # sql: SAFE.parse_datetime("%d-%b-%y %T", ${TABLE}.untouched_images_completed) ;;
    sql: SAFE_CAST(${TABLE}.product_images_completed AS DATETIME) ;;
  }



### End of Product card information
#}

###################################
### Main information         ###
###################################{

  dimension: styleoraclenumber {
    group_label: "Main information"
    type: string
    sql: ${TABLE}.styleoraclenumber ;;
  }

  dimension: oracle_sku {
    group_label: "Main information"
    type: string
    sql: ${TABLE}.oracle_sku ;;
  }

  dimension: core_sku {
    group_label: "Main information"
    type: string
    sql: ${TABLE}.core_sku ;;
  }

  dimension: barcode {
    group_label: "Main information"
    type: string
    sql: ${TABLE}.barcode ;;
  }

  dimension: store {
    group_label: "Main information"
    type: string
    sql: ${TABLE}.store ;;
  }

  dimension: product_status {
    group_label: "Main information"
    type: string
    sql: ${TABLE}.Product_status ;;
  }


### End of Main information
#}

###################################
###   Buyer information        ###
###################################{

  dimension: supplier_colour_name {
    group_label: "Buyer information"
    type: string
    sql: ${TABLE}.supplier_colour_name ;;
  }

  dimension: country_of_origin {
    group_label: "Buyer information"
    type: string
    sql: ${TABLE}.country_of_origin ;;
  }

  dimension: origin_country_id {
    group_label: "Buyer information"
    type: string
    sql: ${TABLE}.origin_country_id ;;
  }

  dimension: hs_code {
    group_label: "Buyer information"
    type: string
    sql: ${TABLE}.hs_code ;;
  }

  dimension: supplier_colour_code {
    group_label: "Buyer information"
    type: string
    sql: ${TABLE}.supplier_colour_code ;;
  }

  dimension: supplier_size {
    group_label: "Buyer information"
    type: string
    sql: ${TABLE}.supplier_size ;;
  }

  dimension: recurrence {
    group_label: "Buyer information"
    type: string
    sql: ${TABLE}.recurrence ;;
  }

  dimension: theme {
    group_label: "Buyer information"
    type: string
    sql: ${TABLE}.theme ;;
  }


  dimension: material_composition {
    group_label: "Buyer information"
    type: string
    sql: ${TABLE}.material_composition ;;
  }

  dimension: apparel_style {
    group_label: "Buyer information"
    type: string
    sql: ${TABLE}.apparel_style ;;
  }

  dimension: supplier_class {
    group_label: "Buyer information"
    type: string
    sql: ${TABLE}.supplier_class ;;
  }

  dimension: supplier_subclass {
    group_label: "Buyer information"
    type: string
    sql: ${TABLE}.supplier_subclass ;;
  }

  dimension: supplier {
    group_label: "Buyer information"
    type: string
    sql: ${TABLE}.supplier ;;
  }

  dimension: cost_price_per_unit {
    group_label: "Buyer information"
    type: string
    sql: ${TABLE}.cost_price_per_unit ;;
  }

  dimension: currency {
    group_label: "Buyer information"
    type: string
    sql: ${TABLE}.currency ;;
  }

### End of Buyer information
#}

###################################
###   Magento information        ###
###################################{

  dimension: non_purchasable {
    group_label: "Magento information"
    type: string
    sql: ${TABLE}.non_purchasable ;;
  }

  dimension: visibility {
    group_label: "Magento information"
    type: string
    sql: ${TABLE}.visibility ;;
  }

  dimension: tax_class {
    group_label: "Magento information"
    type: string
    sql: ${TABLE}.tax_class ;;
  }

  dimension: weight {
    group_label: "Magento information"
    type: string
    sql: ${TABLE}.weight ;;
  }

  dimension: enable_rma {
    group_label: "Magento information"
    type: string
    sql: ${TABLE}.enable_rma ;;
  }

  dimension: websites {
    group_label: "Magento information"
    type: string
    sql: ${TABLE}.websites ;;
  }

  dimension: shopping_feeds {
    group_label: "Magento information"
    type: string
    sql: ${TABLE}.shopping_feeds ;;
  }

  dimension: attribute_sets {
    group_label: "Magento information"
    type: string
    sql: ${TABLE}.attribute_sets ;;
  }

### End of Magento information

#}

###################################
###   Media  information        ###
###################################{

  dimension: product_images {
    group_label: "Media information"
    type: string
    sql: ${TABLE}.product_images ;;
  }

  dimension: untouched_images {
    group_label: "Media information"
    type: string
    sql: ${TABLE}.untouched_images ;;
  }

  dimension: video {
    group_label: "Media information"
    type: string
    sql: ${TABLE}.video ;;
  }

### End of Media  information
#}


###################################
###          Measures           ###
###################################{

  measure: count_id {
    label: "# of SKUs"
    type: count
    sql: ${id} ;;
    drill_fields: [id, brand, taxonomy_1, product_name, color, std_size]
  }

  measure: count_vpn {
    label: "# of VPNs"
    type: count_distinct
    sql: ${vpn} ;;
    drill_fields: [cover, vpn, brand, taxonomy_1, product_name, color, status, create_date_bq_date, received_date, untouched_images_completed_date, product_images_completed_date]
  }

  measure: average_recieve_time {
    label: "Average Recieve Time"
    type: average
    value_format_name: decimal_2
    sql: DATE_DIFF(${received_date}, ${create_date_bq_date}, DAY) ;;
  }

  measure: average_photo_time {
    label: "Average Time to Shoot"
    type: average
    value_format_name: decimal_2
    sql: DATE_DIFF(${untouched_images_completed_date}, ${create_date_bq_date}, DAY) ;;
  }


  measure: average_edit_time {
    label: "Average Time to Edit"
    type: average
    value_format_name: decimal_2
    sql: DATE_DIFF(${product_images_completed_date}, ${untouched_images_completed_date}, DAY) ;;
  }



### End of Measures ##}

}
