view: namshi_sales {
  sql_table_name: `chb-prod-data-comm.prod_commercial.namshi_sales`
    ;;
  label: "Sales"

  dimension: vertical_security{
    type: string
    hidden: yes
    sql: "FASHION" ;;
  }

  dimension: country_code{
    type: string
    hidden: yes
    sql: "UAE" ;;
  }

  dimension: pk {
    primary_key: yes
    hidden: yes
    type: string
    sql:CONCAT(${brand},${date_date},${sku},${id_sales_order_item}) ;;
  }

  dimension: brand {
    type: string
    sql: ${TABLE}.brand ;;
  }

  dimension: category {
    type: string
    sql: ${TABLE}.category ;;
  }

  dimension: color {
    type: string
    sql: ${TABLE}.color ;;
  }

  dimension: converted_original_price {
    type: number
    hidden: yes
    sql: ${TABLE}.converted_original_price ;;
  }
  measure: original_price_converted {
    label: "Converted Original Price"
    type: sum
    sql: ${converted_original_price} ;;
    filters: [latest_update_boolean: "Yes"]
  }

  dimension: country {
    type: string
    map_layer_name: countries
    sql: ${TABLE}.country ;;
  }

  dimension: coupon_percentage {
    type: number
    hidden: yes
    sql: ${TABLE}.coupon_percentage ;;
  }
  measure: coupon_percent {
    label: "Coupon Value"
    type: sum
    sql: ${coupon_percentage} ;;
    filters: [latest_update_boolean: "Yes"]
  }

  dimension: coupon_promotion_applied {
    type: number
    sql: ${TABLE}.coupon_promotion_applied ;;
  }

  dimension_group: date {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.date ;;
  }

  dimension_group: delivered {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.delivered_date ;;
  }

  dimension: department {
    type: string
    sql: ${TABLE}.department ;;
  }

  dimension: fulfillment_wh_code {
    type: string
    sql: ${TABLE}.fulfillment_wh_code ;;
  }

  dimension: gender {
    type: string
    sql: ${TABLE}.gender ;;
  }

  dimension: id_sales_order_item {
    type: string
    sql: ${TABLE}.id_sales_order_item ;;
  }

  dimension: is_return {
    type: string
    sql: ${TABLE}.is_return ;;
  }
  dimension: latest_update {
    type: string
    sql: ${TABLE}.latest_update ;;
    hidden: yes
  }
  dimension: latest_update_boolean {
    hidden: yes
    group_label: "Statuses"
    description: "When filtered by yes; this returns the latest status a tracking # received"
    label: "Filter by latest status"
    type: yesno
    sql: ${latest_update}=1 ;;
  }

  dimension: mark_down_percent {
    type: number
    hidden: yes
    sql: ${TABLE}.mark_down_percent ;;
  }
  measure: markdown_percentage {
    label: "Markdown Value"
    type: sum
    sql: ${mark_down_percent} ;;
    filters: [latest_update_boolean: "Yes"]
  }

  dimension: net_selling_price {
    type: number
    hidden: yes
    sql: ${TABLE}.net_selling_price ;;
  }

  dimension: product_name {
    type: string
    sql: ${TABLE}.product_name ;;
  }
  dimension: gross_quantity {
    type: number
    hidden: yes
    sql: ${TABLE}.gross_quantity ;;
  }
  dimension: net_quantity {
    type: number
    hidden: yes
    sql: ${TABLE}.net_quantity ;;
  }


  dimension_group: return {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.return_date ;;
  }

  dimension_group: sales {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.sales_date ;;
  }

  dimension: season {
    type: string
    sql: ${TABLE}.season ;;
  }

  dimension: selling_price {
    type: number
    hidden: yes
    sql: ${TABLE}.selling_price ;;
  }
  measure: gross_sales_value {
    label: "Gross Sales Value"
    description: "Selling Price (with returns and cancellations)"
    type: sum
    sql: ${selling_price} ;;
    filters: [latest_update_boolean: "Yes"]
  }
  measure: sale_price {
    label: "Net Sales Value"
    description: "Selling Price (minus returns and cancellations)"
    type: sum
    sql: ${net_selling_price} ;;
    filters: [latest_update_boolean: "Yes"]
  }
  measure: net_qty {
    label: "Net Quantity"
    description: "Quantity Sold (minus returns and cancellations)"
    type: sum
    sql: ${net_quantity} ;;
    filters: [latest_update_boolean: "Yes"]
  }
  measure: gross_qty {
    label: "Gross Quantity"
    description: "Quantity Sold (with returns and cancellations)"
    type: sum
    sql: ${gross_quantity} ;;
    filters: [latest_update_boolean: "Yes"]
  }


  dimension: size {
    type: string
    sql: ${TABLE}.size ;;
  }

  dimension: sku {
    type: string
    sql: ${TABLE}.sku ;;
  }

  dimension: status_name {
    type: string
    sql: ${TABLE}.status_name ;;
  }

  dimension: subcategory {
    type: string
    sql: ${TABLE}.subcategory ;;
  }

  dimension: supplier_barcode {
    type: string
    sql: ${TABLE}.supplier_barcode ;;
  }

  dimension: supplier_color {
    type: string
    sql: ${TABLE}.supplier_color ;;
  }

  dimension: supplier_name {
    type: string
    sql: ${TABLE}.supplier_name ;;
  }

  dimension: supplier_sku {
    type: string
    sql: ${TABLE}.supplier_sku ;;
  }

  dimension: vat {
    type: number
    hidden:  yes
    sql: ${TABLE}.VAT ;;
  }
  measure: value_added_tax {
    label: "VAT"
    type: sum
    sql: ${vat} ;;
    filters: [latest_update_boolean: "Yes"]
  }

  measure: count {
    type: count
    hidden: yes
    drill_fields: [supplier_name, status_name, product_name]
  }
}
