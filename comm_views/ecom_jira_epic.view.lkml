view: ecom_jira_epic {
  sql_table_name: `chb-prod-data-comm.prod_commercial.ecom_jira_epic`
    ;;

  dimension: primary_key {
    hidden:  yes
    primary_key: yes
    sql: ${TABLE}.issue_id ;;
  }

  dimension: project_name {
    type: string
    sql: ${TABLE}.project_name ;;
  }

  dimension: epic_name {
    type: string
    sql: ${TABLE}.epic_name ;;
  }

  dimension: status {
    type: string
    sql: ${TABLE}.epic_status ;;
  }

  dimension_group: start_date {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.start_date ;;
  }

  dimension_group: due_date {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.due_date ;;
  }

  measure: count {
    type: count
    drill_fields: [epic_name, project_name]
  }
}
