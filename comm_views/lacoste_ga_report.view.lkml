view: lacoste_ga_report {
  sql_table_name: `chb-prod-stage-comm.prod_comm.stg_comm_lacoste_ga_report`
  ;;

  dimension: pk {
    type:  string
    primary_key:  yes
    sql:  concat(${ecom_site}, cast(${date_date} as string), ${device_category}, ${source}, ${medium}, ${campaign}, ${keyword}) ;; #ecom_site, date,device_category, source, medium, campaign, keyword
  }

  dimension: device_category {
    type: string
    label: "Device Category"
    sql: ${TABLE}.device_category ;;
  }

  dimension: ecom_site {
    type: string
    label: "Store Country"
    sql: ${TABLE}.ecom_site ;;
  }

  dimension: source {
    type: string
    label: "Source"
    sql: ${TABLE}.source ;;
  }

  dimension: medium {
    type: string
    label: "Medium"
    sql: ${TABLE}.medium ;;
  }

  dimension: campaign {
    type: string
    label: "Campaign"
    sql: ${TABLE}.campaign ;;
  }

  dimension: keyword {
    type: string
    label: "Keyword"
    sql: ${TABLE}.keyword ;;
  }

  dimension_group: date {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    allow_fill: no
    datatype: date
    sql: ${TABLE}.date ;;
  }

  dimension: sessions {
    type: number
    sql: ${TABLE}.session ;;
  }

  ################################################
  #############      measure       ###############
  ################################################

  measure: count {
    type: count
    drill_fields: []
  }

  measure: number_of_sessions {
    type: sum
    label: "# of Sessions"
    sql: ${TABLE}.session ;;
  }

  measure: number_of_pageviews {
    type: sum
    label: "# of Pageviews"
    sql: ${TABLE}.pageviews ;;
  }

  measure: pageviews_per_session {
    type: number
    label: "Pageviews per Session"
    value_format_name: decimal_4
    sql: ${number_of_pageviews}/NULLIF(${number_of_sessions},0) ;;
  }

  measure: session_duration {
    type: sum
    label: "Sessions duration"
    sql: ${TABLE}.session_duration ;;
  }

  measure: bounces {
    type: sum
    label: "# of Bounces"
    sql: ${TABLE}.bounces ;;
  }

  measure: bounce_rate {
    type: number
    label: "Bounce Rate %"
    value_format_name: percent_2
    sql: ${bounces}/NULLIF(${number_of_sessions},0) ;;
  }

  measure: unique_pageviews {
    type: sum
    label: "# of Unique Page views"
    sql: ${TABLE}.unique_pageviews ;;
  }

  measure: product_adds_to_cart {
    type: sum
    label: "# of Products Added to Cart"
    sql: ${TABLE}.product_adds_to_cart ;;
  }

  measure: product_checkouts {
    type: sum
    label: "# of Product Checkouts"
    sql: ${TABLE}.product_checkouts ;;
  }

  measure: number_of_users {
    type: sum
    label: "# of Users"
    sql: ${TABLE}.users ;;
  }

  measure: avg_time_on_site {
    type: number
    label: "Avg Time On Site"
    value_format_name: decimal_2
    sql: ${session_duration}/NULLIF(${number_of_sessions},0) ;;
  }

  measure: add_to_basket_rate {
    type: number
    label: "Add to Basket %"
    value_format_name: percent_2
    sql: ${product_adds_to_cart}/NULLIF(${number_of_pageviews},0) ;;
  }
  measure: cart_abandonment_rate {
    type: number
    label: "Cart Abandonment %"
    value_format_name: percent_2
    sql: (${product_adds_to_cart}-${product_checkouts})/NULLIF(${product_adds_to_cart},0) ;;
  }

  measure: checkout_to_add_rate {
    type: number
    label: "Checkout to Add to Basket %"
    value_format_name: percent_2
    sql: ${product_checkouts}/NULLIF(${product_adds_to_cart},0) ;;
  }



}
