include: "../_common/period_over_period.view"
include: "../customer_views/ecomm_customer_new_returning.view"

view: lacoste_facts_union {
  sql_table_name: `chb-prod-data-comm.prod_commercial.lacoste_facts_union`
    ;;

  dimension: brand_security{
    type: string
    hidden: yes
    sql: "LACOSTE" ;;
  }

  dimension: vertical_security{
    type: string
    hidden: yes
    sql: "MANAGED COMPANIES" ;;
  }

  extends: [period_over_period]

  dimension_group: pop_no_tz {
    sql: ${common_raw} ;;
  }

  dimension: primary_key {
    type: number
    primary_key: yes
    hidden: yes
    sql: ${TABLE}.primary_key ;;
  }

  dimension_group: common {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year,
      week_of_year,
      day_of_week,
      day_of_month
    ]
    convert_tz: no
    datatype: date
    label: "Date"
    sql: ${TABLE}.common_date ;;
  }
  dimension_group: order {
    type: time
    timeframes: [
      raw,
      second,
      minute,
      hour,
      date,
      week,
      month,
      quarter,
      year,
      week_of_year,
      day_of_week
    ]
    convert_tz: no
    datatype: datetime
    label: "Order"
    sql: ${TABLE}.order_datetime ;;
  }

  dimension: business_week_no{
    type: number
    description: "Week No. extracted from the business date"
    sql: EXTRACT( WEEK FROM(PARSE_DATE('%Y-%m-%d', CAST(${TABLE}.common_date AS STRING))))+1 ;;
  }

  # dimension: date_group {
  #   label: "Date Group"
  #   hidden: yes
  #   sql: {% if lacoste.common_date._in_query %} CAST(${TABLE}.common_date  AS DATE)
  #       {% elsif lacoste.common_month._in_query %} FORMAT_DATE('%Y-%m', ${TABLE}.common_date )
  #       {% elsif lacoste.common_quarter._in_query %} (FORMAT_TIMESTAMP('%Y-%m', TIMESTAMP_TRUNC(CAST(${TABLE}.common_date AS TIMESTAMP), QUARTER)))
  #       {% elsif lacoste.common_week._in_query %} CONCAT(CAST(EXTRACT(YEAR FROM ${TABLE}.common_date ) AS STRING),'-', CAST(EXTRACT(WEEK(SUNDAY) FROM ${TABLE}.common_date) AS STRING))
  #       {% elsif lacoste.common_year._in_query %} EXTRACT(YEAR FROM ${TABLE}.common_date )
  #       {% else %} CAST(${TABLE}.common_date  AS DATE)
  #       {% endif %} ;;
  # }


  dimension: country {
    type: string
    map_layer_name: countries
    description: "Country"
    sql: ${TABLE}.country ;;
  }

  dimension: source {
    type: string
    description: "Source can one of the following - Sales order, Google Analytics (GA) and Funnel"
    sql: ${TABLE}.source ;;
  }

  dimension: measure_label {
    label: "Measure Name"
    description: "Dummy dimensions showing measure label (used to transpose table)"
    case: {
      when: {
        label: "# of Sessions"
        sql: 1=1;;
      }
      when: {
        label: "Conversion Rate (%)"
        sql: 1=1;;
      }
      when: {
        label: "# Orders"
        sql: 1=1;;
      }
      when: {
        label: "Average Order Value USD"
        sql: 1=1;;
      }
      when: {
        label: "UPT"
        sql: 1=1;;
      }
      when: {
        label: "USP"
        sql: 1=1;;
      }
      when: {
        label: "Gross Revenue USD"
        sql: 1=1;;
      }
      when: {
        label: "Cancelled Rate (%)"
        sql: 1=1;;
      }
      when: {
        label: "Returned Rate (%)"
        sql: 1=1;;
      }
      when: {
        label: "Shipped Net (USD)"
        sql: 1=1;;
      }
      when: {
        label: "ROAS (on Gross Rev)"
        sql: 1=1;;
      }
    }
  }


###################################
### Sales Order - Dimensions ###
###################################{

  dimension: order_id {
    type: string
    label: "Sales Order ID"
    group_label: "Sales Order"
    description: "Sales Order Id"
    sql: ${TABLE}.order_id ;;
  }

  dimension: status {
    type: string
    label: "Order Status"
    description: "Sales order status"
    group_label: "Sales Order"
    sql: ${TABLE}.status ;;
  }

  dimension: cancel_status {
    type: string
    group_label: "Sales Order"
    label: "Cancel Status"
    sql: ${TABLE}.cancel_status ;;
  }

  dimension: brand {
    type: string
    hidden: yes
    sql: ${TABLE}.brand ;;
  }

########################################
### Order level Coupons - Dimensions ###
########################################{

  dimension: order_promotion_detail_coupon_code {
    type: string
    label: "Order - Promotion coupon code"
    description: "Order promotion coupon code"
    group_label: "Coupons - order level"
    sql: ${TABLE}.order_promotion_detail_coupon_code ;;
  }

  dimension: order_promotion_detail_promotion_text {
    type: string
    label: "Order - Promotion text"
    description: "Order promotion text"
    group_label: "Coupons - order level"
    sql: ${TABLE}.order_promotion_detail_promotion_text ;;
  }

  dimension: order_promotion_detail_campaign_id {
    type: string
    label: "Order - Promotion Campaign ID"
    description: "Order promotion Campaign ID"
    group_label: "Coupons - order level"
    sql: ${TABLE}.order_promotion_detail_campaign_id ;;
  }

  dimension: order_promotion_detail_discount {
    type: string
    label: "Order - Promotion discount"
    description: "Order promotion discount (Local currency)"
    group_label: "Coupons - order level"
    sql: ${TABLE}.order_promotion_detail_discount ;;
  }


########################################
### Item level Coupons - Dimensions ###
########################################{

  dimension: promotion_detail_coupon_code {
    type: string
    label: "Item - Promotion coupon code"
    description: "Item promotion coupon code"
    group_label: "Coupons - item level"
    sql: ${TABLE}.promotion_detail_coupon_code ;;
  }

  dimension: promotion_detail_promotion_text {
    type: string
    label: "Item - Promotion text"
    description: "Item promotion text"
    group_label: "Coupons - item level"
    sql: ${TABLE}.promotion_detail_promotion_text ;;
  }

  dimension: promotion_detail_campaign_id {
    type: string
    label: "Item - Promotion Campaign ID"
    description: "Item promotion Campaign ID"
    group_label: "Coupons - item level"
    sql: ${TABLE}.promotion_detail_campaign_id ;;
  }

  dimension: promotion_detail_discount {
    type: string
    label: "Item - Promotion discount"
    description: "Item promotion discount (Local currency)"
    group_label: "Coupons - item level"
    sql: ${TABLE}.promotion_detail_discount ;;
  }



  dimension: record_type {
    type: string
    sql: ${TABLE}.record_type ;;
  }

  dimension: final_record_status {
    type: string
    label: "Final Item Status"
    description: "Shows the last known status of the item on this order"
    group_label: "Sales Order Item"
    sql: ${TABLE}.final_record_status ;;
  }

  dimension_group: final_record_date {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    label: "Final Item Status Date"
    description: "Shows the last date of the known status of the item on this order"
    group_label: "Sales Order Item"
    sql: ${TABLE}.final_status_datetime ;;
  }

  dimension: sku {
    type: string
    label: "SKU"
    group_label: "Sales Order"
    description: "SKU of the Item"
    sql: ${TABLE}.sku ;;
  }

  dimension: store_id {
    type: number
    group_label: "Sales Order"
    label: "Store ID"
    sql: ${TABLE}.store_id ;;
  }

  dimension: payment_method {
    type: string
    label: "Payment Method"
    group_label: "Sales Order"
    description: "Payment method for the order / transaction"
    sql: ${TABLE}.payment_method ;;
  }

  dimension: tender_type_id {
    type: string
    label: "Payment Method ID"
    group_label: "Sales Order"
    description: "Payment method ID for the order / transaction"
    sql: ${TABLE}.payment_detail_tender_type_id ;;
  }

  dimension: price {
    type: number
    label: "Item Price (Local)"
    description: "Item price in local currency"
    group_label: "Sales Order"
    sql: ${TABLE}.price ;;
  }


  #####################################
  ##### Sales Orders
  #####################################{

  measure: all_orders {
    type: count_distinct
    group_label: "Sales Order"
    label: "# Orders"
    description: "Total number of orders placed on the website"
    sql: ${order_id} ;;
    filters: [record_type: "SALE"]
  }

  measure: units_ordered {
    type: sum
    group_label: "Sales Order"
    label: "Units Ordered"
    description: "Sum of Qty Ordered"
    sql: ${TABLE}.qty_ordered;;
    filters: [record_type: "SALE"]
  }

  measure: gross_revenue_local {
    type: sum_distinct
    group_label: "Sales Order"
    label: "Gross Revenue Local"
    description: "Total value of incoming orders on the website - Local Currency"
    value_format: "#,##0.00"
    sql: ${TABLE}.gross_revenue_local;;
    filters: [record_type: "SALE"]
  }

  measure: gross_revenue_usd {
    type: sum_distinct
    group_label: "Sales Order"
    label: "Gross Revenue USD"
    description: "Total value of incoming orders on the website - USD"
    value_format: "$#,##0.00"
    sql: ${TABLE}.gross_revenue_usd;;
    filters: [record_type: "SALE"]
  }

  measure: gross_revenue_percent {
    type: percent_of_total
    group_label: "Sales Order"
    label: "% Gross Revenue"
    description: "% of Gross Revenue in selected dimension"
    value_format_name: percent_2
    sql: ${gross_revenue_usd} ;;
  }

  measure: upt {
    type: number
    group_label: "Sales Order"
    label: "UPT"
    description: "Units per transaction = Average number of units within each order"
    value_format_name: decimal_2
    sql:${units_ordered}/nullif(${all_orders}, 0);;
  }

  measure: aov_local {
    type: number
    group_label: "Sales Order"
    label: "Average Order Value Local"
    description: "Average Value of each order - Local currency"
    value_format: "#,##0.00"
    sql: ${gross_revenue_local}/${all_orders};;
  }

  measure: aov_usd {
    type: number
    group_label: "Sales Order"
    label: "Average Order Value USD"
    description: "Average Value of each order - USD"
    value_format_name: usd
    sql: ${gross_revenue_usd}/nullif(${all_orders}, 0);;
  }

  measure: unit_selling_price {
    type:  number
    label: "USP"
    group_label: "Sales Order"
    description: "Unit Selling Price = Average price of each item ordered"
    value_format: "#,##0.00"
    sql: ${gross_revenue_usd} / NULLIF(${units_ordered},0) ;;
  }

  measure: discount_amt_local {
    type:  sum
    group_label: "Sales Order"
    label: "Discount Amount (Local)"
    description: "Discount Amount (Local currency)"
    value_format: "#,##0.00"
    sql: ${TABLE}.discount_amount ;;
  }


#####################################
##### Cancelled Orders
####################################{

  measure: count_cancelled_orders {
    type: count_distinct
    group_label: "Sales Order"
    label: "# Cancelled Orders"
    description: "Count of Cancelled Orders"
    sql: ${order_id};;
    filters: [record_type: "CANCELLED"]
  }

  measure: unit_canceled {
    type: sum
    group_label: "Sales Order"
    label: "Units Canceled"
    description: "Sum of Qty Ordered"
    sql: ${TABLE}.qty_canceled;;
  }

  measure: cancelled_order_ratio {
    type: number
    group_label: "Sales Order"
    label: "Cancelled Order %"
    description: "Percentage of incoming orders (based on numbers) that were cancelled"
    value_format_name: percent_2
    sql: ${count_cancelled_orders}/NULLIF(${all_orders},0) ;;
  }

  measure: canceled_amount_local {
    type: sum_distinct
    group_label: "Sales Order"
    label: "Cancelled amount Local"
    description: "Cancelled amount Local"
    value_format: "#,##0.00"
    sql: ${TABLE}.canceled_amount_local;;
    filters: [record_type: "CANCELLED"]
  }

  measure: canceled_amount_usd {
    type: sum_distinct
    group_label: "Sales Order"
    label: "Cancelled amount USD"
    description: "Cancelled amount USD"
    value_format: "$#,##0.00"
    sql: ${TABLE}.canceled_amount_usd;;
    filters: [record_type: "CANCELLED"]
  }


  measure: gross_revenue_minus_cancelled_local {
    type: number
    group_label: "Sales Order"
    label: "Gross Rev Minus Cancelled Local"
    description: "Gross Revenue Minus Cancelled Local"
    value_format: "#,##0.00"
    sql: ${gross_revenue_local} - ${canceled_amount_local};;
  }

  measure: gross_revenue_minus_cancelled_usd {
    type: number
    group_label: "Sales Order"
    label: "Gross Rev Minus Cancelled USD"
    description: "Gross Revenue Minus Cancelled USD"
    value_format: "$#,##0.00"
    sql: ${gross_revenue_usd} - ${canceled_amount_usd};;
  }

  measure: cancelled_item_ratio {
    type: number
    group_label: "Sales Order"
    label: "Cancelled Item %"
    description: "Cancelled Item %"
    value_format_name: percent_2
    sql: ${unit_canceled}/NULLIF(${units_ordered},0) ;;
  }

  measure: cancelled_rate {
    type: number
    group_label: "Sales Order"
    label: "Cancelled Rate (%)"
    description: "Value of orders cancelled (based on cancellation date) divided by total value of incoming orders (based on order date) (excluding failed credit card attempts)"
    value_format_name: percent_2
    sql: ${canceled_amount_usd}/nullif(${gross_revenue_usd}, 0);;
  }
  measure: number_of_partially_cancelled_orders {
    type: count_distinct
    group_label: "Sales Order"
    label: "# Partially Cancelled Orders"
    description: "Total number of partially cancelled orders"
    sql: ${order_id} ;;
    filters: [cancel_status: "Partially Cancelled"]
  }

### End of Cancelled orders measures
#}

#####################################
##### Shipped Orders
####################################{

  measure: shipped_orders {
    type: count_distinct
    group_label: "Sales Order"
    label: "# Shipped Orders"
    description: "Count of Distinct Order IDs that were shipped"
    sql: ${order_id} ;;
    filters: [record_type: "SHIPMENT"]
  }

  measure: units_shipped {
    type: sum
    group_label: "Sales Order"
    label: "Units Shipped"
    description: "Sum of Qty Shipped"
    sql: ${TABLE}.qty_shipped;;
    filters: [record_type: "SHIPMENT"]
  }

  measure: shipment_revenue_local {
    type: sum_distinct
    group_label: "Sales Order"
    label: "Shipped Revenue (Local)"
    description: "Total value of shipped orders (excluding failed credit card attempts, VAT and cancellations) - Local currency"
    value_format: "#,##0.00"
    sql: ${TABLE}.shipment_revenue_local;;
    filters: [record_type: "SHIPMENT"]
  }

  measure: shipment_revenue_usd {
    type: sum_distinct
    group_label: "Sales Order"
    label: "Shipped Revenue (USD)"
    description: "Total value of shipped orders (excluding failed credit card attempts, VAT and cancellations) - USD"
    value_format: "$#,##0.00"
    sql: ${TABLE}.shipment_revenue_usd;;
    filters: [record_type: "SHIPMENT"]
  }

  measure: shipment_revenue_minus_cancelled_local {
    type: number
    hidden: yes
    group_label: "Sales Order"
    label: "Shipped Revenue Minus Cancelled (Local)"
    description: "Shipped Revenue Minus Cancelled Local"
    value_format: "#,##0.00"
    sql: ${shipment_revenue_local} - ${canceled_amount_local};;
  }

  measure: shipment_revenue_minus_cancelled_usd {
    type: number
    hidden: yes
    group_label: "Sales Order"
    label: "Shipped Revenue Minus Cancelled (USD)"
    description: "Shipped Revenue Minus Cancelled USD"
    value_format: "$#,##0.00"
    sql: ${shipment_revenue_usd} - ${canceled_amount_usd};;
  }

### End of Shipped orders measures
#}


#####################################
##### REFUND - MEASURES ########
#####################################{

  measure: units_refunded {
    type: sum
    group_label: "Sales Order"
    label: "Units Refunded"
    description: "Sum of units Refunded"
    sql: ${TABLE}.qty_refunded;;
    filters: [record_type: "REFUND"]
  }

  measure: refund_amount_local {
    type: sum_distinct
    group_label: "Sales Order"
    label: "Refund Amount Local"
    description: "Refund Amount Local"
    value_format: "#,##0.00"
    sql: ${TABLE}.refund_amount_local;;
    filters: [record_type: "REFUND"]
  }

  measure: refund_amount_usd {
    type: sum_distinct
    group_label: "Sales Order"
    label: "Refund Amount USD"
    description: "Refund Amount USD"
    value_format: "$#,##0.00"
    sql: ${TABLE}.refund_amount_usd;;
    filters: [record_type: "REFUND"]
  }


  measure: gross_revenue_minus_cancelled_minus_returns_local {
    type: number
    group_label: "Sales Order"
    label: "Gross Rev Minus Cancelled Minus Returns Local"
    description: "Total value of incoming orders on the website excluding cancellations and returns. Based on order date - Local currency"
    value_format: "#,##0.00"
    sql: ${gross_revenue_local} - ${canceled_amount_local} - ${refund_amount_local};;
  }

  measure: gross_revenue_minus_cancelled_minus_returns_usd {
    type: number
    group_label: "Sales Order"
    label: "Gross Rev Minus Cancelled Minus Returns USD"
    description: "Total value of incoming orders on the website excluding cancellations and returns. Based on order date - USD"
    value_format: "$#,##0.00"
    sql: ${gross_revenue_usd} - ${canceled_amount_usd} - ${refund_amount_usd};;
  }

  measure: shipment_revenue_minus_cancelled_minus_returns_local {
    type: number
    hidden: yes
    group_label: "Sales Order"
    description: "Total value of incoming orders on the website excluding cancellations and returns. Based on shipping date - Local currency"
    value_format: "#,##0.00"
    sql: ${shipment_revenue_local} - ${canceled_amount_local} - ${refund_amount_local};;
  }

  measure: shipment_revenue_minus_cancelled_minus_returns_usd {
    type: number
    hidden: yes
    group_label: "Sales Order"
    description: "Total value of incoming orders on the website excluding cancellations and returns. Based on shipping date - USD"
    value_format: "$#,##0.00"
    sql: ${shipment_revenue_usd} - ${canceled_amount_usd} - ${refund_amount_usd};;
  }

  measure: shipped_net_local {
    type: number
    group_label: "Sales Order"
    label: "Shipped Net (Local)"
    description: "Total value of incoming orders on the website excluding returns. Based on shipping date - Local currency"
    value_format: "#,##0.00"
    sql: ${shipment_revenue_local} - ${refund_amount_local};;
  }

  measure: shipment_net_usd {
    type: number
    group_label: "Sales Order"
    label: "Shipped Net (USD)"
    description: "Total value of incoming orders on the website excluding returns. Based on shipping date - USD"
    value_format: "$#,##0.00"
    sql: ${shipment_revenue_usd} - ${refund_amount_usd};;
  }

  measure: return_rate {
    type: number
    group_label: "Sales Order"
    label: "Return Rate (%)"
    description: "Return Rate (%) = Refund Amount in USD / Shipment Revenue in USD [Value of orders returned (based on return date in warehouse) divided by total value of orders shipped (based on shipping date)]"
    value_format_name: percent_2
    sql: ${refund_amount_usd}/nullif(${shipment_revenue_usd}, 0);;
  }

  measure: return_item_rate {
    type: number
    group_label: "Sales Order"
    label: "Return Item (%)"
    description: "Return Item (%) = Percentage of shipped quantity that were returned out of total shipped quantity"
    value_format_name: percent_2
    sql: nullif(${units_refunded}/nullif(${units_shipped}, 0),0);;
  }

###################################
### Funnel - Dimensions ###
###################################{

  dimension: common_clicks {
    type: number
    hidden: yes
    sql: ${TABLE}.common_clicks ;;
  }

  dimension: common_cost_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.common_cost_usd ;;
  }


  dimension: common_impressions {
    type: number
    hidden: yes
    sql: ${TABLE}.common_impressions ;;
  }

###################################
###   Funnel - Measures         ###
###################################{

  measure: total_ad_spend_usd {
    type: sum
    group_label: "Funnel Metrics"
    label: "Total Ad Spend (USD)"
    description: "Total value of marketing ads spent across all different channels - USD"
    value_format: "$#,##0.00"
    sql: ${common_cost_usd} ;;
    # filters: [record_type: "SALE", source: "funnel"]
  }

  measure: roas_gross_rev {
    type: number
    group_label: "Funnel Metrics"
    label: "ROAS (on Gross Rev)"
    description: "Return on Ad Spend = The Return in Gross Sales coming from your Total Ad Spend (based on Gross Sales)"
    value_format: "#,##0.00"
    sql: ${gross_revenue_usd}/nullif(${total_ad_spend_usd}, 0) ;;
  }

  measure: roi_gross_rev {
    type: number
    group_label: "Funnel Metrics"
    label: "ROI (on Gross Rev)"
    description: "Return on Investment : The Return on Investment in Total Ad Spend in Gross Sales - USD"
    value_format: "#,##0.00"
    sql: (${gross_revenue_usd}-${total_ad_spend_usd})/nullif(${total_ad_spend_usd}, 0) ;;
  }

  measure: cost_per_order {
    type: number
    group_label: "Funnel Metrics"
    label: "CPO"
    description: "The marketing cost of one order"
    value_format: "$#,##0.00"
    sql: ${total_ad_spend_usd}/nullif(${all_orders}, 0) ;;
  }

  measure: total_clicks {
    type: sum
    group_label: "Funnel Metrics"
    label: "# Clicks"
    description: "Total number of times an ad is clicked by a user"
    sql: ${common_clicks} ;;
    # filters: [record_type: "SALE", source: "funnel"]
  }

  measure: total_impressions {
    type: sum
    group_label: "Funnel Metrics"
    label: "# Impressions"
    description: "Total number of times an ad is seen"
    sql:  ${common_impressions};;
    # filters: [record_type: "SALE", source: "funnel"]
  }

  measure: cost_per_click {
    type: number
    group_label: "Funnel Metrics"
    label: "CPC"
    description: "The marketing cost of one ad click"
    value_format_name: usd
    sql: ${total_ad_spend_usd} / NULLIF(${total_clicks},0) ;;
  }


  measure: click_through_rate {
    type: number
    group_label: "Funnel Metrics"
    label: "Click Through Rate %"
    description: "Percentage ad impressions that have led to a click"
    value_format_name: percent_2
    sql: NULLIF(${total_clicks},0) / NULLIF(${total_impressions},0);;
  }


  measure: cost_per_mill {
    type: number
    group_label: "Funnel Metrics"
    label: "CPM"
    description: "The marketing cost of 1000 impressions"
    value_format_name: usd
    sql: NULLIF(NULLIF(${total_ad_spend_usd},0) / NULLIF(${total_impressions}/1000,0),0) ;;
  }

  # measure: cost_of_acquisition{
  #   type: number
  #   value_format: "$#,##0.00"
  #   description: "The marketing cost of one acquired new customer"
  #   group_label: "Funnel Metrics"
  #   label: "Cost of Acquisition"
  #   sql: NULLIF(${total_ad_spend_usd} / NULLIF(${ecomm_customer_type_pop.new_cust_count},0),0) ;;
  # }

  ### End of Funnel Measures
  #}


#######################################
###   Gross Margin Calculation      ###
#######################################{

  measure: unit_cost_usd {
    type: sum
    group_label: "Sales Order"
    label: "Unit Cost USD"
    description: "Unit Cost USD"
    value_format_name: usd
    sql: ${TABLE}.unit_cost_usd;;
    hidden:  yes
  }

  measure: item_level_cost {
    type: sum
    group_label: "Sales Order"
    label: "Item level cost USD"
    description: "Item level cost USD"
    value_format_name: decimal_2
    sql: ${TABLE}.unit_cost_usd*${TABLE}.qty_ordered ;;
    filters: [record_type: "SALE"]
    hidden:  yes
  }

  measure: gross_margin_ratio {
    type: number
    group_label: "Sales Order Metrics"
    label: "Gross Margin (%)"
    description: "Gross margin is a company's net sales revenue minus its cost of goods sold (COGS)"
    value_format_name: percent_2
    sql: sum(case when ${unit_cost_usd} <> 0 then nullif(${gross_revenue_usd}, 0)-nullif(${units_ordered}*${unit_cost_usd}, 0) else 0 end)/sum(case when ${unit_cost_usd} <> 0 then nullif(${gross_revenue_usd}, 0) end);;
    hidden:  yes
  }

  measure: cogs_usd {
    type: number
    group_label: "Sales Order Metrics"
    value_format: "$#,##0.00"
    description: "Cost of Good Sold (USD)"
    sql: case when ${unit_cost_usd} <> 0 then ${units_ordered}*${unit_cost_usd} else 0 end;;
    hidden:  yes
  }

  measure: gross_margin_usd {
    type: number
    group_label: "Sales Order Metrics"
    label: "Gross Margin (USD)"
    value_format: "$#,##0.00"
    description: "Gross margin is a company's net sales revenue minus its cost of goods sold (COGS) in USD"
    sql: case when ${unit_cost_usd} <> 0 then nullif(${gross_revenue_usd}, 0)-nullif(${units_ordered}*${unit_cost_usd}, 0) else 0 end;;
    hidden:  yes
  }

#######################################
### Acquisition/Marketing dimensions ###
#######################################{

  dimension: acq_campaign {
    type: string
    group_label: "Acquisition / Marketing"
    description: "Acquisition campaign"
    label: "Campaign"
    sql: ${TABLE}.acq_campaign ;;
  }

  dimension: acq_device {
    type: string
    group_label: "Acquisition / Marketing"
    description: "Acquisition device"
    label: "Device"
    sql: ${TABLE}.acq_device ;;
  }

  dimension: acq_keyword {
    type: string
    group_label: "Acquisition / Marketing"
    description: "Acquisition keyword"
    label: "Keyword"
    sql: ${TABLE}.acq_keyword ;;
  }

  dimension: acq_medium {
    type: string
    group_label: "Acquisition / Marketing"
    description: "Acquisition Medium"
    label: "Medium"
    sql: ${TABLE}.acq_medium ;;
  }

  dimension: acq_source {
    type: string
    group_label: "Acquisition / Marketing"
    description: "Acquisition source"
    label: "Source"
    sql: ${TABLE}.acq_source ;;
  }

  dimension: marketing_source {
    type: string
    group_label: "Acquisition / Marketing"
    description: "Acquisition Platform"
    label: "Marketing Source"
    sql: ${TABLE}.marketing_source ;;
  }

##############################################
### End of Acquisition/Marketing dimensions ###}


##################################
#####   GA Measures          #####
##################################{

  measure: number_of_sessions {
    type: sum
    group_label: "GA Metrics"
    label: "# of Sessions"
    description: "Total number of sessions on the website"
    sql: ${TABLE}.sessions ;;
    # filters: [record_type: "SALE", source: "GA"]
  }

  measure: percent_of_sessions {
    type: percent_of_total
    group_label: "GA Metrics"
    label: "% of Total Sessions"
    description: "Percentage of sessions from a particular dimension"
    value_format_name: decimal_2
    sql: ${number_of_sessions} ;;
  }

  measure: sum_session_duration {
    type: sum
    group_label: "GA Metrics"
    label: "Sessions duration"
    description: "Sum of session duration"
    sql: ${TABLE}.session_duration ;;
    # filters: [record_type: "SALE", source: "GA"]
  }

  measure: sum_bounces {
    type: sum
    group_label: "GA Metrics"
    label: "# of Bounces"
    description: "Sum of bounces as recorded"
    sql: ${TABLE}.bounces ;;
    # filters: [record_type: "SALE", source: "GA"]
  }

  measure: number_of_pageviews {
    type: sum
    group_label: "GA Metrics"
    label: "# of Page views"
    description: "Sum of page views"
    sql: ${TABLE}.pageviews ;;
    # filters: [record_type: "SALE", source: "GA"]
  }

  measure: sum_unique_pageviews {
    type: sum
    group_label: "GA Metrics"
    label: "# of Unique Page views"
    description: "Sum of unique page views"
    sql: ${TABLE}.unique_pageviews ;;
  }

  measure: sum_product_adds_to_cart {
    type: sum
    group_label: "GA Metrics"
    label: "# of Products Added to Cart"
    description: "Total number of items added to cart for the selected dimension"
    sql: ${TABLE}.product_adds_to_cart ;;
    # filters: [record_type: "SALE", source: "GA"]
  }

  measure: sum_product_checkouts {
    type: sum
    group_label: "GA Metrics"
    label: "# of Product Checkouts"
    description: "Total number of product checkouts"
    sql: ${TABLE}.product_checkouts ;;
    # filters: [record_type: "SALE", source: "GA"]
  }

  measure: sum_users {
    type: sum
    group_label: "GA Metrics"
    label: "# of Users"
    description: "Total number of users"
    sql: ${TABLE}.users ;;
    # filters: [record_type: "SALE", source: "GA"]
  }

  measure: pageviews_per_session {
    type: number
    group_label: "GA Metrics"
    label: "Pageviews per Session"
    description: "Average number of pages viewed per session"
    value_format_name: decimal_2
    sql: ${number_of_pageviews}/NULLIF(${number_of_sessions},0) ;;
  }


  measure: bounce_rate {
    type: number
    group_label: "GA Metrics"
    label: "Bounce Rate %"
    description: "Percentage of visitors who leave the website after only one page view"
    value_format_name: percent_2
    sql: ${sum_bounces}/NULLIF(${number_of_sessions},0) ;;
  }

  measure: avg_time_on_site {
    type: number
    group_label: "GA Metrics"
    label: "Avg Time On Site"
    description: "Average of time spend on the site in seconds per session"
    value_format: "h:mm:ss"
    sql: ${sum_session_duration}/NULLIF(${number_of_sessions},0)/86400.0 ;;
  }

  measure: add_to_basket_rate {
    type: number
    group_label: "GA Metrics"
    label: "Add to Basket %"
    description: "Percentage of product detail views that have added the product to their basket over a period of time"
    value_format_name: percent_2
    sql: ${sum_product_adds_to_cart}/NULLIF(${number_of_pageviews},0) ;;
  }

  measure: cart_abandonment_rate {
    type: number
    group_label: "GA Metrics"
    label: "Cart Abandonment %"
    description: "Number of cart created that did not convert into an order (1-% cart abandonment = cart completion rate)"
    value_format_name: percent_2
    sql: (${sum_product_adds_to_cart}-${sum_product_checkouts})/NULLIF(${sum_product_adds_to_cart},0) ;;
  }

  measure: checkout_to_add_rate {
    type: number
    group_label: "GA Metrics"
    label: "Checkout to Add to Basket %"
    description: "Products checkouts / Products added to cart"
    value_format_name: percent_2
    sql: ${sum_product_checkouts}/NULLIF(${sum_product_adds_to_cart},0) ;;
  }


  measure: conversion_rate {
    type: number
    group_label: "GA Metrics"
    label: "Conversion Rate (%)"
    description: "Percentage of sessions that make an order"
    value_format_name: percent_2
    sql: ${all_orders}/NULLIF(${number_of_sessions},0) ;;
  }

  ### End of GA Measures
  #}


###############################################
##########  Affiliate Coupon Codes   ##########
###############################################

  measure: affiliate_cost {
    type: sum
    value_format_name: usd
    label: "Affiliate Cost"
    group_label: "Funnel Metrics"
    description: "Cost paid to the affiliate for order using a coupon code"
    sql: CASE WHEN ${final_record_status} not in ("REFUND", "CANCELLED") THEN ${lacoste_coupon_code_affiliates.commission_percentage}* ${TABLE}.ship_net_usd
              ELSE 0
         END;;
  }

  # measure: affiliate_cost_packed {
  #   type: sum
  #   value_format_name: usd
  #   label: "Affiliate Cost (Packed Sales)"
  #   group_label: "Funnel Metrics"
  #   description: "Cost paid to the affiliate for order using a coupon code base on packed sales minus returns"
  #   sql: CASE WHEN ${final_record_status} not in ("REFUND", "CANCELLED") THEN ${lacoste_coupon_code_affiliates.commission_percentage}* ${TABLE}.packed_sales_minus_return_usd
  #             ELSE 0
  #       END;;
  # }

  ##############################################
  ####### End of Affiliate Coupon Codes ########}




##########################
#######   ALERTS   #######
##########################

  dimension_group: record {
    type: time
    timeframes: [
      raw,
      second,
      minute,
      hour,
      date,
      week,
      month,
      quarter,
      year,
      week_of_year,
      day_of_week
    ]
    convert_tz: no
    datatype: datetime
    label: "Record"
    sql: ${TABLE}.record_datetime ;;
  }


  measure: minutes_since_last_update {
    type: min
    group_label: "Alerts"
    sql: DATETIME_DIFF(current_datetime(),${record_raw},minute)+240 ;;
  }


}
