view: akeneo_retail_product_faces {
  sql_table_name: `chb-prod-data-comm.prod_commercial.akeneo_retail_product_faces`
    ;;
  drill_fields: [id]

  dimension: brand_security {
    type: string
    hidden: yes
    sql: "FACES" ;;
  }


  dimension: id {
    primary_key: yes
    type: string
    sql: ${TABLE}.id ;;
  }

  dimension: categories {
    type: string
    sql: ${TABLE}.categories ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created ;;
  }

  dimension: enabled {
    type: yesno
    sql: ${TABLE}.enabled ;;
  }

  dimension: family {
    group_label: "Product Attributes"
    type: string
    sql: ${TABLE}.family ;;
    drill_fields: [product_attributes__sub_family]
  }

  dimension: groups_ {
    type: string
    sql: ${TABLE}.groups_ ;;
  }

  dimension: parent {
    type: string
    sql: ${TABLE}.parent ;;
  }

###################################
### Assets Attributes           ###
###################################{

  dimension: assets__product_model_images {
    type: string
    group_label: "Assets"
    label: "Product Model Images"
    sql: ${TABLE}.assets__product_model_images ;;
  }

  dimension: assets__productimages {
    type: string
    group_label: "Assets"
    label: "Product Images"
    sql: ${TABLE}.assets__productimages ;;
  }

  dimension: assets__shadethumbnail {
    type: string
    group_label: "Assets"
    label: "Shade Thumbnail"
    sql: ${TABLE}.assets__shadethumbnail ;;
  }

  ### End of Assets Attributes
  #}


###################################
### ERP Attributes              ###
###################################{

  dimension: erp__erp_axis {
    type: string
    group_label: "ERP"
    label: "ERP_AXIS"
    sql: ${TABLE}.ERP__ERP_AXIS ;;
  }

  dimension: erp__erp_barcode {
    type: string
    group_label: "ERP"
    label: "ERP_BARCODE"
    sql: ${TABLE}.ERP__ERP_BARCODE ;;
  }

  dimension: erp__erp_brand {
    type: string
    group_label: "ERP"
    label: "ERP_BRAND"
    sql: ${TABLE}.ERP__ERP_BRAND ;;
  }

  dimension: erp__erp_brand_code {
    type: string
    group_label: "ERP"
    label: "ERP_BRAND_CODE"
    sql: ${TABLE}.ERP__ERP_BRAND_CODE ;;
  }

  dimension: erp__erp_classification {
    type: string
    group_label: "ERP"
    label: "ERP_CLASSIFICATION"
    sql: ${TABLE}.ERP__ERP_CLASSIFICATION ;;
  }

  dimension: erp__erp_country_origin {
    type: string
    group_label: "ERP"
    label: "ERP_COUNTRY_ORIGIN"
    sql: ${TABLE}.ERP__ERP_COUNTRY_ORIGIN ;;
  }

  dimension: erp__erp_division {
    type: string
    group_label: "ERP"
    label: "ERP_DIVISION"
    sql: ${TABLE}.ERP__ERP_DIVISION ;;
  }

  dimension: erp__erp_gender {
    type: string
    group_label: "ERP"
    label: "ERP_GENDER"
    sql: ${TABLE}.ERP__ERP_GENDER ;;
  }

  dimension: erp__erp_gender_code {
    type: string
    group_label: "ERP"
    label: "ERP_GENDER_CODE"
    sql: ${TABLE}.ERP__ERP_GENDER_CODE ;;
  }

  dimension: erp__erp_hs_code {
    type: string
    group_label: "ERP"
    label: "ERP_HS_CODE"
    sql: ${TABLE}.ERP__ERP_HS_CODE ;;
  }

  dimension: erp__erp_last_update_datetime {
    type: string
    group_label: "ERP"
    label: "ERP_LAST_UPDATE_DATETIME"
    sql: ${TABLE}.ERP__ERP_LAST_UPDATE_DATETIME ;;
  }

  dimension: erp__erp_line {
    type: string
    group_label: "ERP"
    label: "ERP_LINE"
    sql: ${TABLE}.ERP__ERP_LINE ;;
  }

  dimension: erp__erp_market {
    type: string
    group_label: "ERP"
    label: "ERP_MARKET"
    sql: ${TABLE}.ERP__ERP_MARKET ;;
  }

  dimension: erp__erp_nature {
    type: string
    group_label: "ERP"
    label: "ERP_NATURE"
    sql: ${TABLE}.ERP__ERP_NATURE ;;
  }

  dimension: erp__erp_net_weight {
    type: string
    group_label: "ERP"
    label: "ERP_NET_WEIGHT"
    sql: ${TABLE}.ERP__ERP_NET_WEIGHT ;;
  }

  dimension: erp__erp_oracle_style {
    type: string
    group_label: "ERP"
    label: "ERP_ORACLE_STYLE"
    sql: ${TABLE}.ERP__ERP_ORACLE_STYLE ;;
  }

  dimension: erp__erp_product_name {
    type: string
    group_label: "ERP"
    label: "ERP_PRODUCT_NAME"
    sql: ${TABLE}.ERP__ERP_PRODUCT_NAME ;;
  }

  dimension: erp__erp_range {
    type: string
    group_label: "ERP"
    label: "ERP_RANGE"
    sql: ${TABLE}.ERP__ERP_RANGE ;;
  }

  dimension: erp__erp_standard_size {
    type: string
    group_label: "ERP"
    label: "ERP_STANDARD_SIZE"
    sql: ${TABLE}.ERP__ERP_STANDARD_SIZE ;;
  }

  dimension: erp__erp_sub_line {
    type: string
    group_label: "ERP"
    label: "ERP_SUB_LINE"
    sql: ${TABLE}.ERP__ERP_SUB_LINE ;;
  }

  dimension: erp__erp_taxonomy_1 {
    type: string
    group_label: "ERP"
    label: "ERP_TAXONOMY_1"
    sql: ${TABLE}.ERP__ERP_TAXONOMY_1 ;;
  }

  dimension: erp__erp_taxonomy_2 {
    type: string
    group_label: "ERP"
    label: "ERP_TAXONOMY_2"
    sql: ${TABLE}.ERP__ERP_TAXONOMY_2 ;;
  }

  dimension: erp__erp_unit_retail_aed {
    type: string
    group_label: "ERP"
    label: "ERP_UNIT_RETAIL_AED"
    sql: ${TABLE}.ERP__ERP_UNIT_RETAIL_AED ;;
  }

  dimension: erp__erp_unit_retail_egp {
    type: string
    group_label: "ERP"
    label: "ERP_UNIT_RETAIL_EGP"
    sql: ${TABLE}.ERP__ERP_UNIT_RETAIL_EGP ;;
  }

  dimension: erp__erp_unit_retail_kwd {
    type: string
    group_label: "ERP"
    label: "ERP_UNIT_RETAIL_KWD"
    sql: ${TABLE}.ERP__ERP_UNIT_RETAIL_KWD ;;
  }

  dimension: erp__erp_unit_retail_kwt {
    type: string
    group_label: "ERP"
    label: "ERP_UNIT_RETAIL_KWT"
    sql: ${TABLE}.ERP__ERP_UNIT_RETAIL_KWT ;;
  }

  dimension: erp__erp_unit_retail_qat {
    type: string
    group_label: "ERP"
    label: "ERP_UNIT_RETAIL_QAT"
    sql: ${TABLE}.ERP__ERP_UNIT_RETAIL_QAT ;;
  }

  dimension: erp__erp_unit_retail_sar {
    type: string
    group_label: "ERP"
    label: "ERP_UNIT_RETAIL_SAR"
    sql: ${TABLE}.ERP__ERP_UNIT_RETAIL_SAR ;;
  }

  dimension: erp__erp_vpn {
    type: string
    group_label: "ERP"
    label: "ERP_VPN"
    sql: ${TABLE}.ERP__ERP_VPN ;;
  }

  dimension: erp__sku {
    type: string
    group_label: "ERP"
    label: "SKU"
    sql: ${TABLE}.ERP__sku ;;
  }

    ### End of ERP Attributes
  #}


###################################
### Mobile Attributes           ###
###################################{

  dimension: mobile_attributes__mbottomresponseids {
    type: string
    group_label: "Mobile Attributes"
    label: "Mobile Bottom Response IDs"
    sql: ${TABLE}.mobile_attributes__mbottomresponseids ;;
  }

  dimension: mobile_attributes__mcenterresponseids {
    type: string
    group_label: "Mobile Attributes"
    label: "Mobile Center Response IDs"
    sql: ${TABLE}.mobile_attributes__mcenterresponseids ;;
  }

  dimension: mobile_attributes__mobilcomplimentarydescription {
    type: string
    group_label: "Mobile Attributes"
    label: "Mobile Complimentary Description"
    sql: ${TABLE}.mobile_attributes__mobilcomplimentarydescription ;;
  }

  dimension: mobile_attributes__mobilcomplimentarysubtitle {
    type: string
    group_label: "Mobile Attributes"
    label: "Mobile Complimentary Subtitle"
    sql: ${TABLE}.mobile_attributes__mobilcomplimentarysubtitle ;;
  }

  dimension: mobile_attributes__mobilcomplimentarytitle {
    type: string
    group_label: "Mobile Attributes"
    label: "Mobile Complimentary Title"
    sql: ${TABLE}.mobile_attributes__mobilcomplimentarytitle ;;
  }

  dimension: mobile_attributes__mobilheaderpromotionbold {
    type: string
    group_label: "Mobile Attributes"
    label: "Mobile Header Promotion Bold"
    sql: ${TABLE}.mobile_attributes__mobilheaderpromotionbold ;;
  }

  dimension: mobile_attributes__mobilheaderpromotionregular {
    type: string
    group_label: "Mobile Attributes"
    label: "Mobile Header Promotion Regular"
    sql: ${TABLE}.mobile_attributes__mobilheaderpromotionregular ;;
  }

  dimension: mobile_attributes__mobilsliderbadge {
    type: string
    group_label: "Mobile Attributes"
    label: "Mobile Slider Badge"
    sql: ${TABLE}.mobile_attributes__mobilsliderbadge ;;
  }

  dimension: mobile_attributes__mobilunderdescriptionbadge {
    type: string
    group_label: "Mobile Attributes"
    label: "Mobile Under Description Badge"
    sql: ${TABLE}.mobile_attributes__mobilunderdescriptionbadge ;;
  }

      ### End of Mobile Attributes
  #}

  dimension: other__product_websites {
    type: string
    group_label: "Other"
    label: "Product Websites"
    sql: ${TABLE}.other__productWebsites ;;
  }

###################################
### Product Attributes           ###
###################################{

  dimension: product_attributes__aboutthebrand {
    type: string
    group_label: "Product Attributes"
    label: "Discover the Brand"
    sql: ${TABLE}.product_attributes__aboutthebrand ;;
  }

  dimension: product_attributes__application {
    type: string
    group_label: "Product Attributes"
    label: "Application"
    sql: ${TABLE}.product_attributes__application ;;
  }

  dimension: product_attributes__badgebgcolor {
    type: string
    group_label: "Product Attributes"
    label: "Custom Badge Background Color Code"
    sql: ${TABLE}.product_attributes__badgebgcolor ;;
  }

  dimension: product_attributes__badges {
    type: string
    group_label: "Product Attributes"
    label: "Badges"
    sql: ${TABLE}.product_attributes__badges ;;
  }

  dimension: product_attributes__badgetext {
    type: string
    group_label: "Product Attributes"
    label: "Custom Badge Text"
    sql: ${TABLE}.product_attributes__badgetext ;;
  }

  dimension: product_attributes__badgetextcolor {
    type: string
    group_label: "Product Attributes"
    label: "Custom Badge Text Color Code"
    sql: ${TABLE}.product_attributes__badgetextcolor ;;
  }

  dimension: product_attributes__brand {
    type: string
    group_label: "Product Attributes"
    label: "Brand"
    sql: ${TABLE}.product_attributes__brand ;;
  }

  dimension: product_attributes__brandtext {
    type: string
    group_label: "Product Attributes"
    label: "Brand (text)"
    sql: ${TABLE}.product_attributes__brandtext ;;
  }

  dimension: product_attributes__colorgroup {
    type: string
    group_label: "Product Attributes"
    label: "Color Group"
    sql: ${TABLE}.product_attributes__colorgroup ;;
  }

  dimension: product_attributes__description {
    type: string
    group_label: "Product Attributes"
    label: "Description"
    sql: ${TABLE}.product_attributes__description ;;
  }

  dimension: product_attributes__discount {
    type: string
    group_label: "Product Attributes"
    label: "Discount"
    sql: ${TABLE}.product_attributes__discount ;;
  }

  dimension: product_attributes__gender {
    type: string
    group_label: "Product Attributes"
    label: "Gender"
    sql: ${TABLE}.product_attributes__gender ;;
  }

  dimension: product_attributes__guarantee {
    type: string
    group_label: "Product Attributes"
    label: "Guaranteed 100% authentic"
    sql: ${TABLE}.product_attributes__guarantee ;;
  }

  dimension: product_attributes__ingredients {
    type: string
    group_label: "Product Attributes"
    label: "Ingredients"
    sql: ${TABLE}.product_attributes__ingredients ;;
  }

  dimension: product_attributes__maxorderquantity {
    type: string
    group_label: "Product Attributes"
    label: "Maximum Order Quantity"
    sql: ${TABLE}.product_attributes__maxorderquantity ;;
  }

  dimension: product_attributes__minorderquantity {
    type: string
    group_label: "Product Attributes"
    label: "Minimum Order Quantity"
    sql: ${TABLE}.product_attributes__minorderquantity ;;
  }

  dimension: product_attributes__name {
    type: string
    group_label: "Product Attributes"
    label: "Name"
    sql: ${TABLE}.product_attributes__name ;;
  }

  dimension: product_attributes__product_type {
    type: string
    group_label: "Product Attributes"
    label: "Product Type"
    sql: ${TABLE}.product_attributes__product_type ;;
  }

  dimension: product_attributes__productnameeng {
    type: string
    group_label: "Product Attributes"
    label: "Product URL Name"
    sql: ${TABLE}.product_attributes__productnameeng ;;
  }

  dimension: product_attributes__safetytreshold {
    type: string
    group_label: "Product Attributes"
    label: "Inventory Safety Treshold"
    sql: ${TABLE}.product_attributes__safetytreshold ;;
  }

  dimension: product_attributes__secondname {
    type: string
    group_label: "Product Attributes"
    label: "Second Name"
    sql: ${TABLE}.product_attributes__secondname ;;
  }

  dimension: product_attributes__shade {
    type: string
    group_label: "Product Attributes"
    label: "Shade"
    sql: ${TABLE}.product_attributes__shade ;;
  }

  dimension: product_attributes__shortdescription {
    type: string
    group_label: "Product Attributes"
    label: "Short Description"
    sql: ${TABLE}.product_attributes__shortdescription ;;
  }

  dimension: product_attributes__size {
    type: string
    group_label: "Product Attributes"
    label: "Size"
    sql: ${TABLE}.product_attributes__size ;;
  }

  dimension: product_attributes__sub_family {
    type: string
    group_label: "Product Attributes"
    label: "Sub Family"
    sql: ${TABLE}.product_attributes__sub_family ;;
  }

  dimension: product_attributes__tabs {
    type: string
    group_label: "Product Attributes"
    label: "Tabs"
    sql: ${TABLE}.product_attributes__tabs ;;
  }

  dimension: product_attributes__taxclassid {
    type: string
    group_label: "Product Attributes"
    label: "Tax Class ID"
    sql: ${TABLE}.product_attributes__taxclassid ;;
  }

  dimension: product_attributes__worth {
    type: string
    group_label: "Product Attributes"
    label: "Product Worth"
    sql: ${TABLE}.product_attributes__worth ;;
  }

      ### End of Product Attributes
  #}

  ###################################
### Product Specs           ###
###################################{

  dimension: product_specifications__benefits {
    type: string
    group_label: "Product Specifications"
    label: "Benefits"
    sql: ${TABLE}.productSpecifications__benefits ;;
  }

  dimension: product_specifications__concentration {
    type: string
    group_label: "Product Specifications"
    label: "Concentration"
    sql: ${TABLE}.productSpecifications__concentration ;;
  }

  dimension: product_specifications__coverage {
    type: string
    group_label: "Product Specifications"
    label: "Coverage"
    sql: ${TABLE}.productSpecifications__coverage ;;
  }

  dimension: product_specifications__finish {
    type: string
    group_label: "Product Specifications"
    label: "Finish"
    sql: ${TABLE}.productSpecifications__finish ;;
  }

  dimension: product_specifications__formulation {
    type: string
    group_label: "Product Specifications"
    label: "Formulation"
    sql: ${TABLE}.productSpecifications__formulation ;;
  }

  dimension: product_specifications__fragrancefamily {
    type: string
    group_label: "Product Specifications"
    label: "Fragrance Family"
    sql: ${TABLE}.productSpecifications__fragrancefamily ;;
  }

  dimension: product_specifications__hairconcern {
    type: string
    group_label: "Product Specifications"
    label: "Hair Concern"
    sql: ${TABLE}.productSpecifications__hairconcern ;;
  }

  dimension: product_specifications__hairtype {
    type: string
    group_label: "Product Specifications"
    label: "Hair Type"
    sql: ${TABLE}.productSpecifications__hairtype ;;
  }

  dimension: product_specifications__keyingredients {
    type: string
    group_label: "Product Specifications"
    label: "Key Ingredients"
    sql: ${TABLE}.productSpecifications__keyingredients ;;
  }

  dimension: product_specifications__keynotes {
    type: string
    group_label: "Product Specifications"
    label: "Key Notes"
    sql: ${TABLE}.productSpecifications__keynotes ;;
  }

  dimension: product_specifications__skinconcern {
    type: string
    group_label: "Product Specifications"
    label: "Skin Concern"
    sql: ${TABLE}.productSpecifications__skinconcern ;;
  }

  dimension: product_specifications__skintype {
    type: string
    group_label: "Product Specifications"
    label: "Skin type"
    sql: ${TABLE}.productSpecifications__skintype ;;
  }

  dimension: product_specifications__spftype {
    type: string
    group_label: "Product Specifications"
    label: "SPF Type"
    sql: ${TABLE}.productSpecifications__spftype ;;
  }

  ### End of Product Sepcs
  #}

###################################
### Product Status              ###
###################################{

  dimension: product_status__backinstocknotification {
    type: string
    group_label: "Product Status"
    label: "Back In Stock Notification"
    sql: ${TABLE}.productStatus__backinstocknotification ;;
  }

  dimension: product_status__egyonlineflag {
    type: string
    group_label: "Product Status"
    label: "Online Flag EGY"
    sql: ${TABLE}.productStatus__egyonlineflag ;;
  }

  dimension: product_status__erp_product_websites {
    type: string
    group_label: "Product Status"
    label: "ERP_PRODUCT_WEBSITES"
    sql: ${TABLE}.productStatus__ERP_PRODUCT_WEBSITES ;;
  }

  dimension: product_status__isenabled {
    type: string
    group_label: "Product Status"
    label: "Is Enabled"
    sql: ${TABLE}.productStatus__isenabled ;;
  }

  dimension: product_status__ksaonlineflag {
    type: string
    group_label: "Product Status"
    label: "Online Flag KSA"
    sql: ${TABLE}.productStatus__ksaonlineflag ;;
  }

  dimension: product_status__kwtonlineflag {
    type: string
    group_label: "Product Status"
    label: "Online Flag KWT"
    sql: ${TABLE}.productStatus__kwtonlineflag ;;
  }

  dimension: product_status__onlinefrom {
    type: string
    group_label: "Product Status"
    label: "Online From"
    sql: ${TABLE}.productStatus__onlinefrom ;;
  }

  dimension: product_status__onlineto {
    type: string
    group_label: "Product Status"
    label: "Online To"
    sql: ${TABLE}.productStatus__onlineto ;;
  }

  dimension: product_status__qatonlineflag {
    type: string
    group_label: "Product Status"
    label: "Online Flag QAT"
    sql: ${TABLE}.productStatus__qatonlineflag ;;
  }

  dimension: product_status__sellable {
    type: string
    group_label: "Product Status"
    label: "Sellable"
    sql: ${TABLE}.productStatus__sellable ;;
  }

  dimension: product_status__uaeonlineflag {
    type: string
    group_label: "Product Status"
    label: "Online Flag UAE"
    sql: ${TABLE}.productStatus__uaeonlineflag ;;
  }

  ### End of Product Status
  #}

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated ;;
  }

  measure: count {
    type: count
    hidden: yes
    drill_fields: [id, product_attributes__name, product_attributes__secondname, erp__erp_product_name]
  }
}
