include: "../_common/period_over_period.view"
view: commercial_unified_facts_union {
  sql_table_name: `chb-prod-data-comm.prod_commercial.commercial_unified_facts_union`
    ;;
view_label: "Group Level Data"

  dimension: pk {
    type: string
    hidden: yes
    sql: ${TABLE}.primary_key ;;
    primary_key: yes
  }

  dimension: date_group {
    label: "Date Group"
    hidden: yes
    sql: {% if commercial_unified_facts_union.common_date._in_query %} CAST(${TABLE}.common_date  AS DATE)
      {% elsif commercial_unified_facts_union.common_month._in_query %} FORMAT_DATE('%Y-%m', ${TABLE}.common_date )
      {% elsif commercial_unified_facts_union.common_quarter._in_query %} (FORMAT_TIMESTAMP('%Y-%m', TIMESTAMP_TRUNC(CAST(${TABLE}.common_date AS TIMESTAMP), QUARTER)))
      {% elsif commercial_unified_facts_union.common_week._in_query %} CONCAT(CAST(EXTRACT(YEAR FROM ${TABLE}.common_date ) AS STRING),'-', CAST(EXTRACT(WEEK(SUNDAY) FROM ${TABLE}.common_date) AS STRING))
      {% elsif commercial_unified_facts_union.common_year._in_query %} EXTRACT(YEAR FROM ${TABLE}.common_date )
      {% else %} CAST(${TABLE}.common_date  AS DATE)
      {% endif %} ;;
  }

  dimension: vertical_security {
    type: string
    hidden: yes
    sql: ${vertical} ;;
  }

  dimension: brand_security {
    type: string
    hidden: yes
    sql: ${TABLE}.brand ;;
  }

  dimension: acq_campaign {
    group_label: "Acquisition/Marketing"
    type: string
    sql: ${TABLE}.acq_campaign ;;
  }

  dimension: acq_device {
    group_label: "Acquisition/Marketing"
    type: string
    sql: ${TABLE}.acq_device ;;
  }

  dimension: acq_channel {
    group_label: "Acquisition/Marketing"
    type: string
    sql: ${TABLE}.acq_channel ;;
  }

  dimension: marketing_source {
    group_label: "Acquisition/Marketing"
    label: "Marketing Source"
    type: string
    sql: ${TABLE}.marketing_source ;;
  }

  dimension: acq_keyword {
    group_label: "Acquisition/Marketing"
    type: string
    sql: ${TABLE}.acq_keyword ;;
  }

  dimension: acq_medium {
    group_label: "Acquisition/Marketing"
    type: string
    sql: ${TABLE}.acq_medium ;;
  }

  dimension: acq_source {
    group_label: "Acquisition/Marketing"
    type: string
    sql: ${TABLE}.acq_source ;;
  }

  dimension: acq_country {
    group_label: "Acquisition/Marketing"
    type: string
    sql: ${TABLE}.acq_country ;;
  }

  dimension: amount_refunded {
    type: number
    hidden: yes
    sql: ${TABLE}.amount_refunded ;;
  }

  dimension: barcode {
    group_label: "Sales Order Item"
    type: string
    sql: ${TABLE}.barcode ;;
  }

  dimension: bounces {
    type: number
    hidden: yes
    sql: ${TABLE}.bounces ;;
  }

  dimension: brand {
    type: string
    sql: ${TABLE}.brand ;;
  }
  dimension: cancel_amount_local {
    type: number
    hidden: yes
    sql: ${TABLE}.cancel_amount_local ;;
  }
  dimension: cancel_amount_aed {
    type: number
    hidden: yes
    sql: ${TABLE}.cancel_amount_aed ;;
  }
  dimension: cancel_amount_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.cancel_amount_usd ;;
  }
  dimension: common_clicks {
    type: number
    hidden: yes
    sql: ${TABLE}.common_clicks ;;
  }

  dimension: common_cost {
    type: number
    hidden: yes
    sql: ${TABLE}.common_cost ;;
  }

  dimension: common_cost_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.common_cost_usd ;;
  }

  dimension_group: common {
    type: time
    timeframes: [
      raw,
      date,
      day_of_month,
      week,
      month,
      quarter,
      year,
      month_name,
      month_num,
      week_of_year,
      day_of_week_index,
      day_of_week
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.common_date ;;
  }
  dimension_group: common_datetime {
    type: time
    timeframes: [
      raw,
      time,
      date,
      day_of_month,
      week,
      month,
      quarter,
      year,
      month_name,
      month_num,
      week_of_year,
      day_of_week_index,
      day_of_week
    ]
    sql: CAST(${TABLE}.common_datetime AS TIMESTAMP) ;;
  }

  dimension: common_impressions {
    type: number
    hidden: yes
    sql: ${TABLE}.common_impressions ;;
  }

  dimension: coupon_code {
    group_label: "Sales Order"
    type: string
    sql: ${TABLE}.coupon_code ;;
  }

  dimension: customer_email {
    group_label: "Sales Order"
    type: string
    hidden: yes
    sql: ${TABLE}.customer_email ;;
  }

  dimension: customer_phonenumber {
    group_label: "Sales Order"
    type: string
    hidden: yes
    sql: ${TABLE}.customer_phonenumber ;;
  }

  dimension: delivery_city {
    group_label: "Sales Order Shipment"
    type: string
    sql: ${TABLE}.delivery_city ;;
  }

  dimension: delivery_country {
    group_label: "Sales Order Shipment"
    type: string
    sql: ${TABLE}.delivery_country ;;
  }

  dimension: delivery_type {
    group_label: "Sales Order Shipment"
    type: string
    sql: ${TABLE}.delivery_type ;;
  }

  dimension: detail_brand {
    group_label: "Sales Order Item"
    type: string
    sql: ${TABLE}.detail_brand ;;
  }
  dimension: detail_sku_code {
    group_label: "Sales Order Item"
    type: string
    sql: ${TABLE}.detail_sku_code ;;
  }
  dimension: detail_id {
    group_label: "Sales Order Item"
    type: string
    sql: ${TABLE}.detail_id ;;
  }
  dimension: detail_external_id {
    group_label: "Sales Order Item"
    type: string
    sql: ${TABLE}.detail_external_id ;;
  }

  dimension: detail_price_original {
    type: number
    hidden: yes
    sql: ${TABLE}.detail_price_original ;;
  }
  dimension: detail_price_original_aed {
    type: number
    hidden: yes
    sql: ${TABLE}.detail_price_original_aed ;;
  }

  dimension: detail_tax {
    type: number
    hidden: yes
    sql: ${TABLE}.detail_tax ;;
  }

  dimension: detail_tax_amount {
    type: number
    hidden: yes
    sql: ${TABLE}.detail_tax_amount ;;
  }
  dimension: detail_tax_amount_aed {
    type: number
    hidden: yes
    sql: ${TABLE}.detail_tax_amount_aed ;;
  }

  dimension: discount_amount {
    type: number
    hidden: yes
    sql: ${TABLE}.discount_amount ;;
  }

  dimension: discount_amount_aed {
    type: number
    hidden: yes
    sql: ${TABLE}.discount_amount_aed ;;
  }

  dimension: discount_refunded {
    type: number
    hidden: yes
    sql: ${TABLE}.discount_refunded ;;
  }
  dimension: discount_refunded_aed {
    type: number
    hidden: yes
    sql: ${TABLE}.discount_refunded_aed ;;
  }

  dimension: cancellation_reason {
    type: string
    label: "Internal Item Cancel Reason"
    group_label: "Sales Order Item"
    sql: ${TABLE}.cancellation_reason ;;
  }

  dimension: employee_id {
    group_label: "Sales Order"
    type: string
    sql: ${TABLE}.employee_id ;;
  }

  dimension: gross_minus_cancel_local {
    type: number
    hidden: yes
    sql: ${TABLE}.gross_minus_cancel_local ;;
  }

  dimension: gross_minus_cancel_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.gross_minus_cancel_usd ;;
  }

  dimension: gross_minus_cancel_aed {
    type: number
    hidden: yes
    sql: ${TABLE}.gross_minus_cancel_aed ;;
  }

  dimension: gross_net_local {
    type: number
    hidden: yes
    sql: ${TABLE}.gross_net_sales_local ;;
  }

  dimension: gross_net_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.gross_net_sales_usd ;;
  }

  dimension: gross_net_aed {
    type: number
    hidden: yes
    sql: ${TABLE}.gross_net_sales_aed ;;
  }

  dimension: gross_revenue_local {
    type: number
    hidden: yes
    sql: ${TABLE}.gross_revenue_amount_local ;;
  }

  dimension: gross_revenue_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.gross_revenue_amount_usd ;;
  }

  dimension: gross_revenue_aed {
    type: number
    hidden: yes
    sql: ${TABLE}.gross_revenue_amount_aed ;;
  }

  dimension: order_currency {
    group_label: "Sales Order"
    type: string
    sql: ${TABLE}.order_currency ;;
  }

  dimension: order_custom_duty_fee {
    type: number
    hidden: yes
    sql: ${TABLE}.order_custom_duty_fee ;;
  }
  dimension: order_custom_duty_fee_aed {
    type: number
    hidden: yes
    sql: ${TABLE}.order_custom_duty_fee_aed ;;
  }

  dimension_group: order {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.order_date ;;
  }

  dimension_group: order_datetime {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.order_datetime AS TIMESTAMP) ;;
  }

  dimension: order_id {
    group_label: "Sales Order"
    type: string
    sql: ${TABLE}.order_id ;;
  }

  dimension: order_payment_fee {
    type: number
    hidden: yes
    sql: ${TABLE}.order_payment_fee ;;
  }
  dimension: order_payment_fee_aed {
    type: number
    hidden: yes
    sql: ${TABLE}.order_payment_fee_aed ;;
  }

  dimension: order_shipping {
    type: number
    hidden: yes
    sql: ${TABLE}.order_shipping ;;
  }
  dimension: order_shipping_aed {
    type: number
    hidden: yes
    sql: ${TABLE}.order_shipping_aed ;;
  }

  dimension: order_total {
    type: number
    hidden: yes
    sql: ${TABLE}.order_total ;;
  }

  dimension: order_total_aed {
    type: number
    hidden: yes
    sql: ${TABLE}.order_total_aed ;;
  }

  dimension: packed_revenue_local {
    type: number
    hidden: yes
    sql: ${TABLE}.packed_revenue_local ;;
  }

  dimension: packed_revenue_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.packed_revenue_usd ;;
  }
  dimension: packed_revenue_aed {
    type: number
    hidden: yes
    sql: ${TABLE}.packed_revenue_aed ;;
  }

  dimension: packed_sales_minus_return_local {
    type: number
    hidden: yes
    sql: ${TABLE}.packed_sales_minus_return_local ;;
  }
  dimension: packed_sales_minus_return_aed {
    type: number
    hidden: yes
    sql: ${TABLE}.packed_sales_minus_return_aed ;;
  }

  dimension: packed_sales_minus_return_local_incl_tax {
    type: number
    hidden: yes
    sql: ${TABLE}.packed_sales_minus_return_local_incl_tax ;;
  }
  dimension: packed_sales_minus_return_aed_incl_tax {
    type: number
    hidden: yes
    sql: ${TABLE}.packed_sales_minus_return_aed_incl_tax ;;
  }

  dimension: packed_sales_minus_return_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.packed_sales_minus_return_usd ;;
  }

  dimension: packed_sales_minus_return_usd_incl_tax {
    type: number
    hidden: yes
    sql: ${TABLE}.packed_sales_minus_return_usd_incl_tax ;;
  }

  dimension: pageviews {
    type: number
    hidden: yes
    sql: ${TABLE}.pageviews ;;
  }

  dimension: parcel_id {
    group_label: "Sales Order Shipment"
    type: string
    sql: ${TABLE}.parcel_id ;;
  }

  dimension: payment_method {
    group_label: "Sales Order"
    type: string
    sql: ${TABLE}.payment_method ;;
  }

  dimension: payment_status {
    group_label: "Sales Order"
    type: string
    sql: ${TABLE}.payment_status ;;
  }

  dimension: price {
    type: number
    hidden: yes
    sql: ${TABLE}.price ;;
  }

  dimension: product_adds_to_cart {
    type: number
    hidden: yes
    sql: ${TABLE}.product_adds_to_cart ;;
  }

  dimension: product_checkouts {
    type: number
    hidden: yes
    sql: ${TABLE}.product_checkouts ;;
  }

  dimension: products_in_order {
    hidden: yes
    type: string
    sql: ${TABLE}.products_in_order ;;
  }

  dimension: products_in_parcel {
    hidden: yes
    type: string
    sql: ${TABLE}.products_in_parcel ;;
  }

  dimension: qty_canceled {
    type: number
    hidden: yes
    sql: ${TABLE}.qty_canceled ;;
  }

  dimension: qty_ordered {
    type: number
    hidden: yes
    sql: ${TABLE}.qty_ordered ;;
  }

  dimension: qty_packed {
    type: number
    hidden: yes
    sql: ${TABLE}.qty_packed ;;
  }

  dimension: qty_refunded {
    type: number
    hidden: yes
    sql: ${TABLE}.qty_returned ;;
  }

  dimension: qty_shipped {
    type: number
    hidden: yes
    sql: ${TABLE}.qty_shipped ;;
  }

  dimension_group: record {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.record_date ;;
  }

  dimension_group: record_datetime {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.record_datetime AS TIMESTAMP) ;;
  }

  dimension: business_week_no{
    type: number
    description: "Week No. extracted from the business date"
    sql: EXTRACT( WEEK FROM(PARSE_DATE('%Y-%m-%d', CAST(${TABLE}.record_datetime AS STRING))))+1 ;;
  }

  extends: [period_over_period]
  dimension_group: pop_no_tz {
    sql: ${record_date} ;;
  }

  dimension: record_type {
    group_label: "Sales Order Item"
    type: string
    sql: ${TABLE}.record_type ;;
  }

  dimension: refund_amount_local {
    type: number
    hidden: yes
    sql: ${TABLE}.refund_amount_local ;;
  }

  dimension: refund_amount_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.refund_amount_usd ;;
  }
  dimension: refund_amount_aed {
    type: number
    hidden: yes
    sql: ${TABLE}.refund_amount_aed ;;
  }

  dimension: retailunity_order_id {
    group_label: "Sales Order"
    type: string
    sql: ${TABLE}.retailunity_order_id ;;
  }

  dimension: return_cancel_reason {
    group_label: "Sales Order Item"
    type: string
    sql: ${TABLE}.return_cancel_reason ;;
  }

  dimension: return_location_id {
    group_label: "Sales Order Item"
    type: string
    sql: ${TABLE}.return_location_id ;;
  }

  dimension: return_location_name {
    group_label: "Sales Order Item"
    type: string
    sql: ${TABLE}.return_location_name ;;
  }
  dimension: sales_channel {
    group_label: "Sales Order"
    type: string
    sql: ${TABLE}.sales_channel ;;
  }

  dimension: sales_origin_id {
    type: number
    hidden: yes
    sql: ${TABLE}.sales_origin_id ;;
  }
  dimension: sales_origin_name {
    type: number
    hidden: yes
    sql: ${TABLE}.sales_origin_name ;;
  }

  dimension: session_duration {
    type: number
    hidden: yes
    sql: ${TABLE}.session_duration ;;
  }

  dimension: sessions {
    type: number
    hidden: yes
    sql: ${TABLE}.sessions ;;
  }

  dimension: ship_net_local {
    type: number
    hidden: yes
    sql: ${TABLE}.shipment_net_sales_local ;;
  }

  dimension: ship_net_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.shipment_net_sales_usd ;;
  }

  dimension: ship_net_aed {
    type: number
    hidden: yes
    sql: ${TABLE}.shipment_net_sales_aed;;
  }

  dimension: shipment_revenue_local {
    type: number
    hidden: yes
    sql: ${TABLE}.shipment_revenue_amount_local ;;
  }

  dimension: shipment_revenue_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.shipment_revenue_amount_usd ;;
  }
  dimension: shipment_revenue_aed {
    type: number
    hidden: yes
    sql: ${TABLE}.shipment_revenue_amount_aed ;;
  }

  dimension: shipping_location_id {
    group_label: "Sales Order Shipment"
    type: string
    sql: ${TABLE}.shipping_location_id ;;
  }

  dimension: shipping_location_name {
    group_label: "Sales Order Shipment"
    type: string
    sql: ${TABLE}.shipping_location_name ;;
  }

  dimension: sku {
    group_label: "Sales Order Item"
    type: string
    sql: ${TABLE}.sku ;;
  }

  dimension: source {
    label: "Data Source"
    type: string
    sql: ${TABLE}.source ;;
  }

  dimension: status {
    group_label: "Sales Order Item"
    type: string
    sql: ${TABLE}.status ;;
  }

  dimension: store_country {
    group_label: "Sales Order"
    type: string
    sql: ${TABLE}.store_country ;;
  }

  dimension: store_id {
    group_label: "Sales Order"
    type: number
    sql: ${TABLE}.store_id ;;
  }

  dimension: tracking_id {
    group_label: "Sales Order Shipment"
    type: string
    sql: ${TABLE}.tracking_id ;;
  }

  dimension: unique_pageviews {
    type: number
    hidden: yes
    sql: ${TABLE}.unique_pageviews ;;
  }

  dimension: users {
    type: number
    hidden: yes
    sql: ${TABLE}.users ;;
  }

  dimension: vertical {
    type: string
    sql: ${TABLE}.vertical ;;
  }

###################################
##     Sales Order Measures      ##
###################################{


##################### SALE #####################


  measure: count_sales_orders {
    type: count_distinct
    group_label: "Sales Metrics"
    label: "# of Sales Orders"
    description: "Total number of orders placed on the website"
    sql: ${order_id};;
    filters: [record_type: "SALE"]
  }

  measure: units_ordered {
    type: sum
    group_label: "Sales Metrics"
    label: "Units Ordered"
    description: "Total number of items ordered on the website"
    sql: ${qty_ordered};;
  }

  measure: aov_usd {
    type: number
    group_label: "Sales Metrics"
    label: "Average Order Value (USD)"
    description: "Average Value of each order in USD"
    value_format: "$#,##0.00"
    sql:${gross_rev_usd}/nullif(${count_sales_orders}, 0);;
  }

  measure: aov_local {
    type: number
    group_label: "Sales Metrics"
    label: "Average Order Value (AED)"
    description: "Average Value of each order in local currency"
    value_format: "#,##0.00"
    sql: ${gross_rev_local}/nullif(${count_sales_orders}, 0);;
  }

  measure: gross_net_local_measure {
    type: sum
    group_label: "Sales Metrics"
    label: "Gross Revenue w/o cancellations & returns (AED)"
    description: "In AED"
    value_format: "#,##0.00"
    sql: ${gross_net_local};;
  }

  measure: gross_net_usd_measure {
    type: sum
    group_label: "Sales Metrics"
    label: "Gross Revenue w/o cancellations & returns (USD)"
    description: "Total value of incoming orders on the website excluding cancellations and returns (excluding failed credit card attempts, VAT, customs and shipping fees and COD fees). To be computed based on order date"
    value_format: "$#,##0.00"
    sql:${gross_net_usd};;
  }

  measure: gross_minus_cancelled_local {
    type: sum
    group_label: "Sales Metrics"
    label: "Gross Revenue w/o cancellations (AED)"
    description: "Total value of incoming orders on the website excluding cancellations (excluding failed credit card attempts, VAT, customs and shipping fees and COD fees). To be computed based on order date"
    value_format: "#,##0.00"
    sql: ${gross_minus_cancel_local};;
  }

  measure: gross_minus_cancelled_usd {
    type: sum
    group_label: "Sales Metrics"
    label: "Gross Revenue w/o cancellations (USD)"
    description: "Total value of incoming orders on the website excluding cancellations (excluding failed credit card attempts, VAT, customs and shipping fees and COD fees). To be computed based on order date"
    value_format: "$#,##0.00"
    sql: ${gross_minus_cancel_usd};;
  }

  measure: gross_rev_local {
    type: sum
    group_label: "Sales Metrics"
    label: "Gross Revenue (AED)"
    description: "Total value of incoming orders on the website in AED (excluding VAT, discount, customs and shipping fees and COD fees)"
    value_format: "#,##0.00"
    sql:  ${gross_revenue_local};;
  }

  measure: gross_rev_usd {
    type: sum
    group_label: "Sales Metrics"
    label: "Gross Revenue (USD)"
    description: "Total value of incoming orders on the website (excluding VAT, discount, customs and shipping fees and COD fees)"
    value_format: "$#,##0.00"
    sql: ${gross_revenue_usd};;
  }

  measure: upt {
    type: number
    group_label: "Sales Metrics"
    label: "UPT"
    description: "Average number of units within each order"
    value_format_name: decimal_2
    sql:${units_ordered}/nullif(${count_sales_orders}, 0);;
  }

  measure: usp {
    type:  number
    label: "USP (USD)"
    group_label: "Sales Metrics"
    description: "Unit Selling Price = Average price of each item ordered (in USD)"
    value_format: "$#,##0.00"
    sql: ${gross_rev_usd} / NULLIF(${units_ordered},0) ;;
  }

  measure: usp_local {
    type:  number
    label: "USP (AED)"
    group_label: "Sales Metrics"
    description: "Unit Selling Price = Average price of each item ordered (in AED)"
    value_format: "#,##0.00"
    sql: ${gross_rev_local} / NULLIF(${units_ordered},0) ;;
  }


  measure: units_ordered_minus_cancelled {
    type: number
    group_label: "Sales Metrics"
    label: "Units Ordered minus Cancelled"
    description: "Total number of ordered items excluding cancelled items"
    sql: ${units_ordered} - ${unit_canceled} ;;
  }

  measure: units_ordered_minus_cancelled_minus_returned{
    type: number
    group_label: "Sales Metrics"
    label: "Units Ordered minus Cancelled & Returns"
    description: "Total number of ordered items excluding cancelled and returned items"
    sql: ${units_ordered} - ${units_returned} - ${unit_canceled} ;;
  }

##################### CANCELLATIONS #####################


  measure: total_cancelled_orders {
    type: count_distinct
    hidden:  yes
    group_label: "Sales Metrics"
    label: "# of Cancelled Orders"
    description: "Total number of orders that are partially or fully cancelled"
    sql: ${order_id};;
    filters: [record_type: "CANCELLED"]
  }

  measure: unit_canceled {
    type: sum
    group_label: "Sales Metrics"
    label: "Units Canceled"
    description: "Total number of canceled items"
    sql: ${qty_canceled};;
  }

  measure: unit_canceled_custom_req {
    type: sum
    group_label: "Sales Metrics"
    label: "Units Canceled - Customer Requeted"
    description: "Total number of canceled items"
    sql: ${qty_canceled};;
    filters: [cancellation_reason: "Customer Request"]
  }

  measure: cancel_rate_item_test {
    type: number
    group_label: "Sales Metrics"
    label: "Cancellation Rate (% item) - Customer Request"
    description: "Percentage of incoming items quantity that were cancelled out of total incoming items quantity, initiated by the Customer. Only available for Brands on OMS"
    value_format_name: percent_2
    sql: nullif(${unit_canceled_custom_req}/nullif(${units_ordered}, 0),0);;
  }

  measure: canceled_amount_local {
    type: sum
    group_label: "Sales Metrics"
    label: "Cancelled Amount (AED)"
    description: "Total value of incoming orders that are cancelled in AED"
    value_format: "#,##0.00"
    sql: ${cancel_amount_local};;
  }

  measure: canceled_amount_usd {
    type: sum
    group_label: "Sales Metrics"
    label: "Cancelled Amount (USD)"
    description: "Total value of incoming orders that are cancelled in USD"
    value_format: "$#,##0.00"
    sql: ${cancel_amount_usd};;
  }

  measure: canceled_amount_usd_cust_req {
    type: sum
    group_label: "Sales Metrics"
    label: "Cancelled Amount (USD) - Customer Request"
    description: "Total value of incoming orders that are cancelled in USD"
    value_format: "$#,##0.00"
    sql: ${cancel_amount_usd};;
    filters: [cancellation_reason: "Customer Request"]
  }

  measure: cancel_rate_item {
    type: number
    group_label: "Sales Metrics"
    label: "Cancellation Rate (% item)"
    description: "Percentage of incoming items quantity that were cancelled out of total incoming items quantity"
    value_format_name: percent_2
    sql: nullif(${unit_canceled}/nullif(${units_ordered}, 0),0);;
  }

  measure: cancel_rate_value {
    type: number
    group_label: "Sales Metrics"
    label: "Cancellation Rate (% value)"
    description: "Percentage of incoming items that were cancelled"
    value_format_name: percent_2
    sql: ${canceled_amount_usd}/nullif(${gross_rev_usd}, 0);;
  }

  measure: cancel_rate_value_cust_req {
    type: number
    group_label: "Sales Metrics"
    label: "Cancellation Rate (% value) - Customer Request"
    description: "Percentage of incoming items that were cancelled"
    value_format_name: percent_2
    sql: ${canceled_amount_usd_cust_req}/nullif(${gross_rev_usd}, 0);;
  }


##################### SHIPPED #####################


  measure: total_shipped_orders {
    type: count_distinct
    group_label: "Sales Metrics"
    label: "# of Shipped Orders"
    description: "Total number of orders that are partially or fully shipped"
    sql: ${order_id};;
    filters: [record_type: "SHIPMENT"]
  }

  measure: units_shipped {
    type: sum
    group_label: "Sales Metrics"
    label: "Units Shipped"
    description: "Total number of shipped items"
    sql: ${qty_shipped};;
  }

  measure: net_units_shipped {
    type: number
    group_label: "Sales Metrics"
    label: "Units Shipped minus Returns"
    description: "Total number of shipped items excluding returned items"
    sql: ${units_shipped} - ${units_returned} ;;
  }

  measure: shipped_amount_local {
    type: sum
    group_label: "Sales Metrics"
    label: "Shipped Revenue (AED)"
    description: "Total value of shipped orders (excluding failed credit card attempts, VAT and cancellations but including customs and shipping fees)"
    value_format: "#,##0.00"
    sql: ${shipment_revenue_local};;
  }

  measure: shipped_amount_usd {
    type: sum
    group_label: "Sales Metrics"
    label: "Shipped Revenue (USD)"
    description: "Total value of shipped orders (excluding failed credit card attempts, VAT and cancellations but including customs and shipping fees)"
    value_format: "$#,##0.00"
    sql: ${shipment_revenue_usd};;
  }

  measure: net_sales_local {
    type: sum
    group_label: "Sales Metrics"
    label: "Shipped Net Sales (AED)"
    description: "Total value of incoming orders on the website excluding returns (excluding failed credit card attempts, VAT, customs and shipping fees and COD fees). To be computed based on shipping date"
    value_format: "#,##0.00"
    sql:  ${ship_net_local};;
  }

  measure: net_sales_usd {
    type: sum
    group_label: "Sales Metrics"
    label: "Shipped Net Sales (USD)"
    description: "Total value of incoming orders on the website excluding returns (excluding failed credit card attempts, VAT, customs and shipping fees and COD fees). To be computed based on shipping date"
    value_format: "$#,##0.00"
    sql: ${ship_net_usd};;
  }


##################### RETURNS #####################


  measure: units_returned {
    type: sum
    group_label: "Sales Metrics"
    label: "Units Returned"
    description: "Total numer of returned items"
    sql: ${qty_refunded};;
  }

  measure: total_return_orders {
    type: count_distinct
    hidden:  yes
    group_label: "Sales Metrics"
    label: "# of Returned Orders"
    description: "Total value of orders that are partially or fully returned"
    sql: ${order_id};;
    filters: [record_type: "REFUND"]
  }

  measure: refund_amount_local_measure {
    type: sum
    group_label: "Sales Metrics"
    label: "Returned Amount (AED)"
    description: "Total value of orders that are returned in local currency"
    value_format: "#,##0.00"
    sql: ${refund_amount_local};;
  }

  measure: refund_amount_usd_measure {
    type: sum
    group_label: "Sales Metrics"
    label: "Returned Amount (USD)"
    description: "Total value of orders that are returned in USD"
    value_format: "$#,##0.00"
    sql: ${refund_amount_usd};;
  }

  measure: return_rate_value {
    type: number
    group_label: "Sales Metrics"
    label: "Returned Rate (% Shipped value)"
    description: "Percentage of shipped items that were returned"
    value_format_name: percent_2
    sql: ${refund_amount_usd_measure}/nullif(${shipped_amount_usd}, 0);;
  }

  measure: return_rate_item {
    type: number
    group_label: "Sales Metrics"
    label: "Returned Rate (% items)"
    description: "Percentage of shipped items quantity that were returned out of total shipped items quantity"
    value_format_name: percent_2
    sql: nullif(${units_returned}/nullif(${units_shipped}, 0),0);;
  }

  measure: return_rate_value_packed {
    type: number
    group_label: "Sales Metrics"
    label: "Returned Rate (% Packed value)"
    description: "Percentage of packed items that were returned"
    value_format_name: percent_2
    sql: ${refund_amount_usd_measure}/nullif(${packed_amount_usd}, 0);;
  }

  measure: return_rate_item_packed {
    type: number
    group_label: "Sales Metrics"
    label: "Returned Item (% packed items)"
    description: "Percentage of packed items quantity that were returned out of total packed items quantity"
    value_format_name: percent_2
    sql: nullif(${units_returned}/nullif(${units_packed}, 0),0);;
  }


##################### PACKED #####################


  measure: total_packed_orders {
    type: count_distinct
    hidden:  yes
    group_label: "Sales Metrics"
    label: "# of Packed Orders"
    description: "Total number of orders that are partially or fully packed"
    sql: ${order_id};;
    filters: [record_type: "PACKED"]
  }

  measure: units_packed {
    type: sum
    group_label: "Sales Metrics"
    label: "Units Packed"
    description: "Total number of packed items"
    sql: ${qty_packed};;
  }

  measure: net_units_packed {
    type: number
    group_label: "Sales Metrics"
    label: "Units Packed minus Returns"
    description: "Total number of packed items excluding returned items"
    sql: ${units_packed} - ${units_returned} ;;
  }

  measure: packed_amount_local {
    type: sum
    group_label: "Sales Metrics"
    label: "Packed Sales (AED)"
    description: "Total value of shipped orders (excluding failed credit card attempts, VAT and cancellations but including customs and shipping fees)"
    value_format: "#,##0.00"
    sql: ${packed_revenue_local};;
  }

  measure: packed_amount_usd {
    type: sum
    group_label: "Sales Metrics"
    label: "Packed Sales (USD)"
    description: "Total value of shipped orders (excluding failed credit card attempts, VAT and cancellations but including customs and shipping fees)"
    value_format: "$#,##0.00"
    sql: ${packed_revenue_usd};;
  }


  measure: packed_net_sales_local {
    type: sum
    group_label: "Sales Metrics"
    label: "Packed Sales minus Returns (AED)"
    description: "Total value of incoming orders on the website excluding returns (excluding failed credit card attempts, VAT, customs and shipping fees and COD fees). To be computed based on packing date"
    value_format: "#,##0.00"
    sql: ${packed_sales_minus_return_local};;
  }

  measure: packed_net_sales_usd {
    type: sum
    group_label: "Sales Metrics"
    label: "Packed Sales minus Returns (USD)"
    description: "Total value of incoming orders on the website excluding returns (excluding failed credit card attempts, VAT, customs and shipping fees and COD fees). To be computed based on packing date"
    value_format: "$#,##0.00"
    sql: ${packed_sales_minus_return_usd};;
  }

###################################
##            GA Measures        ##
###################################{

  measure: number_of_sessions {
    type: sum
    group_label: "GA Metrics"
    label: "# of Sessions"
    description: "Total number of sessions on the website"
    sql: ${sessions} ;;
  }

  measure: sum_session_duration {
    type: sum
    hidden: yes
    group_label: "GA Metrics"
    label: "Sessions duration"
    sql: ${TABLE}.session_duration ;;
  }

  measure: sum_bounces {
    type: sum
    group_label: "GA Metrics"
    label: "# of Bounces"
    description: "Total number of visitors who leave the website after only one visit"
    sql: ${TABLE}.bounces ;;
  }

  measure: number_of_pageviews {
    type: sum
    group_label: "GA Metrics"
    label: "# of Page views"
    description: "Total number of pages viewed"
    sql: ${TABLE}.pageviews ;;
  }

  measure: sum_unique_pageviews {
    type: sum
    group_label: "GA Metrics"
    label: "# of Unique Page views"
    description: "Total number of unique pages viewed"
    sql: ${TABLE}.unique_pageviews ;;
  }

  measure: sum_product_adds_to_cart {
    type: sum
    group_label: "GA Metrics"
    label: "# of Products Added to Cart"
    description: "Total number of products added to cart"
    sql: ${TABLE}.product_adds_to_cart ;;
  }

  measure: sum_product_checkouts {
    type: sum
    group_label: "GA Metrics"
    label: "# of Product Checkouts"
    description: "Total number of product that were bought"
    sql: ${TABLE}.product_checkouts ;;
  }

  measure: sum_users {
    type: sum
    group_label: "GA Metrics"
    label: "# of Users"
    description: "Total number of users"
    sql: ${TABLE}.users ;;
  }

  measure: sum_new_users {
    type: sum
    group_label: "GA Metrics"
    label: "# of New Users"
    description: "Total number of new users"
    sql: ${TABLE}.new_users ;;
  }

  measure: sum_product_list_views {
    type: sum
    group_label: "GA Metrics"
    label: "# of Product List Views"
    description: "Total number of Product List Views"
    sql: ${TABLE}.product_list_views ;;
  }

  measure: sum_product_detail_views {
    type: sum
    group_label: "GA Metrics"
    label: "# of Product Detail Views"
    description: "Total number of Product Detail Views"
    sql: ${TABLE}.product_detail_views ;;
  }

  measure: pageviews_per_session {
    type: number
    group_label: "GA Metrics"
    label: "Pageviews per Session"
    description: "Average number of pages viewed per session"
    value_format_name: decimal_2
    sql: ${number_of_pageviews}/NULLIF(${number_of_sessions},0) ;;
  }


  measure: bounce_rate {
    type: number
    group_label: "GA Metrics"
    label: "Bounce Rate %"
    description: "Percentage of visitors who leave the website after only one page view"
    value_format_name: percent_2
    sql: ${sum_bounces}/NULLIF(${number_of_sessions},0) ;;
  }

  measure: avg_time_on_site {
    type: number
    group_label: "GA Metrics"
    label: "Avg Time On Site"
    description: "Average of time spend on the site per session"
    value_format: "h:mm:ss"
    sql: ${sum_session_duration}/NULLIF(${number_of_sessions},0) / 86400 ;;
  }


  measure: add_to_basket_rate {
    type: number
    group_label: "GA Metrics"
    label: "Add to Basket %"
    description: "Percentage of product detail views that have added the product to their basket over a period of time"
    value_format_name: percent_2
    sql: ${sum_product_adds_to_cart}/NULLIF(${number_of_pageviews},0) ;;
  }

  measure: cart_abandonment_rate {
    type: number
    group_label: "GA Metrics"
    label: "Cart Abandonment %"
    description: "Number of cart created that did not convert into an order (1-% cart abandonment = cart compeltion rate)"
    value_format_name: percent_2
    sql: (${sum_product_adds_to_cart}-${sum_product_checkouts})/NULLIF(${sum_product_adds_to_cart},0) ;;
  }

  measure: checkout_to_add_rate {
    type: number
    group_label: "GA Metrics"
    label: "Checkout to Add to Basket %"
    description: "Percentage of products that were added to cart and converted into an order"
    value_format_name: percent_2
    sql: ${sum_product_checkouts}/NULLIF(${sum_product_adds_to_cart},0) ;;
  }


  measure: conversion_rate {
    type: number
    group_label: "GA Metrics"
    label: "Conversion Rate (%)"
    description: "Percentage of sessions that make an order"
    value_format_name: percent_2
    sql: ${count_sales_orders}/NULLIF(${number_of_sessions},0) ;;
  }




##       End of GA Measures      ##}



###################################
##         Funnel Measures       ##
###################################{



  measure: total_ad_spend_usd {
    type: sum
    group_label: "Marketing Metrics"
    label: "Total Ad Spend (USD)"
    value_format: "$#,##0.00"
    description: "Total value of marketing ads spent across all different channels"
    sql: ${common_cost_usd} ;;
  }

  measure: roas_gross_rev {
    type: number
    group_label: "Marketing Metrics"
    label: "ROAS (on Gross Rev)"
    description: "The Return in Gross Sales from Performance Ad Spend"
    value_format: "#,##0.00"
    sql: ${gross_rev_usd}/(nullif(${total_ad_spend_usd}, 0)) ;;
  }

  measure: roi_gross_rev {
    type: number
    group_label: "Marketing Metrics"
    label: "ROI (on Gross Rev)"
    description: "The Return in Gross Sales from Digital Ad Spend"
    value_format: "#,##0.00"
    sql: ${gross_rev_usd}/nullif(${total_ad_spend_usd}, 0) ;;
  }

  measure: cost_per_order {
    type: number
    group_label: "Marketing Metrics"
    label: "CPO"
    description: "The marketing cost of one order"
    value_format: "$#,##0.00"
    sql: ${total_ad_spend_usd}/nullif(${count_sales_orders}, 0) ;;
  }

  measure: total_clicks {
    type: sum
    group_label: "Marketing Metrics"
    label: "# Clicks"
    description: "Total number of times an ad is clicked by a user"
    sql: ${common_clicks} ;;
  }

  measure: total_impressions {
    type: sum
    group_label: "Marketing Metrics"
    label: "# Impressions"
    description: "Total number of times an ad is seen"
    sql:  ${common_impressions};;
  }

  measure: cost_per_click {
    type: number
    group_label: "Marketing Metrics"
    label: "CPC"
    description: "The marketing cost of one ad click"
    value_format_name: usd
    sql: ${total_ad_spend_usd} / NULLIF(${total_clicks},0) ;;
  }


  measure: click_through_rate {
    type: number
    group_label: "Marketing Metrics"
    label: "Click Through Rate"
    description: "Percentage ad impressions that have led to a click"
    value_format_name: percent_2
    sql: NULLIF(${total_clicks},0) / NULLIF(${total_impressions},0);;
  }


  measure: cost_per_mill {
    type: number
    group_label: "Marketing Metrics"
    label: "CPM"
    description: "The marketing cost of 1000 impressions"
    value_format_name: usd
    sql: NULLIF(${total_ad_spend_usd},0) / NULLIF(${total_impressions}/1000,0) ;;
  }

  measure: number_of_days {
    type: count_distinct
    hidden: yes
    sql: ${common_date} ;;
  }

  measure: clicks_per_day {
    type: number
    group_label: "Marketing Metrics"
    value_format_name: decimal_2
    label: "Clicks per Day"
    description: "Average number of clicks in a day for a given period"
    sql: ${total_clicks} / NULLIF(${number_of_days},0) ;;
  }

  measure: cost_per_day {
    type: number
    group_label: "Marketing Metrics"
    label: "Cost per Day"
    description: "Average ad spend in value in a day for a given period in USD"
    value_format_name: usd
    sql: ${total_ad_spend_usd} / NULLIF(${number_of_days},0) ;;
  }


## End of  Measures ##}


#### Refreshes #####

  dimension_group: last_updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.last_updated AS TIMESTAMP) ;;
  }

}
