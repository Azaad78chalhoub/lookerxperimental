view: tryano_external_concession_stock {
  sql_table_name: `chb-prod-data-comm.prod_commercial.tryano_external_concession_stock_by_day`
    ;;

  dimension: pk {
    hidden: yes
    primary_key: yes
    type: string
    sql: ${TABLE}.primary_key  ;;
  }

  dimension_group: _modified {
    hidden: yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}._modified ;;
  }

  dimension_group: end_of_interval {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.end_of_interval ;;
  }

  dimension: original_sku {
    type: string
    sql: ${TABLE}.original_sku ;;
  }

  dimension_group: period {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.period ;;
  }

  dimension: rsp {
    type: string
    sql: ${TABLE}.rsp ;;
  }

  dimension: sku {
    type: string
    sql: ${TABLE}.sku ;;
  }

  dimension: soh {
    type: number
    sql: ${TABLE}.soh ;;
  }

  dimension: srp {
    type: string
    sql: ${TABLE}.srp ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
