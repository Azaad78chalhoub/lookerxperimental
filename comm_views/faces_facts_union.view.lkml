include: "../_common/period_over_period.view"
include: "../customer_views/ecomm_customer_new_returning.view"

view: faces_facts_union {
  sql_table_name: `chb-prod-data-comm.prod_commercial.faces_facts_union`
    ;;

  dimension: brand_security{
    type: string
    hidden: yes
    sql: "FACES" ;;
  }

  dimension: vertical_security{
    type: string
    hidden: yes
    sql: "MANAGED COMPANIES" ;;
  }

  extends: [period_over_period]

  dimension_group: pop_no_tz {
    sql: ${common_raw} ;;
  }

  dimension: primary_key {
    type: number
    primary_key: yes
    hidden: yes
    sql: ${TABLE}.primary_key ;;
  }

  # dimension_group: common {
  #   type: time
  #   timeframes: [
  #     raw,
  #     time,
  #     date,
  #     week,
  #     month,
  #     quarter,
  #     year,
  #     week_of_year,
  #     day_of_week
  #   ]
  #   convert_tz: no
  #   datatype: date
  #   label: "Sales Order"
  #   description: "Order creation datetime"
  #   sql: ${TABLE}.common_date ;;
  # }

  dimension_group: common {
    type: time
    timeframes: [
      raw,
      date,
      week,
      time,
      month,
      quarter,
      year,
      week_of_year,
      day_of_week
    ]
    convert_tz: no
    datatype: timestamp
    label: "Date"
    description: "Date when a specific even (Record Type) took place"
    sql: ${TABLE}.common_datetime;;
  }

  dimension_group: order_datetime {
    type: time
    timeframes: [
      raw,
      time,
      hour,
      date,
      week,
      month,
      quarter,
      year,
      week_of_year,
      day_of_week
    ]
    convert_tz: no
    datatype: timestamp
    label: "Sales Order"
    description: "Order Creation Date"
    sql: cast(${TABLE}.order_datetime as timestamp) ;;
  }

  dimension: business_week_no{
    type: number
    description: "Week No. extracted from the business date"
    sql: EXTRACT( WEEK FROM(PARSE_DATE('%Y-%m-%d', CAST(${TABLE}.common_date AS STRING))))+1 ;;
  }

  dimension: date_group {
    label: "Date Group"
    hidden: yes
    sql: {% if faces.common_date._in_query %} CAST(${TABLE}.common_date  AS DATE)
        {% elsif faces.common_month._in_query %} FORMAT_DATE('%Y-%m', ${TABLE}.common_date )
        {% elsif faces.common_quarter._in_query %} (FORMAT_TIMESTAMP('%Y-%m', TIMESTAMP_TRUNC(CAST(${TABLE}.common_date AS TIMESTAMP), QUARTER)))
        {% elsif faces.common_week._in_query %} CONCAT(CAST(EXTRACT(YEAR FROM ${TABLE}.common_date ) AS STRING),'-', CAST(EXTRACT(WEEK(SUNDAY) FROM ${TABLE}.common_date) AS STRING))
        {% elsif faces.common_year._in_query %} EXTRACT(YEAR FROM ${TABLE}.common_date )
        {% else %} CAST(${TABLE}.common_date  AS DATE)
        {% endif %} ;;
  }

  dimension: country {
    type: string
    map_layer_name: countries
    description: "Ecom site country"
    sql: ${TABLE}.country ;;
  }

  dimension: source {
    type: string
    description: "Data Source. Sales order(OMS, Magento, ...) , Google Analytics (GA) and Funnel"
    sql: ${TABLE}.source ;;
  }

  dimension: measure_label {
    label: "Measure Name"
    description: "Dummy dimensions showing measure label (used to transpose table)"
    case: {
      when: {
        label: "# of Sessions"
        sql: 1=1;;
      }
      when: {
        label: "Conversion Rate (%)"
        sql: 1=1;;
      }
      when: {
        label: "# Orders"
        sql: 1=1;;
      }
      when: {
        label: "Average Order Value USD"
        sql: 1=1;;
      }
      when: {
        label: "UPT"
        sql: 1=1;;
      }
      when: {
        label: "USP"
        sql: 1=1;;
      }
      when: {
        label: "Gross Revenue USD"
        sql: 1=1;;
      }
      when: {
        label: "Cancelled Rate (%)"
        sql: 1=1;;
      }
      when: {
        label: "Returned Rate (%)"
        sql: 1=1;;
      }
      when: {
        label: "Shipped Net (USD)"
        sql: 1=1;;
      }
      when: {
        label: "ROAS (on Gross Rev)"
        sql: 1=1;;
      }
    }
  }


###################################
### Sales Order - Dimensions ###
###################################{

  dimension: order_id {
    type: string
    label: "Sales Order ID"
    group_label: "Sales Order"
    description: "Sales Order Id"
    sql: ${TABLE}.order_id ;;
    link: {
      label: "View Order in OMS"
      url: "https://fcs.retailunity.com/cpanel/cpanel_includes/inc_datatables/orders/orders_all.php7?action=edit&id={{faces.retailunity_order_id._value}}"
    }
  }

  dimension: retailunity_order_id {
    type: string
    hidden: yes
    sql: ${TABLE}.retailunity_order_id ;;
  }

  dimension: status {
    type: string
    label: "Order Status"
    description: "Sales order status"
    group_label: "Sales Order"
    sql: ${TABLE}.status ;;
  }

  dimension: brand {
    type: string
    hidden: yes
    sql: ${TABLE}.brand ;;
  }

  dimension: delivery_type {
    type: string
    group_label: "Sales Order"
    sql: ${TABLE}.delivery_type ;;
  }

  dimension: payment_method {
    type: string
    label: "Payment Method"
    group_label: "Sales Order"
    description: "Payment method for the order / transaction"
    sql: ${TABLE}.payment_method ;;
  }

  dimension: payment_status {
    type: string
    group_label: "Sales Order"
    sql: ${TABLE}.payment_status ;;
  }

  dimension: products_in_order {
    type: string
    group_label: "Sales Order"
    label: "Number of Products in Order"
    sql: ${TABLE}.products_in_order ;;
  }

  dimension: order_custom_duty_fee {
    type: number
    group_label: "Sales Order"
    label: "Order Custom Duty Fee amount (Local)"
    sql: ${TABLE}.order_custom_duty_fee ;;
  }

  dimension: order_payment_fee {
    type: number
    group_label: "Sales Order"
    label: "Order Payment Fee amount (Local)"
    sql: ${TABLE}.order_payment_fee ;;
  }

  dimension: order_shipping {
    type: number
    group_label: "Sales Order"
    label: "Order Shipping amount (Local)"
    sql: ${TABLE}.order_shipping ;;
  }

  dimension: order_total {
    type: number
    group_label: "Sales Order"
    label: "Order Total Amount (Local)"
    sql: ${TABLE}.order_total ;;
  }

  dimension: channel {
    type: string
    group_label: "Sales Order"
    sql: ${TABLE}.channel ;;
  }

  dimension: order_currency {
    type: string
    group_label: "Sales Order"
    sql: ${TABLE}.order_currency ;;
  }

  dimension: xstore_invoice_number {
    type: string
    hidden: yes
    group_label: "Sales Order"
    sql: ${TABLE}.xstore_invoice_number ;;
  }

  dimension: voucher_code {
    type: string
    group_label: "Sales Order"
    label: "Voucher Codes"
    sql: ${TABLE}.voucher_code ;;
  }

  dimension: giftwrapping {
    type: string
    label: "Gift Wrapping (Y/N)"
    description: "Shows Y (Yes) if a Gift Wrapping service item was part of the order, else N (No)"
    group_label: "Sales Order"
    sql: ${TABLE}.giftwrapping ;;
  }


  ### End of Sales Order dimensions
  #}

########################################
### Sales Order item - Dimensions ###
########################################{

  dimension: detail_id {
    type: number
    hidden: yes
    sql: cast(${TABLE}.detail_id as int64) ;;
  }

  dimension: record_type {
    type: string
    group_label: "Sales Order Item"
    description: "Categorizes the sales order item record. Shows either SALE, CANCELLED, REFUND, SHIPMENT, ..."
    sql: ${TABLE}.record_type ;;
  }

  dimension: final_record_status {
    type: string
    label: "Latest Item Status"
    description: "Shows the last known status of the item on this order"
    group_label: "Sales Order Item"
    sql: ${TABLE}.final_record_status ;;
  }

  dimension_group: final_record {
    type: time
    timeframes: [
      time,
      date
    ]
    convert_tz: no
    datatype: date
    label: "Latest Item Status Date"
    description: "Shows the last date of the known status of the item on this order"
    group_label: "Sales Order Item"
    sql: ${TABLE}.final_status_datetime ;;
  }

  dimension: sku {
    type: string
    label: "SKU"
    group_label: "Sales Order Item"
    description: "SKU of the Item"
    hidden: yes
    sql: ${TABLE}.sku ;;
  }

  dimension: price {
    type: number
    label: "Item Price (Local)"
    description: "Item price in local currency"
    group_label: "Sales Order Item"
    sql: ${TABLE}.price ;;
  }

  dimension: cancellation_reason {
    type: string
    label: "Item Cancel Reason"
    group_label: "Sales Order Item"
    sql: ${TABLE}.cancellation_reason ;;
  }


  dimension: cc_location_internal_id {
    type: string
    label: "Click & Collect Shop/WH Id"
    description: "Click & Collect Shop/WH Id"
    group_label: "Sales Order Item"
    sql: ${TABLE}.cc_location_internal_id ;;
  }

  dimension: cc_location_name {
    type: string
    label: "Click & Collect Shop/WH Name"
    description: "Click & Collect Shop/WH Name"
    group_label: "Sales Order Item"
    sql: ${TABLE}.cc_location_name ;;
  }

  dimension: detail_custom_duty {
    type: string
    label: "Custom Duty amount (Local)"
    group_label: "Sales Order Item"
    sql: ${TABLE}.detail_custom_duty ;;
  }

  dimension: detail_external_id {
    type: string
    label: "Source SKU"
    group_label: "Sales Order Item"
    description: "Item SKU number coming from OMS"
    sql: ${TABLE}.detail_external_id ;;
  }

  dimension: barcode {
    type: string
    label: "Source Barcode"
    group_label: "Sales Order Item"
    description: "Item Barcode number coming from OMS"
    sql: ${TABLE}.barcode ;;
  }

  dimension: detail_name {
    type: string
    hidden: yes
    group_label: "Sales Order Item"
    sql: ${TABLE}.detail_name ;;
  }

  dimension: detail_price_original {
    type: number
    group_label: "Sales Order Item"
    label: "Item Original Price (Local)"
    sql: ${TABLE}.detail_price_original ;;
  }

  dimension: detail_sku_code {
    type: string
    hidden: yes
    group_label: "Sales Order Item"
    sql: ${TABLE}.detail_sku_code ;;
  }

  dimension: detail_tax {
    type: number
    group_label: "Sales Order Item"
    label: "Tax rate"
    sql: ${TABLE}.detail_tax ;;
  }

  dimension: detail_tax_amount {
    type: number
    group_label: "Sales Order Item"
    label: "Tax Amount (Local)"
    sql: ${TABLE}.detail_tax_amount ;;
  }

  dimension: employee_id {
    type: string
    group_label: "Sales Order Item"
    sql: ${TABLE}.employee_id ;;
  }

  dimension: fulfilment_employee_id {
    type: string
    group_label: "Sales Order Item"
    description: "Employee fulfilling the order item"
    sql: ${TABLE}.fulfilment_employee_id ;;
  }

  dimension: return_cancel_reason {
    type: string
    group_label: "Sales Order Item"
    label: "Item Return/Cancel Reason"
    sql: ${TABLE}.return_cancel_reason ;;
  }

  dimension: return_location_id {
    type: string
    hidden: yes
    sql: ${TABLE}.return_location_id ;;
  }

  dimension: return_location_internal_id {
    type: string
    group_label: "Sales Order Item"
    label: "Return WH/Store Loc Id"
    sql: ${TABLE}.return_location_internal_id ;;
  }

  dimension: return_location_name {
    type: string
    group_label: "Sales Order Item"
    label: "Return WH/Store Loc Name"
    sql: ${TABLE}.return_location_name ;;
  }

  dimension: return_quality {
    type: string
    group_label: "Sales Order Item"
    label: "Return Quality"
    sql: ${TABLE}.return_quality ;;
  }


### End of Sales Order item
#}


########################################
### Sales Order item Shipment - Dimensions ###
########################################{

  dimension: delivery_city {
    type: string
    group_label: "Sales Order Item Shipment"
    label: "Delivery City"
    sql: ${TABLE}.delivery_city ;;
  }

  dimension: delivery_country {
    type: string
    group_label: "Sales Order Item Shipment"
    label: "Delivery Country"
    sql: ${TABLE}.delivery_country ;;
  }

  dimension: parcel_id {
    type: string
    group_label: "Sales Order Item Shipment"
    sql: ${TABLE}.parcel_id ;;
  }

  dimension: products_in_parcel {
    type: string
    group_label: "Sales Order Item Shipment"
    sql: ${TABLE}.products_in_parcel ;;
  }

  dimension: shipment_id {
    type: string
    group_label: "Sales Order Item Shipment"
    sql: ${TABLE}.shipment_id ;;
  }

  dimension: shipping_location_group{
    type: string
    label: "Shipping location Group"
    group_label: "Sales Order Item Shipment"
    sql: CASE WHEN ${TABLE}.shipping_location_id like "100%" THEN "Warehouse"
              WHEN ${TABLE}.shipping_location_id in ("0", "0%", "99999") THEN "Not Used"
              WHEN ${TABLE}.shipping_location_id is NULL THEN "Not Used"
            ELSE "Store"
            END ;;
  }

  dimension: shipping_discount_amount {
    type: number
    group_label: "Sales Order Item Shipment"
    label: "Shipping Discount Amount (Local)"
    sql: ${TABLE}.shipping_discount_amount ;;
  }


  dimension: shipping_location_internal_id {
    type: string
    group_label: "Sales Order Item Shipment"
    label: "Shipping WH/Store Loc Id"
    sql: ${TABLE}.shipping_location_internal_id ;;
  }

  dimension: shipping_location_name {
    type: string
    group_label: "Sales Order Item Shipment"
    label: "Shipping WH/Store Loc Name"
    sql: ${TABLE}.shipping_location_name ;;
  }

  dimension: shipping_original_amount {
    type: number
    group_label: "Sales Order Item Shipment"
    label: "Shipping Original Amount (Local)"
    sql: ${TABLE}.shipping_original_amount ;;
  }

  dimension: tracking_id {
    type: string
    group_label: "Sales Order Item Shipment"
    sql: ${TABLE}.tracking_id ;;
  }

### End of Sales Order item Shipment
#}

########################################
### Order level Coupons - Dimensions ###
########################################{

  dimension: order_promotion_detail_coupon_code {
    type: string
    label: "Order - Promotion coupon code"
    description: "Order promotion coupon code"
    group_label: "Coupons - order level"
    sql: ${TABLE}.order_promotion_detail_coupon_code ;;
  }

  dimension: order_promotion_detail_promotion_text {
    type: string
    label: "Order - Promotion text"
    description: "Order promotion text"
    group_label: "Coupons - order level"
    sql: ${TABLE}.order_promotion_detail_promotion_text ;;
  }

  dimension: order_promotion_detail_campaign_id {
    type: string
    label: "Order - Promotion Campaign ID"
    description: "Order promotion Campaign ID"
    group_label: "Coupons - order level"
    sql: ${TABLE}.order_promotion_detail_campaign_id ;;
  }

  measure: order_promotion_detail_discount {
    type: sum
    label: "Order - Promotion discount"
    description: "Order promotion discount (Local currency)"
    group_label: "Coupons - order level"
    sql: ${TABLE}.order_promotion_detail_discount ;;
  }

  ### End of Order level Coupons - Dimensions
  #}



########################################
### Item level Coupons - Dimensions ###
########################################{

  dimension: promotion_detail_coupon_code {
    type: string
    label: "Item - Promotion coupon code"
    description: "Item promotion coupon code"
    group_label: "Coupons - item level"
    sql: ${TABLE}.promotion_detail_coupon_code ;;
  }

  dimension: promotion_detail_promotion_text {
    type: string
    label: "Item - Promotion text"
    description: "Item promotion text"
    group_label: "Coupons - item level"
    sql: ${TABLE}.promotion_detail_promotion_text ;;
  }

  dimension: promotion_detail_campaign_id {
    type: string
    label: "Item - Promotion Campaign ID"
    description: "Item promotion Campaign ID"
    group_label: "Coupons - item level"
    sql: ${TABLE}.promotion_detail_campaign_id ;;
  }

  measure: promotion_detail_discount {
    type: sum
    label: "Item - Promotion discount"
    description: "Item promotion discount (Local currency)"
    group_label: "Coupons - item level"
    sql: ${TABLE}.promotion_detail_discount ;;
  }

  ### End of Item level Coupons - Dimensions
  #}


########################################
### Packed - Dimensions ###
########################################{

  dimension: qty_packed {
    type: number
    hidden: yes
    sql: ${TABLE}.qty_packed ;;
  }

  dimension: packed_revenue_local {
    type: number
    hidden: yes
    sql: ${TABLE}.packed_revenue_local ;;
  }

  dimension: packed_revenue_local_minus {
    type: number
    hidden: yes
    sql: ${TABLE}.packed_revenue_local_minus ;;
  }

  dimension: packed_revenue_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.packed_revenue_usd ;;
  }

  dimension: packed_revenue_usd_minus {
    type: number
    hidden: yes
    sql: ${TABLE}.packed_revenue_usd_minus ;;
  }

  dimension: packed_sales_minus_return_local {
    type: number
    hidden: yes
    sql: ${TABLE}.packed_sales_minus_return_local ;;
  }

  dimension: packed_sales_minus_return_local_incl_tax {
    type: number
    hidden: yes
    sql: ${TABLE}.packed_sales_minus_return_local_incl_tax ;;
  }

  dimension: packed_sales_minus_return_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.packed_sales_minus_return_usd ;;
  }

  dimension: packed_sales_minus_return_usd_incl_tax {
    type: number
    hidden: yes
    sql: ${TABLE}.packed_sales_minus_return_usd_incl_tax ;;
  }
  ### End of Packed Amounts ###}


#####################################
##### Packed Measures
#####################################{

  measure: units_packed {
    type: sum
    group_label: "Sales Order"
    label: "Units Packed"
    description: "Number of Items Packed"
    sql: ${qty_packed};;
  }

  measure: packed_minus_returns_usd {
    type: sum
    group_label: "Sales Order"
    label: "Packed Sales minus Returns (USD)"
    value_format: "$#,##0.00"
    description:"Total value of packed items excluding returns (USD)"
    sql: ${packed_sales_minus_return_usd};;
  }

  measure: packed_minus_returns_local {
    type: sum
    group_label: "Sales Order"
    label: "Packed Sales minus Returns (local)"
    value_format: "#,##0.00"
    description:"Total value of packed items excluding returns (local)"
    sql: ${packed_sales_minus_return_local};;
  }

  measure: packed_sales_local {
    type: sum
    group_label: "Sales Order"
    label: "Packed Sales (local)"
    value_format: "#,##0.00"
    description:"Total value of packed items (local)"
    sql: ${packed_revenue_local};;
  }

  measure: packed_sales_usd {
    type: sum
    group_label: "Sales Order"
    label: "Packed Sales UAT(USD)"
    value_format: "$#,##0.00"
    description:"Total value of packed items (USD)"
    sql: ${packed_revenue_usd};;
  }

### End of Packed Measures ###}

#####################################
##### Sales Orders Measures
#####################################{


  measure: all_orders {
    type: count_distinct
    group_label: "Sales Order"
    label: "# Orders"
    description: "Total number of orders placed on the website"
    sql: ${order_id} ;;
    filters: [record_type: "SALE"]
  }

  measure: units_ordered {
    type: sum
    group_label: "Sales Order"
    label: "Units Ordered"
    description: "Sum of Qty Ordered"
    sql: ${TABLE}.qty_ordered;;
    filters: [record_type: "SALE"]
  }

  measure: units_collected {
    type: sum
    group_label: "Sales Order"
    label: "Units Collected"
    description: "Number of Items Collected"
    sql: ${TABLE}.qty_collected;;
  }

  measure: units_delivered {
    type: sum
    group_label: "Sales Order"
    label: "Units Delivered"
    description: "Number of Items Delivered"
    sql: ${TABLE}.qty_delivered;;
  }

  measure: gross_revenue_local {
    type: sum
    group_label: "Sales Order"
    label: "Gross Revenue Local"
    description: "Total value of incoming orders on the website - Local Currency"
    value_format: "#,##0.00"
    sql: ${TABLE}.gross_revenue_local;;
  }

  measure: gross_revenue_usd {
    type: sum
    group_label: "Sales Order"
    label: "Gross Revenue USD"
    description: "Total value of incoming orders on the website - USD"
    value_format: "$#,##0.00"
    sql: ${TABLE}.gross_revenue_usd;;
  }

  measure: gross_revenue_percent {
    type: percent_of_total
    group_label: "Sales Order"
    label: "% Gross Revenue"
    description: "% of Gross Revenue in selected dimension"
    value_format_name: decimal_2
    sql: ${gross_revenue_usd} ;;
  }

  measure: upt {
    type: number
    group_label: "Sales Order"
    label: "UPT"
    description: "Units per transaction = Average number of units within each order"
    value_format_name: decimal_2
    sql:${units_ordered}/nullif(${all_orders}, 0);;
  }

  measure: aov_local {
    type: number
    group_label: "Sales Order"
    label: "Average Order Value Local"
    description: "Average Value of each order - Local currency"
    value_format: "#,##0.00"
    sql: ${gross_revenue_local}/${all_orders};;
  }

  measure: aov_usd {
    type: number
    group_label: "Sales Order"
    label: "Average Order Value USD"
    description: "Average Value of each order - USD"
    value_format: "#,##0.00"
    sql: ${gross_revenue_usd}/nullif(${all_orders}, 0);;
  }

  measure: unit_selling_price {
    type:  number
    label: "USP"
    group_label: "Sales Order"
    description: "Unit Selling Price = Average price of each item ordered"
    value_format: "#,##0.00"
    sql: ${gross_revenue_usd} / NULLIF(${units_ordered},0) ;;
  }

  measure: discount_amt_local {
    type:  sum
    group_label: "Sales Order"
    label: "Discount Amount (Local)"
    description: "Discount Amount (Local currency)"
    value_format: "#,##0.00"
    sql: ${TABLE}.discount_amount ;;
  }

  ### End of Sales Order
  #}


#####################################
##### Cancelled Orders
####################################{

  measure: count_cancelled_orders {
    type: count_distinct
    group_label: "Sales Order"
    label: "# Cancelled Orders"
    description: "Count of Cancelled Orders"
    sql: ${order_id};;
    filters: [record_type: "CANCELLED"]
  }

  measure: unit_canceled {
    type: sum
    group_label: "Sales Order"
    label: "Units Canceled"
    description: "Sum of Qty Ordered"
    sql: ${TABLE}.qty_canceled;;
  }

  measure: cancelled_order_ratio {
    type: number
    group_label: "Sales Order"
    label: "Cancelled Order %"
    description: "Percentage of incoming orders (based on numbers) that were cancelled"
    value_format_name: percent_2
    sql: ${count_cancelled_orders}/NULLIF(${all_orders},0) ;;
  }

  measure: canceled_amount_local {
    type: sum
    group_label: "Sales Order"
    label: "Cancelled amount Local"
    description: "Cancelled amount Local"
    value_format: "#,##0.00"
    sql: ${TABLE}.canceled_amount_local;;
    filters: [record_type: "CANCELLED"]
  }

  measure: canceled_amount_usd {
    type: sum
    group_label: "Sales Order"
    label: "Cancelled amount USD"
    description: "Cancelled amount USD"
    value_format: "$#,##0.00"
    sql: ${TABLE}.canceled_amount_usd;;
    filters: [record_type: "CANCELLED"]
  }


  measure: gross_revenue_minus_cancelled_local {
    type: number
    group_label: "Sales Order"
    label: "Gross Rev Minus Cancelled Local"
    description: "Gross Revenue Minus Cancelled Local"
    value_format: "#,##0.00"
    sql: ${gross_revenue_local} - ${canceled_amount_local};;
  }

  measure: gross_revenue_minus_cancelled_usd {
    type: number
    group_label: "Sales Order"
    label: "Gross Rev Minus Cancelled USD"
    description: "Gross Revenue Minus Cancelled USD"
    value_format: "$#,##0.00"
    sql: ${gross_revenue_usd} - ${canceled_amount_usd};;
  }

  measure: cancelled_item_ratio {
    type: number
    group_label: "Sales Order"
    label: "Cancelled Item %"
    description: "Cancelled Item %"
    value_format_name: percent_2
    sql: ${unit_canceled}/NULLIF(${units_ordered},0) ;;
  }

  measure: cancelled_rate {
    type: number
    group_label: "Sales Order"
    label: "Cancelled Rate (%)"
    description: "Value of items cancelled (based on cancellation date) divided by total value of incoming items (based on order date) (excluding failed credit card attempts)"
    value_format_name: percent_2
    sql: ${canceled_amount_usd}/nullif(${gross_revenue_usd}, 0);;
  }

### End of Cancelled orders measures
#}

#####################################
##### Shipped Orders
####################################{

  measure: shipped_orders {
    type: count_distinct
    group_label: "Sales Order"
    label: "# Shipped Orders"
    description: "Count of Distinct Order IDs that were shipped"
    sql: ${order_id} ;;
    filters: [record_type: "SHIPMENT"]
  }

  measure: units_shipped {
    type: sum
    group_label: "Sales Order"
    label: "Units Shipped"
    description: "Sum of Qty Shipped"
    sql: ${TABLE}.qty_shipped;;
    filters: [record_type: "SHIPMENT"]
  }

  measure: shipment_revenue_local {
    type: sum
    group_label: "Sales Order"
    label: "Shipped Revenue (Local)"
    description: "Total value of shipped orders (excluding failed credit card attempts, VAT and cancellations) - Local currency"
    value_format: "#,##0.00"
    sql: ${TABLE}.shipment_revenue_local;;
    filters: [record_type: "SHIPMENT"]
  }

  measure: shipment_revenue_usd {
    type: sum
    group_label: "Sales Order"
    label: "Shipped Revenue (USD)"
    description: "Total value of shipped orders (excluding failed credit card attempts, VAT and cancellations) - USD"
    value_format: "$#,##0.00"
    sql: ${TABLE}.shipment_revenue_usd;;
    filters: [record_type: "SHIPMENT"]
  }

  measure: shipped_net_local {
    type: sum
    group_label: "Sales Order"
    label: "Shipped Net (Local)"
    description: "Total value of incoming orders on the website excluding returns. Based on shipping date - Local currency"
    value_format: "#,##0.00"
    sql: ${TABLE}.ship_net_local;;
  }

  measure: shipment_net_usd {
    type: sum
    group_label: "Sales Order"
    label: "Shipped Net (USD)"
    description: "Total value of incoming orders on the website excluding returns. Based on shipping date - USD"
    value_format: "$#,##0.00"
    sql: ${TABLE}.ship_net_usd;;
  }


### End of Shipped orders measures
#}


#####################################
##### REFUND - MEASURES ########
#####################################{

  measure: units_refunded {
    type: sum
    group_label: "Sales Order"
    label: "Units Refunded"
    description: "Sum of units Refunded"
    sql: ${TABLE}.qty_refunded;;
    filters: [record_type: "REFUND"]
  }

  measure: refund_amount_local {
    type: sum
    group_label: "Sales Order"
    label: "Refund Amount Local"
    description: "Refund Amount Local"
    value_format: "#,##0.00"
    sql: ${TABLE}.refund_amount_local;;
    filters: [record_type: "REFUND"]
  }

  measure: refund_amount_usd {
    type: sum
    group_label: "Sales Order"
    label: "Refund Amount USD"
    description: "Refund Amount USD"
    value_format: "$#,##0.00"
    sql: ${TABLE}.refund_amount_usd;;
    filters: [record_type: "REFUND"]
  }


  measure: gross_revenue_minus_cancelled_minus_returns_local {
    type: number
    group_label: "Sales Order"
    label: "Gross Rev Minus Cancelled Minus Returns Local"
    description: "Total value of incoming orders on the website excluding cancellations and returns. Based on order date - Local currency"
    value_format: "#,##0.00"
    sql: ${gross_revenue_local} - ${canceled_amount_local} - ${refund_amount_local};;
  }

  measure: gross_revenue_minus_cancelled_minus_returns_usd {
    type: number
    group_label: "Sales Order"
    label: "Gross Rev Minus Cancelled Minus Returns USD"
    description: "Total value of incoming orders on the website excluding cancellations and returns. Based on order date - USD"
    value_format: "$#,##0.00"
    sql: ${gross_revenue_usd} - ${canceled_amount_usd} - ${refund_amount_usd};;
  }

  measure: return_rate {
    type: number
    group_label: "Sales Order"
    label: "Return Rate (%)"
    description: "Return Rate (%) = Refund Amount in USD / Shipment Revenue in USD [Value of items returned (based on return date in warehouse) divided by total value of items shipped (based on shipping date)]"
    value_format_name: percent_2
    sql: ${refund_amount_usd}/nullif(${shipment_revenue_usd}, 0);;
  }

  measure: return_item_rate {
    type: number
    group_label: "Sales Order"
    label: "Return Item (%)"
    description: "Return Item (%) = Percentage of shipped quantity that were returned out of total shipped quantity"
    value_format_name: percent_2
    sql: nullif(${units_refunded}/nullif(${units_shipped}, 0),0);;
  }

  ### End of Refund measures
  #}


###################################
### Funnel - Dimensions ###
###################################{

  dimension: common_clicks {
    type: number
    hidden: yes
    sql: ${TABLE}.common_clicks ;;
  }

  dimension: common_cost_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.common_cost_usd ;;
  }


  dimension: common_impressions {
    type: number
    hidden: yes
    sql: ${TABLE}.common_impressions ;;
  }

    ### End of Funnel Dimensions
  #}

###################################
###   Funnel - Measures         ###
###################################{

  measure: total_ad_spend_usd {
    type: sum
    group_label: "Funnel Metrics"
    label: "Total Ad Spend (USD)"
    description: "Total value of marketing ads spent across all different channels - USD"
    value_format: "$#,##0.00"
    sql: ${common_cost_usd} ;;
    filters: [record_type: "SALE", source: "FUNNEL"]
  }

  measure: roas_gross_rev {
    type: number
    group_label: "Funnel Metrics"
    label: "ROAS (on Gross Rev)"
    description: "Return on Ad Spend = The Return in Gross Sales coming from your Total Ad Spend (based on Gross Sales)"
    value_format: "#,##0.00"
    sql: ${gross_revenue_usd}/nullif(${total_ad_spend_usd}, 0) ;;
  }

  measure: roi_gross_rev {
    type: number
    group_label: "Funnel Metrics"
    label: "ROI (on Gross Rev)"
    description: "Return on Investment : The Return on Investment in Total Ad Spend in Gross Sales - USD"
    value_format: "#,##0.00"
    sql: (${gross_revenue_usd}-${total_ad_spend_usd})/nullif(${total_ad_spend_usd}, 0) ;;
  }

  measure: cost_per_order {
    type: number
    group_label: "Funnel Metrics"
    label: "CPO"
    description: "The marketing cost of one order"
    value_format: "$#,##0.00"
    sql: ${total_ad_spend_usd}/nullif(${all_orders}, 0) ;;
  }

  measure: total_clicks {
    type: sum
    group_label: "Funnel Metrics"
    label: "# Clicks"
    description: "Total number of times an ad is clicked by a user"
    sql: ${common_clicks} ;;
    filters: [record_type: "SALE", source: "FUNNEL"]
  }

  measure: total_impressions {
    type: sum
    group_label: "Funnel Metrics"
    label: "# Impressions"
    description: "Total number of times an ad is seen"
    sql:  ${common_impressions};;
    filters: [record_type: "SALE", source: "FUNNEL"]
  }

  measure: cost_per_click {
    type: number
    group_label: "Funnel Metrics"
    label: "CPC"
    description: "The marketing cost of one ad click"
    value_format_name: usd
    sql: ${total_ad_spend_usd} / NULLIF(${total_clicks},0) ;;
  }


  measure: click_through_rate {
    type: number
    group_label: "Funnel Metrics"
    label: "Click Through Rate %"
    description: "Percentage ad impressions that have led to a click"
    value_format_name: percent_2
    sql: NULLIF(${total_clicks},0) / NULLIF(${total_impressions},0);;
  }


  measure: cost_per_mill {
    type: number
    group_label: "Funnel Metrics"
    label: "CPM"
    description: "The marketing cost of 1000 impressions"
    value_format_name: usd
    sql: NULLIF(NULLIF(${total_ad_spend_usd},0) / NULLIF(${total_impressions}/1000,0),0) ;;
  }

  measure: cost_of_acquisition{
    type: number
    value_format: "$#,##0.00"
    description: "The marketing cost of one acquired new customer"
    group_label: "Funnel Metrics"
    label: "Cost of Acquisition"
    sql: NULLIF(${total_ad_spend_usd} / NULLIF(${ecomm_customer_type_pop.new_cust_count_fixed},0),0) ;;
  }

  ### End of Funnel Measures
  #}


#######################################
###   Gross Margin Calculation      ###
#######################################{

  measure: unit_cost_usd {
    type: sum
    group_label: "Sales Order"
    label: "Unit Cost USD"
    description: "Unit Cost USD"
    value_format_name: decimal_2
    sql: ${TABLE}.fsp_unit_cost_usd;;
    filters: [record_type: "SALE"]
  }

  measure: item_level_cost {
    type: sum
    group_label: "Sales Order"
    label: "Item level cost USD"
    description: "Item level cost USD"
    value_format_name: decimal_2
    sql: ${TABLE}.fsp_unit_cost_usd*${TABLE}.qty_ordered ;;
    filters: [record_type: "SALE"]
  }

  measure: gross_margin {
    type: number
    group_label: "Sales Order"
    label: "Gross Margin %"
    description: "Gross margin is a company's net sales revenue minus its cost of goods sold (COGS)"
    value_format_name: percent_2
    sql: CASE WHEN ${gross_revenue_usd} = 0 then 0
        ELSE (${gross_revenue_usd} - ${item_level_cost})/${gross_revenue_usd}
        end;;
  }

    ### End of Gross Margin Calculation
  #}

#######################################
### Acquisition/Marketing dimensions ###
#######################################{

  dimension: acq_campaign {
    type: string
    group_label: "Acquisition / Marketing"
    description: "Acquisition campaign"
    label: "Campaign"
    sql: ${TABLE}.acq_campaign ;;
  }

  dimension: acq_device {
    type: string
    group_label: "Acquisition / Marketing"
    description: "Acquisition device"
    label: "Device"
    sql: ${TABLE}.acq_device ;;
  }

  dimension: acq_keyword {
    type: string
    group_label: "Acquisition / Marketing"
    description: "Acquisition keyword"
    label: "Keyword"
    sql: ${TABLE}.acq_keyword ;;
  }

  dimension: acq_medium {
    type: string
    group_label: "Acquisition / Marketing"
    description: "Acquisition Medium"
    label: "Medium"
    sql: ${TABLE}.acq_medium ;;
  }

  dimension: acq_source {
    type: string
    group_label: "Acquisition / Marketing"
    description: "Acquisition source"
    label: "Source"
    sql: ${TABLE}.acq_source ;;
  }

  dimension: marketing_source {
    type: string
    group_label: "Acquisition / Marketing"
    description: "Acquisition Platform"
    label: "Marketing Source"
    sql: ${TABLE}.marketing_source ;;
  }

##############################################
### End of Acquisition/Marketing dimensions ###}


##################################
#####   GA Measures          #####
##################################{

  measure: number_of_sessions {
    type: sum
    group_label: "GA Metrics"
    label: "# of Sessions"
    description: "Total number of sessions on the website"
    sql: ${TABLE}.sessions ;;
    filters: [record_type: "SALE", source: "GA"]
  }

  measure: percent_of_sessions {
    type: percent_of_total
    group_label: "GA Metrics"
    label: "% of Total Sessions"
    description: "Percentage of sessions from a particular dimension"
    value_format_name: decimal_2
    sql: ${number_of_sessions} ;;
  }

  measure: sum_session_duration {
    type: sum
    group_label: "GA Metrics"
    label: "Sessions duration"
    description: "Sum of session duration"
    sql: ${TABLE}.session_duration ;;
    filters: [record_type: "SALE", source: "GA"]
  }

  measure: sum_bounces {
    type: sum
    group_label: "GA Metrics"
    label: "# of Bounces"
    description: "Sum of bounces as recorded"
    sql: ${TABLE}.bounces ;;
    filters: [record_type: "SALE", source: "GA"]
  }

  measure: number_of_pageviews {
    type: sum
    group_label: "GA Metrics"
    label: "# of Page views"
    description: "Sum of page views"
    sql: ${TABLE}.pageviews ;;
    filters: [record_type: "SALE", source: "GA"]
  }

  measure: sum_unique_pageviews {
    type: sum
    group_label: "GA Metrics"
    label: "# of Unique Page views"
    description: "Sum of unique page views"
    sql: ${TABLE}.unique_pageviews ;;
  }

  measure: sum_product_adds_to_cart {
    type: sum
    group_label: "GA Metrics"
    label: "# of Products Added to Cart"
    description: "Total number of items added to cart for the selected dimension"
    sql: ${TABLE}.product_adds_to_cart ;;
    filters: [record_type: "SALE", source: "GA"]
  }

  measure: sum_product_checkouts {
    type: sum
    group_label: "GA Metrics"
    label: "# of Product Checkouts"
    description: "Total number of product checkouts"
    sql: ${TABLE}.product_checkouts ;;
    filters: [record_type: "SALE", source: "GA"]
  }

  measure: sum_users {
    type: sum
    group_label: "GA Metrics"
    label: "# of Users"
    description: "Total number of users"
    sql: ${TABLE}.users ;;
    filters: [record_type: "SALE", source: "GA"]
  }

  measure: pageviews_per_session {
    type: number
    group_label: "GA Metrics"
    label: "Pageviews per Session"
    description: "Average number of pages viewed per session"
    value_format_name: decimal_2
    sql: ${number_of_pageviews}/NULLIF(${number_of_sessions},0) ;;
  }


  measure: bounce_rate {
    type: number
    group_label: "GA Metrics"
    label: "Bounce Rate %"
    description: "Percentage of visitors who leave the website after only one page view"
    value_format_name: percent_2
    sql: ${sum_bounces}/NULLIF(${number_of_sessions},0) ;;
  }

  measure: avg_time_on_site {
    type: number
    group_label: "GA Metrics"
    label: "Avg Time On Site"
    description: "Average of time spend on the site in seconds per session"
    value_format_name: decimal_0
    sql: ${sum_session_duration}/NULLIF(${number_of_sessions},0);;
  }

  measure: add_to_basket_rate {
    type: number
    group_label: "GA Metrics"
    label: "Add to Basket %"
    description: "Percentage of product detail views that have added the product to their basket over a period of time"
    value_format_name: percent_2
    sql: ${sum_product_adds_to_cart}/NULLIF(${number_of_pageviews},0) ;;
  }

  measure: cart_abandonment_rate {
    type: number
    group_label: "GA Metrics"
    label: "Cart Abandonment %"
    description: "Number of cart created that did not convert into an order (1-% cart abandonment = cart completion rate)"
    value_format_name: percent_2
    sql: (${sum_product_adds_to_cart}-${sum_product_checkouts})/NULLIF(${sum_product_adds_to_cart},0) ;;
  }

  measure: checkout_to_add_rate {
    type: number
    group_label: "GA Metrics"
    label: "Checkout to Add to Basket %"
    description: "Products checkouts / Products added to cart"
    value_format_name: percent_2
    sql: ${sum_product_checkouts}/NULLIF(${sum_product_adds_to_cart},0) ;;
  }


  measure: conversion_rate {
    type: number
    group_label: "GA Metrics"
    label: "Conversion Rate (%)"
    description: "Percentage of sessions that make an order"
    value_format_name: percent_2
    sql: ${all_orders}/NULLIF(${number_of_sessions},0) ;;
  }

  ### End of GA Measures
  #}

#############################################
##########    SFCC CATEGORIES    ############
#############################################

  dimension: base_locale {
    type: string
    label: "Base Locale"
    group_label: "SFCC Categories"
    # description: ""
    sql: ${TABLE}.base_locale ;;
  }

  dimension: channel_type {
    type: string
    label: "Channel Type"
    group_label: "SFCC Categories"
    # description: ""
    sql: ${TABLE}.channel_type ;;
  }

  dimension: fulfillment_store {
    type: string
    label: "Fulfillment Store"
    group_label: "SFCC Categories"
    # description: ""
    sql: ${TABLE}.fultillment_store ;;
  }

  dimension: crm_customer_phone {
    type: string
    hidden: yes
    label: "Customer Phone number (CRM)"
    group_label: "SFCC Categories"
    # description: ""
    sql: ${TABLE}.crm_customer_phone ;;
  }

  dimension: special_request {
    type: string
    label: "Special Request"
    group_label: "SFCC Categories"
    # description: ""
    sql: ${TABLE}.special_request ;;
  }

  dimension: gift_wrap_type {
    type: string
    label: "Gift Wrap Type"
    group_label: "SFCC Categories"
    # description: ""
    sql: ${TABLE}.gift_wrap_type ;;
  }

  dimension: gift_message_indicator {
    type: string
    label: "Gift Message Indicator"
    group_label: "SFCC Categories"
    # description: ""
    sql: ${TABLE}.gift_message_indicator ;;
  }

  dimension: gift_wrap_description {
    type: string
    label: "Gift Wrap Description"
    group_label: "SFCC Categories"
    # description: ""
    sql: ${TABLE}.gift_wrap_description ;;
  }

  dimension: gift_message {
    type: string
    label: "Gift Message"
    group_label: "SFCC Categories"
    # description: ""
    sql: ${TABLE}.gift_message ;;
  }

  dimension: customer_id {
    type: string
    label: "Customer ID"
    group_label: "SFCC Categories"
    description: "SalesForce customer ID"
    sql: ${TABLE}.customer_id ;;
  }

  dimension: sale_detail_non_inventory {
    type: string
    label: "Non Inventory"
    group_label: "SFCC Categories"
    # description: ""
    sql: ${TABLE}.sale_detail_non_inventory ;;
  }

  dimension: payment_detail_tender_type_id {
    type: number
    label: "Payment Method ID"
    group_label: "SFCC Categories"
    # description: ""
    sql: ${TABLE}.payment_detail_tender_type_id ;;
  }


  dimension: payment_detail_cc_no {
    type: string
    label: "Credit Card No."
    group_label: "SFCC Categories"
    # description: ""
    sql: ${TABLE}.payment_detail_cc_no ;;
  }

  dimension: payment_detail_payment_id {
    type: string
    label: "Payment ID"
    group_label: "SFCC Categories"
    # description: ""
    sql: ${TABLE}.payment_detail_payment_id ;;
  }

  dimension: bill_to_detail_customer_fistname {
    type: string
    label: "Customer First Name"
    hidden: yes
    group_label: "SFCC Categories"
    # description: ""
    sql: ${TABLE}.bill_to_detail_customer_fistname ;;
  }

  dimension: bill_to_detail_customer_lastname {
    type: string
    label: "Customer Last Name"
    hidden: yes
    group_label: "SFCC Categories"
    # description: ""
    sql: ${TABLE}.bill_to_detail_customer_lastname ;;
  }

  dimension: bill_to_detail_customer_address {
    type: string
    label: "Customer Address"
    group_label: "SFCC Categories"
    description: "Customer Billing Address"
    sql: concat(${TABLE}.bill_to_detail_customer_address_1, ${TABLE}.bill_to_detail_customer_address_2) ;;
  }

  measure: charge_detail_shipping_charges {
    type: sum
    label: "Shipping Charges ($)"
    group_label: "SFCC Categories"
    # description: ""
    sql: ${TABLE}.charge_detail_shipping_charges ;;
  }

  # measure: charge_detail_shipping_tax_percentage {
  #   type: percentile
  #   label: "Shipping Charges (%)"
  #   group_label: "SFCC Categories"
  #   # description: ""
  #   sql: ${TABLE}.charge_detail_shipping_tax_percentage ;;
  # }

  measure: charge_detail_shipping_tax_amount {
    type: sum
    label: "Shipping Tax Amount"
    group_label: "SFCC Categories"
    # description: ""
    sql: ${TABLE}.charge_detail_shipping_tax_amount ;;
  }

  dimension: muse_detail_member_id {
    type: string
    label: "MUSE - Member ID"
    group_label: "SFCC Categories"
    # description: ""
    sql: ${TABLE}.muse_detail_member_id ;;
  }

  dimension: muse_detail_tier_level {
    type: string
    label: "MUSE - Tier Level"
    group_label: "SFCC Categories"
    # description: ""
    sql: ${TABLE}.muse_detail_tier_level ;;
  }

  measure: muse_detail_points_earned {
    type: sum
    label: "MUSE - Points Earned"
    group_label: "SFCC Categories"
    # description: ""
    sql: ${TABLE}.muse_detail_points_earned ;;
  }

  measure: sale_detail_muse_points {
    type: sum
    label: "MUSE - Points"
    group_label: "SFCC Categories"
    # description: ""
    sql: ${TABLE}.sale_detail_muse_points ;;
  }


##############################################
########## End of SFCC Categories ############}

###############################################
##########  Affiliate Coupon Codes   ##########
###############################################

  measure: affiliate_cost {
    type: sum
    value_format_name: usd
    label: "Affiliate Cost"
    group_label: "Funnel Metrics"
    description: "Cost paid to the affiliate for order using a coupon code"
    sql: CASE WHEN ${final_record_status} not in ("REFUND", "CANCELLED") THEN ${faces_coupon_code_affiliates.net_sales_commission}* ${TABLE}.ship_net_usd
              ELSE 0
         END;;
  }

  measure: affiliate_cost_packed {
    type: sum
    value_format_name: usd
    label: "Affiliate Cost (Packed Sales)"
    group_label: "Funnel Metrics"
    description: "Cost paid to the affiliate for order using a coupon code base on packed sales minus returns"
    sql: CASE WHEN ${final_record_status} not in ("REFUND", "CANCELLED") THEN ${faces_coupon_code_affiliates.net_sales_commission}* ${TABLE}.packed_sales_minus_return_usd
              ELSE 0
         END;;
  }

  ##############################################
  ####### End of Affiliate Coupon Codes ########}


  ##########################
  #######   ALERTS   #######
  ##########################

  dimension_group: record {
    type: time
    timeframes: [
      raw,
      second,
      minute,
      hour,
      date,
      week,
      month,
      quarter,
      year,
      week_of_year,
      day_of_week
    ]
    convert_tz: no
    datatype: datetime
    label: "Record"
    sql: ${TABLE}.record_datetime ;;
  }


  measure: minutes_since_last_update {
    type: min
    group_label: "Alerts"
    sql: DATETIME_DIFF(current_datetime(),cast(${common_raw} as DATETIME),minute)+240 ;;
  }
}
