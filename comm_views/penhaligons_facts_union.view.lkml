include: "../_common/period_over_period.view"


view: penhaligons_facts_union {
  sql_table_name: `chb-prod-data-comm.prod_commercial.penhaligons_facts_union`
    ;;

  dimension: primary_key {
    type: number
    hidden: yes
    primary_key: yes
    sql: ${TABLE}.primary_key ;;
  }

  dimension: brand_security{
    type: string
    hidden: yes
    sql: ${TABLE}.brand;;
  }

  dimension: vertical_security{
    type: string
    hidden: yes
    sql: ${TABLE}.vertical ;;
  }

  dimension: country_security{
    type: string
    hidden: yes
    sql: ${TABLE}.bu_country ;;
  }

  dimension_group: common_date {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year,
      day_of_month
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.common_date ;;
  }

  dimension: date_group {
    label: "Date Group"
    hidden: yes
    sql: {% if penhaligons_facts_union.common_date_date._in_query %} CAST(${TABLE}.common_date  AS DATE)
              {% elsif penhaligons_facts_union.common_date_month._in_query %} FORMAT_DATE('%Y-%m', ${TABLE}.common_date )
              {% elsif penhaligons_facts_union.common_date_quarter._in_query %} (FORMAT_TIMESTAMP('%Y-%m', TIMESTAMP_TRUNC(CAST(${TABLE}.common_date AS TIMESTAMP), QUARTER)))
              {% elsif penhaligons_facts_union.common_date_week._in_query %} CONCAT(CAST(EXTRACT(YEAR FROM ${TABLE}.common_date ) AS STRING),'-', CAST(EXTRACT(WEEK(SUNDAY) FROM ${TABLE}.common_date) AS STRING))
              {% elsif penhaligons_facts_union.common_date_year._in_query %} EXTRACT(YEAR FROM ${TABLE}.common_date )
              {% else %} CAST(${TABLE}.common_date  AS DATE)
              {% endif %} ;;
  }

  extends: [period_over_period]
  dimension_group: pop_no_tz {
    sql: ${common_date_raw} ;;
  }

  ######################################
  #####   sales order dimensions   #####
  ######################################

  dimension: canceled_amount_local {
    group_label: "Sales order"
    type: number
    hidden: yes
    sql: ${TABLE}.canceled_amount ;;
  }

  dimension: cancel_amount_incl_tax {
    group_label: "Sales order"
    type: number
    hidden: yes
    sql: ${TABLE}.cancel_amount_incl_tax ;;
  }

  dimension: cancel_amount_usd {
    group_label: "Sales order"
    type: number
    hidden: yes
    sql: ${TABLE}.cancel_amount_usd ;;
  }

  dimension: cancel_amount_usd_incl_tax {
    group_label: "Sales order"
    type: number
    hidden: yes
    sql: ${TABLE}.cancel_amount_usd_incl_tax ;;
  }

  dimension: gross_net_local {
    group_label: "Sales order"
    type: number
    hidden: yes
    sql: ${TABLE}.gross_net_local ;;
  }

  dimension: gross_net_local_incl_tax {
    group_label: "Sales order"
    type: number
    hidden: yes
    sql: ${TABLE}.gross_net_local_incl_tax ;;
  }

  dimension: gross_net_usd {
    group_label: "Sales order"
    type: number
    hidden: yes
    sql: ${TABLE}.gross_net_usd ;;
  }

  dimension: gross_net_usd_incl_tax {
    group_label: "Sales order"
    type: number
    hidden: yes
    sql: ${TABLE}.gross_net_usd_incl_tax ;;
  }

  dimension: gross_revenue_local {
    group_label: "Sales order"
    type: number
    hidden: yes
    sql: ${TABLE}.gross_revenue_local ;;
  }

  dimension: gross_revenue_local_incl_tax {
    group_label: "Sales order"
    type: number
    hidden: yes
    sql: ${TABLE}.gross_revenue_local_incl_tax ;;
  }

  dimension: gross_revenue_usd {
    group_label: "Sales order"
    type: number
    hidden: yes
    sql: ${TABLE}.gross_revenue_usd ;;
  }

  dimension: gross_revenue_usd_incl_tax {
    group_label: "Sales order"
    type: number
    hidden: yes
    sql: ${TABLE}.gross_revenue_usd_incl_tax ;;
  }

  dimension_group: order {
    group_label: "Sales order"
    hidden: yes
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.order_date ;;
  }

  dimension_group: order_datetime {
    group_label: "Sales order"
    hidden: yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.order_datetime AS TIMESTAMP) ;;
  }

  dimension: order_currency {
    group_label: "Sales order"
    type: string
    sql: ${TABLE}.order_currency ;;
  }

  dimension: order_id {
    group_label: "Sales order"
    type: string
    sql: ${TABLE}.order_id ;;
  }

  dimension: detail_id {
    group_label: "Sales order"
    hidden: yes
    type: number
    sql: cast(${TABLE}.detail_id as int64) ;;
  }

  dimension: order_payment_fee {
    group_label: "Sales order"
    label: "Order Payment Fee amount (Local)"
    type: number
    sql: ${TABLE}.order_payment_fee ;;
  }

  dimension: order_payment_fee_usd {
    group_label: "Sales order"
    label: "Order Payment Fee amount (USD)"
    type: number
    sql: ${TABLE}.order_payment_fee_usd ;;
  }

  dimension: order_tax {
    group_label: "Sales order"
    type: number
    sql: ${TABLE}.order_tax ;;
  }

  dimension: order_tax_usd {
    group_label: "Sales order"
    type: number
    sql: ${TABLE}.order_tax_usd ;;
  }


  dimension: order_total_usd {
    group_label: "Sales order"
    type: number
    sql: ${TABLE}.order_total_usd ;;
  }

  dimension: order_shipping {
    group_label: "Sales order"
    label: "Order Shipping amount (Local)"
    type: number
    sql: ${TABLE}.order_shipping ;;
  }

  dimension: delivery_type {
    group_label: "Sales order"
    type: string
    sql: ${TABLE}.delivery_type ;;
  }

  dimension: order_total {
    group_label: "Sales order"
    label: "Order Total Amount (Local)"
    type: number
    sql: ${TABLE}.order_total ;;
  }

  dimension: qty_ordered {
    group_label: "Sales order"
    hidden: yes
    type: number
    sql: ${TABLE}.qty_ordered ;;
  }

  dimension: qty_refunded {
    group_label: "Sales order"
    hidden: yes
    type: number
    sql: ${TABLE}.qty_refunded ;;
  }

  dimension: qty_canceled {
    group_label: "Sales order"
    hidden: yes
    type: number
    sql: ${TABLE}.qty_canceled ;;
  }

  dimension: qty_shipped {
    group_label: "Sales order"
    hidden: yes
    type: number
    sql: ${TABLE}.qty_shipped ;;
  }

  dimension: qty_final {
    group_label: "Sales order"
    hidden: yes
    type: number
    sql: ${TABLE}.qty_final ;;
  }

  dimension_group: record {
    group_label: "Sales order"
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.record_date ;;
  }

  dimension_group: record_datetime {
    group_label: "Sales order"
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.record_datetime AS TIMESTAMP) ;;
  }

  dimension: refund_amount_local {
    group_label: "Sales order"
    type: number
    sql: ${TABLE}.refund_amount_local ;;
  }

  dimension: refund_amount_local_incl_tax {
    group_label: "Sales order"
    type: number
    sql: ${TABLE}.refund_amount_local_incl_tax ;;
  }

  dimension: refund_amount_usd {
    group_label: "Sales order"
    type: number
    sql: ${TABLE}.refund_amount_usd ;;
  }

  dimension: refund_amount_usd_incl_tax {
    group_label: "Sales order"
    type: number
    sql: ${TABLE}.refund_amount_usd_incl_tax ;;
  }

  dimension: shipment_revenue_local {
    group_label: "Sales order"
    hidden: yes
    type: number
    sql: ${TABLE}.shipment_revenue_local ;;
  }

  dimension: shipment_revenue_local_incl_tax {
    group_label: "Sales order"
    hidden: yes
    type: number
    sql: ${TABLE}.shipment_revenue_local_incl_tax ;;
  }

  dimension: shipment_revenue_local_incl_tax_minus_refund {
    group_label: "Sales order"
    hidden: yes
    type: number
    sql: ${TABLE}.shipment_revenue_local_incl_tax_minus_refund ;;
  }

  dimension: shipment_revenue_local_minus_refund {
    group_label: "Sales order"
    hidden: yes
    type: number
    sql: ${TABLE}.shipment_revenue_local_minus_refund ;;
  }

  dimension: shipment_revenue_usd {
    group_label: "Sales order"
    hidden: yes
    type: number
    sql: ${TABLE}.shipment_revenue_usd ;;
  }

  dimension: shipment_revenue_usd_incl_tax {
    group_label: "Sales order"
    hidden: yes
    type: number
    sql: ${TABLE}.shipment_revenue_usd_incl_tax ;;
  }

  dimension: shipment_revenue_usd_incl_tax_minus_refund {
    group_label: "Sales order"
    hidden: yes
    type: number
    sql: ${TABLE}.shipment_revenue_usd_incl_tax_minus_refund ;;
  }

  dimension: shipment_revenue_usd_minus_refund {
    group_label: "Sales order"
    hidden: yes
    type: number
    sql: ${TABLE}.shipment_revenue_usd_minus_refund ;;
  }

  dimension: source {
    group_label: "Sales order"
    type: string
    sql: ${TABLE}.source ;;
  }

  dimension: status {
    group_label: "Sales order"
    type: string
    sql: ${TABLE}.status ;;
  }

  dimension: store_country {
    group_label: "Sales order"
    type: string
    sql: ${TABLE}.country ;;
  }

  dimension: store_id {
    group_label: "Sales order"
    type: number
    sql: ${TABLE}.store_id ;;
  }

  dimension: payment_status {
    group_label: "Sales order"
    type: string
    sql: ${TABLE}.payment_status ;;
  }

  dimension: tender_type_group {
    group_label: "Sales order"
    type: string
    sql: ${TABLE}.payment_method ;;
  }

  dimension: payment_method_title {
    group_label: "Sales order"
    type: string
    sql: ${TABLE}.payment_method_title ;;
  }

  dimension: month_name{
    type: string
    label: "Month Name"
    group_label: "Sales order"
    description: "MM"
    sql: format_date("%b", ${TABLE}.record_date);;
  }

  ########################################
  ### Sales Order item - Dimensions ###
  ########################################

  dimension: record_type {
    group_label: "Sales Order Item"
    description: "Categorizes the sales order item record. Shows either SALE, CANCELLED, REFUND, SHIPMENT, ..."
    type: string
    sql: ${TABLE}.record_type ;;
  }

  dimension: final_record_type {
    group_label: "Sales Order Item"
    type: string
    sql: ${TABLE}.final_record_type ;;
  }

  dimension: prices_include_tax {
    group_label: "Sales Order Item"
    type: string
    sql: ${TABLE}.prices_include_tax ;;
  }

  dimension: detail_discount_amount_original {
    group_label: "Sales Order Item"
    type: number
    hidden: yes
    sql: ${TABLE}.detail_discount_amount_original ;;
  }

  dimension: detail_discount_amount_usd {
    group_label: "Sales Order Item"
    type: number
    hidden: yes
    sql: ${TABLE}.detail_discount_amount_usd ;;
  }

  dimension: detail_discount_tax_amount_original {
    group_label: "Sales Order Item"
    type: number
    sql: ${TABLE}.detail_discount_tax_amount_original ;;
  }

  dimension: detail_discount_tax_amount_usd {
    group_label: "Sales Order Item"
    type: number
    sql: ${TABLE}.detail_discount_tax_amount_usd ;;
  }

  dimension: detail_discounted_price_original {
    group_label: "Sales Order Item"
    type: number
    sql: ${TABLE}.detail_discounted_price_original ;;
  }

  dimension: detail_discounted_price_usd {
    group_label: "Sales Order Item"
    type: number
    sql: ${TABLE}.detail_discounted_price_usd ;;
  }

  dimension: detail_discounted_tax_amount_original {
    group_label: "Sales Order Item"
    type: number
    sql: ${TABLE}.detail_discounted_tax_amount_original ;;
  }

  dimension: detail_discounted_tax_amount_usd {
    group_label: "Sales Order Item"
    type: number
    sql: ${TABLE}.detail_discounted_tax_amount_usd ;;
  }

  dimension: discount_amount_local {
    group_label: "Sales Order Item"
    type: number
    hidden: yes
    sql: ${TABLE}.discount_amount_local ;;
  }

  dimension: discount_amount_local_incl_tax {
    group_label: "Sales Order Item"
    type: number
    hidden: yes
    sql: ${TABLE}.discount_amount_local_incl_tax ;;
  }

  dimension: discount_amount_usd {
    group_label: "Sales Order Item"
    type: number
    hidden: yes
    sql: ${TABLE}.discount_amount_usd ;;
  }

  dimension: discount_amount_usd_incl_tax {
    group_label: "Sales Order Item"
    type: number
    hidden: yes
    sql: ${TABLE}.discount_amount_usd_incl_tax ;;
  }

  dimension: sku {
    group_label: "Sales Order Item"
    type: string
    sql: ${TABLE}.sku ;;
  }

  dimension: detail_name {
    group_label: "Sales Order Item"
    type: string
    sql: ${TABLE}.detail_name ;;
  }

  dimension: detail_name_original {
    group_label: "Sales Order Item"
    type: string
    sql: ${TABLE}.detail_name_original ;;
  }

  dimension: detail_item_id {
    group_label: "Sales Order Item"
    type: number
    sql: ${TABLE}.detail_item_id ;;
  }

  dimension: detail_price_usd {
    group_label: "Sales Order Item"
    label: "Item Price (USD)"
    type: number
    sql: ${TABLE}.detail_price_usd ;;
  }

  dimension: detail_tax_amount_original {
    group_label: "Sales Order Item"
    label: "Tax Amount (Original)"
    type: number
    sql: ${TABLE}.detail_tax_amount_original ;;
  }

  dimension: detail_tax_amount_usd {
    group_label: "Sales Order Item"
    label: "Tax Amount (USD)"
    type: number
    sql: ${TABLE}.detail_tax_amount_usd ;;
  }

  dimension: detail_price_original {
    group_label: "Sales Order Item"
    label: "Item Original Price (Local)"
    type: number
    sql: ${TABLE}.detail_price_original ;;
  }

  dimension: detail_tax {
    group_label: "Sales Order Item"
    label: "Tax rate"
    type: number
    sql: ${TABLE}.detail_tax ;;
  }

  dimension: detail_tax_amount {
    group_label: "Sales Order Item"
    label: "Tax Amount (Local)"
    type: number
    sql: ${TABLE}.detail_tax_amount ;;
  }


  ########################################
  ### Sales Order item Shipment - Dimensions ###
  ########################################

  dimension: shipment_city {
    group_label: "Sales Order Item Shipment"
    type: string
    sql: ${TABLE}.shipment_city ;;
  }

  dimension: shipping_city {
    group_label: "Sales Order Item Shipment"
    type: string
    sql: upper(${TABLE}.shipping_city) ;;
  }

  dimension: shipping_address_1 {
    group_label: "Sales Order Item Shipment"
    type: string
    sql: ${TABLE}.shipping_address_1 ;;
  }

  dimension: shipping_address_2 {
    group_label: "Sales Order Item Shipment"
    type: string
    sql: ${TABLE}.shipping_address_2 ;;
  }

  dimension: shipping_address_index {
    group_label: "Sales Order Item Shipment"
    type: string
    sql: ${TABLE}.shipping_address_index ;;
  }

  dimension: shipping_alt {
    group_label: "Sales Order Item Shipment"
    type: string
    sql: ${TABLE}.shipping_alt ;;
  }

  dimension: shipping_amount_usd {
    group_label: "Sales Order Item Shipment"
    type: number
    sql: ${TABLE}.shipping_amount_usd ;;
  }

  dimension: shipping_company {
    group_label: "Sales Order Item Shipment"
    type: string
    sql: ${TABLE}.shipping_company ;;
  }

  dimension: shipping_country {
    group_label: "Sales Order Item Shipment"
    type: string
    sql: ${TABLE}.shipping_country ;;
  }

  dimension: shipping_first_name {
    group_label: "Sales Order Item Shipment"
    type: string
    sql: ${TABLE}.shipping_first_name ;;
  }

  dimension: shipping_last_name {
    group_label: "Sales Order Item Shipment"
    type: string
    sql: ${TABLE}.shipping_last_name ;;
  }

  dimension: shipping_phone {
    group_label: "Sales Order Item Shipment"
    type: string
    sql: ${TABLE}.shipping_phone ;;
  }

  dimension: shipping_postcode {
    group_label: "Sales Order Item Shipment"
    type: string
    sql: ${TABLE}.shipping_postcode ;;
  }

  dimension: shipping_state {
    group_label: "Sales Order Item Shipment"
    type: string
    sql: ${TABLE}.shipping_state ;;
  }

  dimension: shipping_tax_amount_usd {
    group_label: "Sales Order Item Shipment"
    type: number
    sql: ${TABLE}.shipping_tax_amount_usd ;;
  }

  dimension: shipping_tax_original_amount {
    group_label: "Sales Order Item Shipment"
    type: number
    sql: ${TABLE}.shipping_tax_original_amount ;;
  }

  dimension: shipping_original_amount {
    group_label: "Sales Order Item Shipment"
    label: "Shipping Original Amount (Local)"
    type: number
    sql: ${TABLE}.shipping_original_amount ;;
  }

  ######################################
  ########    GA dimensions    #########
  ######################################

  dimension: sessions {
    hidden: yes
    group_label: "GA"
    type: number
    sql: ${TABLE}.sessions ;;
  }

  dimension: session_duration {
    hidden: yes
    group_label: "GA"
    type: number
    sql: ${TABLE}.session_duration ;;
  }

  dimension: bounces {
    hidden: yes
    group_label: "GA"
    type: number
    sql: ${TABLE}.bounces ;;
  }

  dimension: pageviews {
    hidden: yes
    group_label: "GA"
    type: number
    sql: ${TABLE}.pageviews ;;
  }

  dimension: product_adds_to_cart {
    hidden: yes
    group_label: "GA"
    type: number
    sql: ${TABLE}.product_adds_to_cart ;;
  }

  dimension: product_checkouts {
    hidden: yes
    group_label: "GA"
    type: number
    sql: ${TABLE}.product_checkouts ;;
  }

  dimension: users {
    hidden: yes
    group_label: "GA"
    type: number
    sql: ${TABLE}.users ;;
  }

  dimension: device_category {
    group_label: "Acquisition / Marketing"
    type: string
    sql: lower(${TABLE}.device_category) ;;
  }

  dimension: keyword {
    group_label: "Acquisition / Marketing"
    type: string
    sql: lower(${TABLE}.keyword) ;;
  }

  dimension: ga_source {
    group_label: "Acquisition / Marketing"
    type: string
    sql: lower(${TABLE}.ga_source) ;;
  }

  dimension: medium {
    group_label: "Acquisition / Marketing"
    type: string
    sql: lower(${TABLE}.medium) ;;
  }

  dimension: shipment_country {
    group_label: "Acquisition / Marketing"
    type: string
    sql: ${TABLE}.shipment_country ;;
  }

  dimension: marketing_source {
    type: string
    group_label: "Acquisition / Marketing"
    description: "Acquisition Platform"
    label: "Marketing Source"
    sql: ${TABLE}.marketing_source ;;
  }

  ######################################
  ######    funnel dimensions    #######
  ######################################

  dimension: campaign {
    group_label: "Acquisition / Marketing"
    type: string
    sql: lower(${TABLE}.campaign) ;;
  }

  dimension: common_cost_usd {
    hidden: yes
    group_label: "Funnel"
    type: number
    sql: ${TABLE}.common_cost_usd ;;
  }

  dimension: common_impressions {
    hidden: yes
    group_label: "Funnel"
    type: number
    sql: ${TABLE}.common_impressions ;;
  }

  dimension: common_clicks {
    hidden: yes
    group_label: "Funnel"
    type: number
    sql: ${TABLE}.common_clicks ;;
  }


  dimension: acq_campaign {
    group_label: "Acq"
    type: string
    sql: ${TABLE}.acq_campaign ;;
  }

  dimension: acq_device {
    group_label: "Acq"
    type: string
    sql: ${TABLE}.acq_device ;;
  }

  dimension: acq_keyword {
    group_label: "Acq"
    type: string
    sql: ${TABLE}.acq_keyword ;;
  }

  dimension: acq_medium {
    group_label: "Acq"
    type: string
    sql: ${TABLE}.acq_medium ;;
  }

  dimension: acq_source {
    group_label: "Acq"
    type: string
    sql: ${TABLE}.acq_source ;;
  }

  dimension: average_rating {
    group_label: "Acq"
    type: number
    sql: ${TABLE}.average_rating ;;
  }

  dimension_group: common_datetime {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.common_datetime ;;
  }

  dimension_group: completed {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.completed_date ;;
  }

  dimension: country {
    type: string
    map_layer_name: countries
    sql: ${TABLE}.country ;;
  }

  dimension: coupon_code {
    group_label: "Coupon codes"
    type: string
    sql: ${TABLE}.coupon_code ;;
  }

  dimension: created_via {
    type: string
    sql: ${TABLE}.created_via ;;
  }

  dimension: customer_email {
    group_label: "Customer"
    type: string
    sql: ${TABLE}.customer_email ;;
  }

  dimension: customer_phonenumber {
    group_label: "Customer"
    type: string
    sql: ${TABLE}.customer_phonenumber ;;
  }

  dimension: customer_user {
    group_label: "Customer"
    type: string
    sql: ${TABLE}.customer_user ;;
  }


  dimension: downloadable {
    type: yesno
    sql: ${TABLE}.downloadable ;;
  }


  dimension_group: local_completed {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.local_completed_date ;;
  }

  dimension_group: local_completed_datetime {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.local_completed_datetime ;;
  }

  dimension_group: local_order {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.local_order_date ;;
  }

  dimension_group: local_order_datetime {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.local_order_datetime ;;
  }

  dimension_group: local_paid {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.local_paid_date ;;
  }

  dimension_group: local_paid_datetime {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.local_paid_datetime ;;
  }

  dimension: onsale {
    type: yesno
    sql: ${TABLE}.onsale ;;
  }



  dimension_group: original_common {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.original_common_date ;;
  }

  dimension_group: original_common_datetime {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.original_common_datetime ;;
  }

  dimension_group: original_completed {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.original_completed_date ;;
  }

  dimension_group: original_order {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.original_order_date ;;
  }

  dimension_group: original_order_datetime {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.original_order_datetime ;;
  }

  dimension_group: original_paid {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.original_paid_date ;;
  }

  dimension_group: paid {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.paid_date ;;
  }

  dimension: post_title {
    type: string
    sql: ${TABLE}.post_title ;;
  }



  dimension: promo_action {
    type: string
    sql: ${TABLE}.promo_action ;;
  }

  dimension: rating_count {
    type: number
    sql: ${TABLE}.rating_count ;;
  }



  dimension: reduced_stock {
    type: number
    sql: ${TABLE}.reduced_stock ;;
  }


  dimension: sessions___fivetran_id {
    type: string
    sql: ${TABLE}.sessions._fivetran_id ;;
    group_label: "Sessions"
    group_item_label: "Fivetran ID"
  }

  dimension_group: sessions___fivetran_synced {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.sessions._fivetran_synced ;;
    group_label: "Sessions"
    group_item_label: "Fivetran Synced"
  }

  dimension: sessions__bounces {
    type: number
    sql: ${TABLE}.sessions.bounces ;;
    group_label: "Sessions"
    group_item_label: "Bounces"
  }

  dimension: sessions__campaign {
    type: string
    sql: ${TABLE}.sessions.campaign ;;
    group_label: "Sessions"
    group_item_label: "Campaign"
  }

  dimension_group: sessions_ {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.sessions.date ;;
    #X# Invalid LookML inside "dimension_group": {"group_label":null}
    group_item_label: "Sessions"
  }

  dimension: sessions__device_category {
    type: string
    sql: ${TABLE}.sessions.device_category ;;
    group_label: "Sessions"
    group_item_label: "Device Category"
  }

  dimension: sessions__keyword {
    type: string
    sql: ${TABLE}.sessions.keyword ;;
    group_label: "Sessions"
    group_item_label: "Keyword"
  }

  dimension: sessions__medium {
    type: string
    sql: ${TABLE}.sessions.medium ;;
    group_label: "Sessions"
    group_item_label: "Medium"
  }

  dimension: sessions__pageviews {
    type: number
    sql: ${TABLE}.sessions.pageviews ;;
    group_label: "Sessions"
    group_item_label: "Pageviews"
  }

  dimension: sessions__product_adds_to_cart {
    type: number
    sql: ${TABLE}.sessions.product_adds_to_cart ;;
    group_label: "Sessions"
    group_item_label: "Product Adds to Cart"
  }

  dimension: sessions__product_checkouts {
    type: number
    sql: ${TABLE}.sessions.product_checkouts ;;
    group_label: "Sessions"
    group_item_label: "Product Checkouts"
  }

  dimension: sessions__profile {
    type: string
    sql: ${TABLE}.sessions.profile ;;
    group_label: "Sessions"
    group_item_label: "Profile"
  }

  dimension: sessions__session_duration {
    type: number
    sql: ${TABLE}.sessions.session_duration ;;
    group_label: "Sessions"
    group_item_label: "Session Duration"
  }

  dimension: sessions__sessions {
    type: number
    sql: ${TABLE}.sessions.sessions ;;
    group_label: "Sessions"
    group_item_label: "Sessions"
  }

  dimension: sessions__source {
    type: string
    sql: ${TABLE}.sessions.source ;;
    group_label: "Sessions"
    group_item_label: "Source"
  }

  dimension: sessions__user_type {
    type: yesno
    sql: ${TABLE}.sessions.user_type ;;
    group_label: "Sessions"
    group_item_label: "User Type"
  }

  dimension: sessions__users {
    type: number
    sql: ${TABLE}.sessions.users ;;
    group_label: "Sessions"
    group_item_label: "Users"
  }

  dimension: user_country {
    type: string
    sql: ${TABLE}.user_country ;;
  }

  dimension: variation_id {
    type: string
    sql: ${TABLE}.variation_id ;;
  }


  ##############################################################################
  ##########################         MEASURES         ##########################
  ##############################################################################

  measure: sfcc_gross_revenue_usd {
    type: sum
    label: "Gross Revenue in USD"
    group_label: "Sales order"
    value_format_name: usd
    description: "Total value of incoming orders on the website (excluding failed credit card attempts, VAT, customs and shipping fees and COD fees) in USD"
    sql:  ${gross_revenue_usd} ;;
    filters: [record_type: "SALE"]
  }

  measure: sfcc_gross_cancelled_usd{
    type: sum
    hidden: yes
    label: "Cancelled orders ($)"
    group_label: "Sales order"
    description: "Total value of cancelled orders"
    sql: ${cancel_amount_usd};;
    filters: [record_type: "CANCELLED"]
  }

  measure: sfcc_gross_cancelled_items{
    type: sum
    label: "Cancelled Items ($)"
    group_label: "Sales order"
    description: "Total value of cancelled orders"
    sql: ${cancel_amount_usd};;
    filters: [record_type: "CANCELLED"]
  }

  measure: sfcc_gross_returns_usd {
    type: sum
    label: "Returned items ($)"
    group_label: "Sales order"
    value_format_name: usd
    description: "Total value of returned items"
    sql: ${refund_amount_usd};;
    filters: [record_type: "REFUND"]
  }

  measure: sfcc_gross_revenue_less_cancellatios_usd {
    type: number
    label: "Gross Revenue w/o Cancellations (USD)"
    group_label: "Sales order"
    description: "Total value of incoming orders on the website excluding cancellations (excluding failed credit card attempts, VAT, customs and shipping fees and COD fees)"
    value_format_name: usd
    sql: ${sfcc_gross_revenue_usd} - ${sfcc_gross_cancelled_usd} ;;
  }

  measure: sfcc_gross_revenue_less_cancellations_less_returns_usd {
    type: number
    label: "Gross Revenue w/o cancellation and returns  (USD)"
    group_label: "Sales order"
    value_format_name: usd
    description: "Total value of incoming orders on the website excluding cancellations and returns (excluding failed credit card attempts, VAT, customs and shipping fees and COD fees). To be computed based on order date"
    sql: ${sfcc_gross_revenue_usd} - ${sfcc_gross_cancelled_usd} - ${sfcc_gross_returns_usd} ;;
  }

  measure: sfcc_shipped_sales {
    type: sum
    label: "Shipped Sales"
    group_label: "Sales order"
    value_format_name: usd
    description: "Total value of shipped orders (excluding failed credit card attempts, VAT and cancellations but including customs and shipping fees)"
    sql: ${shipment_revenue_usd};;
    filters: [record_type: "SHIPMENT"]
  }

  measure: sfcc_shipped_net_sales {
    type: number
    label: "Shipped Net Sales USD"
    group_label: "Sales order"
    value_format_name: usd
    description: "Total value of incoming orders on the website excluding cancellations and returns (excluding failed credit card attempts, VAT, customs and shipping fees and COD fees). To be computed based on shipping date"
    sql: ${sfcc_shipped_sales} - ${sfcc_gross_returns_usd} ;;
  }

  measure: sfcc_number_of_orders  {
    type: count_distinct
    label: "# of Orders"
    group_label: "Sales order"
    description: "Total number of orders placed on the website"
    sql: ${order_id} ;;
  }

  measure: count_cancelled_orders {
    type: count_distinct
    hidden:  yes
    label: "# of Cancelled Orders"
    group_label: "Sales order"
    description: "Total number of Cancelled Orders"
    sql: ${order_id};;
    filters: [record_type: "CANCELLED"]
  }

  measure: count_returned_orders {
    type: count_distinct
    label: "# of Returned Orders"
    hidden: yes
    group_label: "Sales order"
    description: "Total number of Returned Orders"
    sql: ${order_id};;
    filters: [record_type: "REFUND"]
  }

  measure: units_sold {
    type: sum
    label: "# of Units Sold"
    group_label: "Sales order"
    description: "Total number of items ordered on the website"
    sql: ${qty_ordered};;
    filters: [record_type: "SALE"]
  }

  measure: upt {
    type: number
    label: "UPT"
    group_label: "Sales order"
    description: "Average number of units within each order"
    value_format_name: decimal_2
    sql:  NULLIF(${units_sold}/NULLIF(${sfcc_number_of_orders},0),0);;
  }

  measure: aov {
    type: number
    label: "Average Order Value (USD)"
    group_label: "Sales order"
    description: "Average Value of each order"
    value_format_name: usd
    sql: NULLIF(${sfcc_gross_revenue_usd}/NULLIF(${sfcc_number_of_orders},0),0);;
  }

  measure: unit_selling_price {
    type:  number
    label: "USP"
    group_label: "Sales order"
    description: "Average price of each item ordered"
    value_format_name: usd
    sql: NULLIF(${sfcc_gross_revenue_usd} / NULLIF(${units_sold},0),0) ;;
  }

  measure: sfcc_return_rate {
    type: number
    label: "Returned Rate (%)"
    group_label: "Sales order"
    description: "Percentage of shipped items that were returned based on value"
    value_format_name: percent_2
    sql: NULLIF(${sfcc_gross_returns_usd}/NULLIF(${sfcc_shipped_sales},0),0) ;;
  }

  measure: sfcc_return_order {
    type: number
    hidden: yes
    label: "Returned Order (%)"
    group_label: "Sales order"
    description: "Percentage of shipped orders that were returned"
    value_format_name: percent_2
    sql: NULLIF(${count_returned_orders}/NULLIF(${sfcc_number_of_orders},0),0) ;;
  }

  measure: sfcc_return_item {
    type: number
    label: "Returned Items (%)"
    group_label: "Sales order"
    description: "Percentage of shipped items that were returned"
    value_format_name: percent_2
    sql: NULLIF(${qty_refunded}/NULLIF(${qty_shipped},0),0) ;;
  }

  measure: sfcc_cancellation_rate{
    type: number
    label: "Cancelled Rate (%)"
    group_label: "Sales order"
    description: "Percentage of incoming items that were cancelled based on value"
    value_format_name: percent_2
    sql: NULLIF(${sfcc_gross_cancelled_usd}/NULLIF(${sfcc_gross_revenue_usd},0),0) ;;
  }

  measure: sfcc_cancellation_order{
    type: number
    hidden: yes
    label: "Cancelled Order (%)"
    group_label: "Sales order"
    description: "Percentage of incoming orders that were cancelled"
    value_format_name: percent_2
    sql: NULLIF(${count_cancelled_orders}/NULLIF(${sfcc_number_of_orders},0),0) ;;
  }

  measure: sfcc_cancellation_item{
    type: number
    hidden: yes
    label: "Cancelled Item (%)"
    group_label: "Sales order"
    description: "Percentage of incoming items that were cancelled"
    value_format_name: percent_2
    sql: NULLIF(${qty_canceled}/NULLIF(${qty_ordered},0),0) ;;
  }

  measure: units_sold_cancelled {
    type: sum
    label: "# of Cancelled Units Sold"
    group_label: "Sales order"
    description: "Sum of Total Qty Ordered Cancelled"
    sql: ${qty_canceled};;
  }

  measure: units_sold_returned {
    type: sum
    label: "# of Returned Units Sold"
    group_label: "Sales order"
    description: "Total number of items returned"
    sql: ${qty_refunded};;
  }

  measure: units_sold_shipped {
    type: sum
    label: "# of Shipped Units Sold"
    group_label: "Sales order"
    description: "Total number of items shipped"
    sql: ${qty_shipped};;
  }

  measure: contribution {
    type:  percent_of_total
    label: "Contribution"
    description: "Percentage of the total market value"
    sql: ${sfcc_gross_revenue_less_cancellations_less_returns_usd} ;;
  }

  ######################################
  ########     GA measures     #########
  ######################################

  measure: session_count{
    label: "# of Sessions"
    group_label: "GA metrics"
    type: sum
    description: "Total number of sessions on the website"
    sql: CASE when ${sessions} is null then 0
        Else ${sessions}
        END;;
  }

  measure: bounce_count {
    label: "# of bounces"
    group_label: "GA metrics"
    type: sum
    description: "Total number of visitors who leave the website after only one page view"
    sql: CASE when ${bounces} is null then 0
          Else ${bounces}
          END;;
  }

  measure: bounce_rate {
    label: "Bounce Rate %"
    group_label: "GA metrics"
    type: number
    description: "Percentage of visitors who leave the website after only one page view"
    value_format_name: percent_2
    sql: NULLIF(${bounce_count},0)/NULLIF(${session_count},0) ;;
  }

  measure: pageviews_per_session {
    type: number
    label: "Pageviews per Session"
    group_label: "GA metrics"
    description: "Average number of pages viewed per session"
    value_format_name: decimal_2
    sql: ${number_of_pageviews}/NULLIF(${session_count},0) ;;
  }

  measure: avg_time_on_site {
    type: number
    label: "Avg Time On Site"
    group_label: "GA metrics"
    description: "Average of time spend on the site in seconds per session"
    value_format: "h:mm:ss"
    sql: ${sum_session_duration}/NULLIF(${session_count},0)/86400 ;;
  }

  measure: add_to_basket_rate {
    type: number
    label: "Add to Basket %"
    group_label: "GA metrics"
    description: "Percentage of product detail views that have added the product to their basket over a period of time"
    value_format_name: percent_2
    sql: ${sum_product_adds_to_cart}/NULLIF(${number_of_pageviews},0) ;;
  }

  measure: cart_abandonment_rate {
    type: number
    label: "Cart Abandonment %"
    group_label: "GA metrics"
    description: "Percentage of cart created that did not convert into an order (1-% cart abandonment = cart compeltion rate)"
    value_format_name: percent_2
    sql: (${sum_product_adds_to_cart}-${sum_product_checkouts})/NULLIF(${sum_product_adds_to_cart},0) ;;
  }

  measure: checkout_to_add_rate {
    type: number
    label: "Checkout to Add to Basket %"
    group_label: "GA metrics"
    description: "Percentage of products that were added to cart and converted into an order"
    value_format_name: percent_2
    sql: ${sum_product_checkouts}/NULLIF(${sum_product_adds_to_cart},0) ;;
  }

  measure: sfcc_conversion_rate {
    type: number
    label: "Conversion Rate"
    group_label: "GA metrics"
    description: "Percentage of sessions that make an order"
    value_format_name: percent_2
    sql: NULLIF(${sfcc_number_of_orders} / NULLIF(${session_count},0),0) ;;
  }

  ######################################
  ######     funnel measures     #######
  ######################################

  measure: total_cost_usd {
    type: sum
    label: "Total Ad Spend (USD)"
    group_label: "Funnel metrics"
    value_format_name: usd
    description: "Total value of marketing ads spent across all different channels"
    sql: ${common_cost_usd};;
  }

  measure: total_clicks {
    type: sum
    label: "Total clicks"
    group_label: "Funnel metrics"
    hidden: yes
    description: "Total number of times an ad is clicked by a user"
    sql: ${common_clicks} ;;
  }

  measure: cost_per_click {
    type: number
    label: "CPC"
    group_label: "Funnel metrics"
    description: "The marketing cost of one ad click"
    value_format_name: usd
    sql: ${total_cost_usd} / NULLIF(${total_clicks},0) ;;
  }

  measure: click_through_rate {
    type: number
    label: "Click Through Rate %"
    group_label: "Funnel metrics"
    description: "Percentage ad impressions that have led to a click"
    value_format_name: percent_2
    sql: NULLIF(${total_clicks},0) / NULLIF(${impressions},0);;
  }

  measure: cost_per_mill {
    type: number
    label: "CPM"
    group_label: "Funnel metrics"
    description: "The marketing cost of 1000 impressions"
    value_format_name: usd
    sql: NULLIF(NULLIF(${total_cost_usd},0) / NULLIF(${impressions}/1000,0),0) ;;
  }

  measure: clicks_per_day {
    type: number
    value_format_name: decimal_2
    label: "Clicks per Day"
    group_label: "Funnel metrics"
    description: "Average number of clicks in a day for a given period
    "
    sql: ${total_clicks} / NULLIF(${number_of_days},0) ;;
  }

  measure: cost_per_day {
    type: number
    value_format_name: decimal_2
    label: "Cost per Day"
    description: "Average ad spend in value in a day for a given period"
    group_label: "Funnel metrics"
    sql: ${total_cost_usd} / NULLIF(${number_of_days},0) ;;
  }

  measure: sfcc_roas {
    type: number
    label: "ROAS"
    group_label: "Funnel metrics"
    description: "The Return in Gross Sales coming from your Total Ad Spend (based on Gross Sales)"
    value_format_name: decimal_2
    sql: NULLIF(${sfcc_gross_revenue_usd} / NULLIF(${total_cost_usd},0),0) ;;
  }

  measure: sfcc_cpo {
    type: number
    label: "CPO"
    description: "The marketing cost of one order"
    group_label: "Funnel metrics"
    value_format_name: usd
    sql: NULLIF(${total_cost_usd} / NULLIF(${sfcc_number_of_orders},0),0) ;;
  }

  measure: cost_per_session {
    type: number
    value_format_name: decimal_2
    label: "Cost per Session"
    group_label: "Funnel metrics"
    description: "The marketing cost of one website session"
    sql: NULLIF(${total_cost_usd} / NULLIF(${session_count},0),0) ;;
  }

  # measure: cost_of_acquisition{
  #   type: number
  #   value_format_name: decimal_2
  #   group_label: "Funnel metrics"
  #   label: "Cost of Acquisition"
  #   description: "The marketing cost of one acquired customer"
  #   sql: NULLIF(${total_cost_usd} / NULLIF(${ecomm_customer_type_pop.new_cust_count_fixed},0),0) ;;
  # }

  #########################
  #### UNUSED MEASURES ####
  #########################

  measure: sum_session_duration {
    type: sum
    hidden: yes
    label: "Sessions duration"
    sql: ${TABLE}.session_duration ;;
  }

  measure: number_of_pageviews {
    type: sum
    hidden: yes
    label: "# of Page views"
    sql: ${TABLE}.pageviews ;;
  }

  measure: sum_unique_pageviews {
    type: sum
    hidden: yes
    label: "# of Unique Page views"
    sql: ${TABLE}.unique_pageviews ;;
  }

  measure: sum_product_adds_to_cart {
    type: sum
    hidden: yes
    label: "# of Products Added to Cart"
    sql: ${TABLE}.product_adds_to_cart ;;
  }

  measure: sum_product_checkouts {
    type: sum
    hidden: yes
    label: "# of Product Checkouts"
    sql: ${TABLE}.product_checkouts ;;
  }

  measure: sum_users {
    type: sum
    hidden: yes
    label: "# of Users"
    sql: ${TABLE}.users ;;
  }

  measure: impressions {
    type: sum
    hidden: yes
    label: "# of Impressions"
    sql:  ${common_impressions};;
  }

  measure: number_of_days {
    type: count_distinct
    hidden: yes
    sql: ${common_date_date} ;;
  }

  measure: count {
    type: count
    drill_fields: [detail_name, shipping_first_name, shipping_last_name]
  }



}
