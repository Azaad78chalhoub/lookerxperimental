include: "../_common/period_over_period.view"
include: "../customer_views/ecomm_customer_new_returning.view"

view: tumi_facts_union {
  sql_table_name: `chb-prod-data-comm.prod_commercial.tumi_facts_union`
    ;;


  # dimension: date_group {
  #   label: "Date Group"
  #   hidden: yes
  #   sql: {% if tryano.common_date._in_query %} CAST(${TABLE}.common_date  AS DATE)
  #     {% elsif tryano.common_month._in_query %} FORMAT_DATE('%Y-%m', ${TABLE}.common_date )
  #     {% elsif tryano.common_quarter._in_query %} (FORMAT_TIMESTAMP('%Y-%m', TIMESTAMP_TRUNC(CAST(${TABLE}.common_date AS TIMESTAMP), QUARTER)))
  #     {% elsif tryano.common_week._in_query %} CONCAT(CAST(EXTRACT(YEAR FROM ${TABLE}.common_date ) AS STRING),'-', CAST(EXTRACT(WEEK(SUNDAY) FROM ${TABLE}.common_date) AS STRING))
  #     {% elsif tryano.common_year._in_query %} EXTRACT(YEAR FROM ${TABLE}.common_date )
  #     {% else %} CAST(${TABLE}.common_date  AS DATE)
  #     {% endif %} ;;
  # }


########## ACCESS & SECURITY ############
  dimension: brand_security{
    type: string
    hidden: yes
    sql: ${TABLE}.brand ;;
  }

  dimension: vertical_security{
    type: string
    hidden: yes
    sql: ${TABLE}.vertical ;;
  }

  dimension: country_security{
    type: string
    hidden: yes
    sql: CASE WHEN ${store_country} = "United Arab Emirates" THEN "UAE"
              WHEN ${store_country} = "Saudi Arabia" THEN "KSA"
              WHEN ${store_country} = "Kuwait" THEN "KWT" END;;
  }

###############################################
########## End of Access & Security ###########


############ PERIOD OVER PERIOD ################
  extends: [period_over_period]

  dimension_group: pop_no_tz {
    sql: ${common_raw} ;;
  }

##############################################
######### End of Period over Period ##########

  dimension: brand {
    type: string
    sql: ${TABLE}.brand ;;
  }

  dimension: store_country {
    type: string
    label: "Country"
    sql: ${TABLE}.store_country ;;
  }

  dimension: store_id {
    type: string
    sql: ${TABLE}.store_id ;;
  }


#######################################
########## Dates Dimensions ###########
#######################################

  dimension_group: common {
    type: time
    hidden: yes
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.common_date ;;
  }

  dimension_group: common_datetime {
    type: time
    timeframes: [
      raw,
      date,
      week,
      time,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: timestamp
    label: "Date"
    description: "Date when a specific even (Record Type) took place"
    sql: cast(${TABLE}.common_datetime as timestamp) ;;
  }


  dimension_group: order {
    type: time
    hidden: yes
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.order_date ;;
  }

  dimension_group: order_datetime {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.order_datetime ;;
  }

  dimension_group: record {
    type: time
    hidden: yes
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.record_date ;;
  }

  dimension_group: record_datetime {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.record_datetime ;;
  }

##############################################
########## End of Dates dimensions ###########


##############################################
###### Acquisition/Marketing dimensions ######
##############################################

  dimension: acq_campaign {
    type: string
    group_label: "Acquisition / Marketing"
    label: "Campaign"
    description: "Marketing Campaign name"
    sql: ${TABLE}.acq_campaign ;;
  }

  dimension: acq_device {
    type: string
    group_label: "Acquisition / Marketing"
    label: "Device"
    description: "The type of device where the session iniated from: desktop, tablet, or mobile."
    sql: ${TABLE}.acq_device ;;
  }

  dimension: acq_keyword {
    type: string
    group_label: "Acquisition / Marketing"
    label: "Keyword"
    description: "For manual campaign tracking, it is the value of the utm_term campaign tracking parameter. For AdWords traffic, it contains the best matching targeting criteria. For the display network, where multiple targeting criteria could have caused the ad to show up, it returns the best matching targeting criteria as selected by Ads. This could be display_keyword, site placement, boomuserlist, user_interest, age, or gender. Otherwise its value is (not set)."
    sql: ${TABLE}.acq_keyword ;;
  }

  dimension: acq_medium {
    type: string
    group_label: "Acquisition / Marketing"
    label: "Medium"
    description: "The type of referrals. For manual campaign tracking, it is the value of the utm_medium campaign tracking parameter. For AdWords autotagging, it is cpc. If users came from a search engine detected by Google Analytics, it is organic. If the referrer is not a search engine, it is referral. If users came directly to the property and document.referrer is empty, its value is (none)."
    sql: ${TABLE}.acq_medium ;;
  }

  dimension: acq_source {
    type: string
    group_label: "Acquisition / Marketing"
    label: "Source"
    description: "The source of referrals. For manual campaign tracking, it is the value of the utm_source campaign tracking parameter. For AdWords autotagging, it is google. If you use neither, it is the domain of the source (e.g., document.referrer) referring the users. It may also contain a port address. If users arrived without a referrer, its value is (direct)."
    sql: ${TABLE}.acq_source ;;
  }

  dimension: marketing_source {
    type: string
    group_label: "Acquisition / Marketing"
    description: "Acquisition Platform"
    label: "Marketing Source"
    sql: ${TABLE}.marketing_source ;;
  }


##############################################
### End of Acquisition/Marketing dimensions ###}



##############################################
########### Sales Order dimensions ###########
##############################################



  dimension: city {
    type: string
    group_label: "Sales Order"
    description: "City where the order has been delivered"
    sql: ${TABLE}.city ;;
  }

  dimension: currency {
    type: string
    group_label: "Sales Order"
    description: "Currency in which the order has been placed"
    sql: ${TABLE}.currency ;;
  }

  dimension: entity_id {
    type: number
    group_label: "Sales Order"
    sql: ${TABLE}.entity_id ;;
  }

  dimension: external_order_id {
    type: number
    group_label: "Sales Order"
    description: "Alternate order ID shown in Shopify"
    sql: ${TABLE}.external_order_id ;;
  }

  dimension: order_id {
    type: string
    group_label: "Sales Order"
    description: "Order ID shown in Shopify"
    sql: ${TABLE}.order_id ;;
  }

  dimension: payment_method {
    type: string
    group_label: "Sales Order"
    description: "Method in which the payment has been made"
    sql: ${TABLE}.payment_method ;;
  }

  dimension: record_type {
    type: string
    group_label: "Sales Order"
    description: "Categorizes the sales order item record. Shows either SALE, CANCELLED, REFUND, SHIPMENT"
    sql: ${TABLE}.record_type ;;
  }

  dimension: customer_email {
    type: string
    hidden: yes
    group_label: "Sales Order"
    sql: ${TABLE}.customer_email ;;
  }

##############################################
####### End of Sales Order dimensions ########



##############################################
######## Sales Order Item dimensions #########
##############################################

  dimension: item_comments {
    type: string
    group_label: "Sales Order Item"
    sql: ${TABLE}.item_comments ;;
  }

  dimension: sku {
    type: string
    group_label: "Sales Order Item"
    sql: ${TABLE}.sku ;;
  }


##############################################
#### End of Sales Order Item dimensions ######


###################################
##     Sales Order Measures      ##
###################################{


##################### SALE #####################


  measure: count_sales_orders {
    type: count_distinct
    group_label: "Sales Metrics"
    label: "# of Sales Orders"
    description: "Total number of orders placed on the website"
    sql: ${order_id};;
    filters: [record_type: "SALE"]
  }

  measure: units_ordered {
    type: sum
    group_label: "Sales Metrics"
    label: "Units Ordered"
    description: "Total number of items ordered on the website"
    sql: ${qty_ordered};;
  }

  measure: aov_usd {
    type: number
    group_label: "Sales Metrics"
    label: "Average Order Value (USD)"
    description: "Average Value of each order in USD"
    value_format: "$#,##0.00"
    sql:${gross_rev_usd}/nullif(${count_sales_orders}, 0);;
  }

  measure: aov_original {
    type: number
    group_label: "Sales Metrics"
    label: "Average Order Value (Local)"
    description: "Average Value of each order in local currency"
    value_format: "#,##0.00"
    sql: ${gross_rev_original}/nullif(${count_sales_orders}, 0);;
  }

  measure: gross_net_original {
    type: sum
    group_label: "Sales Metrics"
    label: "Gross Revenue w/o cancellations & returns (Local)"
    description: "In Local currency"
    value_format: "#,##0.00"
    sql: ${gross_net_sales};;
  }

  measure: gross_net_usd {
    type: sum
    group_label: "Sales Metrics"
    label: "Gross Revenue w/o cancellations & returns (USD)"
    description: "Total value of incoming orders on the website excluding cancellations and returns (excluding failed credit card attempts, VAT, customs and shipping fees and COD fees). To be computed based on order date"
    value_format: "$#,##0.00"
    sql:${gross_net_sales_usd};;
  }

  measure: gross_minus_cancelled_original {
    type: sum
    group_label: "Sales Metrics"
    label: "Gross Revenue w/o cancellations (Local)"
    description: "Total value of incoming orders on the website excluding cancellations (excluding failed credit card attempts, VAT, customs and shipping fees and COD fees). To be computed based on order date"
    value_format: "#,##0.00"
    sql: ${gross_minus_can_local};;
  }

  measure: gross_minus_cancelled_usd {
    type: sum
    group_label: "Sales Metrics"
    label: "Gross Revenue w/o cancellations (USD)"
    description: "Total value of incoming orders on the website excluding cancellations (excluding failed credit card attempts, VAT, customs and shipping fees and COD fees). To be computed based on order date"
    value_format: "$#,##0.00"
    sql: ${gross_minus_can_usd};;
  }

  measure: gross_rev_original {
    type: sum
    group_label: "Sales Metrics"
    label: "Gross Revenue (Local)"
    description: "Total value of incoming orders on the website in Local currency (excluding VAT, discount, customs and shipping fees and COD fees)"
    value_format: "#,##0.00"
    sql:  ${gross_local};;
  }

  measure: gross_rev_usd {
    type: sum
    group_label: "Sales Metrics"
    label: "Gross Revenue (USD)"
    description: "Total value of incoming orders on the website (excluding VAT, discount, customs and shipping fees and COD fees)"
    value_format: "$#,##0.00"
    sql: ${gross_usd};;
  }

  measure: upt {
    type: number
    group_label: "Sales Metrics"
    label: "UPT"
    description: "Average number of units within each order"
    value_format_name: decimal_2
    sql:${units_ordered}/nullif(${count_sales_orders}, 0);;
  }

  measure: usp {
    type:  number
    label: "USP (USD)"
    group_label: "Sales Metrics"
    description: "Unit Selling Price = Average price of each item ordered (in USD)"
    value_format: "$#,##0.00"
    sql: ${gross_rev_usd} / NULLIF(${units_ordered},0) ;;
  }

  measure: usp_original {
    type:  number
    label: "USP (Local)"
    group_label: "Sales Metrics"
    description: "Unit Selling Price = Average price of each item ordered (in Local currency)"
    value_format: "#,##0.00"
    sql: ${gross_rev_original} / NULLIF(${units_ordered},0) ;;
  }

  measure: units_ordered_minus_cancelled {
    type: number
    group_label: "Sales Metrics"
    label: "Units Ordered minus Cancelled"
    description: "Total number of ordered items excluding cancelled items"
    sql: ${units_ordered} - ${unit_canceled} ;;
  }

  measure: units_ordered_minus_cancelled_minus_returned{
    type: number
    group_label: "Sales Metrics"
    label: "Units Ordered minus Cancelled & Returns"
    description: "Total number of ordered items excluding cancelled and returned items"
    sql: ${units_ordered} - ${units_returned} - ${unit_canceled} ;;
  }

##################### CANCELLATIONS #####################


  measure: total_cancelled_orders {
    type: count_distinct
    hidden:  yes
    group_label: "Sales Metrics"
    label: "# of Cancelled Orders"
    description: "Total number of orders that are partially or fully cancelled"
    sql: ${order_id};;
    filters: [record_type: "CANCELLED"]
  }

  measure: unit_canceled {
    type: sum
    group_label: "Sales Metrics"
    label: "Units Canceled"
    description: "Total number of canceled items"
    sql: ${qty_canceled};;
  }

  measure: cancel_amount_usd {
    type: sum
    group_label: "Sales Metrics"
    label: "Cancelled Amount (USD)"
    description: "Total value of incoming orders that are cancelled in USD"
    value_format: "$#,##0.00"
    sql: ${canceled_amount_usd};;
  }

  measure: cancel_amount_original {
    type: sum
    group_label: "Sales Metrics"
    label: "Cancelled Amount (Local)"
    description: "Total value of incoming orders that are cancelled in Local currency"
    value_format: "#,##0.00"
    sql: ${canceled_amount_local};;
  }

  measure: cancel_rate_item {
    type: number
    group_label: "Sales Metrics"
    label: "Cancellation Rate (% item)"
    description: "Percentage of incoming items quantity that were cancelled out of total incoming items quantity"
    value_format_name: percent_2
    sql: nullif(${unit_canceled}/nullif(${units_ordered}, 0),0);;
  }

  measure: cancel_rate_value {
    type: number
    group_label: "Sales Metrics"
    label: "Cancellation Rate (% value)"
    description: "Percentage of incoming items that were cancelled"
    value_format_name: percent_2
    sql: ${cancel_amount_usd}/nullif(${gross_rev_usd}, 0);;
  }


##################### SHIPPED #####################


  measure: total_shipped_orders {
    type: count_distinct
    group_label: "Sales Metrics"
    label: "# of Shipped Orders"
    description: "Total number of orders that are partially or fully shipped"
    sql: ${order_id};;
    filters: [record_type: "SHIPMENT"]
  }

  measure: units_shipped {
    type: sum
    group_label: "Sales Metrics"
    label: "Units Shipped"
    description: "Total number of shipped items"
    sql: ${qty_shipped};;
  }

  measure: net_units_shipped {
    type: number
    group_label: "Sales Metrics"
    label: "Units Shipped minus Returns"
    description: "Total number of shipped items excluding returned items"
    sql: ${units_shipped} - ${units_returned} ;;
  }

  measure: shipped_amount_original {
    type: sum
    group_label: "Sales Metrics"
    label: "Shipped Revenue (Local)"
    description: "Total value of shipped orders (excluding failed credit card attempts, VAT and cancellations but including customs and shipping fees)"
    value_format: "#,##0.00"
    sql: ${shipped_local};;
  }

  measure: shipped_amount_usd {
    type: sum
    group_label: "Sales Metrics"
    label: "Shipped Revenue (USD)"
    description: "Total value of shipped orders (excluding failed credit card attempts, VAT and cancellations but including customs and shipping fees)"
    value_format: "$#,##0.00"
    sql: ${shipped_usd};;
  }

  measure: net_sales_usd {
    type: sum
    group_label: "Sales Metrics"
    label: "Shipped Net Sales (USD)"
    description: "Total value of incoming orders on the website excluding returns (excluding failed credit card attempts, VAT, customs and shipping fees and COD fees). To be computed based on shipping date"
    value_format: "$#,##0.00"
    sql: ${shipment_net_sales_usd};;
  }

  measure: net_sales_original {
    type: sum
    group_label: "Sales Metrics"
    label: "Shipped Net Sales (Local)"
    description: "Total value of incoming orders on the website excluding returns (excluding failed credit card attempts, VAT, customs and shipping fees and COD fees). To be computed based on shipping date"
    value_format: "#,##0.00"
    sql: ${shipment_net_sales};;
  }


##################### RETURNS #####################


  measure: units_returned {
    type: sum
    group_label: "Sales Metrics"
    label: "Units Returned"
    description: "Total numer of returned items"
    sql: ${qty_refunded};;
  }

  measure: total_return_orders {
    type: count_distinct
    hidden:  yes
    group_label: "Sales Metrics"
    label: "# of Returned Orders"
    description: "Total value of orders that are partially or fully returned"
    sql: ${order_id};;
    filters: [record_type: "REFUND"]
  }

  measure: amount_refunded_usd {
    type: sum
    group_label: "Sales Metrics"
    label: "Returned Amount (USD)"
    description: "Total value of orders that are returned in USD"
    value_format: "$#,##0.00"
    sql: ${refund_amount_usd};;
  }

  measure: amount_refunded_original {
    type: sum
    group_label: "Sales Metrics"
    label: "Returned Amount (Local)"
    description: "Total value of orders that are returned in USD"
    value_format: "#,##0.00"
    sql: ${refund_amount_local};;
  }

  measure: return_rate_value {
    type: number
    group_label: "Sales Metrics"
    label: "Returned Rate (% Shipped value)"
    description: "Percentage of shipped items that were returned"
    value_format_name: percent_2
    sql: ${amount_refunded_usd}/nullif(${shipped_amount_usd}, 0);;
  }

  measure: return_rate_item {
    type: number
    group_label: "Sales Metrics"
    label: "Returned Rate (% items)"
    description: "Percentage of shipped items quantity that were returned out of total shipped items quantity"
    value_format_name: percent_2
    sql: nullif(${units_returned}/nullif(${units_shipped}, 0),0);;
  }

##################################################
########## End of Sales Order Measures ###########


##################################
  #####   GA Measures          #####
  ##################################{

  measure: number_of_sessions {
    type: sum
    group_label: "GA Metrics"
    label: "# of Sessions"
    description: "Total number of visits on the website"
    sql: ${TABLE}.sessions ;;
  }

  measure: sum_session_duration {
    type: sum
    group_label: "GA Metrics"
    label: "Sessions duration"
    description: "Total Sessions duration"
    sql: ${TABLE}.session_duration ;;
  }

  measure: sum_bounces {
    type: sum
    group_label: "GA Metrics"
    label: "# of Bounces"
    description: "Number of sessions that have left the website after only one page-view"
    sql: ${TABLE}.bounces ;;
  }

  measure: number_of_pageviews {
    type: sum
    group_label: "GA Metrics"
    label: "# of Page views"
    description: "Total number of page views"
    sql: ${TABLE}.pageviews ;;
  }

  measure: sum_unique_pageviews {
    type: sum
    group_label: "GA Metrics"
    label: "# of Unique Page views"
    description: "Number of unique page views"
    sql: ${TABLE}.unique_pageviews ;;
  }

  measure: sum_product_adds_to_cart {
    type: sum
    group_label: "GA Metrics"
    label: "# of Products Added to Cart"
    description: "Number of Products added to Cart"
    sql: ${TABLE}.product_adds_to_cart ;;
  }

  measure: sum_product_checkouts {
    type: sum
    group_label: "GA Metrics"
    label: "# of Product Checkouts"
    description: "Number of Products checked out"
    sql: ${TABLE}.product_checkouts ;;
  }

  measure: sum_users {
    type: sum
    hidden: yes
    group_label: "GA Metrics"
    label: "# of Users"
    description: "Total number of visitors on the website for a given period"
    sql: ${TABLE}.users ;;
  }

  measure: pageviews_per_session {
    type: number
    group_label: "GA Metrics"
    label: "Pageviews per Session"
    description: "Average number of pages viewed per session"
    value_format_name: decimal_2
    sql: ${number_of_pageviews}/NULLIF(${number_of_sessions},0) ;;
  }


  measure: bounce_rate {
    type: number
    group_label: "GA Metrics"
    label: "Bounce Rate %"
    description: "Percentage of sessions that have left the website after only one page-view"
    value_format_name: percent_2
    sql: ${sum_bounces}/NULLIF(${number_of_sessions},0) ;;
  }

  measure: avg_time_on_site {
    type: number
    group_label: "GA Metrics"
    label: "Avg Time On Site"
    description: "Average of time spend on the site in seconds per session"
    value_format_name: decimal_0
    sql: ${sum_session_duration}/NULLIF(${number_of_sessions},0) ;;
  }

  measure: add_to_basket_rate {
    type: number
    group_label: "GA Metrics"
    label: "Add to Basket %"
    description: "Percentage of product detail views that have added the product to their basket over a period of time"
    value_format_name: percent_2
    sql: ${sum_product_adds_to_cart}/NULLIF(${number_of_pageviews},0) ;;
  }

  measure: cart_abandonment_rate {
    type: number
    group_label: "GA Metrics"
    label: "Cart Abandonment %"
    description: "Number of cart created that did not convert into an order"
    value_format_name: percent_2
    sql: (${sum_product_adds_to_cart}-${sum_product_checkouts})/NULLIF(${sum_product_adds_to_cart},0) ;;
  }

  measure: checkout_to_add_rate {
    type: number
    group_label: "GA Metrics"
    label: "Checkout to Add to Basket %"
    description: "Ratio of Product checked out vs Added to Cart"
    value_format_name: percent_2
    sql: ${sum_product_checkouts}/NULLIF(${sum_product_adds_to_cart},0) ;;
  }


  measure: conversion_rate {
    type: number
    group_label: "GA Metrics"
    label: "Conversion Rate (%)"
    description: "Percentage of sessions that make an order"
    value_format_name: percent_2
    sql: ${count_sales_orders}/NULLIF(${number_of_sessions},0) ;;
  }

  ### End of GA Measures
  #}


  #####################################
  #####     Funnel MEASURES       #####
  #####################################{


  measure: total_ad_spend_usd {
    type: sum
    group_label: "Funnel Metrics"
    label: "Total Ad Spend (USD)"
    description: "Total value of marketing ads spent across all different channels"
    value_format: "$#,##0.00"
    sql: ${common_cost_usd} ;;
  }

  measure: roas_gross_rev {
    type: number
    group_label: "Funnel Metrics"
    label: "ROAS (on Gross Rev)"
    value_format: "#,##0.00"
    description: "The Return in Gross Sales coming from your Total Ad Spend (based on Gross Sales)"
    sql: ${gross_rev_usd}/nullif(${total_ad_spend_usd}, 0) ;;
  }

  measure: roi_gross_rev {
    type: number
    group_label: "Funnel Metrics"
    label: "ROI (on Gross Rev)"
    value_format: "#,##0.00"
    description: "The Return on Investment in Total Ad Spend in Gross Sales"
    sql: (${gross_rev_usd}-${total_ad_spend_usd})/nullif(${total_ad_spend_usd}, 0) ;;
  }

  measure: cost_per_order {
    type: number
    group_label: "Funnel Metrics"
    label: "CPO"
    description: "The marketing cost of one order"
    value_format: "$#,##0.00"
    sql: ${total_ad_spend_usd}/nullif(${count_sales_orders}, 0) ;;
  }

  measure: total_clicks {
    type: sum
    group_label: "Funnel Metrics"
    label: "# Clicks"
    description: "Total number of times an ad is clicked by a user"
    sql: ${common_clicks} ;;
  }

  measure: total_impressions {
    type: sum
    group_label: "Funnel Metrics"
    label: "# Impressions"
    description: "Total number of times an ad is seen"
    sql:  ${common_impressions};;
  }

  measure: cost_per_click {
    type: number
    group_label: "Funnel Metrics"
    label: "CPC"
    description: "The marketing cost of one ad click"
    value_format_name: usd
    sql: ${total_ad_spend_usd} / NULLIF(${total_clicks},0) ;;
  }


  measure: click_through_rate {
    type: number
    group_label: "Funnel Metrics"
    label: "Click Through Rate %"
    description: "Percentage ad impressions that have led to a click"
    value_format_name: percent_2
    sql: NULLIF(${total_clicks},0) / NULLIF(${total_impressions},0);;
  }


  measure: cost_per_mill {
    type: number
    group_label: "Funnel Metrics"
    label: "CPM"
    description: "Cost per Thousand Impressions"
    value_format_name: usd
    sql: NULLIF(NULLIF(${total_ad_spend_usd},0) / NULLIF(${total_impressions}/1000,0),0) ;;
  }

  # measure: cost_of_acquisition{
  #   type: number
  #   value_format_name: decimal_2
  #   group_label: "Funnel Metrics"
  #   description: "The marketing cost of one acquired new customer"
  #   label: "Cost of Acquisition"
  #   sql: NULLIF(${total_ad_spend_usd} / NULLIF(${ecomm_customer_type_pop.new_cust_count_fixed},0),0) ;;
  # }

  ### End of Funnel Measures
  #}

####################################
#########      ALERTS      #########
####################################


  # dimension_group: record {
  #   type: time
  #   timeframes: [
  #     raw,
  #     second,
  #     minute,
  #     hour,
  #     date,
  #     week,
  #     month,
  #     quarter,
  #     year,
  #     week_of_year,
  #     day_of_week
  #   ]
  #   convert_tz: no
  #   datatype: datetime
  #   label: "Record"
  #   sql: ${TABLE}.record_datetime ;;
  # }


  measure: minutes_since_last_update {
    type: min
    group_label: "Alerts"
    sql: DATETIME_DIFF(current_datetime(),cast(${record_datetime_raw} as datetime),minute)+240 ;;
  }


##############################################
############## Hidden dimensions #############
##############################################

  dimension: amount_refunded {
    hidden: yes
    type: number
    sql: ${TABLE}.amount_refunded ;;
  }

  dimension: bounces {
    hidden: yes
    type: number
    sql: ${TABLE}.bounces ;;
  }

  dimension: source {
    type: string
    hidden: yes
    sql: ${TABLE}.source ;;
  }

  dimension: canceled_amount_local {
    type: number
    hidden: yes
    sql: ${TABLE}.canceled_amount_local ;;
  }

  dimension: canceled_amount_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.canceled_amount_usd ;;
  }

  dimension: common_clicks {
    type: number
    hidden: yes
    sql: ${TABLE}.common_clicks ;;
  }

  dimension: common_cost_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.common_cost_usd ;;
  }

  dimension: common_impressions {
    type: number
    hidden: yes
    sql: ${TABLE}.common_impressions ;;
  }

  dimension: discount_amount {
    type: number
    hidden: yes
    sql: ${TABLE}.discount_amount ;;
  }

  dimension: discount_refunded {
    type: number
    hidden: yes
    sql: ${TABLE}.discount_refunded ;;
  }

  dimension: gross_local {
    type: number
    hidden: yes
    sql: ${TABLE}.gross_local ;;
  }

  dimension: gross_minus_can_local {
    type: number
    hidden: yes
    sql: ${TABLE}.gross_minus_can_local ;;
  }

  dimension: gross_minus_can_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.gross_minus_can_usd ;;
  }

  dimension: gross_net_sales {
    type: number
    hidden: yes
    sql: ${TABLE}.gross_net_sales ;;
  }

  dimension: gross_net_sales_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.gross_net_sales_usd ;;
  }

  dimension: gross_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.gross_usd ;;
  }

  dimension: pageviews {
    type: number
    hidden: yes
    sql: ${TABLE}.pageviews ;;
  }

  dimension: price {
    type: number
    hidden: yes
    sql: ${TABLE}.price ;;
  }

  dimension: primary_key {
    type: string
    primary_key: yes
    hidden: yes
    sql: ${TABLE}.primary_key ;;
  }

  dimension: product_adds_to_cart {
    type: number
    hidden: yes
    sql: ${TABLE}.product_adds_to_cart ;;
  }

  dimension: product_checkouts {
    type: number
    hidden: yes
    sql: ${TABLE}.product_checkouts ;;
  }

  dimension: qty_canceled {
    type: number
    hidden: yes
    sql: ${TABLE}.qty_canceled ;;
  }

  dimension: qty_ordered {
    type: number
    hidden: yes
    sql: ${TABLE}.qty_ordered ;;
  }

  dimension: qty_refunded {
    type: number
    hidden: yes
    sql: ${TABLE}.qty_refunded ;;
  }

  dimension: qty_shipped {
    type: number
    hidden: yes
    sql: ${TABLE}.qty_shipped ;;
  }

  dimension: refund_amount_local {
    type: number
    hidden: yes
    sql: ${TABLE}.refund_amount_local ;;
  }

  dimension: refund_amount_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.refund_amount_usd ;;
  }

  dimension: session_duration {
    type: number
    hidden: yes
    sql: ${TABLE}.session_duration ;;
  }

  dimension: sessions {
    type: number
    hidden: yes
    sql: ${TABLE}.sessions ;;
  }

  dimension: shipment_net_sales {
    type: number
    hidden: yes
    sql: ${TABLE}.shipment_net_sales ;;
  }

  dimension: shipment_net_sales_incl_tax {
    type: number
    hidden: yes
    sql: ${TABLE}.shipment_net_sales_incl_tax ;;
  }

  dimension: shipment_net_sales_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.shipment_net_sales_usd ;;
  }

  dimension: shipment_net_sales_usd_incl_tax {
    type: number
    hidden: yes
    sql: ${TABLE}.shipment_net_sales_usd_incl_tax ;;
  }

  dimension: shipped_local {
    type: number
    hidden: yes
    sql: ${TABLE}.shipped_local ;;
  }

  dimension: shipped_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.shipped_usd ;;
  }

  dimension: unique_pageviews {
    type: number
    hidden: yes
    sql: ${TABLE}.unique_pageviews ;;
  }

  dimension: vertical {
    type: string
    hidden: yes
    sql: ${TABLE}.vertical ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
