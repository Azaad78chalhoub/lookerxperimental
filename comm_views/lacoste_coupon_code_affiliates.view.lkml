view: lacoste_coupon_code_affiliates {
  sql_table_name: `chb-prod-data-comm.prod_commercial.lacoste_coupon_code_affiliates`
    ;;

  dimension: pk {
    primary_key: yes
    hidden: yes
    sql: concat(${coupon_code}, ${coupon_id}, ${country}, ${type}, ${commission_percentage}, ${vendor}) ;;
  }

  dimension: commission_percentage {
    type: number
    value_format_name: percent_0
    sql: ${TABLE}.commission_percentage/100 ;;
  }

  dimension: country {
    type: string
    map_layer_name: countries
    sql: ${TABLE}.country ;;
  }

  dimension: coupon_code {
    type: string
    sql: ${TABLE}.coupon_code ;;
  }

  dimension: coupon_id {
    type: string
    sql: ${TABLE}.coupon_id ;;
  }

  dimension: type {
    type: string
    sql: ${TABLE}.type ;;
  }

  dimension: vendor {
    type: string
    sql: ${TABLE}.vendor ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
