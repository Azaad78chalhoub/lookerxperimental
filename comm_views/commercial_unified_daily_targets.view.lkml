view: commercial_unified_daily_targets {
  sql_table_name: `chb-prod-data-comm.prod_commercial.commercial_unified_daily_targets`
    ;;

  dimension: primary_key {
    primary_key: yes
    hidden:  yes
    type: number
    sql: CONCAT(${brand},${date_date}, ${country_code}, ${sessions})  ;;
  }

  dimension: aov_usd {
    hidden: yes
    type: number
    sql: ${TABLE}.aov_usd ;;
  }

  dimension: brand {
    hidden: yes
    type: string
    sql: ${TABLE}.brand ;;
  }

  dimension: cancellation_rate {
    hidden: yes
    type: number
    sql: ${TABLE}.cancellation_rate ;;
  }

  dimension: conversion_rate {
    hidden: yes
    type: number
    sql: ${TABLE}.conversion_rate ;;
  }

  dimension: country_code {
    hidden: yes
    type: string
    sql: CASE WHEN ${TABLE}.country_code ="UAE" THEN "United Arab Emirates"
        WHEN ${TABLE}.country_code ="KSA" THEN "Saudi Arabia"
        WHEN ${TABLE}.country_code ="EGY" THEN "Egypt"
        WHEN ${TABLE}.country_code ="QAT" THEN "Qatar"
        WHEN ${TABLE}.country_code ="KUW" THEN "Kuwait"
        WHEN ${TABLE}.country_code ="BAH" THEN "Bahrain"
        WHEN ${TABLE}.country_code ="OM" THEN "Oman"
        ELSE "Other" END;;
  }

  dimension_group: date {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.date ;;
  }

  dimension: day {
    type: string
    sql: ${TABLE}.day ;;
  }

  dimension: gross_revenue_usd {
    hidden: yes
    type: number
    sql: ${TABLE}.gross_revenue_usd ;;
  }

  dimension: net_sales_usd {
    hidden: yes
    type: number
    sql: ${TABLE}.net_sales_usd ;;
  }

  dimension: orders_gross {
    hidden: yes
    type: number
    sql: ${TABLE}.orders_gross ;;
  }

  dimension: return_rate {
    hidden: yes
    type: number
    sql: ${TABLE}.return_rate ;;
  }

  dimension: sales_target {
    hidden: yes
    type: number
    sql: ${TABLE}.sales_target ;;
  }

  dimension: sessions {
    hidden: yes
    type: number
    sql: ${TABLE}.sessions ;;
  }

  dimension: store_code {
    type: string
    sql: ${TABLE}.store_code ;;
  }

  dimension: units {
    hidden: yes
    type: number
    sql: ${TABLE}.units ;;
  }

  dimension: upt {
    hidden: yes
    type: number
    sql: ${TABLE}.upt ;;
  }

  dimension: usp {
    hidden: yes
    type: number
    sql: ${TABLE}.usp ;;
  }

  measure: aov_usd_measure {
    label: "Average Order Value (USD)"
    value_format_name: usd
    type: average
    sql: ${aov_usd} ;;
  }

  measure: cancellation_rate_measure {
    label: "Cancellation Rate (%)"
    value_format_name: percent_2
    type: average
    sql: ${cancellation_rate} ;;
  }

  measure: conversion_rate_measure {
    label: "Conversion Rate (%)"
    value_format_name: percent_2
    type: average
    sql: ${conversion_rate} ;;
  }

  measure: gross_revenue_usd_measure {
    label: "Gross Revenue (USD)"
    value_format_name: usd
    type: sum
    sql: ${gross_revenue_usd} ;;
  }

  measure: net_sales_usd_measure {
    label: "Net Sales (USD)"
    value_format_name: usd
    type: sum
    sql: ${net_sales_usd} ;;
  }

  measure: orders_gross_measure {
    label: "# of Orders"
    value_format_name: decimal_2
    type: sum
    sql: ${orders_gross} ;;
  }

  measure: return_rate_measure {
    label: "Returned Rate (%)"
    value_format_name: percent_2
    type: average
    sql: ${return_rate} ;;
  }

  measure: sales_target_measure {
    label: "Sales Target (USD)"
    value_format_name: usd
    type: sum
    sql: ${sales_target} ;;
  }

  measure: sessions_measure {
    label: "# of Sessions"
    value_format_name: decimal_2
    type: sum
    sql: ${sessions} ;;
  }

  measure: units_measure {
    label: "# of Units"
    value_format_name: decimal_2
    type: sum
    sql: ${units} ;;
  }

  measure: upt_measure {
    label: "UPT"
    value_format_name: decimal_2
    type: average
    sql: ${upt} ;;
  }

  measure: usp_measure {
    label: "USP (USD)"
    value_format_name: usd
    type: average
    sql: ${usp} ;;
  }

}
