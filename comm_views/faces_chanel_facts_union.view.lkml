include: "../_common/period_over_period.view"

view: faces_chanel_facts_union {

    derived_table: {
      sql:
        SELECT *
        FROM
          `chb-prod-data-comm.prod_commercial.faces_facts_union` A,
          `chb-prod-supplychain-data.prod_shared_dimensions.dim_retail_product` B
        WHERE
          A.sku = B.item
          and B.brand = 'CHANEL';;
    }

    dimension: brand_security{
      type: string
      hidden: yes
      sql: "FACES" ;;
    }

    dimension: vertical_security{
      type: string
      hidden: yes
      sql: "MANAGED COMPANIES" ;;
    }

    extends: [period_over_period]

    dimension_group: pop_no_tz {
      sql: ${common_raw} ;;
    }

    dimension: primary_key {
      type: number
      primary_key: yes
      hidden: yes
      sql: concat(${order_id},${record_type},${sku}) ;;
    }

    dimension_group: common {
      type: time
      timeframes: [
        raw,
        date,
        week,
        month,
        quarter,
        year
      ]
      convert_tz: no
      datatype: date
      label: "Date"
      sql: ${TABLE}.common_date ;;
    }

    dimension: country {
      type: string
      map_layer_name: countries
      description: "Country"
      sql: ${TABLE}.country ;;
    }

    dimension: source {
      type: string
      description: "Source can one of the following - Sales order, Google Analytics (GA) and Funnel"
      sql: ${TABLE}.source ;;
    }

#######################################
### Acquisition/Marketing dimensions ###
#######################################{

  dimension: acq_campaign {
    type: string
    group_label: "Acquisition / Marketing"
    label: "Campaign"
    description: "Acquisition campaign"
    sql: ${TABLE}.acq_campaign ;;
  }

  dimension: acq_device {
    type: string
    group_label: "Acquisition / Marketing"
    label: "Device"
    description: "Acquisition device"
    sql: ${TABLE}.acq_device ;;
  }

  dimension: acq_keyword {
    type: string
    group_label: "Acquisition / Marketing"
    label: "Keyword"
    description: "Acquisition keyword"
    sql: ${TABLE}.acq_keyword ;;
  }

  dimension: acq_medium {
    type: string
    group_label: "Acquisition / Marketing"
    label: "Medium"
    description: "Acquisition medium"
    hidden: yes
    sql: ${TABLE}.acq_medium ;;
  }

  dimension: acq_source {
    type: string
    group_label: "Acquisition / Marketing"
    label: "Source"
    description: "Acquistition source"
    hidden: yes
    sql: ${TABLE}.acq_source ;;
  }

  dimension: acq_source_medium {
    type: string
    group_label: "Acquisition / Marketing"
    label: "Source Medium"
    description: "Acquisition source / medium"
    sql: concat(${acq_source},"/",${acq_medium}) ;;
  }

##############################################
### End of Acquisition/Marketing dimensions ###}


###################################
### Sales Order - Dimensions ###
###################################{

    dimension: order_id {
      type: string
      label: "Sales Order ID"
      group_label: "Sales Order"
      description: "Sales Order ID"
      sql: ${TABLE}.order_id ;;
    }

    dimension: status {
      type: string
      label: "Order Status"
      group_label: "Sales Order"
      description: "Sales Order Status"
      sql: ${TABLE}.order_status ;;
    }

    dimension: brand {
      type: string
      hidden: yes
      sql: ${TABLE}.brand ;;
    }

    dimension: record_type {
      type: string
      hidden: yes
      description: "Record type such as SALES, SHIPMENT, etc"
      sql: ${TABLE}.record_type ;;
    }

    dimension: sku {
      type: string
      label: "SKU"
      group_label: "Sales Order"
      description: "SKU of the Item"
      sql: ${TABLE}.sku ;;
    }

    dimension: payment_method {
      type: string
      label: "Payment Method"
      group_label: "Sales Order"
      description: "Payment method"
      sql: ${TABLE}.payment_method ;;
    }


    #####################################
    ##### Sales Orders
    #####################################{


    measure: all_orders {
      type: count_distinct
      group_label: "Sales Order"
      label: "# Orders"
      description: "Total number of orders placed on the website"
      sql: ${order_id} ;;
      filters: [record_type: "SALE"]
    }

    measure: units_ordered {
      type: sum
      group_label: "Sales Order"
      label: "Units Ordered"
      description: "Sum of Qty Ordered"
      sql: ${TABLE}.qty_ordered;;
      filters: [record_type: "SALE"]
    }

    measure: gross_revenue_local {
      type: sum_distinct
      group_label: "Sales Order"
      label: "Gross Revenue Local"
      description: "Total value of incoming orders on the website - Local Currency"
      value_format: "#,##0.00"
      sql: ${TABLE}.gross_revenue_local;;
      filters: [record_type: "SALE"]
    }

    measure: gross_revenue_usd {
      type: sum_distinct
      group_label: "Sales Order"
      label: "Gross Revenue USD"
      description: "Total value of incoming orders on the website - USD"
      value_format: "$#,##0.00"
      sql: ${TABLE}.gross_revenue_usd;;
      filters: [record_type: "SALE"]
    }

    measure: gross_revenue_percent {
      type: percent_of_total
      group_label: "Sales Order"
      label: "% Gross Revenue"
      description: "% of Gross Revenue in selected dimension"
      value_format_name: decimal_2
      sql: ${gross_revenue_usd} ;;
    }

    measure: upt {
      type: number
      group_label: "Sales Order"
      label: "UPT"
      description: "Units per transaction = Average number of units within each order"
      value_format_name: decimal_2
      sql:${units_ordered}/nullif(${all_orders}, 0);;
    }

    measure: aov_local {
      type: number
      group_label: "Sales Order"
      label: "Average Order Value Local"
      description: "Average Value of each order - Local currency"
      value_format: "#,##0.00"
      sql: ${gross_revenue_local}/${all_orders};;
    }

    measure: aov_usd {
      type: number
      group_label: "Sales Order"
      label: "Average Order Value USD"
      description: "Average Value of each order - USD"
      value_format: "#,##0.00"
      sql: ${gross_revenue_usd}/nullif(${all_orders}, 0);;
    }

    measure: unit_selling_price {
      type:  number
      label: "USP"
      group_label: "Sales Order"
      description: "Unit Selling Price = Average price of each item ordered"
      value_format: "#,##0.00"
      sql: ${gross_revenue_usd} / NULLIF(${units_ordered},0) ;;
    }


#####################################
##### Cancelled Orders
####################################{

    measure: count_cancelled_orders {
      type: count_distinct
      group_label: "Cancelled Order"
      label: "# Cancelled Orders"
      description: "Count of Cancelled Orders"
      sql: ${order_id};;
      filters: [record_type: "CANCELLED"]
    }

    measure: unit_canceled {
      type: sum
      group_label: "Cancelled Order"
      label: "Units Canceled"
      description: "Sum of Qty Ordered"
      sql: ${TABLE}.qty_canceled;;
    }

    measure: cancelled_order_ratio {
      type: number
      group_label: "Cancelled Order"
      label: "Cancelled Order %"
      description: "Percentage of incoming orders (based on numbers) that were cancelled"
      value_format_name: percent_2
      sql: ${count_cancelled_orders}/NULLIF(${all_orders},0) ;;
    }

    measure: canceled_amount_local {
      type: sum_distinct
      group_label: "Cancelled Order"
      label: "Cancelled amount Local"
      description: "Cancelled amount Local"
      value_format: "#,##0.00"
      sql: ${TABLE}.canceled_amount_local;;
      filters: [record_type: "CANCELLED"]
    }

    measure: canceled_amount_usd {
      type: sum_distinct
      group_label: "Cancelled Order"
      label: "Cancelled amount USD"
      description: "Cancelled amount USD"
      value_format: "$#,##0.00"
      sql: ${TABLE}.canceled_amount_usd;;
      filters: [record_type: "CANCELLED"]
    }


    measure: gross_revenue_minus_cancelled_local {
      type: number
      group_label: "Cancelled Order"
      label: "Gross Rev Minus Cancelled Local"
      description: "Gross Revenue Minus Cancelled Local"
      value_format: "#,##0.00"
      sql: ${gross_revenue_local} - ${canceled_amount_local};;
    }

    measure: gross_revenue_minus_cancelled_usd {
      type: number
      group_label: "Cancelled Order"
      label: "Gross Rev Minus Cancelled USD"
      description: "Gross Revenue Minus Cancelled USD"
      value_format: "$#,##0.00"
      sql: ${gross_revenue_usd} - ${canceled_amount_usd};;
    }

    measure: cancelled_item_ratio {
      type: number
      group_label: "Cancelled Order"
      label: "Cancelled Item %"
      description: "Cancelled Item %"
      value_format_name: percent_2
      sql: ${unit_canceled}/NULLIF(${units_ordered},0) ;;
    }

    measure: cancelled_rate {
      type: number
      group_label: "Cancelled Order"
      label: "Cancelled Rate (%)"
      description: "Cancelled Rate (%)"
      value_format_name: percent_2
      sql: ${canceled_amount_usd}/nullif(${gross_revenue_usd}, 0);;
    }

### End of Cancelled orders measures
#}

#####################################
##### Shipped Orders
####################################{

    measure: shipped_orders {
      type: count_distinct
      group_label: "Shipped Order"
      label: "# Shipped Orders"
      description: "Count of Distinct Order IDs that were shipped"
      sql: ${order_id} ;;
      filters: [record_type: "SHIPMENT"]
    }

    measure: units_shipped {
      type: sum
      group_label: "Shipped Order"
      label: "Units Shipped"
      description: "Sum of Qty Shipped"
      sql: ${TABLE}.qty_shipped;;
      filters: [record_type: "SHIPMENT"]
    }

    measure: shipment_revenue_local {
      type: sum_distinct
      group_label: "Shipped Order"
      label: "Shipped Revenue Local"
      description: "Total value of shipped orders (excluding failed credit card attempts, VAT and cancellations) - Local currency"
      value_format: "#,##0.00"
      sql: ${TABLE}.shipment_revenue_local;;
      filters: [record_type: "SHIPMENT"]
    }

    measure: shipment_revenue_usd {
      type: sum_distinct
      group_label: "Shipped Order"
      label: "Shipped Revenue USD"
      description: "Total value of shipped orders (excluding failed credit card attempts, VAT and cancellations) - USD"
      value_format: "$#,##0.00"
      sql: ${TABLE}.shipment_revenue_usd;;
      filters: [record_type: "SHIPMENT"]
    }

    measure: shipment_revenue_minus_cancelled_local {
      type: number
      group_label: "Cancelled Order"
      label: "Shipped Rev Minus Cancelled Local"
      description: "Shipped Revenue Minus Cancelled Local"
      value_format: "#,##0.00"
      sql: ${shipment_revenue_local} - ${canceled_amount_local};;
    }

    measure: shipment_revenue_minus_cancelled_usd {
      type: number
      group_label: "Cancelled Order"
      label: "Shipped Rev Minus Cancelled USD"
      description: "Shipped Revenue Minus Cancelled USD"
      value_format: "$#,##0.00"
      sql: ${shipment_revenue_usd} - ${canceled_amount_usd};;
    }

### End of Shipped orders measures
#}


#####################################
##### REFUND - MEASURES ########
#####################################{

    measure: units_refunded {
      type: sum
      group_label: "Returned Order"
      label: "Units Refunded"
      description: "Sum of Qty Refunded"
      sql: ${TABLE}.qty_refunded;;
      filters: [record_type: "REFUND"]
    }

    measure: refund_amount_local {
      type: sum_distinct
      group_label: "Returned Order"
      label: "Refund Amount Local"
      description: "Refund Amount Local"
      value_format: "#,##0.00"
      sql: ${TABLE}.refund_amount_local;;
      filters: [record_type: "REFUND"]
    }

    measure: refund_amount_usd {
      type: sum_distinct
      group_label: "Returned Order"
      label: "Refund Amount USD"
      description: "Refund Amount USD"
      value_format: "$#,##0.00"
      sql: ${TABLE}.refund_amount_usd;;
      filters: [record_type: "REFUND"]
    }


    measure: gross_revenue_minus_cancelled_minus_returns_local {
      type: number
      group_label: "Returned Order"
      label: "Gross Rev Minus Cancelled Minus Returns Local"
      description: "Total value of incoming orders on the website excluding cancellations and returns. Based on order date - Local currency"
      value_format: "#,##0.00"
      sql: ${gross_revenue_local} - ${canceled_amount_local} - ${refund_amount_local};;
    }

    measure: gross_revenue_minus_cancelled_minus_returns_usd {
      type: number
      group_label: "Returned Order"
      label: "Gross Rev Minus Cancelled Minus Returns USD"
      description: "Total value of incoming orders on the website excluding cancellations and returns. Based on order date - USD"
      value_format: "$#,##0.00"
      sql: ${gross_revenue_usd} - ${canceled_amount_usd} - ${refund_amount_usd};;
    }

    measure: shipment_revenue_minus_cancelled_minus_returns_local {
      type: number
      group_label: "Returned Order"
      label: "Shipped Rev Minus Cancelled Minus Returns Local"
      description: "Total value of incoming orders on the website excluding cancellations and returns. Based on shipping date - Local currency"
      value_format: "#,##0.00"
      sql: ${shipment_revenue_local} - ${canceled_amount_local} - ${refund_amount_local};;
    }

    measure: shipment_revenue_minus_cancelled_minus_returns_usd {
      type: number
      group_label: "Returned Order"
      label: "Shipped Rev Minus Cancelled Minus Returns USD"
      description: "Total value of incoming orders on the website excluding cancellations and returns. Based on shipping date - USD"
      value_format: "$#,##0.00"
      sql: ${shipment_revenue_usd} - ${canceled_amount_usd} - ${refund_amount_usd};;
    }

    measure: return_rate {
      type: number
      group_label: "Returned Order"
      label: "Return Rate (%)"
      description: "Return Rate (%) = Refund Amount in USD / Shipment Revenue in USD [Value of items returned (based on return date in warehouse) divided by total value of items shipped (based on shipping date)]"
      value_format_name: percent_2
      sql: ${refund_amount_usd}/nullif(${gross_revenue_usd}, 0);;
    }


##################################
#####   GA Measures          #####
##################################{

    measure: number_of_sessions {
      type: sum
      group_label: "GA Metrics"
      label: "# of Sessions"
      description: "Total number of sessions on the website"
      sql: ${TABLE}.sessions ;;
      filters: [record_type: "SALE", source: "GA"]
    }

    measure: percent_of_sessions {
      type: percent_of_total
      group_label: "GA Metrics"
      label: "% of Total Sessions"
      description: "Percentage of sessions from a particular dimension"
      value_format_name: decimal_2
      sql: ${number_of_sessions} ;;
    }

    measure: sum_session_duration {
      type: sum
      group_label: "GA Metrics"
      label: "Sessions duration"
      description: "Sum of session duration"
      sql: ${TABLE}.session_duration ;;
      filters: [record_type: "SALE", source: "GA"]
    }

    measure: sum_bounces {
      type: sum
      group_label: "GA Metrics"
      label: "# of Bounces"
      description: "Total number of bounces"
      sql: ${TABLE}.bounces ;;
      filters: [record_type: "SALE", source: "GA"]
    }

    measure: number_of_pageviews {
      type: sum
      group_label: "GA Metrics"
      label: "# of Page views"
      description: "Total number of page views"
      sql: ${TABLE}.pageviews ;;
      filters: [record_type: "SALE", source: "GA"]
    }

    measure: sum_unique_pageviews {
      type: sum
      group_label: "GA Metrics"
      label: "# of Unique Page views"
      description: "Total number of unique page views"
      sql: ${TABLE}.unique_pageviews ;;
    }

    measure: sum_product_adds_to_cart {
      type: sum
      group_label: "GA Metrics"
      label: "# of Products Added to Cart"
      description: "Total number of products added to cart"
      sql: ${TABLE}.product_adds_to_cart ;;
      filters: [record_type: "SALE", source: "GA"]
    }

    measure: sum_product_checkouts {
      type: sum
      group_label: "GA Metrics"
      label: "# of Product Checkouts"
      description: "Total number of product checkouts"
      sql: ${TABLE}.product_checkouts ;;
      filters: [record_type: "SALE", source: "GA"]
    }

    measure: sum_users {
      type: sum
      group_label: "GA Metrics"
      label: "# of Users"
      description: "Total number of users"
      sql: ${TABLE}.users ;;
      filters: [record_type: "SALE", source: "GA"]
    }

    measure: pageviews_per_session {
      type: number
      group_label: "GA Metrics"
      label: "Pageviews per Session"
      description: "Page views per session"
      value_format_name: decimal_2
      sql: ${number_of_pageviews}/NULLIF(${number_of_sessions},0) ;;
    }


    measure: bounce_rate {
      type: number
      group_label: "GA Metrics"
      label: "Bounce Rate %"
      description: "Percentage of visitors who leave the website after only one page view"
      value_format_name: percent_2
      sql: ${sum_bounces}/NULLIF(${number_of_sessions},0) ;;
    }

    measure: avg_time_on_site {
      type: number
      group_label: "GA Metrics"
      label: "Avg Time On Site"
      description: "Average of time spend on the site in seconds per session"
      value_format: "h:mm:ss"
      sql: ${sum_session_duration}/NULLIF(${number_of_sessions},0)/86400.0 ;;
    }

    measure: add_to_basket_rate {
      type: number
      group_label: "GA Metrics"
      label: "Add to Basket %"
      description: "Percentage of product detail views that have added the product to their basket over a period of time"
      value_format_name: percent_2
      sql: ${sum_product_adds_to_cart}/NULLIF(${number_of_pageviews},0) ;;
    }

    measure: cart_abandonment_rate {
      type: number
      group_label: "GA Metrics"
      label: "Cart Abandonment %"
      description: "Number of cart created that did not convert into an order (1-% cart abandonment = cart completion rate)"
      value_format_name: percent_2
      sql: (${sum_product_adds_to_cart}-${sum_product_checkouts})/NULLIF(${sum_product_adds_to_cart},0) ;;
    }

    measure: checkout_to_add_rate {
      type: number
      group_label: "GA Metrics"
      label: "Checkout to Add to Basket %"
      description: "Sum of Products checked out / Sum of products added to cart"
      value_format_name: percent_2
      sql: ${sum_product_checkouts}/NULLIF(${sum_product_adds_to_cart},0) ;;
    }


    measure: conversion_rate {
      type: number
      group_label: "GA Metrics"
      label: "Conversion Rate (%)"
      description: "Percentage of sessions that make an order"
      value_format_name: percent_2
      sql: ${all_orders}/NULLIF(${number_of_sessions},0) ;;
    }

    ### End of GA Measures
    #}


  }
