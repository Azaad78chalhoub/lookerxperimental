view: affinity_total_orders {
  sql_table_name: `chb-prod-data-comm.prod_commercial.affinity_total_orders`  ;;

  dimension: common_date {
    sql: ${TABLE}.common_date ;;
  }

  dimension: brand {
    sql: ${TABLE}.brand ;;
  }

  dimension: count {
    type: number
    sql: ${TABLE}.count ;;
    label: "Total Order Count"
  }

  dimension: primary_key {
    type: number
    primary_key: yes
    hidden: yes
    sql: concat(${TABLE}.common_date,${TABLE}.brand) ;;
  }

}
