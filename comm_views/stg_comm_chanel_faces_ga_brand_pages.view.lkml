view: stg_comm_chanel_faces_ga_brand_pages {

  derived_table: {
    sql:
        SELECT *
        FROM
          `chb-prod-stage-comm.prod_comm.stg_comm_faces_ga_brand_pages`
        WHERE
          page_path like "%chanel%";;
  }

  dimension: pk {
    type:  string
    primary_key:  yes
    sql:  concat(${ecom_site}, cast(${date_date} as string), ${page_path},
                  ${landing_page_path}, ${device_category}, ${campaign}, ${source_medium}) ;;
  }

  dimension: ecom_site {
    type: string
    label: "Store Country"
    sql: ${TABLE}.ecom_site ;;
  }

  dimension_group: date {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.date ;;
  }

  dimension: magento_orders {
    type: number
    sql: ${TABLE}.magento_orders ;;
  }

  dimension: sessions {
    type: number
    description: "Number of sessions"
    sql: ${TABLE}.sessions ;;
  }

  dimension: campaign {
    type: string
    description: "Campaign"
    sql: ${TABLE}.campaign ;;
  }

  dimension: page_path {
    type: string
    description: "Page path"
    sql: ${TABLE}.page_path ;;
  }

  dimension: landing_page_path {
    type: string
    description: "Landing page path"
    sql: ${TABLE}.landing_page_path ;;
  }

  dimension: device_category {
    type: string
    description: "Device category"
    sql: ${TABLE}.device_category ;;
  }

  dimension: source_medium {
    type: string
    description: "Source / medium"
    sql: ${TABLE}.source_medium ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }


  measure: number_of_sessions {
    type: sum
    label: "# of Sessions"
    description: "Total number of sessions"
    sql: ${TABLE}.sessions ;;
  }

  measure: number_of_pageviews {
    type: sum
    label: "# of Pageviews"
    description: "Total number of page views"
    sql: ${TABLE}.pageviews ;;
  }

  measure: number_of_unique_pageviews {
    type: sum
    label: "# of Unique Pageviews"
    description: "Total number of unique page views"
    sql: ${TABLE}.unique_pageviews ;;
  }

  measure: number_of_bounces {
    type: sum
    label: "# of Bounces"
    description: "Total number of bounces"
    sql: ${TABLE}.bounces ;;
  }

  measure: number_of_exits {
    type: sum
    label: "# of Exits"
    description: "Total number of exits"
    sql: ${TABLE}.exits ;;
  }

  measure: number_of_entrances {
    type: sum
    label: "# of Entrances"
    description: "Total number of entrances"
    sql: ${TABLE}.entrances ;;
  }

  measure: time_on_page {
    type: sum
    label: "Time on page"
    description: "Total Time spent on apges"
    sql: ${TABLE}.time_on_page ;;
  }

  measure: number_of_new_users {
    type: sum
    label: "# of New Users"
    description: "Total number of new users"
    sql: ${TABLE}.new_users ;;
  }

  measure: session_duraton {
    type: sum
    label: "Session duration"
    description: "Sum of session duration"
    sql: ${TABLE}.session_duration ;;
  }

  measure: number_of_new_sessions {
    type: sum
    label: "# of New Sessions"
    description: "Sum of new sessions"
    sql: ${TABLE}.new_sessions ;;
  }


  measure: pageviews_per_session {
    type: number
    label: "Pageviews per Session"
    description: "Page views per session"
    value_format_name: decimal_4
    sql: ${number_of_pageviews}/NULLIF(${number_of_sessions},0) ;;
  }

  measure: average_session_duration {
    type: number
    label: "Average Session Duration"
    description: "Average session duration"
    value_format: "h:mm:ss"
    sql: ${session_duraton}/NULLIF(${number_of_sessions},0)/86400.0 ;;
  }

  measure: number_of_orders {
    type: sum
    label: "# of Orders"
    description: "Number of orders / transactions"
    sql: ${TABLE}.magento_orders ;;
  }

  measure: conversion_rate {
    type: number
    label: "Conversion Rate (%)"
    description: "Number of orders / Number of sessions"
    value_format_name: percent_2
    sql: ${number_of_orders}/NULLIF(${number_of_sessions},0) ;;
  }

  measure: bounce_rate {
    type: number
    label: "Bouce Rate (%)"
    description: "Bounce rate in percentage"
    value_format_name: percent_2
    sql: ${number_of_bounces}/NULLIF(${number_of_sessions},0) ;;
  }

  measure: percent_new_sessions {
    type: number
    label: "New Sessions (%)"
    description: "Percentage of new sessions"
    value_format_name: percent_2
    sql: ${number_of_new_sessions}/NULLIF(${number_of_sessions},0) ;;
  }

  measure: exit_rate {
    type: number
    label: "Exit Rate (%)"
    description: "Exit rate in percentage"
    value_format_name: percent_2
    sql: ${number_of_exits}/NULLIF(${number_of_pageviews},0) ;;
  }

  measure: total_time_spent {
    type: sum
    label: "Total time spent"
    description: "total time spent on pages"
    value_format_name: decimal_2
    sql: ${TABLE}.time_on_page ;;
  }

  measure: average_time_spent {
    type: number
    label: "Average time spent"
    description: "Average time spent on pages"
    value_format: "h:mm:ss"
    sql: ${total_time_spent}/NULLIF(${number_of_unique_pageviews},0)/86400.0 ;;
  }

}
