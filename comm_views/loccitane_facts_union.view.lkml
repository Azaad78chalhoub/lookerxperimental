include: "../_common/period_over_period.view"
include: "../customer_views/ecomm_customer_new_returning.view"

view: loccitane_facts_union {
  sql_table_name: `chb-prod-data-comm.prod_commercial.loccitane_facts_union`
    ;;


  dimension: brand_security{
    type: string
    hidden: yes
    sql: "LOCCITANE" ;;
  }

  dimension: vertical_security{
    type: string
    hidden: yes
    sql: "JOINT VENTURES" ;;
  }

  dimension: origin {
    hidden: yes
    type: string
    sql: ${TABLE}.origin ;;
  }

  dimension_group: common_date {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.common_date ;;
  }

  dimension: date_group {
    label: "Date Group"
    hidden: yes
    sql: {% if loccitane_facts_union.common_date_date._in_query %} CAST(${TABLE}.common_date  AS DATE)
              {% elsif loccitane_facts_union.common_date_month._in_query %} FORMAT_DATE('%Y-%m', ${TABLE}.common_date )
              {% elsif loccitane_facts_union.common_date_quarter._in_query %} (FORMAT_TIMESTAMP('%Y-%m', TIMESTAMP_TRUNC(CAST(${TABLE}.common_date AS TIMESTAMP), QUARTER)))
              {% elsif loccitane_facts_union.common_date_week._in_query %} CONCAT(CAST(EXTRACT(YEAR FROM ${TABLE}.common_date ) AS STRING),'-', CAST(EXTRACT(WEEK(SUNDAY) FROM ${TABLE}.common_date) AS STRING))
              {% elsif loccitane_facts_union.common_date_year._in_query %} EXTRACT(YEAR FROM ${TABLE}.common_date )
              {% else %} CAST(${TABLE}.common_date  AS DATE)
              {% endif %} ;;
  }

  extends: [period_over_period]
  dimension_group: pop_no_tz {
    sql: ${common_date_raw} ;;
  }

  dimension: pk {
    hidden: yes
    primary_key: yes
    sql: concat(${source}, ${store_country},${entity_id}, ${record_type}, ${order_id}, ${sku}, cast(${common_date_raw} as string), ${origin}, ${campaign}, ${device_category}, ${ga_source})  ;;
  }

  dimension: source_customer {
    type: string
    hidden: yes
    sql: concat("LOCCITANE","_",${TABLE}.order_id) ;;
  }


  ######################################
  #####   sales order dimensions   #####
  ######################################

  dimension: canceled_amount_local {
    group_label: "Sales order"
    type: number
    hidden: yes
    sql: ${TABLE}.canceled_amount_local ;;
  }

  dimension: canceled_amount_usd {
    group_label: "Sales order"
    type: number
    hidden: yes
    sql: ${TABLE}.canceled_amount_usd ;;
  }

  dimension: discount_amount {
    group_label: "Sales order"
    type: number
    hidden: yes
    sql: ${TABLE}.discount_amount ;;
  }

  dimension: discount_refunded {
    group_label: "Sales order"
    type: number
    hidden: yes
    sql: ${TABLE}.discount_refunded ;;
  }

  dimension: entity_id {
    group_label: "Sales order"
    type: string
    sql: ${TABLE}.entity_id ;;
  }

  dimension: gross_net_sales {
    group_label: "Sales order"
    type: number
    hidden: yes
    sql: ${TABLE}.gross_net_sales ;;
  }

  dimension: gross_net_sales_usd {
    group_label: "Sales order"
    type: number
    hidden: yes
    sql: ${TABLE}.gross_net_sales_usd ;;
  }

  dimension: gross_revenue_amount_local {
    group_label: "Sales order"
    type: number
    hidden: yes
    sql: ${TABLE}.gross_local ;;
  }

  dimension: gross_revenue_amount_usd {
    group_label: "Sales order"
    type: number
    hidden: yes
    sql: ${TABLE}.gross_usd ;;
  }

  dimension: item_comments {
    group_label: "Sales order"
    type: string
    sql: ${TABLE}.item_comments ;;
  }

  dimension_group: order {
    group_label: "Sales order"
    hidden: yes
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.order_date ;;
  }

  dimension_group: order_datetime {
    group_label: "Sales order"
    hidden: yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.order_datetime AS TIMESTAMP) ;;
  }

  dimension: order_id {
    group_label: "Sales order"
    type: string
    sql: ${TABLE}.order_id ;;
  }

  dimension: price {
    group_label: "Sales order"
    type: number
    sql: ${TABLE}.price ;;
  }

  dimension: qty_ordered {
    group_label: "Sales order"
    hidden: yes
    type: number
    sql: ${TABLE}.qty_ordered ;;
  }

  dimension: qty_refunded {
    group_label: "Sales order"
    hidden: yes
    type: number
    sql: ${TABLE}.qty_refunded ;;
  }

  dimension: qty_shipped {
    group_label: "Sales order"
    hidden: yes
    type: number
    sql: ${TABLE}.qty_shipped ;;
  }

  dimension_group: record {
    group_label: "Sales order"
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.record_date ;;
  }

  dimension_group: record_datetime {
    group_label: "Sales order"
    type: time
    timeframes: [
      raw,
      second,
      minute,
      hour,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql:${TABLE}.record_datetime ;;
  }

  dimension: record_type {
    group_label: "Sales order"
    type: string
    sql: ${TABLE}.record_type ;;
  }

  dimension: final_record_status {
    group_label: "Sales order"
    label: "Final Record Status"
    type: string
    sql: ${TABLE}.final_record_status ;;
  }




  dimension: refund_amount_local {
    group_label: "Sales order"
    type: number
    sql: ${TABLE}.refund_amount_local ;;
  }

  dimension: refund_amount_usd {
    group_label: "Sales order"
    type: number
    sql: ${TABLE}.refund_amount_usd ;;
  }

  dimension: shipment_net_sales {
    group_label: "Sales order"
    hidden: yes
    type: number
    sql: ${TABLE}.shipment_net_sales ;;
  }

  dimension: shipment_net_sales_usd {
    group_label: "Sales order"
    hidden: yes
    type: number
    sql: ${TABLE}.shipment_net_sales_usd ;;
  }

  dimension: shipped_rev_amount_local {
    group_label: "Sales order"
    hidden: yes
    type: number
    sql: ${TABLE}.shipped_local ;;
  }

  dimension: shipped_rev_amount_usd {
    group_label: "Sales order"
    type: number
    hidden: yes
    sql: ${TABLE}.shipped_usd ;;
  }

  dimension: sku {
    group_label: "Sales order"
    type: string
    sql: ${TABLE}.sku ;;
  }

  dimension: source {
    group_label: "Sales order"
    type: string
    sql: ${TABLE}.source ;;
  }

  dimension: status {
    group_label: "Sales order"
    type: string
    sql: ${TABLE}.status ;;
  }

  dimension: store_country {
    group_label: "Sales order"
    type: string
    sql: ${TABLE}.store_country ;;
  }

  dimension: store_id {
    group_label: "Sales order"
    type: number
    sql: ${TABLE}.store_id ;;
  }

  dimension: tender_type_group {
    group_label: "Sales order"
    type: string
    sql: ${TABLE}.tender_type_group ;;
  }

  dimension: tender_type_id {
    group_label: "Sales order"
    type: string
    sql: ${TABLE}.payment_detail_tender_type_id ;;
  }

  dimension: unit_cost {
    group_label: "Sales order"
    type: number
    hidden: yes
    sql: ${TABLE}.unit_cost ;;
  }

  dimension: unit_cost_usd {
    group_label: "Sales order"
    type: number
    hidden: yes
    sql: ${TABLE}.unit_cost_usd ;;
  }

  dimension: city {
    group_label: "Sales order"
    type: string
    sql: upper(${TABLE}.city) ;;
  }

  dimension: high_value_order {
    type: yesno
    label: "High Value Order"
    group_label: "Sales order"
    sql: CASE WHEN upper(${TABLE}.store_country) = "SAUDI ARABIA" and ${gross_revenue_amount_usd} >= 187 then true
        WHEN upper(${TABLE}.store_country) = "UNITED ARAB EMIRATES" and ${gross_revenue_amount_usd} >= 218 then true
        WHEN upper(${TABLE}.store_country) = "KUWAIT" and ${gross_revenue_amount_usd} >= 198 then true
        WHEN upper(${TABLE}.store_country) = "QATAR" and ${gross_revenue_amount_usd} >= 247 then true
        ELSE false
        END;;
  }

  dimension: gift_bag {
    type: string
    label: "Gift Bag"
    group_label: "Sales order"
    sql: ${TABLE}.Gift_bag ;;
  }

  dimension: greeting_card {
    type: string
    label: "Greeting Card"
    group_label: "Sales order"
    sql: ${TABLE}.Greeting_Card ;;
  }

  dimension: none_gifted {
    type: string
    label: "None Gifted"
    group_label: "Sales order"
    sql: case when ${greeting_card} = "FALSE" and ${gift_bag} = "FALSE" then "none"
            when ${greeting_card} = null and ${gift_bag} = null then "none"
            else null;;
  }

  dimension: order_range {
    type: string
    label: "Order range"
    group_label: "Sales order"
    sql: CASE WHEN ${gross_revenue_amount_usd} <= 49 THEN "0$ to 49$"
              WHEN ${gross_revenue_amount_usd} > 49 and ${gross_revenue_amount_usd} <= 99 THEN "50$ to 99$"
              WHEN ${gross_revenue_amount_usd} > 99 and ${gross_revenue_amount_usd} <= 149 THEN "100$ to 149$"
              ELSE "150$ or more"
              END;;
  }

  dimension: month_name{
    type: string
    label: "Month Name"
    group_label: "Sales order"
    description: "MM"
    sql: format_date("%b", ${TABLE}.record_date);;
  }

  dimension: promotion_detail_coupon_code {
    type: string
    label: "Item level Coupon Code"
    group_label: "Sales Order"
    sql: ${TABLE}.promotion_detail_coupon_code ;;
  }

  dimension: promotion_detail_coupon_id {
    type: string
    label: "Item level Coupon ID"
    group_label: "Sales Order"
    sql: ${TABLE}.promotion_detail_coupon_id ;;
  }

  dimension: promotion_detail_promotion_id {
    type: string
    label: "Item level Promotion ID"
    group_label: "Sales Order"
    sql: ${TABLE}.promotion_detail_promotion_id ;;
  }

  dimension: promotion_detail_campaign_id {
    type: string
    label: "Item level Campaign ID"
    group_label: "Sales Order"
    sql: ${TABLE}.promotion_detail_campaign_id ;;
  }

  dimension: promotion_detail_discount {
    type: number
    label: "Item level Discount"
    group_label: "Sales Order"
    sql: ${TABLE}.promotion_detail_discount ;;
  }

  dimension: promotion_detail_promotion_text {
    type: string
    label: "Item level Promotion Text"
    group_label: "Sales Order"
    sql: ${TABLE}.promotion_detail_promotion_text ;;
  }

  dimension: order_promotion_detail_promotion_text {
    type: string
    label: "Order level Promotion Text"
    group_label: "Sales Order"
    sql: ${TABLE}.order_promotion_detail_promotion_text ;;
  }

  dimension: order_promotion_detail_promotion_id {
    type: string
    label: "Order level Promotion ID"
    group_label: "Sales Order"
    sql: ${TABLE}.order_promotion_detail_promotion_id ;;
  }

  dimension: order_promotion_detail_campaign_id {
    type: string
    label: "Order level Campaign ID"
    group_label: "Sales Order"
    sql: ${TABLE}.order_promotion_detail_campaign_id ;;
  }

  dimension: order_promotion_detail_coupon_id {
    type: string
    label: "Order level Coupon ID"
    group_label: "Sales Order"
    sql: ${TABLE}.order_promotion_detail_coupon_id ;;
  }

  dimension: order_promotion_detail_coupon_code {
    type: string
    label: "Order level Coupon Code"
    group_label: "Sales Order"
    sql: ${TABLE}.order_promotion_detail_coupon_code ;;
  }

  dimension: order_promotion_detail_discount {
    type: number
    label: "Order level Detail Discount"
    group_label: "Sales Order"
    sql: ${TABLE}.order_promotion_detail_discount ;;
  }

    ######################################
    ########    GA dimensions    #########
    ######################################

    dimension: sessions {
      hidden: yes
      group_label: "Acquisition / Marketing"
      type: number
      sql: ${TABLE}.sessions ;;
    }

    dimension: session_duration {
      hidden: yes
      group_label: "Acquisition / Marketing"
      type: number
      sql: ${TABLE}.session_duration ;;
    }

    dimension: bounces {
      hidden: yes
      group_label: "Acquisition / Marketing"
      type: number
      sql: ${TABLE}.bounces ;;
    }

    dimension: pageviews {
      hidden: yes
      group_label: "Acquisition / Marketing"
      type: number
      sql: ${TABLE}.pageviews ;;
    }

    dimension: unique_pageviews {
      hidden: yes
      group_label: "Acquisition / Marketing"
      type: number
      sql: ${TABLE}.unique_pageviews ;;
    }

    dimension: product_adds_to_cart {
      hidden: yes
      group_label: "Acquisition / Marketing"
      type: number
      sql: ${TABLE}.product_adds_to_cart ;;
    }

    dimension: product_checkouts {
      hidden: yes
      group_label: "Acquisition / Marketing"
      type: number
      sql: ${TABLE}.product_checkouts ;;
    }

    dimension: users {
      hidden: yes
      group_label: "Acquisition / Marketing"
      type: number
      sql: ${TABLE}.users ;;
    }

    dimension: device_category {
      group_label: "Acquisition / Marketing"
      type: string
      sql: ${TABLE}.device_category ;;
    }

    dimension: keyword {
      group_label: "Acquisition / Marketing"
      type: string
      sql: ${TABLE}.keyword ;;
    }

    dimension: ga_source {
      group_label: "Acquisition / Marketing"
      type: string
      sql: ${TABLE}.ga_source ;;
    }


    dimension: medium {
      group_label: "Acquisition / Marketing"
      type: string
      sql: ${TABLE}.medium ;;
    }

    dimension: shipment_country {
      group_label: "Acquisition / Marketing"
      type: string
      sql: ${TABLE}.shipment_country ;;
    }

    dimension: user_type {
      group_label: "Acquisition / Marketing"
      type: string
      sql: ${TABLE}.user_type ;;
    }

    ######################################
    ######    funnel dimensions    #######
    ######################################

    dimension: campaign {
      group_label: "Acquisition / Marketing"
      type: string
      sql: ${TABLE}.campaign ;;
    }

    dimension: common_cost_usd {
      hidden: yes
      group_label: "Acquisition / Marketing"
      type: number
      sql: ${TABLE}.common_cost_usd ;;
    }

    dimension: common_impressions {
      hidden: yes
      group_label: "Acquisition / Marketing"
      type: number
      sql: ${TABLE}.common_impressions ;;
    }

    dimension: common_clicks {
      hidden: yes
      group_label: "Acquisition / Marketing"
      type: number
      sql: ${TABLE}.common_clicks ;;
    }

  dimension: marketing_source {
    type: string
    group_label: "Acquisition / Marketing"
    description: "Acquisition Platform"
    label: "Marketing Source"
    sql: ${TABLE}.marketing_source ;;
  }


    ##############################################################################
    ##########################         MEASURES         ##########################
    ##############################################################################

# <<<<<<< HEAD
# =======
#   measure: cost_of_acquisition{
#     type: number
#     value_format_name: decimal_2
#     description: "The marketing cost of one acquired new customer"
#     group_label: "Funnel"
#     label: "Cost of Acquisition"
#     sql: NULLIF(${total_cost_usd} / NULLIF(${ecomm_customer_new_returning.new_cust_count},0),0) ;;
#   }
#
# >>>>>>> branch 'master' of https://gitlab.com/chalhoub-data/looker/chalhoubdata.git
    ######################################
    #####    sales order measures    #####
    ######################################

    measure: sfcc_gross_revenue_usd {
      type: sum
      label: "Gross Revenue in USD"
      group_label: "Sales order"
      value_format_name: usd
      description: "Total value of incoming orders on the website (excluding failed credit card attempts, VAT, customs and shipping fees and COD fees) in USD"
      sql:  ${gross_revenue_amount_usd} ;;
      filters: [record_type: "SALE"]
    }

    measure: sfcc_gross_cancelled_usd{
      type: sum
      hidden: yes
      label: "Cancelled orders ($)"
      group_label: "Sales order"
      value_format_name: usd
      description: "Total value of cancelled orders"
      sql: ${canceled_amount_usd};;
      filters: [record_type: "CANCELLED"]
    }

  measure: sfcc_gross_cancelled_item_usd{
    type: sum
    label: "Cancelled items ($)"
    group_label: "Sales order"
    value_format_name: usd
    description: "Total value of cancelled items"
    sql: ${canceled_amount_usd};;
    filters: [record_type: "CANCELLED"]
  }

    measure: sfcc_gross_returns_usd {
      type: sum
      label: "Returned orders ($)"
      group_label: "Sales order"
      value_format_name: usd
      description: "Total value of returned orders"
      sql: ${refund_amount_usd};;
      filters: [record_type: "REFUND"]
    }

    measure: sfcc_gross_revenue_less_cancellatios_usd {
      type: number
      label: "Gross Revenue w/o Cancellations (USD)"
      group_label: "Sales order"
      description: "Total value of incoming orders on the website excluding cancellations (excluding failed credit card attempts, VAT, customs and shipping fees and COD fees)"
      value_format_name: usd
      sql: ${sfcc_gross_revenue_usd} - ${sfcc_gross_cancelled_usd} ;;
    }

    measure: sfcc_gross_revenue_less_cancellations_less_returns_usd {
      type: number
      label: "Gross Revenue w/o cancellation and returns  (USD)"
      group_label: "Sales order"
      value_format_name: usd
      description: "Total value of incoming orders on the website excluding cancellations and returns (excluding failed credit card attempts, VAT, customs and shipping fees and COD fees). To be computed based on order date"
      sql: ${sfcc_gross_revenue_usd} - ${sfcc_gross_cancelled_usd} - ${sfcc_gross_returns_usd} ;;
    }

    measure: sfcc_shipped_sales {
      type: sum
      label: "Shipped Sales"
      group_label: "Sales order"
      value_format_name: usd
      description: "Total value of shipped orders (excluding failed credit card attempts, VAT and cancellations but including customs and shipping fees)"
      sql: ${shipped_rev_amount_usd};;
      filters: [record_type: "SHIPMENT"]
    }

#   measure: sfcc_gross_net_sales{
#     type: sum
#     label: "Gross Net Sales USD"
#     value_format_name: usd
#     sql: ${gross_net_sales_usd} ;;
#   }

    measure: sfcc_shipped_net_sales {
      type: number
      label: "Shipped Net Sales USD"
      group_label: "Sales order"
      value_format_name: usd
      description: "Total value of incoming orders on the website excluding cancellations and returns (excluding failed credit card attempts, VAT, customs and shipping fees and COD fees). To be computed based on shipping date"
      sql: ${sfcc_shipped_sales} - ${sfcc_gross_returns_usd} ;;
    }

    measure: sfcc_number_of_orders  {
      type: count_distinct
      label: "# of Orders"
      group_label: "Sales order"
      description: "Total number of orders placed on the website"
      sql: ${order_id} ;;
      filters: [record_type: "SALE"]
    }

    measure: count_cancelled_orders {
      type: count_distinct
      hidden:  yes
      label: "# of Cancelled Orders"
      group_label: "Sales order"
      description: "Total number of Cancelled Orders"
      sql: ${order_id};;
      filters: [record_type: "CANCELLED"]
    }


    measure: count_returned_orders {
      type: count_distinct
      hidden: yes
      label: "# of Returned Orders"
      group_label: "Sales order"
      description: "Total number of Returned Orders"
      sql: ${order_id};;
      filters: [record_type: "REFUND"]
    }

    measure: units_sold {
      type: sum
      label: "# of Units Sold"
      group_label: "Sales order"
      description: "Total number of items ordered on the website"
      sql: ${qty_ordered};;
      filters: [record_type: "SALE"]
    }

    measure: upt {
      type: number
      label: "UPT"
      group_label: "Sales order"
      description: "Average number of units within each order"
      value_format_name: decimal_2
      sql:  NULLIF(${units_sold}/NULLIF(${sfcc_number_of_orders},0),0);;
    }

    measure: aov {
      type: number
      label: "Average Order Value (USD)"
      group_label: "Sales order"
      description: "Average Value of each order"
      value_format_name: usd
      sql: NULLIF(${sfcc_gross_revenue_usd}/NULLIF(${sfcc_number_of_orders},0),0);;
    }

    measure: unit_selling_price {
      type:  number
      label: "USP"
      group_label: "Sales order"
      description: "Average price of each item ordered"
      value_format_name: usd
      sql: NULLIF(${sfcc_gross_revenue_usd} / NULLIF(${units_sold},0),0) ;;
    }

    measure: sfcc_return_rate {
      type: number
      label: "Returned Rate (%)"
      group_label: "Sales order"
      description: "Percentage of shipped items that were returned based on value"
      value_format_name: percent_2
      sql: NULLIF(${sfcc_gross_returns_usd}/NULLIF(${sfcc_shipped_sales},0),0) ;;
    }

    measure: sfcc_return_order {
      type: number
      hidden: yes
      label: "Returned Order (%)"
      group_label: "Sales order"
      description: "Percentage of shipped orders that were returned"
      value_format_name: percent_2
      sql: NULLIF(${count_returned_orders}/NULLIF(${sfcc_number_of_orders},0),0) ;;
    }

  measure: sfcc_return_items {
    type: number
    label: "Returned items (%)"
    group_label: "Sales order"
    description: "Percentage of shipped items that were returned"
    value_format_name: percent_2
    sql: NULLIF(${qty_refunded}/NULLIF(${qty_shipped},0),0) ;;
  }

    measure: sfcc_cancellation_rate{
      type: number
      label: "Cancelled Rate (%)"
      group_label: "Sales order"
      description: "Percentage of incoming items that were cancelled based on value"
      value_format_name: percent_2
      sql: NULLIF(${sfcc_gross_cancelled_usd}/NULLIF(${sfcc_gross_revenue_usd},0),0) ;;
    }

    measure: sfcc_cancellation_order{
      type: number
      label: "Cancelled Order (%)"
      group_label: "Sales order"
      description: "Percentage of incoming orders that were cancelled"
      value_format_name: percent_2
      sql: NULLIF(${count_cancelled_orders}/NULLIF(${sfcc_number_of_orders},0),0) ;;
    }

    measure: cost_unit {
      type: average
      value_format_name: decimal_2
      label: "Cost per unit local"
      group_label: "Sales order"
      description: "Average cost of a single unit in local currency"
      sql: ${unit_cost} ;;
      filters: [record_type: "SALE"]
    }

    measure: cost_unit_usd {
      type: average
      value_format_name: usd
      label: "Cost per unit USD"
      group_label: "Sales order"
      description: "Average cost of a single unit in USD"
      sql: ${unit_cost_usd} ;;
      filters: [record_type: "SALE"]
    }

    measure: count_high_value_order{
      type: count_distinct
      label: "# of high value order"
      group_label: "Sales order"
      description: "Total number of orders that are high value: FOR SAUDI ARABIA >= 187, FOR UNITED ARAB EMIRATES >= 218, FOR KUWAIT >= 198, FOR QATAR >= 247"
      sql: ${order_id};;
      filters: [high_value_order: "yes"]
    }


    measure: gross_unit_margin_percentage {
      type: number
      value_format_name: percent_2
      label: "Unit Gross Margin (%)"
      group_label: "Sales order"
      description: "Gross margin is a company's net sales revenue minus its cost of goods sold (COGS)"
      sql: CASE WHEN ${sfcc_gross_revenue_usd} = 0 then 0
               ELSE NULLIF(1 - ((${cost_unit_usd}*${units_sold}) / NULLIF(${sfcc_gross_revenue_usd},1)),0)
               END;;
    }

    measure: gross_unit_margin {
      type: number
      value_format_name: usd
      label: "Unit Gross Margin ($)"
      group_label: "Sales order"
      description: "Gross margin is a company's net sales revenue minus its cost of goods sold (COGS)"
      sql: CASE WHEN ${sfcc_gross_revenue_usd} = 0 then 0
               ELSE NULLIF(${sfcc_gross_revenue_usd} - ((${cost_unit_usd}*${units_sold})),0)
               END;;
    }

    measure: cogs {
      type: sum
      value_format_name: usd
      label: "COGS ($)"
      group_label: "Sales order"
      description: "Cost of unit sold"
      sql: ${unit_cost_usd} ;;
      filters: [record_type: "SALE"]
    }

    measure: gift_bag_count {
      type: count_distinct
      label: "# of Gift Bag"
      group_label: "Sales order"
      description: "Total number of orders that have a gift bag"
      sql: ${order_id} ;;
      filters: [gift_bag: "TRUE", record_type: "SALE", greeting_card: "FALSE"]
    }

    measure: greeting_card_count {
      type: count_distinct
      label: "# of Greeting Cards"
      group_label: "Sales order"
      description: "Total number of orders that have a greeting card"
      sql: ${order_id} ;;
      filters: [greeting_card: "TRUE", record_type: "SALE", gift_bag: "FALSE"]
    }

    measure: gift_bag_and_card {
      type: count_distinct
      label: "# of Both Greeting Card and Bag"
      group_label: "Sales order"
      description: "Total number of orders that have both a gift bag and greeting card"
      sql: ${order_id} ;;
      filters: [greeting_card: "TRUE", gift_bag: "TRUE", record_type: "SALE"]
    }

    measure: none_gifted_count {
      type: count_distinct
      label: "# of None Gifted Orders"
      group_label: "Sales order"
      description: "Total number of orders that have neither a gift bag nor greeting card"
      sql: ${order_id}  ;;
      filters: [none_gifted: "none" , record_type: "SALE"]
    }

    #measure: units_sold_cancelled {
    #  type: sum
    #  label: "# of Cancelled Units Sold"
    #  group_label: "Sales order"
    #  description: "Sum of Total Qty Ordered Cancelled"
    #  sql: ${qty};;
    #  filters: [record_type: "SALE"]
    #}

    measure: units_sold_returned {
      type: sum
      label: "# of Returned Units Sold"
      group_label: "Sales order"
      description: "Total number of items returned"
      sql: ${qty_refunded};;
    }

    measure: contribution {
      type:  percent_of_total
      label: "Contribution"
      description: "Percentage of the total market value"
      sql: ${sfcc_gross_revenue_less_cancellations_less_returns_usd} ;;
    }

    ######################################
    ########     GA measures     #########
    ######################################

    measure: session_count{
      label: "# of Sessions"
      group_label: "GA metrics"
      type: sum
      description: "Total number of sessions on the website"
      sql: CASE when ${sessions} is null then 0
        Else ${sessions}
        END;;
    }

    measure: bounce_count {
      label: "# of bounces"
      group_label: "GA metrics"
      type: sum
      description: "Total number of visitors who leave the website after only one page view"
      sql: CASE when ${bounces} is null then 0
          Else ${bounces}
          END;;
    }

    measure: bounce_rate {
      label: "Bounce Rate %"
      group_label: "GA metrics"
      type: number
      description: "Percentage of visitors who leave the website after only one page view"
      value_format_name: percent_2
      sql: NULLIF(${bounce_count},0)/NULLIF(${session_count},0) ;;
    }

    measure: pageviews_per_session {
      type: number
      label: "Pageviews per Session"
      group_label: "GA metrics"
      description: "Average number of pages viewed per session"
      value_format_name: decimal_2
      sql: ${number_of_pageviews}/NULLIF(${session_count},0) ;;
    }

    measure: avg_time_on_site {
      type: number
      label: "Avg Time On Site"
      group_label: "GA metrics"
      description: "Average of time spend on the site in seconds per session"
      value_format: "h:mm:ss"
      sql: ${sum_session_duration}/NULLIF(${session_count},0)/86400 ;;
    }

    measure: add_to_basket_rate {
      type: number
      label: "Add to Basket %"
      group_label: "GA metrics"
      description: "Percentage of product detail views that have added the product to their basket over a period of time"
      value_format_name: percent_2
      sql: ${sum_product_adds_to_cart}/NULLIF(${number_of_pageviews},0) ;;
    }

    measure: cart_abandonment_rate {
      type: number
      label: "Cart Abandonment %"
      group_label: "GA metrics"
      description: "Percentage of cart created that did not convert into an order (1-% cart abandonment = cart compeltion rate)"
      value_format_name: percent_2
      sql: (${sum_product_adds_to_cart}-${sum_product_checkouts})/NULLIF(${sum_product_adds_to_cart},0) ;;
    }

    measure: checkout_to_add_rate {
      type: number
      label: "Checkout to Add to Basket %"
      group_label: "GA metrics"
      description: "Percentage of products that were added to cart and converted into an order"
      value_format_name: percent_2
      sql: ${sum_product_checkouts}/NULLIF(${sum_product_adds_to_cart},0) ;;
    }

    measure: sfcc_conversion_rate {
      type: number
      label: "Conversion Rate"
      group_label: "GA metrics"
      description: "Percentage of sessions that make an order"
      value_format_name: percent_2
      sql: NULLIF(${sfcc_number_of_orders} / NULLIF(${session_count},0),0) ;;
    }

    ######################################
    ######     funnel measures     #######
    ######################################

    measure: total_cost_usd {
      type: sum
      label: "Total Ad Spend (USD)"
      group_label: "Funnel metrics"
      value_format_name: usd
      description: "Total value of marketing ads spent across all different channels"
      sql: ${common_cost_usd};;
    }

    measure: total_clicks {
      type: sum
      label: "Total clicks"
      group_label: "Funnel metrics"
      hidden: yes
      description: "Total number of times an ad is clicked by a user"
      sql: ${common_clicks} ;;
    }

    measure: cost_per_click {
      type: number
      label: "CPC"
      group_label: "Funnel metrics"
      description: "The marketing cost of one ad click"
      value_format_name: usd
      sql: ${total_cost_usd} / NULLIF(${total_clicks},0) ;;
    }

    measure: click_through_rate {
      type: number
      label: "Click Through Rate %"
      group_label: "Funnel metrics"
      description: "Percentage ad impressions that have led to a click"
      value_format_name: percent_2
      sql: NULLIF(${total_clicks},0) / NULLIF(${impressions},0);;
    }

    measure: cost_per_mill {
      type: number
      label: "CPM"
      group_label: "Funnel metrics"
      description: "The marketing cost of 1000 impressions"
      value_format_name: usd
      sql: NULLIF(NULLIF(${total_cost_usd},0) / NULLIF(${impressions}/1000,0),0) ;;
    }

    measure: clicks_per_day {
      type: number
      value_format_name: decimal_2
      label: "Clicks per Day"
      group_label: "Funnel metrics"
      description: "Average number of clicks in a day for a given period
      "
      sql: ${total_clicks} / NULLIF(${number_of_days},0) ;;
    }

    measure: cost_per_day {
      type: number
      value_format_name: decimal_2
      label: "Cost per Day"
      description: "Average ad spend in value in a day for a given period"
      group_label: "Funnel metrics"
      sql: ${total_cost_usd} / NULLIF(${number_of_days},0) ;;
    }

    measure: sfcc_roas {
      type: number
      label: "ROAS"
      group_label: "Funnel metrics"
      description: "The Return in Gross Sales coming from your Total Ad Spend (based on Gross Sales)"
      value_format_name: decimal_2
      sql: NULLIF(${sfcc_gross_revenue_usd} / NULLIF(${total_cost_usd},0),0) ;;
    }

    measure: sfcc_cpo {
      type: number
      label: "CPO"
      description: "The marketing cost of one order"
      group_label: "Funnel metrics"
      value_format_name: usd
      sql: NULLIF(${total_cost_usd} / NULLIF(${sfcc_number_of_orders},0),0) ;;
    }

    measure: cost_per_session {
      type: number
      value_format_name: decimal_2
      label: "Cost per Session"
      group_label: "Funnel metrics"
      description: "The marketing cost of one website session"
      sql: NULLIF(${total_cost_usd} / NULLIF(${session_count},0),0) ;;
    }

#     measure: cost_of_acquisition{
#       type: number
#       value_format_name: decimal_2
#       group_label: "Funnel"
#       label: "Cost of Acquisition"
#       description: "The marketing cost of one acquired customer"
#       sql: NULLIF(${total_cost_usd} / NULLIF(${ecomm_customer_type_pop.new_cust_count},0),0) ;;
#     }

    #########################
    #### UNUSED MEASURES ####
    #########################

    measure: sum_session_duration {
      type: sum
      hidden: yes
      label: "Sessions duration"
      sql: ${TABLE}.session_duration ;;
    }

    measure: number_of_pageviews {
      type: sum
      hidden: yes
      label: "# of Page views"
      sql: ${TABLE}.pageviews ;;
    }

    measure: sum_unique_pageviews {
      type: sum
      hidden: yes
      label: "# of Unique Page views"
      sql: ${TABLE}.unique_pageviews ;;
    }

    measure: sum_product_adds_to_cart {
      type: sum
      hidden: yes
      label: "# of Products Added to Cart"
      sql: ${TABLE}.product_adds_to_cart ;;
    }

    measure: sum_product_checkouts {
      type: sum
      hidden: yes
      label: "# of Product Checkouts"
      sql: ${TABLE}.product_checkouts ;;
    }

    measure: sum_users {
      type: sum
      hidden: yes
      label: "# of Users"
      sql: ${TABLE}.users ;;
    }

    measure: impressions {
      type: sum
      hidden: yes
      label: "# of Impressions"
      sql:  ${common_impressions};;
    }

    measure: number_of_days {
      type: count_distinct
      hidden: yes
      sql: ${common_date_date} ;;
    }

#   measure: number_of_days {
#     type: number
#     sql: date_diff('day', MAX(${record_datetime_date}), MIN(${record_datetime_date})) ;;
#   }

    measure: count {
      type: count
#       drill_fields: [group_name, class_name, dept_name, subclass_name]
    }

##########################
#######   ALERTS   #######
##########################

  # dimension_group: record {
  #   type: time
  #   timeframes: [
  #     raw,
  #     second,
  #     minute,
  #     hour,
  #     date,
  #     week,
  #     month,
  #     quarter,
  #     year,
  #     week_of_year,
  #     day_of_week
  #   ]
  #   convert_tz: no
  #   datatype: datetime
  #   label: "Record"
  #   sql: ${TABLE}.record_datetime ;;
  # }


  measure: minutes_since_last_update {
    type: min
    group_label: "Alerts"
    sql: DATETIME_DIFF(current_datetime(),${record_datetime_raw},minute)+240 ;;
  }
  }
