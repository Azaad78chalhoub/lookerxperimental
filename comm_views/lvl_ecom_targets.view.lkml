view: lvl_ecom_targets {
  sql_table_name: `chb-prod-data-comm.prod_commercial.level_shoes_dashboard_target`
    ;;

  dimension: country {
    type: string
    sql: ${TABLE}.Country ;;
  }

  dimension: date_raw_date {
    type: date
    primary_key: yes
    hidden: yes
    sql: ${TABLE}.date ;;
  }

  dimension_group: date {
    type: time
    hidden: yes
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.date ;;
  }

  dimension: date_pk_raw {
    #primary_key: yes
    hidden: yes
    type: date
    sql: ${TABLE}.date ;;
  }

  dimension: month {
    type: string
    hidden: yes
    sql: ${TABLE}.month ;;
  }

  dimension: day {
    type: string
    hidden: yes
    sql: ${TABLE}.day ;;
  }

  dimension: week_number {
    type: string
    hidden: yes
    sql: ${TABLE}.week_number ;;
  }





  dimension: cancellation_percent {
    type: number
    hidden: yes
    sql: ${TABLE}.cancellation_percent ;;
  }

  dimension: cancellations_aed {
    type: number
    hidden: yes
    sql: ${TABLE}.cancellations_aed ;;
  }

  dimension: conversion_percent {
    type: number
    hidden: yes
    sql: ${TABLE}.conversion_percent ;;
  }

  dimension: sessions {
    type: number
    hidden: yes
    sql: ${TABLE}.sessions ;;
  }

  dimension: orders {
    type: number
    hidden: yes
    sql: ${TABLE}.orders ;;
  }

  dimension: gross_aov_aed {
    type: number
    hidden: yes
    sql: ${TABLE}.gross_aov_aed ;;
  }

  dimension: gross_revenue_aed {
    type: number
    hidden: yes
    sql: ${TABLE}.gross_revenue_aed ;;
  }

  dimension: shipped_revenue_aed {
    type: number
    hidden: yes
    sql: ${TABLE}.shipped_revenue_aed ;;
  }

  dimension: returns_aed {
    type: number
    hidden: yes
    sql: ${TABLE}.returns_aed ;;
  }

  dimension: return_percent {
    type: number
    hidden: yes
    sql: ${TABLE}.return_percent ;;
  }

  dimension: net_sales_aed {
    type: number
    hidden: yes
    sql: ${TABLE}.net_sales_aed ;;
  }








  measure: sessions_target  {
    label: " # of Sessions Target"
    group_label: "Ecom Targets"
    type: sum
    sql: ${sessions} ;;
  }

  measure: conversion_target  {
    label: "Conversion % Target"
    group_label: "Ecom Targets"
    type: average
    value_format_name: percent_2
    sql: ${conversion_percent} ;;
  }

  measure: orders_target  {
    label: " # of Sales Orders Target"
    group_label: "Ecom Targets"
    type: sum
    sql: ${orders} ;;
  }

  measure: gross_aov_aed_target  {
    label: "Gross AOV (AED) Target"
    group_label: "Ecom Targets"
    type: average
    value_format_name: decimal_2
    sql: ${gross_aov_aed} ;;
  }

  measure: gross_revenue_aed_target  {
    label: "Gross revenue (AED) Target"
    group_label: "Ecom Targets"
    type: sum
    sql: ${gross_revenue_aed} ;;
  }

  measure: cancellation_aed_target  {
    label: "Cancellation (AED) Target"
    group_label: "Ecom Targets"
    type: sum
    sql: ${cancellations_aed} ;;
  }

  measure: cancellation_target  {
    label: "Cancellation % Target"
    group_label: "Ecom Targets"
    type: average
    value_format_name: percent_2
    sql: ${cancellation_percent} ;;
  }

  measure: shipping_revenue_aed_target  {
    label: "Shipping revenue (AED) Target"
    group_label: "Ecom Targets"
    type: sum
    sql: ${shipped_revenue_aed} ;;
  }

  measure: returns_aed_target  {
    label: "Returns (AED) Target"
    group_label: "Ecom Targets"
    type: sum
    sql: ${returns_aed} ;;
  }

  measure: return_target  {
    label: "Returns % Target"
    group_label: "Ecom Targets"
    type: average
    value_format_name: percent_2
    sql: ${return_percent} ;;
  }

  measure: net_sales_aed_target  {
    label: "Net sales (AED) Target"
    group_label: "Ecom Targets"
    type: sum
    sql: ${net_sales_aed} ;;
  }

  measure: count {
    type: count
    hidden: yes
    drill_fields: []
  }
}
