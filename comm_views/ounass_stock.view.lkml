view: ounass_stock {
  sql_table_name: `chb-prod-data-comm.prod_commercial.ounass_stock`
    ;;
  label: "Stock"
  dimension: pk {
    primary_key: yes
    hidden: yes
    type: string
    sql:CONCAT(${sku},${brand_name},${ingestion_time},${size},${color}) ;;
  }

  dimension: brand_name {
    type: string
    sql: ${TABLE}.brandName ;;
  }

  dimension: category {
    type: string
    sql: ${TABLE}.category ;;
  }

  dimension: color {
    type: string
    sql: ${TABLE}.color ;;
  }

  dimension: gender {
    type: string
    sql: ${TABLE}.gender ;;
  }

  dimension_group: ingestion {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.ingestion_date ;;
  }

  dimension: item_name {
    type: string
    sql: ${TABLE}.itemName ;;
  }

  dimension: season {
    type: string
    sql: ${TABLE}.season ;;
  }

  dimension: size {
    type: string
    sql: ${TABLE}.size ;;
  }

  dimension: sku {
    type: string
    sql: ${TABLE}.sku ;;
  }

  dimension: status {
    type: string
    sql: ${TABLE}.status ;;
  }

  dimension: vendor_id {
    type: string
    sql: ${TABLE}.vendorId ;;
  }

  dimension: vendor_name {
    type: string
    sql: ${TABLE}.vendorName ;;
  }

  dimension: vpn {
    type: string
    sql: ${TABLE}.vpn ;;
  }

  dimension: quantity {
    hidden: yes
    type: number
    sql: CAST(${TABLE}.quantity AS NUMERIC);;
  }

  dimension: is_latest_ingestion_date {
    type: yesno
    sql: ${ingestion_date} = (SELECT MAX(DATE(ingestion_date)) FROM `chb-prod-data-comm.prod_commercial.ounass_stock`);;
  }

  measure: last_day_soh_quantity {
    type: sum
    label: "Closing Stock On Hand"
    sql: ${quantity};;
    filters: [status: "STOCK_ON_HAND", is_latest_ingestion_date: "yes"]
  }

}
