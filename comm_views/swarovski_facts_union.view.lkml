include: "../_common/period_over_period.view"
include: "../customer_views/ecomm_customer_new_returning.view"


view: swarovski_facts_union {
  sql_table_name: `chb-prod-data-comm.prod_commercial.swarovski_facts_union`
    ;;

  dimension: brand_security{
    type: string
    hidden: yes
    sql: ${TABLE}.brand_name ;;
  }

  dimension: vertical_security{
    type: string
    hidden: yes
    sql: ${TABLE}.vertical ;;
  }

  dimension: country_security{
    type: string
    hidden: yes
    sql: ${TABLE}.bu_country ;;
  }

  dimension: origin {
    hidden: yes
    type: string
    sql: ${TABLE}.origin ;;
  }

  dimension_group: common_date {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year,
      day_of_week,
      month_name,
      day_of_month
    ]
    convert_tz: no
    hidden: yes
    datatype: date
    sql: ${TABLE}.common_date ;;
  }

  dimension_group: common {
    type: time
    timeframes: [
      raw,
      date,
      week,
      time,
      month,
      quarter,
      year,
      week_of_year,
      day_of_week
    ]
    convert_tz: no
    datatype: timestamp
    label: "Date"
    description: "Date when a specific even (Record Type) took place"
    sql: ${TABLE}.common_datetime;;
  }


  dimension: business_week_no{
    type: number
    description: "Week No. extracted from the business date"
    sql: EXTRACT( WEEK FROM(PARSE_DATE('%Y-%m-%d', CAST(${TABLE}.common_date AS STRING))))+1 ;;
  }

  dimension: date_group {
    label: "Date Group"
    hidden: yes
    sql: {% if swarovski.common_date_date._in_query %} CAST(${TABLE}.common_date  AS DATE)
      {% elsif swarovski.common_date_month._in_query %} FORMAT_DATE('%Y-%m', ${TABLE}.common_date )
      {% elsif swarovski.common_date_quarter._in_query %} (FORMAT_TIMESTAMP('%Y-%m', TIMESTAMP_TRUNC(CAST(${TABLE}.common_date AS TIMESTAMP), QUARTER)))
      {% elsif swarovski.common_date_week._in_query %} CONCAT(CAST(EXTRACT(YEAR FROM ${TABLE}.common_date ) AS STRING),'-', CAST(EXTRACT(WEEK(SUNDAY) FROM ${TABLE}.common_date) AS STRING))
      {% elsif swarovski.common_date_year._in_query %} EXTRACT(YEAR FROM ${TABLE}.common_date )
      {% else %} CAST(${TABLE}.common_date  AS DATE)
      {% endif %} ;;
  }

  extends: [period_over_period]
  dimension_group: pop_no_tz {
    sql: ${common_raw} ;;
  }

  dimension: pk {
    hidden: yes
    primary_key: yes
    sql: ${TABLE}.primary_key  ;;
  }

  dimension: source_customer {
    type: string
    hidden: yes
    sql: concat("SWAROVSKI","_",${TABLE}.order_id) ;;
  }

  dimension: measure_label {

    label: "Measure Name"

    description: "Dummy dimensions shwoing measure label (used to transpose table)"

    case: {

      when: {

        label: "# of Sessions"

        sql: 1=1;;

      }

      when: {

        label: "# of Orders"

        sql: 1=1;;

      }

      when: {

        label: "Conversion Rate"

        sql: 1=1;;

      }

      when: {

        label: "Average Order Value (USD)"

        sql: 1=1;;

      }

      when: {

        label: "UPT"

        sql: 1=1;;

      }

      when: {

        label: "USP"

        sql: 1=1;;

      }

      when: {

        label: "Gross Revenue in USD"

        sql: 1=1;;

      }

      when: {

        label: "Cancelled Rate (%)"

        sql: 1=1;;

      }

      when: {

        label: "Returned Rate (%)"

        sql: 1=1;;

      }

      when: {

        label: "ROAS"

        sql: 1=1;;

      }

      when: {

        label: "Shipped Net Sales USD"

        sql: 1=1;;

      }

    }

  }

########################################
### Hidden Dimensions                ###
########################################{

  dimension: canceled_amount_local {
    group_label: "Sales order"
    type: number
    hidden: yes
    sql: ${TABLE}.canceled_amount_local ;;
  }

  dimension: canceled_amount_usd {
    group_label: "Sales order"
    type: number
    hidden: yes
    sql: ${TABLE}.canceled_amount_usd ;;
  }

  dimension: discount_amount {
    group_label: "Sales order"
    type: number
    hidden: yes
    sql: ${TABLE}.discount_amount ;;
  }

  dimension: discount_refunded {
    group_label: "Sales order"
    type: number
    hidden: yes
    sql: ${TABLE}.discount_refunded ;;
  }

  dimension: gross_net_sales {
    group_label: "Sales order"
    type: number
    hidden: yes
    sql: ${TABLE}.gross_net_sales ;;
  }

  dimension: gross_net_sales_usd {
    group_label: "Sales order"
    type: number
    hidden: yes
    sql: ${TABLE}.gross_net_sales_usd ;;
  }

  dimension: gross_revenue_amount_local {
    group_label: "Sales order"
    type: number
    hidden: yes
    sql: ${TABLE}.gross_revenue_amount_local ;;
  }

  dimension: gross_revenue_amount_usd {
    group_label: "Sales order"
    type: number
    hidden: yes
    sql: ${TABLE}.gross_revenue_amount_usd ;;
  }

  dimension_group: order {
    group_label: "Sales order"
    hidden: yes
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.order_date ;;
  }

  dimension: qty_ordered {
    group_label: "Sales order"
    hidden: yes
    type: number
    sql: ${TABLE}.qty_ordered ;;
  }

  dimension: qty_refunded {
    group_label: "Sales order"
    hidden: yes
    type: number
    sql: ${TABLE}.qty_refunded ;;
  }

  dimension: qty_canceled {
    group_label: "Sales order"
    hidden: yes
    type: number
    sql: ${TABLE}.qty_canceled ;;
  }

  dimension: qty_shipped {
    group_label: "Sales order"
    hidden: yes
    type: number
    sql: ${TABLE}.qty_shipped ;;
  }

  dimension: shipment_net_sales {
    group_label: "Sales order"
    hidden: yes
    type: number
    sql: ${TABLE}.shipment_net_sales ;;
  }

  dimension: shipment_net_sales_usd {
    group_label: "Sales order"
    hidden: yes
    type: number
    sql: ${TABLE}.shipment_net_sales_usd ;;
  }

  dimension: shipped_rev_amount_local {
    group_label: "Sales order"
    hidden: yes
    type: number
    sql: ${TABLE}.shipped_rev_amount_local ;;
  }

  dimension: shipped_rev_amount_usd {
    group_label: "Sales order"
    type: number
    hidden: yes
    sql: ${TABLE}.shipped_rev_amount_usd ;;
  }

  dimension: unit_cost {
    group_label: "Sales order"
    type: number
    hidden: yes
    sql: ${TABLE}.unit_cost ;;
  }

  dimension: unit_cost_usd {
    group_label: "Sales order"
    type: number
    hidden: yes
    sql: ${TABLE}.unit_cost_usd ;;
  }

### End of Hidden dimensions
#}



######################################
#####   sales order dimensions   #####
######################################{

  dimension: carryo_status {
    group_label: "Sales Order"
    label: "Carriyo Status"
    type: string
    sql: ${TABLE}.carryo_status ;;
  }


  dimension: refund_amount_local {
    group_label: "Sales order"
    type: number
    hidden: yes
    sql: ${TABLE}.refund_amount_local ;;
  }

  dimension: refund_amount_usd {
    group_label: "Sales order"
    type: number
    hidden: yes
    sql: ${TABLE}.refund_amount_usd ;;
  }

  dimension: order_delivery_type {
    group_label: "Sales Order"
    type: string
    hidden: no
    sql: ${TABLE}.delivery_type ;;
  }


  dimension: source {
    #group_label: "Sales Order"
    type: string
    sql: ${TABLE}.source ;;
  }

  dimension: status {
    group_label: "Sales Order"
    type: string
    sql: ${TABLE}.status ;;
  }

  dimension: store_country {
    label: "Country"
    type: string
    sql: ${TABLE}.store_country ;;
  }

  dimension: store_id {
    group_label: "Sales Order"
    type: number
    #hidden: yes
    sql: ${TABLE}.store_id ;;
  }

  dimension: tender_type_group {
    group_label: "Sales Order"
    label: "Payment Method"
    type: string
    sql: ${TABLE}.tender_type_group ;;
  }

  dimension: city {
    group_label: "Sales order"
    type: string
    hidden: yes
    sql: upper(${TABLE}.city) ;;
  }

  dimension: high_value_order {
    type: yesno
    label: "High Value Order"
    group_label: "Sales Order"
    sql: CASE WHEN upper(${TABLE}.store_country) = "SAUDI ARABIA" and ${gross_revenue_amount_usd} >= 187 then true
        WHEN upper(${TABLE}.store_country) = "UNITED ARAB EMIRATES" and ${gross_revenue_amount_usd} >= 218 then true
        WHEN upper(${TABLE}.store_country) = "KUWAIT" and ${gross_revenue_amount_usd} >= 198 then true
        WHEN upper(${TABLE}.store_country) = "QATAR" and ${gross_revenue_amount_usd} >= 247 then true
        ELSE false
        END;;
  }

  dimension: gift_bag {
    type: string
    label: "Gift Bag"
    group_label: "Sales Order"
    sql: ${TABLE}.Gift_bag ;;
  }

  dimension: greeting_card {
    type: string
    label: "Greeting Card"
    group_label: "Sales Order"
    sql: ${TABLE}.Greeting_Card ;;
  }

  dimension: none_gifted {
    type: string
    label: "None Gifted"
    group_label: "Sales Order"
    sql: case when ${greeting_card} = "FALSE" and ${gift_bag} = "FALSE" then "none"
          when ${greeting_card} = null and ${gift_bag} = null then "none"
          else null end;;
  }

  dimension: order_range {
    type: string
    label: "Order range"
    group_label: "Sales Order"
    sql: CASE WHEN ${gross_revenue_amount_usd} <= 49 THEN "0$ to 49$"
              WHEN ${gross_revenue_amount_usd} > 49 and ${gross_revenue_amount_usd} <= 99 THEN "50$ to 99$"
              WHEN ${gross_revenue_amount_usd} > 99 and ${gross_revenue_amount_usd} <= 149 THEN "100$ to 149$"
              ELSE "150$ or more"
              END;;
  }

  dimension: order_id {
    group_label: "Sales Order"
    type: string
    sql: ${TABLE}.order_id ;;
    link: {
      label: "View Order in OMS"
      url: "https://swa.retailunity.com/cpanel/cpanel_includes/inc_datatables/orders/orders_all.php7?action=edit&id={{swarovski.retailunity_order_id._value}}"
    }
  }

  dimension: retailunity_order_id {
    type: string
    hidden: yes
    sql: ${TABLE}.retailunity_order_id ;;
  }

  dimension_group: order_datetime {
    type: time
    timeframes: [
      raw,
      time,
      hour,
      date,
      week,
      month,
      quarter,
      year,
      week_of_year,
      day_of_week
    ]
    convert_tz: no
    datatype: timestamp
    label: "Sales Order"
    description: "Order Creation Date"
    sql: cast(${TABLE}.order_datetime as timestamp) ;;
  }

  dimension: month_name{
    type: string
    label: "Month Name"
    group_label: "Sales Order"
    description: "MM"
    sql: format_date("%b", ${TABLE}.record_date);;
  }

  dimension: sales_origin_name {
    type: string
    label: "Store name"
    group_label: "Sales Order"
    sql: ${TABLE}.sales_origin_name ;;
  }

  dimension: sales_origin_internal_id {
    type: string
    label: "Store ID"
    group_label: "Sales Order"
    hidden: yes
    sql: ${TABLE}.sales_origin_internal_id ;;
  }


  dimension: payment_status {
    type: string
    group_label: "Sales Order"
    sql: ${TABLE}.payment_status ;;
  }

  dimension: cancel_status {
    type: string
    group_label: "Sales Order"
    sql: ${TABLE}.cancel_status ;;
  }

  dimension: products_in_order {
    type: string
    group_label: "Sales Order"
    label: "Number of Products in Order"
    sql: ${TABLE}.products_in_order ;;
  }

  dimension: order_custom_duty_fee {
    type: number
    group_label: "Sales Order"
    label: "Order Custom Duty Fee amount (Local)"
    sql: ${TABLE}.order_custom_duty_fee ;;
  }

  dimension: order_payment_fee {
    type: number
    group_label: "Sales Order"
    label: "Order Payment Fee amount (Local)"
    sql: ${TABLE}.order_payment_fee ;;
  }

  dimension: order_shipping {
    type: number
    group_label: "Sales Order"
    label: "Order Shipping amount (Local)"
    sql: ${TABLE}.order_shipping ;;
  }

  dimension: order_total {
    type: number
    group_label: "Sales Order"
    label: "Order Total Amount (Local)"
    sql: ${TABLE}.order_total ;;
  }

  dimension: channel {
    type: string
    group_label: "Sales Order"
    sql: ${TABLE}.channel ;;
  }

  dimension: order_currency {
    type: string
    group_label: "Sales Order"
    sql: ${TABLE}.currency ;;
  }

  dimension: xstore_invoice_number {
    type: string
    hidden: yes
    group_label: "Sales Order"
    sql: ${TABLE}.xstore_invoice_number ;;
  }

  dimension: voucher_code {
    type: string
    group_label: "Sales Order"
    label: "Voucher Codes"
    sql: ${TABLE}.voucher_code ;;
  }

  dimension: coupon_code {
    type: string
    group_label: "Sales Order"
    sql: ${TABLE}.coupon_code ;;
  }

  dimension: giftwrapping {
    type: string
    label: "Gift Wrapping (Y/N)"
    description: "Shows Y (Yes) if a Gift Wrapping service item was part of the order, else N (No)"
    group_label: "Sales Order"
    sql: ${TABLE}.giftwrapping ;;
  }

### End of Sales Order dimensions
#}

########################################
### Sales Order item - Dimensions ###
########################################{

  dimension: detail_id {
    type: number
    hidden: yes
    sql: cast(${TABLE}.detail_id as int64) ;;
  }

  dimension: record_type {
    type: string
    group_label: "Sales Order Item"
    description: "Categorizes the sales order item record. Shows either SALE, CANCELLED, REFUND, SHIPMENT, ..."
    sql: ${TABLE}.record_type ;;
  }

  dimension: final_record_status {
    type: string
    label: "Latest Item Status"
    description: "Shows the last known status of the item on this order"
    group_label: "Sales Order Item"
    sql: ${TABLE}.final_record_status ;;
  }

  dimension_group: final_record {
    type: time
    timeframes: [
      time,
      date
    ]
    convert_tz: no
    datatype: date
    label: "Latest Item Status Date"
    description: "Shows the last date of the known status of the item on this order"
    group_label: "Sales Order Item"
    sql: ${TABLE}.final_status_datetime ;;
  }

  dimension: cancellation_reason {
    type: string
    label: "Item Cancel Reason"
    group_label: "Sales Order Item"
    sql: ${TABLE}.cancellation_reason ;;
  }


  dimension: cc_location_internal_id {
    type: string
    label: "Click & Collect Shop/WH Id"
    description: "Click & Collect Shop/WH Id"
    group_label: "Sales Order Item"
    sql: ${TABLE}.cc_location_internal_id ;;
  }

  dimension: cc_location_name {
    type: string
    label: "Click & Collect Shop/WH Name"
    description: "Click & Collect Shop/WH Name"
    group_label: "Sales Order Item"
    sql: ${TABLE}.cc_location_name ;;
  }

  dimension: detail_custom_duty {
    type: string
    label: "Custom Duty amount (Local)"
    group_label: "Sales Order Item"
    sql: ${TABLE}.detail_custom_duty ;;
  }

  dimension: sku {
    type: string
    label: "Source SKU"
    group_label: "Sales Order Item"
    description: "Item SKU number coming from OMS"
    sql: ${TABLE}.sku ;;
  }

  dimension: barcode {
    type: string
    label: "Source Barcode"
    group_label: "Sales Order Item"
    description: "Item Barcode number coming from OMS"
    sql: ${TABLE}.barcode ;;
  }

  dimension: detail_name {
    type: string
    hidden: yes
    group_label: "Sales Order Item"
    sql: ${TABLE}.detail_name ;;
  }

  dimension: detail_price_original {
    type: number
    group_label: "Sales Order Item"
    label: "Item Original Price (Local)"
    sql: ${TABLE}.detail_price_original ;;
  }

  dimension: price {
    type: number
    label: "Item Price (Local)"
    description: "Item price in local currency"
    group_label: "Sales Order Item"
    sql: ${TABLE}.price ;;
  }

  dimension: detail_sku_code {
    type: string
    hidden: yes
    group_label: "Sales Order Item"
    sql: ${TABLE}.detail_sku_code ;;
  }

  dimension: detail_tax {
    type: number
    group_label: "Sales Order Item"
    label: "Tax rate"
    sql: ${TABLE}.detail_tax ;;
  }

  dimension: detail_tax_amount {
    type: number
    group_label: "Sales Order Item"
    label: "Tax Amount (Local)"
    sql: ${TABLE}.detail_tax_amount ;;
  }

  dimension: employee_id {
    type: string
    group_label: "Sales Order Item"
    sql: ${TABLE}.employee_id ;;
  }

  dimension: fulfilment_employee_id {
    type: string
    group_label: "Sales Order Item"
    description: "Employee fulfilling the order item"
    sql: ${TABLE}.fulfilment_employee_id ;;
  }

  dimension: return_cancel_reason {
    type: string
    group_label: "Sales Order Item"
    label: "Item Return/Cancel Reason"
    description: "Employee handling the status update"
    sql: ${TABLE}.return_cancel_reason ;;
  }

  dimension: return_location_id {
    type: string
    hidden: yes
    sql: ${TABLE}.return_location_id ;;
  }

  dimension: return_location_internal_id {
    type: string
    group_label: "Sales Order Item"
    label: "Return WH/Store Loc Id"
    sql: ${TABLE}.return_location_internal_id ;;
  }

  dimension: return_location_name {
    type: string
    group_label: "Sales Order Item"
    label: "Return WH/Store Loc Name"
    sql: ${TABLE}.return_location_name ;;
  }

  dimension: return_quality {
    type: string
    group_label: "Sales Order Item"
    label: "Return Quality"
    sql: ${TABLE}.return_quality ;;
  }

### End of Sales Order Item dimensions
#}


########################################
### Sales Order item Shipment - Dimensions ###
########################################{

  dimension: delivery_city {
    type: string
    group_label: "Sales Order Item Shipment"
    label: "Delivery City"
    sql: ${TABLE}.city ;;
  }

  dimension: delivery_country {
    type: string
    group_label: "Sales Order Item Shipment"
    label: "Delivery Country"
    sql: ${TABLE}.delivery_country ;;
  }

  dimension: parcel_id {
    type: string
    group_label: "Sales Order Item Shipment"
    sql: ${TABLE}.parcel_id ;;
  }

  dimension: products_in_parcel {
    type: string
    group_label: "Sales Order Item Shipment"
    sql: ${TABLE}.products_in_parcel ;;
  }

  dimension: shipment_id {
    type: string
    group_label: "Sales Order Item Shipment"
    sql: ${TABLE}.shipment_id ;;
  }

  dimension: shipping_discount_amount {
    type: number
    group_label: "Sales Order Item Shipment"
    label: "Shipping Discount Amount (Local)"
    sql: ${TABLE}.shipping_discount_amount ;;
  }


  dimension: shipping_location_internal_id {
    type: string
    group_label: "Sales Order Item Shipment"
    label: "Shipping WH/Store Loc Id"
    sql: ${TABLE}.shipping_location_internal_id ;;
  }

  dimension: shipping_location_name {
    type: string
    group_label: "Sales Order Item Shipment"
    label: "Shipping WH/Store Loc Name"
    sql: ${TABLE}.shipping_location_name ;;
  }

  dimension: shipping_original_amount {
    type: number
    group_label: "Sales Order Item Shipment"
    label: "Shipping Original Amount (Local)"
    sql: ${TABLE}.shipping_original_amount ;;
  }

  dimension: tracking_id {
    type: string
    group_label: "Sales Order Item Shipment"
    sql: ${TABLE}.tracking_id ;;
  }

  dimension: shipping_location_group{
    type: string
    label: "Shipping location Group"
    group_label: "Sales Order Item Shipment"
    sql: CASE WHEN ${TABLE}.shipping_location_id like "100%" THEN "Warehouse"
              WHEN ${TABLE}.shipping_location_id in ("0", "0%", "99999") THEN "Not Used"
              WHEN ${TABLE}.shipping_location_id is NULL THEN "Not Used"
            ELSE "Store"
            END ;;
  }

### End of Sales Order item Shipment
#}

########################################
### Packed - Dimensions ###
########################################{

  dimension: qty_packed {
    type: number
    hidden: yes
    sql: ${TABLE}.qty_packed ;;
  }

  dimension: packed_revenue_local {
    type: number
    hidden: yes
    sql: ${TABLE}.packed_revenue_local ;;
  }

  dimension: packed_revenue_local_minus {
    type: number
    hidden: yes
    sql: ${TABLE}.packed_revenue_local_minus ;;
  }

  dimension: packed_revenue_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.packed_revenue_usd ;;
  }

  dimension: packed_revenue_usd_minus {
    type: number
    hidden: yes
    sql: ${TABLE}.packed_revenue_usd_minus ;;
  }

  dimension: packed_sales_minus_return_local {
    type: number
    hidden: yes
    sql: ${TABLE}.packed_sales_minus_return_local ;;
  }

  dimension: packed_sales_minus_return_local_incl_tax {
    type: number
    hidden: yes
    sql: ${TABLE}.packed_sales_minus_return_local_incl_tax ;;
  }

  dimension: packed_sales_minus_return_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.packed_sales_minus_return_usd ;;
  }

  dimension: packed_sales_minus_return_usd_incl_tax {
    type: number
    hidden: yes
    sql: ${TABLE}.packed_sales_minus_return_usd_incl_tax ;;
  }
  ### End of Packed Amounts ###}

########################################
### Promotions & Coupons - Dimensions ###
########################################{

  dimension: promotion_detail_coupon_code {
    type: string
    label: "Item - Promotion coupon code"
    description: "Item promotion coupon code"
    group_label: "Coupons - item level"
    sql: ${TABLE}.promotion_detail_coupon_code ;;
  }

  dimension: promotion_detail_coupon_id {
    type: string
    label: "Item - Coupon ID"
    group_label: "Coupons - item level"
    sql: ${TABLE}.promotion_detail_coupon_id ;;
  }

  dimension: promotion_detail_promotion_id {
    type: string
    label: "Item - Promotion ID"
    group_label: "Coupons - item level"
    sql: ${TABLE}.promotion_detail_promotion_id ;;
  }

  dimension: promotion_detail_campaign_id {
    type: string
    label: "Item - Promotion Campaign ID"
    description: "Item promotion Campaign ID"
    group_label: "Coupons - item level"
    sql: ${TABLE}.promotion_detail_campaign_id ;;
  }

  dimension: promotion_detail_discount {
    type: number
    label: "Item - Promotion discount"
    description: "Item promotion discount (Local currency)"
    group_label: "Coupons - item level"
    sql: ${TABLE}.promotion_detail_discount ;;
  }

  dimension: promotion_detail_promotion_text {
    type: string
    label: "Item - Promotion text"
    description: "Item promotion text"
    group_label: "Coupons - item level"
    sql: ${TABLE}.promotion_detail_promotion_text ;;
  }

  dimension: order_promotion_detail_promotion_text {
    type: string
    label: "Order - Promotion text"
    description: "Order promotion text"
    group_label: "Coupons - order level"
    sql: ${TABLE}.order_promotion_detail_promotion_text ;;
  }

  dimension: order_promotion_detail_promotion_id {
    type: string
    label: "Order - Promotion ID"
    group_label: "Coupons - order level"
    sql: ${TABLE}.order_promotion_detail_promotion_id ;;
  }

  dimension: order_promotion_detail_campaign_id {
    type: string
    label: "Order - Promotion Campaign ID"
    description: "Order promotion Campaign ID"
    group_label: "Coupons - order level"
    sql: ${TABLE}.order_promotion_detail_campaign_id ;;
  }

  dimension: order_promotion_detail_coupon_id {
    type: string
    label: "Orcer - Coupon ID"
    group_label: "Coupons - order level"
    sql: ${TABLE}.order_promotion_detail_coupon_id ;;
  }

  dimension: order_promotion_detail_coupon_code {
    type: string
    label: "Order - Promotion coupon code"
    description: "Order promotion coupon code"
    group_label: "Coupons - order level"
    sql: ${TABLE}.order_promotion_detail_coupon_code ;;
  }

  dimension: order_promotion_detail_discount {
    type: number
    label: "Order - Promotion discount"
    description: "Order promotion discount (Local currency)"
    group_label: "Coupons - order level"
    sql: ${TABLE}.order_promotion_detail_discount ;;
  }

### End of Promotions & Coupons dimensions
#}


  ######################################
  ########    GA dimensions    #########
  ######################################{

  dimension: sessions {
    hidden: yes
    group_label: "GA"
    type: number
    sql: ${TABLE}.sessions ;;
  }

  dimension: session_duration {
    hidden: yes
    group_label: "GA"
    type: number
    sql: ${TABLE}.session_duration ;;
  }

  dimension: bounces {
    hidden: yes
    group_label: "GA"
    type: number
    sql: ${TABLE}.bounces ;;
  }

  dimension: pageviews {
    hidden: yes
    group_label: "GA"
    type: number
    sql: ${TABLE}.pageviews ;;
  }

  dimension: unique_pageviews {
    hidden: yes
    group_label: "GA"
    type: number
    sql: ${TABLE}.unique_pageviews ;;
  }

  dimension: product_adds_to_cart {
    hidden: yes
    group_label: "GA"
    type: number
    sql: ${TABLE}.product_adds_to_cart ;;
  }

  dimension: product_checkouts {
    hidden: yes
    group_label: "GA"
    type: number
    sql: ${TABLE}.product_checkouts ;;
  }

  dimension: users {
    hidden: yes
    group_label: "GA"
    type: number
    sql: ${TABLE}.users ;;
  }

  dimension: device_category {
    group_label: "Acquisition / Marketing"
    type: string
    sql: ${TABLE}.device_category ;;
  }

  dimension: keyword {
    group_label: "Acquisition / Marketing"
    type: string
    sql: ${TABLE}.keyword ;;
  }

  dimension: ga_source {
    group_label: "Acquisition / Marketing"
    type: string
    sql: ${TABLE}.ga_source ;;
  }


  dimension: medium {
    group_label: "Acquisition / Marketing"
    type: string
    sql: ${TABLE}.medium ;;
  }

  dimension: shipment_country {
    group_label: "Acquisition / Marketing"
    type: string
    sql: ${TABLE}.shipment_country ;;
  }

  dimension: user_type {
    group_label: "Acquisition / Marketing"
    type: string
    sql: ${TABLE}.user_type ;;
  }

  dimension: ga_channel {
    group_label: "Acquisition / Marketing"
    type: string
    sql: ${TABLE}.ga_channel ;;
  }

  ### End of GA dimensions
  #}

  ######################################
  ######    funnel dimensions    #######
  ######################################{

  dimension: campaign {
    group_label: "Acquisition / Marketing"
    type: string
    sql: ${TABLE}.campaign ;;
  }

  dimension: common_cost_usd {
    hidden: yes
    group_label: "Acquisition / Marketing"
    type: number
    sql: ${TABLE}.common_cost_usd ;;
  }

  dimension: common_impressions {
    hidden: yes
    group_label: "Acquisition / Marketing"
    type: number
    sql: ${TABLE}.common_impressions ;;
  }

  dimension: common_clicks {
    hidden: yes
    group_label: "Acquisition / Marketing"
    type: number
    sql: ${TABLE}.common_clicks ;;
  }

  dimension: marketing_source {
    type: string
    group_label: "Acquisition / Marketing"
    description: "Acquisition Platform"
    label: "Marketing Source"
    sql: ${TABLE}.marketing_source ;;
  }

  ### End of Funnel dimensions
  #}

  ################################################
  #####    products dimensions DEPRECATED   ######
  ################################################{

  # dimension: activity {
  #   group_label: "Products"
  #   type: string
  #   sql: ${TABLE}.product_activity ;;
  # }

  # dimension: age_of_skin {
  #   group_label: "Products"
  #   type: string
  #   sql: ${TABLE}.product_age_of_skin ;;
  # }

  # dimension: alcohol_content_gross {
  #   group_label: "Products"
  #   type: string
  #   sql: ${TABLE}.product_alcohol_content_gross ;;
  # }

  # dimension: alcohol_content_net {
  #   group_label: "Products"
  #   type: string
  #   sql: ${TABLE}.product_alcohol_content_net ;;
  # }

  # dimension: amount_refunded {
  #   group_label: "Products"
  #   hidden: yes
  #   type: number
  #   sql: ${TABLE}.product_amount_refunded ;;
  # }

  # dimension: arabic_desc {
  #   group_label: "Products"
  #   hidden: yes
  #   type: string
  #   sql: ${TABLE}.product_arabic_desc ;;
  # }

  # dimension: atrb_boy_girl {
  #   group_label: "Products"
  #   type: string
  #   sql: ${TABLE}.product_atrb_boy_girl ;;
  # }

  # dimension: attrb_color {
  #   group_label: "Products"
  #   type: string
  #   sql: ${TABLE}.product_attrb_color ;;
  # }

  # dimension: attrb_made_of {
  #   group_label: "Products"
  #   type: string
  #   sql: ${TABLE}.product_attrb_made_of ;;
  # }

  # dimension: attrb_theme {
  #   group_label: "Products"
  #   type: string
  #   sql: ${TABLE}.product_attrb_theme ;;
  # }

  # dimension: product_barcode {
  #   group_label: "Products"
  #   type: string
  #   sql: ${TABLE}.product_barcode ;;
  # }

  # dimension: batch_ind {
  #   group_label: "Products"
  #   hidden: yes
  #   type: string
  #   sql: ${TABLE}.product_batch_ind ;;
  # }

  # dimension: brand {
  #   group_label: "Products"
  #   type: string
  #   sql: ${TABLE}.product_brand ;;
  # }

  # dimension: class {
  #   group_label: "Products"
  #   type: number
  #   sql: ${TABLE}.product_class ;;
  # }

  # dimension: class_name {
  #   group_label: "Products"
  #   type: string
  #   sql: ${TABLE}.product_class_name ;;
  # }

  # dimension: country_of_manu {
  #   group_label: "Products"
  #   type: string
  #   sql: ${TABLE}.product_country_of_manu ;;
  # }

  # dimension: dept_name {
  #   group_label: "Products"
  #   type: string
  #   sql: ${TABLE}.product_dept_name ;;
  # }

  # dimension: dept_no {
  #   group_label: "Products"
  #   hidden: yes
  #   type: number
  #   sql: ${TABLE}.product_dept_no ;;
  # }

  # dimension: dgr_ind {
  #   group_label: "Products"
  #   hidden: yes
  #   type: string
  #   sql: ${TABLE}.product_dgr_ind ;;
  # }

  # dimension: diff_1 {
  #   group_label: "Products"
  #   hidden: yes
  #   type: string
  #   sql: ${TABLE}.product_diff_1 ;;
  # }

  # dimension: diff_2 {
  #   group_label: "Products"
  #   hidden: yes
  #   type: string
  #   sql: ${TABLE}.product_diff_2 ;;
  # }

  # dimension: division {
  #   group_label: "Products"
  #   type: string
  #   sql: ${TABLE}.product_division ;;
  # }

  # dimension: division_no {
  #   group_label: "Products"
  #   hidden: yes
  #   type: number
  #   sql: ${TABLE}.product_division_no ;;
  # }

  # dimension: entity_type {
  #   group_label: "Products"
  #   type: string
  #   sql: ${TABLE}.product_entity_type ;;
  # }

  # dimension: format {
  #   group_label: "Products"
  #   type: string
  #   sql: ${TABLE}.product_format ;;
  # }

  # dimension: gender {
  #   group_label: "Products"
  #   type: string
  #   sql: ${TABLE}.product_gender ;;
  # }

  # dimension: grey_mkt_ind {
  #   group_label: "Products"
  #   hidden: yes
  #   type: string
  #   sql: ${TABLE}.product_grey_mkt_ind ;;
  # }

  # dimension: group_name {
  #   group_label: "Products"
  #   type: string
  #   sql: ${TABLE}.product_group_name ;;
  # }

  # dimension: group_no {
  #   group_label: "Products"
  #   type: number
  #   sql: ${TABLE}.product_group_no ;;
  # }

  # dimension: item {
  #   group_label: "Products"
  #   type: string
  #   sql: ${TABLE}.product_item ;;
  # }

  # dimension: item_desc {
  #   group_label: "Products"
  #   type: string
  #   sql: ${TABLE}.product_item_desc ;;
  # }

  # dimension: item_style {
  #   group_label: "Products"
  #   hidden: yes
  #   type: string
  #   sql: ${TABLE}.product_item_style ;;
  # }

  # dimension: line {
  #   group_label: "Products"
  #   type: string
  #   sql: ${TABLE}.product_line ;;
  # }

  # dimension: perishable_ind {
  #   group_label: "Products"
  #   hidden: yes
  #   type: string
  #   sql: ${TABLE}.product_perishable_ind ;;
  # }

  # dimension: recurrence {
  #   group_label: "Products"
  #   type: string
  #   sql: ${TABLE}.product_recurrence ;;
  # }

  # dimension: sap_item_code {
  #   group_label: "Products"
  #   type: string
  #   sql: ${TABLE}.product_sap_item_code ;;
  # }

  # dimension: saso_ind {
  #   group_label: "Products"
  #   hidden: yes
  #   type: string
  #   sql: ${TABLE}.product_saso_ind ;;
  # }

  # dimension: season_desc {
  #   group_label: "Products"
  #   type: string
  #   sql: ${TABLE}.product_season_desc ;;
  # }

  # dimension: sep_axis {
  #   group_label: "Products"
  #   hidden: yes
  #   type: string
  #   sql: ${TABLE}.product_sep_axis ;;
  # }

  # dimension: sep_category_item {
  #   group_label: "Products"
  #   hidden: yes
  #   type: string
  #   sql: ${TABLE}.product_sep_category_ITEM ;;
  # }

  # dimension: sep_category_sep_category {
  #   group_label: "Products"
  #   hidden: yes
  #   type: string
  #   sql: ${TABLE}.product_sep_category_SEP_CATEGORY ;;
  # }

  # dimension: sep_category_uda_value {
  #   group_label: "Products"
  #   hidden: yes
  #   type: number
  #   sql: ${TABLE}.product_sep_category_UDA_VALUE ;;
  # }

  # dimension: sep_market {
  #   group_label: "Products"
  #   type: string
  #   sql: ${TABLE}.product_sep_market ;;
  # }

  # dimension: sep_nature {
  #   group_label: "Products"
  #   type: string
  #   sql: ${TABLE}.product_sep_nature ;;
  # }

  # dimension: sep_range {
  #   group_label: "Products"
  #   type: string
  #   sql: ${TABLE}.product_sep_range ;;
  # }

  # dimension: size_uda {
  #   group_label: "Products"
  #   type: string
  #   sql: ${TABLE}.product_size_uda ;;
  # }

  # dimension: standard_uom {
  #   group_label: "Products"
  #   type: string
  #   sql: ${TABLE}.product_standard_uom ;;
  # }

  # dimension: sub_line {
  #   group_label: "Products"
  #   type: string
  #   sql: ${TABLE}.product_sub_line ;;
  # }

  # dimension: subclass {
  #   group_label: "Products"
  #   type: number
  #   sql: ${TABLE}.product_subclass ;;
  # }

  # dimension: subclass_name {
  #   group_label: "Products"
  #   type: string
  #   sql: ${TABLE}.product_subclass_name ;;
  # }

  # dimension: sup_class {
  #   group_label: "Products"
  #   type: string
  #   sql: ${TABLE}.product_sup_class ;;
  # }

  # dimension: sup_subclass {
  #   group_label: "Products"
  #   type: string
  #   sql: ${TABLE}.product_sup_subclass ;;
  # }

  # dimension: taxo_class {
  #   group_label: "Products"
  #   type: string
  #   sql: ${TABLE}.product_taxo_class ;;
  # }

  # dimension: taxo_subclass {
  #   group_label: "Products"
  #   type: string
  #   hidden: yes
  #   sql: ${TABLE}.product_taxo_subclass ;;
  # }

  # dimension: uc_code {
  #   group_label: "Products"
  #   type: string
  #   hidden: yes
  #   sql: ${TABLE}.product_uc_code ;;
  # }

  # dimension: uda_zone {
  #   group_label: "Products"
  #   type: string
  #   sql: ${TABLE}.product_uda_zone ;;
  # }

  # dimension: unique_key_per_row {
  #   group_label: "Products"
  #   type: number
  #   hidden: yes
  #   sql: ${TABLE}.product_unique_key_per_row ;;
  # }

  # dimension: usage_specificity {
  #   group_label: "Products"
  #   type: string
  #   hidden: yes
  #   sql: ${TABLE}.product_usage_specificity ;;
  # }

  # dimension: vpn {
  #   group_label: "Products"
  #   type: string
  #   sql: ${TABLE}.product_vpn ;;
  # }

  ### End of Product dimensions
  #}

  ##############################################################################
  ##########################         MEASURES         ##########################
  ##############################################################################


#####################################
##### Packed Measures
#####################################{

  measure: units_packed {
    type: sum
    group_label: "Sales order"
    label: "# of Units Packed"
    description: "Number of Items Packed"
    sql: ${qty_packed};;
  }

  measure: packed_minus_returns_usd {
    type: sum
    group_label: "Sales order"
    label: "Packed Sales minus Returns (USD)"
    value_format: "$#,##0.00"
    description:"Total value of packed items excluding returns (USD)"
    sql: ${packed_sales_minus_return_usd};;
  }

  measure: packed_minus_returns_local {
    type: sum
    group_label: "Sales order"
    label: "Packed Sales minus Returns (local)"
    value_format: "#,##0.00"
    description:"Total value of packed items excluding returns (local)"
    sql: ${packed_sales_minus_return_local};;
  }

  measure: packed_sales_local {
    type: sum
    group_label: "Sales order"
    label: "Packed Sales (local)"
    value_format: "#,##0.00"
    description:"Total value of packed items (local)"
    sql: ${packed_revenue_local};;
  }

  measure: packed_sales_usd {
    type: sum
    group_label: "Sales order"
    label: "Packed Sales (USD)"
    value_format: "$#,##0.00"
    description:"Total value of packed items (USD)"
    sql: ${packed_revenue_usd};;
  }


### End of Packed Measures ###}

  ######################################
  #####    sales order measures    #####
  ######################################{

  measure: sfcc_gross_revenue_usd {
    type: sum
    label: "Gross Revenue in USD"
    group_label: "Sales order"
    value_format_name: usd
    description: "Total value of incoming orders on the website (excluding failed credit card attempts, VAT, customs and shipping fees and COD fees) in USD"
    sql:  ${gross_revenue_amount_usd} ;;
    filters: [record_type: "SALE"]
  }

  measure: sfcc_gross_cancelled_usd{
    type: sum
    label: "Cancelled Items ($)"
    group_label: "Sales order"
    value_format_name: usd
    description: "Total value of cancelled items"
    sql: ${canceled_amount_usd};;
    filters: [record_type: "CANCELLED"]
  }

  measure: sfcc_gross_returns_usd {
    type: sum
    label: "Returned Items ($)"
    group_label: "Sales order"
    value_format_name: usd
    description: "Total value of returned items"
    sql: ${refund_amount_usd};;
    filters: [record_type: "REFUND"]
  }

  measure: sfcc_gross_revenue_less_cancellatios_usd {
    type: number
    label: "Gross Revenue w/o Cancellations (USD)"
    group_label: "Sales order"
    description: "Total value of incoming orders on the website excluding cancellations (excluding failed credit card attempts, VAT, customs and shipping fees and COD fees)"
    value_format_name: usd
    sql: ${sfcc_gross_revenue_usd} - ${sfcc_gross_cancelled_usd} ;;
  }

  measure: sfcc_gross_revenue_less_cancellations_less_returns_usd {
    type: number
    label: "Gross Revenue w/o cancellation and returns  (USD)"
    group_label: "Sales order"
    value_format_name: usd
    description: "Total value of incoming orders on the website excluding cancellations and returns (excluding failed credit card attempts, VAT, customs and shipping fees and COD fees). To be computed based on order date"
    sql: ${sfcc_gross_revenue_usd} - ${sfcc_gross_cancelled_usd} - ${sfcc_gross_returns_usd} ;;
  }

  measure: sfcc_shipped_sales {
    type: sum
    label: "Shipped Sales"
    group_label: "Sales order"
    value_format_name: usd
    description: "Total value of shipped orders (excluding failed credit card attempts, VAT and cancellations but including customs and shipping fees)"
    sql: ${shipped_rev_amount_usd};;
    #filters: [record_type: "SHIPMENT"]
  }

#   measure: sfcc_gross_net_sales{
#     type: sum
#     label: "Gross Net Sales USD"
#     value_format_name: usd
#     sql: ${gross_net_sales_usd} ;;
#   }

  measure: sfcc_shipped_net_sales {
    type: number
    label: "Shipped Net Sales USD"
    group_label: "Sales order"
    value_format_name: usd
    description: "Total value of incoming orders on the website excluding cancellations and returns (excluding failed credit card attempts, VAT, customs and shipping fees and COD fees). To be computed based on shipping date"
    sql: ${sfcc_shipped_sales} - ${sfcc_gross_returns_usd} ;;
  }

  measure: sfcc_number_of_orders  {
    type: count_distinct
    label: "# of Orders"
    group_label: "Sales order"
    description: "Total number of orders placed on the website"
    sql: ${order_id} ;;
    filters: [record_type: "SALE"]
  }

  measure: units_sold {
    type: sum
    label: "# of Units Sold"
    group_label: "Sales order"
    description: "Total number of items ordered on the website"
    sql: ${qty_ordered};;
    filters: [record_type: "SALE"]
  }

  measure: qty_collected {
    type: sum
    group_label: "Sales order"
    label: "# of Units Collected"
    description: "Number of Items Collected (Click & Collect)"
    sql: ${TABLE}.qty_collected;;
  }

  measure: qty_delivered {
    type: sum
    group_label: "Sales order"
    label: "# of Units Delivered"
    description: "Number of Items Delivered"
    sql: ${TABLE}.qty_delivered;;
  }

  measure: upt {
    type: number
    label: "UPT"
    group_label: "Sales order"
    description: "Average number of units within each order"
    value_format_name: decimal_2
    sql:  NULLIF(${units_sold}/NULLIF(${sfcc_number_of_orders},0),0);;
  }

  measure: aov {
    type: number
    label: "Average Order Value (USD)"
    group_label: "Sales order"
    description: "Average Value of each order"
    value_format_name: usd
    sql: NULLIF(${sfcc_gross_revenue_usd}/NULLIF(${sfcc_number_of_orders},0),0);;
  }

  measure: unit_selling_price {
    type:  number
    label: "USP"
    group_label: "Sales order"
    description: "Average price of each item ordered"
    value_format_name: usd
    sql: NULLIF(${sfcc_gross_revenue_usd} / NULLIF(${units_sold},0),0) ;;
  }

  measure: sfcc_return_rate {
    type: number
    label: "Returned Rate (%)"
    group_label: "Sales order"
    description: "Percentage of shipped items that were returned based on value"
    value_format_name: percent_2
    sql: NULLIF(${sfcc_gross_returns_usd}/NULLIF(${sfcc_shipped_sales},0),0) ;;
  }

  measure: sfcc_return_items {
    type: number
    label: "Returned Items (%)"
    group_label: "Sales order"
    description: "Percentage of shipped items qty that were returned out of total shipped items qty"
    value_format_name: percent_2
    sql: NULLIF(${units_sold_returned}/NULLIF(${units_sold_shipped},0),0) ;;
  }

  measure: sfcc_cancellation_rate{
    type: number
    label: "Cancellation Rate (%)"
    group_label: "Sales order"
    description: "Percentage of incoming items that were cancelled based on value"
    value_format_name: percent_2
    sql: NULLIF(${sfcc_gross_cancelled_usd}/NULLIF(${sfcc_gross_revenue_usd},0),0) ;;
  }

  measure: sfcc_cancellation_items{
    type: number
    label: "Cancelled Items (%)"
    group_label: "Sales order"
    description: "Percentage of incoming items qty that were cancelled out of total incoming items qtyd"
    value_format_name: percent_2
    sql: NULLIF(${units_sold_cancelled}/NULLIF(${units_sold},0),0) ;;
  }

  measure: cost_unit {
    type: average
    value_format_name: decimal_2
    label: "Cost per unit local"
    group_label: "Sales order"
    description: "Average cost of a single unit in local currency"
    sql: ${unit_cost} ;;
    filters: [record_type: "SALE"]
  }

  measure: cost_unit_usd {
    type: average
    value_format_name: usd
    label: "Cost per unit USD"
    group_label: "Sales order"
    description: "Average cost of a single unit in USD"
    sql: ${unit_cost_usd} ;;
    filters: [record_type: "SALE"]
  }

  measure: count_high_value_order{
    type: count_distinct
    label: "# of high value order"
    group_label: "Sales order"
    description: "Total number of orders that are high value: FOR SAUDI ARABIA >= 187, FOR UNITED ARAB EMIRATES >= 218, FOR KUWAIT >= 198, FOR QATAR >= 247"
    sql: ${order_id};;
    filters: [high_value_order: "yes"]
  }


  measure: gross_unit_margin_percentage {
    type: number
    value_format_name: percent_2
    label: "Unit Gross Margin (%)"
    group_label: "Sales order"
    description: "Gross margin is a company's net sales revenue minus its cost of goods sold (COGS)"
    sql: CASE WHEN ${sfcc_gross_revenue_usd} = 0 then 0
       ELSE NULLIF(1 - ((${cost_unit_usd}*${units_sold}) / NULLIF(${sfcc_gross_revenue_usd},1)),0)
       END;;
  }

  measure: gross_unit_margin {
    type: number
    value_format_name: usd
    label: "Unit Gross Margin ($)"
    group_label: "Sales order"
    description: "Gross margin is a company's net sales revenue minus its cost of goods sold (COGS)"
    sql: CASE WHEN ${sfcc_gross_revenue_usd} = 0 then 0
       ELSE NULLIF(${sfcc_gross_revenue_usd} - ((${cost_unit_usd}*${units_sold})),0)
       END;;
  }

  measure: cogs {
    type: sum
    value_format_name: usd
    label: "COGS ($)"
    group_label: "Sales order"
    description: "Cost of unit sold"
    sql: ${unit_cost_usd} ;;
    filters: [record_type: "SALE"]
  }

  measure: gift_bag_count {
    type: count_distinct
    label: "# of Gift Bag"
    group_label: "Sales order"
    description: "Total number of orders that have a gift bag"
    sql: ${order_id} ;;
    filters: [gift_bag: "TRUE", record_type: "SALE", greeting_card: "FALSE"]
  }

  measure: greeting_card_count {
    type: count_distinct
    label: "# of Greeting Cards"
    group_label: "Sales order"
    description: "Total number of orders that have a greeting card"
    sql: ${order_id} ;;
    filters: [greeting_card: "TRUE", record_type: "SALE", gift_bag: "FALSE"]
  }

  measure: gift_bag_and_card {
    type: count_distinct
    label: "# of Both Greeting Card and Bag"
    group_label: "Sales order"
    description: "Total number of orders that have both a gift bag and greeting card"
    sql: ${order_id} ;;
    filters: [greeting_card: "TRUE", gift_bag: "TRUE", record_type: "SALE"]
  }

  measure: none_gifted_count {
    type: count_distinct
    label: "# of None Gifted Orders"
    group_label: "Sales order"
    description: "Total number of orders that have neither a gift bag nor greeting card"
    sql: ${order_id}  ;;
    filters: [none_gifted: "none" , record_type: "SALE"]
  }


  measure: units_sold_returned {
    type: sum
    label: "# of Units Returned"
    group_label: "Sales order"
    description: "Total number of items returned"
    sql: ${qty_refunded};;
  }

  measure: units_sold_cancelled {
    type: sum
    label: "# of Units Cancelled"
    group_label: "Sales order"
    description: "Total number of items cancelled"
    sql: ${qty_canceled};;
  }

  measure: units_sold_shipped {
    type: sum
    label: "# of Units Shipped"
    group_label: "Sales order"
    description: "Total number of items shipped"
    sql: ${qty_shipped};;
  }
#   measure: contribution {
#     type:  percent_of_total
#     label: "Contribution"
#     description: "Percentage of the total market value"
#     sql: ${sfcc_gross_revenue_less_cancellations_less_returns_usd} ;;
#   }


  ### End of Sales Order measures
  #}

  ######################################
  ########     GA measures     #########
  ######################################{

  measure: session_count{
    label: "# of Sessions"
    group_label: "GA"
    type: sum
    description: "Total number of sessions on the website"
    sql: CASE when ${sessions} is null then 0
        Else ${sessions}
        END;;
  }

  measure: bounce_count {
    label: "# of bounces"
    group_label: "GA"
    type: sum
    description: "Total number of visitors who leave the website after only one page view"
    sql: CASE when ${bounces} is null then 0
          Else ${bounces}
          END;;
  }

  measure: bounce_rate {
    label: "Bounce Rate %"
    group_label: "GA"
    type: number
    description: "Percentage of visitors who leave the website after only one page view"
    value_format_name: percent_2
    sql: NULLIF(${bounce_count},0)/NULLIF(${session_count},0) ;;
  }

  measure: pageviews_per_session {
    type: number
    label: "Pageviews per Session"
    group_label: "GA"
    description: "Average number of pages viewed per session"
    value_format_name: decimal_2
    sql: ${number_of_pageviews}/NULLIF(${session_count},0) ;;
  }

  measure: avg_time_on_site {
    type: number
    label: "Avg Time On Site"
    group_label: "GA"
    description: "Average of time spend on the site in seconds per session"
    value_format: "h:mm:ss"
    sql: ${sum_session_duration}/NULLIF(${session_count},0)/86400 ;;
  }

  measure: add_to_basket_rate {
    type: number
    label: "Add to Basket %"
    group_label: "GA"
    description: "Percentage of product detail views that have added the product to their basket over a period of time"
    value_format_name: percent_2
    sql: ${sum_product_adds_to_cart}/NULLIF(${number_of_pageviews},0) ;;
  }

  measure: cart_abandonment_rate {
    type: number
    label: "Cart Abandonment %"
    group_label: "GA"
    description: "Percentage of cart created that did not convert into an order (1-% cart abandonment = cart compeltion rate)"
    value_format_name: percent_2
    sql: (${sum_product_adds_to_cart}-${sum_product_checkouts})/NULLIF(${sum_product_adds_to_cart},0) ;;
  }

  measure: checkout_to_add_rate {
    type: number
    label: "Checkout to Add to Basket %"
    group_label: "GA"
    description: "Percentage of products that were added to cart and converted into an order"
    value_format_name: percent_2
    sql: ${sum_product_checkouts}/NULLIF(${sum_product_adds_to_cart},0) ;;
  }

  measure: sfcc_conversion_rate {
    type: number
    label: "Conversion Rate"
    group_label: "GA"
    description: "Percentage of sessions that make an order"
    value_format_name: percent_2
    sql: NULLIF(${sfcc_number_of_orders} / NULLIF(${session_count},0),0) ;;
  }

  ### End of GA dimensions
  #}

  ######################################
  ######     funnel measures     #######
  ######################################{

  measure: total_cost_usd {
    type: sum
    label: "Total Ad Spend (USD)"
    group_label: "Funnel"
    value_format_name: usd
    description: "Total value of marketing ads spent across all different channels"
    sql: ${common_cost_usd};;
  }

  measure: total_clicks {
    type: sum
    label: "Total clicks"
    group_label: "Funnel"
    hidden: yes
    description: "Total number of times an ad is clicked by a user"
    sql: ${common_clicks} ;;
  }

  measure: cost_per_click {
    type: number
    label: "CPC"
    group_label: "Funnel"
    description: "The marketing cost of one ad click"
    value_format_name: usd
    sql: ${total_cost_usd} / NULLIF(${total_clicks},0) ;;
  }

  measure: click_through_rate {
    type: number
    label: "Click Through Rate %"
    group_label: "Funnel"
    description: "Percentage ad impressions that have led to a click"
    value_format_name: percent_2
    sql: NULLIF(${total_clicks},0) / NULLIF(${impressions},0);;
  }

  measure: cost_per_mill {
    type: number
    label: "CPM"
    group_label: "Funnel"
    description: "The marketing cost of 1000 impressions"
    value_format_name: usd
    sql: NULLIF(NULLIF(${total_cost_usd},0) / NULLIF(${impressions}/1000,0),0) ;;
  }

  measure: clicks_per_day {
    type: number
    value_format_name: decimal_2
    label: "Clicks per Day"
    group_label: "Funnel"
    description: "Average number of clicks in a day for a given period
    "
    sql: ${total_clicks} / NULLIF(${number_of_days},0) ;;
  }

  measure: cost_per_day {
    type: number
    value_format_name: decimal_2
    label: "Cost per Day"
    description: "Average ad spend in value in a day for a given period"
    group_label: "Funnel"
    sql: ${total_cost_usd} / NULLIF(${number_of_days},0) ;;
  }

  measure: sfcc_roas {
    type: number
    label: "ROAS"
    group_label: "Funnel"
    description: "The Return in Gross Sales coming from your Total Ad Spend (based on Gross Sales)"
    value_format_name: decimal_2
    sql: NULLIF(${sfcc_gross_revenue_usd} / NULLIF(${total_cost_usd},0),0) ;;
  }
  measure: roi_gross_rev {
    type: number
    group_label: "Funnel"
    label: "ROI"
    description: "The Return on Investment in Total Ad Spend in Gross Sales (based on Gross Sales)"
    value_format_name: decimal_2
    sql: (${sfcc_gross_revenue_usd}-${total_cost_usd})/nullif(${total_cost_usd}, 0) ;;
  }

  measure: sfcc_cpo {
    type: number
    label: "CPO"
    description: "The marketing cost of one order"
    group_label: "Funnel"
    value_format_name: usd
    sql: NULLIF(${total_cost_usd} / NULLIF(${sfcc_number_of_orders},0),0) ;;
  }

  measure: cost_per_session {
    type: number
    value_format_name: decimal_2
    label: "Cost per Session"
    group_label: "Funnel"
    description: "The marketing cost of one website session"
    sql: NULLIF(${total_cost_usd} / NULLIF(${session_count},0),0) ;;
  }

  measure: cost_of_acquisition{
    type: number
    value_format_name: decimal_2
    group_label: "Funnel"
    label: "Cost of Acquisition"
    description: "The marketing cost of one acquired customer"
    sql: NULLIF(${total_cost_usd} / NULLIF(${ecomm_customer_type_pop.new_cust_count_fixed},0),0) ;;
  }

  ### End of Funnel measures
  #}


#############################################
##########    SFCC CATEGORIES    ############
#############################################

  dimension: base_locale {
    type: string
    label: "Base Locale"
    group_label: "SFCC Categories"
    # description: ""
    sql: ${TABLE}.base_locale ;;
  }

  dimension: channel_type {
    type: string
    label: "Channel Type"
    group_label: "SFCC Categories"
    # description: ""
    sql: ${TABLE}.channel_type ;;
  }

  dimension: fulfillment_store {
    type: string
    label: "Fulfillment Store"
    group_label: "SFCC Categories"
    # description: ""
    sql: ${TABLE}.fultillment_store ;;
  }

  dimension: crm_customer_phone {
    type: string
    hidden: yes
    label: "Customer Phone number (CRM)"
    group_label: "SFCC Categories"
    # description: ""
    sql: ${TABLE}.crm_customer_phone ;;
  }

  dimension: special_request {
    type: string
    label: "Special Request"
    group_label: "SFCC Categories"
    # description: ""
    sql: ${TABLE}.special_request ;;
  }

  dimension: gift_wrap_type {
    type: string
    label: "Gift Wrap Type"
    group_label: "SFCC Categories"
    # description: ""
    sql: ${TABLE}.gift_wrap_type ;;
  }

  dimension: gift_message_indicator {
    type: string
    label: "Gift Message Indicator"
    group_label: "SFCC Categories"
    # description: ""
    sql: ${TABLE}.gift_message_indicator ;;
  }

  dimension: gift_wrap_description {
    type: string
    label: "Gift Wrap Description"
    group_label: "SFCC Categories"
    # description: ""
    sql: ${TABLE}.gift_wrap_description ;;
  }

  dimension: gift_message {
    type: string
    label: "Gift Message"
    group_label: "SFCC Categories"
    # description: ""
    sql: ${TABLE}.gift_message ;;
  }

  dimension: customer_id {
    type: string
    label: "Customer ID"
    group_label: "SFCC Categories"
    description: "SalesForce customer ID"
    sql: ${TABLE}.customer_id ;;
  }

  dimension: sale_detail_non_inventory {
    type: string
    label: "Non Inventory"
    group_label: "SFCC Categories"
    # description: ""
    sql: ${TABLE}.sale_detail_non_inventory ;;
  }

  dimension: payment_detail_tender_type_id {
    type: string
    label: "Payment Method ID"
    group_label: "SFCC Categories"
    # description: ""
    sql: ${TABLE}.payment_detail_tender_type_id ;;
  }

  dimension: payment_detail_cc_no {
    type: string
    label: "Credit Card No."
    group_label: "SFCC Categories"
    # description: ""
    sql: ${TABLE}.payment_detail_cc_no ;;
  }

  dimension: payment_detail_payment_id {
    type: string
    label: "Payment ID"
    group_label: "SFCC Categories"
    # description: ""
    sql: ${TABLE}.payment_detail_payment_id ;;
  }

  dimension: bill_to_detail_customer_fistname {
    type: string
    label: "Customer First Name"
    hidden: yes
    group_label: "SFCC Categories"
    # description: ""
    sql: ${TABLE}.bill_to_detail_customer_fistname ;;
  }

  dimension: bill_to_detail_customer_lastname {
    type: string
    hidden: yes
    label: "Customer Last Name"
    group_label: "SFCC Categories"
    # description: ""
    sql: ${TABLE}.bill_to_detail_customer_lastname ;;
  }

  dimension: bill_to_detail_customer_address {
    type: string
    label: "Customer Address"
    group_label: "SFCC Categories"
    description: "Customer Billing Address"
    sql: concat(${TABLE}.bill_to_detail_customer_address_1, ${TABLE}.bill_to_detail_customer_address_2) ;;
  }

  measure: charge_detail_shipping_charges {
    type: sum
    label: "Shipping Charges ($)"
    group_label: "SFCC Categories"
    # description: ""
    sql: ${TABLE}.charge_detail_shipping_charges ;;
  }

  # measure: charge_detail_shipping_tax_percentage {
  #   type: percentile
  #   label: "Shipping Charges (%)"
  #   group_label: "SFCC Categories"
  #   # description: ""
  #   sql: ${TABLE}.charge_detail_shipping_tax_percentage ;;
  # }

  measure: charge_detail_shipping_tax_amount {
    type: sum
    label: "Shipping Tax Amount"
    group_label: "SFCC Categories"
    # description: ""
    sql: ${TABLE}.charge_detail_shipping_tax_amount ;;
  }

  dimension: muse_detail_member_id {
    type: string
    label: "MUSE - Member ID"
    group_label: "SFCC Categories"
    # description: ""
    sql: ${TABLE}.muse_detail_member_id ;;
  }

  dimension: muse_detail_tier_level {
    type: string
    label: "MUSE - Tier Level"
    group_label: "SFCC Categories"
    # description: ""
    sql: ${TABLE}.muse_detail_tier_level ;;
  }

  measure: muse_detail_points_earned {
    type: sum
    label: "MUSE - Points Earned"
    group_label: "SFCC Categories"
    # description: ""
    sql: ${TABLE}.muse_detail_points_earned ;;
  }

  measure: sale_detail_muse_points {
    type: sum
    label: "MUSE - Points"
    group_label: "SFCC Categories"
    # description: ""
    sql: ${TABLE}.sale_detail_muse_points ;;
  }


##############################################
########## End of SFCC Categories ############}



  #########################
  #### UNUSED MEASURES ####
  #########################{

  measure: sum_session_duration {
    type: sum
    hidden: yes
    label: "Sessions duration"
    sql: ${TABLE}.session_duration ;;
  }

  measure: number_of_pageviews {
    type: sum
    hidden: yes
    label: "# of Page views"
    sql: ${TABLE}.pageviews ;;
  }

  measure: sum_unique_pageviews {
    type: sum
    hidden: yes
    label: "# of Unique Page views"
    sql: ${TABLE}.unique_pageviews ;;
  }

  measure: sum_product_adds_to_cart {
    type: sum
    hidden: yes
    label: "# of Products Added to Cart"
    sql: ${TABLE}.product_adds_to_cart ;;
  }

  measure: sum_product_checkouts {
    type: sum
    hidden: yes
    label: "# of Product Checkouts"
    sql: ${TABLE}.product_checkouts ;;
  }

  measure: sum_users {
    type: sum
    hidden: yes
    label: "# of Users"
    sql: ${TABLE}.users ;;
  }

  measure: impressions {
    type: sum
    hidden: yes
    label: "# of Impressions"
    sql:  ${common_impressions};;
  }

  measure: number_of_days {
    type: count_distinct
    hidden: yes
    sql: ${common_date_date} ;;
  }

#   measure: number_of_days {
#     type: number
#     sql: date_diff('day', MAX(${record_datetime_date}), MIN(${record_datetime_date})) ;;
#   }


  measure: count {
    type: count
    drill_fields: []
  }

  ### End of Unused dimensions
  #}

####################################
#########      ALERTS      #########
####################################


  # dimension_group: record {
  #   type: time
  #   timeframes: [
  #     raw,
  #     second,
  #     minute,
  #     hour,
  #     date,
  #     week,
  #     month,
  #     quarter,
  #     year,
  #     week_of_year,
  #     day_of_week
  #   ]
  #   convert_tz: no
  #   datatype: datetime
  #   label: "Record"
  #   sql: ${TABLE}.record_datetime ;;
  # }

  dimension_group: record_datetime {
    group_label: "Sales order"
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    hidden: yes
    sql: CAST(${TABLE}.record_datetime AS TIMESTAMP) ;;
  }


  measure: minutes_since_last_update {
    type: min
    group_label: "Alerts"
    sql: DATETIME_DIFF(current_datetime(),cast(${record_datetime_raw} as datetime),minute)+240 ;;
  }
}
