#### Event Properties ####

view: events_generated {
  extension: required

  dimension: add_shipping_info.ga_session_id {
    type: number
    sql: CASE WHEN ${event_name} = 'add_shipping_info' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'ga_session_id')
       END ;;
  }

  dimension: add_shipping_info.value {
    type: number
    sql: CASE WHEN ${event_name} = 'add_shipping_info' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'value')
       END ;;
  }

  dimension: add_shipping_info.payment_type {
    type: string
    sql: CASE WHEN ${event_name} = 'add_shipping_info' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'payment_type')
       END ;;
  }

  dimension: add_shipping_info.coupon {
    type: string
    sql: CASE WHEN ${event_name} = 'add_shipping_info' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'coupon')
       END ;;
  }

  dimension: add_shipping_info.engaged_session_event {
    type: number
    sql: CASE WHEN ${event_name} = 'add_shipping_info' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'engaged_session_event')
       END ;;
  }

  dimension: add_shipping_info.tax {
    type: number
    sql: CASE WHEN ${event_name} = 'add_shipping_info' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'tax')
       END ;;
  }

  dimension: add_shipping_info.currency {
    type: string
    sql: CASE WHEN ${event_name} = 'add_shipping_info' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'currency')
       END ;;
  }

  dimension: add_shipping_info.shipping_tier {
    type: string
    sql: CASE WHEN ${event_name} = 'add_shipping_info' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'shipping_tier')
       END ;;
  }

  dimension: add_shipping_info.ga_session_number {
    type: number
    sql: CASE WHEN ${event_name} = 'add_shipping_info' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'ga_session_number')
       END ;;
  }

  dimension: add_shipping_info.firebase_screen_class {
    type: string
    sql: CASE WHEN ${event_name} = 'add_shipping_info' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_screen_class')
       END ;;
  }

  dimension: add_shipping_info.firebase_event_origin {
    type: string
    sql: CASE WHEN ${event_name} = 'add_shipping_info' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_event_origin')
       END ;;
  }

  dimension: add_to_cart.firebase_screen_id {
    type: number
    sql: CASE WHEN ${event_name} = 'add_to_cart' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_screen_id')
       END ;;
  }

  dimension: add_to_cart.firebase_event_origin {
    type: string
    sql: CASE WHEN ${event_name} = 'add_to_cart' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_event_origin')
       END ;;
  }

  dimension: add_to_cart.ga_session_number {
    type: number
    sql: CASE WHEN ${event_name} = 'add_to_cart' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'ga_session_number')
       END ;;
  }

  dimension: add_to_cart.engaged_session_event {
    type: number
    sql: CASE WHEN ${event_name} = 'add_to_cart' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'engaged_session_event')
       END ;;
  }

  dimension: add_to_cart.item_name {
    type: string
    sql: CASE WHEN ${event_name} = 'add_to_cart' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'item_name')
       END ;;
  }

  dimension: add_to_cart.firebase_screen_class {
    type: string
    sql: CASE WHEN ${event_name} = 'add_to_cart' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_screen_class')
       END ;;
  }

  dimension: add_to_cart.item_brand {
    type: string
    sql: CASE WHEN ${event_name} = 'add_to_cart' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'item_brand')
       END ;;
  }

  dimension: add_to_cart.currency {
    type: string
    sql: CASE WHEN ${event_name} = 'add_to_cart' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'currency')
       END ;;
  }

  dimension: add_to_cart.value {
    type: number
    sql: CASE WHEN ${event_name} = 'add_to_cart' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'value')
       END ;;
  }

  dimension: add_to_cart.ga_session_id {
    type: number
    sql: CASE WHEN ${event_name} = 'add_to_cart' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'ga_session_id')
       END ;;
  }

  dimension: app_exception.timestamp {
    type: number
    sql: CASE WHEN ${event_name} = 'app_exception' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'timestamp')
       END ;;
  }

  dimension: app_exception.fatal {
    type: number
    sql: CASE WHEN ${event_name} = 'app_exception' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'fatal')
       END ;;
  }

  dimension: app_exception.firebase_event_origin {
    type: string
    sql: CASE WHEN ${event_name} = 'app_exception' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_event_origin')
       END ;;
  }

  dimension: app_exception.engaged_session_event {
    type: number
    sql: CASE WHEN ${event_name} = 'app_exception' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'engaged_session_event')
       END ;;
  }

  dimension: app_exception.ga_session_number {
    type: number
    sql: CASE WHEN ${event_name} = 'app_exception' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'ga_session_number')
       END ;;
  }

  dimension: app_exception.ga_session_id {
    type: number
    sql: CASE WHEN ${event_name} = 'app_exception' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'ga_session_id')
       END ;;
  }

  dimension: app_update.ga_session_number {
    type: number
    sql: CASE WHEN ${event_name} = 'app_update' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'ga_session_number')
       END ;;
  }

  dimension: app_update.firebase_event_origin {
    type: string
    sql: CASE WHEN ${event_name} = 'app_update' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_event_origin')
       END ;;
  }

  dimension: app_update.previous_app_version {
    type: string
    sql: CASE WHEN ${event_name} = 'app_update' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'previous_app_version')
       END ;;
  }

  dimension: app_update.firebase_screen_id {
    type: number
    sql: CASE WHEN ${event_name} = 'app_update' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_screen_id')
       END ;;
  }

  dimension: app_update.firebase_screen_class {
    type: string
    sql: CASE WHEN ${event_name} = 'app_update' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_screen_class')
       END ;;
  }

  dimension: app_update.ga_session_id {
    type: number
    sql: CASE WHEN ${event_name} = 'app_update' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'ga_session_id')
       END ;;
  }

  dimension: app_update.engaged_session_event {
    type: number
    sql: CASE WHEN ${event_name} = 'app_update' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'engaged_session_event')
       END ;;
  }

  dimension: begin_checkout.engaged_session_event {
    type: number
    sql: CASE WHEN ${event_name} = 'begin_checkout' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'engaged_session_event')
       END ;;
  }

  dimension: begin_checkout.currency {
    type: string
    sql: CASE WHEN ${event_name} = 'begin_checkout' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'currency')
       END ;;
  }

  dimension: begin_checkout.firebase_event_origin {
    type: string
    sql: CASE WHEN ${event_name} = 'begin_checkout' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_event_origin')
       END ;;
  }

  dimension: begin_checkout.ga_session_number {
    type: number
    sql: CASE WHEN ${event_name} = 'begin_checkout' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'ga_session_number')
       END ;;
  }

  dimension: begin_checkout.value {
    type: number
    sql: CASE WHEN ${event_name} = 'begin_checkout' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'value')
       END ;;
  }

  dimension: begin_checkout.tax {
    type: number
    sql: CASE WHEN ${event_name} = 'begin_checkout' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'tax')
       END ;;
  }

  dimension: begin_checkout.coupon {
    type: string
    sql: CASE WHEN ${event_name} = 'begin_checkout' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'coupon')
       END ;;
  }

  dimension: begin_checkout.firebase_screen_class {
    type: string
    sql: CASE WHEN ${event_name} = 'begin_checkout' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_screen_class')
       END ;;
  }

  dimension: begin_checkout.firebase_screen_id {
    type: number
    sql: CASE WHEN ${event_name} = 'begin_checkout' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_screen_id')
       END ;;
  }

  dimension: begin_checkout.shipping_tier {
    type: string
    sql: CASE WHEN ${event_name} = 'begin_checkout' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'shipping_tier')
       END ;;
  }

  dimension: begin_checkout.ga_session_id {
    type: number
    sql: CASE WHEN ${event_name} = 'begin_checkout' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'ga_session_id')
       END ;;
  }

  dimension: error.ga_session_number {
    type: number
    sql: CASE WHEN ${event_name} = 'error' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'ga_session_number')
       END ;;
  }

  dimension: error.firebase_screen_id {
    type: number
    sql: CASE WHEN ${event_name} = 'error' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_screen_id')
       END ;;
  }

  dimension: error.firebase_error {
    type: number
    sql: CASE WHEN ${event_name} = 'error' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_error')
       END ;;
  }

  dimension: error.firebase_event_origin {
    type: string
    sql: CASE WHEN ${event_name} = 'error' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_event_origin')
       END ;;
  }

  dimension: error.ga_session_id {
    type: number
    sql: CASE WHEN ${event_name} = 'error' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'ga_session_id')
       END ;;
  }

  dimension: error.engaged_session_event {
    type: number
    sql: CASE WHEN ${event_name} = 'error' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'engaged_session_event')
       END ;;
  }

  dimension: error.firebase_screen_class {
    type: string
    sql: CASE WHEN ${event_name} = 'error' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_screen_class')
       END ;;
  }

  dimension: firebase_campaign.term {
    type: string
    sql: CASE WHEN ${event_name} = 'firebase_campaign' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'term')
       END ;;
  }

  dimension: firebase_campaign.firebase_event_origin {
    type: string
    sql: CASE WHEN ${event_name} = 'firebase_campaign' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_event_origin')
       END ;;
  }

  dimension: firebase_campaign.medium {
    type: string
    sql: CASE WHEN ${event_name} = 'firebase_campaign' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'medium')
       END ;;
  }

  dimension: firebase_campaign.ga_session_number {
    type: number
    sql: CASE WHEN ${event_name} = 'firebase_campaign' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'ga_session_number')
       END ;;
  }

  dimension: firebase_campaign.engaged_session_event {
    type: number
    sql: CASE WHEN ${event_name} = 'firebase_campaign' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'engaged_session_event')
       END ;;
  }

  dimension: firebase_campaign.gclid {
    type: string
    sql: CASE WHEN ${event_name} = 'firebase_campaign' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'gclid')
       END ;;
  }

  dimension: firebase_campaign.firebase_screen_id {
    type: number
    sql: CASE WHEN ${event_name} = 'firebase_campaign' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_screen_id')
       END ;;
  }

  dimension: firebase_campaign.firebase_screen_class {
    type: string
    sql: CASE WHEN ${event_name} = 'firebase_campaign' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_screen_class')
       END ;;
  }

  dimension: firebase_campaign.click_timestamp {
    type: number
    sql: CASE WHEN ${event_name} = 'firebase_campaign' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'click_timestamp')
       END ;;
  }

  dimension: firebase_campaign.campaign {
    type: string
    sql: CASE WHEN ${event_name} = 'firebase_campaign' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'campaign')
       END ;;
  }

  dimension: firebase_campaign.ga_session_id {
    type: number
    sql: CASE WHEN ${event_name} = 'firebase_campaign' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'ga_session_id')
       END ;;
  }

  dimension: firebase_campaign.source {
    type: string
    sql: CASE WHEN ${event_name} = 'firebase_campaign' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'source')
       END ;;
  }

  dimension: first_open.previous_first_open_count {
    type: number
    sql: CASE WHEN ${event_name} = 'first_open' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'previous_first_open_count')
       END ;;
  }

  dimension: first_open.firebase_event_origin {
    type: string
    sql: CASE WHEN ${event_name} = 'first_open' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_event_origin')
       END ;;
  }

  dimension: first_open.update_with_analytics {
    type: number
    sql: CASE WHEN ${event_name} = 'first_open' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'update_with_analytics')
       END ;;
  }

  dimension: first_open.firebase_screen_class {
    type: string
    sql: CASE WHEN ${event_name} = 'first_open' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_screen_class')
       END ;;
  }

  dimension: first_open.firebase_screen_id {
    type: number
    sql: CASE WHEN ${event_name} = 'first_open' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_screen_id')
       END ;;
  }

  dimension: first_open.firebase_conversion {
    type: number
    sql: CASE WHEN ${event_name} = 'first_open' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_conversion')
       END ;;
  }

  dimension: first_open_consented.firebase_event_origin {
    type: string
    sql: CASE WHEN ${event_name} = 'first_open_consented' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_event_origin')
       END ;;
  }

  dimension: first_open_consented.ga_session_id {
    type: number
    sql: CASE WHEN ${event_name} = 'first_open_consented' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'ga_session_id')
       END ;;
  }

  dimension: first_open_consented.ga_session_number {
    type: number
    sql: CASE WHEN ${event_name} = 'first_open_consented' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'ga_session_number')
       END ;;
  }

  dimension: first_open_consented.engaged_session_event {
    type: number
    sql: CASE WHEN ${event_name} = 'first_open_consented' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'engaged_session_event')
       END ;;
  }

  dimension: homepage_view.ga_session_id {
    type: number
    sql: CASE WHEN ${event_name} = 'homepage_view' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'ga_session_id')
       END ;;
  }

  dimension: homepage_view.category_id {
    type: string
    sql: CASE WHEN ${event_name} = 'homepage_view' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'category_id')
       END ;;
  }

  dimension: homepage_view.firebase_screen_class {
    type: string
    sql: CASE WHEN ${event_name} = 'homepage_view' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_screen_class')
       END ;;
  }

  dimension: homepage_view.engaged_session_event {
    type: number
    sql: CASE WHEN ${event_name} = 'homepage_view' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'engaged_session_event')
       END ;;
  }

  dimension: homepage_view.firebase_screen_id {
    type: number
    sql: CASE WHEN ${event_name} = 'homepage_view' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_screen_id')
       END ;;
  }

  dimension: homepage_view.firebase_event_origin {
    type: string
    sql: CASE WHEN ${event_name} = 'homepage_view' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_event_origin')
       END ;;
  }

  dimension: homepage_view.ga_session_number {
    type: number
    sql: CASE WHEN ${event_name} = 'homepage_view' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'ga_session_number')
       END ;;
  }

  dimension: login.ga_session_id {
    type: number
    sql: CASE WHEN ${event_name} = 'login' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'ga_session_id')
       END ;;
  }

  dimension: login.firebase_screen_class {
    type: string
    sql: CASE WHEN ${event_name} = 'login' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_screen_class')
       END ;;
  }

  dimension: login.ga_session_number {
    type: number
    sql: CASE WHEN ${event_name} = 'login' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'ga_session_number')
       END ;;
  }

  dimension: login.engaged_session_event {
    type: number
    sql: CASE WHEN ${event_name} = 'login' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'engaged_session_event')
       END ;;
  }

  dimension: login.firebase_event_origin {
    type: string
    sql: CASE WHEN ${event_name} = 'login' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_event_origin')
       END ;;
  }

  dimension: login.firebase_screen_id {
    type: number
    sql: CASE WHEN ${event_name} = 'login' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_screen_id')
       END ;;
  }

  dimension: notification_open.message_name {
    type: string
    sql: CASE WHEN ${event_name} = 'notification_open' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'message_name')
       END ;;
  }

  dimension: notification_open.ga_session_number {
    type: number
    sql: CASE WHEN ${event_name} = 'notification_open' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'ga_session_number')
       END ;;
  }

  dimension: notification_open.message_device_time {
    type: string
    sql: CASE WHEN ${event_name} = 'notification_open' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'message_device_time')
       END ;;
  }

  dimension: notification_open.ga_session_id {
    type: number
    sql: CASE WHEN ${event_name} = 'notification_open' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'ga_session_id')
       END ;;
  }

  dimension: notification_open.message_id {
    type: string
    sql: CASE WHEN ${event_name} = 'notification_open' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'message_id')
       END ;;
  }

  dimension: notification_open.firebase_event_origin {
    type: string
    sql: CASE WHEN ${event_name} = 'notification_open' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_event_origin')
       END ;;
  }

  dimension: notification_open.engaged_session_event {
    type: number
    sql: CASE WHEN ${event_name} = 'notification_open' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'engaged_session_event')
       END ;;
  }

  dimension: notification_open.message_time {
    type: number
    sql: CASE WHEN ${event_name} = 'notification_open' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'message_time')
       END ;;
  }

  dimension: os_update.engaged_session_event {
    type: number
    sql: CASE WHEN ${event_name} = 'os_update' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'engaged_session_event')
       END ;;
  }

  dimension: os_update.previous_os_version {
    type: string
    sql: CASE WHEN ${event_name} = 'os_update' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'previous_os_version')
       END ;;
  }

  dimension: os_update.firebase_screen_id {
    type: number
    sql: CASE WHEN ${event_name} = 'os_update' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_screen_id')
       END ;;
  }

  dimension: os_update.firebase_screen_class {
    type: string
    sql: CASE WHEN ${event_name} = 'os_update' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_screen_class')
       END ;;
  }

  dimension: os_update.firebase_event_origin {
    type: string
    sql: CASE WHEN ${event_name} = 'os_update' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_event_origin')
       END ;;
  }

  dimension: os_update.ga_session_id {
    type: number
    sql: CASE WHEN ${event_name} = 'os_update' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'ga_session_id')
       END ;;
  }

  dimension: os_update.ga_session_number {
    type: number
    sql: CASE WHEN ${event_name} = 'os_update' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'ga_session_number')
       END ;;
  }

  dimension: purchase.engaged_session_event {
    type: number
    sql: CASE WHEN ${event_name} = 'purchase' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'engaged_session_event')
       END ;;
  }

  dimension: purchase.tax {
    type: number
    sql: CASE WHEN ${event_name} = 'purchase' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'tax')
       END ;;
  }

  dimension: purchase.ga_session_number {
    type: number
    sql: CASE WHEN ${event_name} = 'purchase' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'ga_session_number')
       END ;;
  }

  dimension: purchase.shipping_tier {
    type: string
    sql: CASE WHEN ${event_name} = 'purchase' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'shipping_tier')
       END ;;
  }

  dimension: purchase.value {
    type: number
    sql: CASE WHEN ${event_name} = 'purchase' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'value')
       END ;;
  }

  dimension: purchase.currency {
    type: string
    sql: CASE WHEN ${event_name} = 'purchase' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'currency')
       END ;;
  }

  dimension: purchase.coupon {
    type: string
    sql: CASE WHEN ${event_name} = 'purchase' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'coupon')
       END ;;
  }

  dimension: purchase.firebase_event_origin {
    type: string
    sql: CASE WHEN ${event_name} = 'purchase' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_event_origin')
       END ;;
  }

  dimension: purchase.firebase_screen_class {
    type: string
    sql: CASE WHEN ${event_name} = 'purchase' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_screen_class')
       END ;;
  }

  dimension: purchase.firebase_screen_id {
    type: number
    sql: CASE WHEN ${event_name} = 'purchase' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_screen_id')
       END ;;
  }

  dimension: purchase.ga_session_id {
    type: number
    sql: CASE WHEN ${event_name} = 'purchase' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'ga_session_id')
       END ;;
  }

  dimension: purchase.firebase_conversion {
    type: number
    sql: CASE WHEN ${event_name} = 'purchase' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_conversion')
       END ;;
  }

  dimension: purchase.transaction_id {
    type: string
    sql: CASE WHEN ${event_name} = 'purchase' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'transaction_id')
       END ;;
  }

  dimension: remove_from_cart.firebase_screen_id {
    type: number
    sql: CASE WHEN ${event_name} = 'remove_from_cart' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_screen_id')
       END ;;
  }

  dimension: remove_from_cart.item_brand {
    type: string
    sql: CASE WHEN ${event_name} = 'remove_from_cart' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'item_brand')
       END ;;
  }

  dimension: remove_from_cart.ga_session_number {
    type: number
    sql: CASE WHEN ${event_name} = 'remove_from_cart' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'ga_session_number')
       END ;;
  }

  dimension: remove_from_cart.item_name {
    type: string
    sql: CASE WHEN ${event_name} = 'remove_from_cart' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'item_name')
       END ;;
  }

  dimension: remove_from_cart.currency {
    type: string
    sql: CASE WHEN ${event_name} = 'remove_from_cart' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'currency')
       END ;;
  }

  dimension: remove_from_cart.engaged_session_event {
    type: number
    sql: CASE WHEN ${event_name} = 'remove_from_cart' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'engaged_session_event')
       END ;;
  }

  dimension: remove_from_cart.firebase_screen_class {
    type: string
    sql: CASE WHEN ${event_name} = 'remove_from_cart' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_screen_class')
       END ;;
  }

  dimension: remove_from_cart.firebase_event_origin {
    type: string
    sql: CASE WHEN ${event_name} = 'remove_from_cart' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_event_origin')
       END ;;
  }

  dimension: remove_from_cart.value {
    type: number
    sql: CASE WHEN ${event_name} = 'remove_from_cart' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'value')
       END ;;
  }

  dimension: remove_from_cart.ga_session_id {
    type: number
    sql: CASE WHEN ${event_name} = 'remove_from_cart' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'ga_session_id')
       END ;;
  }

  dimension: screen_view.firebase_screen_id {
    type: number
    sql: CASE WHEN ${event_name} = 'screen_view' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_screen_id')
       END ;;
  }

  dimension: screen_view.firebase_previous_id {
    type: number
    sql: CASE WHEN ${event_name} = 'screen_view' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_previous_id')
       END ;;
  }

  dimension: screen_view.ga_session_id {
    type: number
    sql: CASE WHEN ${event_name} = 'screen_view' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'ga_session_id')
       END ;;
  }

  dimension: screen_view.session_engaged {
    type: number
    sql: CASE WHEN ${event_name} = 'screen_view' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'session_engaged')
       END ;;
  }

  dimension: screen_view.engaged_session_event {
    type: number
    sql: CASE WHEN ${event_name} = 'screen_view' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'engaged_session_event')
       END ;;
  }

  dimension: screen_view.firebase_screen_class {
    type: string
    sql: CASE WHEN ${event_name} = 'screen_view' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_screen_class')
       END ;;
  }

  dimension: screen_view.ga_session_number {
    type: number
    sql: CASE WHEN ${event_name} = 'screen_view' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'ga_session_number')
       END ;;
  }

  dimension: screen_view.firebase_event_origin {
    type: string
    sql: CASE WHEN ${event_name} = 'screen_view' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_event_origin')
       END ;;
  }

  dimension: screen_view.firebase_previous_class {
    type: string
    sql: CASE WHEN ${event_name} = 'screen_view' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_previous_class')
       END ;;
  }

  dimension: screen_view.entrances {
    type: number
    sql: CASE WHEN ${event_name} = 'screen_view' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'entrances')
       END ;;
  }

  dimension: search.engaged_session_event {
    type: number
    sql: CASE WHEN ${event_name} = 'search' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'engaged_session_event')
       END ;;
  }

  dimension: search.firebase_event_origin {
    type: string
    sql: CASE WHEN ${event_name} = 'search' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_event_origin')
       END ;;
  }

  dimension: search.firebase_screen_id {
    type: number
    sql: CASE WHEN ${event_name} = 'search' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_screen_id')
       END ;;
  }

  dimension: search.ga_session_number {
    type: number
    sql: CASE WHEN ${event_name} = 'search' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'ga_session_number')
       END ;;
  }

  dimension: search.firebase_screen_class {
    type: string
    sql: CASE WHEN ${event_name} = 'search' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_screen_class')
       END ;;
  }

  dimension: search.search_term {
    type: string
    sql: CASE WHEN ${event_name} = 'search' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'search_term')
       END ;;
  }

  dimension: search.ga_session_id {
    type: number
    sql: CASE WHEN ${event_name} = 'search' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'ga_session_id')
       END ;;
  }

  dimension: select_item.engaged_session_event {
    type: number
    sql: CASE WHEN ${event_name} = 'select_item' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'engaged_session_event')
       END ;;
  }

  dimension: select_item.ga_session_number {
    type: number
    sql: CASE WHEN ${event_name} = 'select_item' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'ga_session_number')
       END ;;
  }

  dimension: select_item.item_list_name {
    type: string
    sql: CASE WHEN ${event_name} = 'select_item' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'item_list_name')
       END ;;
  }

  dimension: select_item.firebase_event_origin {
    type: string
    sql: CASE WHEN ${event_name} = 'select_item' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_event_origin')
       END ;;
  }

  dimension: select_item.error_value {
    type: string
    sql: CASE WHEN ${event_name} = 'select_item' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'error_value')
       END ;;
  }

  dimension: select_item.firebase_screen_class {
    type: string
    sql: CASE WHEN ${event_name} = 'select_item' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_screen_class')
       END ;;
  }

  dimension: select_item.firebase_error {
    type: number
    sql: CASE WHEN ${event_name} = 'select_item' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_error')
       END ;;
  }

  dimension: select_item.item_list_id {
    type: string
    sql: CASE WHEN ${event_name} = 'select_item' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'item_list_id')
       END ;;
  }

  dimension: select_item.firebase_screen_id {
    type: number
    sql: CASE WHEN ${event_name} = 'select_item' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_screen_id')
       END ;;
  }

  dimension: select_item.ga_session_id {
    type: number
    sql: CASE WHEN ${event_name} = 'select_item' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'ga_session_id')
       END ;;
  }

  dimension: session_start.firebase_event_origin {
    type: string
    sql: CASE WHEN ${event_name} = 'session_start' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_event_origin')
       END ;;
  }

  dimension: session_start.session_engaged {
    type: number
    sql: CASE WHEN ${event_name} = 'session_start' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'session_engaged')
       END ;;
  }

  dimension: session_start.firebase_screen_class {
    type: string
    sql: CASE WHEN ${event_name} = 'session_start' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_screen_class')
       END ;;
  }

  dimension: session_start.firebase_screen_id {
    type: number
    sql: CASE WHEN ${event_name} = 'session_start' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_screen_id')
       END ;;
  }

  dimension: session_start.ga_session_id {
    type: number
    sql: CASE WHEN ${event_name} = 'session_start' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'ga_session_id')
       END ;;
  }

  dimension: session_start.firebase_conversion {
    type: number
    sql: CASE WHEN ${event_name} = 'session_start' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_conversion')
       END ;;
  }

  dimension: session_start.ga_session_number {
    type: number
    sql: CASE WHEN ${event_name} = 'session_start' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'ga_session_number')
       END ;;
  }

  dimension: session_start.engaged_session_event {
    type: number
    sql: CASE WHEN ${event_name} = 'session_start' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'engaged_session_event')
       END ;;
  }

  dimension: sign_up.firebase_screen_class {
    type: string
    sql: CASE WHEN ${event_name} = 'sign_up' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_screen_class')
       END ;;
  }

  dimension: sign_up.ga_session_number {
    type: number
    sql: CASE WHEN ${event_name} = 'sign_up' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'ga_session_number')
       END ;;
  }

  dimension: sign_up.firebase_screen_id {
    type: number
    sql: CASE WHEN ${event_name} = 'sign_up' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_screen_id')
       END ;;
  }

  dimension: sign_up.ga_session_id {
    type: number
    sql: CASE WHEN ${event_name} = 'sign_up' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'ga_session_id')
       END ;;
  }

  dimension: sign_up.firebase_event_origin {
    type: string
    sql: CASE WHEN ${event_name} = 'sign_up' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_event_origin')
       END ;;
  }

  dimension: sign_up.engaged_session_event {
    type: number
    sql: CASE WHEN ${event_name} = 'sign_up' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'engaged_session_event')
       END ;;
  }

  dimension: user_engagement.session_engaged {
    type: number
    sql: CASE WHEN ${event_name} = 'user_engagement' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'session_engaged')
       END ;;
  }

  dimension: user_engagement.firebase_screen_id {
    type: number
    sql: CASE WHEN ${event_name} = 'user_engagement' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_screen_id')
       END ;;
  }

  dimension: user_engagement.ga_session_id {
    type: number
    sql: CASE WHEN ${event_name} = 'user_engagement' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'ga_session_id')
       END ;;
  }

  dimension: user_engagement.engagement_time_msec {
    type: number
    sql: CASE WHEN ${event_name} = 'user_engagement' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'engagement_time_msec')
       END ;;
  }

  dimension: user_engagement.engaged_session_event {
    type: number
    sql: CASE WHEN ${event_name} = 'user_engagement' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'engaged_session_event')
       END ;;
  }

  dimension: user_engagement.ga_session_number {
    type: number
    sql: CASE WHEN ${event_name} = 'user_engagement' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'ga_session_number')
       END ;;
  }

  dimension: user_engagement.firebase_event_origin {
    type: string
    sql: CASE WHEN ${event_name} = 'user_engagement' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_event_origin')
       END ;;
  }

  dimension: user_engagement.firebase_screen_class {
    type: string
    sql: CASE WHEN ${event_name} = 'user_engagement' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_screen_class')
       END ;;
  }

  dimension: view_cart.engaged_session_event {
    type: number
    sql: CASE WHEN ${event_name} = 'view_cart' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'engaged_session_event')
       END ;;
  }

  dimension: view_cart.shipping_tier {
    type: string
    sql: CASE WHEN ${event_name} = 'view_cart' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'shipping_tier')
       END ;;
  }

  dimension: view_cart.currency {
    type: string
    sql: CASE WHEN ${event_name} = 'view_cart' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'currency')
       END ;;
  }

  dimension: view_cart.coupon {
    type: string
    sql: CASE WHEN ${event_name} = 'view_cart' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'coupon')
       END ;;
  }

  dimension: view_cart.ga_session_id {
    type: number
    sql: CASE WHEN ${event_name} = 'view_cart' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'ga_session_id')
       END ;;
  }

  dimension: view_cart.firebase_event_origin {
    type: string
    sql: CASE WHEN ${event_name} = 'view_cart' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_event_origin')
       END ;;
  }

  dimension: view_cart.value {
    type: number
    sql: CASE WHEN ${event_name} = 'view_cart' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'value')
       END ;;
  }

  dimension: view_cart.firebase_screen_id {
    type: number
    sql: CASE WHEN ${event_name} = 'view_cart' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_screen_id')
       END ;;
  }

  dimension: view_cart.firebase_screen_class {
    type: string
    sql: CASE WHEN ${event_name} = 'view_cart' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_screen_class')
       END ;;
  }

  dimension: view_cart.tax {
    type: number
    sql: CASE WHEN ${event_name} = 'view_cart' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'tax')
       END ;;
  }

  dimension: view_cart.ga_session_number {
    type: number
    sql: CASE WHEN ${event_name} = 'view_cart' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'ga_session_number')
       END ;;
  }

  dimension: view_item.currency {
    type: string
    sql: CASE WHEN ${event_name} = 'view_item' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'currency')
       END ;;
  }

  dimension: view_item.engaged_session_event {
    type: number
    sql: CASE WHEN ${event_name} = 'view_item' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'engaged_session_event')
       END ;;
  }

  dimension: view_item.firebase_screen_class {
    type: string
    sql: CASE WHEN ${event_name} = 'view_item' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_screen_class')
       END ;;
  }

  dimension: view_item.ga_session_number {
    type: number
    sql: CASE WHEN ${event_name} = 'view_item' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'ga_session_number')
       END ;;
  }

  dimension: view_item.firebase_event_origin {
    type: string
    sql: CASE WHEN ${event_name} = 'view_item' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_event_origin')
       END ;;
  }

  dimension: view_item.value {
    type: number
    sql: CASE WHEN ${event_name} = 'view_item' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'value')
       END ;;
  }

  dimension: view_item.firebase_screen_id {
    type: number
    sql: CASE WHEN ${event_name} = 'view_item' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_screen_id')
       END ;;
  }

  dimension: view_item.ga_session_id {
    type: number
    sql: CASE WHEN ${event_name} = 'view_item' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'ga_session_id')
       END ;;
  }

  dimension: view_item_list.ga_session_number {
    type: number
    sql: CASE WHEN ${event_name} = 'view_item_list' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'ga_session_number')
       END ;;
  }

  dimension: view_item_list.firebase_screen_class {
    type: string
    sql: CASE WHEN ${event_name} = 'view_item_list' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_screen_class')
       END ;;
  }

  dimension: view_item_list.firebase_error {
    type: number
    sql: CASE WHEN ${event_name} = 'view_item_list' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_error')
       END ;;
  }

  dimension: view_item_list.error_value {
    type: string
    sql: CASE WHEN ${event_name} = 'view_item_list' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'error_value')
       END ;;
  }

  dimension: view_item_list.item_list_id {
    type: string
    sql: CASE WHEN ${event_name} = 'view_item_list' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'item_list_id')
       END ;;
  }

  dimension: view_item_list.ga_session_id {
    type: number
    sql: CASE WHEN ${event_name} = 'view_item_list' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'ga_session_id')
       END ;;
  }

  dimension: view_item_list.engaged_session_event {
    type: number
    sql: CASE WHEN ${event_name} = 'view_item_list' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'engaged_session_event')
       END ;;
  }

  dimension: view_item_list.firebase_screen_id {
    type: number
    sql: CASE WHEN ${event_name} = 'view_item_list' THEN
         (SELECT value.int_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_screen_id')
       END ;;
  }

  dimension: view_item_list.firebase_event_origin {
    type: string
    sql: CASE WHEN ${event_name} = 'view_item_list' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'firebase_event_origin')
       END ;;
  }

  dimension: view_item_list.item_list_name {
    type: string
    sql: CASE WHEN ${event_name} = 'view_item_list' THEN
         (SELECT value.string_value
             FROM UNNEST(${event_params})
             WHERE key = 'item_list_name')
       END ;;
  }

}
