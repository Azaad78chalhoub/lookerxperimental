view: looker_usage {
  sql_table_name: `chb-prod-stage-oracle.prod_onboarded_users.looker_usage`
    ;;

  dimension: history_approximate_web_usage_in_minutes {
    type: number
    sql: ${TABLE}.History_Approximate_Web_Usage_in_Minutes ;;
  }

  dimension: history_average_runtime_in_seconds {
    type: number
    sql: ${TABLE}.History_Average_Runtime_in_Seconds ;;
  }

  dimension: history_dashboard_run_count {
    type: number
    sql: ${TABLE}.History_Dashboard_Run_Count ;;
  }

  measure: total_run_count {
    type: sum
    sql: ${history_dashboard_run_count} ;;
  }
measure: average_run_count {
  type: average
  value_format: "0.00"
 sql: ${history_dashboard_run_count};;
}

  dimension: history_dashboard_user {
    type: number
    sql: ${TABLE}.History_Dashboard_User ;;
  }

  dimension: history_explore_user {
    type: number
    sql: ${TABLE}.History_Explore_User ;;
  }

  dimension_group: history_most_recent_query {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.History_Most_Recent_Query_Date ;;
  }

  dimension: history_query_run_count {
    type: number
    sql: ${TABLE}.History_Query_Run_Count ;;
  }

  dimension: history_total_runtime_in_seconds {
    type: number
    sql: ${TABLE}.History_Total_Runtime_in_Seconds ;;
  }

  dimension: history_users_30_days {
    type: number
    sql: ${TABLE}.History_Users_30_Days ;;
  }

  dimension: history_users_7_days {
    type: number
    sql: ${TABLE}.History_Users_7_Days ;;
  }

  dimension_group: user_created {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.User_Created_Date ;;
  }

  dimension: user_email {
    type: string
    sql: ${TABLE}.User_Email ;;
  }

  dimension: user_email_lower {
    type: string
    sql: ${TABLE}.user_email_lower ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
