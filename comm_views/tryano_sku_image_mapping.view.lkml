view: tryano_sku_image_mapping {
  sql_table_name: `chb-prod-data-comm.prod_commercial.tryano_sku_image_mapping`
    ;;

  dimension: sku {
    primary_key: yes
    type: string
    sql: ${TABLE}.sku ;;
  }

  dimension: image_1 {
    type: string
    sql: ${TABLE}.image_1 ;;
    html: <img src="{{image_1}}" /> ;;
  }

  dimension: image_2 {
    type: string
    sql: ${TABLE}.image_2 ;;
    html: <img src="{{image_2}}" /> ;;
  }

  dimension: image_3 {
    type: string
    sql: ${TABLE}.image_3 ;;
    html: <img src="{{image_3}}" /> ;;
  }

  dimension: image_4 {
    type: string
    sql: ${TABLE}.image_4 ;;
    html: <img src="{{image_4}}" /> ;;
  }

  dimension: image_5 {
    type: string
    sql: ${TABLE}.image_5 ;;
    html: <img src="{{image_5}}" /> ;;
  }

  dimension: image_6 {
    type: string
    sql: ${TABLE}.image_6 ;;
    html: <img src="{{image_6}}" /> ;;
  }

  dimension: image_7 {
    type: string
    sql: ${TABLE}.image_7 ;;
    html: <img src="{{image_7}}" /> ;;
  }

  dimension: image_8 {
    type: string
    sql: ${TABLE}.image_8 ;;
    html: <img src="{{image_8}}" /> ;;
  }

  dimension: image_9 {
    type: string
    sql: ${TABLE}.image_9 ;;
    html: <img src="{{image_9}}" /> ;;
  }

  dimension: all_images {
    type: string
    sql: ${TABLE}.image_1 ;;
    html: <img src="{{image_1}}" /> <img src="{{image_2}}" /> <img src="{{image_3}}" /> <img src="{{image_4}}" /> <img src="{{image_5}}" /> <img src="{{image_6}}" /> <img src="{{image_7}}" /> <img src="{{image_8}}" /> <img src="{{image_9}}" /> ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
