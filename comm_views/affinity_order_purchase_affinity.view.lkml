include: "../_common/period_over_period.view"

view: affinity_order_purchase_affinity {
  sql_table_name: `chb-prod-data-comm.prod_commercial.affinity_order_purchase_affinity`  ;;

  extends: [period_over_period]

  dimension_group: pop_no_tz {
    sql: ${common_raw} ;;
  }

  dimension: primary_key {
    type: number
    primary_key: yes
    hidden: yes
    sql: concat(${TABLE}.common_date,${TABLE}.brand,${TABLE}.product_a,${TABLE}.product_b) ;;
  }

  dimension_group: common {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    label: "Date"
    sql: ${TABLE}.common_date ;;
  }

  # brand fix
  dimension:  brand {
    type: string
    sql: ${TABLE}.brand ;;
  }

  dimension:  product_a {
    type: string
    sql: ${TABLE}.product_a ;;
  }

  dimension: product_name_A {
    sql: ${total_order_product_A.product_name} ;;
  }

  dimension: order_item_id {
    sql: ${TABLE}.order_item_id ;;
  }

  dimension:  product_b {
    type: string
    sql: ${TABLE}.product_b ;;
  }

  dimension: product_name_B {
    sql: ${total_order_product_B.product_name} ;;
  }

  dimension: joint_order_count {
    description: "How many times item A and B were purchased in the same order"
    type: number
    hidden: yes
    sql: ${TABLE}.joint_order_count ;;
    value_format: "#"
  }


  dimension: product_a_order_count {
    description: "Total number of orders with product A in them, during specified timeframe"
    type: number
    hidden: yes
    sql: ${TABLE}.product_a_order_count ;;
    value_format: "#"
  }

  dimension: product_b_order_count {
    description: "Total number of orders with product B in them, during specified timeframe"
    type: number
    hidden: yes
    sql: ${TABLE}.product_b_order_count ;;
    value_format: "#"
  }



  # Measure from Other views
  # Measure --------------------------------------------------------------

  measure: m_basket_sales_A {
    label: "Basket Sales A"
    type: sum_distinct
    sql_distinct_key: concat(${brand},${product_a},${common_date}) ;;
    sql: ${basket_metrics_A.basket_sales} ;;
  }

  measure:  m_basket_margin_A {
    label: "Basket Margin A"
    type: sum_distinct
    sql_distinct_key: concat(${brand},${product_a},${common_date}) ;;
    sql: ${basket_metrics_A.basket_margin} ;;
  }

  measure: m_basket_sales_B {
    label: "Basket Sales B"
    type: sum_distinct
    sql_distinct_key: concat(${brand},${product_b},${common_date}) ;;
    sql: ${basket_metrics_B.basket_sales} ;;
  }

  measure:  m_basket_margin_B {
    label: "Basket Margin B"
    type: sum_distinct
    sql_distinct_key: concat(${brand},${product_b},${common_date}) ;;
    sql: ${basket_metrics_B.basket_margin} ;;
  }

  measure: m_count {
    type: sum_distinct
    sql_distinct_key: concat(${brand},${product_a},${common_date}) ;;
    sql: ${total_orders.count} ;;
    label: "Total Order Count"
  }

  # Measure --------------------------------------------------------------



  # Measures --------------------------------------------------

  measure: m_joint_order_count {
    label: "Joint Order Count"
    description: "How many times item A and B were purchased in the same order"
    type: sum_distinct
    sql: ${TABLE}.joint_order_count ;;
    value_format: "#"
  }


  measure: m_product_a_order_count {
    label: "Product A Order Count"
    description: "Total number of orders with product A in them, during specified timeframe"
    type: sum_distinct
    sql_distinct_key: concat(${brand},${product_a},${common_date}) ;;
    sql: ${total_order_product_A.product_order_count} ;;
    value_format: "#"
  }

  measure: m_product_b_order_count {
    label: "Product B Order Count"
    description: "Total number of orders with product B in them, during specified timeframe"
    type: sum_distinct
    sql_distinct_key: concat(${brand},${product_b},${common_date}) ;;
    sql: ${total_order_product_B.product_order_count} ;;
    value_format: "#"
  }

  # Measures --------------------------------------------------





  #  Frequencies
  dimension: product_a_order_frequency {
    description: "How frequently orders include product A as a percent of total orders"
    type: number
    hidden: yes
    sql: 1.0*${total_order_product_A.product_order_count}/${total_orders.count} ;;
    value_format: "0.0000%"
  }

  dimension: product_b_order_frequency {
    description: "How frequently orders include product B as a percent of total orders"
    type: number
    hidden: yes
    sql: 1.0*${total_order_product_B.product_order_count}/${total_orders.count} ;;
    value_format: "0.0000%"
  }

  dimension: joint_order_frequency {
    description: "How frequently orders include both product A and B as a percent of total orders"
    type: number
    hidden: yes
    sql: 1.0*${joint_order_count}/${total_orders.count} ;;
    value_format: "0.0000%"
  }




  # Measures --------------------------------------------------

  measure: m_product_a_order_frequency {
    label: "Product A Order Frequency"
    description: "How frequently orders include product A as a percent of total orders"
    type: number
    sql: 1.0*${total_order_product_A.m_product_order_count}/NULLIF(${m_count},0) ;;
    value_format: "0.0000%"
  }

  measure: m_product_b_order_frequency {
    label: "Product B Order Frequency"
    description: "How frequently orders include product B as a percent of total orders"
    type: number
    sql: 1.0*${total_order_product_B.m_product_order_count}/NULLIF(${m_count},0) ;;
    value_format: "0.0000%"
  }

  measure: m_joint_order_frequency {
    label: "Joint Order Frequency"
    description: "How frequently orders include both product A and B as a percent of total orders"
    type: number
    sql: 1.0*${m_joint_order_count}/NULLIF(${m_count},0) ;;
    value_format: "0.0000%"
  }

  # Measures --------------------------------------------------




  # Affinity Metrics
  dimension: add_on_frequency {
    description: "How many times both Products are purchased when Product A is purchased"
    type: number
    hidden: yes
    sql: 1.0*${joint_order_count}/${total_order_product_A.product_order_count} ;;
    value_format: "0.00%"
  }

  dimension: lift {
    description: "The likelihood that buying product A drove the purchase of product B"
    type: number
    hidden: yes
    value_format_name: decimal_3
    sql: 1*${joint_order_frequency}/NULLIF((${product_a_order_frequency} * ${product_b_order_frequency}),0) ;;
  }


  dimension: product_a_count_purchased_alone  {
    type: number
    hidden: yes
    sql: ${total_order_product_A.product_count_purchased_alone} ;;
  }

  dimension: product_a_percent_purchased_alone {
    description: "The % of times product A is purchased alone, over all transactions containing product A"
    type: number
    hidden: yes
    sql: 1.0*${product_a_count_purchased_alone}/(CASE WHEN ${product_a_order_count}=0 THEN NULL ELSE ${product_a_order_count} END);;
    value_format_name: percent_1
  }

  dimension: product_b_count_purchased_alone  {
    type: number
    hidden: yes
    sql: ${total_order_product_B.product_count_purchased_alone} ;;
  }

  dimension: product_b_percent_purchased_alone {
    description: "The % of times product B is purchased alone, over all transactions containing product B"
    type: number
    hidden: yes
    sql: 1.0*${product_b_count_purchased_alone}/(CASE WHEN ${product_b_order_count}=0 THEN NULL ELSE ${product_b_order_count} END);;
    value_format_name: percent_1
  }


  # Measures --------------------------------------------------

  measure: m_add_on_frequency {
    label: "Add on Frequency"
    description: "How many times both Products are purchased when Product A is purchased"
    type: number
    sql: 1.0*${m_joint_order_count}/NULLIF(${total_order_product_A.m_product_order_count},0) ;;
    value_format: "0.00%"
  }

  measure: m_lift {
    label: "Lift"
    description: "The likelihood that buying product A drove the purchase of product B"
    type: number
    value_format_name: decimal_3
    sql: 1*${m_joint_order_frequency}/NULLIF((${m_product_a_order_frequency} * ${m_product_b_order_frequency}),0) ;;
  }


  measure: m_product_a_count_purchased_alone  {
    label: "Product A Count Purchased Alone"
    type: number
    hidden: yes
    sql: ${total_order_product_A.m_product_count_purchased_alone} ;;
  }

  measure: m_product_a_percent_purchased_alone {
    label: "Product A Percent Purchased Alone"
    description: "The % of times product A is purchased alone, over all transactions containing product A"
    type: number
    sql: 1.0*${m_product_a_count_purchased_alone}/(CASE WHEN ${m_product_a_order_count}=0 THEN NULL ELSE ${m_product_a_order_count} END);;
    value_format_name: percent_1
  }

  measure: m_product_b_count_purchased_alone  {
    label: "Product A Count Purchased Alone"
    type: number
    hidden: yes
    sql: ${total_order_product_B.m_product_count_purchased_alone} ;;
  }

  measure: m_product_b_percent_purchased_alone {
    label: "Product B Percent Purchased Alone"
    description: "The % of times product B is purchased alone, over all transactions containing product B"
    type: number
    sql: 1.0*${m_product_b_count_purchased_alone}/(CASE WHEN ${m_product_b_order_count}=0 THEN NULL ELSE ${m_product_b_order_count} END);;
    value_format_name: percent_1
  }

  # Measures --------------------------------------------------



  # Sales Metrics - Totals

  dimension: product_a_total_sales {
    view_label: "Sales and Margin - Total"
    group_label: "Product A - Sales"
    type: number
    hidden: yes
    sql:  ${total_order_product_A.product_sales} ;;
    value_format: "0.00"
  }

  dimension: product_a_total_basket_sales {
    view_label: "Sales and Margin - Total"
    group_label: "Product A - Sales"
    type: number
    hidden: yes
    sql: ${basket_metrics_A.basket_sales} ;;
    value_format: "0.00"
  }

  dimension: product_a_total_rest_of_basket_sales {
    view_label: "Sales and Margin - Total"
    group_label: "Product A - Sales"
    type: number
    hidden: yes
    sql: ${product_a_total_basket_sales}-IFNULL(${product_a_total_sales},0) ;;
    value_format: "0.00"
  }

  dimension: product_b_total_sales {
    view_label: "Sales and Margin - Total"
    group_label: "Product B - Sales"
    type: number
    hidden: yes
    sql:  ${total_order_product_B.product_sales} ;;
    value_format: "0.00"
  }

  dimension: product_b_total_basket_sales {
    view_label: "Sales and Margin - Total"
    group_label: "Product B - Sales"
    type: number
    hidden: yes
    sql: ${basket_metrics_B.basket_sales} ;;
    value_format: "0.00"
  }

  dimension: product_b_total_rest_of_basket_sales {
    view_label: "Sales and Margin - Total"
    group_label: "Product B - Sales"
    type: number
    hidden: yes
    sql: ${product_b_total_basket_sales}-IFNULL(${product_b_total_sales},0) ;;
    value_format: "0.00"
  }




  # Measures --------------------------------------------------

  measure: m_product_a_total_sales {
    view_label: "Sales and Margin - Total"
    group_label: "Product A - Sales"
    type: number
    sql:  ${total_order_product_A.m_product_sales} ;;
    value_format: "0.00"
  }

  measure: m_product_a_total_basket_sales {
    view_label: "Sales and Margin - Total"
    group_label: "Product A - Sales"
    type: number
    sql: ${m_basket_sales_A} ;;
    value_format: "0.00"
  }

  measure: m_product_a_total_rest_of_basket_sales {
    view_label: "Sales and Margin - Total"
    group_label: "Product A - Sales"
    type: number
    sql: ${m_product_a_total_basket_sales}-IFNULL(${m_product_a_total_sales},0) ;;
    value_format: "0.00"
  }

  measure: m_product_b_total_sales {
    view_label: "Sales and Margin - Total"
    group_label: "Product B - Sales"
    type: number
    sql:  ${total_order_product_B.m_product_sales} ;;
    value_format: "0.00"
  }

  measure: m_product_b_total_basket_sales {
    view_label: "Sales and Margin - Total"
    group_label: "Product B - Sales"
    type: number
    sql: ${m_basket_sales_B} ;;
    value_format: "0.00"
  }

  measure: m_product_b_total_rest_of_basket_sales {
    view_label: "Sales and Margin - Total"
    group_label: "Product B - Sales"
    type: number
    sql: ${m_product_b_total_basket_sales}-IFNULL(${m_product_b_total_sales},0) ;;
    value_format: "0.00"
  }

  # Measures --------------------------------------------------







  # Margin Metrics - Totals

  dimension: product_a_total_margin {
    view_label: "Sales and Margin - Total"
    group_label: "Product A - Margin"
    type: number
    hidden: yes
    sql: ${total_order_product_A.product_margin} ;;
    value_format: "0.00"
  }

  dimension: product_a_total_basket_margin {
    view_label: "Sales and Margin - Total"
    group_label: "Product A - Margin"
    type: number
    hidden: yes
    sql: ${basket_metrics_A.basket_margin} ;;
    value_format: "0.00"
  }

  dimension: product_a_total_rest_of_basket_margin {
    view_label: "Sales and Margin - Total"
    group_label: "Product A - Margin"
    type: number
    hidden: yes
    sql: ${product_a_total_basket_margin}-IFNULL(${product_a_total_margin},0) ;;
    value_format: "0.00"
  }

  dimension: product_b_total_margin {
    view_label: "Sales and Margin - Total"
    group_label: "Product B - Margin"
    type: number
    hidden: yes
    sql: ${total_order_product_B.product_margin} ;;
    value_format: "0.00"
  }

  dimension: product_b_total_basket_margin {
    view_label: "Sales and Margin - Total"
    group_label: "Product B - Margin"
    type: number
    hidden: yes
    sql: ${basket_metrics_B.basket_margin} ;;
    value_format: "0.00"
  }

  dimension: product_b_total_rest_of_basket_margin {
    view_label: "Sales and Margin - Total"
    group_label: "Product B - Margin"
    type: number
    hidden: yes
    sql: ${product_b_total_basket_margin}-IFNULL(${product_b_total_margin},0) ;;
    value_format: "0.00"
  }





  # Measures --------------------------------------------------

  measure: m_product_a_total_margin {
    view_label: "Sales and Margin - Total"
    group_label: "Product A - Margin"
    label: "Product margin - A"
    type: number
    sql: ${total_order_product_A.m_product_margin} ;;
    value_format: "0.00"
  }

  measure: m_product_a_total_basket_margin {
    view_label: "Sales and Margin - Total"
    group_label: "Product A - Margin"
    label: "Basket margin - A"
    type: number
    sql: ${m_basket_margin_A} ;;
    value_format: "0.00"
  }

  measure: m_product_a_total_rest_of_basket_margin {
    view_label: "Sales and Margin - Total"
    group_label: "Product A - Margin"
    label: "Rest of basket margin - A"
    type: number
    sql: ${m_product_a_total_basket_margin}-IFNULL(${m_product_a_total_margin},0) ;;
    value_format: "0.00"
  }

  measure: m_product_b_total_margin {
    view_label: "Sales and Margin - Total"
    group_label: "Product B - Margin"
    label: "Product margin - B"
    type: number
    sql: ${total_order_product_B.m_product_margin} ;;
    value_format: "0.00"
  }

  measure: m_product_b_total_basket_margin {
    view_label: "Sales and Margin - Total"
    group_label: "Product B - Margin"
    label: "Basket margin - B"
    type: number
    sql: ${m_basket_margin_B} ;;
    value_format: "0.00"
  }

  measure: m_product_b_total_rest_of_basket_margin {
    view_label: "Sales and Margin - Total"
    group_label: "Product B - Margin"
    label: "Rest of basket margin - B"
    type: number
    sql: ${m_product_b_total_basket_margin}-IFNULL(${m_product_b_total_margin},0) ;;
    value_format: "0.00"
  }

  # Measures --------------------------------------------------





  # Sales Metrics - Average

  dimension: product_a_average_sales {
    view_label: "Sales and Margin - Average"
    group_label: "Product A - Sales"
    type: number
    hidden: yes
    sql: 1.0*${product_a_total_sales}/${product_a_order_count} ;;
    value_format: "0.00"
  }

  dimension: product_a_average_basket_sales {
    view_label: "Sales and Margin - Average"
    group_label: "Product A - Sales"
    type: number
    hidden: yes
    sql: 1.0*${product_a_total_basket_sales}/${product_a_order_count} ;;
    value_format: "0.00"
  }

  dimension: product_a_average_rest_of_basket_sales {
    view_label: "Sales and Margin - Average"
    group_label: "Product A - Sales"
    type: number
    hidden: yes
    sql: 1.0*${product_a_total_rest_of_basket_sales}/${product_a_order_count} ;;
    value_format: "0.00"
  }

  dimension: product_b_average_sales {
    view_label: "Sales and Margin - Average"
    group_label: "Product B - Sales"
    type: number
    hidden: yes
    sql: 1.0*${product_b_total_sales}/${product_b_order_count} ;;
    value_format: "0.00"
  }

  dimension: product_b_average_basket_sales {
    view_label: "Sales and Margin - Average"
    group_label: "Product B - Sales"
    type: number
    hidden: yes
    sql: 1.0*${product_b_total_basket_sales}/${product_b_order_count} ;;
    value_format: "0.00"
  }

  dimension: product_b_average_rest_of_basket_sales {
    view_label: "Sales and Margin - Average"
    group_label: "Product B - Sales"
    type: number
    hidden: yes
    sql: 1.0*${product_b_total_rest_of_basket_sales}/${product_b_order_count} ;;
    value_format: "0.00"
  }



  # Measures --------------------------------------------------

  measure: m_product_a_average_sales {
    view_label: "Sales and Margin - Average"
    group_label: "Product A - Sales"
    type: number
    sql: 1.0*${m_product_a_total_sales}/NULLIF(${m_product_a_order_count},0) ;;
    value_format: "0.00"
  }

  measure: m_product_a_average_basket_sales {
    view_label: "Sales and Margin - Average"
    group_label: "Product A - Sales"
    type: number
    sql: 1.0*${m_product_a_total_basket_sales}/NULLIF(${m_product_a_order_count},0) ;;
    value_format: "0.00"
  }

  measure: m_product_a_average_rest_of_basket_sales {
    view_label: "Sales and Margin - Average"
    group_label: "Product A - Sales"
    type: number
    sql: 1.0*${m_product_a_total_rest_of_basket_sales}/NULLIF(${m_product_a_order_count},0) ;;
    value_format: "0.00"
  }

  measure: m_product_b_average_sales {
    view_label: "Sales and Margin - Average"
    group_label: "Product B - Sales"
    type: number
    sql: 1.0*${m_product_b_total_sales}/NULLIF(${m_product_b_order_count},0) ;;
    value_format: "0.00"
  }

  measure: m_product_b_average_basket_sales {
    view_label: "Sales and Margin - Average"
    group_label: "Product B - Sales"
    type: number
    sql: 1.0*${m_product_b_total_basket_sales}/NULLIF(${m_product_b_order_count},0) ;;
    value_format: "0.00"
  }

  measure: m_product_b_average_rest_of_basket_sales {
    view_label: "Sales and Margin - Average"
    group_label: "Product B - Sales"
    type: number
    sql: 1.0*${m_product_b_total_rest_of_basket_sales}/NULLIF(${m_product_b_order_count},0) ;;
    value_format: "0.00"
  }

  # Measures --------------------------------------------------








  # Margin Metrics - Average

  dimension: product_a_average_margin {
    view_label: "Sales and Margin - Average"
    group_label: "Product A - Margin"
    type: number
    hidden: yes
    sql: 1.0*${product_a_total_margin}/${product_a_order_count} ;;
    value_format: "0.00"
  }

  dimension: product_a_average_basket_margin {
    view_label: "Sales and Margin - Average"
    group_label: "Product A - Margin"
    type: number
    hidden: yes
    sql: 1.0*${product_a_total_basket_margin}/${product_a_order_count} ;;
    value_format: "0.00"
    drill_fields: [product_a, product_a_percent_purchased_alone]
  }

  dimension: product_a_average_rest_of_basket_margin {
    view_label: "Sales and Margin - Average"
    group_label: "Product A - Margin"
    type: number
    hidden: yes
    sql: 1.0*${product_a_total_rest_of_basket_margin}/${product_a_order_count} ;;
    value_format: "0.00"
    drill_fields: [product_a, product_a_percent_purchased_alone]
  }

  dimension: product_b_average_margin {
    view_label: "Sales and Margin - Average"
    group_label: "Product B - Margin"
    type: number
    hidden: yes
    sql: 1.0*${product_b_total_margin}/${product_b_order_count} ;;
    value_format: "0.00"
  }

  dimension: product_b_average_basket_margin {
    view_label: "Sales and Margin - Average"
    group_label: "Product B - Margin"
    type: number
    hidden: yes
    sql: 1.0*${product_b_total_basket_margin}/${product_b_order_count} ;;
    value_format: "0.00"
    drill_fields: [product_b, product_b_percent_purchased_alone]
  }

  dimension: product_b_average_rest_of_basket_margin {
    view_label: "Sales and Margin - Average"
    group_label: "Product B - Margin"
    type: number
    hidden: yes
    sql: 1.0*${product_b_total_rest_of_basket_margin}/${product_b_order_count} ;;
    value_format: "0.00"
    drill_fields: [product_b, product_b_percent_purchased_alone]
  }


  # Measures --------------------------------------------------

  measure: m_product_a_average_margin {
    view_label: "Sales and Margin - Average"
    group_label: "Product A - Margin"
    label: "Product margin - A"
    type: number
    sql: 1.0*${m_product_a_total_margin}/NULLIF(${total_order_product_A.m_product_order_count},0) ;;
    value_format: "0.00"
  }

  measure: m_product_a_average_basket_margin {
    view_label: "Sales and Margin - Average"
    group_label: "Product A - Margin"
    label: "Average basket margin - A"
    type: number
    sql: 1.0*${m_product_a_total_basket_margin}/NULLIF(${total_order_product_A.m_product_order_count},0) ;;
    value_format: "0.00"
    drill_fields: [product_a, product_a_percent_purchased_alone]
  }

  measure: m_product_a_average_rest_of_basket_margin {
    view_label: "Sales and Margin - Average"
    group_label: "Product A - Margin"
    label: "Average Rest of basket margin - A"
    type: number
    sql: 1.0*${m_product_a_total_rest_of_basket_margin}/NULLIF(${total_order_product_A.m_product_order_count},0) ;;
    value_format: "0.00"
    drill_fields: [product_a, product_a_percent_purchased_alone]
  }

  measure: m_product_b_average_margin {
    view_label: "Sales and Margin - Average"
    group_label: "Product B - Margin"
    label: "Product margin - B"
    type: number
    sql: 1.0*${m_product_b_total_margin}/NULLIF(${total_order_product_B.m_product_order_count},0) ;;
    value_format: "0.00"
  }

  measure: m_product_b_average_basket_margin {
    view_label: "Sales and Margin - Average"
    group_label: "Product B - Margin"
    label: "Average basket margin - B"
    type: number
    sql: 1.0*${m_product_b_total_basket_margin}/NULLIF(${total_order_product_B.m_product_order_count},0) ;;
    value_format: "0.00"
    drill_fields: [product_b, product_b_percent_purchased_alone]
  }

  measure: m_product_b_average_rest_of_basket_margin {
    view_label: "Sales and Margin - Average"
    group_label: "Product B - Margin"
    label: "Average Rest of basket margin - B"
    type: number
    sql:  1.0*${m_product_b_total_rest_of_basket_margin}/NULLIF(${total_order_product_B.m_product_order_count},0) ;;
    value_format: "0.00"
    drill_fields: [product_b, product_b_percent_purchased_alone]
  }

  # Measures --------------------------------------------------







  # Aggregate Measures - ONLY TO BE USED WHEN FILTERING ON AN AGGREGATE DIMENSION (E.G. BRAND_A, CATEGORY_A)

  measure: aggregated_joint_order_count {
    description: "Only use when filtering on a rollup of product items, such as brand_a or category_a"
    type: sum_distinct
    sql: ${joint_order_count} ;;
  }

}
