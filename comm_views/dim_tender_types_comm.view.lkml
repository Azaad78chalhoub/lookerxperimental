include: "../_common/dim_tender_types.view"
view: dim_tender_types_comm {
  extends: [dim_tender_types]


  view_label: "{% if _explore._name == 'faces' %}Faces
        {% elsif _explore._name == 'toryburch_facts_union' %}Tory Burch
        {% elsif _explore._name == 'lacoste_facts_union' %}Lacoste
        {% elsif _explore._name == 'loccitane_facts_union' %}Loccitane
        {% elsif _explore._name == 'swarovski' %}Swarovski
        {% endif %}"


  dimension: tender_type_desc {
    group_label: "SFCC Categories"
    label: "Payment Method Desc"
    type: string
    sql: ${TABLE}.tender_type_desc ;;
  }

  dimension: tender_type_group {
    group_label: "SFCC Categories"
    label: "Payment Method Group"
    type: string
    sql: ${TABLE}.tender_type_group ;;
  }

}
