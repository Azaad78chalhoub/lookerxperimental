view: level_shoes_daily_targets {

  # sql_table_name: `chb-prod-data-comm.prod_commercial.level_shoes_daily_targets`;;

  derived_table: {
    persist_for: "168 hours"
    sql:
      SELECT
          MIN(primary_key) AS primary_key,
          date,
          zone,
          COALESCE(SUM(orders), 0) AS orders,
          COALESCE(SUM(sessions), 0) AS sessions,
          AVG(aov_usd) AS aov_usd,
          AVG(cancellation_rate) AS cancellation_rate,
          AVG(conversion_rate) AS conversion_rate,
          COALESCE(SUM(gross_revenue_usd), 0) AS gross_revenue_usd,
          COALESCE(SUM(net_sales), 0) AS net_sales,
          AVG(return_rate) AS return_rate,
          COALESCE(SUM(units), 0) AS units,
          AVG(upt) AS upt,
          AVG(usp_usd) AS usp_usd
      FROM `chb-prod-data-comm.prod_commercial.level_shoes_daily_targets` AS level_shoes_daily_targets
      GROUP BY
          date,
          zone
      ;;

    partition_keys: [ "date" ]
    cluster_keys: [ "zone" ]
    indexes: [ "date", "zone" ]
  }

  #######################################
  #####         DIMENSIONS          #####
  #######################################

  dimension: primary_key {
    primary_key: yes
    hidden:  yes
    type: number
    # sql: CONCAT(${budget_date}, ${country}, ${zone})  ;;
    sql: ${TABLE}.primary_key  ;;
  }

  dimension_group: budget {
    label: "Budget"
    type: time
    description: "Budget Date"
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.date ;;
  }

  # dimension: country {
  #   label: "Country"
  #   # group_label: ""
  #   # description: "Budget Country"
  #   type: string
  #   map_layer_name: countries
  #   sql: ${TABLE}.country ;;
  # }

  dimension: zone {
    label: "Zone"
    description: "Zone Code"
    type: string
    sql: ${TABLE}.zone ;;
  }

  #######################################
  #####          MEASURES           #####
  #######################################

  measure: sessions {
    label: "# of Sessions"
    description: "Target for total number of user sessions"
    value_format_name: decimal_0
    type: sum
    sql: ${TABLE}.sessions ;;
  }

  measure: conversion_rate {
    label: "Conversion Rate (%)"
    description: "Target for % of sessions that made an order"
    value_format_name: percent_2
    type: average
    sql: ${TABLE}.conversion_rate ;;
  }

  measure: orders {
    label: "# of Orders"
    description: "Target for Total number of sales orders placed"
    value_format_name: decimal_0
    type: sum
    sql: ${TABLE}.orders ;;
  }

  measure: units {
    label: "Units ordered"
    description: "Target for Total number of units ordered"
    value_format_name: decimal_0
    type: sum
    sql: ${TABLE}.units ;;
  }

  measure: aov_usd {
    label: "Average Order Value (USD)"
    description: "Target for Average Value of each Order, based on Gross Sales (USD)"
    value_format_name: usd
    type: average
    sql: ${TABLE}.aov_usd ;;
  }

  measure: aov_aed {
    label: "Average Order Value (AED)"
    description: "Target for Average Value of each Order, based on Gross Sales (AED)"
    value_format_name: decimal_2
    type: average
    sql: ${TABLE}.aov_usd / 0.272257 ;;
  }

  measure: upt {
    label: "UPT"
    description: "Target for Average number of units within each order"
    value_format_name: decimal_2
    type: average
    sql: ${TABLE}.upt ;;
  }

  measure: usp_usd {
    label: "USP (USD)"
    description: "Target for Unit Selling Price - Average value of each item ordered (USD)"
    value_format_name: usd
    type: average
    sql: ${TABLE}.usp_usd ;;
  }

  measure: usp_aed {
    label: "USP (AED)"
    description: "Target for Unit Selling Price - Average value of each item ordered (AED)"
    value_format_name: decimal_2
    type: average
    sql: ${TABLE}.usp_usd / 0.272257 ;;
  }

  measure: gross_revenue_usd {
    label: "Gross Revenue (USD)"
    description: "Target for Total value of incoming orders in USD (excluding VAT, discount, customs and shipping fees and COD fees)"
    value_format_name: usd
    type: sum
    sql: ${TABLE}.gross_revenue_usd ;;
  }

  measure: gross_revenue_aed {
    label: "Gross Revenue (AED)"
    description: "Target for Total value of incoming orders in AED (excluding VAT, discount, customs and shipping fees and COD fees)"
    value_format_name: decimal_2
    type: sum
    sql: ${TABLE}.gross_revenue_usd / 0.272257 ;;
  }

  measure: cancellation_rate {
    label: "Cancellation Rate (% value)"
    description: "Target for % of incoming order value that was canceled"
    value_format_name: percent_0
    type: average
    sql: ${TABLE}.cancellation_rate ;;
  }

  measure: return_rate {
    label: "Returned Rate (% value)"
    description: "Target for % of packed orders value that was returned"
    value_format_name: percent_0
    type: average
    sql: ${TABLE}.return_rate ;;
  }

  measure: net_sales_usd {
    label: "Net Sales (USD)"
    description: "Target for Total value of incoming orders in USD excluding returns (excluding failed credit card attempts, VAT, customs and shipping fees and COD fees)."
    value_format_name: usd
    type: sum
    sql: ${TABLE}.net_sales ;;
  }

  measure: net_sales_aed {
    label: "Net Sales (AED)"
    description: "Target for Total value of incoming orders in AED excluding returns (excluding failed credit card attempts, VAT, customs and shipping fees and COD fees)."
    value_format_name: decimal_2
    type: sum
    sql: ${TABLE}.net_sales / 0.272257 ;;
  }

  measure: count {
    type: count
    hidden: yes
    drill_fields: []
  }
}
