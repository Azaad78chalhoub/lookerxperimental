view: namshi_stock {
  sql_table_name: `chb-prod-data-comm.prod_commercial.namshi_stock`
    ;;
  label: "Inventory"
  dimension: pk {
    primary_key: yes
    hidden: yes
    type: string
    sql: CONCAT(${brand},${date_date},${sku},${warehouse}) ;;
  }
  dimension: latest_update {
    type: number
    hidden: yes
    sql: ${TABLE}.latest_update ;;
  }
  dimension: latest_update_boolean {
    hidden: yes
    group_label: "Statuses"
    description: "When filtered by yes; this returns the latest status a tracking # received"
    label: "Filter by latest status"
    type: yesno
    sql: ${latest_update}=1 ;;
  }

  dimension_group: activation {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.activation_date ;;
  }

  dimension: average_selling_price {
    type: number
    hidden: yes
    sql: ${TABLE}.average_selling_price ;;
  }
  measure: selling_price_avg {
    label: "Average Selling Price"
    type: sum
    sql: ${average_selling_price} ;;
    filters: [latest_update_boolean: "Yes"]
  }

  dimension: brand {
    type: string
    sql: ${TABLE}.brand ;;
  }

  dimension: category {
    type: string
    sql: ${TABLE}.category ;;
  }

  dimension: color {
    type: string
    sql: ${TABLE}.color ;;
  }

  dimension: current_price_ae {
    type: number
    hidden: yes
    sql: ${TABLE}.current_price_ae ;;
  }
  measure: ae_price {
    label: "Current Price (AE)"
    type: sum
    sql: ${current_price_ae} ;;
    filters: [latest_update_boolean: "Yes"]
  }

  dimension: current_price_sa {
    type: number
    hidden: yes
    sql: ${TABLE}.current_price_sa ;;
  }
  measure: sa_price {
    label: "Current Price (SA)"
    type: sum
    sql: ${current_price_sa} ;;
    filters: [latest_update_boolean: "Yes"]
  }

  dimension_group: date {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.date ;;
  }

  dimension: days_of_inventory {
    type: number
    hidden: yes
    sql: ${TABLE}.days_of_inventory ;;
  }
  measure: inventory_days {
    label: "Days of Inventory"
    type: sum
    sql: ${days_of_inventory} ;;
    filters: [latest_update_boolean: "Yes"]
  }

  dimension: department {
    type: string
    sql: ${TABLE}.department ;;
  }

  dimension: gender {
    type: string
    sql: ${TABLE}.gender ;;
  }

  dimension: inventory {
    type: number
    hidden: yes
    sql: ${TABLE}.inventory ;;
  }
  measure: inventory_count {
    label: "Inventory"
    type: sum
    sql: ${inventory} ;;
    filters: [latest_update_boolean: "Yes"]
  }

  dimension: online_inventory {
    type: number
    hidden: yes
    sql: ${TABLE}.online_inventory ;;
  }
  measure: inventory_online {
    label: "Online Inventory"
    type: sum
    sql: ${online_inventory} ;;
    filters: [latest_update_boolean: "Yes"]
  }

  dimension: original_price_ae {
    type: number
    hidden: yes
    sql: ${TABLE}.original_price_ae ;;
  }
  measure: ae_original_price {
    label: "Original Price (AE)"
    type: sum
    sql: ${original_price_ae} ;;
    filters: [latest_update_boolean: "Yes"]
  }

  dimension: original_price_sa {
    type: number
    hidden: yes
    sql: ${TABLE}.original_price_sa ;;
  }
  measure: sa_original_price {
    label: "Original Price (SA)"
    type: sum
    sql: ${original_price_sa} ;;
    filters: [latest_update_boolean: "Yes"]
  }

  dimension: product_name {
    type: string
    sql: ${TABLE}.product_name ;;
  }

  dimension: season {
    type: string
    sql: ${TABLE}.season ;;
  }

  dimension: size {
    type: string
    sql: ${TABLE}.size ;;
  }

  dimension: sku {
    type: string
    sql: ${TABLE}.sku ;;
  }

  dimension: subcategory {
    type: string
    sql: ${TABLE}.subcategory ;;
  }

  dimension: supplier_barcode {
    type: string
    sql: ${TABLE}.supplier_barcode ;;
  }

  dimension: supplier_color {
    type: string
    sql: ${TABLE}.supplier_color ;;
  }

  dimension: supplier_name {
    type: string
    sql: ${TABLE}.supplier_name ;;
  }

  dimension: supplier_sku {
    type: string
    sql: ${TABLE}.supplier_sku ;;
  }

  dimension: unit_sold_last_30_days {
    type: number
    hidden: yes
    sql: ${TABLE}.unit_sold_last_30_days ;;
  }
  measure: last_30_days_units {
    label: "Units Sold in Last 30 Days"
    type: sum
    sql: ${unit_sold_last_30_days} ;;
    filters: [latest_update_boolean: "Yes"]
  }

  dimension: warehouse {
    type: string
    sql: ${TABLE}.warehouse ;;
  }

  measure: count {
    type: count
    hidden: yes
    drill_fields: [supplier_name, product_name]
  }
}
