view: muse_adjust {
  sql_table_name: `chb-prod-data-comm.prod_commercial.muse_adjust`
    ;;

  dimension: adgroup_name {
    type: string
    sql: ${TABLE}.adgroup_name ;;
  }

  dimension: app_version {
    type: string
    sql: ${TABLE}.app_version ;;
  }

  dimension: campaign_name {
    type: string
    sql: ${TABLE}.campaign_name ;;
  }

  dimension: event_name {
    type: string
    sql: ${TABLE}.event_name ;;
  }

  dimension: fb_adgroup_name {
    type: string
    sql: ${TABLE}.fb_adgroup_name ;;
  }

  dimension: fb_campaign_name {
    type: string
    sql: ${TABLE}.fb_campaign_name ;;
  }

  dimension: gps_adid {
    type: string
    sql: ${TABLE}.gps_adid ;;
    primary_key: yes
  }

  dimension_group: installed {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.installed_at ;;
  }

  dimension: os_name {
    type: string
    sql: ${TABLE}.os_name ;;
  }

  dimension: publisher_parameters {
    type: string
    sql: ${TABLE}.publisher_parameters ;;
  }

  dimension: source {
    type: string
    sql: ${TABLE}.source ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      campaign_name,
      fb_campaign_name,
      fb_adgroup_name,
      event_name,
      os_name,
      adgroup_name
    ]
  }
}
