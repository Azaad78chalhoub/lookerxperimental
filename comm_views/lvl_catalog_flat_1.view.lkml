view: lvl_catalog_product_flat_1 {
  sql_table_name: prod_comm.stg_comm_lvl_catalog_product_flat_1 ;;
  label: "Product Catalog"
  dimension: _fivetran_deleted {
    type: yesno
    hidden: yes
    sql: ${TABLE}._fivetran_deleted ;;
  }

  ##################################
  #####         DATES          #####
  ##################################


  dimension_group: updated {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_at ;;
  }
  #######################################
  #####         DIMENSIONS          #####
  #######################################



  dimension: brand_name {
    type: number
    hidden: yes
    label: "Brand Name Code"
    sql: ${TABLE}.brand_name ;;
  }

  dimension: brand_name_value {
    type: string
    hidden: yes
    label: "Brand Name Value"
    sql: ${TABLE}.brand_name_value ;;
  }

  dimension: consignmentflag {
    type: number
    hidden: yes
    label: "Consignment Flag Code"
    sql: ${TABLE}.consignmentflag ;;
  }

  dimension: consignmentflag_value {
    type: string
    hidden: yes
    label: "Consignment Flag"
    sql: ${TABLE}.consignmentflag_value ;;
  }

  dimension: color_value {
    type: string
    label: "Color"
    sql: ${TABLE}.color_value ;;
  }

  dimension: color {
    type: number
    hidden: yes
    label: "Color Code"
    sql: ${TABLE}.color ;;
  }

  dimension: division_value {
    type: string
    label: "Division"
    sql: ${TABLE}.division_value ;;
  }

  dimension: division {
    type: number
    hidden: yes
    label: "Division Code"
    sql: ${TABLE}.division ;;
  }


  dimension: lvl_category_value {
    type: string
    label: "Category"
    sql: ${TABLE}.lvl_category_value ;;
    link: {
      label: "Drill by Brand"
      url: "https://chalhoubgroup.de.looker.com/looks/1950?&f[lvl_catalog_product_flat_1.lvl_category_value]={{ value | url_encode }}"
    }
  }

 dimension: gender_value {
    type: string
    label: "Gender"
    sql: ${TABLE}.gender_value ;;
  }


  dimension: gender {
    type: number
    hidden: yes
    label: "Gender Code"
    sql: ${TABLE}.gender ;;
  }

 dimension: primaryvpn {
    type: string
    label: "Primary VPN"
    sql: ${TABLE}.primaryvpn ;;
  }

  dimension: lvl_accessories_category {
    type: number
    hidden: yes
    sql: ${TABLE}.lvl_accessories_category ;;
  }

  dimension: lvl_accessories_category_value {
    type: string
    hidden: no
    label: "Accessories Category"
    sql: ${TABLE}.lvl_accessories_category_value ;;
  }

  dimension: lvl_category {
    type: number
    hidden: yes
    sql: ${TABLE}.lvl_category ;;
  }



  dimension: lvl_concession_core_sku {
    type: string
    hidden: no
    label: "Concession Core SKU"
    sql: ${TABLE}.lvl_concession_core_sku ;;
  }

  dimension: lvl_concession_type {
    type: number
    hidden: yes
    sql: ${TABLE}.lvl_concession_type ;;
  }

  dimension: lvl_concession_type_value {
    type: string
    hidden: no
    label: "Concession Type"
    sql: ${TABLE}.lvl_concession_type_value ;;
  }

  dimension: manufacturer {
    type: number
    hidden: yes
    label: "Manufacturer Code"
    sql: ${TABLE}.manufacturer ;;
  }

  dimension: manufacturer_value {
    type: string
    label: "Manufacturer"
    sql: ${TABLE}.manufacturer_value ;;
  }

  dimension: name {
    type: string
    label: "Name"
    sql: ${TABLE}.name ;;
  }


  dimension: size {
    type: number
    hidden: yes
    label: "Size Code"
    sql: ${TABLE}.size ;;
  }

  dimension: size_value {
    type: string
    label: "Size"
    hidden: no
    sql: ${TABLE}.size_value ;;
  }

  dimension: sku {
    type: string
    sql: ${TABLE}.sku ;;
  }

  dimension: sku_type {
    type: number
    hidden: yes
    sql: ${TABLE}.sku_type ;;
  }


  dimension: seasondesc_value {
    type: string
    label: "Season"
    sql: ${TABLE}.seasondesc_value ;;
  }

  dimension: seasondesc {
    type: number
    label: "Season"
    hidden: yes
    sql: ${TABLE}.seasondesc ;;
  }


  dimension: styleoraclenumber {
    type: string
    label: "Oracle Style Number"
    sql: ${TABLE}.styleoraclenumber ;;
  }
  dimension: subcategory_value {
    type: string
    label: "Sub Category"
    sql: ${TABLE}.subcategory_value ;;
  }

  dimension: zone_value {
    type: string
    label: "Zone"
    sql: ${TABLE}.zone_value ;;
  }

  ##############################################
  #####         UNUSED MEASURES            #####
  ##############################################

  measure: count {
    type: count
    hidden: yes
    drill_fields: [brand_name, name]
  }

  ##############################################
  #####         UNUSED DIMENSIONS          #####
  ##############################################

  dimension_group: _fivetran_synced {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}._fivetran_synced ;;
  }

  dimension: allow_open_amount {
    type: number
    hidden: yes
    sql: ${TABLE}.allow_open_amount ;;
  }

  dimension: attribute_set_id {
    type: number
    hidden: yes
    sql: ${TABLE}.attribute_set_id ;;
  }


  dimension: consignment {
    type: yesno
    sql: CASE WHEN ${consignmentflag_value} = "N" Then TRUE
          ELSE FALSE
          END;;
  }

  dimension: core_sku {
    type: string
    hidden: yes
    sql: ${TABLE}.core_sku ;;
  }

  dimension: cost {
    type: number
    hidden: yes
    sql: ${TABLE}.cost ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: description {
    type: string
    sql: ${TABLE}.description ;;
  }



  dimension: email_template {
    type: string
    hidden: yes
    sql: ${TABLE}.email_template ;;
  }

  dimension: entity_id {
    type: number
    hidden: yes
    primary_key: yes
    sql: ${TABLE}.entity_id ;;
  }




  dimension: gift_message_available {
    type: number
    hidden: yes
    sql: ${TABLE}.gift_message_available ;;
  }

  dimension: gift_wrapping_available {
    type: number
    hidden: yes
    sql: ${TABLE}.gift_wrapping_available ;;
  }

  dimension: gift_wrapping_price {
    type: number
    hidden: yes
    sql: ${TABLE}.gift_wrapping_price ;;
  }

  dimension: giftcard_amounts {
    type: number
    hidden: yes
    sql: ${TABLE}.giftcard_amounts ;;
  }

  dimension: giftcard_type {
    type: number
    hidden: yes
    sql: ${TABLE}.giftcard_type ;;
  }

  dimension: has_options {
    type: number
    hidden: yes
    sql: ${TABLE}.has_options ;;
  }

  dimension: hscode {
    type: string
    hidden: yes
    sql: ${TABLE}.hscode ;;
  }

  dimension: image {
    type: string
    hidden: yes
    sql: ${TABLE}.image ;;
  }

  dimension: image_label {
    type: string
    hidden: yes
    sql: ${TABLE}.image_label ;;
  }

  dimension: is_redeemable {
    type: number
    hidden: yes
    sql: ${TABLE}.is_redeemable ;;
  }

  dimension: lifetime {
    type: number
    hidden: yes
    sql: ${TABLE}.lifetime ;;
  }

  dimension: links_exist {
    type: number
    hidden: yes
    sql: ${TABLE}.links_exist ;;
  }

  dimension: links_purchased_separately {
    type: number
    hidden: yes
    sql: ${TABLE}.links_purchased_separately ;;
  }

  dimension: links_title {
    type: string
    hidden: yes
    sql: ${TABLE}.links_title ;;
  }





  dimension: msrp {
    type: number
    hidden: yes
    sql: ${TABLE}.msrp ;;
  }

  dimension: msrp_display_actual_price_type {
    type: string
    hidden: yes
    sql: ${TABLE}.msrp_display_actual_price_type ;;
  }


  dimension_group: news_from {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.news_from_date AS TIMESTAMP) ;;
  }

  dimension_group: news_to {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.news_to_date AS TIMESTAMP) ;;
  }

  dimension: offers {
    type: number
    hidden: yes
    sql: ${TABLE}.offers ;;
  }

  dimension: offers_value {
    type: string
    hidden: yes
    sql: ${TABLE}.offers_value ;;
  }

  dimension: open_amount_max {
    type: number
    hidden: yes
    sql: ${TABLE}.open_amount_max ;;
  }

  dimension: open_amount_min {
    type: number
    hidden: yes
    sql: ${TABLE}.open_amount_min ;;
  }

  dimension: price {
    type: number
    hidden: yes
    sql: ${TABLE}.price ;;
  }

  dimension: price_type {
    type: number
    hidden: yes
    sql: ${TABLE}.price_type ;;
  }

  dimension: price_view {
    type: number
    hidden: yes
    sql: ${TABLE}.price_view ;;
  }



  dimension: required_options {
    type: number
    hidden: yes
    sql: ${TABLE}.required_options ;;
  }

  dimension: row_id {
    type: number
    hidden: yes
    sql: ${TABLE}.row_id ;;
  }



  dimension: short_description {
    type: string
    sql: ${TABLE}.short_description ;;
  }

  dimension: small_image {
    type: string
    hidden: yes
    sql: ${TABLE}.small_image ;;
  }

  dimension: small_image_label {
    type: string
    hidden: yes
    sql: ${TABLE}.small_image_label ;;
  }

  dimension_group: special_from {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.special_from_date AS TIMESTAMP) ;;
  }

  dimension: special_price {
    type: number
    hidden: yes
    sql: ${TABLE}.special_price ;;
  }

  dimension_group: special_to {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.special_to_date AS TIMESTAMP) ;;
  }



  dimension: subcategory {
    type: number
    hidden: yes
    sql: ${TABLE}.subcategory ;;
  }



  dimension: swatch_image {
    type: string
    hidden: yes
    sql: ${TABLE}.swatch_image ;;
  }

  dimension: tax_class_id {
    type: number
    hidden: yes
    sql: ${TABLE}.tax_class_id ;;
  }

  dimension: taxonomy1 {
    type: string
    hidden: yes
    sql: ${TABLE}.taxonomy1 ;;
  }

  dimension: thumbnail {
    type: string
    hidden: yes
    sql: ${TABLE}.thumbnail ;;
  }

  dimension: thumbnail_label {
    type: string
    hidden: yes
    sql: ${TABLE}.thumbnail_label ;;
  }

  dimension: trend1 {
    type: number
    hidden: yes
    sql: ${TABLE}.trend1 ;;
  }

  dimension: trend1_value {
    type: string
    hidden: no
    label: "Trend1"
    sql: ${TABLE}.trend1_value ;;
  }

  dimension: type_id {
    type: string
    hidden: yes
    sql: ${TABLE}.type_id ;;
  }

  dimension: url_key {
    type: string
    hidden: yes
    sql: ${TABLE}.url_key ;;
  }

  dimension: url_path {
    type: string
    hidden: yes
    sql: ${TABLE}.url_path ;;
  }

  dimension: use_config_email_template {
    type: number
    hidden: yes
    sql: ${TABLE}.use_config_email_template ;;
  }

  dimension: use_config_is_redeemable {
    type: number
    hidden: yes
    sql: ${TABLE}.use_config_is_redeemable ;;
  }

  dimension: use_config_lifetime {
    type: number
    hidden: yes
    sql: ${TABLE}.use_config_lifetime ;;
  }

  dimension: video_ids {
    type: string
    hidden: yes
    sql: ${TABLE}.video_ids ;;
  }

  dimension: visibility {
    type: number
    hidden: yes
    sql: ${TABLE}.visibility ;;
  }

  dimension: voucher {
    type: number
    hidden: yes
    sql: ${TABLE}.voucher ;;
  }

  dimension: voucher_value {
    type: string
    hidden: yes
    sql: ${TABLE}.voucher_value ;;
  }

  dimension: weight {
    type: number
    hidden: yes
    sql: ${TABLE}.weight ;;
  }

  dimension: weight_type {
    type: number
    hidden: yes
    sql: ${TABLE}.weight_type ;;
  }

  dimension: zone {
    type: number
    hidden: yes
    sql: ${TABLE}.zone ;;
  }

}
