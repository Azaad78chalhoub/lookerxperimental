view: stg_comm_chanel_faces_ga_transactions {
  sql_table_name: `chb-prod-stage-comm.prod_comm.stg_comm_faces_ga_transactions`;;

  dimension: pk {
    type:  string
    primary_key:  yes
    sql:  concat(${transaction_id}) ;;
  }

  dimension_group: date {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.date ;;
  }

  dimension: brand_security{
    type: string
    hidden: yes
    sql: "FACES" ;;
  }

  dimension: vertical_security{
    type: string
    hidden: yes
    sql: "MANAGED COMPANIES" ;;
  }

  dimension: sessions {
    type: number
    description: "Number of sessions"
    sql: ${TABLE}.sessions ;;
  }

  dimension: campaign {
    type: string
    description: "Campaign"
    sql: ${TABLE}.campaign ;;
  }

  dimension: landing_page_path {
    type: string
    description: "Landing page path"
    sql: ${TABLE}.landing_page_path ;;
  }

  dimension: device_category {
    type: string
    description: "Device category"
    sql: ${TABLE}.device_category ;;
  }

  dimension: source_medium {
    type: string
    description: "Source / Medium"
    sql: ${TABLE}.source_medium ;;
  }

  dimension: transaction_id {
    type: string
    label: "Transaction ID"
    description: "Transaction ID / Order ID"
    sql: ${TABLE}.transaction_id ;;
  }

  measure: transactions {
    type: count_distinct
    label: "# of Transactions"
    description: "Number of transactions"
    sql: ${TABLE}.transaction_id ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }

}
