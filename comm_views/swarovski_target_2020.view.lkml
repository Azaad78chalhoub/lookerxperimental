view: swarovski_target_2020 {
  sql_table_name: `chb-prod-data-comm.prod_commercial.swarovski_target_2020`
    ;;

  dimension: aov_usd {
    type: number
    hidden:  yes
    sql: ${TABLE}.aov_usd ;;
  }

  dimension: budget {
    type: string
    hidden:  no
    sql: ${TABLE}.budget ;;
  }

  dimension: cancellation_rate {
    type: number
    hidden:  yes
    sql: ${TABLE}.cancellation_rate ;;
  }

  dimension: conversion_rate {
    type: number
    hidden:  yes
    sql: ${TABLE}.conversion_rate ;;
  }

  dimension: country_code {
    type: string
    sql: ${TABLE}.country_code ;;
  }

  dimension_group: date {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.date ;;
  }

  dimension: day {
    type: string
    sql: ${TABLE}.day ;;
  }

  dimension: net_order {
    type: number
    hidden:  yes
    sql: ${TABLE}.net_order ;;
  }

  dimension: orders_gross {
    type: number
    hidden:  yes
    sql: ${TABLE}.orders_gross ;;
  }

  dimension: sls_mix {
    type: number
    hidden:  yes
    sql: ${TABLE}.sls_mix ;;
  }

  dimension: return_rate {
    type: number
    hidden:  yes
    sql: ${TABLE}.return_rate ;;
  }

  dimension: revenue_gross_usd {
    type: number
    hidden:  yes
    sql: ${TABLE}.revenue_gross_usd ;;
  }

  dimension: revenue_net_usd {
    type: number
    hidden:  yes
    sql: ${TABLE}.revenue_net_usd ;;
  }

  dimension: sessions {
    type: number
    hidden:  yes
    sql: ${TABLE}.sessions ;;
  }

  dimension: country {
    type: string
    sql: CASE WHEN ${country_code} = "AE" then "United Arab Emirates"
              WHEN ${country_code} = "QAT" then "Qatar"
              WHEN ${country_code} = "SA" then "Saudi Arabia"
              WHEN ${country_code} = "EGY" then "Egypt"
              WHEN ${country_code} = "KWT" then "Kuwait"
              ELSE "Other"
              END;;
  }

  dimension: pk {
    primary_key: yes
    hidden: yes
    sql: concat(${country_code}, ${date_date}, ${day}) ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }

  measure: number_of_gross_orders {
    type: sum
    label: "# of orders"
    description: "Total number of orders placed on the website"
    sql: ${orders_gross} ;;
  }

  measure: number_of_net_orders {
    type: sum
    label: "# of orders w/o cancellations and returns"
    description: "Total number of orders placed on the website excluding cancellations and returns"
    sql: ${net_order} ;;
  }

  measure: sum_revenue_gross_usd {
    type: sum
    value_format_name: usd
    label: "Gross Revenue USD"
    description: "Total Revenue of orders placed on the website"
    sql: ${revenue_gross_usd} ;;
  }

  measure: sum_revenue_net_usd {
    type: sum
    value_format_name: usd
    label: "Net revenue USD"
    description: "Total revenue of orders placed on the website, excluding cancellations and returns"
    sql: ${revenue_net_usd};;
  }

  measure: aov {
    type: average
    value_format_name: usd
    label: "AOV USD"
    description: "Average order value in USD"
    sql: ${aov_usd};;
  }

  measure: rate_cancellation {
    type: average
    value_format_name: percent_2
    label: "Cancellation Rate %"
    description: "Cancellation rate based on Revenue"
    sql: ${cancellation_rate};;
  }

  measure: rate_return {
    type: average
    value_format_name: percent_2
    label: "Return Rate %"
    description: "Return rate based on Revenue"
    sql: ${return_rate};;
  }

  measure: conversion {
    type: average
    value_format_name: percent_2
    label: "Conversion Rate"
    description: "Conversion rate"
    sql: ${conversion_rate};;
  }

  measure: order_net {
    type: sum
    label: "Net Orders"
    sql: ${net_order};;
  }

  measure: gross_orders {
    type: sum
    label: "Gross Orders"
    sql: ${orders_gross} ;;
  }

  measure: number_of_sessions {
    type: sum
    label: "Sessions"
    sql: ${sessions} ;;
}

  measure: sls {
    type: average
    label: "Sls Mix"
    value_format_name: percent_2
    sql: ${sls_mix};;
  }

}
