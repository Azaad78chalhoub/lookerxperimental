view: faces_coupon_code_affiliates {
  sql_table_name: `chb-prod-data-comm.prod_commercial.faces_coupon_code_affiliates`
    ;;

  dimension: pk {
    type: string
    primary_key: yes
    hidden: yes
    sql: concat(${affiliate_name}, ${country},${coupon_code},${coupon_status},${type},${vendor}, ifnull(${creation_date},""), ifnull(${expiry_date},""),${duplicates}) ;;
  }

  dimension: affiliate_name {
    type: string
    sql: ${TABLE}.affiliate_name ;;
  }

  dimension: country {
    type: string
    map_layer_name: countries
    sql: ${TABLE}.Country ;;
  }

  dimension: coupon_code {
    type: string
    sql: ${TABLE}.coupon_code ;;
  }

  dimension: coupon_status {
    type: string
    sql: ${TABLE}.coupon_status ;;
  }

  dimension: creation_date {
    type: string
    sql: ${TABLE}.creation_date ;;
  }

  dimension: duplicates {
    type: number
    sql: ${TABLE}.Duplicates ;;
  }

  dimension: expiry_date {
    type: string
    sql: ${TABLE}.expiry_date ;;
  }

  dimension: net_sales_commission {
    type: number
    value_format_name: percent_0
    sql: ${TABLE}.net_sales_commission/100 ;;
  }

  dimension: percentage_discount {
    type: number
    sql: ${TABLE}.percentage_discount ;;
  }

  dimension: type {
    type: string
    sql: ${TABLE}.Type ;;
  }

  dimension: vendor {
    type: string
    sql: ${TABLE}.Vendor ;;
  }

  measure: count {
    type: count
    drill_fields: [affiliate_name]
  }
}
