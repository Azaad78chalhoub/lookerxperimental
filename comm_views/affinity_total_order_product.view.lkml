view: affinity_total_order_product {
  sql_table_name: `chb-prod-data-comm.prod_commercial.affinity_total_order_product`  ;;

  dimension: common_date {
    sql: ${TABLE}.common_date ;;
  }

  dimension: brand {
    sql: ${TABLE}.brand ;;
  }

  dimension: product_id {
    sql: ${TABLE}.product_id ;;
  }

  dimension: primary_key {
    type: number
    primary_key: yes
    hidden: yes
    sql: concat(${TABLE}.common_date,${TABLE}.brand,${TABLE}.product_id) ;;
  }

  dimension: product_name {
    sql: ${TABLE}.product_name ;;
  }

  dimension: product_count_purchased_alone {
    type: number
    hidden: yes
    sql: ${TABLE}.product_count_purchased_alone ;;
  }

  dimension: product_order_count {
    description: "Total number of orders with product in them, during specified timeframe"
    type: number
    hidden: yes
    sql: ${TABLE}.product_order_count ;;
    value_format: "#"
  }

  dimension: product_percent_purchased_alone {
    description: "The % of times product is purchased alone, over all transactions containing product"
    type: number
    hidden: yes
    sql: 1.0*${product_count_purchased_alone}/(CASE WHEN ${product_order_count}=0 THEN NULL ELSE ${product_order_count} END);;
    value_format_name: percent_1
  }

  dimension: product_sales {
    hidden: yes
    sql: ${TABLE}.product_sales ;;
  }

  dimension: product_margin {
    hidden: yes
    sql: ${TABLE}.product_margin ;;
  }



  #  Frequencies
  dimension: product_order_frequency {
    description: "How frequently orders include product as a percent of total orders"
    type: number
    hidden: yes
    sql: 1.0*${product_order_count}/${total_orders.count} ;;
    value_format: "0.0000%"
  }




  # Measure --------------------------------------------------------------

  measure: m_product_count_purchased_alone {
    type: sum
    hidden: yes
    sql: ${TABLE}.product_count_purchased_alone ;;
  }

  measure: m_product_order_count {
    description: "Total number of orders with product in them, during specified timeframe"
    type: sum_distinct
    sql: ${TABLE}.product_order_count ;;
    value_format: "#"
  }

  measure: m_product_percent_purchased_alone {
    description: "The % of times product is purchased alone, over all transactions containing product"
    type: number
    sql: 1.0*${m_product_count_purchased_alone}/(CASE WHEN ${m_product_order_count}=0 THEN NULL ELSE ${m_product_order_count} END);;
    value_format_name: percent_1
  }

  measure: m_product_sales {
    type: sum
    sql: ${TABLE}.product_sales ;;
  }

  measure: m_product_margin {
    type: sum
    sql: ${TABLE}.product_margin ;;
  }

  measure: m_product_order_frequency {
    description: "How frequently orders include product as a percent of total orders"
    type: number
    sql: 1.0*${product_order_count}/${total_orders.count} ;;
    value_format: "0.0000%"
  }

  # Measure --------------------------------------------------------------


}
