include: "../_common/period_over_period.view"

view: toryburch_acceptance_facts_union {
  sql_table_name: `chb-prod-data-comm.prod_commercial.toryburch_acceptance_facts_union`
    ;;

  extends: [period_over_period]
  dimension_group: pop_no_tz {
    sql: ${common_datetime_raw} ;;
  }

  dimension: primary_key {
    type: number
    hidden: yes
    primary_key: yes
    sql: ${TABLE}.primary_key ;;
  }

  dimension: brand_security{
    type: string
    hidden: yes
    sql: ${TABLE}.brand;;
  }

  dimension: vertical_security{
    type: string
    hidden: yes
    sql: ${TABLE}.vertical ;;
  }

  dimension: country_security{
    type: string
    hidden: yes
    sql: ${TABLE}.bu_country ;;
  }

  dimension: country {
    type: string
    sql: ${TABLE}.store_country ;;
  }

  dimension_group: common_datetime {
    label: "Date"
    type: time
    description: "Date when the record was recorded"
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.common_datetime ;;
  }


######################################
#####   sales order dimensions   #####
######################################{

  dimension: order_currency {
    group_label: "Sales Order"
    type: string
    description: "Currency used for the order"
    sql: ${TABLE}.order_currency ;;
  }

  dimension: order_custom_duty_fee {
    group_label: "Sales Order"
    label: "Order Custom Duty Fee amount (Local)"
    type: number
    description: "Custom Duty Fee for the order"
    sql: ${TABLE}.order_custom_duty_fee ;;
  }


  dimension_group: order_datetime {
    label: "Sales Order"
    type: time
    description: "Date when the order was created"
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.order_datetime ;;
  }

  dimension: order_id {
    group_label: "Sales Order"
    type: string
    sql: ${TABLE}.order_id ;;
    link: {
      label: "View Order in OMS"
      url: "https://acc-trb.retailunity.com/cpanel/cpanel_includes/inc_datatables/orders/orders_all.php7?action=edit&id={{toryburch_acceptance.retailunity_order_id._value}}"
    }
  }

  dimension: retailunity_order_id {
    group_label: "Sales Order"
    hidden: yes
    type: string
    sql: ${TABLE}.retailunity_order_id ;;
  }

  dimension: sales_origin_id {
    group_label: "Sales Order"
    type: number
    hidden: yes
    sql: ${TABLE}.sales_origin_id ;;
  }

  dimension: sales_origin_internal_id {
    group_label: "Sales Order"
    hidden: yes
    type: string
    sql: ${TABLE}.sales_origin_internal_id ;;
  }

  dimension: sales_origin_name {
    group_label: "Sales Order"
    label: "Store name"
    description: "Name of the store where the order is placed"
    type: string
    sql: ${TABLE}.sales_origin_name ;;
  }

  dimension: store_id {
    group_label: "Sales Order"
    type: number
    description: "ID of the store where the order is placed"
    sql: ${TABLE}.store_id ;;
  }

  dimension: order_payment_fee {
    group_label: "Sales Order"
    label: "Order Payment Fee amount (Local)"
    description: "Payment Fee for the order"
    type: number
    sql: ${TABLE}.order_payment_fee ;;
  }

  dimension: order_shipping {
    group_label: "Sales Order"
    label: "Order Shipping amount (Local)"
    description: "Shipping Fee for the order (Local currency)"
    type: number
    sql: ${TABLE}.order_shipping ;;
  }

  dimension: delivery_type {
    group_label: "Sales Order"
    description: "Delivery type chosen by the customer"
    type: string
    sql: ${TABLE}.delivery_type ;;
  }

  dimension: order_total {
    group_label: "Sales Order"
    label: "Order Total Amount (Local)"
    hidden: yes
    type: number
    sql: ${TABLE}.order_total ;;
  }

  dimension: payment_method {
    group_label: "Sales Order"
    description: "Payment method chosen by the customer"
    type: string
    sql: ${TABLE}.payment_method ;;
  }

  dimension: payment_status {
    group_label: "Sales Order"
    description: "Determines if the order was paid or pending"
    type: string
    sql: ${TABLE}.payment_status ;;
  }

  dimension: coupon_code {
    group_label: "Sales Order"
    hidden: yes
    type: string
    sql: ${TABLE}.coupon_code ;;
  }

  dimension: voucher_code {
    group_label: "Sales Order"
    hidden: yes
    type: string
    sql: ${TABLE}.voucher_code ;;
  }

  dimension: xstore_invoice_number {
    group_label: "Sales Order"
    type: string
    description: ""
    sql: ${TABLE}.xstore_invoice_number ;;
  }

  measure: products_in_order {
    group_label: "Sales Order"
    label: "Number of Products in Order"
    type: sum
    sql: cast(${TABLE}.products_in_order as int64) ;;
    filters: [record_type: "SALE"]
  }

  dimension: employee_id {
    group_label: "Sales Order"
    description: "ID of the employee who processed the order"
    type: string
    sql: ${TABLE}.employee_id ;;
  }

  dimension: channel {
    group_label: "Sales Order"
    description: "Channel in which the order was placed"
    type: string
    sql: ${TABLE}.channel ;;
  }

  dimension: giftwrapping {
    type: string
    label: "Gift Wrapping (Y/N)"
    description: "Shows Y (Yes) if a Gift Wrapping service item was part of the order, else N (No)"
    group_label: "Sales Order"
    sql: ${TABLE}.giftwrapping ;;
  }


### End of Sales Order dimensions
#}

########################################
### Sales Order item - Dimensions ###
########################################{

  dimension: cc_location_id {
    group_label: "Sales Order Item"
    hidden: yes
    type: string
    sql: ${TABLE}.cc_location_id ;;
  }

  dimension: cc_location_internal_id {
    group_label: "Sales Order Item"
    label: "Click & Collect Shop/WH Id"
    description: "Click & Collect Shop/WH Id"
    type: string
    sql: ${TABLE}.cc_location_internal_id ;;
  }

  dimension: cc_location_name {
    group_label: "Sales Order Item"
    label: "Click & Collect Shop/WH Name"
    description: "Click & Collect Shop/WH Name"
    type: string
    sql: ${TABLE}.cc_location_name ;;
  }

  dimension: detail_brand {
    group_label: "Sales Order Item"
    hidden: yes
    type: string
    sql: ${TABLE}.detail_brand ;;
  }

  dimension: detail_custom_duty {
    group_label: "Sales Order Item"
    label: "Custom Duty amount (Local)"
    description: "Custom Duty amount for the order (Local Currency)"
    type: string
    sql: ${TABLE}.detail_custom_duty ;;
  }

  dimension: detail_external_id {
    label: "Source SKU"
    group_label: "Sales Order Item"
    description: "Item SKU number coming from OMS"
    type: string
    sql: ${TABLE}.detail_external_id ;;
  }

  dimension: sku {
    group_label: "Sales Order Item"
    hidden: yes
    type: string
    sql: ${TABLE}.sku ;;
  }

  dimension: detail_id {
    group_label: "Sales Order Item"
    hidden: yes
    type: number
    sql: cast(${TABLE}.detail_id as int64) ;;
  }

  dimension: detail_name {
    group_label: "Sales Order Item"
    hidden: yes
    type: string
    sql: ${TABLE}.detail_name ;;
  }

  dimension: detail_price_original {
    group_label: "Sales Order Item"
    label: "Item Original Price (Local)"
    description: "Original price of the item (Before discount) (Local currency)"
    type: number
    sql: ${TABLE}.detail_price_original ;;
  }

  dimension: discount_amount {
    group_label: "Sales Order Item"
    label: "Discount Amount (Local)"
    description: "Discount amount for the item (Local Currency)"
    type: number
    sql: ${TABLE}.discount_amount ;;
  }

  dimension: detail_sku_code {
    group_label: "Sales Order Item"
    hidden: yes
    type: string
    sql: ${TABLE}.detail_sku_code ;;
  }

  dimension: detail_tax {
    group_label: "Sales Order Item"
    label: "Tax rate"
    description: "Tax rate applied to the order"
    type: number
    sql: ${TABLE}.detail_tax ;;
  }

  dimension: detail_tax_amount {
    group_label: "Sales Order Item"
    label: "Tax Amount (Local)"
    description: "Tax amount applied to the order (Local Currency)"
    type: number
    sql: ${TABLE}.detail_tax_amount ;;
  }

  dimension: barcode {
    label: "Source Barcode"
    group_label: "Sales Order Item"
    description: "Item Barcode number coming from OMS"
    type: string
    sql: ${TABLE}.barcode ;;
  }

  dimension: price {
    group_label: "Sales Order Item"
    label: "Item Price (Local)"
    description: "Item price (Local Currency)"
    type: number
    sql: ${TABLE}.price ;;
  }

  dimension: record_type {
    group_label: "Sales Order Item"
    description: "Categorizes the sales order item record. Shows either SALE, CANCELLED, REFUND, SHIPMENT, ..."
    type: string
    sql: ${TABLE}.record_type ;;
  }

  dimension: return_cancel_reason {
    group_label: "Sales Order Item"
    label: "Item Return/Cancel Reason"
    description: "Reason why the customer returned/cancelled the order"
    type: string
    sql: ${TABLE}.return_cancel_reason ;;
  }

  dimension: return_location_id {
    group_label: "Sales Order Item"
    hidden: yes
    type: string
    sql: ${TABLE}.return_location_id ;;
  }

  dimension: return_location_internal_id {
    group_label: "Sales Order Item"
    label: "Return WH/Store Loc Id"
    description: "ID of the WH/store where the item was returned"
    type: string
    sql: ${TABLE}.return_location_internal_id ;;
  }

  dimension: return_location_name {
    group_label: "Sales Order Item"
    label: "Return WH/Store Loc Name"
    description: "Name of the WH/store where the item was returned"
    type: string
    sql: ${TABLE}.return_location_name ;;
  }

  dimension: return_quality {
    group_label: "Sales Order Item"
    description: "Quality in which the item was returned"
    type: string
    sql: ${TABLE}.return_quality ;;
  }

  dimension: cancellation_reason {
    group_label: "Sales Order Item"
    label: "Item Cancel Reason"
    description: "Reason why the item was cancelled"
    type: string
    sql: ${TABLE}.cancellation_reason ;;
  }

  dimension: fulfilment_employee_id {
    group_label: "Sales Order Item"
    description: "Employee fulfilling the order item"
    type: string
    sql: ${TABLE}.fulfilment_employee_id ;;
  }


### End of Sales Order Item dimensions
#}


########################################
### Sales Order item Shipment - Dimensions ###
########################################{

  dimension: shipment_id {
    group_label: "Sales Order Item Shipment"
    description: "Shipment ID"
    type: string
    sql: ${TABLE}.shipment_id ;;
  }

  dimension: tracking_id {
    group_label: "Sales Order Item Shipment"
    description: "Tracking ID"
    type: string
    sql: ${TABLE}.tracking_id ;;
  }

  dimension: parcel_id {
    group_label: "Sales Order Item Shipment"
    description: "Parcel ID"
    type: string
    sql: ${TABLE}.parcel_id ;;
  }

  dimension: delivery_city {
    group_label: "Sales Order Item Shipment"
    description: "City where the order is to be delivered"
    type: string
    sql: ${TABLE}.delivery_city ;;
  }

  dimension: delivery_country {
    group_label: "Sales Order Item Shipment"
    description: "Country where the order is to be delivered"
    type: string
    sql: ${TABLE}.delivery_country ;;
  }

  measure: products_in_parcel {
    group_label: "Sales Order Item Shipment"
    label: "Number of Products in Parcel"
    type: sum
    sql: CASE WHEN ${TABLE}.products_in_parcel = "-" THEN 0
         ELSE cast(${TABLE}.products_in_parcel as int64)
         END ;;
    filters: [record_type: "SHIPMENT"]
  }

  measure: shipping_discount_amount {
    group_label: "Sales Order Item Shipment"
    label: "Shipping Discount Amount (Local)"
    type: sum
    sql: ${TABLE}.shipping_discount_amount ;;
    filters: [record_type: "SHIPMENT"]
  }

  dimension: shipping_location_id {
    group_label: "Sales Order Item Shipment"
    hidden: yes
    type: string
    sql: ${TABLE}.shipping_location_id ;;
  }

  dimension: shipping_location_internal_id {
    group_label: "Sales Order Item Shipment"
    label: "Shipping WH/Store Loc Id"
    description: "ID of the WH/Store from where the item was shipped"
    type: string
    sql: ${TABLE}.shipping_location_internal_id ;;
  }

  dimension: shipping_location_name {
    group_label: "Sales Order Item Shipment"
    label: "Shipping WH/Store Loc Name"
    description: "Name of the WH/Store from where the item was shipped"
    type: string
    sql: ${TABLE}.shipping_location_name ;;
  }

  measure: shipping_original_amount {
    group_label: "Sales Order Item Shipment"
    label: "Shipping Original Amount (Local)"
    type: sum
    sql: ${TABLE}.shipping_original_amount ;;
    filters: [record_type: "SHIPMENT"]
  }


### End of Sales Order item Shipment
#}


  ######################################
  ########    GA dimensions    #########
  ######################################{

  dimension: acq_campaign {
    group_label: "Acquisition / Marketing"
    label: "Campaign"
    description: "Marketing Campaign name"
    type: string
    sql: ${TABLE}.acq_campaign ;;
  }

  dimension: acq_device {
    group_label: "Acquisition / Marketing"
    label: "Device"
    description: "The type of device where the session iniated from: desktop, tablet, or mobile."
    type: string
    sql: ${TABLE}.acq_device ;;
  }

  dimension: acq_keyword {
    group_label: "Acquisition / Marketing"
    label: "Keyword"
    description: "For manual campaign tracking, it is the value of the utm_term campaign tracking parameter. For AdWords traffic, it contains the best matching targeting criteria. For the display network, where multiple targeting criteria could have caused the ad to show up, it returns the best matching targeting criteria as selected by Ads. This could be display_keyword, site placement, boomuserlist, user_interest, age, or gender. Otherwise its value is (not set)."
    type: string
    sql: ${TABLE}.acq_keyword ;;
  }

  dimension: acq_medium {
    group_label: "Acquisition / Marketing"
    label: "Medium"
    description: "The type of referrals. For manual campaign tracking, it is the value of the utm_medium campaign tracking parameter. For AdWords autotagging, it is cpc. If users came from a search engine detected by Google Analytics, it is organic. If the referrer is not a search engine, it is referral. If users came directly to the property and document.referrer is empty, its value is (none)."
    type: string
    sql: ${TABLE}.acq_medium ;;
  }

  dimension: acq_source {
    group_label: "Acquisition / Marketing"
    label: "Source"
    description: "The source of referrals. For manual campaign tracking, it is the value of the utm_source campaign tracking parameter. For AdWords autotagging, it is google. If you use neither, it is the domain of the source (e.g., document.referrer) referring the users. It may also contain a port address. If users arrived without a referrer, its value is (direct)."
    type: string
    sql: ${TABLE}.acq_source ;;
  }

  dimension: marketing_source {
    type: string
    group_label: "Acquisition / Marketing"
    description: "Acquisition Platform"
    label: "Marketing Source"
    sql: ${TABLE}.marketing_source ;;
  }

  ### End of GA dimensions
  #}


  ##############################################################################
  ##########################         MEASURES         ##########################
  ##############################################################################


  ######################################
  #####    sales order measures    #####
  ######################################{

  ### Item Quantities ###{

  # dimension: qty_canceled {
  #   type: number
  #   sql: ${TABLE}.qty_canceled ;;
  # }

  # dimension: qty_collected {
  #   type: number
  #   sql: ${TABLE}.qty_collected ;;
  # }

  # dimension: qty_delivered {
  #   type: number
  #   sql: ${TABLE}.qty_delivered ;;
  # }

  # dimension: qty_ordered {
  #   type: number
  #   sql: ${TABLE}.qty_ordered ;;
  # }

  # dimension: qty_packed {
  #   type: number
  #   sql: ${TABLE}.qty_packed ;;
  # }

  # dimension: qty_returned {
  #   type: number
  #   sql: ${TABLE}.qty_returned ;;
  # }

  # dimension: qty_shipped {
  #   type: number
  #   sql: ${TABLE}.qty_shipped ;;
  # }

  measure: units_ordered {
    type: sum
    group_label: "Sales Order Metrics"
    label: "Units Ordered"
    description: "Number of Items Ordered"
    sql: ${TABLE}.qty_ordered ;;
  }

  measure: unit_canceled {
    type: sum
    group_label: "Sales Order Metrics"
    label: "Units Canceled"
    description: "Number of Items Cancelled"
    sql: ${TABLE}.qty_canceled ;;
  }

  measure: unit_collected {
    type: sum
    group_label: "Sales Order Metrics"
    label: "Units Collected"
    description: "Number of Items Collected by the customer"
    sql: ${TABLE}.qty_collected ;;
  }

  measure: units_refunded {
    type: sum
    group_label: "Sales Order Metrics"
    label: "Units Refunded"
    description: "Number of Items Refunded"
    sql: ${TABLE}.qty_returned ;;
  }

  measure: units_shipped {
    type: sum
    group_label: "Sales Order Metrics"
    label: "Units Shipped"
    description: "Number of Items Shipped"
    sql: ${TABLE}.qty_shipped ;;
  }

  measure: units_packed {
    type: sum
    group_label: "Sales Order Metrics"
    label: "Units Packed"
    description: "Number of Items Packed"
    sql: ${TABLE}.qty_packed ;;
  }

  measure: units_delivered {
    type: sum
    group_label: "Sales Order Metrics"
    label: "Units Delivered"
    description: "Number of Items Delivered"
    sql: ${TABLE}.qty_delivered ;;
  }

  measure: upt {
    type: number
    group_label: "Sales Order Metrics"
    label: "UPT"
    description: "Average number of units within each order"
    value_format_name: decimal_2
    sql:${units_ordered}/nullif(${all_orders}, 0);;
  }

  measure: usp {
    type:  number
    label: "USP (USD)"
    group_label: "Sales Order Metrics"
    description: "Unit Selling Price = Average price of each item ordered (in Dollar)"
    value_format: "$#,##0.00"
    sql: ${gross_rev_usd} / NULLIF(${units_ordered},0) ;;
  }

  measure: usp_local {
    type:  number
    label: "USP (Local)"
    group_label: "Sales Order Metrics"
    description: "Unit Selling Price = Average price of each item ordered (in Local currency)"
    value_format: "#,##0.00"
    sql: ${gross_rev_local} / NULLIF(${units_ordered},0) ;;
  }

  measure: cancelled_item_ratio {
    type: number
    group_label: "Sales Order Metrics"
    label: "Cancelled Item %"
    description: "Cancelled Item %"
    value_format_name: percent_2
    sql: ${unit_canceled}/NULLIF(${units_ordered},0) ;;
  }

  ### End of Item Quantity measures ###}

  ### Order Quantities ###

  measure: all_orders {
    type: count_distinct
    group_label: "Sales Order Metrics"
    label: "# Orders"
    description: "Total number of orders placed on the website"
    sql: ${order_id};;
  }

  ### Gross Measures ###{

  # dimension: gross_delivered_local {
  #   type: number
  #   sql: ${TABLE}.gross_delivered_local ;;
  # }

  # dimension: gross_delivered_usd {
  #   type: number
  #   sql: ${TABLE}.gross_delivered_usd ;;
  # }

  # dimension: gross_net_local {
  #   type: number
  #   sql: ${TABLE}.gross_net_local ;;
  # }

  # dimension: gross_net_usd {
  #   type: number
  #   sql: ${TABLE}.gross_net_usd ;;
  # }

  # dimension: gross_revenue_local {
  #   type: number
  #   sql: ${TABLE}.gross_revenue_local ;;
  # }

  # dimension: gross_revenue_local_minus {
  #   type: number
  #   sql: ${TABLE}.gross_revenue_local_minus ;;
  # }

  # dimension: gross_revenue_usd {
  #   type: number
  #   sql: ${TABLE}.gross_revenue_usd ;;
  # }

  # dimension: gross_revenue_usd_minus {
  #   type: number
  #   sql: ${TABLE}.gross_revenue_usd_minus ;;
  # }

  measure: gross_rev_local {
    type: sum
    group_label: "Sales Order Metrics"
    label: "Gross Revenue (Local)"
    description: "Total value of incoming orders on the website (excluding failed credit card attempts, VAT, customs and shipping fees and COD fees)"
    value_format: "#,##0.00"
    sql: ${TABLE}.gross_revenue_local ;;
  }

  measure: gross_rev_usd {
    type: sum
    group_label: "Sales Order Metrics"
    label: "Gross Revenue (USD)"
    description: "Total value of incoming orders on the website (excluding failed credit card attempts, VAT, customs and shipping fees and COD fees)"
    value_format: "$#,##0.00"
    sql: ${TABLE}.gross_revenue_usd ;;
  }

  measure: aov_local {
    type: number
    group_label: "Sales Order Metrics"
    label: "Average Order Value (Local)"
    description: "Average Value of each order"
    value_format: "#,##0.00"
    sql: ${gross_rev_local}/nullif(${all_orders}, 0);;
  }
  measure: aov_usd {
    type: number
    group_label: "Sales Order Metrics"
    label: "Average Order Value (USD)"
    description: "Average Value of each order"
    value_format: "$#,##0.00"
    sql: ${gross_rev_usd}/nullif(${all_orders}, 0);;
  }


  measure: gross_minus_cancelled_local {
    type: sum
    group_label: "Sales Order Metrics"
    label: "Gross Revenue w/o cancellations (Local)"
    description: "Gross Revenue minus Cancellation (Local Currency)"
    value_format: "#,##0.00"
    sql: ${TABLE}.gross_revenue_local_minus ;;
  }

  measure: gross_minus_cancelled_usd {
    type: sum
    group_label: "Sales Order Metrics"
    label: "Gross Revenue w/o cancellations (USD)"
    description: "Gross Revenue minus Cancellation (USD)"
    value_format: "$#,##0.00"
    sql: ${TABLE}.gross_revenue_usd_minus ;;
  }

  measure: gross_minus_cancelled_minus_returns_local {
    type: sum
    group_label: "Sales Order Metrics"
    label: "Gross Revenue w/o cancellations & returns (Local)"
    value_format: "#,##0.00"
    description: "Gross Revenue minus cancellations & returns (Local currency)"
    sql: ${TABLE}.gross_net_local ;;
  }

  measure: gross_minus_cancelled_minus_returns_usd {
    type: sum
    group_label: "Sales Order Metrics"
    label: "Gross Revenue w/o cancellations & returns (USD)"
    value_format: "$#,##0.00"
    description: "Gross Revenue minus cancellations & returns (USD)"
    sql: ${TABLE}.gross_net_usd ;;
  }



  ### End of Gross Measures ###}


  ### Cancelled measures ###

  # dimension: cancel_amount {
  #   type: number
  #   sql: ${TABLE}.cancel_amount ;;
  # }

  # dimension: cancel_amount_usd {
  #   type: number
  #   sql: ${TABLE}.cancel_amount_usd ;;
  # }

  measure: gross_cancelled_local {
    type: sum
    group_label: "Sales Order Metrics"
    label: "Cancelled Amount (Local)"
    value_format: "#,##0.00"
    description: "Value of Cancelled items (Local Currency)"
    sql: ${TABLE}.cancel_amount ;;
  }

  measure: gross_cancelled_usd {
    type: sum
    group_label: "Sales Order Metrics"
    label: "Cancelled Amount (USD)"
    description: "Value of Cancelled items (USD)"
    value_format: "$#,##0.00"
    sql: ${TABLE}.cancel_amount_usd ;;
  }

  measure: cancelled_ratio {
    type: number
    group_label: "Sales Order Metrics"
    label: "Cancelled Rate (% value)"
    description: "Cancelled Items value vs Gross Revenue"
    value_format_name: percent_2
    sql: ${gross_cancelled_usd}/nullif(${gross_rev_usd}, 0);;
  }


  ### Shipment measures ###{


  # dimension: shipment_net_sales_incl_tax {
  #   type: number
  #   sql: ${TABLE}.shipment_net_sales_incl_tax ;;
  # }

  # dimension: shipment_net_sales_usd_incl_tax {
  #   type: number
  #   sql: ${TABLE}.shipment_net_sales_usd_incl_tax ;;
  # }

  # dimension: shipment_revenue_local {
  #   type: number
  #   sql: ${TABLE}.shipment_revenue_local ;;
  # }

  # dimension: shipment_revenue_usd {
  #   type: number
  #   sql: ${TABLE}.shipment_revenue_usd ;;
  # }

  # dimension: shipment_net_sales {
  #   type: number
  #   sql: ${TABLE}.shipment_net_sales ;;
  # }

  # dimension: shipment_net_sales_usd {
  #   type: number
  #   sql: ${TABLE}.shipment_net_sales_usd ;;
  # }

  # dimension: shipped_revenue_local_minus {
  #   type: number
  #   sql: ${TABLE}.shipped_revenue_local_minus ;;
  # }

  # dimension: shipped_revenue_usd_minus {
  #   type: number
  #   sql: ${TABLE}.shipped_revenue_usd_minus ;;
  # }

  measure: shipped_revenue_usd {
    type: sum
    group_label: "Sales Order Metrics"
    label: "Shipped Revenue (USD)"
    description: "Value of Shipped items (USD)"
    value_format: "$#,##0.00"
    sql: ${TABLE}.shipment_revenue_usd ;;
  }

  measure: shipped_revenue_local {
    type: sum
    group_label: "Sales Order Metrics"
    label: "Shipped Revenue (Local)"
    description: "Value of Shipped items (Local)"
    value_format: "#,##0.00"
    sql: ${TABLE}.shipment_revenue_local ;;
  }

  measure: shipment_net_sales_usd {
    type: sum
    group_label: "Sales Order Metrics"
    label: "Shipped Net (USD)"
    description: "Value of Shipped items excluding Returns (USD)"
    value_format: "$#,##0.00"
    sql: ${TABLE}.shipment_net_sales_usd ;;
  }

  measure: shipment_net_sales {
    type: sum
    group_label: "Sales Order Metrics"
    label: "Shipped Net (Local)"
    description: "Value of Shipped items excluding Returns (Local)"
    value_format: "#,##0.00"
    sql: ${TABLE}.shipment_net_sales ;;
  }

  ### End of Shipment measures ###}


  ### Return/Refund measures ###{

  # dimension: amount_refunded {
  #   type: number
  #   sql: ${TABLE}.amount_refunded ;;
  # }

  # dimension: refund_amount_local {
  #   type: number
  #   sql: ${TABLE}.refund_amount_local ;;
  # }

  # dimension: refund_amount_usd {
  #   type: number
  #   sql: ${TABLE}.refund_amount_usd ;;
  # }

  measure: return_rate {
    type: number
    group_label: "Sales Order Metrics"
    label: "Returned Item (%)"
    description: "Rate % of returned items"
    value_format_name: percent_2
    sql: ${units_refunded}/nullif(${units_shipped}, 0);;
  }

  measure: return_rate_value {
    type: number
    group_label: "Sales Order Metrics"
    label: "Returned Rate (% value)"
    description: "Returned items value vs Shipped Revenue"
    value_format_name: percent_2
    sql: ${refund_amount_usd}/nullif(${shipped_revenue_usd}, 0);;
  }

  measure: refund_amount_local {
    type: sum
    group_label: "Sales Order Metrics"
    label: "Return Amount (Local)"
    description: "Value of Returned items (Local Currency)"
    value_format: "#,##0.00"
    sql: ${TABLE}.refund_amount_local ;;
  }

  measure: refund_amount_usd {
    type: sum
    group_label: "Sales Order Metrics"
    label: "Return Amount (USD)"
    description: "Value of Returned items (USD)"
    value_format: "$#,##0.00"
    sql: ${TABLE}.refund_amount_usd ;;
  }

  ### End of Refund/return measures ###}

  ### Packed Measures ###{

  measure: packed_minus_returns_usd {
    type: sum
    group_label: "Sales Order Metrics"
    label: "Packed Sales minus Returns (USD)"
    value_format: "$#,##0.00"
    description:"Total value of packed items excluding returns (USD)"
    sql: ${TABLE}.packed_sales_minus_return_usd;;
  }

  measure: packed_minus_returns_local {
    type: sum
    group_label: "Sales Order Metrics"
    label: "Packed Sales minus Returns (local)"
    value_format: "#,##0.00"
    description:"Total value of packed items excluding returns (local)"
    sql: ${TABLE}.packed_sales_minus_return_local;;
  }

  measure: packed_sales_local {
    type: sum
    group_label: "Sales Order Metrics"
    label: "Packed Sales (local)"
    value_format: "#,##0.00"
    description:"Total value of packed items (local)"
    sql: ${TABLE}.packed_revenue_local;;
  }

  measure: packed_sales_usd {
    type: sum
    group_label: "Sales Order Metrics"
    label: "Packed Sales (USD)"
    value_format: "$#,##0.00"
    description:"Total value of packed items (USD)"
    sql: ${TABLE}.packed_revenue_usd;;
  }

  ### End of Packed measures ###}

  ### End of Sales Order measures
  #}

  ######################################
  ########     GA measures     #########
  ######################################{

  # dimension: bounces {
  #   type: number
  #   sql: ${TABLE}.bounces ;;
  # }

  # dimension: common_clicks {
  #   type: number
  #   sql: ${TABLE}.common_clicks ;;
  # }

  # dimension: common_cost {
  #   type: number
  #   sql: ${TABLE}.common_cost ;;
  # }

  # dimension: common_impressions {
  #   type: number
  #   sql: ${TABLE}.common_impressions ;;
  # }

  # dimension: pageviews {
  #   type: number
  #   sql: ${TABLE}.pageviews ;;
  # }

  # dimension: users {
  #   type: number
  #   sql: ${TABLE}.users ;;
  # }

  # dimension: session_duration {
  #   type: number
  #   sql: ${TABLE}.session_duration ;;
  # }

  # dimension: sessions {
  #   type: number
  #   sql: ${TABLE}.sessions ;;
  # }

  # dimension: product_adds_to_cart {
  #   type: number
  #   sql: ${TABLE}.product_adds_to_cart ;;
  # }

  # dimension: product_checkouts {
  #   type: number
  #   sql: ${TABLE}.product_checkouts ;;
  # }

  # dimension: unique_pageviews {
  #   type: number
  #   sql: ${TABLE}.unique_pageviews ;;
  # }


  measure: number_of_sessions {
    type: sum
    group_label: "GA Metrics"
    label: "# of Sessions"
    description: "Total number of sessions on the website"
    sql: ${TABLE}.sessions ;;
  }

  measure: sum_session_duration {
    type: sum
    group_label: "GA Metrics"
    label: "Sessions duration"
    description: "Total session duration"
    sql: ${TABLE}.session_duration ;;
  }

  measure: sum_bounces {
    type: sum
    group_label: "GA Metrics"
    label: "# of Bounces"
    description: "Number of sessions that have left the website after only one page-view"
    sql: ${TABLE}.bounces ;;
  }

  measure: number_of_pageviews {
    type: sum
    group_label: "GA Metrics"
    label: "# of Page views"
    description: "Total number of page views"
    sql: ${TABLE}.pageviews ;;
  }

  measure: sum_unique_pageviews {
    type: sum
    group_label: "GA Metrics"
    label: "# of Unique Page views"
    description: "Number of unique page views"
    sql: ${TABLE}.unique_pageviews ;;
  }

  measure: sum_product_adds_to_cart {
    type: sum
    group_label: "GA Metrics"
    label: "# of Products Added to Cart"
    description: "Number of Products added to Cart"
    sql: ${TABLE}.product_adds_to_cart ;;
  }

  measure: sum_product_checkouts {
    type: sum
    group_label: "GA Metrics"
    label: "# of Product Checkouts"
    description: "Number of Products checked out"
    sql: ${TABLE}.product_checkouts ;;
  }

  measure: sum_users {
    type: sum
    group_label: "GA Metrics"
    label: "# of Users"
    description: "Total number of visitors on the website for a given period"
    sql: ${TABLE}.users ;;
  }

  measure: pageviews_per_session {
    type: number
    group_label: "GA Metrics"
    label: "Pageviews per Session"
    description: "Average number of pages viewed per session"
    value_format_name: decimal_2
    sql: ${number_of_pageviews}/NULLIF(${number_of_sessions},0) ;;
  }


  measure: bounce_rate {
    type: number
    group_label: "GA Metrics"
    label: "Bounce Rate %"
    description: "Percentage of sessions that have left the website after only one page-view"
    value_format_name: percent_2
    sql: ${sum_bounces}/NULLIF(${number_of_sessions},0) ;;
  }

  measure: avg_time_on_site {
    type: number
    group_label: "GA Metrics"
    label: "Avg Time On Site"
    description: "Average of time spend on the site in seconds per session"
    value_format_name: decimal_2
    sql: ${sum_session_duration}/NULLIF(${number_of_sessions},0) ;;
  }

  measure: add_to_basket_rate {
    type: number
    group_label: "GA Metrics"
    label: "Add to Basket %"
    description: "Percentage of product detail views that have added the product to their basket over a period of time"
    value_format_name: percent_2
    sql: ${sum_product_adds_to_cart}/NULLIF(${number_of_pageviews},0) ;;
  }

  measure: cart_abandonment_rate {
    type: number
    group_label: "GA Metrics"
    label: "Cart Abandonment %"
    description: "Number of cart created that did not convert into an order"
    value_format_name: percent_2
    sql: (${sum_product_adds_to_cart}-${sum_product_checkouts})/NULLIF(${sum_product_adds_to_cart},0) ;;
  }

  measure: checkout_to_add_rate {
    type: number
    group_label: "GA Metrics"
    label: "Checkout to Add to Basket %"
    description: "Ratio of Product checked out vs Added to Cart"
    value_format_name: percent_2
    sql: ${sum_product_checkouts}/NULLIF(${sum_product_adds_to_cart},0) ;;
  }


  measure: conversion_rate {
    type: number
    group_label: "GA Metrics"
    label: "Conversion Rate (%)"
    description: "Percentage of sessions that make an order"
    value_format_name: percent_2
    sql: ${all_orders}/NULLIF(${number_of_sessions},0) ;;
  }

  ### End of GA dimensions
  #}

  ######################################
  ######     funnel measures     #######
  ######################################{

  measure: total_ad_spend_usd {
    type: sum
    group_label: "Funnel Metrics"
    label: "Total Ad Spend (USD)"
    description: "Total value of marketing ads spent across all different channels"
    value_format: "$#,##0.00"
    sql: ${TABLE}.common_cost_usd ;;
  }

  measure: roas_gross_rev {
    type: number
    group_label: "Funnel Metrics"
    label: "ROAS (on Gross Rev)"
    description: "The Return in Gross Sales coming from your Total Ad Spend (based on Gross Sales)"
    value_format: "#,##0.00"
    sql: ${gross_rev_usd}/nullif(${total_ad_spend_usd}, 0) ;;
  }

  measure: roi_gross_rev {
    type: number
    group_label: "Funnel Metrics"
    label: "ROI (on Gross Rev)"
    description: "The Return on Investment in Total Ad Spend in Gross Sales"
    value_format: "#,##0.00"
    sql: (${gross_rev_usd}-${total_ad_spend_usd})/nullif(${total_ad_spend_usd}, 0) ;;
  }

  measure: cost_per_order {
    type: number
    group_label: "Funnel Metrics"
    label: "CPO"
    description: "The marketing cost of one order"
    value_format: "$#,##0.00"
    sql: ${total_ad_spend_usd}/nullif(${all_orders}, 0) ;;
  }

  measure: total_clicks {
    type: sum
    group_label: "Funnel Metrics"
    label: "# Clicks"
    description: "Total number of times an ad is clicked by a user"
    sql: ${TABLE}.common_clicks ;;
  }

  measure: total_impressions {
    type: sum
    group_label: "Funnel Metrics"
    label: "# Impressions"
    description: "Total number of times an ad is seen"
    sql: ${TABLE}.common_impressions ;;
  }

  measure: cost_per_click {
    type: number
    group_label: "Funnel Metrics"
    label: "CPC"
    description: "The marketing cost of one ad click"
    value_format_name: usd
    sql: ${total_ad_spend_usd} / NULLIF(${total_clicks},0) ;;
  }


  measure: click_through_rate {
    type: number
    group_label: "Funnel Metrics"
    label: "Click Through Rate %"
    description: "Percentage ad impressions that have led to a click"
    value_format_name: percent_2
    sql: NULLIF(${total_clicks},0) / NULLIF(${total_impressions},0);;
  }


  measure: cost_per_mill {
    type: number
    group_label: "Funnel Metrics"
    label: "CPM"
    description: "Cost per Thousand Impressions"
    value_format_name: usd
    sql: NULLIF(NULLIF(${total_ad_spend_usd},0) / NULLIF(${total_impressions}/1000,0),0) ;;
  }

  ### End of Funnel measures
  #}

####################################
#########      ALERTS      #########
####################################


  # dimension_group: record_datetime {
  #   type: time
  #   timeframes: [
  #     raw,
  #     time,
  #     date,
  #     week,
  #     month,
  #     quarter,
  #     year
  #   ]
  #   sql: ${TABLE}.record_datetime ;;
  # }

  dimension: source {
    type: string
    description: "Source where the record is recorded"
    sql: ${TABLE}.source ;;
  }


}
