include: "../_common/period_over_period.view"



view: order_items {
  derived_table: {
    sql:
select
  concat(A.atr_cginvoiceno, A.bk_productid) as order_item_id,
  A.atr_trandate as common_date,
  A.atr_cginvoiceno as order_id,
  A.bk_productid as product_id,
  A.mea_quantity as quantity,
  floor(A.mea_amountusd/A.mea_quantity) as sale_price,
  floor(mea_amountusd/mea_quantity - av_cost_usd) as margin,
  B.bu_desc as bu_description,
  C.item_desc as product
from
  `chb-prod-supplychain-data.prod_supply_chain.factretailsales` A,
  `chb-prod-supplychain-data.prod_shared_dimensions.dim_alternate_bu_hierarchy` B,
  `chb-prod-supplychain-data.prod_shared_dimensions.dim_retail_product` C
where
  A.bk_storeid = cast(B.store_code as int64)
  and B.brand = "FACES"
  and A.bk_productid = C.item
order by 1,2 desc
    ;;
  }

  dimension: order_item_id {
    sql: ${TABLE}.order_item_id ;;
  }

  dimension: order_id {
    sql: ${TABLE}.order_id ;;
  }
  dimension: product_id {
    sql: ${TABLE}.product_id ;;
  }

  dimension: common_date {
    sql: ${TABLE}.common_date ;;
  }

}


view: aa_orders {
  derived_table: {
    sql:
select
  A.atr_cginvoiceno as order_id,
  min(A.atr_trandate) as common_date,
  COUNT(distinct A.bk_productid) as distinct_products
from
  `chb-prod-supplychain-data.prod_supply_chain.factretailsales` A,
  `chb-prod-supplychain-data.prod_shared_dimensions.dim_alternate_bu_hierarchy` B,
  `chb-prod-supplychain-data.prod_shared_dimensions.dim_retail_product` C
where
  A.bk_storeid = cast(B.store_code as int64)
  and B.brand = "FACES"
  and A.bk_productid = C.item
group by 1
      ;;
  }

  dimension:  order_id{
    sql: ${TABLE}.order_id ;;
  }

  dimension: common_date {
    sql: ${TABLE}.common_date ;;
  }

  dimension: order_with_single_product {
    sql: ${TABLE}.distinct_products ;;
  }
}


view: order_product {
  derived_table: {
    sql:
select
  concat(A.atr_cginvoiceno, A.bk_productid) as order_item_id,
  A.atr_cginvoiceno as order_id,
  A.atr_trandate as common_date,
  C.item_desc as product
from
  `chb-prod-supplychain-data.prod_supply_chain.factretailsales` A,
  `chb-prod-supplychain-data.prod_shared_dimensions.dim_alternate_bu_hierarchy` B,
  `chb-prod-supplychain-data.prod_shared_dimensions.dim_retail_product` C
where
  A.bk_storeid = cast(B.store_code as int64)
  and B.brand = "FACES"
  and A.bk_productid = C.item
       ;;
  }

  dimension: order_item_id {
    sql: ${TABLE}.order_item_id ;;
  }

  dimension: order_id {
    sql: ${TABLE}.order_id ;;
  }

  dimension: common_date {
    sql: ${TABLE}.common_date ;;
  }

}


view: order_metrics {
  derived_table: {
    sql:
select
  AA.order_item_id,
  AA.order_id,
  AA.common_date,
  BB.basket_sales as basket_sales,
  BB.basket_margin as basket_margin
from
(
select
  concat(A.atr_cginvoiceno, A.bk_productid) as order_item_id,
  A.atr_cginvoiceno as order_id,
  A.atr_trandate as common_date,
from
  `chb-prod-supplychain-data.prod_supply_chain.factretailsales` A,
  `chb-prod-supplychain-data.prod_shared_dimensions.dim_alternate_bu_hierarchy` B,
  `chb-prod-supplychain-data.prod_shared_dimensions.dim_retail_product` C
where
  A.bk_storeid = cast(B.store_code as int64)
  and B.brand = "FACES"
  and A.bk_productid = C.item
) AA
left join
(
select
  A.atr_cginvoiceno as order_id,
  floor(sum(A.mea_amountusd/A.mea_quantity)) as basket_sales,
  floor(sum(mea_amountusd/mea_quantity - av_cost_usd)) as basket_margin,
from
  `chb-prod-supplychain-data.prod_supply_chain.factretailsales` A,
  `chb-prod-supplychain-data.prod_shared_dimensions.dim_alternate_bu_hierarchy` B,
  `chb-prod-supplychain-data.prod_shared_dimensions.dim_retail_product` C
where
  A.bk_storeid = cast(B.store_code as int64)
  and B.brand = "FACES"
  and A.bk_productid = C.item
  and A.mea_quantity > 0
group by 1
) BB
on AA.order_id = BB.order_id
    ;;
  }

  dimension: order_item_id {
    sql: ${TABLE}.order_item_id ;;
  }

}








view: basket_metrics {
  derived_table: {
    sql:
select
  AA.common_date,
  AA.product_id,
  sum(BB.basket_sales) as basket_sales,
  sum(BB.basket_margin) as basket_margin
from
(
select
  A.bk_productid as product_id,
  A.atr_cginvoiceno as order_id,
  A.atr_trandate as common_date,
from
  `chb-prod-supplychain-data.prod_supply_chain.factretailsales` A,
  `chb-prod-supplychain-data.prod_shared_dimensions.dim_alternate_bu_hierarchy` B,
  `chb-prod-supplychain-data.prod_shared_dimensions.dim_retail_product` C
where
  A.bk_storeid = cast(B.store_code as int64)
  and B.brand = "FACES"
  and A.bk_productid = C.item
  and A.mea_quantity > 0
) AA
left join
(
select
  A.atr_cginvoiceno as order_id,
  floor(sum(A.mea_amountusd/A.mea_quantity)) as basket_sales,
  floor(sum(mea_amountusd/mea_quantity - av_cost_usd)) as basket_margin,
from
  `chb-prod-supplychain-data.prod_supply_chain.factretailsales` A,
  `chb-prod-supplychain-data.prod_shared_dimensions.dim_alternate_bu_hierarchy` B,
  `chb-prod-supplychain-data.prod_shared_dimensions.dim_retail_product` C
where
  A.bk_storeid = cast(B.store_code as int64)
  and B.brand = "FACES"
  and A.bk_productid = C.item
  and A.mea_quantity > 0
group by 1
) BB
on AA.order_id = BB.order_id
group by 1,2
    ;;
  }

  dimension: common_date {
    sql: ${TABLE}.common_date ;;
  }

  dimension: product_id {
    sql: ${TABLE}.product_id ;;
  }

  dimension: basket_sales {
    sql: ${TABLE}.basket_sales ;;
  }

  dimension:  basket_margin {
    sql: ${TABLE}.basket_margin ;;
  }

}



view: total_orders {
  derived_table: {
    sql:

select
  A.atr_trandate as common_date,
  count(distinct atr_cginvoiceno) as count
from
`chb-prod-supplychain-data.prod_supply_chain.factretailsales` A,
`chb-prod-supplychain-data.prod_shared_dimensions.dim_alternate_bu_hierarchy` B,
`chb-prod-supplychain-data.prod_shared_dimensions.dim_retail_product` C
where
A.bk_storeid = cast(B.store_code as int64)
and B.brand = "FACES"
and A.bk_productid = C.item
and A.mea_quantity > 0
group by 1
       ;;
  }

  dimension: common_date {
    sql: ${TABLE}.common_date ;;
  }

  dimension: count {
    type: number
    sql: ${TABLE}.count ;;
    label: "Total Order Count"
  }

}



view: total_order_product {
  derived_table: {
    sql:
select
  A.atr_trandate as common_date,
  A.bk_productid as product_id,
  C.item_desc as product_name,
  sum(case when A.mea_quantity = 1 then 1 else 0 end) as product_count_purchased_alone,
  sum(A.mea_quantity) as total_quantity,
  sum(floor(A.mea_amountusd/A.mea_quantity)) as product_sales,
  sum(floor(mea_amountusd/mea_quantity - av_cost_usd)) as product_margin,
  count(A.atr_cginvoiceno) as product_order_count
from
  `chb-prod-supplychain-data.prod_supply_chain.factretailsales` A,
  `chb-prod-supplychain-data.prod_shared_dimensions.dim_alternate_bu_hierarchy` B,
  `chb-prod-supplychain-data.prod_shared_dimensions.dim_retail_product` C
where
  A.bk_storeid = cast(B.store_code as int64)
  and B.brand = "FACES"
  and A.bk_productid = C.item
  and A.mea_quantity > 0
group by 1,2,3
    ;;
  }

  dimension: common_date {
    sql: ${TABLE}.common_date ;;
  }

  dimension: product_id {
    sql: ${TABLE}.product_id ;;
  }

  dimension: product_name {
    sql: ${TABLE}.product_name ;;
  }

  dimension: product_count_purchased_alone {
    type: number
    hidden: yes
    sql: ${TABLE}.product_count_purchased_alone ;;
  }

  dimension: product_order_count {
    description: "Total number of orders with product in them, during specified timeframe"
    type: number
    sql: ${TABLE}.product_order_count ;;
    value_format: "#"
  }

  dimension: product_percent_purchased_alone {
    description: "The % of times product is purchased alone, over all transactions containing product"
    type: number
    sql: 1.0*${product_count_purchased_alone}/(CASE WHEN ${product_order_count}=0 THEN NULL ELSE ${product_order_count} END);;
    value_format_name: percent_1
  }

  dimension: product_sales {
    sql: ${TABLE}.product_sales ;;
  }

  dimension: product_margin {
    sql: ${TABLE}.product_margin ;;
  }



  #  Frequencies
  dimension: product_order_frequency {
    description: "How frequently orders include product as a percent of total orders"
    type: number
    sql: 1.0*${product_order_count}/${total_orders.count} ;;
    value_format: "0.0000%"
  }




  # Measure --------------------------------------------------------------

  measure: m_product_count_purchased_alone {
    type: sum
    hidden: yes
    sql: ${TABLE}.product_count_purchased_alone ;;
  }

  measure: m_product_order_count {
    description: "Total number of orders with product in them, during specified timeframe"
    type: sum
    sql: ${TABLE}.product_order_count ;;
    value_format: "#"
  }

  measure: m_product_percent_purchased_alone {
    description: "The % of times product is purchased alone, over all transactions containing product"
    type: number
    sql: 1.0*${m_product_count_purchased_alone}/(CASE WHEN ${m_product_order_count}=0 THEN NULL ELSE ${m_product_order_count} END);;
    value_format_name: percent_1
  }

  measure: m_product_sales {
    type: sum
    sql: ${TABLE}.product_sales ;;
  }

  measure: m_product_margin {
    type: sum
    sql: ${TABLE}.product_margin ;;
  }

  measure: m_product_order_frequency {
    description: "How frequently orders include product as a percent of total orders"
    type: number
    sql: 1.0*${product_order_count}/${total_orders.count} ;;
    value_format: "0.0000%"
  }

  # Measure --------------------------------------------------------------


}


view: order_purchase_affinity {

  derived_table: {
      sql:

select
  atr_trandate_a as common_date,
  product_a,
  product_b,
  count(invoice_a) as joint_order_count,
  sum(product_a_order_count) as product_a_order_count,
  sum(product_b_order_count) as product_b_order_count,
  sum(product_a_product_sales) as product_a_product_sales,
  sum(product_b_product_sales) as product_b_product_sales

from



(select
  concat(A.atr_cginvoiceno, A.bk_productid) as order_item_id_a,
  atr_trandate as atr_trandate_a,
  bk_productid as product_A,
  atr_cginvoiceno as invoice_a,
  count(atr_cginvoiceno) as product_a_order_count,
  sum(sale_price) as product_a_product_sales

  from
(
select
  A.atr_trandate,
  A.atr_cginvoiceno,
  A.bk_productid,
  A.mea_quantity,
  floor(A.mea_amountusd/A.mea_quantity) as sale_price,
  B.bu_desc,
  C.item_desc
from
  `chb-prod-supplychain-data.prod_supply_chain.factretailsales` A,
  `chb-prod-supplychain-data.prod_shared_dimensions.dim_alternate_bu_hierarchy` B,
  `chb-prod-supplychain-data.prod_shared_dimensions.dim_retail_product` C
where
  A.bk_storeid = cast(B.store_code as int64)
  and B.brand = "FACES"
  and A.bk_productid = C.item
  and A.mea_quantity > 0
order by 1,2 desc
) as A
group by atr_trandate_a, bk_productid, atr_cginvoiceno
) as AA



left join



(select
  concat(B.atr_cginvoiceno, B.bk_productid) as order_item_id_b,
  atr_trandate_b,
  bk_productid as product_B,
  atr_cginvoiceno as invoice_b,
  count(atr_cginvoiceno) as product_b_order_count,
  sum(sale_price) as product_b_product_sales

  from
(
select
  A.atr_trandate as atr_trandate_b,
  A.atr_cginvoiceno,
  A.bk_productid,
  A.mea_quantity,
  floor(A.mea_amountusd/A.mea_quantity) as sale_price,
  B.bu_desc, C.item_desc
from
  `chb-prod-supplychain-data.prod_supply_chain.factretailsales` A,
  `chb-prod-supplychain-data.prod_shared_dimensions.dim_alternate_bu_hierarchy` B,
  `chb-prod-supplychain-data.prod_shared_dimensions.dim_retail_product` C
where
  A.bk_storeid = cast(B.store_code as int64)
  and B.brand = "FACES"
  and A.bk_productid = C.item
  and A.mea_quantity > 0
order by 1,2 desc
) as B

group by atr_trandate_b, bk_productid, atr_cginvoiceno
) as BB




on
  invoice_a = invoice_b
  and product_A != product_B

group by atr_trandate_a, product_a, product_b

order by joint_order_count desc

  ;;
    }


  dimension: brand_security{
    type: string
    hidden: yes
    sql: "FACES" ;;
  }

  dimension: vertical_security{
    type: string
    hidden: yes
    sql: "BEAUTY" ;;
  }

  extends: [period_over_period]

  dimension_group: pop_no_tz {
    sql: ${common_raw} ;;
  }

  dimension: primary_key {
    type: number
    primary_key: yes
    hidden: yes
    sql: concat(${TABLE}.common_date,${TABLE}.product_a,${TABLE}.product_b) ;;
  }

  dimension_group: common {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    label: "Date"
    sql: ${TABLE}.common_date ;;
  }

  dimension:  product_a {
    type: string
    sql: ${TABLE}.product_a ;;
  }

  dimension: product_name_A {
    sql: ${total_order_product_A.product_name} ;;
  }

  dimension: order_item_id {
    sql: ${TABLE}.order_item_id ;;
  }

  dimension:  product_b {
    type: string
    sql: ${TABLE}.product_b ;;
  }

  dimension: product_name_B {
    sql: ${total_order_product_B.product_name} ;;
  }

  dimension: joint_order_count {
    description: "How many times item A and B were purchased in the same order"
    type: number
    sql: ${TABLE}.joint_order_count ;;
    value_format: "#"
  }


  dimension: product_a_order_count {
    description: "Total number of orders with product A in them, during specified timeframe"
    type: number
    sql: ${TABLE}.product_a_order_count ;;
    value_format: "#"
  }

  dimension: product_b_order_count {
    description: "Total number of orders with product B in them, during specified timeframe"
    type: number
    sql: ${TABLE}.product_b_order_count ;;
    value_format: "#"
  }



  # Measure from Other views
  # Measure --------------------------------------------------------------

  measure: m_basket_sales_A {
    label: "Basket Sales A"
    type: sum
    sql: ${basket_metrics_A.basket_sales} ;;
  }

  measure:  m_basket_margin_A {
    label: "Basket Margin A"
    type: sum
    sql: ${basket_metrics_A.basket_margin} ;;
  }

  measure: m_basket_sales_B {
    label: "Basket Sales B"
    type: sum
    sql: ${basket_metrics_B.basket_sales} ;;
  }

  measure:  m_basket_margin_B {
    label: "Basket Margin B"
    type: sum
    sql: ${basket_metrics_B.basket_margin} ;;
  }

  measure: m_count {
    type: sum
    sql: ${total_orders.count} ;;
    label: "Total Order Count"
  }

  # Measure --------------------------------------------------------------



  # Measures --------------------------------------------------

  measure: m_joint_order_count {
    label: "Joint Order Count"
    description: "How many times item A and B were purchased in the same order"
    type: sum
    sql: ${TABLE}.joint_order_count ;;
    value_format: "#"
  }


  measure: m_product_a_order_count {
    label: "Product A Order Count"
    description: "Total number of orders with product A in them, during specified timeframe"
    type: sum
    sql: ${TABLE}.product_a_order_count ;;
    value_format: "#"
  }

  measure: m_product_b_order_count {
    label: "Product B Order Count"
    description: "Total number of orders with product B in them, during specified timeframe"
    type: sum
    sql: ${TABLE}.product_b_order_count ;;
    value_format: "#"
  }

  # Measures --------------------------------------------------





  #  Frequencies
  dimension: product_a_order_frequency {
    description: "How frequently orders include product A as a percent of total orders"
    type: number
    sql: 1.0*${total_order_product_A.product_order_count}/${total_orders.count} ;;
    value_format: "0.0000%"
  }

  dimension: product_b_order_frequency {
    description: "How frequently orders include product B as a percent of total orders"
    type: number
    sql: 1.0*${total_order_product_B.product_order_count}/${total_orders.count} ;;
    value_format: "0.0000%"
  }

  dimension: joint_order_frequency {
    description: "How frequently orders include both product A and B as a percent of total orders"
    type: number
    sql: 1.0*${joint_order_count}/${total_orders.count} ;;
    value_format: "0.0000%"
  }




  # Measures --------------------------------------------------

  measure: m_product_a_order_frequency {
    label: "Product A Order Frequency"
    description: "How frequently orders include product A as a percent of total orders"
    type: number
    sql: 1.0*${total_order_product_A.m_product_order_count}/${m_count} ;;
    value_format: "0.0000%"
  }

  measure: m_product_b_order_frequency {
    label: "Product B Order Frequency"
    description: "How frequently orders include product B as a percent of total orders"
    type: number
    sql: 1.0*${total_order_product_B.m_product_order_count}/${m_count} ;;
    value_format: "0.0000%"
  }

  measure: m_joint_order_frequency {
    label: "Joint Order Frequency"
    description: "How frequently orders include both product A and B as a percent of total orders"
    type: number
    sql: 1.0*${m_joint_order_count}/${m_count} ;;
    value_format: "0.0000%"
  }

  # Measures --------------------------------------------------




  # Affinity Metrics
  dimension: add_on_frequency {
    description: "How many times both Products are purchased when Product A is purchased"
    type: number
    sql: 1.0*${joint_order_count}/${total_order_product_A.product_order_count} ;;
    value_format: "0.00%"
  }

  dimension: lift {
    description: "The likelihood that buying product A drove the purchase of product B"
    type: number
    value_format_name: decimal_3
    sql: 1*${joint_order_frequency}/(${product_a_order_frequency} * ${product_b_order_frequency}) ;;
  }


  dimension: product_a_count_purchased_alone  {
    type: number
    hidden: yes
    sql: ${total_order_product_A.product_count_purchased_alone} ;;
  }

  dimension: product_a_percent_purchased_alone {
    description: "The % of times product A is purchased alone, over all transactions containing product A"
    type: number
    sql: 1.0*${product_a_count_purchased_alone}/(CASE WHEN ${product_a_order_count}=0 THEN NULL ELSE ${product_a_order_count} END);;
    value_format_name: percent_1
  }

  dimension: product_b_count_purchased_alone  {
    type: number
    hidden: yes
    sql: ${total_order_product_B.product_count_purchased_alone} ;;
  }

  dimension: product_b_percent_purchased_alone {
    description: "The % of times product B is purchased alone, over all transactions containing product B"
    type: number
    sql: 1.0*${product_b_count_purchased_alone}/(CASE WHEN ${product_b_order_count}=0 THEN NULL ELSE ${product_b_order_count} END);;
    value_format_name: percent_1
  }

  dimension: product_order_count {
    description: "Total number of orders with product in them, during specified timeframe"
    type: number
    sql: ${TABLE}.product_order_count ;;
    value_format: "#"
  }



  # Measures --------------------------------------------------

  measure: m_add_on_frequency {
    label: "Add on Frequency"
    description: "How many times both Products are purchased when Product A is purchased"
    type: number
    sql: 1.0*${m_joint_order_count}/${total_order_product_A.m_product_order_count} ;;
    value_format: "0.00%"
  }

  measure: m_lift {
    label: "Lift"
    description: "The likelihood that buying product A drove the purchase of product B"
    type: number
    value_format_name: decimal_3
    sql: 1*${m_joint_order_frequency}/(${m_product_a_order_frequency} * ${m_product_b_order_frequency}) ;;
  }


  measure: m_product_a_count_purchased_alone  {
    label: "Product A Count Purchased Alone"
    type: number
    hidden: yes
    sql: ${total_order_product_A.m_product_count_purchased_alone} ;;
  }

  measure: m_product_a_percent_purchased_alone {
    label: "Product A Percent Purchased Alone"
    description: "The % of times product A is purchased alone, over all transactions containing product A"
    type: number
    sql: 1.0*${m_product_a_count_purchased_alone}/(CASE WHEN ${m_product_a_order_count}=0 THEN NULL ELSE ${m_product_a_order_count} END);;
    value_format_name: percent_1
  }

  measure: m_product_b_count_purchased_alone  {
    label: "Product A Count Purchased Alone"
    type: number
    hidden: yes
    sql: ${total_order_product_B.m_product_count_purchased_alone} ;;
  }

  measure: m_product_b_percent_purchased_alone {
    label: "Product B Percent Purchased Alone"
    description: "The % of times product B is purchased alone, over all transactions containing product B"
    type: number
    sql: 1.0*${m_product_b_count_purchased_alone}/(CASE WHEN ${m_product_b_order_count}=0 THEN NULL ELSE ${m_product_b_order_count} END);;
    value_format_name: percent_1
  }

  measure: m_product_order_count {
    label: "Product Order Count"
    description: "Total number of orders with product in them, during specified timeframe"
    type: number
    sql: ${TABLE}.m_product_order_count ;;
    value_format: "#"
  }

  # Measures --------------------------------------------------



  # Sales Metrics - Totals

  dimension: product_a_total_sales {
    view_label: "Sales and Margin - Total"
    group_label: "Product A - Sales"
    type: number
    sql:  ${total_order_product_A.product_sales} ;;
    value_format_name: decimal_2
  }

  dimension: product_a_total_basket_sales {
    view_label: "Sales and Margin - Total"
    group_label: "Product A - Sales"
    type: number
    sql: ${basket_metrics_A.basket_sales} ;;
    value_format_name: decimal_2
  }

  dimension: product_a_total_rest_of_basket_sales {
    view_label: "Sales and Margin - Total"
    group_label: "Product A - Sales"
    type: number
    sql: ${product_a_total_basket_sales}-IFNULL(${product_a_total_sales},0) ;;
    value_format_name: decimal_2
  }

  dimension: product_b_total_sales {
    view_label: "Sales and Margin - Total"
    group_label: "Product B - Sales"
    type: number
    sql:  ${total_order_product_B.product_sales} ;;
    value_format_name: decimal_2
  }

  dimension: product_b_total_basket_sales {
    view_label: "Sales and Margin - Total"
    group_label: "Product B - Sales"
    type: number
    sql: ${basket_metrics_B.basket_sales} ;;
    value_format_name: decimal_2
  }

  dimension: product_b_total_rest_of_basket_sales {
    view_label: "Sales and Margin - Total"
    group_label: "Product B - Sales"
    type: number
    sql: ${product_b_total_basket_sales}-IFNULL(${product_b_total_sales},0) ;;
    value_format_name: decimal_2
  }




  # Measures --------------------------------------------------

  measure: m_product_a_total_sales {
    view_label: "Sales and Margin - Total"
    group_label: "Product A - Sales"
    type: number
    sql:  ${total_order_product_A.m_product_sales} ;;
    value_format_name: decimal_2
  }

  measure: m_product_a_total_basket_sales {
    view_label: "Sales and Margin - Total"
    group_label: "Product A - Sales"
    type: number
    sql: ${m_basket_sales_A} ;;
    value_format_name: decimal_2
  }

  measure: m_product_a_total_rest_of_basket_sales {
    view_label: "Sales and Margin - Total"
    group_label: "Product A - Sales"
    type: number
    sql: ${m_product_a_total_basket_sales}-IFNULL(${m_product_a_total_sales},0) ;;
    value_format_name: decimal_2
  }

  measure: m_product_b_total_sales {
    view_label: "Sales and Margin - Total"
    group_label: "Product B - Sales"
    type: number
    sql:  ${total_order_product_B.m_product_sales} ;;
    value_format_name: decimal_2
  }

  measure: m_product_b_total_basket_sales {
    view_label: "Sales and Margin - Total"
    group_label: "Product B - Sales"
    type: number
    sql: ${m_basket_sales_B} ;;
    value_format_name: decimal_2
  }

  measure: m_product_b_total_rest_of_basket_sales {
    view_label: "Sales and Margin - Total"
    group_label: "Product B - Sales"
    type: number
    sql: ${m_product_b_total_basket_sales}-IFNULL(${m_product_b_total_sales},0) ;;
    value_format_name: decimal_2
  }

  # Measures --------------------------------------------------







  # Margin Metrics - Totals

  dimension: product_a_total_margin {
    view_label: "Sales and Margin - Total"
    group_label: "Product A - Margin"
    type: number
    sql: ${total_order_product_A.product_margin} ;;
    value_format_name: decimal_2
  }

  dimension: product_a_total_basket_margin {
    view_label: "Sales and Margin - Total"
    group_label: "Product A - Margin"
    type: number
    sql: ${basket_metrics_A.basket_margin} ;;
    value_format_name: decimal_2
  }

  dimension: product_a_total_rest_of_basket_margin {
    view_label: "Sales and Margin - Total"
    group_label: "Product A - Margin"
    type: number
    sql: ${product_a_total_basket_margin}-IFNULL(${product_a_total_margin},0) ;;
    value_format_name: decimal_2
  }

  dimension: product_b_total_margin {
    view_label: "Sales and Margin - Total"
    group_label: "Product B - Margin"
    type: number
    sql: ${total_order_product_B.product_margin} ;;
    value_format_name: decimal_2
  }

  dimension: product_b_total_basket_margin {
    view_label: "Sales and Margin - Total"
    group_label: "Product B - Margin"
    type: number
    sql: ${basket_metrics_B.basket_margin} ;;
    value_format_name: decimal_2
  }

  dimension: product_b_total_rest_of_basket_margin {
    view_label: "Sales and Margin - Total"
    group_label: "Product B - Margin"
    type: number
    sql: ${product_b_total_basket_margin}-IFNULL(${product_b_total_margin},0) ;;
    value_format_name: decimal_2
  }





  # Measures --------------------------------------------------

  measure: m_product_a_total_margin {
    view_label: "Sales and Margin - Total"
    group_label: "Product A - Margin"
    type: number
    sql: ${total_order_product_A.m_product_margin} ;;
    value_format_name: decimal_2
  }

  measure: m_product_a_total_basket_margin {
    view_label: "Sales and Margin - Total"
    group_label: "Product A - Margin"
    type: number
    sql: ${m_basket_margin_A} ;;
    value_format_name: decimal_2
  }

  measure: m_product_a_total_rest_of_basket_margin {
    view_label: "Sales and Margin - Total"
    group_label: "Product A - Margin"
    type: number
    sql: ${m_product_a_total_basket_margin}-IFNULL(${m_product_a_total_margin},0) ;;
    value_format_name: decimal_2
  }

  measure: m_product_b_total_margin {
    view_label: "Sales and Margin - Total"
    group_label: "Product B - Margin"
    type: number
    sql: ${total_order_product_B.m_product_margin} ;;
    value_format_name: decimal_2
  }

  measure: m_product_b_total_basket_margin {
    view_label: "Sales and Margin - Total"
    group_label: "Product B - Margin"
    type: number
    sql: ${m_basket_margin_B} ;;
    value_format_name: decimal_2
  }

  measure: m_product_b_total_rest_of_basket_margin {
    view_label: "Sales and Margin - Total"
    group_label: "Product B - Margin"
    type: number
    sql: ${m_product_b_total_basket_margin}-IFNULL(${m_product_b_total_margin},0) ;;
    value_format_name: decimal_2
  }

  # Measures --------------------------------------------------





  # Sales Metrics - Average

  dimension: product_a_average_sales {
    view_label: "Sales and Margin - Average"
    group_label: "Product A - Sales"
    type: number
    sql: 1.0*${product_a_total_sales}/${product_a_order_count} ;;
    value_format_name: decimal_2
  }

  dimension: product_a_average_basket_sales {
    view_label: "Sales and Margin - Average"
    group_label: "Product A - Sales"
    type: number
    sql: 1.0*${product_a_total_basket_sales}/${product_a_order_count} ;;
    value_format_name: decimal_2
  }

  dimension: product_a_average_rest_of_basket_sales {
    view_label: "Sales and Margin - Average"
    group_label: "Product A - Sales"
    type: number
    sql: 1.0*${product_a_total_rest_of_basket_sales}/${product_a_order_count} ;;
    value_format_name: decimal_2
  }

  dimension: product_b_average_sales {
    view_label: "Sales and Margin - Average"
    group_label: "Product B - Sales"
    type: number
    sql: 1.0*${product_b_total_sales}/${product_b_order_count} ;;
    value_format_name: decimal_2
  }

  dimension: product_b_average_basket_sales {
    view_label: "Sales and Margin - Average"
    group_label: "Product B - Sales"
    type: number
    sql: 1.0*${product_b_total_basket_sales}/${product_b_order_count} ;;
    value_format_name: decimal_2
  }

  dimension: product_b_average_rest_of_basket_sales {
    view_label: "Sales and Margin - Average"
    group_label: "Product B - Sales"
    type: number
    sql: 1.0*${product_b_total_rest_of_basket_sales}/${product_b_order_count} ;;
    value_format_name: decimal_2
  }



  # Measures --------------------------------------------------

  measure: m_product_a_average_sales {
    view_label: "Sales and Margin - Average"
    group_label: "Product A - Sales"
    type: number
    sql: 1.0*${m_product_a_total_sales}/${m_product_a_order_count} ;;
    value_format_name: decimal_2
  }

  measure: m_product_a_average_basket_sales {
    view_label: "Sales and Margin - Average"
    group_label: "Product A - Sales"
    type: number
    sql: 1.0*${m_product_a_total_basket_sales}/${m_product_a_order_count} ;;
    value_format_name: decimal_2
  }

  measure: m_product_a_average_rest_of_basket_sales {
    view_label: "Sales and Margin - Average"
    group_label: "Product A - Sales"
    type: number
    sql: 1.0*${m_product_a_total_rest_of_basket_sales}/${m_product_a_order_count} ;;
    value_format_name: decimal_2
  }

  measure: m_product_b_average_sales {
    view_label: "Sales and Margin - Average"
    group_label: "Product B - Sales"
    type: number
    sql: 1.0*${m_product_b_total_sales}/${m_product_b_order_count} ;;
    value_format_name: decimal_2
  }

  measure: m_product_b_average_basket_sales {
    view_label: "Sales and Margin - Average"
    group_label: "Product B - Sales"
    type: number
    sql: 1.0*${m_product_b_total_basket_sales}/${m_product_b_order_count} ;;
    value_format_name: decimal_2
  }

  measure: m_product_b_average_rest_of_basket_sales {
    view_label: "Sales and Margin - Average"
    group_label: "Product B - Sales"
    type: number
    sql: 1.0*${m_product_b_total_rest_of_basket_sales}/${m_product_b_order_count} ;;
    value_format_name: decimal_2
  }

  # Measures --------------------------------------------------








  # Margin Metrics - Average

  dimension: product_a_average_margin {
    view_label: "Sales and Margin - Average"
    group_label: "Product A - Margin"
    type: number
    sql: 1.0*${product_a_total_margin}/${product_a_order_count} ;;
    value_format_name: decimal_2
  }

  dimension: product_a_average_basket_margin {
    view_label: "Sales and Margin - Average"
    group_label: "Product A - Margin"
    type: number
    sql: 1.0*${product_a_total_basket_margin}/${product_a_order_count} ;;
    value_format_name: decimal_2
    drill_fields: [product_a, product_a_percent_purchased_alone]
  }

  dimension: product_a_average_rest_of_basket_margin {
    view_label: "Sales and Margin - Average"
    group_label: "Product A - Margin"
    type: number
    sql: 1.0*${product_a_total_rest_of_basket_margin}/${product_a_order_count} ;;
    value_format_name: decimal_2
    drill_fields: [product_a, product_a_percent_purchased_alone]
  }

  dimension: product_b_average_margin {
    view_label: "Sales and Margin - Average"
    group_label: "Product B - Margin"
    type: number
    sql: 1.0*${product_b_total_margin}/${product_b_order_count} ;;
    value_format_name: decimal_2
  }

  dimension: product_b_average_basket_margin {
    view_label: "Sales and Margin - Average"
    group_label: "Product B - Margin"
    type: number
    sql: 1.0*${product_b_total_basket_margin}/${product_b_order_count} ;;
    value_format_name: decimal_2
    drill_fields: [product_b, product_b_percent_purchased_alone]
  }

  dimension: product_b_average_rest_of_basket_margin {
    view_label: "Sales and Margin - Average"
    group_label: "Product B - Margin"
    type: number
    sql: 1.0*${product_b_total_rest_of_basket_margin}/${product_b_order_count} ;;
    value_format_name: decimal_2
    drill_fields: [product_b, product_b_percent_purchased_alone]
  }


  # Measures --------------------------------------------------

  measure: m_product_a_average_margin {
    view_label: "Sales and Margin - Average"
    group_label: "Product A - Margin"
    type: number
    sql: 1.0*${m_product_a_total_margin}/${m_product_a_order_count} ;;
    value_format_name: decimal_2
  }

  measure: m_product_a_average_basket_margin {
    view_label: "Sales and Margin - Average"
    group_label: "Product A - Margin"
    type: number
    sql: 1.0*${m_product_a_total_basket_margin}/${m_product_a_order_count} ;;
    value_format_name: decimal_2
    drill_fields: [product_a, product_a_percent_purchased_alone]
  }

  measure: m_product_a_average_rest_of_basket_margin {
    view_label: "Sales and Margin - Average"
    group_label: "Product A - Margin"
    type: number
    sql: 1.0*${m_product_a_total_rest_of_basket_margin}/${m_product_a_order_count} ;;
    value_format_name: decimal_2
    drill_fields: [product_a, product_a_percent_purchased_alone]
  }

  measure: m_product_b_average_margin {
    view_label: "Sales and Margin - Average"
    group_label: "Product B - Margin"
    type: number
    sql: 1.0*${m_product_b_total_margin}/${m_product_b_order_count} ;;
    value_format_name: decimal_2
  }

  measure: m_product_b_average_basket_margin {
    view_label: "Sales and Margin - Average"
    group_label: "Product B - Margin"
    type: number
    sql: 1.0*${m_product_b_total_basket_margin}/${m_product_b_order_count} ;;
    value_format_name: decimal_2
    drill_fields: [product_b, product_b_percent_purchased_alone]
  }

  measure: m_product_b_average_rest_of_basket_margin {
    view_label: "Sales and Margin - Average"
    group_label: "Product B - Margin"
    type: number
    sql: 1.0*${m_product_b_total_rest_of_basket_margin}/${m_product_b_order_count} ;;
    value_format_name: decimal_2
    drill_fields: [product_b, product_b_percent_purchased_alone]
  }

  # Measures --------------------------------------------------







  # Aggregate Measures - ONLY TO BE USED WHEN FILTERING ON AN AGGREGATE DIMENSION (E.G. BRAND_A, CATEGORY_A)

  measure: aggregated_joint_order_count {
    description: "Only use when filtering on a rollup of product items, such as brand_a or category_a"
    type: sum
    sql: ${joint_order_count} ;;
  }

}
