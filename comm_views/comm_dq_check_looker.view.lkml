view: comm_dq_check_looker {
  sql_table_name: `chb-prod-data-comm.prod_commercial.comm_dq_check_looker`
    ;;

  dimension: model {
    type: string
    sql: ${TABLE}.model ;;
  }

  dimension: primary_key {
    type: string
    sql: ${TABLE}.primary_key ;;
  }

  dimension_group: ran {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.ran_at ;;
  }

  dimension: result {
    type: string
    sql: ${TABLE}.result ;;
  }

  dimension: test {
    type: string
    sql: ${TABLE}.test ;;
  }

  measure: link_to_look{
    type: string
    label: "Link to look"
    sql: CASE when ${model} = "tryano_facts_union_new" then "https://chalhoubgroup.de.looker.com/looks/2197"
              when ${model} = "toryburch_facts_union" then "https://chalhoubgroup.de.looker.com/looks/2196"
              when ${model} = "stg_comm_swarovski_combined" then "https://chalhoubgroup.de.looker.com/looks/2195"
              when ${model} = "lvl_facts_union" then "https://chalhoubgroup.de.looker.com/looks/2193"
              when ${model} = "loccitane_facts_union" then "https://chalhoubgroup.de.looker.com/looks/2194"
              when ${model} = "faces_facts_union" then "https://chalhoubgroup.de.looker.com/looks/2192"
              END;;
  }

  measure: test_result {
    type: number
    label: "Test Result"
    sql: CASE WHEN ${result} = "FAILED" then 0
         ELSE 1
         END;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
