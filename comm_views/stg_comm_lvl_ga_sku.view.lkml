include: "../_common/period_over_period.view"

view: stg_comm_lvl_ga_sku {

  sql_table_name: `chb-prod-stage-comm.prod_comm.stg_comm_lvl_ga_sku` ;;

  dimension: brand_security{
    type: string
    hidden: yes
    sql: "LEVEL SHOES" ;;
  }

  dimension: vertical_security{
    type: string
    hidden: yes
    sql: "MANAGED COMPANIES" ;;
  }

  extends: [period_over_period]

  dimension_group: pop_no_tz {
    sql: ${date_raw} ;;
  }

  dimension: primary_key {
    hidden: yes
    primary_key: yes
    sql: CONCAT(${date_date},${product_sku},${site_country},${language} ) ;;
  }

  dimension: product_sku {
    type: string
    label: "Primary VPN"
    sql: ${TABLE}.product_sku ;;
  }

  dimension_group: date {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.date ;;
  }

  dimension: language {
    type: string
    sql: ${TABLE}.language ;;
  }

  dimension: site_country {
    type: string
    sql: ${TABLE}.site_country ;;
  }

  dimension: buy_to_detail_rate {
    hidden: yes
    type: number
    sql: ${TABLE}.buy_to_detail_rate ;;
  }

  dimension: cart_to_detail_rate {
    hidden: yes
    type: number
    sql: ${TABLE}.cart_to_detail_rate ;;
  }

  dimension: product_adds_to_cart {
    hidden: yes
    type: number
    sql: ${TABLE}.product_adds_to_cart ;;
  }

  dimension: product_checkouts {
    hidden: yes
    type: number
    sql: ${TABLE}.product_checkouts ;;
  }

  dimension: product_detail_views {
    hidden: yes
    type: number
    sql: ${TABLE}.product_detail_views ;;
  }


  measure: count {
    type: count
    drill_fields: []
  }

  measure: sum_product_adds_to_cart {
    type: sum_distinct
    label: "Product add to cart"
    sql: ${TABLE}.product_adds_to_cart ;;
  }

  measure: sum_product_checkouts {
    type: sum_distinct
    label: "Product checkouts"
    sql: ${TABLE}.product_checkouts ;;
  }

  measure: sum_product_detail_views {
    type: sum_distinct
    label: "Product detail views"
    sql: ${TABLE}.product_detail_views ;;
  }

  measure: sum_buy_to_detail_rate {
    type: number
    value_format_name: percent_2
    label: "Buy to detail rate"
    sql: ${sum_product_checkouts}/NULLIF(${sum_product_detail_views},0) ;;
  }

  measure: sum_cart_to_detail_rate {
    type: number
    value_format_name: percent_2
    label: "Cart to detail rate"
    sql: ${sum_product_adds_to_cart}/NULLIF(${sum_product_detail_views},0) ;;
  }

}
