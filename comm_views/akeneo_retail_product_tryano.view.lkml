include: "../_common/period_over_period.view"

view: akeneo_retail_product_tryano {
  sql_table_name: `chb-prod-data-comm.prod_commercial.akeneo_retail_product_tryano`
    ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: string
    sql: ${TABLE}.id ;;
    link: {
      label: "View SKU on Tryano.com (UAE)"
      url: "https://www.tryano.com/en-ae/{{ id }}.html"
    }
    ###html: <img height="100" src="https://www.tryano.com/dw/image/v2/BDDB_PRD/on/demandware.static/-/Sites-tryano-master-catalog/default/dw06f2338a/Assets/rs21_129c_ivory_/d/b/4/5/db454f29e1dc6e6f3c0d850789ffc329ca0807c9_RS21_129C_IVORY_.jpg?sw=800&sh=1040&vpn={{erpattributes__erpvpn}}">
    ###      <img height="100" src="https://www.tryano.com/dw/image/v2/BDDB_PRD/on/demandware.static/-/Sites-tryano-master-catalog/default/dw06f2338a/Assets/rs21_129c_ivory_/d/b/4/5/db454f29e1dc6e6f3c0d850789ffc329ca0807c9_RS21_129C_IVORY_.jpg?sw=800&sh=1040&vpn={{erpattributes__erpvpn}}">;;
  }

  dimension: brand_security{
    type: string
    hidden: yes
    sql: "TRYANO" ;;
  }

  dimension: vertical_security{
    type: string
    hidden: yes
    sql: "MANAGED COMPANIES" ;;
  }

  dimension: assets__productimages {
    type: string
    hidden: no
    group_label: "Product Attributes"
    label: "Product Images"
    sql: ${TABLE}.assets__productimages ;;
  }

  dimension: categories {
    type: string
    sql: ${TABLE}.categories ;;
  }

  dimension: collection {
    label: "Collection"
    description: "Collection derived on the basis of categories"
    case: {
      when: {sql: strpos(${TABLE}.categories, "women_shop_by_style_athleisure")>0 ;; label: "athleisure"}
      when: {sql: strpos(${TABLE}.categories, "women_shop_by_style_croco effect")>0 ;; label: "croco effect"}
      when: {sql: strpos(${TABLE}.categories, "women_shop_by_style_eveningwear")>0 ;; label: "eveningwear"}
      when: {sql: strpos(${TABLE}.categories, "women_shop_by_style_feminine")>0 ;; label: "feminine"}
      when: {sql: strpos(${TABLE}.categories, "women_shop_by_style_local_designers")>0 ;; label: "local_designers"}
      when: {sql: strpos(${TABLE}.categories, "women_shop_by_style_lounge_wear")>0 ;; label: "lounge_wear"}
      when: {sql: strpos(${TABLE}.categories, "women_shop_by_style_resortwear")>0 ;; label: "resortwear"}
      when: {sql: strpos(${TABLE}.categories, "women_shop_by_style_statement_bags")>0 ;; label: "statement_bags"}
      when: {sql: strpos(${TABLE}.categories, "women_shop_by_style_statement_shoes")>0 ;; label: "statement_shoes"}
      when: {sql: strpos(${TABLE}.categories, "women_shop_by_style_vibrant_colors")>0 ;; label: "vibrant_colors"}
      when: {sql: strpos(${TABLE}.categories, "women_shop_by_style_workwear")>0 ;; label: "workwear"}
      when: {sql: strpos(${TABLE}.categories, "women_shop_by_style_floral")>0 ;; label: "floral"}
      when: {sql: strpos(${TABLE}.categories, "men_shop_by_style_athleisure")>0 ;; label: "athleisure"}
      when: {sql: strpos(${TABLE}.categories, "men_shop_by_style_cool_sneakers")>0 ;; label: "sneakers"}
      when: {sql: strpos(${TABLE}.categories, "men_shop_by_style_logo_love")>0 ;; label: "logo_love"}
      when: {sql: strpos(${TABLE}.categories, "men_shop_by_style_normcore")>0 ;; label: "normcore"}
      when: {sql: strpos(${TABLE}.categories, "men_shop_by_style_resortwear")>0 ;; label: "resortwear"}
      when: {sql: strpos(${TABLE}.categories, "men_shop_by_style_vibrant_colors")>0 ;; label: "vibrant_colors"}
      when: {sql: strpos(${TABLE}.categories, "kids_shop_by_style_floral")>0 ;; label: "floral"}
      when: {sql: strpos(${TABLE}.categories, "kids_shop_by_style_activewear")>0 ;; label: "activewear"}
      when: {sql: strpos(${TABLE}.categories, "kids_shop_by_style_local_designers")>0 ;; label: "local_designers"}
      when: {sql: strpos(${TABLE}.categories, "kids_shop_by_style_logo_love")>0 ;; label: "logo_love"}
      when: {sql: strpos(${TABLE}.categories, "kids_shop_by_style_occasionwear")>0 ;; label: "occasionwear"}
      when: {sql: strpos(${TABLE}.categories, "kids_shop_by_style_rainbow_colors")>0 ;; label: "rainbow_colors"}
      when: {sql: strpos(${TABLE}.categories, "kids_shop_by_style_school_essentials")>0 ;; label: "school_essentials"}
      when: {sql: strpos(${TABLE}.categories, "beauty_shop_by_style_anti_aging")>0 ;; label: "anti_aging"}
      when: {sql: strpos(${TABLE}.categories, "beauty_shop_by_style_arabic_scents")>0 ;; label: "arabic_scents"}
      when: {sql: strpos(${TABLE}.categories, "beauty_shop_by_style_aromatherapy")>0 ;; label: "aromatherapy"}
      when: {sql: strpos(${TABLE}.categories, "beauty_shop_by_style_clean_beauty")>0 ;; label: "clean_beauty"}
      when: {sql: strpos(${TABLE}.categories, "beauty_shop_by_style_cult_favorites")>0 ;; label: "cult_favorites"}
      when: {sql: strpos(${TABLE}.categories, "beauty_shop_by_style_detox")>0 ;; label: "detox"}
      when: {sql: strpos(${TABLE}.categories, "beauty_shop_by_style_mosturizers")>0 ;; label: "moisturizers"}
      when: {sql: strpos(${TABLE}.categories, "beauty_shop_by_style_the_oud_mood")>0 ;; label: "oud_mood"}
      when: {sql: strpos(${TABLE}.categories, "beauty_shop_by_style_suncare")>0 ;; label: "suncare"}
      when: {sql: strpos(${TABLE}.categories, "beauty_shop_by_style_sunkissed_glow")>0 ;; label: "sunkissed_glow"}
      when: {sql: strpos(${TABLE}.categories, "beauty_shop_by_style_skincare_routine")>0 ;; label: "skincare_routine"}
    else: "unknown"
    }
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created ;;
  }

  dimension: enabled {
    type: yesno
    sql: ${TABLE}.enabled ;;
  }

  dimension: family {
    type: string
    hidden: yes
    sql: ${TABLE}.family ;;
  }

  dimension: groups_ {
    type: string
    hidden: yes
    sql: ${TABLE}.groups_ ;;
  }

  dimension: parent {
    type: string
    sql: ${TABLE}.parent ;;
  }

###################################
### ERP Attributes              ###
###################################{

  dimension: erpattributes__erpbabyjunior {
    type: string
    group_label: "ERP Attributes"
    label: "ERP Kids Type Name"
    sql: ${TABLE}.erpattributes__erpbabyjunior ;;
  }

  dimension: erpattributes__erpbabyjuniorcode {
    type: string
    group_label: "ERP Attributes"
    label: "ERP Kids Type Code"
    sql: ${TABLE}.erpattributes__erpbabyjuniorcode ;;
  }

  dimension: erpattributes__erpbarcode {
    type: string
    group_label: "ERP Attributes"
    label: "ERP Product Barcode"
    sql: ${TABLE}.erpattributes__erpbarcode ;;
  }

  dimension: erpattributes__erpbrand {
    type: string
    group_label: "ERP Attributes"
    label: "ERP Brand Name"
    sql: ${TABLE}.erpattributes__erpbrand ;;
  }

  dimension: erpattributes__erpbrandcode {
    type: string
    group_label: "ERP Attributes"
    label: "ERP Brand Code"
    sql: ${TABLE}.erpattributes__erpbrandcode ;;
  }

  dimension: erpattributes__erpclassification {
    type: string
    group_label: "ERP Attributes"
    label: "ERP Classification Name"
    sql: ${TABLE}.erpattributes__erpclassification ;;
  }

  dimension: erpattributes__erpclassificationcode {
    type: string
    group_label: "ERP Attributes"
    label: "ERP Classification Code"
    sql: ${TABLE}.erpattributes__erpclassificationcode ;;
  }

  dimension: erpattributes__erpcolor {
    type: string
    group_label: "ERP Attributes"
    label: "ERP Color Name"
    sql: ${TABLE}.erpattributes__erpcolor ;;
  }

  dimension: erpattributes__erpcolorcode {
    type: string
    group_label: "ERP Attributes"
    label: "ERP Color Code"
    sql: ${TABLE}.erpattributes__erpcolorcode ;;
  }

  dimension: erpattributes__erpconsignmentflag {
    type: string
    group_label: "ERP Attributes"
    label: "ERP Consigment Flag"
    sql: ${TABLE}.erpattributes__erpconsignmentflag ;;
  }

  dimension: erpattributes__erpcountryoforigin {
    type: string
    group_label: "ERP Attributes"
    label: "ERP Country of Origin"
    sql: ${TABLE}.erpattributes__erpcountryoforigin ;;
  }

  dimension: erpattributes__erpdivision {
    type: string
    group_label: "ERP Attributes"
    label: "ERP Division Name"
    sql: ${TABLE}.erpattributes__erpdivision ;;
  }

  dimension: erpattributes__erpgender {
    type: string
    group_label: "ERP Attributes"
    label: "ERP Gender"
    sql: ${TABLE}.erpattributes__erpgender ;;
  }

  dimension: erpattributes__erpgendercode {
    type: string
    group_label: "ERP Attributes"
    label: "ERP Gender Code"
    sql: ${TABLE}.erpattributes__erpgendercode ;;
  }

  dimension: erpattributes__erpheelhight {
    type: string
    group_label: "ERP Attributes"
    label: "ERP Heel Height"
    sql: ${TABLE}.erpattributes__erpheelhight ;;
  }

  dimension: erpattributes__erplastupdatedatetime {
    type: string
    group_label: "ERP Attributes"
    label: "ERP Last Updated Date/Time"
    sql: ${TABLE}.erpattributes__erplastupdatedatetime ;;
  }

  dimension: erpattributes__erpmaterial {
    type: string
    group_label: "ERP Attributes"
    label: "ERP Material"
    sql: ${TABLE}.erpattributes__erpmaterial ;;
  }

  dimension: erpattributes__erpmaterialcomposition {
    type: string
    group_label: "ERP Attributes"
    label: "ERP Material Composition"
    sql: ${TABLE}.erpattributes__erpmaterialcomposition ;;
  }

  dimension: erpattributes__erporaclestyle {
    type: string
    group_label: "ERP Attributes"
    label: "ERP Oracle Style"
    sql: ${TABLE}.erpattributes__erporaclestyle ;;
  }

  dimension: erpattributes__erpproductname {
    type: string
    group_label: "ERP Attributes"
    label: "ERP Product Name"
    sql: ${TABLE}.erpattributes__erpproductname ;;
  }

  dimension: erpattributes__erpproductwebsites {
    type: string
    group_label: "ERP Attributes"
    label: "ERP Product Websites"
    sql: ${TABLE}.erpattributes__erpproductwebsites ;;
  }

  dimension: erpattributes__erprecurrence {
    type: string
    group_label: "ERP Attributes"
    label: "ERP Recurrance Desc"
    sql: ${TABLE}.erpattributes__erprecurrence ;;
  }

  dimension: erpattributes__erprecurrencecode {
    type: string
    group_label: "ERP Attributes"
    label: "ERP Recurrance Code"
    sql: ${TABLE}.erpattributes__erprecurrencecode ;;
  }

  dimension: erpattributes__erpseason {
    type: string
    group_label: "ERP Attributes"
    label: "ERP Season"
    sql: ${TABLE}.erpattributes__erpseason ;;
  }

  dimension: erpattributes__erpstandardsize {
    type: string
    group_label: "ERP Attributes"
    label: "ERP Standard Size"
    sql: ${TABLE}.erpattributes__erpstandardsize ;;
  }

  dimension: erpattributes__erpsuppliercolourname {
    type: string
    group_label: "ERP Attributes"
    label: "ERP Supplier Color Name"
    sql: ${TABLE}.erpattributes__erpsuppliercolourname ;;
  }

  dimension: erpattributes__erpsuppliersize {
    type: string
    group_label: "ERP Attributes"
    label: "ERP Supplier Size"
    sql: ${TABLE}.erpattributes__erpsuppliersize ;;
  }

  dimension: erpattributes__erptaxonomy1 {
    type: string
    group_label: "ERP Attributes"
    label: "ERP Taxonomy 1 Name"
    sql: ${TABLE}.erpattributes__erptaxonomy1 ;;
  }

  dimension: erpattributes__erptaxonomy1code {
    type: string
    group_label: "ERP Attributes"
    label: "ERP Taxonomy 1 Code"
    sql: ${TABLE}.erpattributes__erptaxonomy1code ;;
  }

  dimension: erpattributes__erptaxonomy2 {
    type: string
    group_label: "ERP Attributes"
    label: "ERP Taxonomy 2 Name"
    sql: ${TABLE}.erpattributes__erptaxonomy2 ;;
  }

  dimension: erpattributes__erptaxonomy2code {
    type: string
    group_label: "ERP Attributes"
    label: "ERP Taxonomy 2 Code"
    sql: ${TABLE}.erpattributes__erptaxonomy2code ;;
  }

  dimension: erpattributes__erpunitretailaed {
    type: string
    group_label: "ERP Attributes"
    label: "Unit Price AED"
    sql: ${TABLE}.erpattributes__erpunitretailaed ;;
  }

  dimension: erpattributes__erpunitretailbhr {
    type: string
    group_label: "ERP Attributes"
    label: "Unit Price BHR"
    sql: ${TABLE}.erpattributes__erpunitretailbhr ;;
  }

  dimension: erpattributes__erpunitretailint {
    type: string
    group_label: "ERP Attributes"
    label: "Unit Price INT"
    sql: ${TABLE}.erpattributes__erpunitretailint ;;
  }

  dimension: erpattributes__erpunitretailkwt {
    type: string
    group_label: "ERP Attributes"
    label: "Unit Price KWT"
    sql: ${TABLE}.erpattributes__erpunitretailkwt ;;
  }

  dimension: erpattributes__erpunitretailqat {
    type: string
    group_label: "ERP Attributes"
    label: "Unit Price QAT"
    sql: ${TABLE}.erpattributes__erpunitretailqat ;;
  }

  dimension: erpattributes__erpunitretailsar {
    type: string
    group_label: "ERP Attributes"
    label: "Unit Price SAR"
    sql: ${TABLE}.erpattributes__erpunitretailsar ;;
  }

  dimension: erpattributes__erpusagespecificity {
    type: string
    group_label: "ERP Attributes"
    label: "ERP Usage Specificity"
    sql: ${TABLE}.erpattributes__erpusagespecificity ;;
  }

  dimension: erpattributes__erpvpn {
    type: string
    group_label: "ERP Attributes"
    label: "ERP Vendor Product Number (VPN)"
    sql: ${TABLE}.erpattributes__erpvpn ;;
  }

  dimension: erpattributes__erpzonecode {
    type: string
    group_label: "ERP Attributes"
    label: "ERP Zone Code"
    sql: ${TABLE}.erpattributes__erpzonecode ;;
  }

  dimension: erpattributes__erpzonedesc {
    type: string
    group_label: "ERP Attributes"
    label: "ERP Zone Description"
    sql: ${TABLE}.erpattributes__erpzonedesc ;;
  }

  dimension: erpattributes__sku {
    type: string
    group_label: "ERP Attributes"
    label: "SKU"
    sql: ${TABLE}.erpattributes__sku ;;
  }

  ### End of ERP Attributes
  #}


###################################
### Product Attributes          ###
###################################{

  dimension: brand_department_url {
    type: string
    group_label: "Product Attributes"
    label: "Brand (Department) URL"
    sql: lower(concat(${productattributes__department},"-shop-by-designer-",replace(${TABLE}.productattributes__brand," ","-"))) ;;
    link: {
      label: "View Brand on Tryano.com (UAE)"
      url: "https://www.tryano.com/en-ae/{{ brand_department_url }}"
    }
  }

  dimension: productattributes__brand {
    type: string
    group_label: "Product Attributes"
    label: "Brand"
    sql: ${TABLE}.productattributes__brand ;;
  }

  dimension: productattributes__businesstype {
    type: string
    group_label: "Product Attributes"
    label: "Business Type"
    sql: ${TABLE}.productattributes__businesstype ;;
  }

  dimension: productattributes__chalhoubid {
    type: string
    group_label: "Product Attributes"
    label: "Chalhoub ID"
    sql: ${TABLE}.productattributes__chalhoubid ;;
  }

  dimension: productattributes__color {
    type: string
    group_label: "Product Attributes"
    label: "Variant Color"
    sql: ${TABLE}.productattributes__color ;;
  }

  dimension: productattributes__department {
    type: string
    group_label: "Product Attributes"
    label: "Department"
    sql: ${TABLE}.productattributes__department ;;
  }

  dimension: productattributes__discountpercentage {
    type: string
    group_label: "Product Attributes"
    label: "Discount Percentage"
    sql: ${TABLE}.productattributes__discountpercentage ;;
  }

  dimension: productattributes__displaycolor {
    type: string
    group_label: "Product Attributes"
    label: "Display Color"
    sql: ${TABLE}.productattributes__displaycolor ;;
  }

  dimension: productattributes__gender {
    type: string
    group_label: "Product Attributes"
    label: "Gender"
    sql: ${TABLE}.productattributes__gender ;;
  }

  dimension: productattributes__genericbarcode {
    type: string
    group_label: "Product Attributes"
    label: "Generic Barcode"
    sql: ${TABLE}.productattributes__genericbarcode ;;
  }

  dimension: productattributes__hairtype {
    type: string
    group_label: "Product Attributes"
    label: "Hair Type"
    sql: ${TABLE}.productattributes__hairtype ;;
  }

  dimension: productattributes__hscode {
    type: string
    group_label: "Product Attributes"
    label: "HS Code"
    sql: ${TABLE}.productattributes__hscode ;;
  }

  dimension: productattributes__kidsgender {
    type: string
    group_label: "Product Attributes"
    label: "Kids Type"
    sql: ${TABLE}.productattributes__kidsgender ;;
  }

  dimension: productattributes__pdpsize {
    type: string
    group_label: "Product Attributes"
    label: "PDP Size"
    sql: ${TABLE}.productattributes__pdpsize ;;
  }

  dimension: productattributes__perfumetype {
    type: string
    group_label: "Product Attributes"
    label: "Perfume Type"
    sql: ${TABLE}.productattributes__perfumetype ;;
  }

  dimension: productattributes__perfumetype_ar {
    type: string
    group_label: "Product Attributes"
    label: "Perfume Type (in Arabic)"
    sql: ${TABLE}.productattributes__perfumetype_ar ;;
  }

  dimension: productattributes__plpsize {
    type: string
    group_label: "Product Attributes"
    label: "PLP Size"
    sql: ${TABLE}.productattributes__plpsize ;;
  }

  dimension: productattributes__productbadge {
    type: string
    group_label: "Product Attributes"
    label: "Product badge"
    sql: ${TABLE}.productattributes__productbadge ;;
  }

  dimension: productattributes__productcategory {
    type: string
    group_label: "Product Attributes"
    label: "Category"
    sql: ${TABLE}.productattributes__productcategory ;;
  }

  dimension: productattributes__productdescreference {
    type: string
    group_label: "Product Attributes"
    label: "Product Desc Reference"
    sql: ${TABLE}.productattributes__productdescreference ;;
  }

  dimension: productattributes__productdescription {
    type: string
    group_label: "Product Attributes"
    label: "Product Description"
    sql: ${TABLE}.productattributes__productdescription ;;
  }

  dimension: productattributes__productdescription_ar {
    type: string
    group_label: "Product Attributes"
    label: "Product Description (in Arabic)"
    sql: ${TABLE}.productattributes__productdescription_ar ;;
  }

  dimension: productattributes__productname {
    type: string
    group_label: "Product Attributes"
    label: "Product Name"
    sql: ${TABLE}.productattributes__productname ;;
  }

  dimension: productattributes__productname_ar {
    type: string
    group_label: "Product Attributes"
    label: "Product Name (in Arabic)"
    sql: ${TABLE}.productattributes__productname_ar ;;
  }

  dimension: productattributes__productoccasion {
    type: string
    group_label: "Product Attributes"
    label: "Occasion"
    sql: ${TABLE}.productattributes__productoccasion ;;
  }

  dimension: productattributes__productshortdescription {
    type: string
    group_label: "Product Attributes"
    label: "Product Short Description"
    sql: ${TABLE}.productattributes__productshortdescription ;;
  }

  dimension: productattributes__productshortdescription_ar {
    type: string
    group_label: "Product Attributes"
    label: "Product Short Description (in Arabic)"
    sql: ${TABLE}.productattributes__productshortdescription_ar ;;
  }

  dimension: productattributes__producttype {
    type: string
    group_label: "Product Attributes"
    label: "Product Type"
    sql: ${TABLE}.productattributes__producttype ;;
  }

  dimension: productattributes__sale {
    type: string
    group_label: "Product Attributes"
    label: "Sale"
    sql: ${TABLE}.productattributes__sale ;;
  }

  dimension: productattributes__season {
    type: string
    group_label: "Product Attributes"
    label: "Season"
    sql: ${TABLE}.productattributes__season ;;
  }

  dimension: productattributes__sizechartid {
    type: string
    group_label: "Product Attributes"
    label: "Size Chart ID"
    sql: ${TABLE}.productattributes__sizechartid ;;
  }

  dimension: productattributes__skintype {
    type: string
    group_label: "Product Attributes"
    label: "Skin Type"
    sql: ${TABLE}.productattributes__skintype ;;
  }

  dimension: productattributes__stylefilter {
    type: string
    group_label: "Product Attributes"
    label: "Style"
    sql: ${TABLE}.productattributes__stylefilter ;;
  }

  dimension: productattributes__subcategory {
    type: string
    group_label: "Product Attributes"
    label: "Subcategory"
    sql: ${TABLE}.productattributes__subcategory ;;
  }

  dimension: productattributes__sunglassesframecolor {
    type: string
    group_label: "Product Attributes"
    label: "Sunglasses Frame Color"
    sql: ${TABLE}.productattributes__sunglassesframecolor ;;
  }

  dimension: productattributes__sunglasseslenscolor {
    type: string
    group_label: "Product Attributes"
    label: "Sunglass Lens Color"
    sql: ${TABLE}.productattributes__sunglasseslenscolor ;;
  }

  dimension: productattributes__sunglassespolarised {
    type: string
    group_label: "Product Attributes"
    label: "Sunglasses Polarized"
    sql: ${TABLE}.productattributes__sunglassespolarised ;;
  }

  dimension: productattributes__sunglassesshape {
    type: string
    group_label: "Product Attributes"
    label: "Sunglasses Shape"
    sql: ${TABLE}.productattributes__sunglassesshape ;;
  }

  dimension: productattributes__variantsize {
    type: string
    group_label: "Product Attributes"
    label: "Variant Size"
    sql: ${TABLE}.productattributes__variantsize ;;
  }

  dimension: productattributes__looker_label_sku {
    type: string
    group_label: "Product Attributes"
    label: "Looker Label SKU"
    sql: ${TABLE}.productattributes__looker_label_sku ;;
  }

  dimension: productattributes__looker_label_vpn {
    type: string
    group_label: "Product Attributes"
    label: "Looker Label VPN"
    sql: ${TABLE}.productattributes__looker_label_vpn ;;
  }

  ### End of Product Attributes
  #}

###################################
### Product Status              ###
###################################{

  dimension: productstatus__bhronlineflag {
    type: string
    group_label: "Product Status"
    label: "Online Flag BHR"
    sql: ${TABLE}.productstatus__bhronlineflag ;;
  }

  dimension: productstatus__dgcode {
    type: string
    group_label: "Product Status"
    label: "Is Dangerous Good"
    sql: ${TABLE}.productstatus__dgcode ;;
  }

  dimension: productstatus__intonlineflag {
    type: string
    group_label: "Product Status"
    label: "Online Flag INT"
    sql: ${TABLE}.productstatus__intonlineflag ;;
  }

  dimension: productstatus__ksaonlineflag {
    type: string
    group_label: "Product Status"
    label: "Online Flag KSA"
    sql: ${TABLE}.productstatus__ksaonlineflag ;;
  }

  dimension: productstatus__kwtonlineflag {
    type: string
    group_label: "Product Status"
    label: "Online Flag KWT"
    sql: ${TABLE}.productstatus__kwtonlineflag ;;
  }

  dimension: productstatus__productstatusset {
    type: string
    group_label: "Product Status"
    label: "Product Status Set"
    sql: ${TABLE}.productstatus__productstatusset ;;
  }

  dimension: productstatus__qatonlineflag {
    type: string
    group_label: "Product Status"
    label: "Online Flag QAT"
    sql: ${TABLE}.productstatus__qatonlineflag ;;
  }

  dimension: productstatus__uaeonlineflag {
    type: string
    group_label: "Product Status"
    label: "Online Flag UAE"
    sql: ${TABLE}.productstatus__uaeonlineflag ;;
  }

    ### End of Product Status
  #}

  dimension_group: online {
    type: time
    timeframes: [
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: cast(${TABLE}.productstatus__onlinedate as timestamp) ;;
  }

  ###################################
### SFCC Attributes               ###
###################################{

  dimension: sfcc_attributes__brandcategoryid {
    type: string
    group_label: "SFCC Attributes"
    label: "Brand Category ID"
    sql: ${TABLE}.sfcc_attributes__brandcategoryid ;;
  }

  dimension: sfcc_attributes__googleagegroup {
    type: string
    group_label: "SFCC Attributes"
    label: "Google Age Group"
    sql: ${TABLE}.sfcc_attributes__googleagegroup ;;
  }

  dimension: sfcc_attributes__googleproductcategory {
    type: string
    group_label: "SFCC Attributes"
    label: "Google Product Category"
    sql: ${TABLE}.sfcc_attributes__googleproductcategory ;;
  }

  dimension: sfcc_attributes__googleproducttype {
    type: string
    group_label: "SFCC Attributes"
    label: "Google Product Type"
    sql: ${TABLE}.sfcc_attributes__googleproducttype ;;
  }

  dimension: sfcc_attributes__urlbrand {
    type: string
    group_label: "SFCC Attributes"
    label: "URL Brand"
    sql: ${TABLE}.sfcc_attributes__urlbrand ;;
  }

  dimension: sfcc_attributes__urlcategory {
    type: string
    group_label: "SFCC Attributes"
    label: "URL Category"
    sql: ${TABLE}.sfcc_attributes__urlcategory ;;
  }

  dimension: sfcc_attributes__urldepartment {
    type: string
    group_label: "SFCC Attributes"
    label: "URL Department"
    sql: ${TABLE}.sfcc_attributes__urldepartment ;;
  }

  ### End of SFCC Attr
  #}

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated ;;
  }


  ###################################
### Enrichment level              ###
###################################{


  ## Name

  dimension: product_name_ar_length {
    type: number
    group_label: "Enrichment level"
    label: "Arabic - Product Name - Length"
    sql: length(${TABLE}.productattributes__productname_ar) ;;
  }

  dimension: product_name_ar_yesno {
    type: number
    group_label: "Enrichment level"
    label: "Arabic - Product Name - Yes / No"
    sql: if(${product_name_ar_length}>0,"Yes","No") ;;
  }

  dimension: product_name_en_length {
    type: number
    group_label: "Enrichment level"
    label: "English - Product Name - Length"
    sql: length(${TABLE}.productattributes__productname) ;;
  }

  dimension: product_name_en_yesno {
    type: number
    group_label: "Enrichment level"
    label: "English - Product Name - Yes / No"
    sql: if(${product_name_en_length}>0,"Yes","No") ;;
  }



  ## Description

  dimension: product_description_ar_length {
    type: number
    group_label: "Enrichment level"
    label: "Arabic - Product Description - Length"
    sql: length(${TABLE}.productattributes__productdescription_ar) ;;
  }

  dimension: product_description_ar_yesno {
    type: number
    group_label: "Enrichment level"
    label: "Arabic - Product Description - Yes / No"
    sql: if(${product_description_ar_length}>0,"Yes","No") ;;
  }

  dimension: product_description_en_length {
    type: number
    group_label: "Enrichment level"
    label: "English - Product Description - Length"
    sql: length(${TABLE}.productattributes__productdescription) ;;
  }

  dimension: product_description_en_yesno {
    type: number
    group_label: "Enrichment level"
    label: "English - Product Description - Yes / No"
    sql: if(${product_description_en_length}>0,"Yes","No") ;;
  }



  ## Measures

  measure: product_name_ar_avg_enrichment {
    type: average
    value_format_name: percent_2
    group_label: "Enrichment level"
    label: "Arabic - Product name - Avg enrichment"
    sql: if(${product_name_ar_length}>0,1,0) ;;
  }

  measure: product_name_en_avg_enrichment {
    type: average
    value_format_name: percent_2
    group_label: "Enrichment level"
    label: "English - Product name - Avg enrichment"
    sql: if(${product_name_en_length}>0,1,0) ;;
  }

  measure: product_description_ar_avg_enrichment {
    type: average
    value_format_name: percent_2
    group_label: "Enrichment level"
    label: "Arabic - Product description - Avg enrichment"
    sql: if(${product_description_ar_length}>0,1,0) ;;
  }

  measure: product_description_en_avg_enrichment {
    type: average
    value_format_name: percent_2
    group_label: "Enrichment level"
    label: "English - Product description - Avg enrichment"
    sql: if(${product_description_en_length}>0,1,0) ;;
  }

  ### End of Enrichment level
  #}

  measure: count_vpn  {
    type: count_distinct
    sql: ${erpattributes__erpvpn};;
  }

  measure: count {
    type: count
    drill_fields: [id, erpattributes__erpsuppliercolourname, erpattributes__erpproductname, productattributes__productname]
  }
}
