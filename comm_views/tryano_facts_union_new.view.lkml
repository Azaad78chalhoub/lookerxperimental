include: "../_common/period_over_period.view"
include: "../customer_views/ecomm_customer_new_returning.view"


view: tryano_facts_union_new {
  sql_table_name: `chb-prod-data-comm.prod_commercial.tryano_facts_union_new`
    ;;

  dimension: measure_label {
    label: "Measure Name"
    description: "Dummy dimensions shwoing measure label (used to transpose table)"
    case: {
      when: {
        label: "# of Sessions"
        sql: 1=1;;
      }
      when: {
        label: "# Orders"
        sql: 1=1;;
      }
      when: {
        label: "Conversion Rate (%)"
        sql: 1=1;;
      }
      when: {
        label: "Units Ordered"
        sql: 1=1;;
      }
      when: {
        label: "UPT"
        sql: 1=1;;
      }
      when: {
        label: "Average Order Value (USD)"
        sql: 1=1;;
      }
      when: {
        label: "Gross Revenue (USD)"
        sql: 1=1;;
      }
      when: {
        label: "# Cancelled Orders"
        sql: 1=1;;
      }
      when: {
        label: "Cancelled Rate %"
        sql: 1=1;;
      }
      when: {
        label: "Gross Revenue w/o cancellations (USD)"
        sql: 1=1;;
      }
      when: {
        label: "# Returned Orders"
        sql: 1=1;;
      }
      when: {
        label: "Returned Order % (in value)"
        sql: 1=1;;
      }
      when: {
        label: "Gross Revenue w/o cancellations & returns (USD)"
        sql: 1=1;;
      }
      when: {
        label: "Total Ad Spend (USD)"
        sql: 1=1;;
      }
      when: {
        label: "ROAS (on Gross Rev)"
        sql: 1=1;;
      }
      when: {
        label: "CPO"
        sql: 1=1;;
      }
      when: {
        label: "Customer Unique Count"
        sql: 1=1;;
      }
      when: {
        label: "Budget R0 (USD)"
        sql: 1=1;;
      }
      when: {
        label: "Budget R1 (USD)"
        sql: 1=1;;
      }
      when: {
        label: "Budget R2 (USD)"
        sql: 1=1;;
      }
    }
  }

  dimension: measure_label_group {
    label: "Group Measure Name"
    description: "Dummy dimensions shwoing measure label (used to transpose table) - for group report"
    case: {
      when: {
        label: "# of Sessions"
        sql: 1=1;;
      }
      when: {
        label: "# Orders"
        sql: 1=1;;
      }
      when: {
        label: "Conversion Rate (%)"
        sql: 1=1;;
      }
      when: {
        label: "Units Ordered"
        sql: 1=1;;
      }
      when: {
        label: "UPT"
        sql: 1=1;;
      }
      when: {
        label: "USP (USD)"
        sql: 1=1 ;;
      }
      when: {
        label: "Average Order Value (USD)"
        sql: 1=1;;
      }
      when: {
        label: "Gross Revenue (USD)"
        sql: 1=1;;
      }
      when: {
        label: "# Cancelled Orders"
        sql: 1=1;;
      }
      when: {
        label: "Cancelled Rate %"
        sql: 1=1;;
      }
      when: {
        label: "Gross Revenue w/o cancellations (USD)"
        sql: 1=1;;
      }
      when: {
        label: "# Returned Orders"
        sql: 1=1;;
      }
      when: {
        label: "Returned Order % (in value)"
        sql: 1=1;;
      }
      when: {
        label: "Gross Revenue w/o cancellations & returns (USD)"
        sql: 1=1;;
      }
      when: {
        label: "Shipped Net (USD)"
        sql: 1=1 ;;
      }
      when: {
        label: "Total Ad Spend (USD)"
        sql: 1=1;;
      }
      when: {
        label: "ROAS (on Gross Rev)"
        sql: 1=1;;
      }
      when: {
        label: "CPO"
        sql: 1=1;;
      }
      when: {
        label: "Customer Unique Count"
        sql: 1=1;;
      }
      when: {
        label: "Budget R0 (USD)"
        sql: 1=1;;
      }
      when: {
        label: "Budget R1 (USD)"
        sql: 1=1;;
      }
      when: {
        label: "Budget R2 (USD)"
        sql: 1=1;;
      }
    }
  }

  dimension: date_group {
    label: "Date Group"
    hidden: yes
    sql: {% if tryano.common_date._in_query %} CAST(${TABLE}.common_date  AS DATE)
      {% elsif tryano.common_month._in_query %} FORMAT_DATE('%Y-%m', ${TABLE}.common_date )
      {% elsif tryano.common_quarter._in_query %} (FORMAT_TIMESTAMP('%Y-%m', TIMESTAMP_TRUNC(CAST(${TABLE}.common_date AS TIMESTAMP), QUARTER)))
      {% elsif tryano.common_week._in_query %} CONCAT(CAST(EXTRACT(YEAR FROM ${TABLE}.common_date ) AS STRING),'-', CAST(EXTRACT(WEEK(SUNDAY) FROM ${TABLE}.common_date) AS STRING))
      {% elsif tryano.common_year._in_query %} EXTRACT(YEAR FROM ${TABLE}.common_date )
      {% else %} CAST(${TABLE}.common_date  AS DATE)
      {% endif %} ;;
  }

  dimension: brand_security{
    type: string
    hidden: yes
    sql: ${TABLE}.brand ;;
  }

  dimension: vertical_security{
    type: string
    hidden: yes
    sql: ${TABLE}.vertical ;;
  }

  dimension: country_security{
    type: string
    hidden: yes
    sql: ${TABLE}.bu_country ;;
  }

  extends: [period_over_period]

  dimension_group: pop_no_tz {
    sql: ${common_raw} ;;
  }

  dimension: primary_key {
    type: number
    primary_key: yes
    hidden: yes
    sql: ${TABLE}.primary_key ;;
  }

  dimension_group: common {
    type: time
    timeframes: [
      raw,
      date,
      week,
      time,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: timestamp
    label: "Date"
    description: "Date when a specific even (Record Type) took place"
    sql: cast(${TABLE}.common_datetime as timestamp) ;;
  }

  dimension: business_week_no{
    type: number
    description: "Week No. extracted from the business date"
    sql: EXTRACT( WEEK FROM(PARSE_DATE('%Y-%m-%d', CAST(${TABLE}.common_datetime AS STRING))))+1 ;;
  }

  dimension: country {
    type: string
    map_layer_name: countries
    description: "Store Country or Country in which the marketing campaign was run"
    sql: ${TABLE}.country ;;
  }

  dimension: combined_country {
    type: string
    label: "Country Combined"
    description: "Shows the Order Shipment Country if available, else the Ecom store country"
    sql: case when ifnull(${TABLE}.shipment_country, '') not in ('AE', 'KW', 'QA', 'BH', 'SA') then ${TABLE}.shipment_country else ${TABLE}.country end;;
  }

  dimension: source {
    type: string
    sql: ${TABLE}.source ;;
  }

  dimension_group: order_date {
    type: time
    timeframes: [
      raw,
      date,
      week,
      time,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: timestamp
    label: "Sales Order"
    description: "Order creation datetime"
    sql: cast(${TABLE}.order_datetime as timestamp) ;;
  }

#   dimension: order_date {
#     type: date
#     datatype: date
#     group_label: "Sales Order"
#     sql: ${TABLE}.order_date ;;
#   }



###################################
### Sales Order dimensions ###
###################################{

  dimension: increment_id {
    type: string
    label: "Sales Order ID"
    description: "Order unique identifier"
    group_label: "Sales Order"
    sql: ${TABLE}.increment_id ;;
    link: {
      label: "View Order in OMS"
      url: "https://trn.retailunity.com/cpanel/cpanel_includes/inc_datatables/orders/orders_all.php7?action=edit&id={{tryano.entity_id._value}}"
    }
  }


  dimension: status {
    type: string
    label: "Status"
    description: "Order status from Magento"
    group_label: "Sales Order"
    sql: ${TABLE}.status ;;
  }

  dimension: order_currency {
    type: string
    label: "Order Currency"
    group_label: "Sales Order"
    description: "Local currency in which the Order was paid"
    sql: ${TABLE}.order_currency ;;
  }

  dimension: record_type {
    type: string
    label: "Record Type"
    group_label: "Sales Order"
    description: "Categorizes the Order record. Shows either SALE, CANCELLED, REFUND, SHIPMENT"
    sql: ${TABLE}.record_type ;;
  }


  dimension: price {
    type: number
    label: "Item Price (Local)"
    description: "Item price in local currency"
    group_label: "Sales Order"
    sql: ${TABLE}.price ;;
  }

  dimension: price_usd {
    type: number
    label: "Item Price (USD)"
    description: "Item price in USD"
    group_label: "Sales Order"
    sql: ${TABLE}.price_usd ;;
  }

  dimension: entity_id {
    type: number
    hidden: yes
    sql: ${TABLE}.entity_id ;;
  }


  dimension: payment_method {
    type: string
    group_label: "Sales Order"
    description: "Order Payment Order"
    sql: ${TABLE}.payment_method ;;
  }

  dimension: unit_cost {
    type: number
    value_format: "#,##0.00"
    group_label: "Sales Order"
    description: "Item unit cost in local currency"
    #hidden: yes
    sql: ${TABLE}.unit_cost ;;
  }

  dimension: unit_cost_usd {
    type: number
    value_format: "$#,##0.00"
    group_label: "Sales Order"
    description: "Item unit cost in USD"
    #hidden: yes
    sql: ${TABLE}.unit_cost_usd ;;
  }

  dimension: org_num {
    type: number
    hidden: yes
    sql: ${TABLE}.org_num ;;
  }

  # dimension: coupon_code {
  #   type: string
  #   group_label: "Sales Order"
  #   sql: ${TABLE}.coupon_code ;;
  # }

  # dimension: coupon_rule_name {
  #   type: string
  #   group_label: "Sales Order"
  #   label: "Coupon Desc"
  #   sql: ${TABLE}.coupon_rule_name ;;
  # }

  dimension: xstore_invoice_number {
    type: string
    group_label: "Sales Order"
    sql: ${TABLE}.xstore_invoice_number ;;
  }

  dimension: sales_origin_internal_id {
    type: string
    group_label: "Sales Order"
    label: "Sales Origin Store ID"
    sql: ${TABLE}.sales_origin_internal_id ;;
  }

  dimension: final_record_status {
    type: string
    label: "Final Record Status"
    hidden: no
    group_label: "Sales Order"
    sql: ${TABLE}.final_record_status ;;
  }

  dimension: giftwrapping {
    type: string
    label: "Gift Wrapping (Y/N)"
    description: "Shows Y (Yes) if a Gift Wrapping service item was part of the order, else N (No)"
    group_label: "Sales Order"
    sql: ${TABLE}.giftwrapping ;;
  }


  ### End of Sales Order dimensions
  #}

###################################
##  Sales Order Item Dimensions  ##
###################################{


  dimension: detail_id {
    type: string
    label: "Detail ID"
    description: "ID of each individual item sold"
    group_label: "Sales Order Item"
    sql: cast(${TABLE}.detail_id as int64) ;;
  }


  dimension: detail_status {
    type: string
    label: "Item Status"
    group_label: "Sales Order Item"
    sql: ${TABLE}.status ;;
  }

  dimension: sku {
    type: string
    hidden: yes
    label: "SKU"
    description: "Item SKU number"
    group_label: "Sales Order Item"
    sql: ${TABLE}.sku ;;
  }

  dimension: detail_sku_code {
    type: string
    label: "Source SKU"
    description: "Item SKU number coming from OMS"
    group_label: "Sales Order Item"
    sql: case when ${source} = 'MAGENTO' then ${TABLE}.detail_sku_code else ${TABLE}.detail_external_id end;;
  }

  dimension: barcode {
    type: string
    label: "Source Barcode"
    description: "Item Barcode number coming from OMS"
    group_label: "Sales Order Item"
    sql: ${TABLE}.barcode ;;
  }


  dimension: tax_rate {
    type: number
    label: "Item Tax Rate"
    description: "VAT rate for OMS data; both VAT and Custom Duty rate for Magento data"
    group_label: "Sales Order Item"
    sql: ${TABLE}.detail_tax;;
  }

  dimension: tax_amount {
    type: number
    hidden: no
    label: "Item Tax Amount"
    description: "Local tax of price after discount in original currency"
    group_label: "Sales Order Item"
    sql: ${TABLE}.detail_tax_amount ;;
  }

  dimension: voucher_code {
    type: string
    label: "Voucher Code"
    hidden: no
    group_label: "Sales Order Item"
    sql: ${TABLE}.voucher_code ;;
  }


  dimension: employee_id {
    type: string
    label: "Employee ID"
    description: "Employee handling the status update"
    hidden: no
    group_label: "Sales Order Item"
    sql: ${TABLE}.employee_id ;;
  }


  dimension: return_cancel_reason {
    type: string
    label: "Item Return/Cancel Reason"
    group_label: "Sales Order Item"
    sql: ${TABLE}.return_cancel_reason ;;
  }

  dimension: cancellation_reason {
    type: string
    label: "Item Cancel Reason"
    group_label: "Sales Order Item"
    sql: ${TABLE}.cancellation_reason ;;
  }

  dimension: fulfilment_employee_id {
    type: string
    label: "fulfilment_employee_id"
    group_label: "Sales Order Item"
    sql: ${TABLE}.fulfilment_employee_id ;;
  }

  dimension: return_location_internal_id {
    type: string
    label: "Returned to Shop/WH ID"
    hidden: no
    group_label: "Sales Order Item"
    sql: ${TABLE}.return_location_internal_id ;;
  }

  dimension: return_location_name {
    type: string
    label: "Returned to Shop/WH Name"
    hidden: no
    group_label: "Sales Order Item"
    sql: ${TABLE}.return_location_name ;;
  }

  dimension: cc_location_internal_id {
    type: string
    label: "Click & Collect Shop/WH ID"
    hidden: no
    group_label: "Sales Order Item"
    sql: ${TABLE}.cc_location_internal_id ;;
  }

  dimension: cc_location_name {
    type: string
    label: "Click & Collect Shop/WH Name"
    hidden: no
    group_label: "Sales Order Item"
    sql: ${TABLE}.cc_location_name ;;
  }

#End of Sales Order Item Dimensions#}

#######################################
### Sales Order Shipment dimensions ###
#######################################{

  dimension: shipment_id {
    type: string
    label: "Shipment ID"
    group_label: "Sales Order Shipment"
    description: "Carrier Shipment ID"
    sql: ${TABLE}.shipment_id ;;
    link: {
      label: "View Order in Carriyo"
      url: "https://dashboard.carriyo.com/shipments/{{value}}"
    }
  }

  dimension: shipment_country {
    type: string
    label: "Shipment Country"
    group_label: "Sales Order Shipment"
    description: "Country where the order was shipped"
    sql: ${TABLE}.shipment_country ;;
  }

  dimension: shipment_city {
    type: string
    label: "Shipment City"
    group_label: "Sales Order Shipment"
    description: "City where the order was shipped"
    sql: ${TABLE}.shipment_city ;;
  }


  dimension: shipment_type {
    type: string
    label: "Shipment Type"
    group_label: "Sales Order Shipment"
    sql: ${TABLE}.delivery_type ;;
  }

  dimension: parcel_id {
    type: string
    label: "Parcel ID"
    group_label: "Sales Order Shipment"
    sql: ${TABLE}.parcel_id ;;
  }

  dimension: tracking_id {
    type: string
    label: "Shipment Tracking ID"
    group_label: "Sales Order Shipment"
    sql: ${TABLE}.tracking_id ;;
  }

  dimension: shipping_location_id {
    type: string
    hidden: yes
    group_label: "Sales Order Shipment"
    sql: ${TABLE}.shipping_location_id ;;
  }

  dimension: shipping_location_internal_id {
    type: string
    hidden: no
    group_label: "Sales Order Shipment"
    label: "Shipment Store/WH ID"
    sql: ${TABLE}.shipping_location_internal_id ;;
  }

  dimension: shipping_location_name {
    type: string
    hidden: no
    group_label: "Sales Order Shipment"
    label: "Shipment Store/WH name"
    sql: ${TABLE}.shipping_location_name ;;
  }

  dimension: shipping_original_amount {
    type: number
    group_label: "Sales Order Shipment"
    description: "Shipping fees in order currency, before discount"
    sql: ${TABLE}.shipping_original_amount ;;
  }

  dimension: shipping_discount_amount {
    type: number
    group_label: "Sales Order Shipment"
    description: "Discount amount applied on shipping fees in order currency"
    sql: ${TABLE}.shipping_discount_amount ;;
  }



  ### End of Sales Order Shipment dimensions
  #}


#######################################
### Acquisition/Marketing dimensions ###
#######################################{

  dimension: acq_campaign {
    type: string
    group_label: "Acquisition / Marketing"
    label: "Campaign"
    description: "Marketing Campaign name"
    sql: ${TABLE}.acq_campaign ;;
  }

  dimension: acq_device {
    type: string
    group_label: "Acquisition / Marketing"
    label: "Device"
    description: "The type of device where the session iniated from: desktop, tablet, or mobile."
    sql: ${TABLE}.acq_device ;;
  }

  dimension: acq_keyword {
    type: string
    group_label: "Acquisition / Marketing"
    label: "Keyword"
    description: "For manual campaign tracking, it is the value of the utm_term campaign tracking parameter. For AdWords traffic, it contains the best matching targeting criteria. For the display network, where multiple targeting criteria could have caused the ad to show up, it returns the best matching targeting criteria as selected by Ads. This could be display_keyword, site placement, boomuserlist, user_interest, age, or gender. Otherwise its value is (not set)."
    sql: ${TABLE}.acq_keyword ;;
  }

  dimension: acq_medium {
    type: string
    group_label: "Acquisition / Marketing"
    label: "Medium"
    description: "The type of referrals. For manual campaign tracking, it is the value of the utm_medium campaign tracking parameter. For AdWords autotagging, it is cpc. If users came from a search engine detected by Google Analytics, it is organic. If the referrer is not a search engine, it is referral. If users came directly to the property and document.referrer is empty, its value is (none)."
    sql: ${TABLE}.acq_medium ;;
  }

  dimension: acq_source {
    type: string
    group_label: "Acquisition / Marketing"
    label: "Source"
    description: "The source of referrals. For manual campaign tracking, it is the value of the utm_source campaign tracking parameter. For AdWords autotagging, it is google. If you use neither, it is the domain of the source (e.g., document.referrer) referring the users. It may also contain a port address. If users arrived without a referrer, its value is (direct)."
    sql: ${TABLE}.acq_source ;;
  }

  dimension: user_country {
    type: string
    hidden: no
    group_label: "Acquisition / Marketing"
    label: "User Country"
    map_layer_name: countries
    # description: "The country from where the user comes from"
    sql: ${TABLE}.user_country ;;
  }

  dimension: marketing_source {
    type: string
    group_label: "Acquisition / Marketing"
    description: "Acquisition Platform"
    label: "Marketing Source"
    sql: ${TABLE}.marketing_source ;;
  }


##############################################
### End of Acquisition/Marketing dimensions ###}


#############################################
##########    SFCC CATEGORIES    ############
#############################################

  dimension: base_locale {
    type: string
    label: "Base Locale"
    group_label: "SFCC Categories"
    # description: ""
    sql: ${TABLE}.base_locale ;;
  }

  dimension: channel_type {
    type: string
    label: "Channel Type"
    group_label: "SFCC Categories"
    # description: ""
    sql: ${TABLE}.channel_type ;;
  }

  dimension: fulfillment_store {
    type: string
    label: "Fulfillment Store"
    group_label: "SFCC Categories"
    # description: ""
    sql: ${TABLE}.fultillment_store ;;
  }

  dimension: crm_customer_phone {
    type: string
    hidden: yes
    label: "Customer Phone number (CRM)"
    group_label: "SFCC Categories"
    # description: ""
    sql: ${TABLE}.crm_customer_phone ;;
  }

  dimension: special_request {
    type: string
    label: "Special Request"
    group_label: "SFCC Categories"
    # description: ""
    sql: ${TABLE}.special_request ;;
  }

  dimension: gift_wrap_type {
    type: string
    label: "Gift Wrap Type"
    group_label: "SFCC Categories"
    # description: ""
    sql: ${TABLE}.gift_wrap_type ;;
  }

  dimension: gift_message_indicator {
    type: string
    label: "Gift Message Indicator"
    group_label: "SFCC Categories"
    # description: ""
    sql: ${TABLE}.gift_message_indicator ;;
  }

  dimension: gift_wrap_description {
    type: string
    label: "Gift Wrap Description"
    group_label: "SFCC Categories"
    # description: ""
    sql: ${TABLE}.gift_wrap_description ;;
  }

  dimension: gift_message {
    type: string
    label: "Gift Message"
    group_label: "SFCC Categories"
    # description: ""
    sql: ${TABLE}.gift_message ;;
  }

  dimension: customer_id {
    type: string
    label: "Customer ID"
    group_label: "SFCC Categories"
    description: "SalesForce customer ID"
    sql: ${TABLE}.customer_id ;;
  }

  measure: sale_detail_muse_points {
    type: sum
    label: "Muse Points"
    group_label: "SFCC Categories"
    # description: ""
    sql: ${TABLE}.sale_detail_muse_points ;;
  }

  dimension: sale_detail_non_inventory {
    type: string
    label: "Non Inventory"
    group_label: "SFCC Categories"
    # description: ""
    sql: ${TABLE}.sale_detail_non_inventory ;;
  }

  dimension: promotion_detail_promotion_text {
    type: string
    label: "Item Promotion - Promotion Detail"
    group_label: "Item Promotion"
    # description: ""
    sql: ${TABLE}.promotion_detail_promotion_text ;;
  }

  dimension: promotion_detail_promotion_id {
    type: string
    label: "Item Promotion - Promotion ID"
    group_label: "Item Promotion"
    # description: ""
    sql: ${TABLE}.promotion_detail_promotion_id ;;
  }

  dimension: promotion_detail_campaign_id {
    type: string
    label: "Item Promotion - Campaign ID"
    group_label: "Item Promotion"
    # description: ""
    sql: ${TABLE}.promotion_detail_campaign_id ;;
  }

  dimension: promotion_detail_coupon_id {
    type: string
    label: "Item Promotion - Coupon ID"
    group_label: "Item Promotion"
    # description: ""
    sql: ${TABLE}.promotion_detail_coupon_id ;;
  }

  dimension: promotion_detail_coupon_code {
    type: string
    label: "Item Promotion - Coupon Code"
    group_label: "Item Promotion"
    # description: ""
    sql: ${TABLE}.promotion_detail_coupon_code ;;
  }

  measure: promotion_detail_discount {
    type: sum
    label: "Item Promotion - Discount"
    group_label: "Item Promotion"
    # description: ""
    sql: ${TABLE}.promotion_detail_discount ;;
  }

  dimension: order_promotion_detail_promotion_text {
    type: string
    label: "Order Promotion - Promotion Text"
    group_label: "Order Promotion"
    # description: ""
    sql: COALESCE(${TABLE}.order_promotion_detail_promotion_text, ${TABLE}.coupon_rule_name) ;;
  }

  dimension: order_promotion_detail_promotion_id {
    type: string
    label: "Order Promotion - Promotion ID"
    group_label: "Order Promotion"
    # description: ""
    sql: ${TABLE}.order_promotion_detail_promotion_id ;;
  }

  dimension: order_promotion_detail_campaign_id {
    type: string
    label: "Order Promotion - Campaign ID"
    group_label: "Order Promotion"
    # description: ""
    sql: ${TABLE}.order_promotion_detail_campaign_id ;;
  }

  dimension: order_promotion_detail_coupon_id {
    type: string
    label: "Order Promotion - Coupon ID"
    group_label: "Order Promotion"
    # description: ""
    sql: ${TABLE}.order_promotion_detail_coupon_id ;;
  }

  dimension: order_promotion_detail_coupon_code {
    type: string
    label: "Order Promotion - Coupon Code"
    group_label: "Order Promotion"
    # description: ""
    sql: COALESCE(${TABLE}.order_promotion_detail_coupon_code, ${TABLE}.coupon_code) ;;
  }

  measure: order_promotion_detail_discount {
    type: sum
    label: "Order Promotion - Discount"
    group_label: "Order Promotion"
    # description: ""
    sql: ${TABLE}.order_promotion_detail_discount ;;
  }

  dimension: coupon_code_combined {
    type: string
    label: "Coupon Code Combined"
    sql: concat(${order_promotion_detail_coupon_code},${promotion_detail_coupon_code}) ;;
  }

  dimension: coupon_promotion_id_combined {
    type: string
    label: "Coupon - Promotion ID Combined"
    sql: concat(${order_promotion_detail_promotion_id},${promotion_detail_promotion_id}) ;;
  }

  dimension: payment_detail_tender_type_id {
    type: string
    label: "Payment Method ID"
    group_label: "SFCC Categories"
    # description: ""
    sql: ${TABLE}.payment_detail_tender_type_id ;;
  }

  dimension: payment_detail_cc_no {
    type: string
    label: "Credit Card No."
    group_label: "SFCC Categories"
    # description: ""
    sql: ${TABLE}.payment_detail_cc_no ;;
  }

  dimension: payment_detail_payment_id {
    type: string
    label: "Payment ID"
    group_label: "SFCC Categories"
    # description: ""
    sql: ${TABLE}.payment_detail_payment_id ;;
  }

  dimension: bill_to_detail_customer_fistname {
    type: string
    label: "Customer First Name"
    hidden: yes
    group_label: "SFCC Categories"
    # description: ""
    sql: ${TABLE}.bill_to_detail_customer_fistname ;;
  }

  dimension: bill_to_detail_customer_lastname {
    type: string
    label: "Customer Last Name"
    hidden: yes
    group_label: "SFCC Categories"
    # description: ""
    sql: ${TABLE}.bill_to_detail_customer_lastname ;;
  }

  dimension: bill_to_detail_customer_address {
    type: string
    label: "Customer Address 1"
    group_label: "SFCC Categories"
    description: "Customer Billing Address"
    sql: concat(${TABLE}.bill_to_detail_customer_address_1, ${TABLE}.bill_to_detail_customer_address_2) ;;
  }

  measure: charge_detail_shipping_charges {
    type: sum
    label: "Shipping Charges ($)"
    group_label: "SFCC Categories"
    # description: ""
    sql: ${TABLE}.charge_detail_shipping_charges ;;
  }

  # measure: charge_detail_shipping_tax_percentage {
  #   type: percentile
  #   label: "Shipping Charges (%)"
  #   group_label: "SFCC Categories"
  #   # description: ""
  #   sql: ${TABLE}.charge_detail_shipping_tax_percentage ;;
  # }

  measure: charge_detail_shipping_tax_amount {
    type: sum
    label: "Shipping Tax Amount"
    group_label: "SFCC Categories"
    # description: ""
    sql: ${TABLE}.charge_detail_shipping_tax_amount ;;
  }

  dimension: muse_detail_member_id {
    type: string
    label: "MUSE - Member ID"
    group_label: "SFCC Categories"
    # description: ""
    sql: ${TABLE}.muse_detail_member_id ;;
  }

  dimension: muse_detail_tier_level {
    type: string
    label: "MUSE - Tier Level"
    group_label: "SFCC Categories"
    # description: ""
    sql: ${TABLE}.muse_detail_tier_level ;;
  }

  measure: muse_detail_points_earned {
    type: sum
    label: "MUSE - Points Earned"
    group_label: "SFCC Categories"
    # description: ""
    sql: ${TABLE}.muse_detail_points_earned ;;
  }


##############################################
########## End of SFCC Categories ############}

###############################################
##########  Affiliate Coupon Codes   ##########
###############################################

  measure: affiliate_cost {
    type: sum
    value_format_name: usd
    label: "Affiliate Cost"
    group_label: "Funnel Metrics"
    description: "Cost paid to the affiliate for order using a coupon code"
    sql: ${tryano_coupon_code_affiliates.commission_percentage}*${tryano.shipment_net_sales_usd} ;;
  }

 ##############################################
 ####### End of Affiliate Coupon Codes ########}

  dimension: bounces {
    type: number
    hidden: yes
    sql: ${TABLE}.bounces ;;
  }


  dimension: common_clicks {
    type: number
    hidden: yes
    sql: ${TABLE}.common_clicks ;;
  }

  dimension: common_cost_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.common_cost_usd ;;
  }


  dimension: common_impressions {
    type: number
    hidden: yes
    sql: ${TABLE}.common_impressions ;;
  }

  dimension: discount_amount {
    type: number
    hidden: yes
    sql: ${TABLE}.discount_amount ;;
  }

  dimension: discount_amount_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.discount_amount_usd ;;
  }


  dimension: pageviews {
    type: number
    hidden: yes
    sql: ${TABLE}.pageviews ;;
  }

  dimension: product_adds_to_cart {
    type: number
    hidden: yes
    sql: ${TABLE}.product_adds_to_cart ;;
  }

  dimension: product_checkouts {
    type: number
    hidden: yes
    sql: ${TABLE}.product_checkouts ;;
  }

  dimension: qty_canceled {
    type: number
    hidden: yes
    sql: ${TABLE}.qty_canceled ;;
  }

  dimension: qty_ordered {
    type: number
    hidden: yes
    sql: ${TABLE}.qty_ordered ;;
  }

  dimension: qty_refunded {
    type: number
    hidden: yes
    sql: ${TABLE}.qty_refunded ;;
  }

  dimension: qty_shipped {
    type: number
    hidden: yes
    sql: ${TABLE}.qty_shipped ;;
  }

  dimension: final_qty {
    type: number
    hidden: yes
    sql: ${TABLE}.final_qty ;;
  }

  dimension: rms_store_code {
    type: number
    hidden: yes
    sql: ${TABLE}.rms_store_code ;;
  }

  dimension: session_duration {
    type: number
    hidden: yes
    sql: ${TABLE}.session_duration ;;
  }

  dimension: sessions {
    type: number
    hidden: yes
    sql: ${TABLE}.sessions ;;
  }

  dimension: unique_pageviews {
    type: number
    hidden: yes
    sql: ${TABLE}.unique_pageviews ;;
  }

  dimension: users {
    type: number
    hidden: yes
    sql: ${TABLE}.users ;;
  }


  ### Gross Amounts ###{

  dimension: gross_net_sales {
    type: number
    hidden: yes
    sql: ${TABLE}.gross_net_sales ;;
  }

  dimension: gross_net_sales_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.gross_net_sales_usd ;;
  }

  dimension: gross_revenue_amount_local {
    type: number
    hidden: yes
    sql: ${TABLE}.gross_revenue_amount_local ;;
  }

  dimension: gross_revenue_amount_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.gross_revenue_amount_usd ;;
  }

  dimension: gross_revenue_local {
    type: number
    hidden: yes
    sql: ${TABLE}.gross_revenue_local ;;
  }

  dimension: gross_revenue_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.gross_revenue_usd ;;
  }

  ### End of Gross Amounts ###}

  ### Shipment Amounts ###{

  dimension: shipment_net_sales {
    type: number
    hidden: yes
    sql: ${TABLE}.shipment_net_sales ;;
  }

  dimension: shipment_net_sales_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.shipment_net_sales_usd ;;
  }

  dimension: shipment_revenue_local {
    type: number
    hidden: yes
    sql: ${TABLE}.shipment_revenue_local ;;
  }

  dimension: shipment_revenue_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.shipment_revenue_usd ;;
  }

  dimension: shipped_rev_amount_local {
    type: number
    hidden: yes
    sql: ${TABLE}.shipped_rev_amount_local ;;
  }

  dimension: shipped_rev_amount_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.shipped_rev_amount_usd ;;
  }

  ### End of Shipment Amounts ###}


  ### Refund Amounts ###{

  dimension: refund_amount_local {
    type: number
    hidden: yes
    sql: ${TABLE}.refund_amount_local ;;
  }

  dimension: refund_amount_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.refund_amount_usd ;;
  }

  ### End of Refund Amounts ###}


  ### Packed Amounts ###{


  dimension: qty_packed {
    type: number
    hidden: yes
    sql: ${TABLE}.qty_packed ;;
  }

  dimension: packed_revenue_local {
    type: number
    hidden: yes
    sql: ${TABLE}.packed_revenue_local ;;
  }

  dimension: packed_revenue_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.packed_revenue_usd ;;
  }

  dimension: packed_revenue_local_minus {
    type: number
    hidden: yes
    sql: ${TABLE}.packed_revenue_local_minus ;;
  }

  dimension: packed_revenue_usd_minus {
    type: number
    hidden: yes
    sql: ${TABLE}.packed_revenue_usd_minus ;;
  }
  dimension: packed_sales_minus_return {
    type: number
    hidden: yes
    sql: ${TABLE}.packed_sales_minus_return ;;
  }

  dimension: packed_sales_minus_return_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.packed_sales_minus_return_usd ;;
  }
  ### End of Packed Amounts ###}

  ### Packed Amounts ###{


  dimension: cancel_amount_local {
    type: number
    hidden: yes
    sql: ${TABLE}.cancel_amount_local ;;
  }

  dimension: cancel_amount_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.cancel_amount_usd ;;
  }

  ### End of Packed Amounts ###}



  #####################################
  ##### Sales Order Item MEASURES #####
  #####################################{


  measure: units_ordered {
    type: sum
    group_label: "Sales Order Metrics"
    label: "Units Ordered"
    description: "Number of Items Ordered"
    sql: ${qty_ordered};;
  }

  measure: unit_canceled {
    type: sum
    group_label: "Sales Order Metrics"
    label: "Units Canceled"
    description: "Number of Items Cancelled"
    sql: ${qty_canceled};;
  }

  measure: units_refunded {
    type: sum
    group_label: "Sales Order Metrics"
    label: "Units Refunded"
    description: "Number of Items Refunded"
    sql: ${qty_refunded};;
  }

  measure: units_shipped {
    type: sum
    group_label: "Sales Order Metrics"
    label: "Units Shipped"
    description: "Number of Items Shipped"
    sql: ${qty_shipped};;
  }

  measure: units_packed {
    type: sum
    group_label: "Sales Order Metrics"
    label: "Units Packed UAT"
    description: "Number of Items Packed"
    sql: ${qty_packed};;
  }

  measure: upt {
    type: number
    group_label: "Sales Order Metrics"
    label: "UPT"
    description: "Average number of units within each order"
    value_format_name: decimal_2
    sql:${units_ordered}/nullif(${all_orders}, 0);;
  }

  measure: usp {
    type:  number
    label: "USP (USD)"
    group_label: "Sales Order Metrics"
    description: "Unit Selling Price = Average price of each item ordered (in Dollar)"
    value_format: "$#,##0.00"
    sql: ${gross_rev_usd} / NULLIF(${units_ordered},0) ;;
  }

  measure: usp_local {
    type:  number
    label: "USP (Local)"
    group_label: "Sales Order Metrics"
    description: "Unit Selling Price = Average price of each item ordered (in Local currency)"
    value_format: "#,##0.00"
    sql: ${gross_rev_local} / NULLIF(${units_ordered},0) ;;
  }

  measure: all_orders {
    type: count_distinct
    group_label: "Sales Order Metrics"
    label: "# Orders"
    description: "Total number of orders placed on the website"
    sql: ${entity_id};;
    filters: [record_type: "SALE"]
  }

  measure: count_cancelled_orders {
    type: count_distinct
    group_label: "Sales Order Metrics"
    label: "# Cancelled Orders"
    description: "Number of Cancelled Orders"
    sql: ${entity_id};;
    filters: [record_type: "CANCELLED"]
  }

  measure: cancelled_order_ratio {
    type: number
    group_label: "Sales Order Metrics"
    label: "Cancelled Order Units %"
    description: "Cancelled Order %"
    value_format_name: percent_2
    sql: ${count_cancelled_orders}/NULLIF(${all_orders},0) ;;
  }

  measure: cancellation_rate {
    type: number
    group_label: "Sales Order Metrics"
    label: "Cancelled Rate %"
    description: "Rate of cancelled items"
    value_format_name: percent_2
    sql:${gross_cancelled_usd}/nullif(${gross_rev_usd},0) ;;
  }

  measure: count_returned_orders {
    type: count_distinct
    group_label: "Sales Order Metrics"
    label: "# Returned Orders"
    description: "Number of Returned Orders"
    sql: ${entity_id};;
    filters: [record_type: "REFUND"]
  }

  measure: returned_order_ratio {
    type: number
    group_label: "Sales Order Metrics"
    label: "Returned Order % (in # of Orders)"
    description: "Returned Order %"
    value_format_name: percent_2
    sql: ${count_returned_orders}/NULLIF(${count_shipped_orders},0) ;;
  }

  measure: returned_order_ratio_value {
    type: number
    group_label: "Sales Order Metrics"
    label: "Returned Order % (in value)"
    description: "Returned Order %"
    value_format_name: percent_2
    sql: ${creditmemo_amount_usd}/NULLIF(${shipped_usd},0) ;;
  }

  measure: gross_rev_local {
    type: sum
    group_label: "Sales Order Metrics"
    label: "Gross Revenue (Local)"
    description: "Total value of incoming orders on the website (excluding failed credit card attempts, VAT, customs and shipping fees and COD fees)"
    value_format: "#,##0.00"
    sql: ${gross_revenue_local};;
  }

  measure: gross_rev_usd {
    type: sum
    group_label: "Sales Order Metrics"
    label: "Gross Revenue (USD)"
    description: "Total value of incoming orders on the website (excluding failed credit card attempts, VAT, customs and shipping fees and COD fees)"
    value_format: "$#,##0.00"
    sql: ${gross_revenue_usd};;
  }


  measure: aov_local {
    type: number
    group_label: "Sales Order Metrics"
    label: "Average Order Value (Local)"
    description: "Average Value of each order"
    value_format: "#,##0.00"
    sql: ${gross_rev_local}/nullif(${all_orders}, 0);;
  }
  measure: aov_usd {
    type: number
    group_label: "Sales Order Metrics"
    label: "Average Order Value (USD)"
    description: "Average Value of each order"
    value_format: "$#,##0.00"
    sql: ${gross_rev_usd}/nullif(${all_orders}, 0);;
  }


  measure: cancelled_item_ratio {
    type: number
    group_label: "Sales Order Metrics"
    label: "Cancelled Item %"
    description: "Cancelled Item %"
    value_format_name: percent_2
    sql: ${unit_canceled}/NULLIF(${units_ordered},0) ;;
  }

  measure: gross_cancelled_local {
    type: sum
    group_label: "Sales Order Metrics"
    label: "Cancelled Amount (Local)"
    description: "Value of Cancelled items (Local Currency)"
    value_format: "#,##0.00"
    sql: ${cancel_amount_local};;
  }

  measure: gross_cancelled_usd {
    type: sum
    group_label: "Sales Order Metrics"
    label: "Cancelled Amount (USD)"
    description: "Value of Cancelled items (USD)"
    value_format: "$#,##0.00"
    sql: ${cancel_amount_usd};;
  }

  measure: gross_minus_cancelled_local {
    type: sum
    group_label: "Sales Order Metrics"
    label: "Gross Revenue w/o cancellations (Local)"
    description: "Gross Revenue minus Cancellation (Local Currency)"
    value_format: "$#,##0.00"
    sql: ${gross_revenue_amount_local};;
  }


  measure: gross_minus_cancelled_usd {
    type: sum
    group_label: "Sales Order Metrics"
    label: "Gross Revenue w/o cancellations (USD)"
    description: "Gross Revenue minus Cancellation (USD)"
    value_format: "$#,##0.00"
    sql: ${gross_revenue_amount_usd};;
  }

  measure: discount_amt_local {
    type:  sum
    group_label: "Sales Order Metrics"
    label: "Discount Amount (Local)"
    description: "Discount Amount (Local currency)"
    value_format: "#,##0.00"
    sql: ${discount_amount} ;;
    filters: [record_type: "SALE"]
  }

  measure: discount_amt_usd {
    type:  sum
    group_label: "Sales Order Metrics"
    label: "Discount Amount (USD)"
    description: "Discount Amount (USD)"
    value_format: "$#,##0.00"
    sql: ${discount_amount_usd} ;;
    filters: [record_type: "SALE"]
  }

  ### Credit Memos ###

  measure: creditmemo_amount_local {
    type: sum
    group_label: "Sales Order Metrics"
    label: "Credit Memo Amt (Local)"
    value_format: "#,##0.00"
    description: "Credit Memo Amount (Local Currency)"
    sql: ${refund_amount_local};;
  }

  measure: creditmemo_amount_usd {
    type: sum
    group_label: "Sales Order Metrics"
    label: "Credit Memo Amt (USD)"
    value_format: "$#,##0.00"
    description: "Credit Memo Amount (USD)"
    sql: ${refund_amount_usd};;
  }

  ### End of Credit Memos ###

  measure: gross_minus_cancelled_minus_returns_local {
    type: sum
    group_label: "Sales Order Metrics"
    label: "Gross Revenue w/o cancellations & returns (Local)"
    value_format: "#,##0.00"
    description: "Gross Revenue minus cancellations & returns (Local currency)"
    sql: ${gross_net_sales};;
  }

  measure: gross_minus_cancelled_minus_returns_usd {
    type: sum
    group_label: "Sales Order Metrics"
    label: "Gross Revenue w/o cancellations & returns (USD)"
    value_format: "$#,##0.00"
    description: "Gross Revenue minus cancellations & returns (USD)"
    sql: ${gross_net_sales_usd};;
  }

  measure: return_rate {
    type: number
    group_label: "Sales Order Metrics"
    label: "Returned Item (%)"
    description: "Rate % of returned items"
    value_format_name: percent_2
    sql: ${units_refunded}/nullif(${units_shipped}, 0);;
  }


  ### Shipped ###

  measure: count_shipped_orders {
    type: count_distinct
    group_label: "Sales Order Metrics"
    label: "# Shipped Orders"
    description: "Number of Shipped Orders"
    sql: ${entity_id};;
    filters: [record_type: "SHIPMENT"]
  }

  measure: shipped_local {
    type: sum
    hidden: yes
    group_label: "Sales Order Metrics"
    label: "Shipped Amount (Local)"
    sql: ${shipped_rev_amount_local};;
  }

  measure: shipped_usd {
    type: sum
    hidden: yes
    group_label: "Sales Order Metrics"
    label: "Shipped Amount (USD)"
    sql: ${shipped_rev_amount_usd};;
  }

  measure: shipped_minus_returns_local {
    type: sum
    group_label: "Sales Order Metrics"
    label: "Shipped Net (Local)"
    value_format: "#,##0.00"
    description:"Total value of shipped items (Local Currency)"
    sql: ${shipment_net_sales};;
  }

  measure: shipped_minus_returns_usd {
    type: sum
    group_label: "Sales Order Metrics"
    label: "Shipped Net (USD)"
    value_format: "$#,##0.00"
    description:"Total value of shipped items (USD)"
    sql: ${shipment_net_sales_usd};;
  }

  measure: shipped_amt {
    type: sum
    hidden: yes
    sql: ${shipment_net_sales_usd};;
    filters: [record_type: "SHIPMENT"]
  }

  measure: returned_amt {
    type: sum
    hidden: yes
    sql: abs(${shipment_net_sales_usd});;
    filters: [record_type: "REFUND"]
  }



  measure: ship_time_day {
    type: sum_distinct
    group_label: "Sales Order Metrics"
    label: "Shipment Time - in Days"
    description: "Difference between Order creation datetime and shipment datetime, in days"
    sql: TIMESTAMP_DIFF(${common_raw}, ${order_date_raw}, DAY) ;;
    sql_distinct_key: ${increment_id};;
    filters: [record_type: "SHIPMENT"]
  }

  measure: ship_time_hour {
    type: sum_distinct
    group_label: "Sales Order Metrics"
    label: "Shipment Time - in Hours"
    description: "Difference between Order creation datetime and shipment datetime, in hours"
    value_format: "0.00"
    sql: TIMESTAMP_DIFF(${common_raw}, ${order_date_raw}, HOUR) ;;
    sql_distinct_key: ${increment_id};;
    filters: [record_type: "SHIPMENT"]
  }

  measure: ship_time_minute {
    type: sum_distinct
    group_label: "Sales Order Metrics"
    label: "Shipment Time - in Minutes"
    description: "Difference between Order creation datetime and shipment datetime, in minutes"
    sql: TIMESTAMP_DIFF(${common_raw}, ${order_date_raw}, MINUTE) ;;
    sql_distinct_key: ${increment_id};;
    filters: [record_type: "SHIPMENT"]
  }

  measure: avg_ship_time_day {
    type: average_distinct
    group_label: "Sales Order Metrics"
    label: "Shipment Time - Avg in Days"
    description: "Average difference between Order creation datetime and shipment datetime, in days"
    value_format: "0.00"
    sql: TIMESTAMP_DIFF(${common_raw}, ${order_date_raw}, DAY) ;;
    sql_distinct_key: ${increment_id};;
    filters: [record_type: "SHIPMENT"]
  }

  measure: avg_ship_time_hour {
    type: average_distinct
    group_label: "Sales Order Metrics"
    label: "Shipment Time - Avg in Hours"
    description: "Average difference between Order creation datetime and shipment datetime, in hours"
    value_format: "0.00"
    sql: TIMESTAMP_DIFF(${common_raw}, ${order_date_raw}, HOUR) ;;
    sql_distinct_key: ${increment_id};;
    filters: [record_type: "SHIPMENT"]
  }

  measure: avg_ship_time_minute {
    type: average_distinct
    group_label: "Sales Order Metrics"
    label: "Shipment Time - Avg in Minutes"
    description: "Average difference between Order creation datetime and shipment datetime, in minutes"
    value_format: "0.00"
    sql: TIMESTAMP_DIFF(${common_raw}, ${order_date_raw}, MINUTE) ;;
    sql_distinct_key: ${increment_id};;
    filters: [record_type: "SHIPMENT"]
  }


  ## End of Shipped

  ###Packed###
  measure: packed_minus_returns_usd {
    type: sum
    group_label: "Sales Order Metrics"
    label: "Packed Sales minus Returns UAT(USD)"
    value_format: "$#,##0.00"
    description:"Total value of packed items excluding returns (USD)"
    sql: ${packed_sales_minus_return_usd};;
  }

  measure: packed_minus_returns_local {
    type: sum
    group_label: "Sales Order Metrics"
    label: "Packed Sales minus Returns UAT(local)"
    value_format: "#,##0.00"
    description:"Total value of packed items excluding returns (local)"
    sql: ${packed_sales_minus_return};;
  }

  measure: packed_sales_local {
    type: sum
    group_label: "Sales Order Metrics"
    label: "Packed Sales UAT(local)"
    value_format: "#,##0.00"
    description:"Total value of packed items (local)"
    sql: ${packed_revenue_local};;
  }

  measure: packed_sales_usd {
    type: sum
    group_label: "Sales Order Metrics"
    label: "Packed Sales UAT(USD)"
    value_format: "$#,##0.00"
    description:"Total value of packed items (USD)"
    sql: ${packed_revenue_usd};;
  }

  ##End of Packed##

  ### Gross Margin ###

  measure: cogs_local {
    type: sum
    group_label: "Sales Order Metrics"
    value_format: "#,##0.00"
    description: "Cost of Good Sold (Local Currency)"
    sql: case when ${unit_cost} <> 0 then ${qty_ordered}*${unit_cost} else 0 end;;
    filters: [record_type: "SALE"]
  }


  measure: cogs_usd {
    type: sum
    group_label: "Sales Order Metrics"
    value_format: "$#,##0.00"
    description: "Cost of Good Sold (USD)"
    sql: case when ${unit_cost_usd} <> 0 then ${qty_ordered}*${unit_cost_usd} else 0 end;;
    filters: [record_type: "SALE"]
  }

  measure: gross_margin_local {
    type: sum
    group_label: "Sales Order Metrics"
    label: "Gross Margin (Local Currency)"
    value_format: "#,##0.00"
    description: "Gross Revenue minus COGS"
    sql: case when ${unit_cost} <> 0 then nullif(${gross_revenue_local}, 0)-nullif(${qty_ordered}*${unit_cost}, 0) else 0 end;;
#     sql: nullif(${gross_rev_local}, 0)-nullif(${cogs_local}, 0);;
  }

  measure: gross_margin_usd {
    type: sum
    group_label: "Sales Order Metrics"
    label: "Gross Margin (USD)"
    value_format: "$#,##0.00"
    description: "Gross Revenue minus COGS"
    sql: case when ${unit_cost_usd} <> 0 then nullif(${gross_revenue_usd}, 0)-nullif(${qty_ordered}*${unit_cost_usd}, 0) else 0 end;;
#     sql: nullif(${gross_rev_usd}, 0)-nullif(${cogs_usd}, 0);;
  }


  measure: gross_margin_ratio {
    type: number
    group_label: "Sales Order Metrics"
    label: "Gross Margin (%)"
    description: "Gross Margin value divided by Gross Revenue"
    value_format_name: percent_2
#     sql:case when ${cogs_local} <> 0 then 1-(${cogs_local}/nullif(${gross_rev_local}, 0)) else 0 end;;
    sql: sum(case when ${unit_cost_usd} <> 0 then nullif(${gross_revenue_usd}, 0)-nullif(${qty_ordered}*${unit_cost_usd}, 0) else 0 end)/sum(case when ${unit_cost_usd} <> 0 then nullif(${gross_revenue_usd}, 0) end);;
  }


  ### End of Gross Margin ###

  ### End of Sales Order Item Measures ###
  #}

  ##################################
  #####   GA Measures          #####
  ##################################{

  measure: number_of_sessions {
    type: sum
    group_label: "GA Metrics"
    label: "# of Sessions"
    description: "Total number of visits on the website"
    sql: ${TABLE}.sessions ;;
  }

  measure: sum_session_duration {
    type: sum
    group_label: "GA Metrics"
    label: "Sessions duration"
    description: "Total Sessions duration"
    sql: ${TABLE}.session_duration ;;
  }

  measure: sum_bounces {
    type: sum
    group_label: "GA Metrics"
    label: "# of Bounces"
    description: "Number of sessions that have left the website after only one page-view"
    sql: ${TABLE}.bounces ;;
  }

  measure: number_of_pageviews {
    type: sum
    group_label: "GA Metrics"
    label: "# of Page views"
    description: "Total number of page views"
    sql: ${TABLE}.pageviews ;;
  }

  measure: sum_unique_pageviews {
    type: sum
    group_label: "GA Metrics"
    label: "# of Unique Page views"
    description: "Number of unique page views"
    sql: ${TABLE}.unique_pageviews ;;
  }

  measure: sum_product_adds_to_cart {
    type: sum
    group_label: "GA Metrics"
    label: "# of Products Added to Cart"
    description: "Number of Products added to Cart"
    sql: ${TABLE}.product_adds_to_cart ;;
  }

  measure: sum_product_checkouts {
    type: sum
    group_label: "GA Metrics"
    label: "# of Product Checkouts"
    description: "Number of Products checked out"
    sql: ${TABLE}.product_checkouts ;;
  }

  measure: sum_users {
    type: sum
    hidden: yes
    group_label: "GA Metrics"
    label: "# of Users"
    description: "Total number of visitors on the website for a given period"
    sql: ${TABLE}.users ;;
  }

  measure: pageviews_per_session {
    type: number
    group_label: "GA Metrics"
    label: "Pageviews per Session"
    description: "Average number of pages viewed per session"
    value_format_name: decimal_2
    sql: ${number_of_pageviews}/NULLIF(${number_of_sessions},0) ;;
  }


  measure: bounce_rate {
    type: number
    group_label: "GA Metrics"
    label: "Bounce Rate %"
    description: "Percentage of sessions that have left the website after only one page-view"
    value_format_name: percent_2
    sql: ${sum_bounces}/NULLIF(${number_of_sessions},0) ;;
  }

  measure: avg_time_on_site {
    type: number
    group_label: "GA Metrics"
    label: "Avg Time On Site"
    description: "Average of time spend on the site in seconds per session"
    value_format_name: decimal_0
    sql: ${sum_session_duration}/NULLIF(${number_of_sessions},0) ;;
  }

  measure: add_to_basket_rate {
    type: number
    group_label: "GA Metrics"
    label: "Add to Basket %"
    description: "Percentage of product detail views that have added the product to their basket over a period of time"
    value_format_name: percent_2
    sql: ${sum_product_adds_to_cart}/NULLIF(${number_of_pageviews},0) ;;
  }

  measure: cart_abandonment_rate {
    type: number
    group_label: "GA Metrics"
    label: "Cart Abandonment %"
    description: "Number of cart created that did not convert into an order"
    value_format_name: percent_2
    sql: (${sum_product_adds_to_cart}-${sum_product_checkouts})/NULLIF(${sum_product_adds_to_cart},0) ;;
  }

  measure: checkout_to_add_rate {
    type: number
    group_label: "GA Metrics"
    label: "Checkout to Add to Basket %"
    description: "Ratio of Product checked out vs Added to Cart"
    value_format_name: percent_2
    sql: ${sum_product_checkouts}/NULLIF(${sum_product_adds_to_cart},0) ;;
  }


  measure: conversion_rate {
    type: number
    group_label: "GA Metrics"
    label: "Conversion Rate (%)"
    description: "Percentage of sessions that make an order"
    value_format_name: percent_2
    sql: ${all_orders}/NULLIF(${number_of_sessions},0) ;;
  }

  ### End of GA Measures
  #}


  #####################################
  #####     Funnel MEASURES       #####
  #####################################{


  measure: total_ad_spend_usd {
    type: sum
    group_label: "Funnel Metrics"
    label: "Total Ad Spend (USD)"
    description: "Total value of marketing ads spent across all different channels"
    value_format: "$#,##0.00"
    sql: ${common_cost_usd} ;;
  }

  measure: roas_gross_rev {
    type: number
    group_label: "Funnel Metrics"
    label: "ROAS (on Gross Rev)"
    value_format: "#,##0.00"
    description: "The Return in Gross Sales coming from your Total Ad Spend (based on Gross Sales)"
    sql: ${gross_rev_usd}/nullif(${total_ad_spend_usd}, 0) ;;
  }

  measure: roi_gross_rev {
    type: number
    group_label: "Funnel Metrics"
    label: "ROI (on Gross Rev)"
    value_format: "#,##0.00"
    description: "The Return on Investment in Total Ad Spend in Gross Sales"
    sql: (${gross_rev_usd}-${total_ad_spend_usd})/nullif(${total_ad_spend_usd}, 0) ;;
  }

  measure: cost_per_order {
    type: number
    group_label: "Funnel Metrics"
    label: "CPO"
    description: "The marketing cost of one order"
    value_format: "$#,##0.00"
    sql: ${total_ad_spend_usd}/nullif(${all_orders}, 0) ;;
  }

  measure: total_clicks {
    type: sum
    group_label: "Funnel Metrics"
    label: "# Clicks"
    description: "Total number of times an ad is clicked by a user"
    sql: ${common_clicks} ;;
  }

  measure: total_impressions {
    type: sum
    group_label: "Funnel Metrics"
    label: "# Impressions"
    description: "Total number of times an ad is seen"
    sql:  ${common_impressions};;
  }

  measure: cost_per_click {
    type: number
    group_label: "Funnel Metrics"
    label: "CPC"
    description: "The marketing cost of one ad click"
    value_format_name: usd
    sql: ${total_ad_spend_usd} / NULLIF(${total_clicks},0) ;;
  }


  measure: click_through_rate {
    type: number
    group_label: "Funnel Metrics"
    label: "Click Through Rate %"
    description: "Percentage ad impressions that have led to a click"
    value_format_name: percent_2
    sql: NULLIF(${total_clicks},0) / NULLIF(${total_impressions},0);;
  }


  measure: cost_per_mill {
    type: number
    group_label: "Funnel Metrics"
    label: "CPM"
    description: "Cost per Thousand Impressions"
    value_format_name: usd
    sql: NULLIF(NULLIF(${total_ad_spend_usd},0) / NULLIF(${total_impressions}/1000,0),0) ;;
  }

  measure: cost_of_acquisition{
    type: number
    value_format_name: decimal_2
    group_label: "Funnel Metrics"
    description: "The marketing cost of one acquired new customer"
    label: "Cost of Acquisition"
    sql: NULLIF(${total_ad_spend_usd} / NULLIF(${ecomm_customer_type_pop.new_cust_count_fixed},0),0) ;;
  }

  ### End of Funnel Measures
  #}

####################################
#########      ALERTS      #########
####################################


  # dimension_group: record {
  #   type: time
  #   timeframes: [
  #     raw,
  #     second,
  #     minute,
  #     hour,
  #     date,
  #     week,
  #     month,
  #     quarter,
  #     year,
  #     week_of_year,
  #     day_of_week
  #   ]
  #   convert_tz: no
  #   datatype: datetime
  #   label: "Record"
  #   sql: ${TABLE}.record_datetime ;;
  # }


  measure: minutes_since_last_update {
    type: min
    group_label: "Alerts"
    sql: DATETIME_DIFF(current_datetime(),cast(${common_raw} as datetime),minute)+240 ;;
  }

}
