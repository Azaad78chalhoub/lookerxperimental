view: stg_comm_chanel_faces_ga_landing_pages {

  derived_table: {
    sql:
        SELECT *
        FROM
          `chb-prod-stage-comm.prod_comm.stg_comm_faces_ga_landing_pages`
        WHERE
          landing_page_path like "%chanel%";;
  }

  dimension: pk {
    type:  string
    primary_key:  yes
    sql:  concat(cast(${date_date} as string), ${landing_page_path}, ${device_category},
                ${campaign}, ${source_medium}, ${store_country}) ;;
  }

  dimension_group: date {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.date ;;
  }

  dimension: brand_security{
    type: string
    hidden: yes
    sql: "FACES" ;;
  }

  dimension: vertical_security{
    type: string
    hidden: yes
    sql: "MANAGED COMPANIES" ;;
  }

  dimension: sessions {
    type: number
    description: "Sessions"
    sql: ${TABLE}.sessions ;;
  }

  dimension: campaign {
    type: string
    description: "Campaign"
    sql: ${TABLE}.campaign ;;
  }

  dimension: landing_page_path {
    type: string
    description: "Landing page path"
    sql: ${TABLE}.landing_page_path ;;
  }

  dimension: device_category {
    type: string
    description: "Device category"
    sql: ${TABLE}.device_category ;;
  }

  dimension: source_medium {
    type: string
    description: "Source / Medium"
    sql: ${TABLE}.source_medium ;;
  }

  dimension: store_country {
    type: string
    description: "Country"
    sql: ${TABLE}.ecom_site ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }


  measure: number_of_sessions {
    type: sum
    label: "# of Sessions"
    description: "Total number of sessions"
    sql: ${TABLE}.sessions ;;
  }

  measure: number_of_pageviews {
    type: sum
    label: "# of Pageviews"
    description: "Total number of page views"
    sql: ${TABLE}.pageviews ;;
  }

  measure: number_of_unique_pageviews {
    type: sum
    label: "# of Unique Pageviews"
    description: "Total number of unique page views"
    sql: ${TABLE}.unique_pageviews ;;
  }

  measure: number_of_bounces {
    type: sum
    label: "# of Bounces"
    description: "Total number of bounces"
    sql: ${TABLE}.bounces ;;
  }

  measure: time_on_page {
    type: sum
    label: "Time on page"
    description: "Total Time spent on the pages"
    sql: ${TABLE}.time_on_page ;;
  }

  measure: number_of_new_users {
    type: sum
    label: "# of New Users"
    description: "Sum of new users"
    sql: ${TABLE}.new_users ;;
  }

  measure: session_duraton {
    type: sum
    label: "Session duration"
    description: "Sum of session duration"
    sql: ${TABLE}.session_duration ;;
  }

  measure: number_of_new_sessions {
    type: sum
    label: "# of New Sessions"
    description: "Sum of new sessions"
    sql: ${TABLE}.new_sessions ;;
  }

  measure: pageviews_per_session {
    type: number
    label: "Pageviews per Session"
    description: "Page views per session"
    value_format_name: decimal_4
    sql: ${number_of_pageviews}/NULLIF(${number_of_sessions},0) ;;
  }

  measure: average_session_duration {
    type: number
    label: "Average Session Duration"
    description: "Average session duration"
    value_format: "h:mm:ss"
    sql: ${session_duraton}/NULLIF(${number_of_sessions},0)/86400.0 ;;
  }

  measure: number_of_transctions {
    type: count_distinct
    label: "# of Transactions"
    description: "Number of distinct transactions / orders"
    sql: ${stg_comm_chanel_faces_ga_transactions.transaction_id} ;;
  }

  measure: conversion_rate {
    type: number
    label: "Conversion Rate (%)"
    description: "Number of orders / Number of sessions"
    value_format_name: percent_2
    sql: ${number_of_transctions}/NULLIF(${number_of_sessions},0) ;;
  }

  measure: bounce_rate {
    type: number
    label: "Bouce Rate (%)"
    description: "Bounce rate in percentage"
    value_format_name: percent_2
    sql: ${number_of_bounces}/NULLIF(${number_of_sessions},0) ;;
  }

  measure: percent_new_sessions {
    type: number
    label: "New Sessions (%)"
    description: "Percentage of new sessions"
    value_format_name: percent_2
    sql: ${number_of_new_sessions}/NULLIF(${number_of_sessions},0) ;;
  }


  measure: total_time_spent {
    type: sum
    label: "Total time spent"
    description: "Total time spent on the pages"
    value_format_name: decimal_2
    sql: ${TABLE}.time_on_page ;;
  }

}
