view: table_explore_mapping {
  sql_table_name: `chb-bq-exports-logs.bigquery_tables_audit.table_explore_mapping`
    ;;

  dimension: pk {
    type:  string
    sql: concat(${project_id}, ${dataset_id}, ${table_id}) ;;
    primary_key: yes
    hidden: yes
  }

  dimension: dataset_id {
    type: string
    sql: ${TABLE}.dataset_id ;;
  }

  dimension: looker_explore_name {
    type: string
    sql: ${TABLE}.looker_explore_name ;;
  }

  dimension: project_id {
    type: string
    sql: ${TABLE}.project_id ;;
  }

  dimension: stream {
    type: string
    sql: ${TABLE}.stream ;;
  }

  dimension: table_id {
    type: string
    sql: ${TABLE}.table_id ;;
  }

  dimension: table_type {
    type: string
    sql: ${TABLE}.table_type ;;
    hidden: yes
  }

  measure: count {
    type: count
    drill_fields: [looker_explore_name]
    hidden: yes
  }
}
