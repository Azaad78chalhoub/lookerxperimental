view: bigquery_tables_audit {
  sql_table_name: `chb-bq-exports-logs.bigquery_tables_audit.tables_audit`
    ;;

  dimension: pk {
    type:  string
    sql: concat(${project_id}, ${dataset_id}, ${table_id}) ;;
    primary_key: yes
    hidden: yes
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created ;;
  }

  dimension: dataset_id {
    type: string
    sql: ${TABLE}.dataset_id ;;
  }

  dimension: location {
    type: string
    sql: ${TABLE}.location ;;
  }

  dimension_group: modified {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.modified ;;
  }

  dimension: num_bytes {
    type: number
    sql: ${TABLE}.num_bytes ;;
  }

  dimension: num_rows {
    type: number
    sql: ${TABLE}.num_rows ;;
  }

  dimension: project_id {
    type: string
    sql: ${TABLE}.project_id ;;
  }

  dimension: size {
    type: string
    sql: ${TABLE}.size ;;
  }

  dimension: table_id {
    type: string
    sql: ${TABLE}.table_id ;;
  }

  dimension: table_type {
    type: string
    sql: ${TABLE}.table_type ;;
  }


  dimension: environment {
    type: string
    sql:  case when REGEXP_CONTAINS(${TABLE}.project_id, r'sand') then "SandBox" else "Prod" end ;;
  }

  dimension: zone {
    type: string
    sql:  case when REGEXP_CONTAINS(${TABLE}.project_id, r'ingest') then "Ingest"
    when REGEXP_CONTAINS(${TABLE}.project_id, r'raw') then "Ingest"
    when REGEXP_CONTAINS(${TABLE}.project_id, r'stage') then "Staging"
    else "Serving" end ;;
  }

  dimension: days_since_last_refesh {
    type: number
    label: "# of Days since last Refresh"
    sql: DATE_DIFF(CURRENT_DATE(), ${modified_date}, DAY) ;;
  }

  dimension: days_since_last_refesh_bucket {
    type: tier
    tiers: [0,1,2,5,10,20,40,50,100]
    style: integer
    sql: ${days_since_last_refesh};;
  }


  measure: count {
    type: count
    drill_fields: [details*]
    link: {
      label: "Sort by Modified Date"
      url: "{{ link }}&sorts=bigquery_tables_audit.modified_time+asc"
    }
  }




  set: details {
    fields: [project_id, dataset_id, table_id, modified_time, created_time, days_since_last_refesh, num_rows, size, location]
  }
}
