view: fivetran_statistics {
  sql_table_name: `chb-bq-exports-logs.bigquery_tables_audit.fivetran_statistics`
    ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    hidden: yes
    type: string
    sql: ${TABLE}.id ;;
  }

  dimension_group: _fivetran_synced {
    description: "fivetran_synced (UTC TIMESTAMP) to keep track of when the row was synced by Fivetran"
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}._fivetran_synced ;;
  }

  dimension: dataset_name {
    group_label: "BigQuery metadata"
    description: "Dataset Name"
    type: string
    drill_fields: [table]
    sql: ${TABLE}.dataset_id ;;
  }

  dimension: project_name {
    group_label: "BigQuery metadata"
    description: "Project Name"
    type: string
    drill_fields: [dataset_name, table]
    sql: ${TABLE}.project_id ;;
  }

  dimension: table {
    group_label: "BigQuery metadata"
    type: string
    description: "Table Name"
    sql: ${TABLE}.table ;;
  }

  dimension_group: done {
    label: "Sync Completion time"
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.done ;;
  }

  dimension: message {
    type: string
    description: "notification of an error, e.g, \"This is not supported by the Bulk API\""
    sql: ${TABLE}.message ;;
  }

  dimension_group: progress {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.progress ;;
  }



  dimension: rows_updated_or_inserted {
    type: number
    hidden: yes
    description: "Number of changed or updated rows compared to the previous sync"
    sql: ${TABLE}.rows_updated_or_inserted ;;
  }

  dimension: schema {
    type: string
    description: "Schema name for this integration"
    sql: ${TABLE}.schema ;;
  }

  dimension_group: start {
    label: "Actual Schedule Start"
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.start ;;
  }

  dimension: status {
    type: string
    description: "Stage of sync"
    sql: ${TABLE}.status ;;
  }

  dimension: update_id {
    type: string
    hidden: yes
    sql: ${TABLE}.update_id ;;
  }

  dimension_group: update_started {
    label: "Scheduled Sync Start"
    description: "Scheduled update start time"
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.update_started ;;
  }

###### MEASURES ###### {

  measure: total_rows_updated_or_inserted {
    type:  sum
    description: "Total number of changed or updated rows compared to the previous sync"
    drill_fields: [total_rows_updated_or_inserted_details*]
    sql: ${TABLE}.rows_updated_or_inserted ;;
  }

  measure: delay_in_start_sec {
    label: "Delay in start (in seconds)"
    description: "Difference between scheduled start time and actual start time, in seconds"
    type: sum
    sql:TIMESTAMP_DIFF(${start_raw}, ${update_started_raw}, SECOND) ;;
  }

  measure: delay_in_start_min {
    label: "Delay in start (in minutes)"
    description: "Difference between scheduled start time and actual start time, in minutes"
    type: sum
    sql:TIMESTAMP_DIFF(${start_raw}, ${update_started_raw}, MINUTE) ;;
  }

  measure: delay_in_start_hrs {
    label: "Delay in start (in hours)"
    description: "Difference between scheduled start time and actual start time, in hours"
    type: sum
    sql:TIMESTAMP_DIFF(${start_raw}, ${update_started_raw}, HOUR) ;;
  }

  measure: execution_time_sec {
    label: "Sync execution time (in seconds)"
    description: "Difference between completion time and actual start time, in seconds"
    type: sum
    sql:TIMESTAMP_DIFF(${done_raw}, ${update_started_raw}, SECOND) ;;
  }

  measure: execution_time_min {
    label: "Sync execution time (in minutes)"
    description: "Difference between completion time and actual start time, in minutes"
    type: sum
    sql:TIMESTAMP_DIFF(${done_raw}, ${update_started_raw}, MINUTE) ;;
  }

  measure: execution_time_hrs {
    label: "Sync execution time (in hours)"
    description: "Difference between completion time and actual start time, in hours"
    type: sum
    sql:TIMESTAMP_DIFF(${done_raw}, ${update_started_raw}, HOUR) ;;
  }

  measure: count{
    type: count
  }


###### END OF MEASURES #### }


  set: total_rows_updated_or_inserted_details {
    fields: [project_name, dataset_name, table, update_started_raw, start_raw, total_rows_updated_or_inserted]
  }

}
