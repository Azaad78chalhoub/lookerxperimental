

view: fact_retail_sales_ext {
  extension: required


  dimension: consignment_cogs_usd {
    type: number
    hidden:  yes
    value_format_name: decimal_2
    sql: ${amountusd_beforetax}  * (1-${ch_commission_valid_data.commission}/100) ;;
  }

  dimension: consignment_cost_usd {
    type: number
    hidden:  yes
    value_format_name: decimal_2
    sql: safe_divide(${consignment_cogs_usd},${mea_quantity}) ;;
  }

  dimension: wac_usd_adj {
    type: number
    description: "Adjusted Average Unit Cost (WAC) in USD considering consignments"
    group_label: "Consignment"
    label: "Adj WAC USD"
    hidden: no
    value_format_name: decimal_2
    sql: case when ${dim_item_loc_traits.consignment_flag} ="Y" then
          ${consignment_cost_usd}
    else   ${av_cost_usd} end ;;
  }

  dimension: sales_amt_lcl {
    type: number
    hidden: yes
    value_format_name : decimal_2
    sql: ${TABLE}.amountlocal_beforetax ;;
  }


  dimension: consignment_cogs_lcl {
    type: number
    hidden:  yes
    value_format_name: decimal_2
    sql: ${sales_amt_lcl} * (1-${ch_commission_valid_data.commission}/100) ;;
  }

  dimension: consignment_cost_lcl {
    type: number
    hidden:  yes
    value_format_name: decimal_2
    sql: safe_divide(${consignment_cogs_lcl},${mea_quantity}) ;;
  }

  dimension: wac_lcl_adj {
    type: number
    description: "Adjusted Average Unit Cost (WAC) in Local Currency considering consignments"
    group_label: "Consignment"
    label: "Adj WAC Local"
    hidden: no
    value_format_name: decimal_2
    sql: case when ${dim_item_loc_traits.consignment_flag} ="Y" then
          ${consignment_cost_lcl}
    else   ${av_cost_local} end ;;
  }


  # adjusted COGS

  dimension: commission {
    type: number
    group_label: "Consignment"
    label: "Commission %"
    hidden: yes

    sql: ${ch_commission_valid_data.commission} ;;
    value_format_name: usd
  }

  dimension: cost_of_goods_sold_local_adj {
    type: number
    group_label: "Consignment"
    hidden: yes

    sql: ABS(${wac_lcl_adj})*${mea_quantity} ;;
    value_format_name: usd
  }

  dimension: cost_of_goods_sold_usd_adj {
    type: number
    group_label: "Consignment"
    hidden: yes

    sql: ABS(${wac_usd_adj})*${mea_quantity} ;;
    value_format_name: usd
  }

  measure: cogs_usd_adj {
    type: sum
    sql_distinct_key: concat(${ch_commission_valid_data.pk} ,${fact_retail_sales.id}) ;;
    group_label: "Consignment"
    value_format_name: usd
    description: "Adjusted considering consignments"
    label: "Cost of Goods Sold Adj USD"
    sql:  ${cost_of_goods_sold_usd_adj};;
  }



  measure: cogs_lcl_adj {
    type: sum
    sql_distinct_key: concat(${ch_commission_valid_data.pk} ,${fact_retail_sales.id}) ;;
    group_label: "Consignment"
    label: "Cost of Goods Sold Adj Local"
    description: "Adjusted considering consignments"
    value_format_name: decimal_2

    sql:  ${cost_of_goods_sold_local_adj};;
  }


  ##---------Adding Adj COGS on MD and FP Local

  measure: cogs_lcl_adj_FP{
    group_label: "Consignment"
    description: "Cost (Local) of items sold at full price (Adjusted considering consignments)"
    label: "COGS Adj Local on Full Price"
    type: sum
    value_format_name: decimal_2
    sql: CASE WHEN ${discountamt_local} = 0 or  ${discountamt_local} is NULL THEN ${cost_of_goods_sold_local_adj}
      ELSE null END ;;
  }
  measure: cogs_lcl_adj_MD{
    group_label: "Consignment"
    description: "Cost (Local) of items sold at discount (Adjusted considering consignments)"
    label: "COGS Adj Local on Markdown"
    type: sum
    value_format_name: decimal_2
    sql: CASE WHEN ${discountamt_local} <> 0 THEN ${cost_of_goods_sold_local_adj}
      ELSE null END ;;
  }

  ##----------

  measure: gross_margin_usd_adj {
    type: number
    value_format_name: usd
    group_label: "Consignment"
    label: "Gross Margin Adj USD"
    description: "Adjusted considering consignments"

    sql: ${net_sales_amount_usd} -${cogs_usd_adj} ;;
  }

  measure: gross_margin_local_adj {

    type: number
    value_format_name: decimal_2
    group_label: "Consignment"
    label: "Gross Margin Adj Local"
    description: "Adjusted considering consignments"
    sql: ${net_sales_amount_local} -${cogs_lcl_adj} ;;
  }


  ##------------Adding Adj Gross Margin on FP and MD Local

  measure: gross_margin_local_adj_FP{
    label: "Gross Margin Adj Local on Full Price"
    description: "Net Sales (Local) minus Cost of Goods Sold (Local) of items sold at full price (Adjusted considering consignments)"
    group_label: "Consignment"
    type: number
    value_format_name: decimal_2
    sql: ${NetSalesLocal_onFP}-${cogs_lcl_adj_FP}
      ;;
  }

  #SPD-287
  measure: gross_margin_local_adj_MD{
    label: "Gross Margin Adj Local on Markdown"
    description: "Net Sales (Local) minus Cost of Goods Sold (Local) of items sold at a discount (Adjusted considering consignments)"
    group_label: "Consignment"
    type: number
    value_format_name: decimal_2
    sql: ${NetSalesLocal_onMD}-${cogs_lcl_adj_MD}
      ;;
  }

  ##------------



  measure: gross_margin_share_adj {
    type: number
    value_format_name: percent_2
    group_label: "Consignment"
    label: "Gross Margin Share Adj"
    description: "Adjusted considering consignments"

    sql:  safe_divide(${gross_margin_usd_adj},${net_sales_amount_usd});;
  }

  # adjusted COGS

# budgets

  measure: gross_margin_local_vs_budget {
    type: number
    hidden: no
    label: "Gross Margin Local vs Target"
    description: "Gross Margin (local currency) divided by Gross Margin budget target (local currency)"
    group_label: "Gross Margin"
    sql: SUM(${net_sales_amount_local} -${cogs_local})/SUM(${budgetstarget.target_margin}) ;;
    value_format_name: decimal_0
  }

  measure: net_sales_local_vs_budget {
    type: number
    hidden: no
    group_label: "Net Sales"
    label: "Net Sales Local vs Budget"
    description: "Net Sales (local currency) divided by Net Sales budget target (local currency)"
    sql: SUM(${amountlocal_beforetax})/SUM(${budgetstarget.budget_aed}) ;;
    value_format_name: decimal_0
  }

 }
