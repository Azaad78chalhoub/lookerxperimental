view: oms_rule_agg_last7_new1 {
  sql_table_name: `chb-exp-004.DQ_Rule_Agg1.OMS_RuleAgg_last7_new1`
    ;;

  dimension: awb {
    type: string
    sql: ${TABLE}.awb ;;
    label: "AWB"
    description: "AWB number in WH. Same as tracking no in Carriyo"
  }

  dimension: brand {
    type: string
    sql: ${TABLE}.brand ;;
    label: "Brand"
  }

  dimension: carrier_status {
    type: string
    description: "Latest Carriyo carrier status on the order"
    sql: ${TABLE}.carrier_status ;;
  }

  dimension: carrier_status_description {
    type: string
    hidden: yes
    sql: ${TABLE}.carrier_status_description ;;
  }

  dimension_group: collected_at_carriyo {
    type: time
    label: "Collected at"
    description: "Timestamp for order collection by the DSP from WH/Store. Obtained from Carriyo"
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: cast(${TABLE}.collected_at_carriyo as timestamp) ;;
  }

  dimension: collected_at_status {
    type: string
    label: "Order collected status"
    description: "Indicates if a click&collect order has been collected by customer or not"
    sql: ${TABLE}.collected_at_status ;;
  }

  dimension_group: collected_by_customer {
    type: time
    description: "Collected by customer timestamp for click&collect orders"
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: cast(${TABLE}.collected_by_customer_at as timestamp) ;;
  }

  dimension: completeness_score {
    type: number
    label: "Completeness pass/fail"
    description:"Calculated using the least score from the individual OMS rules for Completeness (OMS1,OMS2,OMS5,OMS7,OMS8, OMS9,OMS11,OMS12, OMS13). 1 indicates pass and 0 indicates fail."
    sql: ${TABLE}.Completeness_score;;
  }

  dimension: consistency_score {
    type: number
    label: "Consistency pass/fail"
    description:"Calculated using the least score from the individual OMS rules for consistency (OMS3,OMS4). 1 indicates pass and 0 indicates fail."
    sql:  ${TABLE}.Consistency_score ;;
  }

  dimension: count_t_nos {
    type: number
    hidden: yes
    sql: ${TABLE}.count_t_nos ;;
  }

  dimension: customer_email {
    type: string
    label: "Customer Email"
    sql: ${TABLE}.customer_email ;;
  }

  dimension: customer_phonenumber {
    type: string
    label: "Customer Phone number"
    sql: ${TABLE}.customer_phonenumber ;;
  }

  dimension: delivery_city {
    type: string
    label: "Delivery City"
    sql: ${TABLE}.delivery_city ;;
  }

  dimension: delivery_type {
    type: string
    label: "Delivery Type"
    sql: ${TABLE}.delivery_type ;;

  }

  dimension: detail_sku_code {
    type: string
    label: "Product Number"
    sql: ${TABLE}.detail_sku_code ;;
  }

  dimension: detail_sku_code_oms2 {
    type: string
    hidden: yes
    sql: ${TABLE}.detail_sku_code_oms2 ;;
  }

  dimension: detail_status {
    type: string
    label: "Order Status"
    sql: ${TABLE}.detail_status ;;
  }

  dimension_group: dq {
    type: time
    label: "Data Quality Assessment"
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.dq_date ;;
  }

  dimension: dq_row_score {
    type: number
    label: "DQ Row Score (Pass/Fail)"
    description: "Calculated using the least score from the individual OMS rules. 1 indicates pass and 0 indicates fail."
    sql: least(${completeness_score},${consistency_score},${validity_score});;
  }

  dimension: final_shipping_loc {
    type: string
    label: "Shipping location in OMS"
    sql: ${TABLE}.final_shipping_loc ;;
  }

  dimension: final_tracking_id {
    type: string
    label: "OMS Tracking no"
    description: "Same as tracking no in Carriyo and AWB in WH"
    sql: ${TABLE}.final_trackingID ;;
  }

  dimension: oms1 {
    type: number
    group_label: "OMS DQ Rules"
    description: "Order delivered to the customer should have a shipped status in OMS. 1 indicates pass and 0 indicates fail"
    sql: ${TABLE}.OMS1 ;;
  }

  dimension: oms10 {
    type: number
    group_label: "OMS DQ Rules"
    description: "Customer E-Mail should be valid in order to give visibility into the consumer.1 indicates pass and 0 indicates fail"
    sql: ${TABLE}.OMS10 ;;

  }

  dimension: oms11 {
    type: number
    group_label: "OMS DQ Rules"
    description: "Customer Phone number should not be null in order to give visibility into the consumer.1 indicates pass and 0 indicates fail"
    sql: ${TABLE}.OMS11 ;;
  }

  dimension: oms12 {
    type: number
    group_label: "OMS DQ Rules"
    description: "Every order shipped from store should have a claimed status before it is packed.1 indicates pass and 0 indicates fail"
    sql: ${TABLE}.OMS12 ;;
  }

  dimension: oms13 {
    type: number
    group_label: "OMS DQ Rules"
    description: "Every order shipped in OMS should have a collected date in Carriyo unified. 1 indicates pass and 0 indicates fail"
    sql: ${TABLE}.OMS13 ;;
  }

  dimension: oms14 {
    type: number
    group_label: "OMS DQ Rules"
    description: "Every Click&collect order should have a collected by customer after a ready for collection status. 1 indicates pass and 0 indicates fail"
    sql: ${TABLE}.OMS14 ;;
  }

  dimension: oms2 {
    type: number
    group_label: "OMS DQ Rules"
    description:"Every shipped item from WH or store has to have an a AWB. It is used for tracking the order in Carriyo. 1 indicates pass and 0 indicates fail"
    sql: ${TABLE}.OMS2 ;;
  }

  dimension: oms3 {
    type: number
    group_label: "OMS DQ Rules"
    description:"Every shipped item from WH with an AWB in OMS should have awb matching in WMS. It is used for tracking the order in Carriyo. 1 indicates pass and 0 indicates fail"
    sql: ${TABLE}.OMS3 ;;
  }

  dimension: oms4 {
    type: number
    group_label: "OMS DQ Rules"
    description: "Every item shipped from the WH, should ideally have an a AWB. 1 indicates pass and 0 indicates fail"
    sql: ${TABLE}.OMS4 ;;
  }

  dimension: oms5 {
    type: number
    group_label: "OMS DQ Rules"
    description: "Delivery city should not be null except for cancelled orders in order to know where do our maximum ecom orders come from.1 indicates pass and 0 indicates fail"
    sql: ${TABLE}.OMS5 ;;
  }

  dimension: oms7 {
    type: number
    group_label: "OMS DQ Rules"
    description: "Payment method should not be null except for cancelled orders.1 indicates pass and 0 indicates fail"
    sql: ${TABLE}.OMS7 ;;
  }

  dimension: oms8 {
    type: number
    group_label: "OMS DQ Rules"
    description: "Sales origin ID should not be NULL. 1 indicates pass and 0 indicates fail"
    sql: ${TABLE}.OMS8 ;;
  }

  dimension: oms9 {
    type: number
    group_label: "OMS DQ Rules"
    description: "Sales origin name should not be NULL. 1 indicates pass and 0 indicates fail"
    sql: ${TABLE}.OMS9 ;;
  }

  dimension_group: order {
    type: time
    label: "Customer Order"
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.order_date ;;
  }

  dimension: order_extern_orderkey {
    type: string
    sql: ${TABLE}.order_extern_orderkey ;;
    label: "External Order key"
    description: "External Order key in WH. Same as partner order reference in Carriyo"
  }

  dimension: order_id {
    type: string
    sql: ${TABLE}.order_id ;;
    label: "Order ID in OMS"
  }

  dimension: order_id_oms2 {
    type: string
    sql: ${TABLE}.order_id_oms2 ;;
    hidden: yes
  }

  dimension: order_id_wms {
    type: string
    sql: ${TABLE}.order_id_wms ;;
    label: "Order ID in WH"
  }

  dimension: partner_order_reference {
    type: string
    label: "Partner Order Reference"
    description: "Partner Order Reference number in Carriyo. Same as External Order key in WH"
    sql: ${TABLE}.partnerOrderReference ;;
  }

  dimension: payment_method {
    type: string
    sql: ${TABLE}.payment_method ;;
  }

  dimension_group: ready_for_collection {
    type: time
    label: "Ready for Collection from Store"
    description: "Ready for Collection timestamp from Store for Click&Collect orders"
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: cast(${TABLE}.ready_for_collection_at as timestamp) ;;
  }

  dimension: sales_origin_id {
    type: string
    label: "Sales Origin ID"
    sql: ${TABLE}.sales_origin_id ;;
  }

  dimension: sales_origin_name {
    type: string
    label: "Sales Origin Name"
    sql: ${TABLE}.sales_origin_name ;;
  }

  dimension_group: shipped {
    type: time
    label: "Shipped Date from WH"
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: cast(${TABLE}.shipped_date as timestamp);;
  }

  dimension: shipping_loc {
    type: string
    sql: ${TABLE}.shipping_loc ;;
  }

  dimension: t_id {
    type: string
    hidden: yes
    sql: ${TABLE}.t_id ;;
  }

  dimension: tracking_id {
    hidden: yes
    type: string
    sql: ${TABLE}.tracking_id ;;
  }

  dimension: tracking_no_carriyo {
    type: string
    label: "Tracking No"
    description: "Tracking number in Carriyo. Same as the AWB in WH"
    sql: ${TABLE}.tracking_no_carriyo ;;
  }

  dimension_group: update {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: cast( ${TABLE}.update_date as timestamp);;
  }
  measure: dq_score {
    label: "DQ PASS%"
    value_format: "0.00%"
    sql: (sum(${dq_row_score})/${count});;
  }
  measure: dq_score_pass {
    label: "DQ PASS Count"
    type: sum
    sql: case when ${dq_row_score}=1 then 1 end;;
  }
  measure: dq_fail_pass {
    label: "Total Rows with DQ Fail"
    type: sum
    sql: case when ${dq_row_score}=0 then 1 end;;
    drill_fields: [dq_fail_details*]
    filters: {
      field: dq_row_score
      value: "0"
    }
  }
  measure: completeness_fail_count {
    label: "Total Rows with Completeness Fail"
    type: sum
    sql: case when ${completeness_score}=0 then 1 end;;
    drill_fields: [completeness_fail_details*]
    filters: {
      field: completeness_score
      value: "0"
    }
  }
  measure: validity_fail_count {
    label: "Total Rows with Validity Fail"
    type: sum
    sql: case when ${validity_score}=0 then 1 end;;
    drill_fields: [validity_fail_details*]
    filters: {
      field: validity_score
      value: "0"
    }
  }
  measure: consistency_fail_count{
    label: "Total Rows with Consistency Fail"
    type: sum
    sql: case when ${consistency_score}=0 then 1 end;;
    drill_fields: [consistency_fail_details*]
    filters: {
      field: consistency_score
      value: "0"
    }
  }
  set: dq_row_details {
    fields: [brand,awb,final_shipping_loc, final_tracking_id,customer_email,customer_phonenumber,order_id,tracking_no_carriyo,detail_sku_code,  order_raw ,sales_origin_id, sales_origin_name,  payment_method, detail_status,  delivery_city,  delivery_type,collected_at_carriyo_date,  ready_for_collection_raw, collected_at_status,  collected_by_customer_raw,  dq_date,  oms1, oms2, oms3,oms4, oms5, oms7, oms8, oms9, oms10,  oms11, oms12,oms13,oms14,  completeness_score, validity_score, consistency_score, dq_row_score]
  }
  set: dq_fail_details {
    fields: [brand,awb,final_shipping_loc, final_tracking_id,customer_email,customer_phonenumber,order_id,tracking_no_carriyo,detail_sku_code,  order_raw ,sales_origin_id, sales_origin_name,  payment_method, detail_status,  delivery_city,  delivery_type,collected_at_carriyo_date,  ready_for_collection_raw, collected_at_status,  collected_by_customer_raw,  dq_date,  oms1, oms2, oms3,oms4, oms5, oms7, oms8, oms9, oms10,  oms11, oms12,oms13,oms14,  completeness_score, validity_score, consistency_score, dq_row_score]
  }
  set:consistency_fail_details {
    fields: [order_id,final_shipping_loc,brand,awb, final_tracking_id,customer_email,customer_phonenumber,tracking_no_carriyo,detail_sku_code,  order_raw ,sales_origin_id, sales_origin_name,  payment_method, detail_status,  delivery_city,  delivery_type,  dq_date,collected_at_carriyo_date,  ready_for_collection_raw, collected_at_status,  collected_by_customer_raw,  oms3,oms4,  completeness_score, validity_score, consistency_score, dq_row_score]
  }
  set:validity_fail_details {
    fields: [brand,awb, final_shipping_loc,final_tracking_id,customer_email,customer_phonenumber,order_id,tracking_no_carriyo,detail_sku_code,  order_raw ,sales_origin_id, sales_origin_name,  payment_method, detail_status,  delivery_city,  delivery_type,  dq_date,collected_at_carriyo_date,  ready_for_collection_raw, collected_at_status,  collected_by_customer_raw,  oms10,oms14,  completeness_score, validity_score, consistency_score, dq_row_score]
  }
  set:completeness_fail_details {
    fields: [brand,awb,final_shipping_loc, final_tracking_id,customer_email,customer_phonenumber,order_id,tracking_no_carriyo,detail_sku_code,  order_raw ,sales_origin_id, sales_origin_name,  payment_method, detail_status,  delivery_city,  delivery_type,  dq_date,collected_at_carriyo_date,  ready_for_collection_raw, collected_at_status,  collected_by_customer_raw,  oms1, oms2, oms5, oms7, oms8, oms9, oms11, oms12,oms13, completeness_score, validity_score, consistency_score, dq_row_score]
  }
  dimension: validity_score {
    type: number
    description:"Calculated using the least score from the individual OMS rules for Validity (OMS10, OMS14). 1 indicates pass and 0 indicates fail."
    sql: ${TABLE}.validity_score ;;
  }
  measure: completeness_score_dq {
    label: "Completeness Pass %"
    value_format: "0.00%"
    sql: (sum(${completeness_score})/${count});;
  }
  measure: validity_score_overall {
    label: "Validity Pass %"
    value_format: "0.00%"
    sql: (sum(${validity_score})/${count});;
  }
  measure: consistency_score_dq {
    label: "Consistency Pass %"
    value_format: "0.00%"
    sql: (sum(${consistency_score})/${count});;
  }
  measure: oms1_dq_percent {
    label: "OMS1 Pass %"
    value_format: "0.00%"
    sql: (sum(${oms1})/${count});;
  }
  measure: oms2_dq_percent {
    label: "OMS2 Pass %"
    value_format: "0.00%"
    sql: (sum(${oms2})/${count});;
  }
  measure: oms3_dq_percent {
    label: "OMS3 Pass %"
    value_format: "0.00%"
    sql: (sum(${oms3})/${count});;
  }
  measure: oms4_dq_percent {
    label: "OMS4 Pass %"
    value_format: "0.00%"
    sql: (sum(${oms4})/${count});;
  }
  measure: oms5_dq_percent {
    label: "OMS5 Pass %"
    value_format: "0.00%"
    sql: (sum(${oms5})/${count});;
  }
  measure: oms7_dq_percent {
    label: "OMS7 Pass %"
    value_format: "0.00%"
    sql: (sum(${oms7})/${count});;
  }
  measure: oms8_dq_percent {
    label: "OMS8 Pass %"
    value_format: "0.00%"
    sql: (sum(${oms8})/${count});;
  }
  measure: oms9_dq_percent {
    label: "OMS9 Pass %"
    value_format: "0.00%"
    sql: (sum(${oms9})/${count});;
  }
  measure: oms10_dq_percent {
    label: "OMS10 Pass %"
    value_format: "0.00%"
    sql: (sum(${oms10})/${count});;
  }
  measure: oms11_dq_percent {
    label: "OMS11 Pass %"
    value_format: "0.00%"
    sql: (sum(${oms11})/${count});;
  }
  measure: oms12_dq_percent {
    label: "OMS12 Pass %"
    value_format: "0.00%"
    sql: (sum(${oms12})/${count});;
  }
  measure: oms13_dq_percent {
    label: "OMS13 Pass %"
    value_format: "0.00%"
    sql: (sum(${oms13})/${count});;
  }
  measure: oms14_dq_percent {
    label: "OMS14 Pass %"
    value_format: "0.00%"
    sql: (sum(${oms14})/${count});;
  }

  measure: count {
    label: "Total Rows processed"
    type: count
    drill_fields: [dq_row_details*]
  }
  measure: OMS1_passed {
    label: "Count of OMS1 passed records"
    type: sum
    sql: ${oms1} ;;
  }
  measure: OMS2_passed {
    label: "Count of OMS2 passed records"
    type: sum
    sql: ${oms2} ;;
  }
  measure: OMS3_passed {
    label: "Count of OMS3 passed records"
    type: sum
    sql: ${oms3} ;;
  }
  measure: OMS4_passed {
    label: "Count of OMS4 passed records"
    type: sum
    sql: ${oms4} ;;
  }
  measure: OMS5_passed {
    label: "Count of OMS5 passed records"
    type: sum
    sql: ${oms5} ;;
  }
  measure: OMS7_passed {
    label: "Count of OMS7 passed records"
    type: sum
    sql: ${oms7} ;;
  }
  measure: OMS8_passed {
    label: "Count of OMS8 passed records"
    type: sum
    sql: ${oms8} ;;
  }
  measure: OMS9_passed {
    label: "Count of OMS9 passed records"
    type: sum
    sql: ${oms9} ;;
  }
  measure: OMS10_passed {
    label: "Count of OMS10 passed records"
    type: sum
    sql: ${oms10} ;;
  }
  measure: OMS11_passed {
    label: "Count of OMS11 passed records"
    type: sum
    sql: ${oms11} ;;
  }
  measure: OMS12_passed {
    label: "Count of OMS12 passed records"
    type: sum
    sql: ${oms12} ;;
  }
  measure: OMS13_passed {
    label: "Count of OMS13 passed records"
    type: sum
    sql: ${oms13} ;;
  }
  measure: OMS14_passed {
    label: "Count of OMS14 passed records"
    type: sum
    sql: ${oms14} ;;
  }

}
