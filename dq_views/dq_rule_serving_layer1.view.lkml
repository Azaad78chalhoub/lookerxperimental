view: dq_rule_serving_layer1 {
  sql_table_name: `chb-exp-004.DQ_Rule_C360_base.DQ_Rule_serving_layer1`
    ;;

dimension: brand_name {
  type: string
  sql: ${TABLE}.brand_name ;;
}
  dimension: c360_1 {
    type: number
    sql: ${TABLE}.C360_1 ;;
    group_label: "Score by Rule"
    description: "email or mobile should not be null"
  }

  dimension: c360_2 {
    type: number
    sql: ${TABLE}.C360_2 ;;
    group_label: "Score by Rule"
    description: "first and last name and gender and ethinicity and nationality and country of residence and residency should not be null"
  }

  dimension: c360_3 {
    type: number
    sql: ${TABLE}.C360_3 ;;
    group_label: "Score by Rule"
    description: "mobile should be in the correct format"
  }

  dimension: c360_4 {
    type: number
    sql: ${TABLE}.C360_4 ;;
    group_label: "Score by Rule"
    description: "email address should be in the correct format"
  }

  dimension: c360_5 {
    type: number
    sql: ${TABLE}.C360_5 ;;
    group_label: "Score by Rule"
    description: "ID number should be unique"
  }

  dimension: c360_6 {
    type: number
    sql: ${TABLE}.C360_6 ;;
    group_label: "Score by Rule"
    description: "mobile number should be unique "
  }

  dimension: c360_7 {
    type: number
    sql: ${TABLE}.C360_7 ;;
    group_label: "Score by Rule"
    description: "email address should be unique"
  }

  dimension: c360_8 {
    type: number
    sql: ${TABLE}.C360_8 ;;
    group_label: "Score by Rule"
    description: "Each ID number should have a unique email, mobile and name"
  }

  dimension: completeness_score {
    type: number
    sql: ${TABLE}.Completeness_Score ;;
    group_label: "Score"
    description: "C360_1 AND C360_2"
  }

  dimension: country_of_residence {
    type: string
    sql: ${TABLE}.country_of_residence ;;
    group_label: "Fields"
  }

  dimension_group: dq {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.DQ_date ;;
  }

  dimension: dq_row_score {
    type: number
    sql: ${TABLE}.DQ_score ;;
    group_label: "Score"
  }

  dimension: email_validated {
    type: string
    sql: {% if _user_attributes['customer_pii_access']  == 'yes' %}
          ${TABLE}.email_validated
      {% else %}
          concat(left(${TABLE}.email_validated,1), repeat('*', LENGTH(${TABLE}.email_validated)- length(regexp_extract(${TABLE}.email_validated, '[^@]*')) - 1),
right(${TABLE}.email_validated,LENGTH(${TABLE}.email_validated)- length(regexp_extract(${TABLE}.email_validated, '[^@]*'))))
      {% endif %}

 ;;
    group_label: "Fields"
  }

  dimension: ethnicity {
    type: string
    sql: ${TABLE}.ethnicity ;;
    group_label: "Fields"
  }

  dimension: first_name {
    type: string
    sql:  {% if _user_attributes['customer_pii_access']  == 'yes' %}
          ${TABLE}.first_name
      {% else %}
          CONCAT(left(${TABLE}.first_name, 1), REPEAT ('*', LENGTH(${TABLE}.first_name) - 1))
      {% endif %} ;;
    group_label: "Fields"
  }

  dimension: gender {
    type: string
    sql: ${TABLE}.gender ;;
    group_label: "Fields"
  }

  dimension: last_name {
    type: string
    sql:  {% if _user_attributes['customer_pii_access']  == 'yes' %}
         ${TABLE}.last_name
      {% else %}
          CONCAT(left(${TABLE}.last_name, 1), REPEAT ('*', LENGTH(${TABLE}.last_name) - 1))
      {% endif %} ;;
    group_label: "Fields"
  }

  dimension: mobile_validated {
    type: string
    sql: {% if _user_attributes['customer_pii_access']  == 'yes' %}
         ${TABLE}.mobile_validated
      {% else %}
          CONCAT(left(${TABLE}.mobile_validated, 4), REPEAT ('*', LENGTH(${TABLE}.mobile_validated) - 4))
      {% endif %};;
    group_label: "Fields"
  }

  dimension: nationality {
    type: string
    sql: ${TABLE}.nationality ;;
    group_label: "Fields"
  }

  dimension: residency {
    type: string
    sql: ${TABLE}.residency ;;
    group_label: "Fields"
  }

  dimension: speedbus_id {
    type: string
    sql: ${TABLE}.speedbus_id ;;
    group_label: "Fields"
  }

  dimension: uniqueness_score {
    type: number
    sql: ${TABLE}.Uniqueness_Score ;;
    group_label: "Score"
    description: "C360_5,C360_6,C360_7 and C360_8 "
  }

  dimension: validity_score {
    type: number
    sql: ${TABLE}.Validity_Score ;;
    group_label: "Score"
    description: "C360_3 and C360_4"
  }


  measure: count {
    label: "Total Rows processed"
    type: count

  }

  measure: dq_score_pass {
    label: "DQ PASS Count"
    type: sum
    sql: case when ${dq_row_score}=1 then 1 end;;
  }

  measure: dq_fail_pass {
    label: "Total Rows with DQ Fail"
    type: sum
    sql: case when ${dq_row_score}=0 then 1 end;;
    drill_fields: [dq_fail_details*]
    filters: {
      field: dq_row_score
      value: "0"
    }
  }

  measure: score_overall {
    label: "DQ Pass %"
    value_format: "0.00%"
    sql: (sum(${dq_row_score})/${count});;
  }

  set: dq_fail_details {
    fields: [first_name, last_name, email_validated, mobile_validated,speedbus_id,gender, ethnicity , country_of_residence,
      dq_date,  c360_1, c360_2,c360_3, c360_4, c360_5,c360_6, c360_7, c360_8,
      completeness_score, validity_score,uniqueness_score, dq_row_score]
  }
  measure: completeness_fail_pass {
    label: "Total rows with Completeness Fail"
    type: sum
    sql: case when ${completeness_score}=0 then 1 end;;
    drill_fields: [first_name, last_name, email_validated, mobile_validated, speedbus_id, gender, ethnicity,
      country_of_residence,c360_1, c360_2]
    filters: {
      field: completeness_score
      value: "0"
    }
    group_label: "Fail by Dimension"
  }

  measure: validity_fail_pass {
    label: "Total rows with Validity Fail"
    type: sum
    sql:case when ${validity_score}=0 then 1 end;;
    drill_fields:  [email_validated, mobile_validated,c360_3, c360_4]
    filters: {
      field: validity_score
      value: "0"
    }
    group_label: "Fail by Dimension"
  }

  measure: uniqueness_fail_pass {
    label: "Total rows with Uniqueness Fail"
    type: sum
    sql: case when ${uniqueness_score}=0 then 1 end;;
    drill_fields: [email_validated, mobile_validated, speedbus_id,c360_5, c360_6, c360_7,c360_8]
    filters: {
      field: uniqueness_score
      value: "0"
    }
    group_label: "Fail by Dimension"
  }

  measure: completeness_score_overall {
    label: "Completeness Pass %"
    value_format: "0.00%"
    sql: (sum(${completeness_score})/${count});;
    group_label: "Overall Score by Dimension"
  }
  measure: validity_score_overall {
    label: "Validity Pass %"
    value_format: "0.00%"
    sql: (sum(${validity_score})/${count});;
    group_label: "Overall Score by Dimension"
  }

  measure: uniqueness_score_overall {
    label: "Uniqueness Pass %"
    value_format: "0.00%"
    sql: (sum(${uniqueness_score})/${count});;
    group_label: "Overall Score by Dimension"
  }

  measure: c360_1_fail_pass {
    label: "Total rows with C360_1 Fail"
    type: sum
    sql: case when  ${c360_1}=0 then 1 end;;
    drill_fields: [ email_validated, mobile_validated,c360_1]
    filters: {
      field: c360_1
      value: "0"
    }
    group_label: "Fail by Rule"
  }

  measure: c360_2_fail_pass {
    label: "Total rows with C360_2 Fail"
    type: sum
    sql: case when  ${c360_2}=0 then 1 end;;
    drill_fields: [first_name, last_name,gender,country_of_residence, ethnicity, nationality,
      residency, c360_2]
    filters: {
      field: c360_2
      value: "0"
    }
    group_label: "Fail by Rule"
  }

  measure: c360_3_fail_pass {
    label: "Total rows with C360_3 Fail"
    type: sum
    sql: case when ${c360_3}=0 then 1 end;;
    drill_fields:  [mobile_validated,c360_3]
    filters: {
      field: c360_3
      value: "0"
    }
    group_label:"Fail by Rule"
  }

  measure: c360_4_fail_pass {
    label: "Total rows with C360_4 Fail"
    type: sum
    sql: case when ${c360_4}=0 then 1 end;;
    drill_fields:  [email_validated,c360_4]
    filters: {
      field: c360_4
      value: "0"
    }
    group_label: "Fail by Rule"
  }


  measure: c360_5_fail_pass {
    label: "Total rows with C360_5 Fail"
    type: sum
    sql: case when ${c360_5}=0 then 1 end;;
    drill_fields: [speedbus_id,first_name, last_name,email_validated,mobile_validated, c360_5]
    filters: {
      field: c360_5
      value: "0"
    }
    group_label: "Fail by Rule"
  }

  measure: c360_6_fail_pass {
    label: "Total rows with C360_6 Fail"
    type: sum
    sql: case when ${c360_6}=0 then 1 end;;
    drill_fields: [first_name, last_name,email_validated,mobile_validated, c360_6]
    filters: {
      field: c360_6
      value: "0"
    }
    group_label: "Fail by Rule"

  }

  measure: c360_7_fail_pass {
    label: "Total rows with C360_7 Fail"
    type: sum
    sql: case when ${c360_7}=0 then 1 end;;
    drill_fields: [first_name, last_name,email_validated,mobile_validated, c360_7]
    filters: {
      field: c360_7
      value: "0"
    }
    group_label: "Fail by Rule"
  }

  measure: c360_8_fail_pass {
    label: "Total rows with C360_8 Fail"
    type: sum
    sql: case when ${c360_8}=0 then 1 end;;
    drill_fields: [speedbus_id, email_validated,mobile_validated, c360_8]
    filters: {
      field: c360_8
      value: "0"
    }
    group_label: "Fail by Rule"
  }



  measure: C360_1_score_overall {
    label: "C360_1 Pass %"
    value_format: "0.00%"
    sql: (sum(${c360_1})/${count});;
    group_label: "Overall Score by Rule"
  }

  measure: C360_2_score_overall {
    label: "C360_2 Pass %"
    value_format: "0.00%"
    sql: (sum(${c360_2})/${count});;
    group_label: "Overall Score by Rule"
  }

  measure: C360_3_score_overall {
    label: "C360_3 Pass %"
    value_format: "0.00%"
    sql: (sum(${c360_3})/${count});;
    group_label: "Overall Score by Rule"
  }
  measure: C360_4_score_overall {
    label: "C360_4 Pass %"
    value_format: "0.00%"
    sql: (sum(${c360_4})/${count});;
    group_label: "Overall Score by Rule"
  }
  measure: C360_5_score_overall {
    label: "C360_5 Pass %"
    value_format: "0.00%"
    sql: (sum(${c360_5})/${count});;
    group_label: "Overall Score by Rule"
  }
  measure: C360_6_score_overall {
    label: "C360_6 Pass %"
    value_format: "0.00%"
    sql: (sum(${c360_6})/${count});;
    group_label: "Overall Score by Rule"
  }
  measure: C360_7_score_overall {
    label: "C360_7 Pass %"
    value_format: "0.00%"
    sql: (sum(${c360_7})/${count});;
    group_label: "Overall Score by Rule"
  }
  measure: C360_8_score_overall {
    label: "C360_8 Pass %"
    value_format: "0.00%"
    sql: (sum(${c360_8})/${count});;
    group_label: "Overall Score by Rule"
  }
}
