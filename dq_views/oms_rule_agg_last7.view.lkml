view: oms_rule_agg_last7 {
  sql_table_name: `chb-exp-004.DQ_Rule_Agg1.OMS_RuleAgg_last7`
    ;;

  dimension: brand {
    type: string
    sql: ${TABLE}.brand ;;
  }
  dimension: pk {
    primary_key: yes
    hidden: yes
    type: string
    sql: CONCAT(${final_tracking_id},${detail_sku_code},${order_id},${dq_date}) ;;
  }
  dimension: customer_email {
    type: string
    sql: ${TABLE}.customer_email ;;
  }

  dimension: customer_phonenumber {
    type: string
    sql: ${TABLE}.customer_phonenumber ;;
  }

  dimension: delivery_city {
    type: string
    sql: ${TABLE}.delivery_city ;;
  }

  dimension: delivery_type {
    type: string
    sql: ${TABLE}.delivery_type ;;
  }

  dimension: detail_sku_code {
    type: string
    sql: ${TABLE}.detail_sku_code ;;
  }

  dimension: detail_status {
    type: string
    sql: ${TABLE}.detail_status ;;
  }

  dimension_group: dq {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.dq_date ;;
  }

  dimension: dq_row_score {
    type: number
    sql: ${TABLE}.DQ_row_score ;;
  }

  dimension: f0_ {
    type: number
    sql: ${TABLE}.f0_ ;;
  }

  dimension: final_tracking_id {
    type: string
    sql: ${TABLE}.final_trackingID ;;
  }

  dimension: oms1 {
    type: number
    sql: ${TABLE}.OMS1 ;;
  }

  dimension: oms10 {
    type: number
    sql: ${TABLE}.OMS10 ;;
  }

  dimension: oms11 {
    type: number
    sql: ${TABLE}.OMS11 ;;
  }

  dimension: oms2 {
    type: number
    sql: ${TABLE}.OMS2 ;;
  }

  dimension: oms5 {
    type: number
    sql: ${TABLE}.OMS5 ;;
  }

  dimension: oms7 {
    type: number
    sql: ${TABLE}.OMS7 ;;
  }

  dimension: oms8 {
    type: number
    sql: ${TABLE}.OMS8 ;;
  }

  dimension: oms9 {
    type: number
    sql: ${TABLE}.OMS9 ;;
  }
  dimension: Completeness_score {
    type: number
    sql: ${TABLE}.Completeness_score ;;
  }
  dimension: validity_score {
    type: number
    sql: ${TABLE}.validity_score ;;
  }
  dimension_group: order {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.order_date ;;
  }

  dimension: order_id {
    type: string
    sql: ${TABLE}.order_id ;;
  }

  dimension: payment_method {
    type: string
    sql: ${TABLE}.payment_method ;;
  }

  dimension: sales_origin_id {
    type: number
    sql: ${TABLE}.sales_origin_id ;;
  }

  dimension: sales_origin_name {
    type: string
    sql: ${TABLE}.sales_origin_name ;;
  }

  dimension: t_id {
    type: string
    sql: ${TABLE}.t_id ;;
  }
measure: dq_score {
  label: "DQ PASS%"
  value_format: "0.00%"
  sql: (sum(${dq_row_score})/${count});;
}
  measure: dq_score_pass {
    label: "DQ PASS Count"
    type: sum
    sql: case when ${dq_row_score}=1 then 1 end;;
  }
  measure: dq_fail_pass {
    label: "Total Rows with DQ Fail"
    type: sum
    sql: case when ${dq_row_score}=0 then 1 end;;
    drill_fields: [dq_fail_details*]
    filters: {
      field: dq_row_score
      value: "0"
    }
  }
  set: dq_fail_details {
    fields: [brand,customer_email,customer_phonenumber,order_id,final_tracking_id,detail_sku_code,  order_raw ,sales_origin_id, sales_origin_name,  payment_method, detail_status,  delivery_city,  delivery_type,  dq_date,  oms1, oms2, oms5, oms7, oms8, oms9, oms10,  oms11,  Completeness_score, validity_score, dq_row_score]
  }
  measure: completeness_score {
    label: "Completeness Pass %"
    value_format: "0.00%"
    sql: (sum(${Completeness_score})/${count});;
  }
  measure: validity_score_overall {
    label: "Validity Pass %"
    value_format: "0.00%"
    sql: (sum(${validity_score})/${count});;
  }
  measure: count {
    label: "Total Rows processed"
    type: count

  }
}
