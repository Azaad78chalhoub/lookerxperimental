view: vpn_brand_zone_agg {
  sql_table_name: `chb-exp-004.DQ_MD.vpn_brand_zone_agg`
    ;;

  dimension: brand {
    type: string
    sql: ${TABLE}.brand ;;
  }

  # dimension: f0_ {
  #   type: number
  #   label: "Number of distinct Zone codes"
  #   sql: ${TABLE}.f0_ ;;
  #   drill_fields: [vpn,vpn_brand_zone.item,vpn_brand_zone.loc1,vpn_brand_zone.reportcode1]

  # }
  dimension: f0_ {
    type: number
    label: "Number of distinct Zone codes"
    sql: ${TABLE}.f0_ ;;
  }

  dimension: vpn {
    type: string
    sql: ${TABLE}.vpn ;;
    link: {
      label: "Drill by VPN"
      url: "https://chalhoubgroup.de.looker.com/looks/6097?&f[vpn_brand_zone.vpn]={{ value | url_encode }}"
    }
  }

  measure: count_distinct_vpn {
    type: count_distinct
    sql: ${vpn} ;;
    drill_fields: [vpn,brand,f0_]
  }
}
