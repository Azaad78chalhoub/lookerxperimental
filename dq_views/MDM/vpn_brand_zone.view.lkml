view: vpn_brand_zone {
  sql_table_name: `chb-exp-004.DQ_MD.vpn_brand_zone`
    ;;

  dimension: brand {
    type: string
    sql: ${TABLE}.brand ;;
  }

  dimension: item {
    type: string
    sql: ${TABLE}.item ;;
  }

  dimension: loc1 {
    type: number
    label: "Location number"
    sql: ${TABLE}.Loc1 ;;
  }

  dimension: loc2 {
    type: number
    sql: ${TABLE}.Loc2 ;;
  }

  dimension: reportcode1 {
    type: string
    label: "Zone (ReportCode)"
    sql: ${TABLE}.Reportcode1 ;;
  }

  dimension: reportcode2 {
    type: string
    sql: ${TABLE}.Reportcode2 ;;
  }

  dimension: vpn {
    type: string
    sql: ${TABLE}.vpn;;
    drill_fields: [brand,vpn,item,loc1,reportcode1]
  }
  measure: count_distinct_zone_value{
    type: count_distinct
    sql:  ${reportcode1};;
    drill_fields: [brand,vpn,item,loc1,reportcode1]
  }
# measure: count_distinct_zone{
#   type: count_distinct
#   sql:  ${reportcode1}||${vpn};;
#   value_format_name:brand
# }
  measure: count_distinct_vpn{
    type: count_distinct
    sql:  ${vpn} ;;
  }
  measure: count {
    type: count
    drill_fields: []
  }
}
