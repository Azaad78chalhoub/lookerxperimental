view: c360_dq_final {
  sql_table_name: `chb-exp-004.DQ_C360_final_score.C360_DQ_Final`
    ;;

  dimension_group: dq {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.DQ_date ;;
  }

  dimension: dq_dimension {
    type: string
    sql: ${TABLE}.DQ_dimension ;;
  }

  dimension: num_rows_passed {
    type: number
    sql: ${TABLE}.num_rows_passed ;;
  }

  dimension: num_rows_processed {
    type: number
    sql: ${TABLE}.num_rows_processed ;;
  }

  dimension: rule_name {
    type: string
    sql: ${TABLE}.rule_name ;;
  }

  dimension: source_name {
    type: string
    sql: ${TABLE}.source_name ;;
  }

  dimension: rule_definition {
    type: string
    sql: ${TABLE}.rule_definition ;;
  }

  measure: count {
    type: count
    drill_fields: [dq_dimension]
  }

  measure: rows_passed {
    type: sum
    sql: ${num_rows_passed} ;;
  }

  measure: rows_processed {
    type: sum
    sql: ${num_rows_processed} ;;
  }

  measure: dq_score {
    label: "DQ pass %"
    type: number
    drill_fields: [num_rows_passed, num_rows_processed]
    sql: (${rows_passed}/${rows_processed})*100 ;;
    value_format: "0.00\%"
  }

  measure: dq_fail_score {
    label: "DQ fail %"
    type: number
    drill_fields: [num_rows_passed, num_rows_processed]
    sql: ((${rows_processed}-${rows_passed})/${rows_processed})*100  ;;
    html:  {{rendered_value}} ||  {{rule_definition}}  ;;  ## here we use || to concatenate the values
    value_format: "0.00\%"
  }




}
