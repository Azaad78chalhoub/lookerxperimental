view: dq_source_full {
  sql_table_name: `DQ_Rule_C360_base.DQ_source_full`
    ;;

  dimension: brand_name {
    type: string
    sql: ${TABLE}.brand_name ;;
  }

  dimension: c360_1 {
    type: number
    sql: ${TABLE}.C360_1 ;;
    group_label: "Score by Rule"
    description: "email or mobile should not be null"
  }

  dimension: c360_2 {
    type: number
    sql: ${TABLE}.C360_2 ;;
    group_label: "Score by Rule"
    description: "first and last name and gender and ethinicity and nationality and country of residence and residency should not be null"

  }

  dimension: c360_3 {
    type: number
    sql: ${TABLE}.C360_3 ;;
    group_label: "Score by Rule"
    description: "mobile should be in the correct format"
  }

  dimension: c360_4 {
    type: number
    sql: ${TABLE}.C360_4 ;;
    group_label: "Score by Rule"
    description: "email should be in the correct format"
  }

  dimension: c360_5 {
    type: number
    sql: ${TABLE}.C360_5 ;;
    group_label: "Score by Rule"
    description: "ID number should be unique"

  }

  dimension: c360_6 {
    type: number
    sql: ${TABLE}.C360_6 ;;
    group_label: "Score by Rule"
    description: "mobile number should be unique "
  }

  dimension: c360_7 {
    type: number
    sql: ${TABLE}.C360_7 ;;
    group_label: "Score by Rule"
    description: "email should be unique "
  }

  dimension: c360_8 {
    type: number
    sql: ${TABLE}.C360_8 ;;
    group_label: "Score by Rule"
    description: "Each ID number should have a unique email, mobile and name"

  }

  dimension: c360_9 {
    type: number
    sql: ${TABLE}.C360_9 ;;
    group_label: "Score by Rule"
    description: "Each email should have a unique mobile and name"
  }

  dimension: contactability_rule{
    type: number
    sql: ${TABLE}.C360_10 ;;
    description: "Customer is contactable - either email or mobile is complete and valid"
  }

  dimension: completeness_score {
    type: number
    sql: ${TABLE}.Completeness_Score ;;
    group_label: "Score"
    description: "C360_1 and C360_2"
  }

  dimension: country_of_residence {
    type: string
    sql: ${TABLE}.country_of_residence ;;
    group_label: "Fields"
  }

  dimension_group: dq {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.DQ_date ;;
  }

  dimension: dq_row_score {
    type: number
    sql: ${TABLE}.DQ_score ;;
    group_label: "Score"
  }

  dimension: email {
    type: string
    sql:{% if _user_attributes['customer_pii_access']  == 'yes' %}
          ${TABLE}.email
      {% else %}
          concat(left(${TABLE}.email,1), repeat('*', abs(LENGTH(${TABLE}.email)- length(regexp_extract(${TABLE}.email, '[^@]*')) - 1)),
right(${TABLE}.email,LENGTH(${TABLE}.email)- length(regexp_extract(${TABLE}.email, '[^@]*'))))
      {% endif %};;
    group_label: "Fields"
  }

  dimension: ethnicity {
    type: string
    sql: ${TABLE}.ethnicity ;;
    group_label: "Fields"
  }

  dimension: first_name {
    type: string
    sql: {% if _user_attributes['customer_pii_access']  == 'yes' %}
          ${TABLE}.first_name
      {% else %}
          CONCAT(left(${TABLE}.first_name, 1), REPEAT ('*', LENGTH(${TABLE}.first_name) - 1))
      {% endif %}  ;;
    group_label: "Fields"
  }

  dimension: gender {
    type: string
    sql: ${TABLE}.gender ;;
    group_label: "Fields"
  }

  dimension: last_name {
    type: string
    sql: {% if _user_attributes['customer_pii_access']  == 'yes' %}
          ${TABLE}.last_name
      {% else %}
          CONCAT(left(${TABLE}.last_name, 1), REPEAT ('*', LENGTH(${TABLE}.last_name) - 1))
      {% endif %} ;;
    group_label: "Fields"
  }

  dimension: mobile {
    type: string
    sql: {% if _user_attributes['customer_pii_access']  == 'yes' %}
         ${TABLE}.mobile
      {% else %}
          CONCAT(left(${TABLE}.mobile, 4), REPEAT ('*', abs(LENGTH(${TABLE}.mobile) - 4)))
      {% endif %} ;;
    group_label: "Fields"
  }

  dimension: muse_member_id {
    type: number
    sql: ${TABLE}.muse_member_id ;;
    group_label: "Fields"
  }

  dimension: nationality {
    type: string
    sql: ${TABLE}.nationality ;;
    group_label: "Fields"
  }

  dimension: orco_pos_id {
    type: string
    sql: ${TABLE}.orco_pos_id ;;
    group_label: "Fields"
  }

  dimension: residency {
    type: string
    sql: ${TABLE}.residency ;;
    group_label: "Fields"
  }

  dimension: speedbus_id {
    type: string
    sql: ${TABLE}.speedbus_id ;;
    group_label: "Fields"
  }

  dimension: source_name {
    type: string
    sql: ${TABLE}.table_name ;;
  }

  dimension: uniqueness_score {
    type: number
    sql: ${TABLE}.Uniqueness_Score ;;
    group_label: "Score"
    description: "C360_5 and C360_6 and C360_7 and C360_8 and C360_9"
  }

  dimension: validity_score {
    type: number
    sql: ${TABLE}.Validity_Score ;;
    group_label: "Score"
    description: "C360_3 and C360_4"
  }

  dimension: xcenter_pos_id {
    type: string
    sql: ${TABLE}.xcenter_pos_id ;;
    group_label: "Fields"
  }

  dimension: name_completeness {
    type: number
    sql: ${TABLE}.name_completeness ;;
    group_label: "Completeness by Field"
  }

  dimension: email_completeness {
    type: number
    sql: ${TABLE}.email_completeness ;;
    group_label: "Completeness by Field"
  }

  dimension: mobile_completeness {
    type: number
    sql: ${TABLE}.mobile_completeness ;;
    group_label: "Completeness by Field"
  }

  dimension: gender_completeness {
    type: number
    sql: ${TABLE}.gender_completeness ;;
    group_label: "Completeness by Field"
  }

  dimension: residency_completeness {
    type: number
    sql: ${TABLE}.residency_completeness ;;
    group_label: "Completeness by Field"
  }

  dimension: country_completeness {
    type: number
    sql: ${TABLE}.country_completeness ;;
    group_label: "Completeness by Field"
  }

  dimension: nationality_completeness {
    type: number
    sql: ${TABLE}.nationality_completeness ;;
    group_label: "Completeness by Field"
  }

  dimension: ethnicity_completeness {
    type: number
    sql: ${TABLE}.ethnicity_completeness ;;
    group_label: "Completeness by Field"
  }

  measure: count {
    label: "Total Rows processed"
    type: count

  }

  measure: dq_score_pass {
    label: "DQ PASS Count"
    type: sum
    sql: case when ${dq_row_score}=1 then 1 end;;
  }

  measure: dq_overall_score {
    label: "DQ Pass %"
    value_format: "0.00%"
    sql: (sum(${dq_row_score})/${count});;
  }

  measure: dq_fail_pass {
    label: "Total Rows with DQ Fail"
    type: sum
    sql: case when ${dq_row_score}=0 then 1 end;;
    drill_fields: [dq_fail_details*]
    filters: {
      field: dq_row_score
      value: "0"
    }
  }

  set: dq_fail_details {
    fields: [brand_name, first_name, last_name, email, mobile,speedbus_id,muse_member_id, orco_pos_id,
      xcenter_pos_id, gender, country_of_residence, ethnicity, nationality, residency,
      dq_date,  c360_1, c360_2,c360_3, c360_4, c360_5,c360_6, c360_7, c360_8,c360_9,contactability_rule,
      completeness_score, validity_score,uniqueness_score, dq_row_score]
  }


  measure: completeness_fail_pass {
    label: "Total rows with Completeness Fail"
    type: sum
    sql: case when  ${completeness_score}=0 then 1 end;;
    drill_fields: [first_name, last_name, email, mobile, speedbus_id, gender,
      country_of_residence,c360_1, c360_2]
    filters: {
      field: completeness_score
      value: "0"
    }
    group_label: "Fail by Dimension"
  }

  measure: validity_fail_pass {
    label: "Total rows with Validity Fail"
    type: sum
    sql: case when ${validity_score}=0 then 1 end;;
    drill_fields:  [email, mobile,c360_3, c360_4]
    filters: {
      field: validity_score
      value: "0"
    }
    group_label: "Fail by Dimension"
  }

  measure: uniqueness_fail_pass {
    label: "Total rows with Uniqueness Fail"
    type: sum
    sql: case when ${uniqueness_score}=0 then 1 end;;
    drill_fields: [email, mobile, speedbus_id, xcenter_pos_id, orco_pos_id, c360_5, c360_6, c360_7,c360_8, c360_9]
    filters: {
      field: uniqueness_score
      value: "0"
    }
    group_label: "Fail by Dimension"
  }

  measure: completeness_score_overall {
    label: "Completeness Pass %"
    value_format: "0.00%"
    sql: (sum(${completeness_score})/${count});;
    group_label: "Overall Score by Dimension"
  }
  measure: validity_score_overall {
    label: "Validity Pass %"
    value_format: "0.00%"
    sql: (sum(${validity_score})/${count});;
    group_label: "Overall Score by Dimension"

  }

  measure: uniqueness_score_overall {
    label: "Uniqueness Pass %"
    value_format: "0.00%"
    sql: (sum(${uniqueness_score})/${count});;
    group_label: "Overall Score by Dimension"
  }


  measure: c360_1_fail_pass {
    label: "Total rows with C360_1 Fail"
    type: sum
    sql: case when  ${c360_1}=0 then 1 end;;
    drill_fields: [email, mobile,c360_1]
    filters: {
      field: c360_1
      value: "0"
    }
    group_label: "Fail by Rule"
  }
  measure: c360_2_fail_pass {
    label: "Total rows with C360_2 Fail"
    type: sum
    sql: case when  ${c360_2}=0 then 1 end;;
    drill_fields: [first_name, last_name,gender,country_of_residence,c360_2]
    filters: {
      field: c360_2
      value: "0"
    }
    group_label: "Fail by Rule"
  }
  measure: c360_3_fail_pass {
    label: "Total rows with C360_3 Fail"
    type: sum
    sql: case when ${c360_3}=0 then 1 end;;
    drill_fields:  [mobile,c360_3]
    filters: {
      field: c360_3
      value: "0"
    }
    group_label: "Fail by Rule"
  }
  measure: c360_4_fail_pass {
    label: "Total rows with C360_4 Fail"
    type: sum
    sql: case when ${c360_4}=0 then 1 end;;
    drill_fields:  [email,c360_4]
    filters: {
      field: c360_4
      value: "0"
    }
    group_label: "Fail by Rule"
  }
  measure: c360_5_fail_pass {
    label: "Total rows with C360_5 Fail"
    type: sum
    sql: case when ${c360_5}=0 then 1 end;;
    drill_fields: [speedbus_id, xcenter_pos_id, orco_pos_id,muse_member_id,first_name, last_name,email,mobile, c360_5]
    filters: {
      field: c360_5
      value: "0"
    }
    group_label: "Fail by Rule"
  }
  measure: c360_6_fail_pass {
    label: "Total rows with C360_6 Fail"
    type: sum
    sql: case when ${c360_6}=0 then 1 end;;
    drill_fields: [first_name, last_name,email,mobile, c360_6]
    filters: {
      field: c360_6
      value: "0"
    }
    group_label: "Fail by Rule"
  }
  measure: c360_7_fail_pass {
    label: "Total rows with C360_7 Fail"
    type: sum
    sql: case when ${c360_7}=0 then 1 end;;
    drill_fields: [first_name, last_name,email,mobile, c360_7]
    filters: {
      field: c360_7
      value: "0"
    }
    group_label: "Fail by Rule"
  }
  measure: c360_8_fail_pass {
    label: "Total rows with C360_8 Fail"
    type: sum
    sql: case when ${c360_8}=0 then 1 end;;
    drill_fields: [speedbus_id, muse_member_id, orco_pos_id,xcenter_pos_id,email,mobile, c360_8]
    filters: {
      field: c360_8
      value: "0"
    }
    group_label: "Fail by Rule"
  }
  measure: c360_9_fail_pass {
    label: "Total rows with C360_9 Fail"
    type: sum
    sql: case when ${c360_9}=0 then 1 end;;
    drill_fields: [first_name, last_name,email,mobile, c360_9]
    filters: {
      field: c360_9
      value: "0"
    }
    group_label: "Fail by Rule"
  }
  measure: c360_10_fail_pass {
    label: "Total rows with Contactability Fail"
    type: sum
    sql: case when ${contactability_rule}=0 then 1 end;;
    drill_fields: [email,mobile, contactability_rule]
    filters: {
      field: contactability_rule
      value: "0"
    }
    group_label: "Fail by Rule"
  }



  measure: C360_1_score_overall {
    label: "C360_1 Pass %"
    value_format: "0.00%"
    sql: (sum(${c360_1})/${count});;
    group_label: "Overall Pass Score by Rule"
  }

  measure: C360_2_score_overall {
    label: "C360_2 Pass %"
    value_format: "0.00%"
    sql: (sum(${c360_2})/${count});;
    group_label: "Overall Pass Score by Rule"
  }

  measure: C360_3_score_overall {
    label: "C360_3 Pass %"
    value_format: "0.00%"
    sql: (sum(${c360_3})/${count});;
    group_label: "Overall Pass Score by Rule"
  }
  measure: C360_4_score_overall {
    label: "C360_4 Pass %"
    value_format: "0.00%"
    sql: (sum(${c360_4})/${count});;
    group_label: "Overall Pass Score by Rule"
  }
  measure: C360_5_score_overall {
    label: "C360_5 Pass %"
    value_format: "0.00%"
    sql: (sum(${c360_5})/${count});;
    group_label: "Overall Pass Score by Rule"
  }
  measure: C360_6_score_overall {
    label: "C360_6 Pass %"
    value_format: "0.00%"
    sql: (sum(${c360_6})/${count});;
    group_label: "Overall Pass Score by Rule"
  }
  measure: C360_7_score_overall {
    label: "C360_7 Pass %"
    value_format: "0.00%"
    sql: (sum(${c360_7})/${count});;
    group_label: "Overall Pass Score by Rule"
  }
  measure: C360_8_score_overall {
    label: "C360_8 Pass %"
    value_format: "0.00%"
    sql: (sum(${c360_8})/${count});;
    group_label: "Overall Pass Score by Rule"
  }
  measure: C360_9_score_overall {
    label: "C360_9 Pass %"
    value_format: "0.00%"
    sql: (sum(${c360_9})/${count});;
    group_label: "Overall Pass Score by Rule"
  }

  measure: C360_10_score_overall {
    label: "Contactability Pass %"
    value_format: "0.00%"
    sql: (sum(${contactability_rule})/${count});;
    group_label: "Overall Pass Score by Rule"
  }

  measure: name_completeness_score {
    label: "Name Completeness %"
    value_format: "0.00%"
    sql: (sum(${name_completeness})/${count});;
    group_label: "Overall Score by Field"
  }

  measure: email_completeness_score {
    label: "Email Completeness %"
    value_format: "0.00%"
    sql: (sum(${email_completeness})/${count});;
    group_label: "Overall Score by Field"
  }

  measure: mobile_completeness_score {
    label: "Mobile Completeness %"
    value_format: "0.00%"
    sql: (sum(${mobile_completeness})/${count});;
    group_label: "Overall Score by Field"
  }


  measure: gender_completeness_score {
    label: "Gender Completeness %"
    value_format: "0.00%"
    sql: (sum(${gender_completeness})/${count});;
    group_label: "Overall Score by Field"
  }


  measure: residency_completeness_score {
    label: "Residency Completeness %"
    value_format: "0.00%"
    sql: (sum(${residency_completeness})/${count});;
    group_label: "Overall Score by Field"
  }


  measure: nationality_completeness_score {
    label: "Nationality Completeness %"
    value_format: "0.00%"
    sql: (sum(${nationality_completeness})/${count});;
    group_label: "Overall Score by Field"
  }

  measure: country_completeness_score {
    label: "Country of Residence Completeness %"
    value_format: "0.00%"
    sql: (sum(${mobile_completeness})/${count});;
    group_label: "Overall Score by Field"
  }

  measure: ethnicity_completeness_score {
    label: "Ethnicity Completeness %"
    value_format: "0.00%"
    sql: (sum(${ethnicity_completeness})/${count});;
    group_label: "Overall Score by Field"
  }
}
