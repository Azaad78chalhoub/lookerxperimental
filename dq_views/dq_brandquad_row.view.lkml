view: dq_brandquad_row {
  sql_table_name: `chb-exp-004.DQ_BrandQuad.dq_brandquad_row`
    ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: string
    sql: ${TABLE}.id ;;
  }

  dimension: abstract_level {
    type: string
    sql: ${TABLE}.abstract_level ;;
  }

  dimension: apparel_style {
    type: string
    sql: ${TABLE}.apparel_style ;;
  }

  dimension: attribute_sets {
    type: string
    sql: ${TABLE}.attribute_sets ;;
  }

  dimension: barcode {
    type: string
    sql: ${TABLE}.barcode ;;
  }

  dimension: bin_number {
    type: string
    sql: ${TABLE}.bin_number ;;
  }

  dimension: brand {
    type: string
    sql: ${TABLE}.brand ;;
  }

  dimension: category_depth_level0 {
    type: string
    sql: ${TABLE}.category_depth_level0 ;;
  }

  dimension: category_depth_level1 {
    type: string
    sql: ${TABLE}.category_depth_level1 ;;
  }

  dimension: category_depth_level2 {
    type: string
    sql: ${TABLE}.category_depth_level2 ;;
  }

  dimension: category_depth_level3 {
    type: string
    sql: ${TABLE}.category_depth_level3 ;;
  }

  dimension: color {
    type: string
    sql: ${TABLE}.color ;;
  }

  dimension: completeness_score {
    type: number
    sql: ${TABLE}.completeness_score ;;
  }

  dimension: consignment_flag {
    type: string
    sql: ${TABLE}.consignment_flag ;;
  }

  dimension: consistency_score {
    type: number
    sql: ${TABLE}.consistency_score ;;
  }

  dimension: core_sku {
    type: string
    sql: ${TABLE}.core_sku ;;
  }

  dimension: cost_price_per_unit {
    type: string
    sql: ${TABLE}.cost_price_per_unit ;;
  }

  dimension: country_of_origin {
    type: string
    sql: ${TABLE}.country_of_origin ;;
  }

  dimension: cover {
    type: string
    sql: ${TABLE}.cover ;;
  }

  dimension: create_date_bq {
    type: string
    sql: ${TABLE}.create_date_bq ;;
  }

  dimension: create_datetime {
    type: string
    sql: ${TABLE}.create_datetime ;;
  }

  dimension: currency {
    type: string
    sql: ${TABLE}.currency ;;
  }

  dimension: division {
    type: string
    sql: ${TABLE}.division ;;
  }

  dimension_group: dq {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.dq_date ;;
  }

  dimension: dq_score {
    type: number
    sql: ${TABLE}.dq_score ;;
  }

  dimension: enable_rma {
    type: string
    sql: ${TABLE}.enable_rma ;;
  }

  dimension: gender {
    type: string
    sql: ${TABLE}.gender ;;
  }

  dimension: heel_hight {
    type: string
    sql: ${TABLE}.heel_hight ;;
  }

  dimension: heel_size {
    type: string
    sql: ${TABLE}.heel_size ;;
  }

  dimension: hs_code {
    type: string
    sql: ${TABLE}.hs_code ;;
  }

  dimension: in_stock {
    type: string
    sql: ${TABLE}.in_stock ;;
  }

  dimension: infomodel {
    type: number
    sql: ${TABLE}.infomodel ;;
  }

  dimension: last_update_date {
    type: string
    sql: ${TABLE}.last_update_date ;;
  }

  dimension: long_description {
    type: string
    sql: ${TABLE}.long_description ;;
  }

  dimension: material {
    type: string
    sql: ${TABLE}.material ;;
  }

  dimension: material_composition {
    type: string
    sql: ${TABLE}.material_composition ;;
  }

  dimension: name {
    type: string
    sql: ${TABLE}.name ;;
  }

  dimension: non_purchasable {
    type: string
    sql: ${TABLE}.non_purchasable ;;
  }

  dimension: oracle_sku {
    type: string
    sql: ${TABLE}.oracle_sku ;;
  }

  dimension: oracle_store {
    type: string
    sql: ${TABLE}.oracle_store ;;
  }

  dimension: origin_country_id {
    type: string
    sql: ${TABLE}.origin_country_id ;;
  }

  dimension: parent_product {
    type: string
    sql: ${TABLE}.parent_product ;;
  }

  dimension: pim_bq_1 {
    type: number
    description: "Products with null Created Date"
    sql: ${TABLE}.pim_bq_1 ;;
  }

  dimension: pim_bq_2 {
    type: number
    description: "Products which are received physically but with null Received Date "
    sql: ${TABLE}.pim_bq_2 ;;
  }

  dimension: pim_bq_3 {
    type: number
    description: "Products which have completed photo shoot but with null Photo Shoot Date "
    sql: ${TABLE}.pim_bq_3 ;;
  }

  dimension: pim_bq_4 {
    type: number
    description: "Products which have completed photo edit but with null Photo Edit Date "
    sql: ${TABLE}.pim_bq_4 ;;
  }

  dimension: pim_bq_5 {
    type: number
    description: "Products with invalid create date(15-YAN-2021,44219 etc)"
    sql: ${TABLE}.pim_bq_5 ;;
  }

  dimension: pim_bq_6 {
    type: number
    description: "Products with invalid received date(15-YAN-2021,44219 etc)"
    sql: ${TABLE}.pim_bq_6 ;;
  }

  dimension: pim_bq_7 {
    type: number
    description: "Products with invalid photo shoot date(15-YAN-2021,44219 etc)"
    sql: ${TABLE}.pim_bq_7 ;;
  }

  dimension: pim_bq_8 {
    type: number
    description: "Products with invalid photo edit date(15-YAN-2021,44219 etc)"
    sql: ${TABLE}.pim_bq_8 ;;
  }

  dimension: pim_bq_9 {
    type: number
    description: "Products with stock in RMS but not present or Ecom Approved in PIM"
    sql: ${TABLE}.pim_bq_9 ;;
  }

  dimension: product_images {
    type: string
    sql: ${TABLE}.product_images ;;
  }

  dimension: product_images_completed {
    type: string
    sql: ${TABLE}.product_images_completed ;;
  }

  dimension: product_model {
    type: string
    sql: ${TABLE}.product_model ;;
  }

  dimension: product_name {
    type: string
    sql: ${TABLE}.product_name ;;
  }

  dimension: product_status {
    type: string
    sql: ${TABLE}.product_status ;;
  }

  dimension: progress_value {
    type: number
    sql: ${TABLE}.progress_value ;;
  }

  dimension: received_date {
    type: string
    sql: ${TABLE}.received_date ;;
  }

  dimension: recurrence {
    type: string
    sql: ${TABLE}.recurrence ;;
  }

  dimension: retail_price_uae {
    type: string
    sql: ${TABLE}.retail_price_uae ;;
  }

  dimension: retail_price_usd {
    type: string
    sql: ${TABLE}.retail_price_usd ;;
  }

  dimension: return_policy {
    type: string
    sql: ${TABLE}.return_policy ;;
  }

  dimension: season {
    type: string
    sql: ${TABLE}.season ;;
  }

  dimension: shopping_feeds {
    type: string
    sql: ${TABLE}.shopping_feeds ;;
  }

  dimension: short_description {
    type: string
    sql: ${TABLE}.short_description ;;
  }

  dimension: size_and_fit_information {
    type: string
    sql: ${TABLE}.size_and_fit_information ;;
  }

  dimension: status {
    type: string
    sql: ${TABLE}.status ;;
  }

  dimension: std_size {
    type: string
    sql: ${TABLE}.std_size ;;
  }

  dimension: std_size_temp {
    type: string
    sql: ${TABLE}.std_size_temp ;;
  }

  dimension: store {
    type: string
    sql: ${TABLE}.store ;;
  }

  dimension: styleoraclenumber {
    type: string
    sql: ${TABLE}.styleoraclenumber ;;
  }

  dimension: supplier {
    type: string
    sql: ${TABLE}.supplier ;;
  }

  dimension: supplier_class {
    type: string
    sql: ${TABLE}.supplier_class ;;
  }

  dimension: supplier_colour_code {
    type: string
    sql: ${TABLE}.supplier_colour_code ;;
  }

  dimension: supplier_colour_name {
    type: string
    sql: ${TABLE}.supplier_colour_name ;;
  }

  dimension: supplier_size {
    type: string
    sql: ${TABLE}.supplier_size ;;
  }

  dimension: supplier_subclass {
    type: string
    sql: ${TABLE}.supplier_subclass ;;
  }

  dimension: tax_class {
    type: string
    sql: ${TABLE}.tax_class ;;
  }

  dimension: taxonomy {
    type: string
    sql: ${TABLE}.taxonomy ;;
  }

  dimension: taxonomy_1 {
    type: string
    sql: ${TABLE}.taxonomy_1 ;;
  }

  dimension: taxonomy_2 {
    type: string
    sql: ${TABLE}.taxonomy_2 ;;
  }

  dimension: theme {
    type: string
    sql: ${TABLE}.theme ;;
  }

  dimension: timestamp {
    type: number
    sql: ${TABLE}.timestamp ;;
  }

  dimension: type {
    type: string
    sql: ${TABLE}.type ;;
  }

  dimension: untouched_images {
    type: string
    sql: ${TABLE}.untouched_images ;;
  }

  dimension: untouched_images_completed {
    type: string
    sql: ${TABLE}.untouched_images_completed ;;
  }

  dimension_group: updated {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated ;;
  }

  dimension: usage_specificity {
    type: string
    sql: ${TABLE}.usage_specificity ;;
  }

  dimension: validity_score {
    type: number
    sql: ${TABLE}.validity_score ;;
  }

  dimension: version {
    type: number
    sql: ${TABLE}.version ;;
  }

  dimension: video {
    type: string
    sql: ${TABLE}.video ;;
  }

  dimension: visibility {
    type: string
    sql: ${TABLE}.visibility ;;
  }

  dimension: vpn {
    type: string
    sql: ${TABLE}.vpn ;;
  }

  dimension: websites {
    type: string
    sql: ${TABLE}.websites ;;
  }

  dimension: weight {
    type: string
    sql: ${TABLE}.weight ;;
  }

  dimension: zone_code {
    type: string
    sql: ${TABLE}.zone_code ;;
  }

  dimension: rms_item {
    type: string
    sql: ${TABLE}.rms_item ;;
  }

  dimension: rms_stock_on_hand {
    type: number
    sql: ${TABLE}.rms_stock_on_hand ;;
  }


  set: dq_fail_dtls {
    fields: [id,name,version,updated_date,type,abstract_level,infomodel,product_model,parent_product,cover,timestamp,status,taxonomy,progress_value,vpn,brand,product_name,short_description,long_description,gender,taxonomy_1,taxonomy_2,color,material,season,division,zone_code,std_size,std_size_temp,size_and_fit_information,heel_hight,heel_size,usage_specificity,consignment_flag,create_date_bq,create_datetime,last_update_date,received_date,retail_price_uae,retail_price_usd,oracle_store,in_stock,return_policy,untouched_images_completed,product_images_completed,styleoraclenumber,oracle_sku,core_sku,barcode,store,product_status,bin_number,supplier_colour_name,country_of_origin,origin_country_id,hs_code,supplier_colour_code,supplier_size,recurrence,theme,material_composition,apparel_style,supplier_class,supplier_subclass,supplier,cost_price_per_unit,currency,non_purchasable,visibility,tax_class,weight,enable_rma,websites,shopping_feeds,attribute_sets,product_images,untouched_images,video,category_depth_level0,category_depth_level1,category_depth_level2,category_depth_level3,pim_bq_1,pim_bq_2,pim_bq_3,pim_bq_4,pim_bq_5,pim_bq_6,pim_bq_7,pim_bq_8,dq_score,completeness_score,validity_score,dq_date]
  }

  set: dq_completeness_dtls {
    fields: [id,name,status,create_date_bq,received_date,untouched_images_completed,product_images_completed,pim_bq_1,pim_bq_2,pim_bq_3,pim_bq_4,completeness_score]
  }

  set: dq_validity_dtls {
    fields: [id,name,status,create_date_bq,received_date,untouched_images_completed,product_images_completed,pim_bq_5,pim_bq_6,pim_bq_7,pim_bq_8,validity_score]
    }

  set: dq_consistency_dtls {
    fields: [rms_item,rms_stock_on_hand, id,name,status,core_sku,oracle_sku,pim_bq_9,consistency_score]
  }


  measure: count {
    type: count
    drill_fields: [id, product_name, name, supplier_colour_name]
  }

  measure: tot_num_rows {
    label: "Rows Processed"
    type:  count
    drill_fields: ["dq_fail_dtls*"]


  }

  measure: pass_num_rows {
    label: "Rows Passed"
    type:  sum
    sql: ${dq_score} ;;
    drill_fields: ["dq_fail_dtls*"]


  }

  measure: fail_num_rows {
    label: "Rows Failed"
    type:  count
    filters: [dq_score: "0"]
    drill_fields: ["dq_fail_dtls*"]
  }

  measure: pass_score {
    label: "DQ Pass Score"
    type: number
    sql: safe_divide(${pass_num_rows} , ${tot_num_rows}) ;;
    value_format_name: percent_2
  }

  measure: complete_rows {
    label: "Rows Complete"
    type:  count
    filters: [completeness_score:  "1"]

  }

  measure: incomplete_rows {
    label: "Rows Incomplete"
    type:  count
    filters: [completeness_score:  "0"]
    drill_fields: ["dq_completeness_dtls*"]
  }


  measure: completeness_dq_score {
    label: "Completeness Score"
    type: number
    sql: safe_divide(${complete_rows} , ${tot_num_rows}) ;;
    value_format_name: percent_2
  }

  measure: valid_rows {
    label: "Rows Valid"
    type:  count
    filters: [validity_score:   "1"]
  }

  measure: invalid_rows {
    label: "Rows Invalid"
    type:  count
    filters: [validity_score:   "0"]
    drill_fields: ["dq_validity_dtls*"]

  }


  measure: validity_dq_score {
    label: "Validity Score"
    type: number
    sql: safe_divide(${valid_rows} , ${tot_num_rows}) ;;
    value_format_name: percent_2
  }


  measure: consistent_rows {
    label: "Rows Consistent"
    type:  count
    filters: [consistency_score:   "1"]
  }

  measure: inconsistent_rows {
    label: "Rows Inconsistent"
    type:  count
    filters: [consistency_score:   "0"]
    drill_fields: ["dq_consistency_dtls*"]

  }


  measure: consistency_dq_score {
    label: "Consistency Score"
    type: number
    sql: safe_divide(${consistent_rows} , ${tot_num_rows}) ;;
    value_format_name: percent_2
  }


  measure: pim_bq_1_failed_rows {
    label: "PIM BQ 1 Failed"
    description: "# Products with null Created Date"
    type: count
    filters: [pim_bq_1: "0"]
    drill_fields: ["dq_completeness_dtls*"]

  }

  measure: pim_bq_1_failed_perc {
    label: "PIM BQ 1 Failed %"
    description: "% Products with null Created Date"
    type: number
    sql:  safe_divide(${pim_bq_1_failed_rows},${tot_num_rows});;
    value_format_name: percent_2


  }

  measure: pim_bq_2_failed_rows {
    label: "PIM BQ 2 Failed"
    description: "# Products which are received physically but with null Received Date "
    type: count
    filters: [pim_bq_2: "0"]
    drill_fields: ["dq_completeness_dtls*"]

  }

  measure: pim_bq_2_failed_perc {
    label: "PIM BQ 2 Failed %"
    description: "% Products which are received physically but with null Received Date "
    type: number
    sql:  safe_divide(${pim_bq_2_failed_rows},${tot_num_rows});;
    value_format_name: percent_2

  }


  measure: pim_bq_3_failed_rows {
    label: "PIM BQ 3 Failed"
    description: "# Products which have completed photo shoot but with null Photo Shoot Date "
    type: count
    filters: [pim_bq_3: "0"]
    drill_fields: ["dq_completeness_dtls*"]


  }

  measure: pim_bq_3_failed_perc {
    label: "PIM BQ 3 Failed %"
    description: "% Products which have completed photo shoot but with null Photo Shoot Date "
    type: number
    sql:  safe_divide(${pim_bq_3_failed_rows},${tot_num_rows});;
    value_format_name: percent_2

  }

  measure: pim_bq_4_failed_rows {
    label: "PIM BQ 4 Failed"
    description: "# Products which have completed photo edit but with null Photo Edit Date "
    type: count
    filters: [pim_bq_4: "0"]
    drill_fields: ["dq_completeness_dtls*"]

  }

  measure: pim_bq_4_failed_perc {
    label: "PIM BQ 4 Failed %"
    description: "% Products which have completed photo edit but with null Photo Edit Date "
    type: number
    sql:  safe_divide(${pim_bq_4_failed_rows},${tot_num_rows});;
    value_format_name: percent_2

  }

  measure: pim_bq_5_failed_rows {
    label: "PIM BQ 5 Failed"
    description: "# Products with invalid create date(15-YAN-2021,44219 etc)"
    type: count
    filters: [pim_bq_5: "0"]
    drill_fields: ["dq_validity_dtls*"]

  }

  measure: pim_bq_5_failed_perc {
    label: "PIM BQ 5 Failed %"
    description: "% Products with invalid create date(15-YAN-2021,44219 etc)"
    type: number
    sql:  safe_divide(${pim_bq_5_failed_rows},${tot_num_rows});;
    value_format_name: percent_2

  }


  measure: pim_bq_6_failed_rows {
    label: "PIM BQ 6 Failed"
    description: "# Products with invalid received date(15-YAN-2021,44219 etc)"
    type: count
    filters: [pim_bq_6: "0"]
    drill_fields: ["dq_validity_dtls*"]

  }

  measure: pim_bq_6_failed_perc {
    label: "PIM BQ 6 Failed %"
    description: "% Products with invalid received date(15-YAN-2021,44219 etc)"
    type: number
    sql:  safe_divide(${pim_bq_6_failed_rows},${tot_num_rows});;
    value_format_name: percent_2

  }

  measure: pim_bq_7_failed_rows {
    label: "PIM BQ 7 Failed"
    description: "# Products with invalid photo shoot date(15-YAN-2021,44219 etc)"
    type: count
    filters: [pim_bq_7: "0"]
    drill_fields: ["dq_validity_dtls*"]

  }

  measure: pim_bq_7_failed_perc {
    label: "PIM BQ 7 Failed %"
    description: "% Products with invalid photo shoot date(15-YAN-2021,44219 etc)"
    type: number
    sql:  safe_divide(${pim_bq_7_failed_rows},${tot_num_rows});;
    value_format_name: percent_2

  }


  measure: pim_bq_8_failed_rows {
    label: "PIM BQ 8 Failed"
    description: "# Products with invalid photo edit date(15-YAN-2021,44219 etc)"
    type: count
    filters: [pim_bq_8: "0"]
    drill_fields: ["dq_validity_dtls*"]

  }

  measure: pim_bq_8_failed_perc {
    label: "PIM BQ 8 Failed %"
    description: "% Products with invalid photo edit date(15-YAN-2021,44219 etc)"
    type: number
    sql:  safe_divide(${pim_bq_8_failed_rows},${tot_num_rows});;
    value_format_name: percent_2

  }

  measure: pim_bq_9_failed_rows {
    label: "PIM BQ 9 Failed"
    description: "# Products with stock but not created or ecom approved in PIM"
    type: count
    filters: [pim_bq_9: "0"]
    drill_fields: ["dq_consistency_dtls*"]

  }

  measure: pim_bq_9_failed_perc {
    label: "PIM BQ 9 Failed %"
    description: "% Products with stock but not created or ecom approved in PIM"
    type: number
    sql:  safe_divide(${pim_bq_9_failed_rows},${tot_num_rows});;
    value_format_name: percent_2

  }





}
