view: rule_score_agg {
  sql_table_name: `chb-exp-004.DQ_final_rule_score.rule_score_agg`
    ;;

  dimension: brand {
    type: string
    sql: ${TABLE}.brand ;;
  }
  dimension: vertical_for_access_filter {
    hidden: yes
    sql:  case when ${brand_vertical_mapping_log.vertical} is null then 'x' else ${brand_vertical_mapping_log.vertical} end ;;

  }
  dimension: bu_brand_for_access_filter {
    hidden: yes
    sql:  case when ${brand_vertical_mapping_log.bu_brand} is null then 'x' else ${brand_vertical_mapping_log.bu_brand} end ;;
  }

  dimension: brand_map {
    type: string
    hidden: yes
    sql: case WHEN ${brand}='Faces' then 'FACES'
              WHEN ${brand}='Toryburch' then 'TORY BURCH'
              WHEN ${brand}='The Deal' then 'THE DEAL'
              WHEN ${brand}='Tanagra' then 'TANAGRA & ADV'
              WHEN ${brand}='Swarovski' then 'SWAROVSKI'
              WHEN ${brand}='Level Shoes' then 'LEVEL SHOES'
              WHEN ${brand}='Lacoste' then 'LACOSTE'
              WHEN ${brand}='Tryano' then 'TRYANO' end ;;
    label: "Brand Mapping"
  }

  dimension_group: dq {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.dq_date ;;
  }

  dimension: dq_dimension {
    type: string
    sql: ${TABLE}.DQ_dimension ;;
  }

  dimension: num_rows_passed {
    type: number
    sql: ${TABLE}.num_rows_passed ;;
  }
measure: dq_pass_percent  {
  label: "DQ Pass %"
  type: number
  sql: (sum(${num_rows_passed})/sum(${num_rows_processed})) ;;
  value_format: "0.00%"
}
  measure: dq_fail_percent  {
    label: "DQ Fail %"
    type: number
    sql: (sum(${num_rows_processed}-${num_rows_passed})/sum(${num_rows_processed})) ;;
    value_format: "0.00%"
  }
  dimension: num_rows_processed {
    type: number
    sql: ${TABLE}.num_rows_processed ;;
  }

  dimension: rule_name {
    type: string
    sql: ${TABLE}.rule_name ;;
  }

  measure: count {
    type: count
    drill_fields: [rule_name]
  }
}
