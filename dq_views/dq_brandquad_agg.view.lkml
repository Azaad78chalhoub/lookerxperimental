view: dq_brandquad_agg {
  sql_table_name: `chb-exp-004.DQ_BrandQuad.dq_brandquad_agg`
    ;;

  dimension_group: dq {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.dq_date ;;
  }

  dimension: dq_dimension {
    type: string
    sql: ${TABLE}.dq_dimension ;;
  }

  dimension: fail {
    type: number
    sql: ${TABLE}.fail ;;
  }

  dimension: pass {
    type: number
    sql: ${TABLE}.pass ;;
  }

  dimension: rule_name {
    type: string
    sql: ${TABLE}.rule_name ;;
  }

  measure: count {
    type: count
    drill_fields: [rule_name]
  }

  measure: total_rows_processed {
    type:  sum
    sql: ${pass} + ${fail}
    ;;
  }

  measure: total_rows_passed {
    type:  sum
    sql: ${pass}

      ;;
  }

  measure: total_rows_failed {
    type: sum
    sql: ${fail} ;;
  }

  measure: fail_per {
    type: number
    sql: safe_divide(${total_rows_failed},${total_rows_processed}) ;;
    value_format_name: percent_2

  }
}
