view: oms_dq_last7_latest {
  sql_table_name: `chb-exp-004.DQ_Rule_Agg1.OMS_DQ_LAST7_LATEST`
    ;;
  dimension: awb {
    type: string
    sql: ${TABLE}.awb ;;
    label: "AWB"
    description: "AWB number in WH. Same as tracking no in Carriyo"
  }

  dimension: brand {
    type: string
    sql: ${TABLE}.brand ;;
    label: "Brand"
  }
  dimension: vertical_for_access_filter {
    hidden: yes
    sql:  case when ${brand_vertical_mapping_log.vertical} is null then 'x' else ${brand_vertical_mapping_log.vertical} end ;;

  }
  dimension: bu_brand_for_access_filter {
    hidden: yes
    sql:  case when ${brand_vertical_mapping_log.bu_brand} is null then 'x' else ${brand_vertical_mapping_log.bu_brand} end ;;
  }

  dimension: brand_map {
    type: string
    hidden: yes
    sql: case WHEN ${brand}='Faces' then 'FACES'
              WHEN ${brand}='Toryburch' then 'TORY BURCH'
              WHEN ${brand}='The Deal' then 'THE DEAL'
              WHEN ${brand}='Tanagra' then 'TANAGRA & ADV'
              WHEN ${brand}='Swarovski' then 'SWAROVSKI'
              WHEN ${brand}='Level Shoes' then 'LEVEL SHOES'
              WHEN ${brand}='Lacoste' then 'LACOSTE'
              WHEN ${brand}='Tryano' then 'TRYANO' end ;;
    label: "Brand Mapping"
  }
  dimension: carrier_status {
    type: string
    description: "Latest Carriyo carrier status on the order"
    sql: ${TABLE}.carrier_status ;;
  }
  dimension: carrier {
    type: string
    description: "Carrier mentioned on Carriyo"
    sql: ${TABLE}.carrier ;;
  }
  dimension: CHB_STATUS {
    type: string
    description: "Latest Carriyo CHB status on the order"
    sql: ${TABLE}.CHB_STATUS ;;
  }
  dimension: carriyo_status {
    type: string
    description: "Latest Carriyo status on the order"
    sql: ${TABLE}.carriyo_status ;;
  }
  dimension: last_status_oms {
    type: string
    description: "Latest OMS status on the order"
    sql: ${TABLE}.last_status_oms ;;
  }


  dimension: carrier_status_description {
    type: string
    hidden: yes
    sql: ${TABLE}.carrier_status_description ;;
  }

  dimension_group: dq_date_time_GST {
    type: time
    label: "Data Quality Assessment"
    description: "Timestamp for Data Quality Assessment conducted"
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: cast(${TABLE}.dq_date_time_GST as timestamp) ;;
  }

  dimension_group: last_update_time_oms {
    type: time
    label: "Last update in OMS"
    description: "Timestamp for the last status update made on OMS for a given order+tracking_no+SKU"
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: cast(${TABLE}.last_update_time_oms as timestamp) ;;
  }
  dimension_group: delivered_at_carriyo {
    type: time
    label: "Delivered at"
    description: "Timestamp for order delivery by the DSP to the customer. Obtained from Carriyo"
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: cast(${TABLE}.delivered_at_carriyo as timestamp) ;;
  }

  dimension_group: collected_at_carriyo {
    type: time
    label: "Collected at"
    description: "Timestamp for order collection by the DSP from WH/Store. Obtained from Carriyo"
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: cast(${TABLE}.collected_at_carriyo as timestamp) ;;
  }

  dimension: collected_at_status {
    type: string
    label: "Order collected status"
    description: "Indicates if a click&collect order has been collected by customer or not"
    sql: ${TABLE}.collected_at_status ;;
  }

  dimension_group: collected_by_customer {
    type: time
    description: "Collected by customer timestamp for click&collect orders"
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: cast(${TABLE}.collected_by_customer_at as timestamp) ;;
  }

  dimension: completeness_score {
    type: number
    label: "Completeness pass/fail"
    description:"Calculated using the least score from the individual OMS rules for Completeness (OMS1,OMS2,OMS5,OMS7,OMS8, OMS9,OMS11,OMS12, OMS13). 1 indicates pass and 0 indicates fail."
    sql: ${TABLE}.Completeness_score;;
  }

  dimension: consistency_score {
    type: number
    label: "Consistency pass/fail"
    description:"Calculated using the least score from the individual OMS rules for consistency (OMS3,OMS4). 1 indicates pass and 0 indicates fail."
    sql:  ${TABLE}.Consistency_score ;;
  }

  dimension: count_t_nos {
    type: number
    hidden: yes
    sql: ${TABLE}.count_t_nos ;;
  }

  dimension: customer_email {
    type: string
    label: "Customer Email"
    sql: ${TABLE}.customer_email ;;
  }

  dimension: customer_phonenumber {
    type: string
    label: "Customer Phone number"
    sql: ${TABLE}.customer_phonenumber ;;
  }

  dimension: delivery_city {
    type: string
    label: "Delivery City"
    sql: ${TABLE}.delivery_city ;;
  }

  dimension: delivery_type {
    type: string
    label: "Delivery Type"
    sql: ${TABLE}.delivery_type ;;

  }

  dimension: detail_sku_code {
    type: string
    label: "Product Number"
    sql: ${TABLE}.detail_sku_code ;;
  }

  dimension: detail_sku_code_oms2 {
    type: string
    hidden: yes
    sql: ${TABLE}.detail_sku_code_oms2 ;;
  }

  dimension: detail_status {
    type: string
    label: "Order Status"
    sql: ${TABLE}.detail_status ;;
  }

  dimension_group: dq {
    type: time
    label: "Data Quality Assessment"
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.dq_date ;;
  }

  dimension: dq_row_score {
    type: number
    label: "DQ Row Score (Pass/Fail)"
    description: "Calculated using the least score from the individual OMS rules. 1 indicates pass and 0 indicates fail."
    sql: least(${completeness_score},${consistency_score},${validity_score});;
  }

  dimension: final_shipping_loc {
    type: string
    label: "Shipping location in OMS"
    sql: ${TABLE}.final_shipping_loc ;;
  }

  dimension: final_tracking_id {
    type: string
    label: "OMS Tracking no"
    description: "Same as tracking no in Carriyo and AWB in WH"
    sql: ${TABLE}.final_trackingID ;;
  }

  dimension: order_currency {
    type: string
    label: "OMS Order currency"
    sql: ${TABLE}.order_currency ;;
  }

  measure: order_total_usd {
    label: "Order Total USD"
    value_format: "$#.00"
    sql: ${order_total}*${conversion_rate};;
  }
 dimension: order_total   {
    type: number
    label: "Total Order Amount"
    sql: ${TABLE}.order_total  ;;
  }
  dimension: conversion_rate   {
    type: number
    label: "Conversion Rate"
    sql: ${TABLE}.conversion_rate  ;;
  }
  dimension: detail_price_original   {
    type: number
    label: "Original Selling Price"
    sql: ${TABLE}.detail_price_original  ;;
  }
  dimension: detail_price_original_usd   {
    value_format: "$#.00"
    label: "Original Selling Price USD"
    sql: ${detail_price_original}* ${conversion_rate} ;;
  }
  dimension: detail_price {
    type: number
    label: "Final Selling Price"
    sql: ${TABLE}.detail_price ;;
  }
  dimension: detail_price_incl_tax {
    type: number
   hidden: yes
    sql: ${TABLE}.detail_price_incl_tax ;;
  }
  dimension: detail_discount {
    type: number
    label: "Discount Amount"
    sql: ${TABLE}.detail_discount ;;
  }
  dimension: detail_tax_amount   {
    type: number
    hidden: yes
    sql: ${TABLE}.detail_tax_amount ;;
  }
  dimension: detail_tax {
    type: number
    hidden: yes
    sql: ${TABLE}.detail_tax ;;
  }
  dimension: oms1 {
    type: number
    group_label: "OMS DQ Rules"
    description: "Order delivered to the customer should have a shipped status in OMS. 1 indicates pass and 0 indicates fail"
    sql: ${TABLE}.OMS1 ;;
  }

  dimension: oms10 {
    type: number
    group_label: "OMS DQ Rules"
    description: "Customer E-Mail should be valid in order to give visibility into the consumer.1 indicates pass and 0 indicates fail"
    sql: ${TABLE}.OMS10 ;;

  }

  dimension: oms11 {
    type: number
    group_label: "OMS DQ Rules"
    description: "Customer Phone number should not be null in order to give visibility into the consumer.1 indicates pass and 0 indicates fail"
    sql: ${TABLE}.OMS11 ;;
  }

  dimension: oms12 {
    type: number
    group_label: "OMS DQ Rules"
    description: "Every order shipped from store should have a claimed status before it is packed.1 indicates pass and 0 indicates fail"
    sql: ${TABLE}.OMS12 ;;
  }

  dimension: oms13 {
    type: number
    group_label: "OMS DQ Rules"
    description: "Every order shipped in OMS should have a collected date in Carriyo unified. 1 indicates pass and 0 indicates fail"
    sql: ${TABLE}.OMS13 ;;
  }

  dimension: oms14 {
    type: number
    group_label: "OMS DQ Rules"
    description: "Every Click&collect order should have a collected by customer after a ready for collection status. 1 indicates pass and 0 indicates fail"
    sql: ${TABLE}.OMS14 ;;
  }
  dimension: oms15 {
    type: number
    group_label: "OMS DQ Rules"
    description: "Delivery type of an order should not be null. 1 indicates pass and 0 indicates fail"
    sql: ${TABLE}.OMS15 ;;
  }

  dimension: oms16 {
    type: number
    group_label: "OMS DQ Rules"
    description: "Last statuses for orders should match in Carriyo and OMS. 1 indicates pass and 0 indicates fail"
    sql: ${TABLE}.OMS16 ;;
  }

  dimension: oms2 {
    type: number
    group_label: "OMS DQ Rules"
    description:"Every shipped item from WH or store has to have an a AWB. It is used for tracking the order in Carriyo. 1 indicates pass and 0 indicates fail"
    sql: ${TABLE}.OMS2 ;;
  }

  dimension: oms3 {
    type: number
    group_label: "OMS DQ Rules"
    description:"Every shipped item from WH with an AWB in OMS should have awb matching in WMS. It is used for tracking the order in Carriyo. 1 indicates pass and 0 indicates fail"
    sql: ${TABLE}.OMS3 ;;
  }

  dimension: oms4 {
    type: number
    group_label: "OMS DQ Rules"
    description: "Every item shipped from the WH, should ideally have an a AWB. 1 indicates pass and 0 indicates fail"
    sql: ${TABLE}.OMS4 ;;
  }

  dimension: oms5 {
    type: number
    group_label: "OMS DQ Rules"
    description: "Delivery city should not be null except for cancelled orders in order to know where do our maximum ecom orders come from.1 indicates pass and 0 indicates fail"
    sql: ${TABLE}.OMS5 ;;
  }

  dimension: oms7 {
    type: number
    group_label: "OMS DQ Rules"
    description: "Payment method should not be null except for cancelled orders.1 indicates pass and 0 indicates fail"
    sql: ${TABLE}.OMS7 ;;
  }

  dimension: oms8 {
    type: number
    group_label: "OMS DQ Rules"
    description: "Sales origin ID should not be NULL. 1 indicates pass and 0 indicates fail"
    sql: ${TABLE}.OMS8 ;;
  }

  dimension: oms9 {
    type: number
    group_label: "OMS DQ Rules"
    description: "Sales origin name should not be NULL. 1 indicates pass and 0 indicates fail"
    sql: ${TABLE}.OMS9 ;;
  }

  dimension_group: order {
    type: time
    label: "Customer Order"
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: cast (${TABLE}.order_date as timestamp);;
  }

  dimension: order_extern_orderkey {
    type: string
    sql: ${TABLE}.order_extern_orderkey ;;
    label: "External Order key"
    description: "External Order key in WH. Same as partner order reference in Carriyo"
  }

  dimension: order_id {
    type: string
    sql: ${TABLE}.order_id ;;
    label: "Order ID in OMS"
  }

  dimension: order_id_oms2 {
    type: string
    sql: ${TABLE}.order_id_oms2 ;;
    hidden: yes
  }

  dimension: order_id_wms {
    type: string
    sql: ${TABLE}.order_id_wms ;;
    label: "Order ID in WH"
  }

  dimension: partner_order_reference {
    type: string
    label: "Partner Order Reference"
    description: "Partner Order Reference number in Carriyo. Same as External Order key in WH"
    sql: ${TABLE}.partnerOrderReference ;;
  }

  dimension: payment_method {
    type: string
    sql: ${TABLE}.payment_method ;;
  }

  dimension_group: ready_for_collection {
    type: time
    label: "Ready for Collection from Store"
    description: "Ready for Collection timestamp from Store for Click&Collect orders"
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: cast(${TABLE}.ready_for_collection_at as timestamp) ;;
  }

  dimension: sales_origin_id {
    type: string
    label: "Sales Origin ID"
    sql: ${TABLE}.sales_origin_id ;;
  }

  dimension: sales_origin_name {
    type: string
    label: "Sales Origin Name"
    sql: ${TABLE}.sales_origin_name ;;
  }

  dimension_group: shipped {
    type: time
    label: "Shipped Date from WH"
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: cast(${TABLE}.shipped_date as timestamp);;
  }

  dimension: shipping_loc {
    type: string
    sql: ${TABLE}.shipping_loc ;;
  }

  dimension: t_id {
    type: string
    hidden: yes
    sql: ${TABLE}.t_id ;;
  }

  dimension: tracking_id {
    hidden: yes
    type: string
    sql: ${TABLE}.tracking_id ;;
  }

  dimension: tracking_no_carriyo {
    type: string
    label: "Tracking No"
    description: "Tracking number in Carriyo. Same as the AWB in WH"
    sql: ${TABLE}.tracking_no_carriyo ;;
  }

  dimension_group: update {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: cast( ${TABLE}.update_date as timestamp);;
  }
  measure: dq_score {
    label: "DQ PASS%"
    value_format: "0.00%"
    sql: (sum(${dq_row_score})/${count});;
  }
  measure: dq_score_pass {
    label: "DQ PASS Count"
    type: sum
    sql: case when ${dq_row_score}=1 then 1 end;;
  }
  measure: dq_fail_pass {
    label: "Total Rows with DQ Fail"
    type: sum
    sql: case when ${dq_row_score}=0 then 1 end;;
    drill_fields: [dq_fail_details*]
    filters: {
      field: dq_row_score
      value: "0"
    }
  }
  measure: amount_fail {
    label: "Total Selling Amount with DQ Fail"
    type: sum
    value_format: "$#,##0.00"
    sql: case when ${dq_row_score}=0 then ${detail_price_original_usd}  end;;
    drill_fields: [dq_fail_amount_details*]
    filters: {
      field: dq_row_score
      value: "0"
    }
  }
  measure: completeness_fail_count {
    label: "Total Rows with Completeness Fail"
    type: sum
    sql: case when ${completeness_score}=0 then 1 end;;
    drill_fields: [completeness_fail_details*]
    filters: {
      field: completeness_score
      value: "0"
    }
  }
  measure: completeness_fail_amount {
    label: "Total Selling Amount with Completeness Fail"
    type: sum
    value_format: "$#,##0.00"
    sql: case when ${completeness_score}=0 then ${detail_price_original_usd} end;;
    drill_fields: [completeness_fail_details*]
    filters: {
      field: completeness_score
      value: "0"
    }
  }
  measure: validity_fail_count {
    label: "Total Rows with Validity Fail"
    type: sum
    sql: case when ${validity_score}=0 then 1 end;;
    drill_fields: [validity_fail_details*]
    filters: {
      field: validity_score
      value: "0"
    }
  }
  measure: validity_fail_amount {
    label: "Total Selling Amount with Validity Fail"
    type: sum
    value_format: "$#,##0.00"
    sql: case when ${validity_score}=0 then ${detail_price_original_usd} end;;
    drill_fields: [validity_fail_details*]
    filters: {
      field: validity_score
      value: "0"
    }
  }
  measure: consistency_fail_count{
    label: "Total Rows with Consistency Fail"
    type: sum
    sql: case when ${consistency_score}=0 then 1 end;;
    drill_fields: [consistency_fail_details*]
    filters: {
      field: consistency_score
      value: "0"
    }
  }
  measure: consistency_fail_amount{
    label: "Total Selling Amount with Consistency Fail"
    type: sum
    value_format: "$#,##0.00"
    sql: case when ${consistency_score}=0 then ${detail_price_original_usd} end;;
    drill_fields: [consistency_fail_details*]
    filters: {
      field: consistency_score
      value: "0"
    }
  }
  set: dq_row_details {
    fields: [brand,awb,final_shipping_loc, final_tracking_id,customer_email,customer_phonenumber,order_id,tracking_no_carriyo,detail_sku_code,  order_raw ,sales_origin_id, sales_origin_name,  payment_method, detail_status,  delivery_city,  delivery_type,last_status_oms,delivered_at_carriyo_raw,collected_at_carriyo_date, carrier, carrier_status, ready_for_collection_raw, collected_at_status,  collected_by_customer_raw,order_currency, detail_price_original_usd,detail_price_original, dq_date,  oms1, oms2, oms3,oms4, oms5, oms7, oms8, oms9, oms10,  oms11, oms12,oms13,oms14,oms15,oms16,  completeness_score, validity_score, consistency_score, dq_row_score]
  }
  set: dq_fail_details {
    fields: [brand,awb,final_shipping_loc, final_tracking_id,customer_email,customer_phonenumber,order_id,tracking_no_carriyo,detail_sku_code,  order_raw ,sales_origin_id, sales_origin_name,  payment_method, detail_status,  delivery_city,  delivery_type,last_status_oms,delivered_at_carriyo_raw,collected_at_carriyo_date, carrier, carrier_status, ready_for_collection_raw, collected_at_status,  collected_by_customer_raw,order_currency,detail_price_original_usd,detail_price_original,  dq_date,  oms1, oms2, oms3,oms4, oms5, oms7, oms8, oms9, oms10,  oms11, oms12,oms13,oms14,oms15, oms16, completeness_score, validity_score, consistency_score, dq_row_score]
  }
  set:dq_fail_amount_details {
    fields: [brand,awb,final_shipping_loc, final_tracking_id,customer_email,customer_phonenumber,order_id,tracking_no_carriyo,detail_sku_code,  order_raw ,sales_origin_id, sales_origin_name,  payment_method, detail_status,  delivery_city,  delivery_type,last_status_oms,delivered_at_carriyo_raw,collected_at_carriyo_date, carrier, carrier_status, ready_for_collection_raw, collected_at_status,  collected_by_customer_raw,order_currency,detail_price_original_usd,detail_price_original,  dq_date,  oms1, oms2, oms3,oms4, oms5, oms7, oms8, oms9, oms10,  oms11, oms12,oms13,oms14,oms15, oms16, completeness_score, validity_score, consistency_score, dq_row_score]
  }
  set:consistency_fail_details {
    fields: [order_id,final_shipping_loc,brand,awb, final_tracking_id,customer_email,customer_phonenumber,tracking_no_carriyo,detail_sku_code,  order_raw ,sales_origin_id, sales_origin_name,  payment_method, detail_status,  delivery_city,  delivery_type,last_status_oms,delivered_at_carriyo_raw,  dq_date,collected_at_carriyo_date, carrier, carrier_status, ready_for_collection_raw, collected_at_status,  collected_by_customer_raw, order_currency,detail_price_original_usd,detail_price_original, oms3,oms4,  oms16,completeness_score, validity_score, consistency_score, dq_row_score]
  }
  set:validity_fail_details {
    fields: [brand,awb, final_shipping_loc,final_tracking_id,customer_email,customer_phonenumber,order_id,tracking_no_carriyo,detail_sku_code,  order_raw ,sales_origin_id, sales_origin_name,  payment_method, detail_status,  delivery_city,  delivery_type,last_status_oms,delivered_at_carriyo_raw,  dq_date,collected_at_carriyo_date, carrier, carrier_status, ready_for_collection_raw, collected_at_status,  collected_by_customer_raw,order_currency,detail_price_original_usd,detail_price_original,  oms10,oms14,  completeness_score, validity_score, consistency_score, dq_row_score]
  }
  set:completeness_fail_details {
    fields: [brand,awb,final_shipping_loc, final_tracking_id,customer_email,customer_phonenumber,order_id,tracking_no_carriyo,detail_sku_code,  order_raw ,sales_origin_id, sales_origin_name,  payment_method, detail_status,  delivery_city,  delivery_type,last_status_oms, delivered_at_carriyo_raw, dq_date,collected_at_carriyo_date, carrier, carrier_status, ready_for_collection_raw, collected_at_status,  collected_by_customer_raw,order_currency,detail_price_original_usd,detail_price_original,  oms1, oms2, oms5, oms7, oms8, oms9, oms11, oms12,oms13, completeness_score, validity_score, consistency_score, dq_row_score]
  }
  dimension: validity_score {
    type: number
    description:"Calculated using the least score from the individual OMS rules for Validity (OMS10, OMS14). 1 indicates pass and 0 indicates fail."
    sql: ${TABLE}.validity_score ;;
  }
  measure: completeness_score_dq {
    label: "Completeness Pass %"
    value_format: "0.00%"
    sql: (sum(${completeness_score})/${count});;
  }
  measure: validity_score_overall {
    label: "Validity Pass %"
    value_format: "0.00%"
    sql: (sum(${validity_score})/${count});;
  }
  measure: consistency_score_dq {
    label: "Consistency Pass %"
    value_format: "0.00%"
    sql: (sum(${consistency_score})/${count});;
  }
  measure: oms1_dq_percent {
    label: "OMS1 Pass %"
    value_format: "0.00%"
    sql: (sum(${oms1})/${count});;
  }
  measure: oms2_dq_percent {
    label: "OMS2 Pass %"
    value_format: "0.00%"
    sql: (sum(${oms2})/${count});;
  }
  measure: oms3_dq_percent {
    label: "OMS3 Pass %"
    value_format: "0.00%"
    sql: (sum(${oms3})/${count});;
  }
  measure: oms4_dq_percent {
    label: "OMS4 Pass %"
    value_format: "0.00%"
    sql: (sum(${oms4})/${count});;
  }
  measure: oms5_dq_percent {
    label: "OMS5 Pass %"
    value_format: "0.00%"
    sql: (sum(${oms5})/${count});;
  }
  measure: oms7_dq_percent {
    label: "OMS7 Pass %"
    value_format: "0.00%"
    sql: (sum(${oms7})/${count});;
  }
  measure: oms8_dq_percent {
    label: "OMS8 Pass %"
    value_format: "0.00%"
    sql: (sum(${oms8})/${count});;
  }
  measure: oms9_dq_percent {
    label: "OMS9 Pass %"
    value_format: "0.00%"
    sql: (sum(${oms9})/${count});;
  }
  measure: oms10_dq_percent {
    label: "OMS10 Pass %"
    value_format: "0.00%"
    sql: (sum(${oms10})/${count});;
  }
  measure: oms11_dq_percent {
    label: "OMS11 Pass %"
    value_format: "0.00%"
    sql: (sum(${oms11})/${count});;
  }
  measure: oms12_dq_percent {
    label: "OMS12 Pass %"
    value_format: "0.00%"
    sql: (sum(${oms12})/${count});;
  }
  measure: oms13_dq_percent {
    label: "OMS13 Pass %"
    value_format: "0.00%"
    sql: (sum(${oms13})/${count});;
  }
  measure: oms14_dq_percent {
    label: "OMS14 Pass %"
    value_format: "0.00%"
    sql: (sum(${oms14})/${count});;
  }
  measure: oms15_dq_percent {
    label: "OMS15 Pass %"
    value_format: "0.00%"
    sql: (sum(${oms15})/${count});;
  }
  measure: oms16_dq_percent {
    label: "OMS16 Pass %"
    value_format: "0.00%"
    sql: (sum(${oms16})/${count});;
  }
  measure: count {
    label: "Total Rows processed"
    type: count
    drill_fields: [dq_row_details*]
  }
  measure: OMS1_passed {
    label: "Count of OMS1 passed records"
    type: sum
    sql: ${oms1} ;;
  }
  measure: OMS1_failed {
    label: "OMS1 failed record count"
    description: "Count of records where orders were delivered to the customer without a shipped status in OMS"
    type: sum
    sql: case when ${oms1}=0 then 1 end;;
    drill_fields: [oms1_fail*]
    filters: {
      field: oms1
      value: "0"
    }
  }
  measure: oms1_amount_fail {
    label: "Total Selling Amount with OMS1 Fail"
    description: "Selling amount for orders delivered to the customer without a shipped status in OMS"
    type: sum
    value_format: "$#,##0.00"
    sql: case when ${oms1}=0 then ${detail_price_original_usd}  end;;
    drill_fields: [dq_fail_amount_details*]
    filters: {
      field: oms1
      value: "0"
    }
  }
  measure: oms2_amount_fail {
    label: "Total Selling Amount with OMS2 Fail"
    description:"Selling amount for orders where every shipped item from WH or store did not have an a AWB. It is used for tracking the order in Carriyo"
    type: sum
    value_format: "$#,##0.00"
    sql: case when ${oms2}=0 then ${detail_price_original_usd}  end;;
    drill_fields: [dq_fail_amount_details*]
    filters: {
      field: oms2
      value: "0"
    }
  }
  measure: oms3_amount_fail {
    label: "Total Selling Amount with OMS3 Fail"
    description:"Selling amount for orders shipped from WH with an AWB in OMS without a matching AWB in WMS"
    type: sum
    value_format: "$#,##0.00"
    sql: case when ${oms3}=0 then ${detail_price_original_usd}  end;;
    drill_fields: [dq_fail_amount_details*]
    filters: {
      field: oms3
      value: "0"
    }
  }
  measure: oms4_amount_fail {
    label: "Total Selling Amount with OMS4 Fail"
    description:"Selling amount for orders shipped from the WH, without a matching AWB in Carriyo"
    type: sum
    value_format: "$#,##0.00"
    sql: case when ${oms4}=0 then ${detail_price_original_usd}  end;;
    drill_fields: [dq_fail_amount_details*]
    filters: {
      field: oms4
      value: "0"
    }
  }
  measure: oms5_amount_fail {
    label: "Total Selling Amount with OMS5 Fail"
    description:"Selling amount for orders where Delivery city was null"
    type: sum
    value_format: "$#,##0.00"
    sql: case when ${oms5}=0 then ${detail_price_original_usd}  end;;
    drill_fields: [dq_fail_amount_details*]
    filters: {
      field: oms5
      value: "0"
    }
  }
  measure: oms7_amount_fail {
    label: "Total Selling Amount with OMS7 Fail"
    description:"Selling amount for orders where payment method was null"
    type: sum
    value_format: "$#,##0.00"
    sql: case when ${oms7}=0 then ${detail_price_original_usd}  end;;
    drill_fields: [dq_fail_amount_details*]
    filters: {
      field: oms7
      value: "0"
    }
  }
  measure: oms8_amount_fail {
    label: "Total Selling Amount with OMS8 Fail"
    description:"Selling amount for orders where Sales origin ID was NULL"
    type: sum
    value_format: "$#,##0.00"
    sql: case when ${oms8}=0 then ${detail_price_original_usd}  end;;
    drill_fields: [dq_fail_amount_details*]
    filters: {
      field: oms8
      value: "0"
    }
  }
  measure: oms9_amount_fail {
    label: "Total Selling Amount with OMS9 Fail"
    description:"Selling amount for orders where Sales origin name was NULL"
    type: sum
    value_format: "$#,##0.00"
    sql: case when ${oms9}=0 then ${detail_price_original_usd}  end;;
    drill_fields: [dq_fail_amount_details*]
    filters: {
      field: oms9
      value: "0"
    }
  }
  measure: oms10_amount_fail {
    label: "Total Selling Amount with OMS10 Fail"
    description:"Selling amount for orders where Customer EMail was null"
    type: sum
    value_format: "$#,##0.00"
    sql: case when ${oms10}=0 then ${detail_price_original_usd}  end;;
    drill_fields: [dq_fail_amount_details*]
    filters: {
      field: oms10
      value: "0"
    }
  }
  measure: oms11_amount_fail {
    label: "Total Selling Amount with OMS11 Fail"
    description:"Selling amount for orders where Customer Phone number was null"
    type: sum
    value_format: "$#,##0.00"
    sql: case when ${oms11}=0 then ${detail_price_original_usd}  end;;
    drill_fields: [dq_fail_amount_details*]
    filters: {
      field: oms11
      value: "0"
    }
  }
  measure: oms12_amount_fail {
    label: "Total Selling Amount with OMS12 Fail"
    description:"Selling amount for shipped orders from store that did not have a claimed status before it was is packed"
    type: sum
    value_format: "$#,##0.00"
    sql: case when ${oms12}=0 then ${detail_price_original_usd}  end;;
    drill_fields: [dq_fail_amount_details*]
    filters: {
      field: oms12
      value: "0"
    }
  }
  measure: oms13_amount_fail {
    label: "Total Selling Amount with OMS13 Fail"
    description:"Selling amount for shipped orders in OMS that did not have a collected date in Carriyo unified"
    type: sum
    value_format: "$#,##0.00"
    sql: case when ${oms13}=0 then ${detail_price_original_usd}  end;;
    drill_fields: [dq_fail_amount_details*]
    filters: {
      field: oms13
      value: "0"
    }
  }
  measure: oms14_amount_fail {
    label: "Total Selling Amount with OMS14 Fail"
    description:"Selling amount for orders where a Click&collect order had a collected by customer before a ready for collection status"
    type: sum
    value_format: "$#,##0.00"
    sql: case when ${oms14}=0 then ${detail_price_original_usd}  end;;
    drill_fields: [dq_fail_amount_details*]
    filters: {
      field: oms14
      value: "0"
    }
  }
  measure: oms15_amount_fail {
    label: "Total Selling Amount with OMS15 Fail"
    description:"Selling amount for orders where delivery type of an order was null"
    type: sum
    value_format: "$#,##0.00"
    sql: case when ${oms15}=0 then ${detail_price_original_usd}  end;;
    drill_fields: [dq_fail_amount_details*]
    filters: {
      field: oms15
      value: "0"
    }
  }
  measure: oms16_amount_fail {
    label: "Total Selling Amount with OMS16 Fail"
    description:"Selling amount for orders where the last status in Carriyo was Delivered but in OMS it was still shipped/undelivered"
    type: sum
    value_format: "$#,##0.00"
    sql: case when ${oms16}=0 then ${detail_price_original_usd}  end;;
    drill_fields: [dq_fail_amount_details*]
    filters: {
      field: oms16
      value: "0"
    }
  }
  measure: OMS2_failed {
    label: "OMS2 failed record count"
    description: "Count of data records where a shipped item from WH or store did not have an a AWB"
    type: sum
    sql: case when ${oms2}=0 then 1 end;;
    drill_fields: [oms2_fail*]
    filters: {
      field: oms2
      value: "0"
    }
  }
  measure: OMS3_failed {
    label: "OMS3 failed record count"
    description:"Count of data records for orders shipped from WH with an AWB in OMS without a matching AWB in WMS"
    type: sum
    sql: case when ${oms3}=0 then 1 end;;
    drill_fields: [oms3_fail*]
    filters: {
      field: oms3
      value: "0"
    }
  }
  measure: OMS4_failed {
    label: "OMS4failed record count"
    description:"Count of data records for orders shipped from the WH, without a matching AWB in Carriyo"
    type: sum
    sql: case when ${oms4}=0 then 1 end;;
    drill_fields: [oms4_fail*]
    filters: {
      field: oms4
      value: "0"
    }
  }
  measure: OMS5_failed {
    label: "OMS5 failed record count"
    description:"Count of data records for orders where Delivery city was null"
    type: sum
    sql: case when ${oms5}=0 then 1 end;;
    drill_fields: [oms5_fail*]
    filters: {
      field: oms5
      value: "0"
    }
  }
  measure: OMS7_failed {
    label: "OMS7 failed record count"
    description:"Count of data records for orders where payment method was null"
    type: sum
    sql: case when ${oms7}=0 then 1 end;;
    drill_fields: [oms7_fail*]
    filters: {
      field: oms7
      value: "0"
    }
  }
  measure: OMS8_failed {
    label: "OMS8 failed record count"
    description:"Count of data records for orders where Sales origin ID was NULL"
    type: sum
    sql: case when ${oms8}=0 then 1 end;;
    drill_fields: [oms8_fail*]
    filters: {
      field: oms8
      value: "0"
    }
  }
  measure: OMS9_failed {
    label: "OMS9 failed record count"
    description:"Count of data records for orders where Sales origin name was NULL"
    type: sum
    sql: case when ${oms9}=0 then 1 end;;
    drill_fields: [oms9_fail*]
    filters: {
      field: oms9
      value: "0"
    }
  }
  measure: OMS10_failed {
    label: "OMS10 failed record count"
    description:"Count of data records for orders where Customer EMail was null"
    type: sum
    sql: case when ${oms10}=0 then 1 end;;
    drill_fields: [oms10_fail*]
    filters: {
      field: oms10
      value: "0"
    }
  }
  measure: OMS11_failed {
    label: "OMS11 failed record count"
    description:"Count of data records for orders where Customer Phone number was null"
    type: sum
    sql: case when ${oms11}=0 then 1 end;;
    drill_fields: [oms11_fail*]
    filters: {
      field: oms11
      value: "0"
    }
  }
  measure: OMS12_failed {
    label: "OMS12 failed record count"
    description:"Count of data records for shipped orders from store that did not have a claimed status before it was is packed"
    type: sum
    sql: case when ${oms12}=0 then 1 end;;
    drill_fields: [oms12_fail*]
    filters: {
      field: oms12
      value: "0"
    }
  }
  measure: OMS13_failed {
    label: "OMS13 failed record count"
    description:"Count of data records for shipped orders in OMS that did not have a collected date in Carriyo unified"
    type: sum
    sql: case when ${oms13}=0 then 1 end;;
    drill_fields: [oms13_fail*]
    filters: {
      field: oms13
      value: "0"
    }
  }
  measure: OMS14_failed {
    label: "OMS14 failed record count"
    description:"Count of data records for orders where a Click&collect order had a collected by customer before a ready for collection status"
    type: sum
    sql: case when ${oms14}=0 then 1 end;;
    drill_fields: [oms14_fail*]
    filters: {
      field: oms14
      value: "0"
    }
  }
  measure: OMS15_failed {
    label: "OMS15 failed record count"
    type: sum
    description:"Count of data records for orders where delivery type of an order was null"
    sql: case when ${oms15}=0 then 1 end;;
    drill_fields: [oms15_fail*]
    filters: {
      field: oms15
      value: "0"
    }
  }
  measure: OMS16_failed {
    label: "OMS16 failed record count"
    type: sum
    description:"Count of data records for orders where the last status in Carriyo was Delivered but in OMS it was still shipped/undelivered"
    sql: case when ${oms16}=0 then 1 end;;
    drill_fields: [oms16_fail*]
    filters: {
      field: oms16
      value: "0"
    }
  }

  set: oms1_fail {
    fields: [brand,awb,final_shipping_loc, final_tracking_id,customer_email,customer_phonenumber,order_id,tracking_no_carriyo,detail_sku_code,  order_raw ,sales_origin_id, sales_origin_name,  payment_method, detail_status,  delivery_city,  delivery_type,collected_at_carriyo_date,carrier, carrier_status,  ready_for_collection_raw, collected_at_status,  collected_by_customer_raw,  dq_date,  oms1, completeness_score, validity_score, consistency_score, dq_row_score]
  }
  set: oms2_fail {
    fields: [brand,awb,final_shipping_loc, final_tracking_id,customer_email,customer_phonenumber,order_id,tracking_no_carriyo,detail_sku_code,  order_raw ,sales_origin_id, sales_origin_name,  payment_method, detail_status,  delivery_city,  delivery_type,collected_at_carriyo_date,carrier, carrier_status,  ready_for_collection_raw, collected_at_status,  collected_by_customer_raw,  dq_date,  oms2, completeness_score, validity_score, consistency_score, dq_row_score]
  }
  set: oms3_fail {
    fields: [brand,awb,final_shipping_loc, final_tracking_id,customer_email,customer_phonenumber,order_id,tracking_no_carriyo,detail_sku_code,  order_raw ,sales_origin_id, sales_origin_name,  payment_method, detail_status,  delivery_city,  delivery_type,collected_at_carriyo_date,carrier, carrier_status,  ready_for_collection_raw, collected_at_status,  collected_by_customer_raw,  dq_date,  oms3,completeness_score, validity_score, consistency_score, dq_row_score]
  }
  set: oms4_fail {
    fields: [brand,awb,final_shipping_loc, final_tracking_id,customer_email,customer_phonenumber,order_id,tracking_no_carriyo,detail_sku_code,  order_raw ,sales_origin_id, sales_origin_name,  payment_method, detail_status,  delivery_city,  delivery_type,collected_at_carriyo_date,carrier, carrier_status,  ready_for_collection_raw, collected_at_status,  collected_by_customer_raw,  dq_date,  oms4,completeness_score, validity_score, consistency_score, dq_row_score]
  }
  set: oms5_fail {
    fields: [brand,awb,final_shipping_loc, final_tracking_id,customer_email,customer_phonenumber,order_id,tracking_no_carriyo,detail_sku_code,  order_raw ,sales_origin_id, sales_origin_name,  payment_method, detail_status,  delivery_city,  delivery_type,collected_at_carriyo_date,carrier, carrier_status,  ready_for_collection_raw, collected_at_status,  collected_by_customer_raw,  dq_date,  oms5,completeness_score, validity_score, consistency_score, dq_row_score]
  }
  set: oms7_fail{
    fields: [brand,awb,final_shipping_loc, final_tracking_id,customer_email,customer_phonenumber,order_id,tracking_no_carriyo,detail_sku_code,  order_raw ,sales_origin_id, sales_origin_name,  payment_method, detail_status,  delivery_city,  delivery_type,collected_at_carriyo_date,carrier, carrier_status,  ready_for_collection_raw, collected_at_status,  collected_by_customer_raw,  dq_date,  oms7,completeness_score, validity_score, consistency_score, dq_row_score]
  }
  set: oms8_fail {
    fields: [brand,awb,final_shipping_loc, final_tracking_id,customer_email,customer_phonenumber,order_id,tracking_no_carriyo,detail_sku_code,  order_raw ,sales_origin_id, sales_origin_name,  payment_method, detail_status,  delivery_city,  delivery_type,collected_at_carriyo_date, carrier, carrier_status, ready_for_collection_raw, collected_at_status,  collected_by_customer_raw,  dq_date,  oms8,completeness_score, validity_score, consistency_score, dq_row_score]
  }
  set: oms9_fail {
    fields: [brand,awb,final_shipping_loc, final_tracking_id,customer_email,customer_phonenumber,order_id,tracking_no_carriyo,detail_sku_code,  order_raw ,sales_origin_id, sales_origin_name,  payment_method, detail_status,  delivery_city,  delivery_type,collected_at_carriyo_date, carrier, carrier_status, ready_for_collection_raw, collected_at_status,  collected_by_customer_raw,  dq_date,  oms9,completeness_score, validity_score, consistency_score, dq_row_score]
  }
  set: oms10_fail {
    fields: [brand,awb,final_shipping_loc, final_tracking_id,customer_email,customer_phonenumber,order_id,tracking_no_carriyo,detail_sku_code,  order_raw ,sales_origin_id, sales_origin_name,  payment_method, detail_status,  delivery_city,  delivery_type,collected_at_carriyo_date, carrier, carrier_status, ready_for_collection_raw, collected_at_status,  collected_by_customer_raw,  dq_date,  oms10,completeness_score, validity_score, consistency_score, dq_row_score]
  }
  set: oms11_fail {
    fields: [brand,awb,final_shipping_loc, final_tracking_id,customer_email,customer_phonenumber,order_id,tracking_no_carriyo,detail_sku_code,  order_raw ,sales_origin_id, sales_origin_name,  payment_method, detail_status,  delivery_city,  delivery_type,collected_at_carriyo_date, carrier, carrier_status, ready_for_collection_raw, collected_at_status,  collected_by_customer_raw,  dq_date,  oms11,completeness_score, validity_score, consistency_score, dq_row_score]
  }
  set: oms12_fail {
    fields: [brand,awb,final_shipping_loc, final_tracking_id,customer_email,customer_phonenumber,order_id,tracking_no_carriyo,detail_sku_code,  order_raw ,sales_origin_id, sales_origin_name,  payment_method, detail_status,  delivery_city,  delivery_type,collected_at_carriyo_date, carrier, carrier_status, ready_for_collection_raw, collected_at_status,  collected_by_customer_raw,  dq_date,  oms12,completeness_score, validity_score, consistency_score, dq_row_score]
  }
  set: oms13_fail {
    fields: [brand,awb,final_shipping_loc, final_tracking_id,customer_email,customer_phonenumber,order_id,tracking_no_carriyo,detail_sku_code,  order_raw ,sales_origin_id, sales_origin_name,  payment_method, detail_status,  delivery_city,  delivery_type,collected_at_carriyo_date, carrier, carrier_status, ready_for_collection_raw, collected_at_status,  collected_by_customer_raw,  dq_date,  oms13,completeness_score, validity_score, consistency_score, dq_row_score]
  }
  set: oms14_fail {
    fields: [brand,awb,final_shipping_loc, final_tracking_id,customer_email,customer_phonenumber,order_id,tracking_no_carriyo,detail_sku_code,  order_raw ,sales_origin_id, sales_origin_name,  payment_method, detail_status,  delivery_city,  delivery_type,collected_at_carriyo_date, carrier, carrier_status, ready_for_collection_raw, collected_at_status,  collected_by_customer_raw,  dq_date,  oms14,completeness_score, validity_score, consistency_score, dq_row_score]
  }
  set: oms15_fail {
    fields: [brand,awb,final_shipping_loc, final_tracking_id,customer_email,customer_phonenumber,order_id,tracking_no_carriyo,detail_sku_code,  order_raw ,sales_origin_id, sales_origin_name,  payment_method, detail_status,  delivery_city,  delivery_type,collected_at_carriyo_date, carrier, carrier_status, ready_for_collection_raw, collected_at_status,  collected_by_customer_raw,  dq_date, oms15,  completeness_score, validity_score, consistency_score, dq_row_score]
  }
  set: oms16_fail {
    fields: [brand,awb,final_shipping_loc, final_tracking_id,customer_email,customer_phonenumber,order_id,tracking_no_carriyo,detail_sku_code,  order_raw ,sales_origin_id, sales_origin_name,  payment_method, detail_status,  delivery_city,  delivery_type,collected_at_carriyo_date,carrier, carrier_status,  ready_for_collection_raw, collected_at_status,  collected_by_customer_raw, delivered_at_carriyo_raw,last_status_oms,  dq_date, oms16,  completeness_score, validity_score, consistency_score, dq_row_score]
  }
  measure: OMS2_passed {
    label: "Count of OMS2 passed records"
    type: sum
    sql: ${oms2} ;;
  }
  measure: OMS3_passed {
    label: "Count of OMS3 passed records"
    type: sum
    sql: ${oms3} ;;
  }
  measure: OMS4_passed {
    label: "Count of OMS4 passed records"
    type: sum
    sql: ${oms4} ;;
  }
  measure: OMS5_passed {
    label: "Count of OMS5 passed records"
    type: sum
    sql: ${oms5} ;;
  }
  measure: OMS7_passed {
    label: "Count of OMS7 passed records"
    type: sum
    sql: ${oms7} ;;
  }
  measure: OMS8_passed {
    label: "Count of OMS8 passed records"
    type: sum
    sql: ${oms8} ;;
  }
  measure: OMS9_passed {
    label: "Count of OMS9 passed records"
    type: sum
    sql: ${oms9} ;;
  }
  measure: OMS10_passed {
    label: "Count of OMS10 passed records"
    type: sum
    sql: ${oms10} ;;
  }
  measure: OMS11_passed {
    label: "Count of OMS11 passed records"
    type: sum
    sql: ${oms11} ;;
  }
  measure: OMS12_passed {
    label: "Count of OMS12 passed records"
    type: sum
    sql: ${oms12} ;;
  }
  measure: OMS13_passed {
    label: "Count of OMS13 passed records"
    type: sum
    sql: ${oms13} ;;
  }
  measure: OMS14_passed {
    label: "Count of OMS14 passed records"
    type: sum
    sql: ${oms14} ;;
  }
  measure: OMS15_passed {
    label: "Count of OMS15 passed records"
    type: sum
    sql: ${oms15} ;;
  }






}
