view: sf_group_survey_result {
  sql_table_name: CX_ControlTower.sf_group_survey_result ;;
  drill_fields: [id]

  ##################################
  #####         DATES          #####
  ##################################

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_date ;;
  }

#   ###############################################
#   #####         PERIOD OVER PERIOD          #####
#   ###############################################
#
#   filter: current_date_range {
#     type: date
#     view_label: "_POP"
#     label: "1. Date Range"
#     description: "Select the date range you are interested in using this filter, can be used by itself. Make sure any filter on Event Date covers this period, or is removed."
#     sql: ${period} IS NOT NULL ;;
#   }
#
#   parameter: compare_to {
#     view_label: "_POP"
#     description: "Choose the period you would like to compare to. Must be used with Current Date Range filter"
#     label: "2. Compare To:"
#     type: unquoted
#     allowed_value: {
#       label: "Previous Period"
#       value: "Period"
#     }
#     allowed_value: {
#       label: "Previous Week"
#       value: "Week"
#     }
#     allowed_value: {
#       label: "Previous Month"
#       value: "Month"
#     }
#     allowed_value: {
#       label: "Previous Quarter"
#       value: "Quarter"
#     }
#     allowed_value: {
#       label: "Previous Year"
#       value: "Year"
#     }
#     default_value: "Period"
#     view_label: "_POP"
#   }
#
# ## -------------------------------------------------- ##
#
# ## ------------------ HIDDEN HELPER DIMENSIONS  ------------------ ##
#
#   dimension: days_in_period {
# #     hidden:  yes
#     view_label: "_POP"
#     description: "Gives the number of days in the current period date range"
#     type: number
#     sql: abs(date_diff(DATE({% date_start current_date_range %}), DATE({% date_end current_date_range %}), day)) ;;
#   }
#
#   dimension: period_2_start {
# #     hidden:  yes
#     view_label: "_POP"
#     description: "Calculates the start of the previous period"
#     type: date
#     sql:
#       {% if compare_to._parameter_value == "Period" %}
#         date_add(DATE({% date_start current_date_range %}), INTERVAL  -${days_in_period} DAY)
#       {% else %}
#         date_add(DATE({% date_start current_date_range %}), INTERVAL  -1 {% parameter compare_to %})
#       {% endif %};;
#   }
#
#   dimension: period_2_end {
# #     hidden:  yes
#     view_label: "_POP"
#     description: "Calculates the end of the previous period"
#     type: date
#     sql:
#       {% if compare_to._parameter_value == "Period" %}
#         date_add(DATE({% date_start current_date_range %}), INTERVAL -1 DAY)
#       {% else %}
#         date_add(date_add(DATE({% date_end current_date_range %}), INTERVAL -1 DAY), INTERVAL -1 {% parameter compare_to %})
#       {% endif %};;
#   }
#
#   dimension: day_in_period {
# #     hidden: yes
#     view_label: "_POP"
#     description: "Gives the number of days since the start of each periods. Use this to align the event dates onto the same axis, the axes will read 1,2,3, etc."
#     type: number
#     sql:
#     {% if current_date_range._is_filtered %}
#       CASE
#         WHEN {% condition current_date_range %} ${created_raw} {% endcondition %}
#         THEN abs(date_diff(DATE({% date_start current_date_range %}), ${created_date}, DAY)) + 1
#         WHEN ${created_date} between ${period_2_start} and ${period_2_end}
#         THEN abs(date_diff(${period_2_start}, ${created_date}, DAY)) + 1
#       END
#     {% else %} NULL
#     {% endif %}
#     ;;
#   }
#
#   dimension: order_for_period {
# #     hidden: yes
#     view_label: "_POP"
#     type: number
#     sql:
#        {% if current_date_range._is_filtered %}
#          CASE
#            WHEN {% condition current_date_range %} ${created_raw} {% endcondition %}
#            THEN 1
#            WHEN ${created_date} between ${period_2_start} and ${period_2_end}
#            THEN 2
#          END
#        {% else %}
#          NULL
#        {% endif %}
#        ;;
#   }
#
# ## -------------------------------------------------- ##
#
# ## ------------------ DIMENSIONS TO PLOT ------------------ ##
#
#   dimension: period {
#     view_label: "_POP"
#     label: "Period"
#     description: "Pivot me! Returns the period the metric covers, i.e. either the 'This Period' or 'Previous Period'"
#     type: string
#     order_by_field: order_for_period
#     sql:
#        {% if current_date_range._is_filtered %}
#          CASE
#            WHEN {% condition current_date_range %} ${created_raw} {% endcondition %}
#            THEN 'This {% parameter compare_to %}'
#            WHEN ${created_date} between ${period_2_start} and ${period_2_end}
#            THEN 'Last {% parameter compare_to %}'
#          END
#        {% else %}
#          NULL
#        {% endif %}
#        ;;
#   }
#
#   dimension_group: date_in_period {
#     description: "Use this as your date dimension when comparing periods. Aligns the previous periods onto the current period"
#     label: "Current Period"
#     type: time
#     sql: date_add(DATE({% date_start current_date_range %}), INTERVAL ${day_in_period} - 1 DAY) ;;
#     view_label: "_POP"
#     timeframes: [date, week, month, quarter, year]
#   }

  #######################################
  #####         DIMENSIONS          #####
  #######################################

  dimension: id {
    primary_key: yes
    type: string
    sql: ${TABLE}.id ;;
  }

  dimension: boutique_improvement_c {
    type: string
    sql: ${TABLE}.boutique_improvement_c ;;
  }

  dimension: boutique_improvement_other_c {
    type: string
    sql: ${TABLE}.boutique_improvement_other_c ;;
  }

  dimension: brand_c {
    type: string
    sql: ${TABLE}.brand_c ;;
  }

  dimension: browse_other_channels_c {
    type: yesno
    sql: ${TABLE}.browse_other_channels_c ;;
  }

  dimension: case_c {
    type: string
    sql: ${TABLE}.case_c ;;
  }

  dimension: channel_c {
    type: string
    sql: ${TABLE}.channel_c ;;
  }

  dimension: comments_c {
    type: string
    sql: ${TABLE}.comments_c ;;
  }

  dimension: contact_c {
    type: string
    sql: ${TABLE}.contact_c ;;
  }

  dimension: created_by_id {
    type: string
    sql: ${TABLE}.created_by_id ;;
  }

  dimension: currency_iso_code {
    type: string
    sql: ${TABLE}.currency_iso_code ;;
  }

  dimension: customer_satisfaction_c {
    type: string
    sql: ${TABLE}.customer_satisfaction_c ;;
  }

  dimension: customer_service_quality_of_experience_c {
    type: string
    sql: ${TABLE}.customer_service_quality_of_experience_c ;;
  }

  dimension: customer_service_time_to_address_c {
    type: string
    sql: ${TABLE}.customer_service_time_to_address_c ;;
  }

  dimension: customer_service_understand_concerns_c {
    type: string
    sql: ${TABLE}.customer_service_understand_concerns_c ;;
  }

  dimension: delivery_c {
    type: number
    sql: ${TABLE}.delivery_c ;;
  }

  dimension: ease_of_goal_accomplishment_c {
    type: string
    sql: ${TABLE}.ease_of_goal_accomplishment_c ;;
  }

  dimension: email_c {
    type: string
    sql: ${TABLE}.email_c ;;
  }

  dimension: experience_with_store_staff_c {
    type: number
    sql: ${TABLE}.experience_with_store_staff_c ;;
  }

  dimension: experience_with_us_service_c {
    type: number
    sql: ${TABLE}.experience_with_us_service_c ;;
  }

  dimension: facebook_improvement_c {
    type: string
    sql: ${TABLE}.facebook_improvement_c ;;
  }

  dimension: facebook_improvement_other_c {
    type: string
    sql: ${TABLE}.facebook_improvement_other_c ;;
  }

  dimension: favorite_beauty_brand_c {
    type: string
    sql: ${TABLE}.favorite_beauty_brand_c ;;
  }

  dimension: favorite_fashion_brand_c {
    type: string
    sql: ${TABLE}.favorite_fashion_brand_c ;;
  }

  dimension: favorite_fragrance_brand_c {
    type: string
    sql: ${TABLE}.favorite_fragrance_brand_c ;;
  }

  dimension: favorite_jewelry_brand_c {
    type: string
    sql: ${TABLE}.favorite_jewelry_brand_c ;;
  }

  dimension: favorite_makeup_brand_c {
    type: string
    sql: ${TABLE}.favorite_makeup_brand_c ;;
  }

  dimension: favorite_skincare_brand_c {
    type: string
    sql: ${TABLE}.favorite_skincare_brand_c ;;
  }

  dimension: favorite_travel_bag_brand_c {
    type: string
    sql: ${TABLE}.favorite_travel_bag_brand_c ;;
  }

  dimension: improve_one_thing_c {
    type: string
    sql: ${TABLE}.improve_one_thing_c ;;
  }

  dimension: improve_one_thing_other_c {
    type: string
    sql: ${TABLE}.improve_one_thing_other_c ;;
  }

  dimension: instagram_improvement_c {
    type: string
    sql: ${TABLE}.instagram_improvement_c ;;
  }

  dimension: instagram_improvement_other_c {
    type: string
    sql: ${TABLE}.instagram_improvement_other_c ;;
  }

  dimension: is_deleted {
    type: yesno
    sql: ${TABLE}.is_deleted ;;
  }

  dimension: last_modified_by_id {
    type: string
    sql: ${TABLE}.last_modified_by_id ;;
  }

  dimension: lead_source_c {
    type: string
    sql: ${TABLE}.lead_source_c ;;
  }

  dimension: lead_source_other_c {
    type: string
    sql: ${TABLE}.lead_source_other_c ;;
  }

  dimension: likely_recommend_brand_c {
    type: number
    sql: ${TABLE}.likely_recommend_brand_c ;;
  }

  dimension: likely_recommend_store_c {
    type: number
    sql: ${TABLE}.likely_recommend_store_c ;;
  }

  dimension: main_reason_to_come_to_store_c {
    label: "Mission"
    type: string
    sql: ${TABLE}.main_reason_to_come_to_store_c ;;
  }

  dimension: missing_disappointing_other_c {
    type: string
    sql: ${TABLE}.missing_disappointing_other_c ;;
  }

  dimension: missing_dissapointing_c {
    type: string
    sql: ${TABLE}.missing_dissapointing_c ;;
  }

  dimension: name {
    type: string
    sql: ${TABLE}.name ;;
  }

  dimension: owner_id {
    type: string
    sql: ${TABLE}.owner_id ;;
  }

  dimension: place_order_c {
    type: number
    sql: ${TABLE}.place_order_c ;;
  }

  dimension: previous_browsing_c {
    type: string
    sql: ${TABLE}.previous_browsing_c ;;
  }

  dimension: previous_browsing_other_c {
    type: string
    sql: ${TABLE}.previous_browsing_other_c ;;
  }

  dimension: purchase_c {
    type: string
    sql: ${TABLE}.purchase_c ;;
  }

  dimension: reason_recommend_friend_c {
    type: string
    sql: ${TABLE}.reason_recommend_friend_c ;;
  }

  dimension: reason_recommend_friend_other_c {
    type: string
    sql: ${TABLE}.reason_recommend_friend_other_c ;;
  }

  dimension: recommend_facebook_c {
    type: number
    sql: ${TABLE}.recommend_facebook_c ;;
  }

  dimension: recommend_instagram_c {
    type: number
    sql: ${TABLE}.recommend_instagram_c ;;
  }

  dimension: recommend_store_c {
    type: number
    sql: ${TABLE}.recommend_store_c ;;
  }

  dimension: recommend_website_c {
    type: number
    sql: ${TABLE}.recommend_website_c ;;
  }

  dimension: record_type_id {
    type: string
    sql: ${TABLE}.record_type_id ;;
  }

  dimension: sales_associate_welcome_c {
    type: number
    sql: ${TABLE}.sales_associate_welcome_c ;;
  }

  dimension: styling_beauty_advice_c {
    type: number
    sql: ${TABLE}.styling_beauty_advice_c ;;
  }

  dimension: survey_type_c {
    type: string
    sql: ${TABLE}.survey_type_c ;;
  }

  dimension: twitter_improvement_c {
    type: string
    sql: ${TABLE}.twitter_improvement_c ;;
  }

  dimension: twitter_improvement_other_c {
    type: string
    sql: ${TABLE}.twitter_improvement_other_c ;;
  }

  dimension: user_experience_c {
    type: number
    sql: ${TABLE}.user_experience_c ;;
  }

  dimension: website_improvement_c {
    type: string
    sql: ${TABLE}.website_improvement_c ;;
  }

  dimension: website_improvement_other_c {
    type: string
    sql: ${TABLE}.website_improvement_other_c ;;
  }

  dimension: what_can_we_improve_c {
    type: string
    sql: ${TABLE}.what_can_we_improve_c ;;
  }

  dimension: what_can_we_improve_others_c {
    type: string
    sql: ${TABLE}.what_can_we_improve_others_c ;;
  }

  dimension: you_tube_improvement_c {
    type: string
    sql: ${TABLE}.you_tube_improvement_c ;;
  }

  dimension: you_tube_improvement_other_c {
    type: string
    sql: ${TABLE}.you_tube_improvement_other_c ;;
  }

  #####################################
  #####         MEASURES          #####
  #####################################

  measure: store_NPS_responses {
    label: "Count NPS Responses"
    type: count
    filters:{
      field: likely_recommend_store_c
      value: " not null"
    }
  }

  measure: store_CES_responses {
    label: "Count CES Responses"
    type: count
    filters:{
      field: ease_of_goal_accomplishment_c
      value: "-NULL"
    }
  }

  measure: count_Store_promoters {
    label: "Count Store Promoters"
    type:  count
    filters: {
      field:  likely_recommend_store_c
      value: ">8 "
    }
  }

  measure: count_store_detractors {
    label: "Count Store Detractors"
    type:  count
    filters: {
      field: likely_recommend_store_c
      value:  "<7"
    }
  }

  measure: store_NPS {
    label: "Store NPS"
    type:  number
    value_format_name: decimal_2
    sql: ((${count_Store_promoters}-${count_store_detractors})/nullif(${store_NPS_responses},0))*100 ;;
  }

#   measure: store_CES_responses {
#  label: "Count CES Responses"
#  type:  number
#  sql: (select count(${ease_of_goal_accomplishment_c}) from sf_group_survey_result where nullif(${ease_of_goal_accomplishment_c},'') is not null) ;;
#  }

  measure: count_CES_VEasy {
    label: "Count CES V Easy"
    type:  count
    filters: {
      field: ease_of_goal_accomplishment_c
      value:  "Very Easy"
    }
  }

  measure: count_CES_BitTime{
    label: "Count CES Bit of Time"
    type:  count
    filters: {
      field: ease_of_goal_accomplishment_c
      value:  "It took a bit of time"
    }
  }

  measure: count_CES_V_Diff{
    label: "Count CES V Difficult"
    type:  count
    filters: {
      field: ease_of_goal_accomplishment_c
      value:  "Very difficult"
    }
  }

  measure: store_CES {
    label: "Store CES Score"
    type:  number
    value_format_name: percent_0
    sql: ((
          (3*(${count_CES_VEasy}))
          +
          (2*(${count_CES_BitTime}))+
          (${count_CES_V_Diff}))/nullif(${store_CES_responses},0))/3 ;;
  }

  measure: store_CSAT_responses {
    label: "Count CSAT Responses"
    type: count
    filters:{
      field: experience_with_store_staff_c
      value: " not null"}
  }

  measure: avg_csat_score {
    label: "Ave CSAT Score"
    type: average
    sql: ${experience_with_store_staff_c} ;;
    filters:{
      field: experience_with_store_staff_c
      value: " not null"
    }
  }

  measure: store_CSAT {
    label: "Store CSAT Score"
    type:  number
    value_format_name: percent_0
    sql: ${avg_csat_score}/5 ;;
  }

  measure: count {
    type: count
    drill_fields: [id, name]
  }

  ##############################################
  #####         UNUSED DIMENSIONS          #####
  ##############################################

  dimension_group: _fivetran_synced {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}._fivetran_synced ;;
  }

  dimension_group: system_modstamp {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.system_modstamp ;;
  }

  dimension_group: last_modified {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.last_modified_date ;;
  }

  dimension_group: last_referenced {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.last_referenced_date ;;
  }

  dimension_group: last_viewed {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.last_viewed_date ;;
  }

  }
