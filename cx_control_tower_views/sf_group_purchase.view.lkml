view: sf_group_purchase {
  sql_table_name: CX_ControlTower.sf_group_purchase ;;
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: string
    sql: ${TABLE}.id ;;
  }

  dimension_group: _fivetran_synced {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}._fivetran_synced ;;
  }

  dimension: address_validation_c {
    type: string
    sql: ${TABLE}.address_validation_c ;;
  }

  dimension: bonus_product_id_c {
    type: string
    sql: ${TABLE}.bonus_product_id_c ;;
  }

  dimension: bonus_product_name_c {
    type: string
    sql: ${TABLE}.bonus_product_name_c ;;
  }

  dimension: brand_c {
    type: string
    sql: ${TABLE}.brand_c ;;
  }

  dimension_group: business_day_date_c {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.business_day_date_c ;;
  }

  dimension: cashier_id_c {
    type: string
    sql: ${TABLE}.cashier_id_c ;;
  }

  dimension: cashier_name_c {
    type: string
    sql: ${TABLE}.cashier_name_c ;;
  }

  dimension: cc_customer_id_c {
    type: string
    sql: ${TABLE}.cc_customer_id_c ;;
  }

  dimension: cc_customer_number_c {
    type: string
    sql: ${TABLE}.cc_customer_number_c ;;
  }

  dimension: coupon_number_c {
    type: string
    sql: ${TABLE}.coupon_number_c ;;
  }

  dimension: created_by_id {
    type: string
    sql: ${TABLE}.created_by_id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_date ;;
  }

  dimension: currency_iso_code {
    type: string
    sql: ${TABLE}.currency_iso_code ;;
  }

  dimension: customer_id_c {
    type: string
    sql: ${TABLE}.customer_id_c ;;
  }

  dimension: customer_name_c {
    type: string
    sql: ${TABLE}.customer_name_c ;;
  }

  dimension_group: effective_date_c {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.effective_date_c ;;
  }

  dimension: from_sfcc_c {
    type: yesno
    sql: ${TABLE}.from_sfcc_c ;;
  }

  dimension: gift_message_c {
    type: string
    sql: ${TABLE}.gift_message_c ;;
  }

  dimension: invoice_number_c {
    type: string
    sql: ${TABLE}.invoice_number_c ;;
  }

  dimension: is_contact_id_exist_c {
    type: yesno
    sql: ${TABLE}.is_contact_id_exist_c ;;
  }

  dimension: is_deleted {
    type: yesno
    sql: ${TABLE}.is_deleted ;;
  }

  dimension: is_this_a_gift_c {
    type: yesno
    sql: ${TABLE}.is_this_a_gift_c ;;
  }

  dimension_group: last_activity {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.last_activity_date ;;
  }

  dimension: last_modified_by_id {
    type: string
    sql: ${TABLE}.last_modified_by_id ;;
  }

  dimension_group: last_modified {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.last_modified_date ;;
  }

  dimension_group: last_referenced {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.last_referenced_date ;;
  }

  dimension_group: last_viewed {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.last_viewed_date ;;
  }

  dimension: name {
    type: string
    sql: ${TABLE}.name ;;
  }

  dimension: order_number_c {
    type: string
    sql: ${TABLE}.order_number_c ;;
  }

  dimension: order_sccsync_status_c {
    type: string
    sql: ${TABLE}.order_sccsync_status_c ;;
  }

  dimension: owner_id {
    type: string
    sql: ${TABLE}.owner_id ;;
  }

  dimension: record_type_id {
    type: string
    sql: ${TABLE}.record_type_id ;;
  }

  dimension: sales_associate_id_c {
    type: string
    sql: ${TABLE}.sales_associate_id_c ;;
  }

  dimension: sales_associate_name_c {
    type: string
    sql: ${TABLE}.sales_associate_name_c ;;
  }

  dimension: sfcc_order_number_c {
    type: string
    sql: ${TABLE}.sfcc_order_number_c ;;
  }

  dimension: sfcc_order_total_c {
    type: number
    sql: ${TABLE}.sfcc_order_total_c ;;
  }

  dimension: sfcc_payment_status_c {
    type: string
    sql: ${TABLE}.sfcc_payment_status_c ;;
  }

  dimension: shipment_status_c {
    type: string
    sql: ${TABLE}.shipment_status_c ;;
  }

  dimension: shipping_method_c {
    type: string
    sql: ${TABLE}.shipping_method_c ;;
  }

  dimension: shipping_promotion_c {
    type: string
    sql: ${TABLE}.shipping_promotion_c ;;
  }

  dimension: shipping_status_c {
    type: string
    sql: ${TABLE}.shipping_status_c ;;
  }

  dimension: shipping_total_c {
    type: number
    sql: ${TABLE}.shipping_total_c ;;
  }

  dimension: shipping_total_tax_c {
    type: number
    sql: ${TABLE}.shipping_total_tax_c ;;
  }

  dimension: shipping_total_tax_usd_c {
    type: number
    sql: ${TABLE}.shipping_total_tax_usd_c ;;
  }

  dimension: shipping_total_usd_c {
    type: number
    sql: ${TABLE}.shipping_total_usd_c ;;
  }

  dimension: special_order_number_c {
    type: string
    sql: ${TABLE}.special_order_number_c ;;
  }

  dimension: sscid_c {
    type: string
    sql: ${TABLE}.sscid_c ;;
  }

  dimension: status_c {
    type: string
    sql: ${TABLE}.status_c ;;
  }

  dimension: store_city_c {
    type: string
    sql: ${TABLE}.store_city_c ;;
  }

  dimension: store_description_c {
    type: string
    sql: ${TABLE}.store_description_c ;;
  }

  dimension: store_id_c {
    type: string
    sql: ${TABLE}.store_id_c ;;
  }

  dimension: store_market_c {
    type: string
    sql: ${TABLE}.store_market_c ;;
  }

  dimension_group: system_modstamp {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.system_modstamp ;;
  }

  dimension: tax_amount_c {
    type: number
    sql: ${TABLE}.tax_amount_c ;;
  }

  dimension: tax_amount_usd_c {
    type: number
    sql: ${TABLE}.tax_amount_usd_c ;;
  }

  dimension: tender_type_group_c {
    type: string
    sql: ${TABLE}.tender_type_group_c ;;
  }

  dimension: total_amount_c {
    type: number
    sql: ${TABLE}.total_amount_c ;;
  }

  dimension: total_amount_usd_c {
    type: number
    sql: ${TABLE}.total_amount_usd_c ;;
  }

  dimension: total_discount_amount_c {
    type: number
    sql: ${TABLE}.total_discount_amount_c ;;
  }

  dimension: total_discount_amount_usd_c {
    type: number
    sql: ${TABLE}.total_discount_amount_usd_c ;;
  }

  dimension: transaction_code_c {
    type: string
    sql: ${TABLE}.transaction_code_c ;;
  }

  dimension_group: transaction_date_time_c {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.transaction_date_time_c ;;
  }

  dimension: transaction_number_c {
    type: string
    sql: ${TABLE}.transaction_number_c ;;
  }

  dimension: transaction_sequence_number_c {
    type: string
    sql: ${TABLE}.transaction_sequence_number_c ;;
  }

  dimension: transaction_type_c {
    type: string
    sql: ${TABLE}.transaction_type_c ;;
  }

  dimension: voucher_number_c {
    type: string
    sql: ${TABLE}.voucher_number_c ;;
  }

  measure: count {
    type: count
    drill_fields: [id, name]
  }
}
