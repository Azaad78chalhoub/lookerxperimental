view: ml_beauty_churn_predictions {
  sql_table_name: chb-prod-data-cust.prod_ml_data.churn_predictions ;;
  #
  # Define your dimensions and measures here, like this:
  dimension: brand_customer_unique_id {
    type: string
    sql: ${TABLE}.brand_customer_unique_id ;;
  }

  dimension: prediction_datetime {
    type: string
    sql: ${TABLE}.prediction_datetime ;;
  }

  dimension: prediction_date {
    type: date
    sql: CAST(LEFT(${TABLE}.prediction_datetime, 10) AS DATE) ;;
  }

  dimension: pk {
    type: string
    primary_key: yes
    hidden: yes
    sql:CONCAT(${TABLE}.brand_customer_unique_id, ${TABLE}.prediction_datetime) ;;
  }

  dimension: class_1 {
    type: number
    sql:${TABLE}.class_1 ;;
  }

  dimension: class_0 {
    type: number
    sql:${TABLE}.class_0 ;;
  }

  measure: count_preds {
    label: "Count Predictions"
    type: count_distinct
    sql: ${pk} ;;
  }

  measure: count_positive_preds {
    label: "Count Positive Predictions"
    type: count_distinct
    sql: CASE WHEN ${class_1} >= ${class_0} THEN ${pk} ELSE NULL END ;;
  }

  measure: true_preds_perc {
    label: "Positive Predictions Rate"
    type: number
    value_format: "0.0%"
    sql: ${count_positive_preds}/${count_preds} ;;
  }


  ### feature attributions
  dimension: total_sales_usd {
    group_label: "Feature attributions"
    type: number
    sql: ${TABLE}.total_sales_usd ;;
  }

  dimension: returned_sales_perc {
    group_label: "Feature attributions"
    type: number
    sql: ${TABLE}.returned_sales_perc ;;
  }

  dimension: avg_transaction_value {
    group_label: "Feature attributions"
    type: number
    sql: ${TABLE}.avg_transaction_value ;;
  }

  dimension: total_items {
    group_label: "Feature attributions"
    type: number
    sql: ${TABLE}.total_items ;;
  }

  dimension: returned_txn_perc {
    group_label: "Feature attributions"
    type: number
    sql: ${TABLE}.returned_txn_perc ;;
  }

  dimension: total_returned_usd {
    group_label: "Feature attributions"
    type: number
    sql: ${TABLE}.total_returned_usd ;;
  }

  dimension: ecomm_sales_perc {
    group_label: "Feature attributions"
    type: number
    sql: ${TABLE}.ecomm_sales_perc ;;
  }

  dimension: ecomm_transactions {
    group_label: "Feature attributions"
    type: number
    sql: ${TABLE}.ecomm_transactions ;;
  }

  dimension: count_returned_items {
    group_label: "Feature attributions"
    type: number
    sql: ${TABLE}.count_returned_items ;;
  }

  dimension: ecomm_items_perc {
    group_label: "Feature attributions"
    type: number
    sql: ${TABLE}.ecomm_items_perc ;;
  }

  dimension: returned_items_perc {
    group_label: "Feature attributions"
    type: number
    sql: ${TABLE}.returned_items_perc ;;
  }


  dimension: avg_item_count {
    group_label: "Feature attributions"
    type: number
    sql: ${TABLE}.avg_item_count ;;
  }

  dimension: ecomm_usd_sales {
    group_label: "Feature attributions"
    type: number
    sql: ${TABLE}.ecomm_usd_sales ;;
  }

  dimension: total_returned_transactions {
    group_label: "Feature attributions"
    type: number
    sql: ${TABLE}.total_returned_transactions ;;
  }

  dimension: ecomm_items {
    group_label: "Feature attributions"
    type: number
    sql: ${TABLE}.ecomm_items ;;
  }

  dimension: transaction_count {
    group_label: "Feature attributions"
    type: number
    sql: ${TABLE}.transaction_count ;;
  }

}
