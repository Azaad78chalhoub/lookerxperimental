view: finance_budgets {
  sql_table_name: `chb-prod-supplychain-data.prod_finance_hyperion.finance_budgets`
    ;;

  dimension: account {
    type: string
    sql: ${TABLE}.account ;;
  }

  dimension: account_type {
    type: string
    sql: ${TABLE}.account_type ;;
  }

  dimension: boat {
    type: string
    sql: ${TABLE}.boat ;;
  }

  dimension: bu_code {
    type: string
    sql: ${TABLE}.bu_code ;;
  }

  dimension: bu_dep_desc {
    type: string
    sql: ${TABLE}.bu_dep_desc ;;
  }

  dimension: bu_desc {
    type: string
    sql: ${TABLE}.bu_desc ;;
  }

  dimension: budget_aed {
    type: number
    sql: ${TABLE}.budget_aed ;;
  }
  measure: cogs_target_usd {
    type: sum
    hidden: no
    label: "R0 COGS Budget USD"
    sql: case when ${line}='Cost Of Sales' and ${account_type}='P&L' then ${budget_usd} end;;
    value_format_name: usd
  }
  measure: cogs_target_aed {
    type: sum
    hidden: no
    label: "R0 COGS Budget AED"
    sql: case when ${line}='Cost Of Sales' and ${account_type}='P&L' then ${budget_aed} end;;
    value_format_name: decimal_2
  }
  measure: sales_target_usd {
    type: sum
    hidden: no
    label: "R0 Sales Budget USD"
    sql: case when ${line}='Net Sales' and ${account_type}='P&L' then ${budget_usd} end;;
    value_format_name: usd
  }
  measure: sales_target_aed {
    type: sum
    hidden: no
    label: "R0 Sales Budget AED"
    sql: case when ${line}='Net Sales' and ${account_type}='P&L' then ${budget_aed} end;;
    value_format_name: decimal_2
  }
  measure: gm_target_usd {
    type: number
    hidden: no
    label: "R0 Gross Margin Budget USD"
    sql: ${sales_target_usd}+${cogs_target_usd};;
    value_format_name: usd
  }
  measure: gm_target_aed {
    type: number
    hidden: no
    label: "R0 Gross Margin Budget AED"
    sql: ${sales_target_aed}+${cogs_target_aed};;
    value_format_name: decimal_2
  }
  measure: total_budget_usd {
    type: sum
    hidden: no
    label: "R0 Budget USD"
    sql: ${budget_usd} ;;
    value_format_name: usd
  }
  measure: total_budget_aed {
    type: sum
    hidden: no
    label: "R0 Budget AED"
    sql: ${budget_aed} ;;
    value_format_name: decimal_2
  }
  dimension: budget_local {
    type: number
    sql: ${TABLE}.budget_local ;;
  }

  dimension: budget_usd {
    type: number
    sql: ${TABLE}.budget_usd ;;
  }

  dimension: business_unit {
    type: string
    sql: ${TABLE}.business_unit ;;
  }

  dimension: country {
    type: string
    map_layer_name: countries
    sql: ${TABLE}.country ;;
  }

  dimension: currency {
    type: string
    sql: ${TABLE}.currency ;;
  }

  dimension: gl_account_code {
    type: string
    sql: ${TABLE}.gl_account_code ;;
  }

  dimension: gl_account_desc {
    type: string
    sql: ${TABLE}.gl_account_desc ;;
  }

  dimension: gl_account_index {
    type: string
    sql: ${TABLE}.gl_account_index ;;
  }

  dimension: legal_entity {
    type: string
    sql: ${TABLE}.legal_entity ;;
  }

  dimension: level_2 {
    type: string
    sql: ${TABLE}.level_2 ;;
  }

  dimension: level_2_code {
    type: string
    sql: ${TABLE}.level_2_code ;;
  }

  dimension: level_2_index {
    type: number
    sql: ${TABLE}.level_2_index ;;
  }

  dimension: level_3 {
    type: string
    sql: ${TABLE}.level_3 ;;
  }

  dimension: level_3_code {
    type: number
    sql: ${TABLE}.level_3_code ;;
  }

  dimension: level_3_index {
    type: string
    sql: ${TABLE}.level_3_index ;;
  }

  dimension: level_4 {
    type: string
    sql: ${TABLE}.level_4 ;;
  }

  dimension: line {
    type: string
    sql: ${TABLE}.line ;;
  }

  dimension: line_code {
    type: number
    sql: ${TABLE}.line_code ;;
  }

  dimension: line_index {
    type: number
    sql: ${TABLE}.line_index ;;
  }

  dimension: month {
    type: number
    sql: ${TABLE}.month ;;
  }
  dimension: date_based_on_month_string {
    description: "Date based on month"
    type: string
    sql:CASE WHEN ${TABLE}.month =1 then "2021-01-01"
    WHEN ${TABLE}.month =2 then "2021-02-01"
    WHEN ${TABLE}.month =3 then "2021-03-01"
    WHEN ${TABLE}.month =4 then "2021-04-01"
    WHEN ${TABLE}.month =5 then "2021-05-01"
    WHEN ${TABLE}.month =6 then "2021-06-01"
    WHEN ${TABLE}.month =7 then "2021-07-01"
    WHEN ${TABLE}.month =8 then "2021-08-01"
    WHEN ${TABLE}.month =9 then "2021-09-01"
    WHEN ${TABLE}.month =10 then "2021-10-01"
    WHEN ${TABLE}.month =11 then "2021-11-01"
    WHEN ${TABLE}.month =12 then "2021-12-01"
    end;;
    }

  dimension_group: date_based_on_month_dt {
    description: "Date based on month"
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql:CASE WHEN ${TABLE}.month =1 then cast("2021-01-01" as timestamp)
          WHEN ${TABLE}.month =2 then  cast("2021-02-01" as timestamp)
          WHEN ${TABLE}.month =3 then  cast("2021-03-01" as timestamp)
          WHEN ${TABLE}.month =4 then  cast("2021-04-01" as timestamp)
          WHEN ${TABLE}.month =5 then  cast("2021-05-01" as timestamp)
          WHEN ${TABLE}.month =6 then  cast("2021-06-01" as timestamp)
          WHEN ${TABLE}.month =7 then  cast("2021-07-01" as timestamp)
          WHEN ${TABLE}.month =8 then  cast("2021-08-01" as timestamp)
          WHEN ${TABLE}.month =9 then  cast("2021-09-01" as timestamp)
          WHEN ${TABLE}.month =10 then  cast("2021-10-01" as timestamp)
          WHEN ${TABLE}.month =11 then  cast("2021-11-01" as timestamp)
          WHEN ${TABLE}.month =12 then  cast("2021-12-01"as timestamp)
          end;;
  }

  dimension: date_based_on_month_date {
    description: "Date based on month"
    type: date
    sql:CAST(${TABLE}.date_based_on_month_string AS DATE);;
    }

  dimension: ownership {
    type: string
    sql: ${TABLE}.ownership ;;
  }

  dimension: r_code {
    type: string
    sql: ${TABLE}.r_code ;;
  }

  dimension: r_index {
    type: number
    sql: ${TABLE}.r_index ;;
  }

  dimension: scenario {
    type: string
    sql: ${TABLE}.scenario ;;
  }

  dimension: store_code {
    type: string
    sql: ${TABLE}.store_code ;;
  }

  dimension: sub_vertical {
    type: string
    sql: ${TABLE}.sub_vertical ;;
  }

  dimension: type_of_map {
    type: string
    sql: ${TABLE}.type_of_map ;;
  }

  dimension: version {
    type: string
    sql: ${TABLE}.version ;;
  }

  dimension: vertical {
    type: string
    sql: ${TABLE}.vertical ;;
  }

  dimension: year {
    type: number
    sql: ${TABLE}.year ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
