view: dm_accounts_payable_by_invoice {
  sql_table_name: `chb-prod-supplychain-data.prod_finance_supply_chain.dm_accounts_payable_by_invoice`
    ;;

  dimension_group: booked {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.BOOKED_DATE ;;
  }

  dimension: brand {
    type: string
    sql: ${TABLE}.BRAND ;;
  }

  dimension: bu {
    type: string
    sql: ${TABLE}.BU ;;
  }

  dimension: bu_desc {
    type: string
    sql: ${TABLE}.BU_DESC ;;
  }

  dimension: code_combination_id {
    type: number
    sql: ${TABLE}.CODE_COMBINATION_ID ;;
  }

  dimension: country {
    type: string
    map_layer_name: countries
    sql: ${TABLE}.COUNTRY ;;
  }

  dimension: cumul_dist_amount_remaining {
    type: number
    sql: ${TABLE}.CUMUL_DIST_AMOUNT_REMAINING ;;
  }

  dimension: cumul_dist_amount_remaining_usd {
    type: number
    sql: ${TABLE}.CUMUL_DIST_AMOUNT_REMAINING_USD ;;
  }

  dimension: cumul_entered_rounded_cr {
    type: number
    sql: ${TABLE}.CUMUL_ENTERED_ROUNDED_CR ;;
  }

  dimension: cumul_entered_rounded_dr {
    type: number
    sql: ${TABLE}.CUMUL_ENTERED_ROUNDED_DR ;;
  }

  dimension: currency {
    type: string
    sql: ${TABLE}.CURRENCY ;;
  }

  dimension_group: date {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.date ;;
  }

  dimension: dist_amount_remaining {
    type: number
    sql: ${TABLE}.DIST_AMOUNT_REMAINING ;;
  }

  dimension: dist_amount_remaining_usd {
    type: number
    sql: ${TABLE}.DIST_AMOUNT_REMAINING_USD ;;
  }

  dimension: due_date {
    type: string
    sql: ${TABLE}.DUE_DATE ;;
  }

  dimension: ent_code {
    type: string
    sql: ${TABLE}.ENT_CODE ;;
  }

  dimension: entered_rounded_cr {
    type: number
    sql: ${TABLE}.ENTERED_ROUNDED_CR ;;
  }

  dimension: entered_rounded_dr {
    type: number
    sql: ${TABLE}.ENTERED_ROUNDED_DR ;;
  }

  dimension: gl {
    type: string
    sql: ${TABLE}.GL ;;
  }

  dimension_group: gl {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.GL_DATE ;;
  }

  dimension_group: gl_date_tb_from {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.GL_DATE_TB_FROM ;;
  }

  dimension_group: gl_date_tb_to {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.GL_DATE_TB_TO ;;
  }

  dimension: gl_desc {
    type: string
    sql: ${TABLE}.GL_DESC ;;
  }

  dimension: head_amount_paid {
    type: number
    value_format_name: id
    sql: ${TABLE}.HEAD_AMOUNT_PAID ;;
  }

  dimension: head_amount_remaining {
    type: number
    sql: ${TABLE}.HEAD_AMOUNT_REMAINING ;;
  }

  dimension: head_invoice_amount {
    type: number
    sql: ${TABLE}.HEAD_INVOICE_AMOUNT ;;
  }

  dimension_group: invoice {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.INVOICE_DATE ;;
  }

  dimension: invoice_desc {
    type: string
    sql: ${TABLE}.INVOICE_DESC ;;
  }

  dimension: invoice_id {
    type: number
    sql: ${TABLE}.INVOICE_ID ;;
  }

  dimension: invoice_num {
    type: string
    sql: ${TABLE}.INVOICE_NUM ;;
  }

  dimension: invoice_type {
    type: string
    sql: ${TABLE}.INVOICE_TYPE ;;
  }

  dimension: legal_entity {
    type: string
    sql: ${TABLE}.LEGAL_ENTITY ;;
  }

  dimension: operating_unit {
    type: string
    sql: ${TABLE}.OPERATING_UNIT ;;
  }

  dimension: org_id {
    type: number
    sql: ${TABLE}.ORG_ID ;;
  }

  dimension: row_number_boundary_dates {
    type: number
    sql: ${TABLE}.ROW_NUMBER_BOUNDARY_DATES ;;
  }

  dimension: original_entered_rounded_cr_usd{
    type: number
    sql: ${TABLE}.ROW_NUMBER_BOUNDARY_DATES ;;
  }

  dimension: source {
    type: string
    sql: ${TABLE}.SOURCE ;;
  }

  dimension: sup_num {
    type: string
    sql: ${TABLE}.SUP_NUM ;;
  }

  dimension: sup_site {
    type: string
    sql: ${TABLE}.SUP_SITE ;;
  }

  dimension: terms {
    type: string
    sql: ${TABLE}.TERMS ;;
  }

  dimension: vendor_id {
    type: number
    sql: ${TABLE}.VENDOR_ID ;;
  }

  dimension: vendor_name {
    type: string
    sql: ${TABLE}.VENDOR_NAME ;;
  }

  dimension: vendor_type_1 {
    type: string
    sql: ${TABLE}.VENDOR_TYPE_1 ;;
  }

  dimension: vendor_type_2 {
    type: string
    sql: ${TABLE}.VENDOR_TYPE_2 ;;
  }

  dimension: vertical {
    type: string
    sql: ${TABLE}.VERTICAL ;;
  }

  dimension: voucher_no {
    type: string
    sql: ${TABLE}.VOUCHER_NO ;;
  }

  measure: amount_remain_usd{
    type: sum
    label: "Cumulative Amount Remaining USD"
    drill_fields: [vendor_name]
    sql: ${cumul_dist_amount_remaining_usd} ;;
  }

  measure: original_amount_remain_usd{
    type: sum
    label: "Original Invoice Amount USD"
    drill_fields: [vendor_name]
    sql: ${original_entered_rounded_cr_usd} ;;
  }
}
