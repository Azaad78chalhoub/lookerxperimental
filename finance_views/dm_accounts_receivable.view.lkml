view: dm_accounts_receivable {
  sql_table_name: `chb-prod-supplychain-data.prod_finance_supply_chain.dm_accounts_receivable`
    ;;

  dimension: account_class {
    type: string
    sql: ${TABLE}.ACCOUNT_CLASS ;;
  }

  dimension: accum_amount_applied_line {
    type: number
    sql: ${TABLE}.accum_amount_applied_line ;;
  }

  dimension: amount {
    type: number
    sql: ${TABLE}.amount ;;
  }

  dimension: amount_applied_line {
    type: number
    sql: ${TABLE}.amount_applied_line ;;
  }

  dimension: amount_applied_total {
    type: number
    sql: ${TABLE}.amount_applied_total ;;
  }

  dimension_group: apply {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.apply_date ;;
  }

  dimension: brand {
    type: string
    sql: ${TABLE}.BRAND ;;
  }

  dimension: bu {
    type: string
    sql: ${TABLE}.BU ;;
  }

  dimension: bu_desc {
    type: string
    sql: ${TABLE}.BU_DESC ;;
  }

  dimension: code_combination_id {
    type: number
    sql: ${TABLE}.CODE_COMBINATION_ID ;;
  }

  dimension: cust_trx_line_gl_dist_id {
    type: number
    sql: ${TABLE}.cust_trx_line_gl_dist_id ;;
  }

  dimension: customer_account_no {
    type: string
    sql: ${TABLE}.CUSTOMER_ACCOUNT_NO ;;
  }

  dimension: customer_trx_id {
    type: number
    sql: ${TABLE}.customer_trx_id ;;
  }

  dimension_group: date {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.date ;;
  }

  dimension_group: final_end_interval {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.final_end_interval ;;
  }

  dimension: final_line_remaining {
    type: number
    sql: ${TABLE}.final_line_remaining ;;
  }

  dimension: final_remaining_receivable_usd {
    type: number
    sql: ${TABLE}.FINAL_REMAINING_RECEIVABLE_USD ;;
  }

  dimension: final_remaining_receivable_usd_12_m_ago {
    type: number
    sql: ${TABLE}.FINAL_REMAINING_RECEIVABLE_USD_12M_AGO ;;
  }

  dimension: final_remaining_receivable_usd_1_m_ago {
    type: number
    sql: ${TABLE}.FINAL_REMAINING_RECEIVABLE_USD_1M_AGO ;;
  }

  dimension: final_remaining_receivable_usd_3_m_ago {
    type: number
    sql: ${TABLE}.FINAL_REMAINING_RECEIVABLE_USD_3M_AGO ;;
  }

  dimension: final_remaining_receivable_usd_6_m_ago {
    type: number
    sql: ${TABLE}.FINAL_REMAINING_RECEIVABLE_USD_6M_AGO ;;
  }

  dimension_group: from {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.from_date ;;
  }

  dimension_group: gl_date_closed {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.gl_date_closed ;;
  }

  dimension: interval_rank {
    type: number
    sql: ${TABLE}.interval_rank ;;
  }

  dimension: invoice_credit_note_no {
    type: string
    sql: ${TABLE}.INVOICE_CREDIT_NOTE_NO ;;
  }

  dimension: invoice_currency {
    type: string
    sql: ${TABLE}.INVOICE_CURRENCY ;;
  }

  dimension_group: invoice {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.INVOICE_DATE ;;
  }

  dimension: line_applied_rank {
    type: number
    sql: ${TABLE}.line_applied_rank ;;
  }

  dimension: line_remaining {
    type: number
    sql: ${TABLE}.line_remaining ;;
  }

  dimension: max_interval_rank {
    type: number
    sql: ${TABLE}.max_interval_rank ;;
  }

  dimension: max_line_applied_rank {
    type: number
    sql: ${TABLE}.max_line_applied_rank ;;
  }

  dimension: org_id {
    type: number
    sql: ${TABLE}.ORG_ID ;;
  }

  dimension: prcnt {
    type: number
    sql: ${TABLE}.prcnt ;;
  }

  dimension: receivable_application_id {
    type: number
    sql: ${TABLE}.receivable_application_id ;;
  }

  dimension: rep_currency {
    type: string
    sql: ${TABLE}.REP_CURRENCY ;;
  }

  dimension: sales_last182_value_sum {
    type: number
    sql: ${TABLE}.sales_last182_value_sum ;;
  }

  dimension: sales_last30_value_sum {
    type: number
    sql: ${TABLE}.sales_last30_value_sum ;;
  }

  dimension: sales_last365_value_sum {
    type: number
    sql: ${TABLE}.sales_last365_value_sum ;;
  }

  dimension: sales_last90_value_sum {
    type: number
    sql: ${TABLE}.sales_last90_value_sum ;;
  }

  dimension: set_of_books_id {
    type: number
    sql: ${TABLE}.SET_OF_BOOKS_ID ;;
  }

  dimension: total_amount {
    type: number
    sql: ${TABLE}.total_amount ;;
  }

  dimension_group: trx {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.trx_date ;;
  }

  dimension: vertical {
    type: string
    sql: ${TABLE}.VERTICAL ;;
  }

  measure: dso_1m {
    type: number
    label: "1) - DSO 1 Month"
    description: "Days Sales Outstanding for 1 month accounting period back"
    hidden: no
    value_format: "#.0;(#.0)"
    sql: 30 /
              NULLIF(sum(${sales_last30_value_sum}) /
                            NULLIF((sum(${final_remaining_receivable_usd}) + sum(${final_remaining_receivable_usd_1_m_ago}))/2, 0), 0) ;;
  }

  measure: dso_3m {
    type: number
    label: "DSO 3 Month"
    group_label: "2) - DSO 3 Months"
    description: "Days Sales Outstanding for 3 month accounting period back"
    hidden: no
    value_format: "#.0;(#.0)"
    sql: 90 /
              NULLIF(sum(${sales_last90_value_sum}) /
                            NULLIF((sum(${final_remaining_receivable_usd}) + sum(${final_remaining_receivable_usd_3_m_ago}))/2, 0), 0) ;;
  }

  measure: sales_last3m {
    type: number
    label: "Sales 3m"
    group_label: "2) - DSO 3 Months"
    description: "Sales for last 3 months"
    hidden: no
    value_format: "$#.0;($#.0)"
    sql:
              sum(${sales_last90_value_sum})  ;;
  }

  measure: average_ar_3m {
    type: number
    label: "AR 3m"
    group_label: "2) - DSO 3 Months"
    description: "Average AR for last 3 months"
    hidden: no
    value_format: "$#.0;($#.0)"
    sql:
              NULLIF((sum(${final_remaining_receivable_usd}) + sum(${final_remaining_receivable_usd_3_m_ago}))/2, 0)  ;;
  }

  measure: dso_6m {
    type: number
    label: "1) - DSO 6 Month"
    description: "Days Sales Outstanding for 6 month accounting period back"
    hidden: no
    value_format: "#.0;(#.0)"
    sql: 182 /
              NULLIF(sum(${sales_last182_value_sum}) /
                            NULLIF((sum(${final_remaining_receivable_usd}) + sum(${final_remaining_receivable_usd_6_m_ago}))/2, 0), 0) ;;
  }

  measure: dso_12m {
    type: number
    label: "1) - DSO 12 Month"
    description: "Days Sales Outstanding for 12 month accounting period back"
    hidden: no
    value_format: "#.0;(#.0)"
    sql: 365 /
              NULLIF(sum(${sales_last365_value_sum}) /
                            NULLIF((sum(${final_remaining_receivable_usd}) + sum(${final_remaining_receivable_usd_12_m_ago}))/2, 0), 0) ;;
  }
}
