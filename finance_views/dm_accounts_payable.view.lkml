view: dm_accounts_payable {
  sql_table_name: `chb-prod-supplychain-data.prod_finance_supply_chain.dm_accounts_payable`
    ;;

  dimension: brand {
    type: string
    label: "Brand"
    sql: ${TABLE}.BRAND ;;
  }

  dimension: bu {
    type: string
    label: "Business Unit Code"
    sql: ${TABLE}.BU ;;
  }

  dimension: bu_desc {
    type: string
    label: "Business Unit Description"
    sql: ${TABLE}.BU_DESC ;;
  }

  dimension: cogs_last182_value_sum {
    type: number
    hidden: yes
    sql: ${TABLE}.cogs_last182_value_sum ;;
  }

  dimension: cogs_last30_value_sum {
    type: number
    hidden: yes
    sql: ${TABLE}.cogs_last30_value_sum ;;
  }

  dimension: cogs_last365_value_sum {
    type: number
    hidden: yes
    sql: ${TABLE}.cogs_last365_value_sum ;;
  }

  dimension: cogs_last90_value_sum {
    type: number
    hidden: yes
    sql: ${TABLE}.cogs_last90_value_sum ;;
  }

  dimension: cumul_dist_amount_remaining_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.CUMUL_DIST_AMOUNT_REMAINING_USD ;;
  }

  dimension: end_of_month_day {
    type: number
    hidden: yes
    sql: ${TABLE}.end_month_date ;;
  }

  dimension_group: date {
    type: time
    timeframes: [
      raw,
      date,
      day_of_month,
      week,
      month,
      quarter,
      year
    ]
    label: "Date"
    convert_tz: no
    datatype: date
    sql: ${TABLE}.date ;;
#     sql: DATE(${year}, ${month}, ${end_of_month_day}) ;;
  }

#   dimension: month {
#     type: number
#     sql: EXTRACT(month from ${TABLE}.date);;
#   }
#
#   dimension: year {
#     type: number
#     sql: EXTRACT(year from ${TABLE}.date);;
#   }



  dimension: dist_amount_remaining_usd_12_m_ago {
    type: number
    hidden: yes
    sql: ${TABLE}.DIST_AMOUNT_REMAINING_USD_12M_AGO ;;
  }

  dimension: dist_amount_remaining_usd_1_m_ago {
    type: number
    hidden: yes
    sql: ${TABLE}.DIST_AMOUNT_REMAINING_USD_1M_AGO ;;
  }

  dimension: dist_amount_remaining_usd_3_m_ago {
    type: number
    hidden: yes
    sql: ${TABLE}.DIST_AMOUNT_REMAINING_USD_3M_AGO ;;
  }

  dimension: dist_amount_remaining_usd_6_m_ago {
    type: number
    hidden: yes
    sql: ${TABLE}.DIST_AMOUNT_REMAINING_USD_6M_AGO ;;
  }

  dimension: vertical {
    type: string
    label: "Vertical"
    sql: ${TABLE}.VERTICAL ;;
  }

  measure: average_ap_12m{
    type: number
    hidden: yes
    sql: (sum(${cumul_dist_amount_remaining_usd}) + sum(${dist_amount_remaining_usd_12_m_ago}))/2 ;;
  }

  measure: ap_turnover_ratio {
    type: number
    hidden: yes
    sql: sum(${cogs_last365_value_sum})/NULLIF(${average_ap_12m}, 0) ;;
  }

  measure: dpo_1m {
    type: number
    label: "1) - DPO 1 Month"
    description: "Days Payable Outstanding for 1 month accounting period back"
    hidden: no
    value_format: "#.0;(#.0)"
    sql: 30 /
              NULLIF(sum(${cogs_last30_value_sum}) /
                            NULLIF((sum(${cumul_dist_amount_remaining_usd}) + sum(${dist_amount_remaining_usd_1_m_ago}))/2, 0), 0) ;;
  }

  measure: dpo_3m {
    type: number
    label: "DPO 3 Months"
    group_label: "2) - DPO 3 Months"
    description: "Days Payable Outstanding for 3 months accounting period back"
    hidden: no
    value_format: "#.0;(#.0)"
    sql: 90 /
              NULLIF(sum(${cogs_last90_value_sum}) /
                            NULLIF((sum(${cumul_dist_amount_remaining_usd}) + sum(${dist_amount_remaining_usd_3_m_ago}))/2, 0), 0) ;;
  }

  measure: cogs_last3m {
    type: number
    label: "COGS"
    group_label: "2) - DPO 3 Months"
    description: "COGS for last 3 months"
    hidden: no
    value_format: "$#.0;($#.0)"
    sql:
              sum(${cogs_last90_value_sum})  ;;
  }

  measure: average_ap_last3m{
    type: number
    label: "Average AP"
    group_label: "2) - DPO 3 Months"
    description: "Average AP last 3 months"
    hidden: no
    value_format: "$#.0;($#.0)"
    sql:
              NULLIF((sum(${cumul_dist_amount_remaining_usd}) + sum(${dist_amount_remaining_usd_3_m_ago}))/2, 0)  ;;
  }

  measure: number_of_days_3m{
    type: number
    label: "Number of Days"
    group_label: "2) - DPO 3 Months"
    description: "Number of Days"
    hidden: no
    value_format: "#"
    sql: 90 ;;
  }

  measure: dpo_6m {
    type: number
    label: "3) - DPO 6 Months"
    description: "Days Payable Outstanding for 6 months accounting period"
    hidden: no
    value_format: "#.0;(#.0)"
    sql: 182 /
              NULLIF(sum(${cogs_last182_value_sum}) /
                            NULLIF((sum(${cumul_dist_amount_remaining_usd}) + sum(${dist_amount_remaining_usd_6_m_ago}))/2, 0), 0) ;;
  }

  measure: dpo_12m {
    type: number
    label: "4) - DPO 12 Months"
    description: "Days Payable Outstanding for 12 months accounting period"
    hidden: no
    value_format: "#.0;(#.0)"
    sql: 365 /
              NULLIF(sum(${cogs_last365_value_sum}) /
                            NULLIF((sum(${cumul_dist_amount_remaining_usd}) + sum(${dist_amount_remaining_usd_12_m_ago}))/2, 0), 0) ;;
  }




}
