view: dm_inventory_outstanding {
  sql_table_name: `chb-prod-supplychain-data.prod_finance_supply_chain.dm_inventory_outstanding`
    ;;

  dimension: avg_inventory_12m {
    type: number
    sql: ${TABLE}.avg_inventory_12m ;;
  }

  dimension: avg_inventory_1m {
    type: number
    sql: ${TABLE}.avg_inventory_1m ;;
  }

  dimension: avg_inventory_3m {
    type: number
    sql: ${TABLE}.avg_inventory_3m ;;
  }

  dimension: avg_inventory_6m {
    type: number
    sql: ${TABLE}.avg_inventory_6m ;;
  }

  dimension: brand {
    type: string
    sql: ${TABLE}.brand ;;
  }

  dimension: business_unit_final {
    type: string
    sql: ${TABLE}.business_unit_final ;;
  }

  dimension: cogs_last182_value_sum {
    type: number
    sql: ${TABLE}.cogs_last182_value_sum ;;
  }

  dimension: cogs_last30_value_sum {
    type: number
    sql: ${TABLE}.cogs_last30_value_sum ;;
  }

  dimension: cogs_last365_value_sum {
    type: number
    sql: ${TABLE}.cogs_last365_value_sum ;;
  }

  dimension: cogs_last90_value_sum {
    type: number
    sql: ${TABLE}.cogs_last90_value_sum ;;
  }

  dimension_group: stock {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.stock_date ;;
  }

  dimension: vertical {
    type: string
    sql: ${TABLE}.vertical ;;
  }

  measure: dio_1m {
    type: number
    label: "DIO for 1 month"
    description: "Days Inventory Outstanding for 1 month period back"
    hidden: no
    value_format: "#.0;(#.0)"
    sql:  30 /
              NULLIF(sum(${cogs_last30_value_sum}) /
                            NULLIF(sum(${avg_inventory_1m}), 0), 0) ;;
  }

  measure: dio_3m {
    type: number
    label: "DIO for 3 month"
    description: "Days Inventory Outstanding for 3 month period back"
    hidden: no
    value_format: "#.0;(#.0)"
    sql:  90 /
              NULLIF(sum(${cogs_last90_value_sum}) /
                            NULLIF(sum(${avg_inventory_3m}), 0), 0) ;;
  }

  measure: dio_6m {
    type: number
    label: "DIO for 6 month"
    description: "Days Inventory Outstanding for 6 month period back"
    hidden: no
    value_format: "#.0;(#.0)"
    sql:  182 /
              NULLIF(sum(${cogs_last182_value_sum}) /
                            NULLIF(sum(${avg_inventory_6m}), 0), 0) ;;
  }

  measure: dio_12m {
    type: number
    label: "DIO for 12 month"
    description: "Days Inventory Outstanding for 12 month period back"
    hidden: no
    value_format: "#.0;(#.0)"
    sql:  365 /
              NULLIF(sum(${cogs_last365_value_sum}) /
                            NULLIF(sum(${avg_inventory_12m}), 0), 0) ;;
  }
}
