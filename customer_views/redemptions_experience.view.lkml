view: redemptions_experience {
  sql_table_name: `chb-prod-data-cust.prod_MUSE_ControlTower.redemptions_experience`
    ;;


  dimension: bit_reference {
    type: string
    sql: ${TABLE}.bit_reference ;;
  }

  dimension: cost_price {
    type: number
    sql: ${TABLE}.cost_price ;;
  }

  dimension: experience_category {
    type: string
    sql: ${TABLE}.experience_category ;;
  }

  dimension: experience_name {
    type: string
    sql: ${TABLE}.experience_name ;;
  }

  dimension: experience_type {
    type: string
    sql: ${TABLE}.experience_type ;;
  }

  dimension: location_code {
    type: string
    sql: ${TABLE}.location_code ;;
  }

  dimension: location_name {
    type: string
    sql: ${TABLE}.location_name ;;
  }

  dimension: market {
    type: string
    sql: ${TABLE}.market ;;
  }

  dimension: member_id {
    type: number
    sql: ${TABLE}.member_id ;;
  }

  dimension: points_burned {
    type: number
    sql: ${TABLE}.points_burned ;;
  }

  dimension_group: redemption {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: DATE(${TABLE}.redemption_date) ;;
  }

  dimension: retail_price {
    type: number
    sql: ${TABLE}.retail_price ;;
  }

  dimension: sponsor_name {
    type: string
    sql: ${TABLE}.sponsor_name ;;
  }


  measure: experiences_count {
    description: "Count of experiences redeemed"
    type: count_distinct
    sql: ${bit_reference};;
  }

  measure: experiences_members  {
    description: "Count of members who redeemed experiences"
    type: count_distinct
    sql: ${member_id} ;;
  }
}
