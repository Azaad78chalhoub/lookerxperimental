include: "../_common/period_over_period_flexible.view"





###Segmentation by Nikita
#creating a native derived table to use the Category visits for creating Category Flags

view: segments_ndt {
  label: "MUSE Transactions Table"
  derived_table: {
    explore_source: muse_control_tower_gravty
    {
      column: member_id {field: muse_gravty.member_id}
      column: beauty_visits {field: muse_gravty.beauty_visits}
      column: fashion_visits {field: muse_gravty.fashion_visits}
      column: lifestyle_visits {field: muse_gravty.lifestyle_visits}
      column: count_of_visits {field: muse_gravty.count_of_visits}
      column: Shopper_type_2020end {field:segments_ndt1.Shopper_type_2020end}
      column: count_of_2020visits {field: muse_gravty.count_of_2020visits}
      column: count_of_2019visits {field: muse_gravty.count_of_2019visits}
      column: count_of_visits_2020end {field: muse_gravty.count_of_visits_2020end}
      # bind_filters: {from_field: muse_gravty.transaction_date_date
      #   to_field: muse_gravty.transaction_date_date}
      # bind_filters: {from_field: muse_gravty.transaction_date_month
      #   to_field: muse_gravty.transaction_date_month}
      # bind_filters: {from_field: muse_gravty.transaction_date_quarter
      #   to_field: muse_gravty.transaction_date_quarter}
      # bind_filters: {from_field: muse_gravty.transaction_date_year
      #   to_field: muse_gravty.transaction_date_year}
      # bind_filters: {from_field: muse_gravty.transaction_date_week
      #   to_field: muse_gravty.transaction_date_week}

      bind_filters: {from_field: muse_calendar.transaction_date_date
        to_field: muse_calendar.transaction_date_date}
      bind_filters: {from_field: muse_calendar.transaction_date_month
        to_field: muse_calendar.transaction_date_month}
      bind_filters: {from_field: muse_calendar.transaction_date_quarter
        to_field: muse_calendar.transaction_date_quarter}
      bind_filters: {from_field: muse_calendar.transaction_date_year
        to_field: muse_calendar.transaction_date_year}
      bind_filters: {from_field: muse_calendar.transaction_date_week
        to_field: muse_calendar.transaction_date_week}



      bind_filters: {from_field: muse_gravty.last_gravty_txn_date
        to_field: muse_gravty.last_gravty_txn_date}
      bind_filters: {from_field: locations.district_name
        to_field: locations.district_name}

      bind_filters: {from_field: muse_gravty.transacting_sponsor_name
        to_field: muse_gravty.transacting_sponsor_name}
      bind_filters: {from_field: muse_gravty.transacting_sponsor_name_modified
        to_field: muse_gravty.transacting_sponsor_name_modified}
      bind_filters: {from_field: muse_gravty.transacting_market
        to_field: muse_gravty.transacting_market}





    }
  }
  dimension: member_id {
    hidden: yes
    primary_key: yes
}

  dimension: beauty_visits {
    hidden: yes
  }

  dimension: fashion_visits {
    hidden: yes
  }
  dimension: lifestyle_visits {
    hidden: yes

  }

  dimension: count_of_visits {
    hidden: yes

  }

  dimension: Days_post_last_purchase {
    hidden: yes

  }

  dimension: Shopper_type_2020end  {
    hidden: yes

}


  dimension: count_of_2020visits  {
    hidden: yes

  }

  dimension:count_of_2019visits{
    hidden: yes

  }

  dimension:count_of_visits_2020end{
    hidden: yes

  }




  dimension: Shopper_type{
    view_label: "MUSE Segmentation Map - Vertical"
    description: "Flagged based on purchasing Vertical"
    label : "Shopper Type - category"
    hidden: no
    type: string
    #filters: [bit_type: "SPEND"]
    sql: case when   ${beauty_visits} != 0 and ${fashion_visits} = 0 and ${lifestyle_visits} = 0
                  then 'Beauty Only Shopper'
              when   ${beauty_visits} = 0 and ${fashion_visits} != 0 and ${lifestyle_visits} = 0
                  then 'Fashion Only Shopper'
              when   ${beauty_visits} = 0 and ${fashion_visits} = 0 and ${lifestyle_visits} != 0
                  then 'Lifestyle Only Shopper'
              when   ${beauty_visits} = 0 and ${fashion_visits} = 0 and ${lifestyle_visits} = 0
                  then "NA"
              else 'Cross Category Shopper'
            end ;;
  }

  dimension: MUSE_Member_Type{
    view_label: "MUSE Segmentation Map - Vertical"
    description: "MUSE Segmentation Map - Vertical Flag"
    label : "Muse Member Type"
    hidden: no
    type: string
    #filters: [bit_type: "SPEND"]
    sql:     case when   ${Shopper_type} = 'Beauty Only Shopper' and
    ${count_of_visits} = 1 then "A. Beauty only Sole Shopper"

     when   ${Shopper_type} = 'Beauty Only Shopper' and
    ${count_of_visits} = 2 then "B. Beauty only Repeat Shopper"

     when   ${Shopper_type} = 'Beauty Only Shopper' and
    ${count_of_visits} >= 3 and ${count_of_visits} <=5 then "C. Beauty only Regulars"

    when   ${Shopper_type} = 'Beauty Only Shopper' and
    ${count_of_visits} > 5 then "D. Beauty only SuperFans"

    when   ${Shopper_type} = 'Fashion Only Shopper' and
    ${count_of_visits} = 1 then "E. Fashion only Sole Shopper"

     when   ${Shopper_type} = 'Fashion Only Shopper' and
    ${count_of_visits} = 2 then "F. Fashion only Repeat Shopper"

     when   ${Shopper_type} = 'Fashion Only Shopper' and
    ${count_of_visits} >= 3 and ${count_of_visits} <=5 then "G. Fashion only Regulars"

    when   ${Shopper_type} = 'Fashion Only Shopper' and
    ${count_of_visits} > 5 then "H. Fashion only SuperFans"

    when   ${Shopper_type} = 'Lifestyle Only Shopper' and
    ${count_of_visits} = 1 then "I. Lifestyle only Sole Shopper"

     when   ${Shopper_type} = 'Lifestyle Only Shopper' and
    ${count_of_visits} = 2 then "J. Lifestyle only Repeat Shopper"

     when   ${Shopper_type} = 'Lifestyle Only Shopper' and
    ${count_of_visits} >= 3 and ${count_of_visits} <=5 then "K. Lifestyle only Regulars"

    when   ${Shopper_type} = 'Lifestyle Only Shopper' and
    ${count_of_visits} > 5 then "L. Lifestyle only SuperFans"

        when   ${Shopper_type} = 'Cross Category Shopper' and
    ${count_of_visits} = 1 then "M. Cross Category only Sole Shopper"

     when   ${Shopper_type} = 'Cross Category Shopper' and
    ${count_of_visits} = 2 then "N. Cross Category only Repeat Shopper"

     when   ${Shopper_type} = 'Cross Category Shopper' and
    ${count_of_visits} >= 3 and ${count_of_visits} <=5 then "O. Cross Category only Regulars"

    when   ${Shopper_type} = 'Cross Category Shopper' and
    ${count_of_visits} > 5 then "P. Cross Category only SuperFans"
    else 'Q. NA'
            end ;;
  }


  dimension: MUSE_Member_Type1{
    view_label: "MUSE Segmentation Map - Vertical"
    description: "MUSE Segmentation Map - Vertical Flag - combining the Cross Category Visitor types"
    label : "Combined Muse Member Type "
    hidden: no
    type: string
    #filters: [bit_type: "SPEND"]
    sql:     case when   ${Shopper_type} = 'Beauty Only Shopper' and
          ${count_of_visits} = 1 then "A. Beauty only Sole Shopper"

           when   ${Shopper_type} = 'Beauty Only Shopper' and
          ${count_of_visits} = 2 then "B. Beauty only Repeat Shopper"

           when   ${Shopper_type} = 'Beauty Only Shopper' and
          ${count_of_visits} >= 3 and ${count_of_visits} <=5 then "C. Beauty only Regulars"

          when   ${Shopper_type} = 'Beauty Only Shopper' and
          ${count_of_visits} > 5 then "D. Beauty only SuperFans"

          when   ${Shopper_type} = 'Fashion Only Shopper' and
          ${count_of_visits} = 1 then "E. Fashion only Sole Shopper"

           when   ${Shopper_type} = 'Fashion Only Shopper' and
          ${count_of_visits} = 2 then "F. Fashion only Repeat Shopper"

           when   ${Shopper_type} = 'Fashion Only Shopper' and
          ${count_of_visits} >= 3 and ${count_of_visits} <=5 then "G. Fashion only Regulars"

          when   ${Shopper_type} = 'Fashion Only Shopper' and
          ${count_of_visits} > 5 then "H. Fashion only SuperFans"

          when   ${Shopper_type} = 'Lifestyle Only Shopper' and
          ${count_of_visits} = 1 then "I. Lifestyle only Sole Shopper"

           when   ${Shopper_type} = 'Lifestyle Only Shopper' and
          ${count_of_visits} = 2 then "J. Lifestyle only Repeat Shopper"

           when   ${Shopper_type} = 'Lifestyle Only Shopper' and
          ${count_of_visits} >= 3 and ${count_of_visits} <=5 then "K. Lifestyle only Regulars"

          when   ${Shopper_type} = 'Lifestyle Only Shopper' and
          ${count_of_visits} > 5 then "L. Lifestyle only SuperFans"

              when   ${Shopper_type} = 'Cross Category Shopper'
           then "M. Cross Category Shopper"

          else 'Q. NA'
                  end ;;
  }


  dimension: MUSE_Segmentation_Movement_type{
    view_label: "MUSE Segmentation Map - Vertical"
    description: "MUSE Segmentation Map - checking the Movement is accross the category or within"
    label : "Flag to track the Member Movement types"
    hidden: no
    type: string
    #filters: [bit_type: "SPEND"]
    sql:case when   ${Shopper_type} = ${Shopper_type_2020end} and ${count_of_visits} > ${count_of_visits_2020end}
           then 'Moved up the Visit Ladder'

         when   ${Shopper_type} = ${Shopper_type_2020end} and ${count_of_visits} = ${count_of_visits_2020end}
          then 'Non Spender in 2021'

     when   ${Shopper_type} <> ${Shopper_type_2020end} and ${count_of_visits} > ${count_of_visits_2020end}
        then 'Moved accross Categories'

              else 'NA'
      end;;

      }




  # #Required dimensions for QBR?Brand action Plans

  dimension: Muse_Visitor_type_Refined{
    view_label: "MUSE Segmentation Map - Vertical"
    description: "Muse Visitor type Refined"
    label : "Visitor Types for Brand Action Plans"
    hidden: no
    type: string
    sql: case when ${count_of_visits}  = 1  then 'A. Sole Shoppers'
              when ${count_of_visits} = 2  then 'B.  Repeat Shoppers'
              when ${count_of_visits} >= 3 and ${count_of_visits} <=5 then 'C. 3-5 Visitors/ Regulars'
              when ${count_of_visits} > 5 then 'D. 5 + Visitors/ Superfans'
              else 'E. Non Spender'
            end ;;
  }


  dimension: Muse_Visitor_type_Refined_temp{
    view_label: "MUSE Segmentation Map - Vertical"
    description: "Muse Visitor type Refined - temp"
    label : "Visitor Types for Brand Action Plans temp"
    hidden: no
    type: string
    sql: case when ${count_of_visits}  = 1  then 'A. Sole Shoppers'
              when ${count_of_visits}  = 2  then 'B.  Repeat Shoppers'
              when ${count_of_visits} >= 3 and ${count_of_visits} <=5 then 'C. 3-5 Visitors/ Regulars'
              --when ${count_of_visits} >  20 then 'H. 20+ Visitors/ Superfans'
              when ${count_of_visits} >  15 then 'G. 16+ Visitors/ Superfans'
              when ${count_of_visits} >  10 then 'F. 11-15 Visitors/ Superfans'
              when ${count_of_visits} >  7 then  'E. 8-10 Visitors/ Superfans'
              when ${count_of_visits} >  5 then 'D. 5-7 Visitors/ Superfans'
              else 'I. Non Spender'
            end ;;
  }

}

###Static Segmentation Flags for all MUSE Members until 2020 end

view: segments_ndt1 {
  label: "MUSE Segmentation Map - Vertical"
  derived_table: {
    explore_source: muse_control_tower_gravty
    {
      column: member_id {field: muse_gravty.member_id}
      column: beauty_visits_2020end {field: muse_gravty.beauty_visits_2020end}
      column: fashion_visits_2020end {field: muse_gravty.fashion_visits_2020end}
      column: lifestyle_visits_2020end {field: muse_gravty.lifestyle_visits_2020end}
      column: count_of_visits_2020end {field: muse_gravty.count_of_visits_2020end}
      column: first_registration_date {field: muse_gravty.first_registration_date}
      column: first_registration_date {field: muse_gravty.first_registration_date}
      column: count_of_2019visits {field: muse_gravty.count_of_2019visits}
      column: count_of_2020visits {field: muse_gravty.count_of_2020visits}

      bind_filters: {from_field: muse_gravty.transacting_market
        to_field: muse_gravty.transacting_market}

      bind_filters: {from_field: locations.district_name
        to_field: locations.district_name}

      bind_filters: {from_field: muse_gravty.transacting_sponsor_name
        to_field: muse_gravty.transacting_sponsor_name}
      bind_filters: {from_field: muse_gravty.transacting_sponsor_name_modified
        to_field: muse_gravty.transacting_sponsor_name_modified}
      bind_filters: {from_field: muse_gravty.transacting_market
        to_field: muse_gravty.transacting_market}

    }
  }
  dimension: member_id {
    hidden: yes
    primary_key: yes
  }

  dimension:  first_registration_date{}

  dimension: beauty_visits_2020end {
    hidden: yes
  }

  dimension: fashion_visits_2020end {
    hidden: yes
  }
  dimension: lifestyle_visits_2020end {
    hidden: yes

  }

  dimension: muse_registration_date {
    hidden: yes
  }

  dimension: count_of_visits_2020end{
    hidden: yes
  }
  dimension: count_of_2020visits{
    hidden: yes
  }
  dimension: count_of_2019visits{
    hidden: yes
  }







  dimension: Muse_Visitor_type_2020end{
    view_label: "MUSE Segmentation Map - Vertical"
    description: "visits grouping until 2020 end "
    label : "Muse Member Type until 2020 end "
    hidden: no
    type: string
    sql: case when ${first_registration_date} > "2020-12-31" then 'A. 2021 Enrolled Member'

          when ${first_registration_date} <= "2020-12-31" and
              ${count_of_visits_2020end} = 1  then 'B. Sole Shoppers'

          when ${first_registration_date} <= "2020-12-31" and
              ${count_of_visits_2020end} = 2  then 'C. Repeat Shoppers'
          when ${first_registration_date} <= "2020-12-31" and
              ${count_of_visits_2020end} >= 3 and
              ${count_of_visits_2020end} <= 5
              then 'D. 3 - 5 Visitors/ Regulars'
          when  ${first_registration_date} <= "2020-12-31" and
                    ${count_of_visits_2020end} > 5 then 'E. 5+ Visitors/ Superfans'
                    else 'F. Non Spender'
                  end ;;
  }


  dimension: Shopper_type_2020end{
    view_label: "MUSE Segmentation Map - Vertical"
    description: "Flagged based on purchasing Vertical until 2020 end"
    label : "Shopper Type until 2020 end"
    hidden: no
    type: string
    #filters: [bit_type: "SPEND"]
    sql: case when   ${beauty_visits_2020end} != 0 and ${fashion_visits_2020end} = 0 and ${lifestyle_visits_2020end} = 0
                  then 'Beauty Only Shopper'
              when   ${beauty_visits_2020end} = 0 and ${fashion_visits_2020end} != 0 and ${lifestyle_visits_2020end} = 0
                  then 'Fashion Only Shopper'
              when   ${beauty_visits_2020end} = 0 and ${fashion_visits_2020end} = 0 and ${lifestyle_visits_2020end} != 0
                  then 'Lifestyle Only Shopper'
              when   ${beauty_visits_2020end} = 0 and ${fashion_visits_2020end} = 0 and ${lifestyle_visits_2020end} = 0
                  then "NA"
              else 'Cross Category Shopper'
            end ;;
  }

   dimension: MUSE_Member_Type_2020end{
    view_label: "MUSE Segmentation Map - Vertical"
    description: "MUSE Segmentation Map - Vertical Flag until 2020 end"
    label : "Muse Member Type until 2020 end"
    hidden: no
    type: string
    #filters: [bit_type: "SPEND"]
    sql:     case when   ${Shopper_type_2020end} = 'Beauty Only Shopper' and
          ${count_of_visits_2020end} = 1 then "A. Beauty only Sole Shopper"

           when   ${Shopper_type_2020end} = 'Beauty Only Shopper' and
          ${count_of_visits_2020end} = 2 then "B. Beauty only Repeat Shopper"

           when   ${Shopper_type_2020end} = 'Beauty Only Shopper' and
          ${count_of_visits_2020end} >= 3 and ${count_of_visits_2020end} <=5 then "C. Beauty only Regulars"

          when   ${Shopper_type_2020end} = 'Beauty Only Shopper' and
          ${count_of_visits_2020end} > 5 then "D. Beauty only SuperFans"

          when   ${Shopper_type_2020end} = 'Fashion Only Shopper' and
          ${count_of_visits_2020end} = 1 then "E. Fashion only Sole Shopper"

           when   ${Shopper_type_2020end} = 'Fashion Only Shopper' and
          ${count_of_visits_2020end} = 2 then "F. Fashion only Repeat Shopper"

           when   ${Shopper_type_2020end} = 'Fashion Only Shopper' and
          ${count_of_visits_2020end} >= 3 and ${count_of_visits_2020end} <=5 then "G. Fashion only Regulars"

          when   ${Shopper_type_2020end} = 'Fashion Only Shopper' and
          ${count_of_visits_2020end} > 5 then "H. Fashion only SuperFans"

          when   ${Shopper_type_2020end} = 'Lifestyle Only Shopper' and
          ${count_of_visits_2020end} = 1 then "I. Lifestyle only Sole Shopper"

           when   ${Shopper_type_2020end} = 'Lifestyle Only Shopper' and
          ${count_of_visits_2020end} = 2 then "J. Lifestyle only Repeat Shopper"

           when   ${Shopper_type_2020end} = 'Lifestyle Only Shopper' and
          ${count_of_visits_2020end} >= 3 and ${count_of_visits_2020end} <=5 then "K. Lifestyle only Regulars"

          when   ${Shopper_type_2020end} = 'Lifestyle Only Shopper' and
          ${count_of_visits_2020end} > 5 then "L. Lifestyle only SuperFans"

              when   ${Shopper_type_2020end} = 'Cross Category Shopper' and
          ${count_of_visits_2020end} = 1 then "M. Cross Category only Sole Shopper"

           when   ${Shopper_type_2020end} = 'Cross Category Shopper' and
          ${count_of_visits_2020end} = 2 then "N. Cross Category only Repeat Shopper"

           when   ${Shopper_type_2020end} = 'Cross Category Shopper' and
          ${count_of_visits_2020end} >= 3 and ${count_of_visits_2020end} <=5 then "O. Cross Category only Regulars"

          when   ${Shopper_type_2020end} = 'Cross Category Shopper' and
          ${count_of_visits_2020end} > 5 then "P. Cross Category only SuperFans"
          else 'Q. NA'
                  end ;;
  }

#identifying the lapsed newbies and active pool

  dimension: Muse_Visitor_type_2020end_updated{
    view_label: "MUSE Segmentation Map - Vertical"
    description: "visits grouping until 2020 end for active pool"
    label : "Muse Member Type until 2020 end for active pool"
    hidden: no
    type: string
    sql: case when ${first_registration_date} > "2020-12-31" then 'A. 2021 Enrolled Member/ Newbies'
              when ${first_registration_date} < "2020-01-01"  and ${count_of_2019visits} > 0 and ${count_of_2020visits} <=0
                          then 'F. Lapsers'

                when ${count_of_visits_2020end} = 1  then 'B. Sole Shoppers'

                when    ${count_of_visits_2020end} = 2  then 'C. Repeat Shoppers'
                when ${count_of_visits_2020end} >= 3 and
                    ${count_of_visits_2020end} <= 5   then 'D. 3-5 Visitors'
                when   ${count_of_visits_2020end} > 5 then 'E. 5+ Visitors'

                          else 'G. Non Spender'
                        end ;;
  }

  dimension: Muse_Visitor_type_2020end_yearly{
    view_label: "MUSE Segmentation Map - Vertical"
    description: "visits grouping until 2020 end for all 3 pools"
    label : "Muse Member Type until 2020 end for all the pools"
    hidden: no
    type: string
    sql: case when ${first_registration_date} > "2020-12-31" then 'A. 2021 Enrolled Member/ Newbies'
              when ${first_registration_date} < "2020-01-01"  and ${count_of_2019visits} > 0 and ${count_of_2020visits} <=0
                          then 'C. Lapsers'

                when  ${first_registration_date} < "2021-01-01"   then 'B. 2020 Active Memberbase'
                          else 'G. Non Spender'
                        end ;;
  }


  }








#########################



view: muse_gravty {

  sql_table_name: `chb-prod-data-cust.prod_MUSE_ControlTower.prod_gravty_transactions` ;;

  dimension: pk {
    label: "pk"
    hidden: yes
    primary_key: yes
    type: string
    sql: ${TABLE}.bit_reference;;
  }

  dimension:  first_name
  { view_label: "MUSE Member Table"
    group_label: "Personal Information"
    type: string
    sql: ${TABLE}.first_name;;
  }

  dimension:  last_name
  { view_label: "MUSE Member Table"
    group_label: "Personal Information"
    type: string
    sql: ${TABLE}.last_name;;
  }

  dimension: full_name{
 view_label: "MUSE Member Table"
    group_label: "Personal Information"
    type:string
    sql:concat(${first_name}, ' ', ${last_name});;
  }


  dimension: member_id {
    label: "Muse ID"
    view_label: "MUSE Member Table"
    group_label: "Muse Dimensions"
    hidden: no
    type: number
    sql: ${TABLE}.member_id ;;
  }

  dimension: vertical {
    hidden: yes
    type: string
    sql: ${TABLE}.vertical ;;
  }

#redundant code - created by Patrick
  # dimension: app_attribution_id {
  #   hidden: no
  #   type: string
  #   sql: ${TABLE}.app_attribution_id ;;
  # }

  # dimension: app_attribution_first_name {
  #   hidden: no
  #   type: string
  #   sql: ${TABLE}.app_attribution_first_name ;;
  # }

  # dimension: app_attribution_last_name {
  #   hidden: no
  #   type: string
  #   sql: ${TABLE}.app_attribution_last_name ;;
  # }

  # dimension: app_attribution_full_name {
  #   hidden: no
  #   type: string
  #   sql: ${TABLE}.app_attribution_full_name ;;
  # }

  # dimension: app_attribution_working_place {
  #   hidden: no
  #   type: string
  #   sql: ${TABLE}.app_attribution_working_place ;;
  # }

  dimension: crm_flag {
    label: "CRM flag"
    hidden: no
    type: string
    sql: CASE WHEN ${TABLE}.crm_flag = 1 THEN 'Yes' ELSE 'No' END ;;
  }

  dimension: crm_registration_date {
    label: "CRM registration date"
    hidden: no
    type: date
    sql: CAST(CAST(${TABLE}.crm_registration_date AS DATE) AS TIMESTAMP) ;;
  }

  dimension: app_attribution_staff_id{
    hidden: no
    type: string
    group_label: "App Attribution"
    sql: app_attribution_staff_id ;;
  }

  dimension: app_attribution_staff_full_name{
    hidden: no
    type: string
    group_label: "App Attribution"
    sql: app_attribution_staff_full_name ;;
  }

  dimension: app_attribution_brand{
    hidden: no
    type: string
    group_label: "App Attribution"
    sql: app_attribution_brand ;;
  }

  dimension: app_attribution_mall_name{
    hidden: no
    type: string
    group_label: "App Attribution"
    sql: app_attribution_mall_name ;;
  }

  dimension: app_attribution_store_code{
    hidden: no
    type: string
    group_label: "App Attribution"
    sql: app_attribution_store_code ;;
  }

  dimension: app_attribution_market{
    hidden: no
    type: string
    group_label: "App Attribution"
    sql: app_attribution_market ;;
  }

  dimension: bit_type {
    hidden: no
    label: "Bit Type"
    view_label: "MUSE Transactions Table"
    group_label: "Transaction Dimensions"
    type: string
    sql: ${TABLE}.bit_type ;;
  }

  dimension: bit_category {
    hidden: no
    label: "Bit Category"
    view_label: "MUSE Transactions Table"
    group_label: "Transaction Dimensions"
    type: string
    sql: ${TABLE}.bit_category  ;;
  }


  dimension: bit_reference {
    hidden: no
    label: "Bit Reference"
    view_label: "MUSE Transactions Table"
    group_label: "Transaction Dimensions"
    type: string
    sql: ${TABLE}.bit_reference ;;
  }

  measure: count_bit_reference {
    hidden: no
    label: "Disitnct Count of Bit Reference"
    view_label: "MUSE Transactions Table"
    type: count_distinct
    sql: ${bit_reference} ;;
  }


dimension: earned {
    view_label: "MUSE Transactions Table"
    hidden: yes
    label: "Earned"
    type: number
    sql: CAST(${TABLE}.earned AS NUMERIC);;
  }

  dimension: burned{
    view_label: "MUSE Transactions Table"
    hidden: yes
    label: "Burned"
    type: number
    sql: CAST(${TABLE}.burned AS NUMERIC);;
  }


  dimension: base_points{
    view_label: "MUSE Transactions Table"
    description: "Base Points issued"
    hidden: yes
    label: "Base Points"
    type: number
    sql: CAST(${TABLE}.base_points AS NUMERIC);;
  }

  dimension: uuid {
    view_label: "MUSE Member Table"
    hidden: yes
    label: "Member UUID"
    type: string
    sql: ${TABLE}.uuid;;
  }


  measure: sum_base_points{
    label: "Base Points (SUM)"
    view_label: "MUSE Transactions Table"
    description: "Sum of base points issued"
    hidden: no
    type: sum
    sql: ${base_points};;
    value_format: "#,##0"
  }



  dimension: bonus_points{
    view_label: "MUSE Transactions Table"
    description: "Bonus Points issued"
    hidden: yes
    label: "Bonus Points"
    type: number
    sql: CAST(${TABLE}.bonus_points AS NUMERIC);;

  }

  measure: sum_bonus {
    type: sum
    label: "Bonus Points (SUM)"
    description: "Sum of bonus points issued"
    view_label: "MUSE Transactions Table"
    sql: ${bonus_points} ;;
    value_format: "#,##0"
  }

  measure: sum_earned {
    type: sum
    label: "Earned Points (SUM)"
    view_label: "MUSE Transactions Table"
    description: "Points issued"
    sql: ${earned} ;;
    value_format: "#,##0"
  }

  measure: sum_burned {
    type: sum
    label: "Burned Points (SUM)"
    view_label: "MUSE Transactions Table"
    description: "Points redeemed"
    sql: ${burned} ;;
    value_format: "#,##0"
  }

#############Pratiksha's stats - NOT TO BE USED

 # measure: burned_points_value_local {
    #view_label: "MUSE Transactions Table"
   # type: number
  #  sql: CASE WHEN ${transacting_market} = "Kuwait" THEN ${sum_burned}/1200
   #      ELSE ${sum_burned}/100
   #      END ;;
   # value_format: "#,##0.00"
 # }

 # measure: burned_points_value_usd {
  #  view_label: "MUSE Transactions Table"
   # label: "Burned Points Value - USD"
  #  type: number
  #  sql: CASE WHEN ${transacting_market} = "Kuwait" THEN ${burned_points_value_local}*3.25
     #         WHEN ${transacting_market} = "United Arab Emirates" THEN ${burned_points_value_local}/3.67
     #         WHEN ${transacting_market} = "Saudi Arabia" THEN ${burned_points_value_local}/3.75
      #   ELSE ${burned_points_value_local}
    #     END ;;
  #  value_format: "#,##0.00"
#  }

  ##measure: topup_spend_local {
    ##view_label: "MUSE Transactions Table"
    ##type: number
    #sql: ${sum_sales_with_vat}-${burned_points_value_local} ;;
    #value_format: "#,##0.00"
  #}

  #measure: topup_spend_usd {
   # view_label: "MUSE Transactions Table"
    #label: "Topup Spend - USD"
    #type: number
    #sql: ${sum_sales_with_vat_usd}-${burned_points_value_usd} ;;
    #value_format: "#,##0.00"
  #}

#####################



  dimension: enrolling_sponsor_name {
    label: "Enrolling Sponsor Name"
    group_label: "Enrolling Dimensions"
  view_label: "MUSE Member Table"
    hidden: no
    type: string
    sql: ${TABLE}.sponsor_name ;;
  }

  dimension: enrolling_sponsor_name_modified {
    label: "Enrolling Sponsor Name (RMS)"
    group_label: "Enrolling Dimensions"
    view_label: "MUSE Member Table"
    description: "Dimension to get ORACLE/RMS district name mapping"
    hidden: no
    type: string
    sql: case when ${enrolling_sponsor_name}='DOLCE & GABBANA' then 'DOLCE&GABBANA'
        when ${enrolling_sponsor_name}='WEEKEND MAX MARA' then 'MAX MARA'
        when ${enrolling_sponsor_name}='KILIAN PARIS' then 'BY KILIAN'
        when ${enrolling_sponsor_name}='HUGO BOSS KIDS' then 'HUGO BOSS'
        when ${enrolling_sponsor_name}='STELLA MCCARTNEY' then 'STELLA MC CARTNEY'
        when ${enrolling_sponsor_name}='b8ta' then 'B8ta'
        else ${enrolling_sponsor_name}
        end;;
  }


  dimension: transacting_sponsor_name {
    label: "Transacting Sponsor Name"
    view_label: "MUSE Transactions Table"
    group_label: "Transaction Dimensions"
    hidden: no
    type: string
    sql: ${TABLE}.sponsor ;;
  }

  dimension:  transacting_sponsor_name_modified {
    label: "Transacting Sponsor Name (RMS)"
    view_label: "MUSE Transactions Table"
    description: "Dimension to get ORACLE/RMS district name mapping"
    group_label: "Transaction Dimensions"
    hidden: no
    type: string
    sql: case when ${transacting_sponsor_name}='DOLCE & GABBANA' then 'DOLCE&GABBANA'
        when ${transacting_sponsor_name}='WEEKEND MAX MARA' then 'MAX MARA'
        when ${transacting_sponsor_name}='KILIAN PARIS' then 'BY KILIAN'
        when ${transacting_sponsor_name}='HUGO BOSS KIDS' then 'HUGO BOSS'
        when ${transacting_sponsor_name}='STELLA MCCARTNEY' then 'STELLA MC CARTNEY'
        when ${transacting_sponsor_name}='b8ta' then 'B8ta'
        else ${transacting_sponsor_name}
        end;;
  }

#####boat/vertical/business unit - AQB HSN

  dimension: txn_boat {
    label: "Boat/Vertical (transacting)"
    group_label: "Transaction Dimensions"
    view_label: "MUSE Transactions Table"
    description: "Boat/Brand Vertical mapping"
    hidden: no
    type: string
    sql: case when ${TABLE}.sponsor  IN ("BOBBI BROWN","FREDERIC MALLE", "GIORGIO ARMANI", "KILIAN PARIS", "NARS", "YSL") then "Beauty 1"
        when ${TABLE}.sponsor  IN ("ATELIER COLOGNE","MOLTON BROWN","PENHALIGON'S") then "Beauty 2"
        when ${TABLE}.sponsor  IN ("FACES") then "FACES"
        when ${TABLE}.sponsor  IN ("HUGO BOSS KIDS") then "HUGO BOSS"
        when ${TABLE}.sponsor  IN ("COURCELLES","DOLCE & GABBANA","DSQUARED2","IL GUFO","KARL LAGERFELD","KENZO","MARINA RINALDI","MAX MARA","WEEKEND MAX MARA","MICHAEL KORS","PAUL SMITH","RENE CAOVILLA","SALVATORE FERRAGAMO","STELLA MCCARTNEY","TORY BURCH","TUMI","ZADIG & VOLTAIRE") then "FF1"
        when ${TABLE}.sponsor  IN ("FILA","LACOSTE","SWAROVSKI") then "FF2"
        when ${TABLE}.sponsor  IN ("LEVEL SHOES") then "LEVEL"
        when ${TABLE}.sponsor  IN ("LOCCITANE") then "LOCCITANE"
        when ${TABLE}.sponsor  IN ("BACCARAT","CHRISTOFLE","ST DUPONT","TANAGRA") then "TANAGRA"
        when ${TABLE}.sponsor  IN ("TRYANO") then "TRYANO"
        else ${TABLE}.sponsor
        end;;
  }

  dimension: enrl_boat {
    label: "Boat/Vertical (enrolling)"
    group_label: "Enrolling Dimensions"
    view_label: "MUSE Member Table"
    description: "Boat/Brand Vertical mapping"
    hidden: no
    type: string
    sql: case when ${TABLE}.sponsor_name IN ("BOBBI BROWN","FREDERIC MALLE", "GIORGIO ARMANI", "KILIAN PARIS", "NARS", "YSL") then "Beauty 1"
        when ${TABLE}.sponsor_name IN ("ATELIER COLOGNE","MOLTON BROWN","PENHALIGON'S") then "Beauty 2"
        when ${TABLE}.sponsor_name IN ("FACES") then "FACES"
        when ${TABLE}.sponsor_name IN ("HUGO BOSS KIDS") then "HUGO BOSS"
        when ${TABLE}.sponsor_name IN ("COURCELLES","DOLCE & GABBANA","DSQUARED2","IL GUFO","KARL LAGERFELD","KENZO","MARINA RINALDI","MAX MARA","WEEKEND MAX MARA","MICHAEL KORS","PAUL SMITH","RENE CAOVILLA","SALVATORE FERRAGAMO","STELLA MCCARTNEY","TORY BURCH","TUMI","ZADIG & VOLTAIRE") then "FF1"
        when ${TABLE}.sponsor_name IN ("FILA","LACOSTE","SWAROVSKI") then "FF2"
        when ${TABLE}.sponsor_name IN ("LEVEL SHOES") then "LEVEL"
        when ${TABLE}.sponsor_name IN ("LOCCITANE") then "LOCCITANE"
        when ${TABLE}.sponsor_name IN ("BACCARAT","CHRISTOFLE","ST DUPONT","TANAGRA") then "TANAGRA"
        when ${TABLE}.sponsor_name IN ("TRYANO") then "TRYANO"
        else ${TABLE}.sponsor_name
        end;;
  }

########################################################

  dimension: brand_logo {
    type: string
    sql: ${transacting_sponsor_name} ;;
    html:
        {% if transacting_sponsor_name._value == "LEVEL SHOES" %}
        <img src="https://www.levelshoes.com/static/version1614077679/frontend/Vaimo/levelshoesm2/en_US/images/logo.svg" height="100" width="200">
        {% elsif transacting_sponsor_name._value == "MUSE" %}
        <img src="https://www.experience-muse.com/assets/img/English-White_Smoke_web.png">
        {% elsif transacting_sponsor_name._value == "LACOSTE" %}
        <img src="https://upload.wikimedia.org/wikipedia/en/4/43/Lacoste_logo.svg" height="100" width="200">
        {% elsif transacting_sponsor_name._value == "SWAROVSKI" %}
        <img src="https://logonoid.com/images/swarovski-logo.jpg" height="100" width="200">
        {% elsif transacting_sponsor_name._value == "LOCCITANE" %}
        <img src="https://1000logos.net/wp-content/uploads/2020/04/LOccitane-Logo.png" height="100" width="200">
        {% elsif transacting_sponsor_name._value == "FACES" %}
        <img src="https://www.faces.com/on/demandware.static/Sites-Faces_AE-Site/-/default/dw1352095f/images/logo.svg" height="100" width="200">
        {% else %}
        <img src="https://icon-library.net/images/no-image-available-icon/no-image-available-icon-6.jpg" height="100" width="200">
        {% endif %} ;;
  }


  dimension: market {
    label: "Enrolling Market"
    group_label: "Enrolling Dimensions"
    view_label: "MUSE Member Table"
    hidden: no
    type: string
    sql: ${TABLE}.market ;;
  }

  dimension: transacting_market {
    label: "Transacting Market"
    view_label: "MUSE Transactions Table"
    group_label: "Transaction Dimensions"
    hidden: no
    type: string
    sql: ${TABLE}.transaction_market ;;
  }

  dimension: currency_code {
    label: "Currency Code"
    view_label: "MUSE Transactions Table"
    group_label: "Transaction Dimensions"
    hidden: no
    type: string
    sql: ${TABLE}.currency_code ;;
  }


  dimension: staff_id {
    label: "Enrolling Staff ID"
    group_label: "Enrolling Dimensions"
    view_label: "MUSE Member Table"
    hidden: no
    type: string
    sql: ${TABLE}.staff_id ;;
  }

  dimension: transaction_staff_id {
    label: "Transacting Staff ID"
    group_label: "Transaction Dimensions"
    view_label: "MUSE Transactions Table"
    hidden: yes
    type: string
    sql: ${TABLE}.h_representative_id ;;
  }



  dimension: membership_stage {
    label: "Membership Stage"
    group_label: "Muse Dimensions"
    view_label: "MUSE Member Table"
    hidden: no
    type: string
    sql: ${TABLE}.membership_stage ;;
  }

  dimension: muse_enrolled_location {
    label: "Enrolling Location"
    group_label: "Enrolling Dimensions"
    view_label: "MUSE Member Table"
    hidden: yes
    type: string
    sql: ${TABLE}.muse_enrolled_location ;;
  }

  dimension: enrolling_location_id {
    label: "Enrolling Location Code"
    group_label: "Enrolling Dimensions"
    view_label: "MUSE Member Table"
    hidden: no
    type: string
    sql: ${TABLE}.enrolling_location_id ;;
  }

  dimension: email {
    view_label: "MUSE Member Table"
    group_label: "Personal Information"
    hidden: no
    type: string
    sql: ${TABLE}.email ;;
  }

  dimension: email_flag {
    view_label: "MUSE Member Table"
    group_label: "Personal Information"
    hidden: no
    type: yesno
    sql: ${email} IS NOT NULL ;;
  }

  dimension: mobile {
    view_label: "MUSE Member Table"
    group_label: "Personal Information"
    hidden: no
    type: string
    sql: ${TABLE}.mobile ;;
  }

  dimension: consent_email {
    label: "Consent Email"
    view_label: "MUSE Member Table"
    group_label: "Personal Information"
    type: string
    sql: ${TABLE}.consent_email ;;
  }

  dimension: store_location_code {
    label: "Transacting Store Location Code"
    view_label: "MUSE Transactions Table"
    group_label: "Transaction Dimensions"
    type: string
    sql: ${TABLE}.store_location_code ;;
  }

  dimension: enrolling_location_id_joiner {
    description: "Used to join to muse calendar"
    hidden: yes
    type: string
    sql: ${TABLE}.enrolling_location_id_joiner ;;
  }

  dimension: consent_push_notification {
    label: "Consent Push Notification"
    view_label: "MUSE Member Table"
    group_label: "Personal Information"
    type: string
    sql: ${TABLE}.consent_push_notification ;;
  }
  dimension: consent_sms {
    label: "Consent SMS"
    view_label: "MUSE Member Table"
    group_label: "Personal Information"
    type: string
    sql: ${TABLE}.consent_sms ;;
  }

  dimension: enrollment_channel {
    label: "Enrolling Channel"
    view_label: "MUSE Member Table"
    group_label: "Enrolling Dimensions"
    type: string
    sql: ${TABLE}.enrollment_channel ;;
  }

  dimension: location_name {
    label: "Enrolling Location Name"
    view_label: "MUSE Member Table"
    group_label: "Enrolling Dimensions"
    type: string
    sql: ${TABLE}.location_name ;;
  }

  dimension: preferred_language {
    label: "Preferred Language"
    view_label: "MUSE Member Table"
    group_label: "Personal Information"
    type: string
    sql: ${TABLE}.preferred_language ;;
  }

  dimension: tier_class {
    label: "Tier Class"
    view_label: "MUSE Member Table"
    group_label: "Muse Dimensions"
    type: string
    sql: ${TABLE}.tier_class ;;
  }

  dimension: staff_first_name {
    label: "Staff First Name"
    view_label: "Enrolling Staff Info"
    type: string
    sql: ${TABLE}.hr_first_name ;;
  }

  dimension: staff_last_name {
    label: "Staff Last Name"
    view_label: "Enrolling Staff Info"
    type: string
    sql: ${TABLE}.hr_last_name ;;
  }

  dimension: hr_name{
    view_label: "Enrolling Staff Info"
    label: "Staff Full Name"
    type:string
    sql:concat(${staff_first_name}, ' ', ${staff_last_name});;
  }



  dimension: store_location {
    label: "Transacting Store Location"
    view_label: "MUSE Transactions Table"
    group_label: "Transaction Dimensions"
    type: string
    sql: ${TABLE}.store_location ;;
  }


     dimension: Store_location_flag {
    label: "Ecommerce/Instore Flag"
      view_label: "MUSE Transactions Table"
      group_label: "Transaction Dimensions"
    type: string
    sql: case when ${TABLE}.store_location like '%ECOM%' then 'ECOMMERCE' else 'INSTORE' end;;
  }



  dimension: app_flag {
    label: "App Flag"
    view_label: "MUSE Member Table"
    group_label: "Muse Dimensions"
    type: number
    sql: ${TABLE}.app_flag ;;
  }

  dimension: invoice_no {
    label: "Invoice No"
    view_label: "MUSE Transactions Table"
    group_label: "Transaction Dimensions"
    type: string
    sql: ${TABLE}.invoice_no ;;
  }




  measure: eligible_spend{
    view_label: "MUSE Transactions Table"
    hidden: no
    label: "Eligible Spend"
    type: sum
    sql: ${TABLE}.eligible_spend;;
    value_format: "#,##0"
  }



  measure: app_downloads {
    label: "App Downloads (COUNT)"
    view_label: "MUSE Member Table"
    type: count_distinct
    sql:case when ${app_flag} = 1 THEN ${member_id}
         ELSE NULL
        END;;
  }

  dimension_group: app_first_login {
    label: "App First Login"
 view_label: "MUSE Member Table"
group_label: "Muse Dimensions"
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.app_first_used_date AS TIMESTAMP) ;;
  }

  dimension_group: muse_registration_date {
    label: "Muse Registration Date"
    view_label: "MUSE Member Table"
    group_label: "Muse Dimensions"
    hidden: no
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year

    ]
    sql: CAST(${TABLE}.date_of_joining AS TIMESTAMP) ;;
  }


  measure: first_registration_date  {
    type: date
    view_label: "MUSE Member Table"
    sql: MIN(${muse_registration_date_raw}) ;;
    convert_tz: no
  }

measure: last_registration_date {
  type: date
  view_label: "MUSE Member Table"
  sql: MAX(${muse_registration_date_raw}) ;;
  convert_tz: no
}

  dimension: gender {
    label: "Gender"
    group_label: "Personal Information"
    view_label: "MUSE Member Table"
    hidden: no
    type: string
    sql: ${TABLE}.gender ;;
  }


  dimension_group: date_of_birth {
    view_label: "MUSE Member Table"
    group_label: "Personal Information"
    type: time
    convert_tz: no
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.date_of_birth ;;
  }


  dimension: age {
    view_label: "MUSE Member Table"
    group_label: "Personal Information"
    hidden: no
    type: number
    sql: date_diff(current_date(),${date_of_birth_date},year)  ;;
  }


  dimension: age_sort {
    hidden: yes
    type: number
    sql: CASE
                         WHEN ${age} < 18 THEN 1
                          WHEN ${age} >= 18 AND  ${age} <= 25 THEN 2
                          WHEN ${age} > 25 AND  ${age} <= 30 THEN 3
                          WHEN ${age} > 30 AND  ${age} <= 35 THEN 4
                          WHEN ${age} > 35 AND  ${age} <= 40 THEN 5
                          WHEN ${age} > 40 AND  ${age} <= 45 THEN 6
                          WHEN ${age} > 45 AND  ${age} <= 55 THEN 7
                          WHEN ${age} > 55 THEN 8
                          ELSE null
                          END;;
  }




  dimension: age_range {
    view_label: "MUSE Member Table"
    group_label: "Personal Information"
    order_by_field: age_sort
    hidden: no
    type: string
    sql: CASE
                          WHEN ${age} < 18 THEN "<18"
                          WHEN ${age} >= 18 AND  ${age} <= 25 THEN "18-25"
                          WHEN ${age} > 25 AND  ${age} <= 30 THEN "26-30"
                          WHEN ${age} > 30 AND  ${age} <= 35 THEN "31-35"
                          WHEN ${age} > 35 AND  ${age} <= 40 THEN "36-40"
                          WHEN ${age} > 40 AND  ${age} <= 45 THEN "41-45"
                          WHEN ${age} > 45 AND  ${age} <= 55 THEN "46-55"
                          WHEN ${age} > 55 THEN ">55"
                          ELSE null
                          END;;
  }



  extends: [period_over_period_flexible]

  dimension_group: pop_no_tz{
    sql: ${transaction_date_date} ;;
  }

  dimension_group: transaction_date {
    view_label: "MUSE Transactions Table"
    group_label: "Transaction Dimensions"
    label: "Transaction Date"
    type: time
    hidden: yes
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.transaction_date ;;
  }


dimension: transaction_date_key {
  hidden: no
  type: number
  sql: ${TABLE}.transaction_date_key;;
}
  dimension: transaction_month_key {
    hidden: yes
    type: number
    sql: ${TABLE}.transaction_month_key;;
  }

dimension: datekey {
  hidden: no
  type: number
  sql: cast(FORMAT_DATE('%Y%m%d', ${transaction_date_date}) as int64)  ;;
}

  dimension: selected_start_date {
    hidden: yes
    type: date
    sql: {% date_start transaction_date_date %} ;;
  }

  dimension: selected_end_date {
    hidden: yes
    type: string
    sql: case when IFNULL({% date_end transaction_date_date %},current_date())>=current_date() then current_date()


                else {% date_end transaction_date_date %} end ;;
  }




  dimension: is_returing_member {
    label: "Is Returning Member"
    view_label: "MUSE Member Table"
    type: yesno
    sql: ${member_id} in
          (select distinct
          member_id
          from `chb-prod-data-cust.prod_MUSE_ControlTower.prod_gravty_transactions`
          where transaction_date<${selected_start_date}
          and bit_type='SPEND')

          ;;
  }


dimension: brand_member_id {
  type:string
  hidden: yes
  sql: concat(${transacting_sponsor_name},cast(${member_id} as string)) ;;

}



  dimension: is_returing_brand_member {
    label: "Is Returning Brand Member"
    view_label: "MUSE Member Table"
    type: yesno
    sql: ${brand_member_id} in
          (select distinct
          concat(sponsor, cast(member_id as string)) as muse_brand_id
          from `chb-prod-data-cust.prod_MUSE_ControlTower.prod_gravty_transactions`
          where transaction_date<${selected_start_date}
          and bit_type='SPEND')

          ;;
  }



measure: first_gravty_txn_date {
  view_label: "MUSE Transactions Table"


  type: date
  sql: min(cast(${TABLE}.transaction_date as timestamp)) ;;
}

  measure: last_gravty_txn_date {
    view_label: "MUSE Transactions Table"


    type: date
    sql: max(cast(${TABLE}.transaction_date as timestamp)) ;;
  }


measure: count_of_visits{
  view_label: "MUSE Transactions Table"
  label: "Visits"
  description: "Unique visits to a mall irrespective of brands shopped"
  type: count_distinct
  filters: [bit_type: "SPEND"]
  sql: concat(cast(${transaction_date_date} as string),cast(${member_id} as string)) ;;

}


  measure: average_visit_frequency{
    view_label: "MUSE Transactions Table"
    label: "Average Visit Frequency (AVF)"
    description: "Based on unique visits"
    value_format: "0.##"
    type: number
    sql: ${count_of_visits}/nullif(${count_customer_transacted},0) ;;

  }



  measure: count_of_brands{
    view_label: "MUSE Transactions Table"
    type: count_distinct
    filters: [bit_type: "SPEND"]
    sql: concat(cast(${transacting_sponsor_name} as string),cast(${member_id} as string)) ;;

  }


  measure: average_brands_adopted{
    view_label: "MUSE Transactions Table"
    label: "Average Brand Frequency (Brands adopted)"
    description: "Based on unique brands"
    value_format: "0.##"
    type: number
    sql:  ${count_of_brands}/nullif(${count_customer_transacted},0);;



  }


  measure: count_of_channels{
    view_label: "MUSE Transactions Table"
    type: count_distinct
    filters: [bit_type: "SPEND"]
    sql: concat(cast(${Store_location_flag} as string),cast(${member_id} as string)) ;;

  }

  dimension: dimension_sales_with_vat  {
    label: "Sales With Vat"
    view_label: "MUSE Transactions Table"
    type: number
    hidden: no
    sql: ${TABLE}.sales_with_vat ;;
  }

  measure: sales_with_vat  {
    label: "Sales With Vat (local)"
    view_label: "MUSE Transactions Table"
    type: sum
    value_format: "#,##0;(#.00)"
    sql: ${dimension_sales_with_vat} ;;
  }


  dimension: dimension_sales_without_tax  {
    label: "Sales Without Vat"
    view_label: "MUSE Transactions Table"
    type: number
    hidden: yes
    sql: ${TABLE}.sales_without_tax ;;
  }


  measure: sales_without_tax  {
    label: "Sales Without Vat (local)"
    view_label: "MUSE Transactions Table"
    type: sum
    value_format: "#,##0;(#.00)"
    sql: ${dimension_sales_without_tax} ;;
  }



  dimension: dimension_spend_local  {
    label: "Spend local"
    view_label: "MUSE Transactions Table"
    type: number
    hidden: yes
    sql: ${TABLE}.spend_local ;;
  }



  measure: spend_local  {
    label: "Spend local"
    view_label: "MUSE Transactions Table"
    type: sum
    value_format: "#,##0;(#.00)"
    sql: ${dimension_spend_local} ;;
  }


  measure: sales_with_vat_usd  {
    label: "Sales With Vat (USD)"
    view_label: "MUSE Transactions Table"
    hidden: no
    type: sum
    value_format: "$#,##0;(#.00)"
    sql: CASE WHEN ${currency_code} = "AED" THEN ${TABLE}.sales_with_vat/3.67
              WHEN ${currency_code} = "KWD" THEN ${TABLE}.sales_with_vat*3.25
              WHEN ${currency_code} = "SAR" THEN ${TABLE}.sales_with_vat/3.75
              ELSE ${TABLE}.sales_with_vat
         END;;
  }


  measure: active_members_sales_with_vat_usd  {
    label: "Sales With Vat (USD) - Active Members"
    view_label: "MUSE Transactions Table"
    hidden: no
    type: sum
    value_format: "$#,##0;(#.00)"
    filters: [membership_stage: "Active"]
    sql: CASE WHEN ${currency_code} = "AED" THEN ${TABLE}.sales_with_vat/3.67
              WHEN ${currency_code} = "KWD" THEN ${TABLE}.sales_with_vat*3.25
              WHEN ${currency_code} = "SAR" THEN ${TABLE}.sales_with_vat/3.75
              ELSE ${TABLE}.sales_with_vat
         END;;
  }




  dimension: spend_bucket_134 {
    label: "Spend Bucket - 134 USD"
    description: "For Ramadan 2021 TLC, dimension to bucket members who have spent >134 USD in each transaction"
    view_label: "MUSE Transactions Table"
    type: tier
    tiers: [134]
    style: integer
    sql: CASE WHEN ${currency_code} = "AED" THEN ${TABLE}.sales_with_vat/3.67
              WHEN ${currency_code} = "KWD" THEN ${TABLE}.sales_with_vat*3.25
              WHEN ${currency_code} = "SAR" THEN ${TABLE}.sales_with_vat/3.75
              ELSE ${TABLE}.sales_with_vat
         END ;;
  }

  dimension: se_spend_buckets_ramadan2021 {
    label: "Spend & Earn Spend Buckets"
    description: "For Ramadan 2021 Spend & Earn"
    view_label: "MUSE Transactions Table"
    type: string
    sql: CASE WHEN ${currency_code} = "AED" AND ${TABLE}.sales_with_vat < 300 AND ${TABLE}.sales_with_vat > 1  THEN "A. < 300 AED"
          WHEN ${currency_code} = "AED" AND (${TABLE}.sales_with_vat >= 300 AND ${TABLE}.sales_with_vat < 700) THEN "B. 300 - 699 AED"
          WHEN ${currency_code} = "AED" AND (${TABLE}.sales_with_vat >= 700 AND ${TABLE}.sales_with_vat < 2000) THEN "C. 700 - 1999 AED"
          WHEN ${currency_code} = "AED" AND ${TABLE}.sales_with_vat >= 2000 THEN "D. >= 2000 AED"
          WHEN ${currency_code} = "KWD" AND ${TABLE}.sales_with_vat < 25 AND ${TABLE}.sales_with_vat > 1 THEN "A. < 25 KWD"
          WHEN ${currency_code} = "KWD" AND (${TABLE}.sales_with_vat >= 25 AND ${TABLE}.sales_with_vat < 50) THEN "B. 25 - 49 KWD"
          WHEN ${currency_code} = "KWD" AND (${TABLE}.sales_with_vat >= 50 AND ${TABLE}.sales_with_vat < 150) THEN "C. 50 - 149 KWD"
          WHEN ${currency_code} = "KWD" AND ${TABLE}.sales_with_vat >= 150 THEN "D. > 150 KWD"
          WHEN ${currency_code} = "SAR" AND ${TABLE}.sales_with_vat < 300 AND ${TABLE}.sales_with_vat >1 THEN "A. < 300 SAR"
          WHEN ${currency_code} = "SAR" AND (${TABLE}.sales_with_vat >= 300 AND ${TABLE}.sales_with_vat < 700) THEN "B. 300 - 699 SAR"
          WHEN ${currency_code} = "SAR" AND (${TABLE}.sales_with_vat >= 700 AND ${TABLE}.sales_with_vat < 2000) THEN "C. 700 - 1999 SAR"
          WHEN ${currency_code} = "SAR" AND ${TABLE}.sales_with_vat >= 2000 THEN "D. >= 2000 SAR"
          ELSE NULL
        END
    ;;
  }


  dimension: se_spend_buckets_ksanational {
    label: "Spend & Earn Spend Buckets"
    description: "For KSA National Day 2021 Spend & Earn"
    view_label: "MUSE Transactions Table"
    type: string
    sql: CASE WHEN ${currency_code} = "SAR" AND ${TABLE}.sales_with_vat < 300 AND ${TABLE}.sales_with_vat > 1  THEN "A. < 300 SAR"
                WHEN ${currency_code} = "SAR" AND (${TABLE}.sales_with_vat >= 300 AND ${TABLE}.sales_with_vat < 600) THEN "B. 300 - 599 SAR"
                WHEN ${currency_code} = "SAR" AND (${TABLE}.sales_with_vat >= 600 AND ${TABLE}.sales_with_vat < 1000) THEN "C. 600 - 999 SAR"
                WHEN ${currency_code} = "SAR" AND ${TABLE}.sales_with_vat >= 1000 THEN "D. >= 1000 SAR"
                ELSE "Excluded"
              END
          ;;

    }

  dimension: spend_bucket_1000 {
    label: "Spend Bucket - 1000 Local Currency"
    description: "For Connor McGregor contest, dimension to bucket members who have spent > 1000 local currency in each transaction"
    view_label: "MUSE Transactions Table"
    type: tier
    tiers: [1000]
    style: integer
    sql: ${TABLE}.sales_with_vat ;;
  }

  measure:  any_transaction_count{
    label: "Any Transaction Count"
    type: count_distinct
    view_label: "MUSE Transactions Table"
    sql: ${pk} ;;
  }

  measure:  spend_transaction_count{
    label: "Spend Transaction Count"
    type: count_distinct
    view_label: "MUSE Transactions Table"
    filters: [bit_type: "SPEND"]
    sql: ${pk} ;;
  }

  measure:  invoice_count{
    label: "Invoice Count"
    type: count_distinct
    hidden: no
    view_label: "MUSE Transactions Table"
    sql: ${invoice_no} ;;
  }

  measure: customer_count {
 view_label: "MUSE Transactions Table"
    label: "Customer Count"
    hidden: no
    type: count_distinct
    sql:  ${member_id};;
  }

##ATV
  measure: vpt {
    filters: [bit_type: "SPEND"]
    label: "Average Transaction Value (ATV) - local"
    view_label: "MUSE Transactions Table"
    value_format: "#,##0"
    description: "Average order value / average basket value (based on sales with vat)"
    type: average
    sql: ${TABLE}.sales_with_vat ;;
  }

  measure: vpt_usd {
    filters: [bit_type: "SPEND"]
    label: "Average Transaction Value (ATV) - USD"
    view_label: "MUSE Transactions Table"
    value_format: "$#,##0"
    description: "Average order value / average basket value (based on sales with vat)"
    type: average
    sql: CASE WHEN ${currency_code} = "AED" THEN ${TABLE}.sales_with_vat/3.67
    WHEN ${currency_code} = "KWD" THEN ${TABLE}.sales_with_vat*3.25
    WHEN ${currency_code} = "SAR" THEN ${TABLE}.sales_with_vat/3.75
    ELSE ${TABLE}.sales_with_vat
    END;;
  }


#####ACV
    measure: avg_customer_value {
    label: "Average Member Value (ACV) - local"
    view_label: "MUSE Transactions Table"
    value_format: "#,##0"
    description: "Average Customer Value [calc]"
    type: number
    sql: ${sales_with_vat}/nullif(${count_customer_transacted}, 0) ;;
  }

  measure: avg_customer_value_usd {
    label: "Average Member Value (ACV) - USD"
    view_label: "MUSE Transactions Table"
    value_format: "$#,##0"
    description: "Average Customer Value $ [calc]"
    type: number
    sql:  ${sales_with_vat_usd}/nullif(${count_customer_transacted}, 0);;
  }


###ATF
  measure: avg_frequency {
    view_label: "MUSE Transactions Table"
    description: "Based on unique invoices"
    label: "Average Transaction Frequency (ATF)"
    hidden: no
    type:  number
    value_format: "0.##"
    sql: ${invoice_count}/nullif(${count_customer_transacted},0) ;;
  }

#############################
#Segmentation

  measure: USD_Beauty_sales {
    label: "Beauty sales - USD"
    view_label: "MUSE Segmentation Map - Vertical"
    type: sum
    value_format: "$#,##0;(#.00)"
    sql: CASE WHEN ${currency_code} = "AED" and
        ${transacting_sponsor_name} in ("GHAWALI", "LOCCITANE",  "PENHALIGON'S",
                                    "MOLTON BROWN", "KILIAN PARIS", "NARS",
                                    "FACES",  "ROGER & GALLET", "URBAN DECAY",
                                    "YSL",  "GIORGIO ARMANI", "BOBBI BROWN",
                                    "FREDERIC MALLE", "ATELIER COLOGNE",  "BY KILIAN")
            THEN ${TABLE}.sales_with_vat/3.67
              WHEN ${currency_code} = "KWD" and
          ${transacting_sponsor_name}  in ("GHAWALI", "LOCCITANE",  "PENHALIGON'S",
                                   "MOLTON BROWN", "KILIAN PARIS", "NARS",
                                    "FACES",  "ROGER & GALLET", "URBAN DECAY",
                                    "YSL",  "GIORGIO ARMANI", "BOBBI BROWN",
                                    "FREDERIC MALLE", "ATELIER COLOGNE",  "BY KILIAN")
                THEN ${TABLE}.sales_with_vat*3.25
              WHEN ${currency_code} = "SAR" and
         ${transacting_sponsor_name}  in ("GHAWALI", "LOCCITANE",  "PENHALIGON'S",
                                    "MOLTON BROWN", "KILIAN PARIS", "NARS",
                                    "FACES",  "ROGER & GALLET", "URBAN DECAY",
                                    "YSL",  "GIORGIO ARMANI", "BOBBI BROWN",
                                    "FREDERIC MALLE", "ATELIER COLOGNE",  "BY KILIAN")
                   THEN ${TABLE}.sales_with_vat/3.75
                   else 0          END;;
  }

    dimension: is_Beauty_Brands {
    view_label: "MUSE Segmentation Map - Vertical"
    description: "Beauty Brands grouping"
    hidden: no
    type: yesno
    sql: ${transacting_sponsor_name} in ("GHAWALI", "LOCCITANE",  "PENHALIGON'S",
                              "MOLTON BROWN", "KILIAN PARIS", "NARS",
                              "FACES",  "ROGER & GALLET", "URBAN DECAY",
                              "YSL",  "GIORGIO ARMANI", "BOBBI BROWN",
                              "FREDERIC MALLE", "ATELIER COLOGNE",  "BY KILIAN") ;;
  }


  measure: beauty_visits{
    view_label: "MUSE Segmentation Map - Vertical"
    type: count_distinct
    filters: [bit_type: "SPEND" , is_Beauty_Brands: "Yes"]
    sql: concat(cast(${transaction_date_date} as string),cast( ${member_id} as string)) ;;

  }
  measure: USD_Fashion_sales {
    label: "Fashion sales - USD"
    view_label: "MUSE Segmentation Map - Vertical"
    type: sum
    value_format: "$#,##0;(#.00)"
    sql: CASE WHEN ${currency_code} = "AED" and
           ${transacting_sponsor_name} in ("SWAROVSKI",  "LACOSTE",  "MICHAEL KORS", "TUMI",
                                          "DOLCE&GABBANA",  "TORY BURCH", "WEEKEND MAX MARA",
                                          "KARL LAGERFELD", "FILA", "LEVEL SHOES",  "PAUL SMITH",
                                          "LONGCHAMP",  "FURLA",  "KENZO",  "MAX MARA", "SALVATORE FERRAGAMO",
                                          "STELLA MCCARTNEY", "TRYANO", "HACKETT",  "ALICE & OLIVIA", "DSQUARED2",
                                          "ZADIG & VOLTAIRE", "MARINA RINALDI", "PAULE KA", "RENE CAOVILLA",
                                          "IL GUFO",  "COURCELLES", "VILEBREQUIN",  "STELLA MC CARTNEY",
                                          "DOLCE & GABBANA","VERSACE" , "HUGO BOSS" , "HUGO BOSS KIDS")
                  THEN ${TABLE}.sales_with_vat/3.67
                    WHEN ${currency_code} = "KWD" and
               ${transacting_sponsor_name}  in ("SWAROVSKI",  "LACOSTE",  "MICHAEL KORS", "TUMI",
                                          "DOLCE&GABBANA",  "TORY BURCH", "WEEKEND MAX MARA",
                                          "KARL LAGERFELD", "FILA", "LEVEL SHOES",  "PAUL SMITH",
                                          "LONGCHAMP",  "FURLA",  "KENZO",  "MAX MARA", "SALVATORE FERRAGAMO",
                                          "STELLA MCCARTNEY", "TRYANO", "HACKETT",  "ALICE & OLIVIA", "DSQUARED2",
                                          "ZADIG & VOLTAIRE", "MARINA RINALDI", "PAULE KA", "RENE CAOVILLA",
                                          "IL GUFO",  "COURCELLES", "VILEBREQUIN",  "STELLA MC CARTNEY",
                                          "DOLCE & GABBANA","VERSACE" , "HUGO BOSS", "HUGO BOSS KIDS")
                      THEN ${TABLE}.sales_with_vat*3.25
                    WHEN ${currency_code} = "SAR" and
           ${transacting_sponsor_name}  in ("SWAROVSKI",  "LACOSTE",  "MICHAEL KORS", "TUMI",
                                          "DOLCE&GABBANA",  "TORY BURCH", "WEEKEND MAX MARA",
                                          "KARL LAGERFELD", "FILA", "LEVEL SHOES",  "PAUL SMITH",
                                          "LONGCHAMP",  "FURLA",  "KENZO",  "MAX MARA", "SALVATORE FERRAGAMO",
                                          "STELLA MCCARTNEY", "TRYANO", "HACKETT",  "ALICE & OLIVIA", "DSQUARED2",
                                          "ZADIG & VOLTAIRE", "MARINA RINALDI", "PAULE KA", "RENE CAOVILLA",
                                          "IL GUFO",  "COURCELLES", "VILEBREQUIN",  "STELLA MC CARTNEY",
                                          "DOLCE & GABBANA","VERSACE" , "HUGO BOSS", "HUGO BOSS KIDS")
                         THEN ${TABLE}.sales_with_vat/3.75
                         else 0
                     END;;
  }


  dimension: is_fashion_Brands {
    view_label: "MUSE Segmentation Map - Vertical"
    description: "Fashion Brands grouping"
    hidden: no
    type: yesno
    sql: ${transacting_sponsor_name} in ("SWAROVSKI",  "LACOSTE",  "MICHAEL KORS", "TUMI",
                                    "DOLCE&GABBANA",  "TORY BURCH", "WEEKEND MAX MARA",
                                    "KARL LAGERFELD", "FILA", "LEVEL SHOES",  "PAUL SMITH",
                                    "LONGCHAMP",  "FURLA",  "KENZO",  "MAX MARA", "SALVATORE FERRAGAMO",
                                    "STELLA MCCARTNEY", "TRYANO", "HACKETT",  "ALICE & OLIVIA", "DSQUARED2",
                                    "ZADIG & VOLTAIRE", "MARINA RINALDI", "PAULE KA", "RENE CAOVILLA",
                                    "IL GUFO",  "COURCELLES", "VILEBREQUIN",  "STELLA MC CARTNEY",
                                    "DOLCE & GABBANA","VERSACE" , "HUGO BOSS", "HUGO BOSS KIDS") ;;
  }

  measure: fashion_visits{
    view_label: "MUSE Segmentation Map - Vertical"
    type: count_distinct
    filters: [bit_type: "SPEND" , is_fashion_Brands: "Yes"]
    sql: concat(cast(${transaction_date_date} as string),cast( ${member_id} as string)) ;;

  }



  measure: USD_Lifestyle_sales {
    label: "Lifestyle sales - USD"
    view_label: "MUSE Segmentation Map - Vertical"
    type: sum
    value_format: "$#,##0;(#.00)"
    sql: CASE WHEN ${currency_code} = "AED" and
              ${transacting_sponsor_name}  in ("TANAGRA",  "CHRISTOFLE",
                                            "BACCARAT", "b8ta", "ST DUPONT",
                                            "DYLAN'S CANDY BAR",  "B8ta")
                  THEN ${TABLE}.sales_with_vat/3.67
                    WHEN ${currency_code} = "KWD" and
               ${transacting_sponsor_name}  in ("TANAGRA",  "CHRISTOFLE",
                                            "BACCARAT", "b8ta", "ST DUPONT",
                                            "DYLAN'S CANDY BAR",  "B8ta")
                      THEN ${TABLE}.sales_with_vat*3.25
                    WHEN ${currency_code} = "SAR"
                        and
             ${transacting_sponsor_name} in ("TANAGRA",  "CHRISTOFLE",
                                            "BACCARAT", "b8ta", "ST DUPONT",
                                            "DYLAN'S CANDY BAR",  "B8ta")THEN ${TABLE}.sales_with_vat/3.75
                          else 0
                     END;;
  }

  dimension: is_lifestyle_Brands {
    view_label: "MUSE Segmentation Map - Vertical"
    description: "Lifestyle Brands grouping"
    hidden: no
    type: yesno
    sql: ${transacting_sponsor_name} in ("TANAGRA",  "CHRISTOFLE",
                                      "BACCARAT", "b8ta", "ST DUPONT",
                                      "DYLAN'S CANDY BAR",  "B8ta") ;;
  }

  measure: lifestyle_visits{
    view_label: "MUSE Segmentation Map - Vertical"
    type: count_distinct
    filters: [bit_type: "SPEND" , is_lifestyle_Brands: "Yes"]
    sql: concat(cast(${transaction_date_date} as string),cast( ${member_id} as string)) ;;

  }

  # 2019 end Static Flags

  ## date flag created to tag until 2019 end
  dimension: Yearly_flag_2019{
    view_label: "MUSE Segmentation Map - Vertical"
    description: "Flag to identify dates until 2019 end"
    hidden: no
    type: yesno
    sql: ${transaction_date_date} <= "2019-12-31" ;;
  }

  dimension: Yearly_flag_2020{
    view_label: "MUSE Segmentation Map - Vertical"
    description: "Flag to identify the year 2020"
    hidden: no
    type: yesno
    sql: ${transaction_date_date} <= "2020-12-31" and
    ${transaction_date_date} >= "2020-01-01";;
  }

  measure: count_of_2020visits{
    view_label: "MUSE Segmentation Map - Vertical"
    label: "Member Visits in 2020"
    description: "Unique visits in 2020 "
    type: count_distinct
    filters: [bit_type: "SPEND",Yearly_flag_2020: "Yes"]
    sql: concat(cast(${transaction_date_date} as string),cast(${member_id} as string)) ;;

  }

  measure: count_of_2019visits{
    view_label: "MUSE Segmentation Map - Vertical"
    label: "Member Visits in 2019"
    description: "Unique visits in 2019 "
    type: count_distinct
    filters: [bit_type: "SPEND",Yearly_flag_2019: "Yes"]
    sql: concat(cast(${transaction_date_date} as string),cast(${member_id} as string)) ;;

  }





  # 2020 end Static Flags

  ## date flag created to tag until 2020 end

  dimension: until2020_end_flag{
    view_label: "MUSE Segmentation Map - Vertical"
    description: "Flag to identify dates until 2020 end"
    hidden: no
    type: yesno
    sql: ${transaction_date_date} <= "2020-12-31" ;;
  }


  measure: count_of_brands_until2020{
    view_label: "MUSE Segmentation Map - Vertical"
    type: count_distinct
    filters: [bit_type: "SPEND", until2020_end_flag: "Yes" ]
    sql: concat(cast(${transacting_sponsor_name} as string),cast(${member_id} as string)) ;;

  }



  # #last visit date in 2020

  # measure: until2020_last_purchase_date{
  #   view_label: "MUSE Segmentation Map - Vertical"
  #   description: "last purchase date in 2020"
  #   hidden: no
  #   type: date
  #   filters: [bit_type: "SPEND" , until2020_end_flag: "Yes"]
  #   sql: concat(cast(${transaction_date_date} as string),cast( ${member_id} as string)) ;;

  # }




## Launch till DEC' 2020 Beauty Sales

  measure: USD_Beauty_sales_2020end {
    label: "Beauty sales - USD until 2020"
    view_label: "MUSE Segmentation Map - Vertical"
    type: sum
    value_format: "$#,##0;(#.00)"
    sql: CASE WHEN ${currency_code} = "AED" and
        ${transacting_sponsor_name} in ("GHAWALI", "LOCCITANE",  "PENHALIGON'S",
                                    "MOLTON BROWN", "KILIAN PARIS", "NARS",
                                    "FACES",  "ROGER & GALLET", "URBAN DECAY",
                                    "YSL",  "GIORGIO ARMANI", "BOBBI BROWN",
                                    "FREDERIC MALLE", "ATELIER COLOGNE",  "BY KILIAN")
                              and ${transaction_date_date} <= "2020-12-31"
            THEN ${TABLE}.sales_with_vat/3.67
              WHEN ${currency_code} = "KWD" and
          ${transacting_sponsor_name}  in ("GHAWALI", "LOCCITANE",  "PENHALIGON'S",
                                   "MOLTON BROWN", "KILIAN PARIS", "NARS",
                                    "FACES",  "ROGER & GALLET", "URBAN DECAY",
                                    "YSL",  "GIORGIO ARMANI", "BOBBI BROWN",
                                    "FREDERIC MALLE", "ATELIER COLOGNE",  "BY KILIAN")
                                    and ${transaction_date_date} <= "2020-12-31"
                THEN ${TABLE}.sales_with_vat*3.25
              WHEN ${currency_code} = "SAR" and
         ${transacting_sponsor_name}  in ("GHAWALI", "LOCCITANE",  "PENHALIGON'S",
                                    "MOLTON BROWN", "KILIAN PARIS", "NARS",
                                    "FACES",  "ROGER & GALLET", "URBAN DECAY",
                                    "YSL",  "GIORGIO ARMANI", "BOBBI BROWN",
                                    "FREDERIC MALLE", "ATELIER COLOGNE",  "BY KILIAN")
                                    and ${transaction_date_date} <= "2020-12-31"
                   THEN ${TABLE}.sales_with_vat/3.75
                   else 0          END;;
  }


  ## till 2020 end Beauty Visits

  measure: beauty_visits_2020end{
    view_label: "MUSE Segmentation Map - Vertical"
    type: count_distinct
    filters: [bit_type: "SPEND" , is_Beauty_Brands: "Yes", until2020_end_flag: "Yes"]
    sql: concat(cast(${transaction_date_date} as string),cast( ${member_id} as string)) ;;

  }

  measure: USD_Fashion_sales_2020end {
    label: "Fashion sales - USD until 2020"
    view_label: "MUSE Segmentation Map - Vertical"
    type: sum
    value_format: "$#,##0;(#.00)"
    sql: CASE WHEN ${currency_code} = "AED" and
           ${transacting_sponsor_name} in ("SWAROVSKI",  "LACOSTE",  "MICHAEL KORS", "TUMI",
                                          "DOLCE&GABBANA",  "TORY BURCH", "WEEKEND MAX MARA",
                                          "KARL LAGERFELD", "FILA", "LEVEL SHOES",  "PAUL SMITH",
                                          "LONGCHAMP",  "FURLA",  "KENZO",  "MAX MARA", "SALVATORE FERRAGAMO",
                                          "STELLA MCCARTNEY", "TRYANO", "HACKETT",  "ALICE & OLIVIA", "DSQUARED2",
                                          "ZADIG & VOLTAIRE", "MARINA RINALDI", "PAULE KA", "RENE CAOVILLA",
                                          "IL GUFO",  "COURCELLES", "VILEBREQUIN",  "STELLA MC CARTNEY",
                                          "DOLCE & GABBANA","VERSACE" , "HUGO BOSS" , "HUGO BOSS KIDS")
                  and ${transaction_date_date} <= "2020-12-31"
                  THEN ${TABLE}.sales_with_vat/3.67
                    WHEN ${currency_code} = "KWD" and
               ${transacting_sponsor_name}  in ("SWAROVSKI",  "LACOSTE",  "MICHAEL KORS", "TUMI",
                                          "DOLCE&GABBANA",  "TORY BURCH", "WEEKEND MAX MARA",
                                          "KARL LAGERFELD", "FILA", "LEVEL SHOES",  "PAUL SMITH",
                                          "LONGCHAMP",  "FURLA",  "KENZO",  "MAX MARA", "SALVATORE FERRAGAMO",
                                          "STELLA MCCARTNEY", "TRYANO", "HACKETT",  "ALICE & OLIVIA", "DSQUARED2",
                                          "ZADIG & VOLTAIRE", "MARINA RINALDI", "PAULE KA", "RENE CAOVILLA",
                                          "IL GUFO",  "COURCELLES", "VILEBREQUIN",  "STELLA MC CARTNEY",
                                          "DOLCE & GABBANA","VERSACE" , "HUGO BOSS", "HUGO BOSS KIDS")
                      and ${transaction_date_date} <= "2020-12-31"
                      THEN ${TABLE}.sales_with_vat*3.25
                    WHEN ${currency_code} = "SAR" and
           ${transacting_sponsor_name}  in ("SWAROVSKI",  "LACOSTE",  "MICHAEL KORS", "TUMI",
                                          "DOLCE&GABBANA",  "TORY BURCH", "WEEKEND MAX MARA",
                                          "KARL LAGERFELD", "FILA", "LEVEL SHOES",  "PAUL SMITH",
                                          "LONGCHAMP",  "FURLA",  "KENZO",  "MAX MARA", "SALVATORE FERRAGAMO",
                                          "STELLA MCCARTNEY", "TRYANO", "HACKETT",  "ALICE & OLIVIA", "DSQUARED2",
                                          "ZADIG & VOLTAIRE", "MARINA RINALDI", "PAULE KA", "RENE CAOVILLA",
                                          "IL GUFO",  "COURCELLES", "VILEBREQUIN",  "STELLA MC CARTNEY",
                                          "DOLCE & GABBANA","VERSACE" , "HUGO BOSS", "HUGO BOSS KIDS")
                         and ${transaction_date_date} <= "2020-12-31"
                        THEN ${TABLE}.sales_with_vat/3.75
                         else 0
                     END;;
  }

  measure: fashion_visits_2020end{
    view_label: "MUSE Segmentation Map - Vertical"
    type: count_distinct
    filters: [bit_type: "SPEND" , is_fashion_Brands: "Yes", until2020_end_flag: "Yes"]
    sql: concat(cast(${transaction_date_date} as string),cast( ${member_id} as string)) ;;

  }

  measure: USD_Lifestyle_sales_2020end {
    label: "Lifestyle sales - USD until 2020 end"
    view_label: "MUSE Segmentation Map - Vertical"
    type: sum
    value_format: "$#,##0;(#.00)"
    sql: CASE WHEN ${currency_code} = "AED" and
              ${transacting_sponsor_name}  in ("TANAGRA",  "CHRISTOFLE",
                                            "BACCARAT", "b8ta", "ST DUPONT",
                                            "DYLAN'S CANDY BAR",  "B8ta")
                   and ${transaction_date_date} <= "2020-12-31"
                  THEN ${TABLE}.sales_with_vat/3.67
                    WHEN ${currency_code} = "KWD" and
               ${transacting_sponsor_name}  in ("TANAGRA",  "CHRISTOFLE",
                                            "BACCARAT", "b8ta", "ST DUPONT",
                                            "DYLAN'S CANDY BAR",  "B8ta")
                       and ${transaction_date_date} <= "2020-12-31"
                      THEN ${TABLE}.sales_with_vat*3.25
                    WHEN ${currency_code} = "SAR"
                        and
             ${transacting_sponsor_name} in ("TANAGRA",  "CHRISTOFLE",
                                            "BACCARAT", "b8ta", "ST DUPONT",
                                            "DYLAN'S CANDY BAR",  "B8ta")
                                             and ${transaction_date_date} <= "2020-12-31"
                        THEN ${TABLE}.sales_with_vat/3.75
                          else 0
                     END;;
  }

  measure: lifestyle_visits_2020end {
    view_label: "MUSE Segmentation Map - Vertical"
    type: count_distinct
    filters: [bit_type: "SPEND" , is_lifestyle_Brands: "Yes", until2020_end_flag: "Yes"]
    sql: concat(cast(${transaction_date_date} as string),cast( ${member_id} as string)) ;;

  }

  measure: count_of_visits_2020end{
    view_label: "MUSE Segmentation Map - Vertical"
    label: "Visits until 2020 end"
    description: "Unique visits util 2020 end"
    type: count_distinct
    filters: [bit_type: "SPEND",until2020_end_flag: "Yes"]
    sql: concat(cast(${transaction_date_date} as string),cast(${member_id} as string)) ;;

  }


  #2021 Flags

  measure: count_of_visits_2021{
    view_label: "MUSE Segmentation Map - Vertical"
    label: "2021 Visits YTD"
    description: "Unique visits during 2021"
    type: count_distinct
    filters: [bit_type: "SPEND",until2020_end_flag: "No"]
    sql: concat(cast(${transaction_date_date} as string),cast(${member_id} as string)) ;;

  }

  measure: USDsales_2021YTD {
    label: "USD sales - 2021 YTD"
    view_label: "MUSE Segmentation Map - Vertical"
    type: sum
    value_format: "$#,##0;(#.00)"
    sql: CASE WHEN ${currency_code} = "AED"
    and ${transaction_date_date} > "2020-12-31"
                  THEN ${TABLE}.sales_with_vat/3.67
                    WHEN ${currency_code} = "KWD"
                    and ${transaction_date_date} > "2020-12-31"
                      THEN ${TABLE}.sales_with_vat*3.25
                    WHEN ${currency_code} = "SAR"
                        and ${transaction_date_date} > "2020-12-31"
                        THEN ${TABLE}.sales_with_vat/3.75
                          else 0
                     END;;

  }

  #flagging 2020 and 2021 Enrolled Members resp.













# Ramadan start Static Flags
  ## date flag created to tag until Ramadan start - 7th April 2021
  dimension: untilRamadan_start_flag{
    view_label: "MUSE Segmentation Map - Vertical"
    description: "Flag to identify dates until Ramadan start"
    hidden: no
    type: yesno
    sql: ${transaction_date_date} <= "2021-04-07" ;;
    }

  measure: count_of_visits_until_Ramadan{
    view_label: "MUSE Segmentation Map - Vertical"
    label: "Visits until Ramadan start"
    description: "Unique visits until Ramadan start"
    type: count_distinct
    filters: [bit_type: "SPEND",untilRamadan_start_flag: "Yes"]
    sql: concat(cast(${transaction_date_date} as string),cast(${member_id} as string)) ;;

  }
  measure: count_of_visits_Ramadan{
    view_label: "MUSE Segmentation Map - Vertical"
    label: "Visits during Ramadan"
    description: "Unique visits during Ramadan"
    type: count_distinct
    filters: [bit_type: "SPEND",ramadan_timeframe_flag: "Yes"]
    sql: concat(cast(${transaction_date_date} as string),cast(${member_id} as string)) ;;

  }

  measure: count_of_brands_Ramadan{
    view_label: "MUSE Segmentation Map - Vertical"
    label: "Brands shopped count during Ramadan"
    description: "Unique Brands shopped during Ramadan"
    type: count_distinct
    filters: [bit_type: "SPEND",ramadan_timeframe_flag: "Yes"]
    sql: concat(cast(${transacting_sponsor_name} as string),cast(${member_id} as string)) ;;
   }

  dimension: ramadan_timeframe_flag{
    view_label: "Ramadan timeframe flag"
    description: "Flag to identify dates for Ramadan"
    hidden: no
    type: yesno
    sql: ${transaction_date_date} <= "2021-05-15" and
      ${transaction_date_date} >= "2021-04-08";;
  }


  measure: sales_with_vat_usd_ramadan  {
    label: "Ramadan Sales With Vat (USD)"
    view_label: "MUSE Transactions Table"
    hidden: no
    type: sum
    value_format: "$#,##0;(#.00)"
    # filters: [ramadan_timeframe_flag: "yes"]
    sql: CASE WHEN ${currency_code} = "AED" and
                   ${transaction_date_date} >= "2021-04-08" and
                  ${transaction_date_date} <= "2021-05-15"  THEN ${TABLE}.sales_with_vat/3.67
              WHEN ${currency_code} = "KWD"
              and
                   ${transaction_date_date} >= "2021-04-08" and
                  ${transaction_date_date} <= "2021-05-15" THEN ${TABLE}.sales_with_vat*3.25
              WHEN ${currency_code} = "SAR" and
                   ${transaction_date_date} >= "2021-04-08" and
                  ${transaction_date_date} <= "2021-05-15"
              THEN ${TABLE}.sales_with_vat/3.75
              ELSE 0
         END;;
  }


  measure: service_fees  {
    label: "Service Fees"
    view_label: "MUSE Billing"
    type: sum
    value_format: "$#,##0;(#.00)"
    sql:  CASE WHEN  ${transacting_sponsor_name} = 'b8ta' THEN
                     CASE WHEN ${transacting_market} = 'United Arab Emirates' THEN (${TABLE}.eligible_spend * 0.005)/3.67
                          WHEN ${transacting_market} = 'Saudi Arabia' THEN (${TABLE}.eligible_spend * 0.005)/3.75
                          WHEN ${transacting_market} = 'Kuwait' THEN (${TABLE}.eligible_spend * 0.005)*3.25
                     END
               WHEN  ${transacting_sponsor_name} in( 'LEVEL SHOES', 'TRYANO', 'LOCCITANE') THEN
                     CASE WHEN ${transacting_market} = 'United Arab Emirates' THEN (${TABLE}.eligible_spend * 0.015)/3.67
                          WHEN ${transacting_market} = 'Saudi Arabia' THEN (${TABLE}.eligible_spend * 0.015)/3.75
                          WHEN ${transacting_market} = 'Kuwait' THEN (${TABLE}.eligible_spend * 0.015)*3.25
                     END
               WHEN  ${transacting_sponsor_name} = 'FACES' THEN
                     CASE WHEN ${transacting_market} = 'United Arab Emirates' THEN (${TABLE}.eligible_spend * 0.01)/3.67
                          WHEN ${transacting_market} = 'Saudi Arabia' THEN (${TABLE}.eligible_spend * 0.01)/3.75
                          WHEN ${transacting_market} = 'Kuwait' THEN (${TABLE}.eligible_spend * 0.01)*3.25
                     END
               WHEN  ${transacting_sponsor_name} in( 'TANAGRA','CHRISTOFLE','BACCARAT','ST DUPONT') THEN
                     CASE WHEN ${transacting_market} = 'United Arab Emirates' THEN (${TABLE}.eligible_spend * 0.013)/3.67
                          WHEN ${transacting_market} = 'Saudi Arabia' THEN (${TABLE}.eligible_spend * 0.013)/3.75
                          WHEN ${transacting_market} = 'Kuwait' THEN (${TABLE}.eligible_spend * 0.013)*3.25
                     END
               ELSE  CASE WHEN ${transacting_market} = 'United Arab Emirates' THEN (${TABLE}.eligible_spend * 0.02)/3.67
                          WHEN ${transacting_market} = 'Saudi Arabia' THEN (${TABLE}.eligible_spend * 0.02)/3.75
                          WHEN ${transacting_market} = 'Kuwait' THEN (${TABLE}.eligible_spend * 0.02)*3.25
                     END
         END;;



    }






    ##ELITE FLAGGING


  dimension: COMBINED_ELITE_FLAG {
    view_label: "ELITE"
    label: "COMBINED ELITE FLAG"
    type: string
    sql:  case WHEN ${member_id} in ( 720100049413, 720104239135, 720101481797, 720100182305, 720100197964, 720101730854, 720100964843, 720100116303, 720100377350,
720101712688, 720100102568, 720100157828, 720100829228, 720100479396, 720100485682, 720100491151, 720100211997, 720102693713,
720100580177, 720100308140, 720100161077, 720100384059, 720100221160, 720101634528, 720100932998, 720100722043, 720100523839,
720101005398, 720100749038, 720100352585, 720102784074, 720100214462, 720101457078, 720100740250, 720100480063, 720101093386,
720100092967, 720100280737, 720100131906, 720101370727, 720100338238, 720100622367, 720100456030, 720100731390, 720100324048,
720101145616, 720100286320, 720100213779, 720100462053, 720100778912, 720100260374, 720100329070, 720100170607, 720100259376,
720100580359, 720100750283, 720100162992, 720100188468, 720100801060, 720103119858, 720100661084, 720100539900, 720100040685,
720100038036, 720100043986, 720100048506, 720100035586, 720100537094, 720100129272, 720101185935, 720107999438, 720101609850,
720100075251, 720100218877, 720100431595, 720100301731, 720100199192, 720100536534, 720100049819, 720101648155, 720101790668, 720100391252, 720102415984, 720111355957)
then "A. UAE ELITE"

WHEN  ${member_id} in ( 720100265654, 720105617214, 720105722378, 720100415283, 720103430602, 720102751388, 720102007799, 720102976332, 720102451898, 720100144446, 720100202665,
720100660243, 720100519795, 720100366916, 720103780428, 720100427213, 720102024604, 720100378879, 720103305853, 720100683054, 720101804303, 720102261909,
720100271991, 720100718439, 720100108003, 720103964386, 720100039299, 720100234841, 720100312423, 720105672300 )
then "B. UAE AMB"

WHEN  ${member_id} in ( 720102405605, 720101237033, 720102076851, 720101828328, 720102119685, 720101252156, 720101154980, 720102103275, 720101785171, 720102337428,
720101241837, 720100675183, 720102083774, 720102550590, 720100951741, 720100783870, 720101628496, 720101284001, 720102104075, 720101942475,
720100574717, 720101616483, 720100676207, 720100669640, 720101186859, 720101516436, 720100675928, 720102090456, 720102342105, 720103568542,
720101728890, 720102240879, 720102274043, 720102551036, 720103583806, 720102978692, 720105054913, 720101722299, 720101148461, 720104125698 )
then "C. KWT ELITE"

WHEN  ${member_id} =  720101783903 then "D. KWT AMB"

WHEN  ${member_id} =  720100584047 then "E. KWT AMB FAHAD"

WHEN  ${member_id} in ( 720104025831, 720104423762, 720104495703, 720104385318, 720105702925, 720105702958, 720105703014, 720105150331, 720104042935, 720105703055,
720105703139, 720104602704, 720105703147, 720105703311, 720105703337, 720104068161, 720105708054, 720106404406, 720105835865, 720104829414,
720104292688, 720107566435, 720107521281, 720104378040, 720103568070, 720103089564, 720105761533, 720103824374, 720106175709, 720104951796,
720100094898, 720103330547, 720104811248, 720105248416, 720105895778, 720106173373, 720106223939, 720106892642, 720107029251, 720109433709,
720104209260, 720111102185, 720106567913, 720100794646, 720110714519, 720107440839, 720105518933, 720110048470, 720104592558, 720106156998, 720108330096
 ) then "F. KSA ELITE"

WHEN  ${member_id} in ( 720103985738, 720104001519, 720104390334, 720104137032, 720104423226 , 720104401073, 720103090125, 720105417417, 720104054708, 720104007839, 720105735792
 )  then "G. KSA AMB"

ELSE "NA" END;;

}


  dimension: ELITE_FLAG_MARKETWISE {
    view_label: "ELITE"
    label: "MARKETWISE ELITE FLAG"
    type: string
    sql:  case WHEN ${member_id} in ( 720100049413, 720104239135, 720101481797, 720100182305, 720100197964, 720101730854, 720100964843, 720100116303, 720100377350,
      720101712688, 720100102568, 720100157828, 720100829228, 720100479396, 720100485682, 720100491151, 720100211997, 720102693713,
      720100580177, 720100308140, 720100161077, 720100384059, 720100221160, 720101634528, 720100932998, 720100722043, 720100523839,
      720101005398, 720100749038, 720100352585, 720102784074, 720100214462, 720101457078, 720100740250, 720100480063, 720101093386,
      720100092967, 720100280737, 720100131906, 720101370727, 720100338238, 720100622367, 720100456030, 720100731390, 720100324048,
      720101145616, 720100286320, 720100213779, 720100462053, 720100778912, 720100260374, 720100329070, 720100170607, 720100259376,
      720100580359, 720100750283, 720100162992, 720100188468, 720100801060, 720103119858, 720100661084, 720100539900, 720100040685,
      720100038036, 720100043986, 720100048506, 720100035586, 720100537094, 720100129272, 720101185935, 720107999438, 720101609850,
      720100075251, 720100218877, 720100431595, 720100301731, 720100199192, 720100536534, 720100049819, 720101648155, 720101790668, 720100391252, 720102415984, 720111355957,
      720100265654, 720105617214, 720105722378, 720100415283, 720103430602, 720102751388, 720102007799, 720102976332, 720102451898, 720100144446, 720100202665,
      720100660243, 720100519795, 720100366916, 720103780428, 720100427213, 720102024604, 720100378879, 720103305853, 720100683054, 720101804303, 720102261909,
      720100271991, 720100718439, 720100108003, 720103964386, 720100039299, 720100234841, 720100312423, 720105672300 )
      then "A. UAE ELITE"

      WHEN  ${member_id} in ( 720102405605, 720101237033, 720102076851, 720101828328, 720102119685, 720101252156, 720101154980, 720102103275, 720101785171, 720102337428,
      720101241837, 720100675183, 720102083774, 720102550590, 720100951741, 720100783870, 720101628496, 720101284001, 720102104075, 720101942475,
      720100574717, 720101616483, 720100676207, 720100669640, 720101186859, 720101516436, 720100675928, 720102090456, 720102342105, 720103568542,
      720101728890, 720102240879, 720102274043, 720102551036, 720103583806, 720102978692, 720105054913, 720101722299, 720101148461, 720104125698 , 720101783903, 720100584047 )
      then "B. KWT ELITE"

      WHEN  ${member_id} in ( 720104025831, 720104423762, 720104495703, 720104385318, 720105702925, 720105702958, 720105703014, 720105150331, 720104042935, 720105703055,
      720105703139, 720104602704, 720105703147, 720105703311, 720105703337, 720104068161, 720105708054, 720106404406, 720105835865, 720104829414,
      720104292688, 720107566435, 720107521281, 720104378040, 720103568070, 720103089564, 720105761533, 720103824374, 720106175709, 720104951796,
      720100094898, 720103330547, 720104811248, 720105248416, 720105895778, 720106173373, 720106223939, 720106892642, 720107029251, 720109433709,
      720104209260, 720111102185, 720106567913, 720100794646, 720110714519, 720107440839, 720105518933, 720110048470, 720104592558, 720106156998, 720108330096,
       720103985738, 720104001519, 720104390334, 720104137032, 720104423226 , 720104401073, 720103090125, 720105417417, 720104054708, 720104007839, 720105735792 ) then "C. KSA ELITE"

      ELSE "NA" END;;

    }

  dimension: ELITE_ONB_YEAR_FLAG {
    view_label: "ELITE"
    label: "FLAG TO IDENTIFY ELITE YEAR OF ONB"
    type: string
    sql:  case WHEN ${member_id} in
    (720100049413,  720104239135, 720101481797, 720100182305, 720100197964, 720101730854, 720100964843, 720100116303, 720100377350, 720101712688,
720100102568, 720100157828, 720100829228, 720100479396, 720100485682, 720100491151, 720100211997, 720102693713, 720100580177, 720100308140,
720100161077, 720100384059, 720100221160, 720101634528, 720100932998, 720100722043, 720100523839, 720101005398, 720100749038, 720100352585,
720102784074, 720100214462, 720101457078, 720100740250, 720100480063, 720101093386, 720100092967, 720100280737, 720100131906, 720101370727,
720100338238, 720100622367, 720100456030, 720100731390, 720100324048, 720101145616, 720100286320, 720100213779, 720100462053, 720100778912,
720100260374, 720100329070, 720100170607, 720100259376, 720100580359, 720100750283, 720100162992, 720102451898, 720100660243, 720100427213,
720100271991, 720100039299, 720102405605, 720101237033, 720102076851, 720101828328, 720102119685, 720101252156, 720101154980, 720102103275,
720101785171, 720101241837, 720100675183, 720102083774, 720102550590, 720100951741, 720100783870, 720101628496, 720101284001, 720102104075,
720100574717, 720101616483, 720100676207, 720100669640, 720101186859, 720101516436, 720100675928, 720102090456, 720102342105, 720101728890,
720102240879, 720102274043, 720104025831, 720104423762, 720104495703, 720104385318, 720105702925, 720105702958, 720105703014, 720105150331,
720104042935, 720105703055, 720105703139, 720104602704, 720105703147, 720105703311, 720105703337, 720104068161, 720105708054)
    THEN "A. 2020 ONB ELITE"

  WHEN ${member_id} in (
    720100188468, 720100265654, 720105617214, 720105722378, 720100415283, 720103430602, 720102751388, 720102007799, 720102976332, 720100144446,
720100202665, 720100519795, 720100366916, 720103780428, 720102024604, 720100378879, 720103305853, 720100683054, 720101804303, 720102261909,
720100801060, 720103119858, 720100718439, 720100108003, 720100661084, 720100539900, 720100040685, 720100038036, 720100043986, 720100048506,
720100035586, 720100537094, 720100129272, 720101185935, 720107999438, 720101609850, 720103964386, 720100234841, 720100312423, 720100075251,
720100218877, 720100431595, 720100301731, 720100199192, 720100536534, 720100049819, 720101648155, 720101790668, 720100391252, 720102415984,
720111355957, 720100584047, 720101783903, 720102551036, 720103583806, 720102978692, 720105054913, 720101722299, 720101148461, 720102337428,
720101942475, 720103568542, 720104125698, 720106404406, 720105835865, 720103985738, 720104001519, 720104390334, 720104829414, 720104292688,
720107566435, 720107521281, 720104137032, 720104423226, 720104401073, 720104378040, 720103568070, 720103089564, 720105761533, 720103824374,
720106175709, 720104951796, 720100094898, 720103330547, 720104811248, 720105248416, 720105895778, 720106173373, 720106223939, 720106892642,
720107029251, 720109433709, 720104209260, 720103090125, 720105417417, 720104054708, 720104007839, 720105735792, 720111102185, 720106567913,
720100794646, 720110714519, 720107440839, 720105518933, 720110048470, 720104592558, 720106156998, 720108330096 )

THEN "B. 2021 ONB ELITE"

ELSE "NA" END;;


}



# Measures and Dimensions for 2020 ONB ELITE
  measure: Pre_ONB_Sales_SEPT20  {
    label: "Pre ONB Sales(USD) until 31st AUG'20"
    view_label: "ELITE"
    type: sum
    value_format: "$#,##0;(#.00)"
    sql:  CASE WHEN ${transaction_date_date} < "2020-09-01" and ${transacting_market} = 'United Arab Emirates' THEN  ${TABLE}.sales_with_vat/3.67
               WHEN ${transaction_date_date} < "2020-09-01" and ${transacting_market} = 'Saudi Arabia' THEN ${TABLE}.sales_with_vat/3.75
               WHEN ${transaction_date_date} < "2020-09-01" and ${transacting_market} = 'Kuwait' THEN ${TABLE}.sales_with_vat*3.25
               ELSE 0

         END;;

        }

  measure: Post_ONB_Sales_SEPT20  {
    label: "Post ONB Sales (USD) post 31st AUG'20"
    view_label: "ELITE"
    type: sum
    value_format: "$#,##0;(#.00)"
    sql:  CASE WHEN ${transaction_date_date} >= "2020-09-01" and ${transacting_market} = 'United Arab Emirates' THEN ${TABLE}.sales_with_vat/3.67
               WHEN ${transaction_date_date} >= "2020-09-01" and ${transacting_market} = 'Saudi Arabia' THEN ${TABLE}.sales_with_vat/3.75
               WHEN ${transaction_date_date} >= "2020-09-01" and ${transacting_market} = 'Kuwait' THEN ${TABLE}.sales_with_vat *3.25
               ELSE 0

         END;;

    }

  ## date flag created to tag until 1st SEpt'20 for ELITE ONB

  dimension: untilSEPT20_flag{
    view_label: "ELITE"
    description: "Flag to identify dates until 31st AUG'20"
    hidden: no
    type: yesno
    sql: ${transaction_date_date} < "2020-09-01" ;;
  }



    measure: Pre_ONB_Visits_SEPT20 {
    label: "Pre ONB Visits upto 31st AUG'20"
    view_label: "ELITE"
    type: count_distinct
    filters: [bit_type: "SPEND" , untilSEPT20_flag: "Yes"]
    sql: concat(cast(${transaction_date_date} as string),cast( ${member_id} as string)) ;;

  }
  measure: POST_ONB_Visits_SEPT20 {
    label: "Post ONB Visits post 31st AUG'20"
    view_label: "ELITE"
    type: count_distinct
    filters: [bit_type: "SPEND" , untilSEPT20_flag: "No"]
    sql: concat(cast(${transaction_date_date} as string),cast( ${member_id} as string)) ;;

  }

 ##########################################################################

# Measures and Dimensions for 2021 ONB AMB
  measure: Pre_ONB_Sales_APRIL21  {
    label: "AMB Pre ONB Sales(USD) until 31st MARCH'21"
    view_label: "ELITE"
    type: sum
    value_format: "$#,##0;(#.00)"
    sql:  CASE WHEN ${transaction_date_date} < "2021-04-01" and ${transacting_market} = 'United Arab Emirates' THEN  ${TABLE}.sales_with_vat/3.67
               WHEN ${transaction_date_date} < "2021-04-01" and ${transacting_market} = 'Saudi Arabia' THEN ${TABLE}.sales_with_vat/3.75
               WHEN ${transaction_date_date} < "2021-04-01" and ${transacting_market} = 'Kuwait' THEN ${TABLE}.sales_with_vat*3.25
               ELSE 0

         END;;

    }

    measure: Post_ONB_Sales_APRIL21  {
      label: "AMB Post ONB Sales (USD) post 31st MARCH'21"
      view_label: "ELITE"
      type: sum
      value_format: "$#,##0;(#.00)"
      sql:  CASE WHEN ${transaction_date_date} >= "2021-04-01" and ${transacting_market} = 'United Arab Emirates' THEN ${TABLE}.sales_with_vat/3.67
               WHEN ${transaction_date_date} >= "2021-04-01" and ${transacting_market} = 'Saudi Arabia' THEN ${TABLE}.sales_with_vat/3.75
               WHEN ${transaction_date_date} >= "2021-04-01" and ${transacting_market} = 'Kuwait' THEN ${TABLE}.sales_with_vat *3.25
               ELSE 0

         END;;

      }

      ## date flag created to tag until 1st SEpt'20 for ELITE ONB

      dimension: untilAPRIL21_flag{
        view_label: "ELITE"
        description: "Flag to identify dates till 31st MARCH'21"
        hidden: no
        type: yesno
        sql: ${transaction_date_date} < "2021-04-01" ;;
      }



      measure: Pre_ONB_Visits_APRIL21 {
        label: "AMB Pre ONB Visits upto 31st MARCH'21"
        view_label: "ELITE"
        type: count_distinct
        filters: [bit_type: "SPEND" , untilAPRIL21_flag: "Yes"]
        sql: concat(cast(${transaction_date_date} as string),cast( ${member_id} as string)) ;;

      }
      measure: POST_ONB_Visits_APRIL21 {
        label: "AMB Post ONB Visits post 31st MARCH'21"
        view_label: "ELITE"
        type: count_distinct
        filters: [bit_type: "SPEND" , untilAPRIL21_flag: "No"]
        sql: concat(cast(${transaction_date_date} as string),cast( ${member_id} as string)) ;;

      }








  dimension: ELITE_FLAG {
    view_label: "ELITE"
    label: "ELITE FLAG"
    type: yesno
    sql:  cast(${member_id} as string) in (

'720100049413', '720104239135', '720101481797', '720100182305', '720100197964', '720101730854', '720100964843',
'720100116303', '720100377350', '720101712688', '720100102568', '720100157828', '720100829228', '720100479396',
'720100485682', '720100491151', '720100211997', '720102693713', '720100580177', '720100308140', '720100161077',
'720100384059', '720100221160', '720101634528', '720100932998', '720100722043', '720100523839', '720101005398',
'720100749038', '720100352585', '720102784074', '720100214462', '720101457078', '720100740250', '720100480063',
'720101093386', '720100039299', '720100092967', '720100280737', '720100131906', '720101370727', '720100338238',
'720100622367', '720100456030', '720100731390', '720100324048', '720101145616', '720100286320', '720100213779',
'720100462053', '720100778912', '720100260374', '720100329070', '720100170607', '720100259376', '720100580359',
'720100750283', '720100162992', '720100188468', '720100265654', '720105617214', '720105722378', '720100415283',
'720103430602', '720102751388', '720102007799', '720102976332', '720102451898', '720100144446', '720100202665',
'720100660243', '720100519795', '720100366916', '720103780428', '720100427213', '720102024604', '720100378879',
'720103305853', '720100683054', '720101804303', '720102261909', '720100801060', '720103119858', '720100271991',
'720100718439', '720100108003', '720100661084', '720100539900', '720100040685', '720100038036', '720100043986',
'720100048506', '720100035586', '720100537094', '720100129272', '720101185935', '720107999438', '720101609850',
'720104025831', '720104423762', '720104495703', '720104385318', '720105702925', '720105702958', '720105703014',
'720105150331', '720104042935', '720105703055', '720105703139', '720104602704', '720105703147', '720105703311',
'720105703337', '720104068161', '720105708054', '720106404406', '720105835865', '720103985738', '720104001519',
'720104390334', '720104829414', '720104292688', '720107566435', '720107521281', '720104137032', '720104423226',
'720104401073', '720104378040', '720103568070', '720103089564', '720105761533', '720103824374', '720104153146',
'720106175709', '720104951796', '720100094898', '720103330547', '720104811248', '720105248416', '720105895778',
'720106173373', '720106223939', '720106892642', '720107029251', '720109433709', '720104209260', '720104125698',
'720100584047', '720101783903', '720102405605', '720101237033', '720102076851', '720101828328', '720102119685',
'720101252156', '720101154980', '720102103275', '720101785171', '720101241837', '720100675183', '720102083774',
'720102550590', '720100951741', '720100783870', '720101628496', '720101284001', '720102104075', '720100574717',
'720101616483', '720100676207', '720100669640', '720101186859', '720101516436', '720100675928', '720102090456',
'720102342105', '720101728890', '720102240879', '720102274043', '720102551036', '720103583806', '720102978692',
'720105054913', '720101722299', '720101148461', '720102337428', '720101942475', '720103568542'

   );;
    }

  measure: COUNT_ELITE_ONBOARDED {
    type: count_distinct
    view_label: "ELITE"
    label: "Total ELITE Onboarded"
    filters: [ELITE_FLAG: "yes"]
    sql: ${member_id};;
  }


  measure: ELITE_ONB_COUNT1 {
    view_label: "ELITE"
    label: "Total ELITE Onboarded1"
    type: count_distinct
    sql:   (

      720100049413, 720104239135, 720101481797, 720100182305, 720100197964, 720101730854, 720100964843,
720100116303, 720100377350, 720101712688, 720100102568, 720100157828, 720100829228, 720100479396,
720100485682, 720100491151, 720100211997, 720102693713, 720100580177, 720100308140, 720100161077,
720100384059, 720100221160, 720101634528, 720100932998, 720100722043, 720100523839, 720101005398,
720100749038, 720100352585, 720102784074, 720100214462, 720101457078, 720100740250, 720100480063,
720101093386, 720100039299, 720100092967, 720100280737, 720100131906, 720101370727, 720100338238,
720100622367, 720100456030, 720100731390, 720100324048, 720101145616, 720100286320, 720100213779,
720100462053, 720100778912, 720100260374, 720100329070, 720100170607, 720100259376, 720100580359,
720100750283, 720100162992, 720100188468, 720100265654, 720105617214, 720105722378, 720100415283,
720103430602, 720102751388, 720102007799, 720102976332, 720102451898, 720100144446, 720100202665,
720100660243, 720100519795, 720100366916, 720103780428, 720100427213, 720102024604, 720100378879,
720103305853, 720100683054, 720101804303, 720102261909, 720100801060, 720103119858, 720100271991,
720100718439, 720100108003, 720100661084, 720100539900, 720100040685, 720100038036, 720100043986,
720100048506, 720100035586, 720100537094, 720100129272, 720101185935, 720107999438, 720101609850,
720104025831, 720104423762, 720104495703, 720104385318, 720105702925, 720105702958, 720105703014,
720105150331, 720104042935, 720105703055, 720105703139, 720104602704, 720105703147, 720105703311,
720105703337, 720104068161, 720105708054, 720106404406, 720105835865, 720103985738, 720104001519,
720104390334, 720104829414, 720104292688, 720107566435, 720107521281, 720104137032, 720104423226,
720104401073, 720104378040, 720103568070, 720103089564, 720105761533, 720103824374, 720104153146,
720106175709, 720104951796, 720100094898, 720103330547, 720104811248, 720105248416, 720105895778,
720106173373, 720106223939, 720106892642, 720107029251, 720109433709, 720104209260, 720104125698,
720100584047, 720101783903, 720102405605, 720101237033, 720102076851, 720101828328, 720102119685,
720101252156, 720101154980, 720102103275, 720101785171, 720101241837, 720100675183, 720102083774,
720102550590, 720100951741, 720100783870, 720101628496, 720101284001, 720102104075, 720100574717,
720101616483, 720100676207, 720100669640, 720101186859, 720101516436, 720100675928, 720102090456,
720102342105, 720101728890, 720102240879, 720102274043, 720102551036, 720103583806, 720102978692,
720105054913, 720101722299, 720101148461, 720102337428, 720101942475, 720103568542

         );;
  }

  dimension: ELITE_UAE_FLAG {
    view_label: "ELITE"
    label: "ELITE UAE FLAG"
    type: yesno
    sql: cast(${member_id} as string) in (
        '720100049413', '720104239135', '720101481797', '720100182305', '720100197964', '720101730854', '720100964843', '720100116303',
'720100377350', '720101712688', '720100102568', '720100157828', '720100829228', '720100479396', '720100485682', '720100491151',
'720100211997', '720102693713', '720100580177', '720100308140', '720100161077', '720100384059', '720100221160', '720101634528',
'720100932998', '720100722043', '720100523839', '720101005398', '720100749038', '720100352585', '720102784074', '720100214462',
'720101457078', '720100740250', '720100480063', '720101093386', '720100039299', '720100092967', '720100280737', '720100131906',
'720101370727', '720100338238', '720100622367', '720100456030', '720100731390', '720100324048', '720101145616', '720100286320',
'720100213779', '720100462053', '720100778912', '720100260374', '720100329070', '720100170607', '720100259376', '720100580359',
'720100750283', '720100162992', '720100188468', '720100265654', '720105617214', '720105722378', '720100415283', '720103430602',
'720102751388', '720102007799', '720102976332', '720102451898', '720100144446', '720100202665', '720100660243', '720100519795',
'720100366916', '720103780428', '720100427213', '720102024604', '720100378879', '720103305853', '720100683054', '720101804303',
'720102261909', '720100801060', '720103119858', '720100271991', '720100718439', '720100108003', '720100661084', '720100539900',
'720100040685', '720100038036', '720100043986', '720100048506', '720100035586', '720100537094', '720100129272', '720101185935',
'720107999438', '720101609850'


) ;;
  }

  dimension: ELITE_KWT_FLAG {
    view_label: "ELITE"
    label: "ELITE KWT FLAG"
    type: yesno
    sql: cast(${member_id} as string) in (
'720100584047', '720101783903', '720102405605', '720101237033', '720102076851', '720101828328',
'720102119685', '720101252156', '720101154980', '720102103275', '720101785171', '720101241837',
'720100675183', '720102083774', '720102550590', '720100951741', '720100783870', '720101628496',
'720101284001', '720102104075', '720100574717', '720101616483', '720100676207', '720100669640',
'720101186859', '720101516436', '720100675928', '720102090456', '720102342105', '720101728890',
'720102240879', '720102274043', '720102551036', '720103583806', '720102978692', '720105054913',
'720101722299', '720101148461', '720102337428', '720101942475', '720103568542', '720104125698'


) ;;
  }

  dimension: ELITE_KSA_FLAG {
    view_label: "ELITE"
    label: "ELITE KSA FLAG"
    type: yesno
    sql: cast(${member_id} as string) in (
      '720104025831', '720104423762', '720104495703', '720104385318', '720105702925', '720105702958', '720105703014', '720105150331',
'720104042935', '720105703055', '720105703139', '720104602704', '720105703147', '720105703311', '720105703337', '720104068161',
'720105708054', '720106404406', '720105835865', '720103985738', '720104001519', '720104390334', '720104829414', '720104292688',
'720107566435', '720107521281', '720104137032', '720104423226', '720104401073', '720104378040', '720103568070', '720103089564',
'720105761533', '720103824374', '720104153146', '720106175709', '720104951796', '720100094898', '720103330547', '720104811248',
'720105248416', '720105895778', '720106173373', '720106223939', '720106892642', '720107029251', '720109433709', '720104209260'

      ) ;;
  }







  measure: bonus_points_fees {
    label: "Bonus Points Fees"
    view_label: "MUSE Billing"
    type: sum
    value_format: "$#,##0;(#.00)"
    sql: CASE WHEN ${transacting_market} = 'United Arab Emirates'  THEN (CAST(${TABLE}.bonus_points AS NUMERIC)* 0.01)/3.67
               WHEN ${transacting_market} = 'United Arab Emirates' THEN (CAST(${TABLE}.bonus_points AS NUMERIC) * 0.01)/3.67
               WHEN ${transacting_market} = 'Kuwait' THEN (CAST(${TABLE}.bonus_points AS NUMERIC) * 0.01)/3.67
               WHEN ${transacting_market} = 'Saudi Arabia' THEN (CAST(${TABLE}.bonus_points AS NUMERIC) * 0.01)/3.75
               ELSE 0
          END;;
  }

  measure: base_points_fees {
    label: "Base Points Fees"
    view_label: "MUSE Billing"
    type: sum
    value_format: "$#,##0;(#.00)"
    sql: CASE WHEN ${transacting_market} = 'United Arab Emirates'  THEN (CAST(${TABLE}.base_points AS NUMERIC) * 0.01)/3.67
               WHEN ${transacting_market} = 'United Arab Emirates' THEN (CAST(${TABLE}.base_points AS NUMERIC)* 0.01)/3.67
               WHEN ${transacting_market} = 'Kuwait' THEN (CAST(${TABLE}.base_points AS NUMERIC) * 0.01)/3.67
               WHEN ${transacting_market} = 'Saudi Arabia' then (CAST(${TABLE}.base_points AS NUMERIC) * 0.01)/3.75
               ELSE 0
          END;;
  }







  measure: count_customer_enrolled {
    type: count_distinct
    label: "Count Customer Enrolled"
view_label: "MUSE Transactions Table"
    filters: [bit_type: "ENROL"]
    sql: ${member_id};;
  }

  measure: count_customer_mnad {
    type: count_distinct
    label: "Count Customer MNAD"
view_label: "MUSE Transactions Table"
    filters: [bit_type: "MNAD"]
    sql: ${member_id};;
  }

  measure: count_customer_points {
    type: count_distinct
    label: "Count Customer Payment With Points"
view_label: "MUSE Transactions Table"
    filters: [bit_type: "PAYMENT_WITH_POINTS"]
    sql: ${member_id};;
  }

  measure: count_customer_tier_change {
    type: count_distinct
    label: "Count Customer Tier Change"
view_label: "MUSE Transactions Table"
    filters: [bit_type: "TIER_CHANGE"]
    sql: ${member_id};;
  }

  measure: count_customer_membership_change {
    type: count_distinct
    label: "Count Customer Membership Change"
view_label: "MUSE Transactions Table"
    filters: [bit_type: "MEMBERSHIP_CHANGE"]
    sql: ${member_id};;
  }

  measure: count_customer_event {
    type: count_distinct
    label: "Count Customer Event"
view_label: "MUSE Transactions Table"
    filters: [bit_type: "EVENT"]
    sql: ${member_id};;
  }

  measure: count_customer_member_update {
    type: count_distinct
    label: "Count Customer Member Update"
view_label: "MUSE Transactions Table"
    filters: [bit_type: "MEMBER_UPDATE"]
    sql: ${member_id};;
  }

  measure: count_customer_member_confirm {
    type: count_distinct
    label: "Count Customer Member Confirm"
view_label: "MUSE Transactions Table"
    filters: [bit_type: "MEMBER_CONFIRM"]
    sql: ${member_id};;
  }

  measure: count_customer_transacted {
    view_label: "MUSE Transactions Table"
    type: count_distinct
    filters: [bit_type: "SPEND"]
    drill_fields: [member_id]
    sql: ${member_id} ;;
  }

}



view: muse_gravty_aggregates {
  derived_table: {
    sql:
    SELECT
    distinct
        member_id,
        lifetime_sales_with_vat,
        first_txn_date,
        last_txn_date,
        customer_frequency,
        tenure,
        recency,
        customer_avg_time_between_purchases
    FROM `chb-prod-data-cust.prod_MUSE_ControlTower.prod_gravty_transactions`;;
  }

  dimension:member_id {
    primary_key: yes
    hidden: yes
    type:  string
    sql: ${TABLE}.member_id ;;
  }

  dimension: lifetime_sales_with_vat {
 view_label: "MUSE Transactions Table"
    group_label: "Tenure Dimensions"
    label: "Customer Lifetime Spend"
    hidden: no
    type:  number
    sql: ${TABLE}.lifetime_sales_with_vat ;;
  }

  dimension_group: first_txn_date {
 view_label: "MUSE Transactions Table"
    group_label: "Tenure Dimensions"
    label: "Customer First txn Date"
    hidden: no
    type: time
    timeframes: [
      raw,
      date
    ]
    sql: CAST(${TABLE}.first_txn_date AS TIMESTAMP) ;;
  }


  dimension_group: last_txn_date {
 view_label: "MUSE Transactions Table"
    group_label: "Tenure Dimensions"
    label: "Customer Last txn Date"
    hidden: no
    type: time
    timeframes: [
      raw,
      date
    ]
    sql: CAST(${TABLE}.last_txn_date AS TIMESTAMP) ;;
  }



  dimension: customer_frequency {
 view_label: "MUSE Transactions Table"
    group_label: "Tenure Dimensions"
    label: "Customer Frequency"
    hidden: no
    type:  number
    sql: ${TABLE}.customer_frequency ;;
  }

  dimension: recency {
 view_label: "MUSE Transactions Table"
    group_label: "Tenure Dimensions"
    label: "Recency"
    hidden: no
    type:  number
    sql: ${TABLE}.recency ;;
  }

  dimension: tenure {
 view_label: "MUSE Transactions Table"
    group_label: "Tenure Dimensions"
    label: "Tenure"
    hidden: no
    type:  number
    sql: ${TABLE}.tenure ;;
  }

  # dimension: cross_brand_count {
  #   view_label: "Customers"
  #   group_label: "Tenure Dimensions"
  #   label: "Cross Brand Adoption"
  #   hidden: no
  #   type:  number
  #   sql: ${TABLE}.cross_brand_count ;;
  # }

  dimension: customer_avg_time_between_purchases {
 view_label: "MUSE Transactions Table"
    group_label: "Tenure Dimensions"
    label: "Customer Time Between Purchases "
    hidden: no
    type:  number
    sql: ${TABLE}.customer_avg_time_between_purchases ;;
  }

  measure: avg_recency {
 view_label: "MUSE Transactions Table"
    label: "Average Recency"
    hidden: no
    type:  average_distinct
    sql_distinct_key: ${member_id} ;;
    value_format: "0.##"
    sql: ${TABLE}.recency ;;
  }

  measure: avg_tenure {
 view_label: "MUSE Transactions Table"
    label: "Average Tenure"
    hidden: no
    type:  average_distinct
    sql_distinct_key: ${member_id} ;;
    value_format: "0.##"
    sql: ${TABLE}.tenure ;;
  }

  # measure: avg_cross_brand_count {
  #   view_label: "Customers"
  #   label: "Average Cross Brand Adoption"
  #   hidden: no
  #   type:  average_distinct
  #   sql_distinct_key: ${member_id} ;;
  #   value_format: "0.##"
  #   sql: ${TABLE}.cross_brand_count ;;
  # }



  measure: last_txn_date {
    label: "Customer Last txn Date"
    view_label: "MUSE Transactions Table"
    hidden: no
    type: date
    sql: MAX(${TABLE}.transaction_date);;
  }


  dimension: Last_active_period_flag{
     view_label: "MUSE Member Table"
    # group_label: "Tenure Dimensions"
    description: "Flagged based on last purchase date"
    label : "Recency Flag"
    hidden: no
    type: string
    #filters: [bit_type: "SPEND"]
    sql: case when   ${recency} > 336    then 'G. >=12+ Months'
              when   ${recency}  > 168    then 'F. At risk of Lapsing >=24+ Weeks/ 6 Months'
              when   ${recency}  >  84    then 'E. At risk of Lapsing >=12+ Weeks/ 3 Months'
              when   ${recency}  > 56     then 'D. 9 - 12 weeks'
              when   ${recency}  > 28     then 'C. Previous Month (4 - 8 weeks)'
              when   ${recency}  > 14     then 'B. Last 1 Month (2 - 4 Weeks)'
              when   ${recency}  < 15     then 'A. Last 2 Weeks'
              when   ${recency}  is null  then 'H. Non Spender'
              else 'NA'
            end ;;

    }


#Patrick's measure
  # measure: visits{
  #   label: "Customer Visits (distinct dates)"
  #   view_label: "MUSE Transactions Table"
  #   hidden: no
  #   type: count_distinct
  #   sql: ${TABLE}.transaction_date;;
  # }


#Patrick's measure
  # measure: avg_frequency {
  #   view_label: "MUSE Transactions Table"
  #   label: "Average Frequency"
  #   hidden: no
  #   type:  average_distinct
  #   sql_distinct_key: ${member_id} ;;
  #   value_format: "0.##"
  #   sql: ${TABLE}.customer_frequency ;;
  # }

  measure: avg_time_between_purchases {
 view_label: "MUSE Transactions Table"
    label: "Average Time Between Purchases "
    hidden: no
    type:  average_distinct
    sql_distinct_key: ${member_id} ;;
    value_format: "0.##"
    sql: ${TABLE}.customer_avg_time_between_purchases ;;
  }

  measure: count_customers {
    hidden: yes
 view_label: "MUSE Transactions Table"
    label: "ONLY FOR CALCULATION - Customer count"
    type: count_distinct
    sql: ${member_id} ;;
  }


  measure: sum_distinct_amount_usd {
    hidden: yes
 view_label: "MUSE Transactions Table"
    label: "ONLY FOR CALCULATION - lifetime_sum"
    type: sum_distinct
    sql_distinct_key: ${member_id} ;;
    sql: ${lifetime_sales_with_vat} ;;
  }

  measure: average_lifetime_spend  {
    label: "Average Lifetime Spend"
 view_label: "MUSE Transactions Table"
    value_format: "$0"
    type: number
    hidden: yes
    sql: ${sum_distinct_amount_usd}/NULLIF(${count_customers},0) ;;
  }



  # measure: cross_brand_shopper_count {
  #   type: count_distinct
  #   label: "Count of Cross Brand Shoppers"
  #   view_label: "Customers"
  #   hidden: no
  #   sql:
  #   CASE WHEN ${cross_brand_count} >= 2 THEN ${member_id}
  #   ELSE NULL
  #   END;;
  # }

  # measure: percent_cross_brand_shoppers{
  #   type: number
  #   label: "% of Cross Brand Shoppers"
  #   view_label: "Customers"
  #   hidden: no
  #   value_format: "0%"
  #   sql: (${cross_brand_shopper_count}/${count_customers})  ;;
  # }

}




view: muse_members {

  sql_table_name: `chb-prod-data-cust.prod_MUSE_ControlTower.stg_muse_prod_members` ;;

  dimension: member_id {
    label: "member_id"
    hidden: yes
    primary_key: yes
    type: string
    sql: CAST(${TABLE}.member_id AS STRING);;
  }

  dimension: tier_class {
    label: "Muse Tier Class"
    view_label: "MUSE Member Table"
    group_label: "Muse Dimensions"
    hidden: no
    primary_key: no
    type: string
    sql: ${TABLE}.tier_class;;
  }
}



view: members_ndt {
  view_label: "MUSE Transactions Table"
  derived_table: {
    explore_source: muse_control_tower_gravty
    {
      column: member_id {field: muse_gravty.member_id}
      column: count_of_visits {field: muse_gravty.count_of_visits}
      column: count_of_brands {field: muse_gravty.count_of_brands}
      column: count_of_channels {field: muse_gravty.count_of_channels}
      column: sales_with_vat_usd {field: muse_gravty.sales_with_vat_usd}
      column: count_of_brands_until2020 {field: muse_gravty.count_of_brands_until2020}

      bind_filters: {from_field: muse_calendar.transaction_date_date
        to_field: muse_calendar.transaction_date_date}
      bind_filters: {from_field: muse_calendar.transaction_date_month
        to_field: muse_calendar.transaction_date_month}
      bind_filters: {from_field: muse_calendar.transaction_date_quarter
        to_field: muse_calendar.transaction_date_quarter}
      bind_filters: {from_field: muse_calendar.transaction_date_year
        to_field: muse_calendar.transaction_date_year}
      bind_filters: {from_field: muse_calendar.transaction_date_week
        to_field: muse_calendar.transaction_date_week}

      bind_filters: {from_field: locations.district_name
        to_field: locations.district_name}

      bind_filters: {from_field: muse_gravty.transacting_sponsor_name
        to_field: muse_gravty.transacting_sponsor_name}
      bind_filters: {from_field: muse_gravty.transacting_sponsor_name_modified
        to_field: muse_gravty.transacting_sponsor_name_modified}
      bind_filters: {from_field: muse_gravty.transacting_market
        to_field: muse_gravty.transacting_market}

    }
  }
  dimension: member_id {
    hidden: yes
    primary_key: yes
  }


  dimension: count_of_visits {
    label: "xxx count_of_visits"
    hidden: yes
  }

  dimension: count_of_brands {
    hidden: yes
  }

  dimension: count_of_channels {
    hidden: yes
  }

  dimension: sales_with_vat_usd {
    hidden: yes
  }


  dimension: count_of_brands_until2020 {
    hidden: yes
  }



  dimension: Muse_Visitor_type{
    view_label: "Key MUSE KPIs"
    description: "Repeat visits grouping"
    label : "Muse Member Type (Visits)"
    hidden: no
    type: string
    sql: case when ${count_of_visits}  = 1  then 'Single'
              when ${count_of_visits} = 2  then 'Twice'
              when ${count_of_visits} >= 3 and ${count_of_visits} <=5 then '3 - 5 Visits'
              when ${count_of_visits} > 5 then ' 6 + Visits'
              else 'Non Spender'
             end ;;
  }

  dimension: Modified_Muse_Tier{
    view_label: "Key MUSE KPIs"
    label : "Modified Tier Class"
    hidden: no
    type: string
    sql: case when ${count_of_visits}  >= 1 and ${count_of_visits}  < 3 and ${muse_gravty.tier_class} = 'T1' then 'Emerald'
              when ${count_of_visits}  >= 3 and ${muse_gravty.tier_class} = 'T1' then 'Emerald+'
              when ${muse_gravty.tier_class} = 'T2' then 'Sapphire'
              when ${muse_gravty.tier_class} = 'T3' then 'Ruby'
              when ${muse_gravty.tier_class} = 'T4' then 'Elite'
              else 'NA'
             end ;;
  }



  dimension: Muse_Visitor_type_Crossbrand{
    view_label: "Key MUSE KPIs"
    description: "Cross brand shoppers grouping"
    label : "Muse Member Type (Cross Brand Adoption)"
    hidden: no
    type: string
    sql: case when ${count_of_brands}  = 1  then 'Single Brand'
              when ${count_of_brands} > 1 then 'Multi Brand'
              else null
             end ;;
  }


  dimension: Muse_Brands_shopped_count_until2020{
    view_label: "MUSE Segmentation Map - Vertical"
    description: " Count of Brands Shopped until 2020"
    label : " Count of Brands Shopped until 2020"
    hidden: no
    type: string
    sql: case when ${count_of_brands_until2020}  = 1  then 'Single Brand Shoppers'
              when ${count_of_brands_until2020} = 2 then '2 Brand shopper Shoppers'
              when ${count_of_brands_until2020} = 3 then '3 Brand shopper Shoppers'
              when ${count_of_brands_until2020} = 4 then '4 Brand shopper Shoppers'
              when ${count_of_brands_until2020} = 5 then '5 Brand shopper Shoppers'
              when ${count_of_brands_until2020} = 6 then '6 Brand shopper Shoppers'
              when ${count_of_brands_until2020} = 7 then '7 Brand shopper Shoppers'
              when ${count_of_brands_until2020} = 8 then '8 Brand shopper Shoppers'
              when ${count_of_brands_until2020} = 9 then '9 Brand shopper Shoppers'
              when ${count_of_brands_until2020} = 10 then '10 Brand shopper Shoppers'
              when ${count_of_brands_until2020} > 10 then '10+ Brand shopper Shoppers'
              else null
             end ;;
  }

  dimension: Muse_Visitor_type_NewBrand_adoption{
    view_label: "MUSE Segmentation Map - Vertical"
    description: "New Brand Trialists Flag"
    label : "New Brand Trialists Flag"
    hidden: no
    type: string
    sql: case when ${count_of_brands_until2020}  < ${count_of_brands}   then 'New Brand Adopted'
              when ${count_of_brands_until2020}  = ${count_of_brands}   then 'No New Brand Adopted'
              else null
             end ;;
  }


  dimension: Muse_Brands_shopped_count {
    view_label: "MUSE Segmentation Map - Vertical"
    description: " Count of Brands Shopped until today"
    label : " Count of Brands Shopped until today"
    hidden: no
    type: string
    sql: case when ${count_of_brands}  = 1  then 'Single Brand Shoppers'
              when ${count_of_brands} = 2 then '2 Brand shopper Shoppers'
              when ${count_of_brands} = 3 then '3 Brand shopper Shoppers'
              when ${count_of_brands} = 4 then '4 Brand shopper Shoppers'
              when ${count_of_brands} = 5 then '5 Brand shopper Shoppers'
              when ${count_of_brands} = 6 then '6 Brand shopper Shoppers'
              when ${count_of_brands} = 7 then '7 Brand shopper Shoppers'
              when ${count_of_brands} = 8 then '8 Brand shopper Shoppers'
              when ${count_of_brands} = 9 then '9 Brand shopper Shoppers'
              when ${count_of_brands} = 10 then '10 Brand shopper Shoppers'
              when ${count_of_brands} > 10 then '10+ Brand shopper Shoppers'
              else null
             end ;;
  }





  dimension: Muse_Visitor_type_Channel{
    view_label: "Key MUSE KPIs"
    description: "Hybrid shoppers grouping"
    label : "Muse Member Type (Ecomm/Instore/Hybrid)"
    hidden: no
    type: string
    sql: case when ${count_of_channels}  = 1  then 'Single Channel'
              when ${count_of_channels} > 1 then 'Hybrid'
              else null
             end ;;
  }
###########################################


#For comparison against multiple time frames, by user input

  filter: timeframe_a {
    type: date_time
    datatype: date
  }

  dimension: timeframe_a_yesno {
    hidden: yes
    type: yesno
    sql: {% condition timeframe_a %} ${muse_gravty.transaction_date_raw} {% endcondition %} ;;
  }

  measure: count_members_timeframe_a {
    type: count_distinct
    filters: [timeframe_a_yesno: "yes"]
    sql: ${member_id} ;;

  }


  measure: sum_sales_usd_timeframe_a {
    type: sum_distinct
    sql_distinct_key: ${member_id} ;;
    filters: [timeframe_a_yesno: "yes"]
    value_format: "$#,##0"
    sql: ${sales_with_vat_usd}  ;;
  }


  # measure: count_visits_timeframe_a {
  #   type: count_distinct
  #   hidden: no
  #   filters: [timeframe_a_yesno: "yes"]
  #   sql: CONCAT(CAST(${member_id} AS STRING), CAST(${muse_gravty.transaction_date_date} AS STRING)) ;;
  # }

  # dimension: visits_timeframe_a {
  #   hidden: yes
  #   sql: ${count_visits_timeframe_a} ;;
  # }

  # dimension: visitor_type_timeframe_a {
  #   type: string
  #   sql: case when ${visits_timeframe_a}  = 1  then 'Single'
  #             when ${visits_timeframe_a} = 2  then 'Twice'
  #             when ${visits_timeframe_a} >= 3 and ${visits_timeframe_a} <=5 then '3 - 5 Visits'
  #             when ${visits_timeframe_a} > 5 then ' 6 + Visits'
  #             else 'Non Spender'
  #           end ;;
  # }


#############################################


}




# view: shopper_type {
#   derived_table: {explore_source:muse_gravty {

#     column: member_id {field:muse_gravty.member_id}
#     column: USD_Beauty_sales {field:muse_gravty.USD_Beauty_sales}
#     column: USD_Fashion_sales {field:muse_gravty.USD_Fashion_sales}
#     column: USD_Lifestyle_sales {field:muse_gravty.USD_Lifestyle_sales}



#       bind_filters: {from_field: muse_gravty.transaction_date_date
#         to_field: muse_gravty.transaction_date_date}
#       bind_filters: {from_field: muse_gravty.transaction_date_month
#         to_field: muse_gravty.transaction_date_month}
#       bind_filters: {from_field: muse_gravty.transaction_date_quarter
#         to_field: muse_gravty.transaction_date_quarter}
#       bind_filters: {from_field: muse_gravty.transaction_date_year
#         to_field: muse_gravty.transaction_date_year}
#       bind_filters: {from_field: muse_gravty.transaction_date_week
#         to_field: muse_gravty.transaction_date_week}

#     }
#     }

#   dimension: member_id{
#     hidden: yes
#     }


#     dimension:USD_Beauty_sales  {
#       hidden: yes
#     }

#     dimension:USD_Fashion_sales  {
#       hidden: yes
#     }

#     dimension:USD_Lifestyle_sales  {
#       hidden: yes
#     }

#       dimension: Shopper_type{
#         view_label: "MUSE Transactions Table"
#         description: "Flagged based on purchasing Vertical"
#         label : "Muse Shopper Type"
#         hidden: no
#         type: string
#         #filters: [bit_type: "SPEND"]
#         sql: case when (${USD_Beauty_sales} != 0 and ${USD_Fashion_sales} = 0
#               and ${}USD_Lifestyle_sales} = 0 )
#               then 'Beauty Only Shopper'
#               when (${USD_Beauty_sales}  = 0 and ${USD_Fashion_sales}!= 0
#               and ${USD_Lifestyle_sales} = 0 )
#               then 'Fashion Only Shopper'
#               when (${USD_Beauty_sales} = 0 and ${USD_Fashion_sales} = 0
#               and ${USD_Lifestyle_sales}!= 0 )
#               then 'Lifestyle Only Shopper'
#               when (${USD_Beauty_sales} = 0 and ${USD_Fashion_sales}= 0
#               and ${USD_Lifestyle_sales} = 0 )
#               then 'NA'
#               else 'Cross Category Shopper'
#             end ;;
#       }
# }
