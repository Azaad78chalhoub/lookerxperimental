view: contest_offers {
  sql_table_name: `chb-prod-data-cust.prod_MUSE_ControlTower.contest_offers`
    ;;


  dimension: pk {
    hidden: yes
    primary_key: yes
    type: string
    sql: CONCAT(${member_id},${offer_name},${offer_id}) ;;
  }

  measure: contest_entries{
    type: count
    sql_distinct_key: ${pk} ;;
    sql: ${member_id} ;;
  }

  dimension: accepted {
    type: yesno
    sql: ${TABLE}.accepted ;;
  }

  dimension_group: entry_created_ts {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_ts ;;
  }

  dimension: customer_type {
    label: "Customer Flag (Staff/Regular)"
    type: string
    sql: ${TABLE}.customer_type ;;
  }

  dimension: email {
    type: string
    hidden: yes
    sql: ${TABLE}.email ;;
  }

  dimension: first_name {
    type: string
    hidden: yes
    sql: ${TABLE}.first_name ;;
  }

  dimension: last_name {
    type: string
    hidden: yes
    sql: ${TABLE}.last_name ;;
  }

  dimension: member_id {
    type: number
    hidden: yes
    sql: ${TABLE}.member_id ;;
  }

  dimension: mobile {
    type: string
    hidden: yes
    sql: ${TABLE}.mobile ;;
  }

  dimension: offer_id {
    type: number
    sql: ${TABLE}.offer_id ;;
  }

  dimension: offer_name {
    type: string
    sql: ${TABLE}.offer_name ;;
  }

  dimension: tier_class {
    hidden: yes
    type: string
    sql: ${TABLE}.tier_class ;;
  }

  dimension_group: updated_ts {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.updated_ts ;;
  }


  measure: distinct_contest_entrants{
    type: count_distinct
    sql: ${member_id} ;;

  }


}
