#include: "../_common/period_over_period_flexible.view"

view: ecomm_customer_transactional {
  sql_table_name: `chb-prod-data-cust.prod_ecomm_cust.ecomm_orders_w_first_txn_date`
    ;;

  dimension: pk {
    hidden:yes
    type:  string
    primary_key:  yes
    sql: CONCAT(${TABLE}.customer_email,'_',${TABLE}.order_id) ;;
  }

  dimension: brand {
    label: "Brand"
    hidden: yes
    type: string
    sql: ${TABLE}.brand ;;
  }
  dimension: order_id {
    label: "Order ID"
    hidden: yes
    type: string
    sql: ${TABLE}.order_id ;;
  }

  dimension_group: order_date_local {
    label: "Order"
    view_label: "Ecomm Customer New Returning"
    hidden: no
    type: time
    datatype: date
    timeframes: [
      raw,
      date,
      day_of_week,
      week,
      month,
      quarter,
      year

    ]
    sql: ${TABLE}.order_date_local ;;
  }
}


view: ecomm_customer_pop {
  view_label: "Ecomm Customer New Returning"
  derived_table: {
    sql:
    WITH pop_period_1_table AS (
    SELECT
      {% if ecomm_customer_transactional.order_date_local_date._in_query %} CAST(order_date_local  AS DATE)
      {% elsif ecomm_customer_transactional.order_date_local_month._in_query %} FORMAT_DATE('%Y-%m', order_date_local )
      {% elsif ecomm_customer_transactional.order_date_local_quarter._in_query %} (FORMAT_TIMESTAMP('%Y-%m', TIMESTAMP_TRUNC(CAST(order_date_local AS TIMESTAMP), QUARTER)))
      {% elsif ecomm_customer_transactional.order_date_local_week._in_query %} EXTRACT(WEEK(MONDAY) FROM order_date_local)
      {% elsif ecomm_customer_transactional.order_date_local_year._in_query %} EXTRACT(YEAR FROM order_date_local )
      {% else %} CAST(order_date_local  AS DATE) {% endif %} AS date_group,
       min(order_date_local) AS period_1_start,
       max(order_date_local)  AS period_1_end,
      DATE_DIFF( max(order_date_local),min(order_date_local) , day) AS period_1_days
    FROM `chb-prod-data-cust.prod_ecomm_cust.ecomm_orders_w_first_txn_date`
    WHERE  {% condition current_date_range %} order_datetime_local {% endcondition %}
        GROUP BY 1
    ),
    pop_period_2_table AS (
      SELECT DISTINCT date_group,
      period_1_start, period_1_end,
           {% if compare_to._parameter_value == "Period" %}
              DATE_SUB(period_1_start , INTERVAL period_1_days DAY)
            {% elsif compare_to._parameter_value == "IsoYear" %}
              DATE_SUB(period_1_start, INTERVAL (DATE_DIFF(DATE(EXTRACT(YEAR FROM period_1_start) - 1 , 12 ,31 ), DATE(EXTRACT(YEAR FROM period_1_start) - 1 , 1 ,1 ), ISOWEEK)) WEEK)
            {% elsif compare_to._parameter_value == "Custom" %}
              {% date_start compare_to_date_range %}
            {% elsif compare_to._parameter_value == "Month" %}
               DATE_TRUNC(DATE_SUB(DATE_TRUNC(period_1_start, MONTH), INTERVAL 1 DAY), MONTH)
            {% elsif compare_to._parameter_value == "Year" %}
             DATE_TRUNC(DATE_SUB(DATE_TRUNC(period_1_start, YEAR), INTERVAL 1 DAY)  , YEAR)
            {% else %}
              DATE_SUB(period_1_start, INTERVAL 1 {% parameter compare_to %})
            {% endif %}  AS period_2_start,

            {% if compare_to._parameter_value == "Period" %}
              DATE_SUB(period_1_start, INTERVAL 0 DAY)
            {% elsif compare_to._parameter_value == "IsoYear" %}
              DATE_SUB(period_1_end, INTERVAL (DATE_DIFF(DATE(EXTRACT(YEAR FROM period_1_end) - 1 , 12 ,31 ), DATE(EXTRACT(YEAR FROM period_1_end) - 1 , 1 ,1 ), ISOWEEK)) WEEK)
            {% elsif compare_to._parameter_value == "Custom" %}
              DATE_SUB(CAST({% date_end compare_to_date_range %} AS DATE), INTERVAL 1 DAY)
            {% elsif compare_to._parameter_value == "Month" %}
              DATE_SUB(period_1_start, INTERVAL 1 DAY)
            {% elsif compare_to._parameter_value == "Year" %}
              DATE_SUB(DATE_TRUNC(period_1_start, YEAR), INTERVAL 1 DAY)
            {% else %}

              DATE_SUB(period_1_end, INTERVAL 1 {% parameter compare_to %})
            {% endif %} AS period_2_end





      FROM pop_period_1_table
    ), pop_base AS (

    SELECT
          pop.date_group,
          CASE
            WHEN txn.order_date_local BETWEEN pop.period_1_start AND pop.period_1_end THEN 'This Period'
            WHEN txn.order_date_local BETWEEN pop.period_2_start AND pop.period_2_end THEN 'Last Period'
          ELSE NULL END AS period,
          brand,
          order_id,
          customer_key
      FROM `chb-prod-data-cust.prod_ecomm_cust.ecomm_orders_w_first_txn_date` txn
      INNER JOIN pop_period_2_table pop ON 1=1 AND (txn.order_date_local BETWEEN pop.period_1_start AND pop.period_1_end) OR
      (txn.order_date_local BETWEEN pop.period_2_start AND pop.period_2_end)
      WHERE txn.order_date_local >= (SELECT MIN(period_2_start) FROM pop_period_2_table ))

      SELECT
        date_group,
        brand,
        --COUNT(DISTINCT order_id,

    count(distinct( CASE WHEN period = "Last Period" THEN customer_key ELSE null END)) AS customers_last_period,

              ((count(distinct( CASE WHEN period = "This Period" THEN customer_key ELSE null END)) +
    count(distinct( CASE WHEN period = "Last Period" THEN customer_key ELSE null END))) -
    count(distinct( CASE WHEN period IN ("This Period","Last Period") THEN customer_key ELSE null END))) AS retained_customers,

        ((count(distinct( CASE WHEN period = "This Period" THEN customer_key ELSE null END)) +
    count(distinct( CASE WHEN period = "Last Period" THEN customer_key ELSE null END))) -
    count(distinct( CASE WHEN period IN ("This Period","Last Period") THEN customer_key ELSE null END)))/NULLIF(count(distinct( CASE WHEN period = "Last Period" THEN customer_key ELSE null END)),0) AS retention_rate

      FROM pop_base
      GROUP BY date_group, brand

      ;;
  }

  dimension: pk {
    hidden:yes
    type:  string
    primary_key:  yes
    sql:  concat(${TABLE}.brand, ${TABLE}.date_group) ;;
  }

  dimension: brand {
    hidden:yes
    type:  string
    sql:  ${TABLE}.brand;;
  }

  filter: current_date_range {
    type: date
    view_label: "-- Period over Period"
    label: "   Date Range"
    description: "Select the date range you are interested in using this filter, can be used by itself. Make sure any filter on Event Date covers this period, or is removed."
    convert_tz: yes
  }

  #### TEST

  parameter: compare_to {
    view_label: "-- Period over Period"
    description: "Choose the period you would like to compare to. Must be used with Current Date Range filter"
    label: "Compare To:"
    type: unquoted
    allowed_value: {
      label: "Previous Period"
      value: "Period"
    }
    allowed_value: {
      label: "Previous Iso Year"
      value: "IsoYear"
    }
    allowed_value: {
      label: "Previous Calendar Week"
      value: "Week"
    }
    allowed_value: {
      label: "Previous Calendar Month"
      value: "Month"
    }
    allowed_value: {
      label: "Previous Calendar Year"
      value: "Year"
    }
    allowed_value: {
      label: "Custom"
      value: "Custom"
    }
    default_value: "Period"
  }

#   dimension: period {
#     view_label: "-- Period over Period"
#     #hidden: {{compare_to._value not null}}
#     type:  string
#     sql:  ${TABLE}.period;;
#   }
#

  measure: retention_rate {
    label: "Retention Rate"
    view_label: "-- Period over Period"
    type:  max
    value_format: "0%"
    sql: ${TABLE}.retention_rate;;
  }

  measure: retained_customers {
    label: "Retained Customers"
    view_label: "-- Period over Period"
    type:  max
    value_format: "0"
    sql: ${TABLE}.retained_customers;;
  }

  measure: customers_last_period {
    label: "Previous period Customers"
    view_label: "-- Period over Period"
    type:  max
    value_format: "0"
    sql: ${TABLE}.customers_last_period;;
  }
}

view: ecomm_customer_orders_derived_table {
  view_label: "Ecomm Customer New Returning"
  #extends: [period_over_period_flexible]
  derived_table: {
    sql:

    SELECT
     ROW_NUMBER() OVER() as row_num_pk,
     brand,
     customer_key,
     date,
     customer_type,

     SUM(gross_net_sales_usd_total) AS gross_net_sales_usd_total,
     SUM(count_orders) AS count_orders


    FROM

      (SELECT
      customer_key,
      brand,


{% if _explore._name == "tryano" %}

      {% if tryano.common_date._in_query %} CAST(order_date_local  AS DATE)
      {% elsif tryano.common_month._in_query %} FORMAT_DATE('%Y-%m', order_date_local )
      {% elsif tryano.common_quarter._in_query %} FORMAT_DATE('%Y-%Q', order_date_local)
      {% elsif tryano.common_week._in_query %} EXTRACT(WEEK(MONDAY) FROM order_date_local)
      {% else %} EXTRACT(YEAR FROM order_date_local )
      {% endif %}

{% elsif _explore._name == "swarovski" %}

      {% if swarovski.common_date_date._in_query %} CAST(order_date_local  AS DATE)
      {% elsif swarovski.common_date_month._in_query %} FORMAT_DATE('%Y-%m', order_date_local )
      {% elsif swarovski.common_date_quarter._in_query %} FORMAT_DATE('%Y-%Q', order_date_local)
      {% elsif swarovski.common_date_week._in_query %} EXTRACT(WEEK(MONDAY) FROM order_date_local)
      {% else %} EXTRACT(YEAR FROM order_date_local )
      {% endif %}


{% elsif _explore._name == "faces" %}

      {% if faces.common_date._in_query %} CAST(order_date_local  AS DATE)
      {% elsif faces.common_month._in_query %} FORMAT_DATE('%Y-%m', order_date_local )
      {% elsif faces.common_quarter._in_query %} FORMAT_DATE('%Y-%Q', order_date_local)
      {% elsif faces.common_week._in_query %} EXTRACT(WEEK(MONDAY) FROM order_date_local)
      {% else %} EXTRACT(YEAR FROM order_date_local )
      {% endif %}

{% elsif _explore._name == "stg_comm_chanel_faces_ga_landing_pages" %}

      {% if stg_comm_chanel_faces_ga_landing_pages.common_date._in_query %} CAST(order_date_local  AS DATE)
      {% elsif stg_comm_chanel_faces_ga_landing_pages.common_month._in_query %} FORMAT_DATE('%Y-%m', order_date_local )
      {% elsif stg_comm_chanel_faces_ga_landing_pages.common_quarter._in_query %} FORMAT_DATE('%Y-%Q', order_date_local)
      {% elsif stg_comm_chanel_faces_ga_landing_pages.common_week._in_query %} EXTRACT(WEEK(MONDAY) FROM order_date_local)
      {% else %} EXTRACT(YEAR FROM order_date_local )
      {% endif %}

{% elsif _explore._name == "lvl_facts_union" %}

      {% if lvl_facts_union.common_date._in_query %} CAST(order_date_local  AS DATE)
      {% elsif lvl_facts_union.common_month._in_query %} FORMAT_DATE('%Y-%m', order_date_local )
      {% elsif lvl_facts_union.common_quarter._in_query %} FORMAT_DATE('%Y-%Q', order_date_local)
      {% elsif lvl_facts_union.common_week._in_query %} EXTRACT(WEEK(MONDAY) FROM order_date_local)
      {% else %} EXTRACT(YEAR FROM order_date_local )
      {% endif %}

{% elsif _explore._name == "ecomm_orders_new_vs_returning" %}
      {% if ecomm_customer_transactional.order_date_local_date._in_query%} CAST(order_date_local  AS DATE)
      {% elsif ecomm_customer_transactional.order_date_local_month._in_query %} FORMAT_DATE('%Y-%m', order_date_local )
      {% elsif ecomm_customer_transactional.order_date_local_quarter._in_query %} (FORMAT_TIMESTAMP('%Y-%m', TIMESTAMP_TRUNC(CAST(order_date_local AS TIMESTAMP), QUARTER)))
      {% elsif ecomm_customer_transactional.order_date_local_week._in_query %} EXTRACT(WEEK(MONDAY) FROM order_date_local)
      {% elsif ecomm_customer_transactional.order_date_local_year._in_query %} EXTRACT(YEAR FROM order_date_local )
      {% else %} CAST(order_date_local  AS DATE)
      {% endif %}

{% else %}
      {% if ecomm_customer_orders_derived_table.order_date_local_date._in_query%} CAST(order_date_local  AS DATE)
      {% elsif ecomm_customer_orders_derived_table.order_date_local_month._in_query %} FORMAT_DATE('%Y-%m', order_date_local )
      {% elsif ecomm_customer_orders_derived_table.order_date_local_quarter._in_query %} FORMAT_DATE('%Y-%Q', order_date_local)
      {% elsif ecomm_customer_orders_derived_table.order_date_local_week._in_query %} EXTRACT(WEEK(MONDAY) FROM order_date_local)
      {% elsif ecomm_customer_orders_derived_table.order_date_local_year._in_query %} EXTRACT(YEAR FROM order_date_local )
      {% else %} CAST(order_date_local  AS DATE)
      {% endif %}

{% endif %}
      as date,

      MIN(customer_type) AS customer_type,
      SUM(gross_net_sales_usd_total) AS gross_net_sales_usd_total,
      COUNT(DISTINCT order_id) AS count_orders

      FROM `chb-prod-data-cust.prod_ecomm_cust.ecomm_orders_w_first_txn_date`

      GROUP BY 1,2,3
      )
    GROUP BY 2,3,4,5
    ORDER BY row_num_pk
    ;;
  }

  dimension: row_num_pk {
    hidden:yes
    type:  string
    primary_key:  yes
    sql:  ${TABLE}.row_num_pk ;;
  }
  dimension: date {type:date hidden:yes sql:${TABLE}.date;;}
  dimension: week {type:string hidden:yes sql:${TABLE}.date;;}
  dimension: month {type:string hidden:yes sql:${TABLE}.date;;}
  dimension: quarter {type:string hidden:yes sql:${TABLE}.date;;}
  dimension: year {type:number hidden:yes sql:${TABLE}.date;;}

  dimension_group: order_date_local {
    label: "Order"
    hidden: yes
    type: time
    datatype: date
    timeframes: [
      raw,
      date,
      day_of_week,
      week,
      month,
      quarter,
      year

    ]
    sql: ${TABLE}.date ;;
  }

  dimension: brand {
    label: "Brand"
    type: string
    sql: ${TABLE}.brand ;;
  }

  dimension: customer_email {
    label: "Customer Email"
    hidden: yes
    type: string
    sql: ${TABLE}.customer_email ;;
  }

  dimension: customer_key {
    hidden: yes
    type: string
    sql: ${TABLE}.customer_key ;;
  }

  dimension: customer_type {
    label: "Customer Type"
    description: "New or Returning Customer"
    type: string
    sql: ${TABLE}.customer_type ;;
  }

#   dimension: order_id {
#     label: "Order ID"
#     type: string
#     sql: ${TABLE}.order_id ;;
#   }
#
#   dimension: is_first_transaction {
#
#     type: yesno
#     sql: ${TABLE}.is_first_transaction ;;
#   }
#

  measure: count_orders {
    label: "Order Count"
    type: sum
    sql: ${TABLE}.count_orders ;;
  }

  measure: total_cust_count {
    label: "Customer Unique Count"
    type: count_distinct
    sql: ${TABLE}.customer_key ;;
  }

  measure: frequency {
    label: "Average Frequency"
    type: number
    value_format: "0.00"
    sql: IFNULL(${count_orders}/NULLIF(${total_cust_count},0),0) ;;
  }

  measure: gross_net_sales_usd_total {
    label: "Sales Amount USD"
    value_format: "$#,##0"
    type: sum
    sql: ${TABLE}.gross_net_sales_usd_total ;;
  }

  measure: aov_ {
    label: "AOV"
    value_format: "$#,##0"
    type: number
    sql: IFNULL(${gross_net_sales_usd_total}/NULLIF(${count_orders},0),0) ;;

  }

  measure: returning_rate {
    label: "Returning Rate"
    type: number
    value_format: "0.00\%"
    sql: (${ret_cust_count}/${total_cust_count})*100 ;;
  }

  measure: recruitment_rate {
    label: "Recruitment Rate"
    type: number
    value_format: "0.00\%"
    sql: (${new_cust_count}/${total_cust_count})*100 ;;
  }

  measure: cumulative_cust_count {
    label: "Cumul. Cust Count"
    type: running_total
    sql: ${new_cust_count} ;;
  }

  measure: ret_cust_count {
    hidden: yes
    label: "Returning Customer Count"
    type: number
    sql: ${total_cust_count}-${new_cust_count};;
  }

  measure: new_cust_count {
    hidden: yes
    label: "New Customer Count"
    type: count_distinct
    filters: {
      field: customer_type
      value: "New"
    }
    sql: ${customer_key} ;;
  }

  measure:  avg_spend{
    label: "Average Spend"
    value_format: "$#"
    type: number
    sql: ROUND(${gross_net_sales_usd_total}/${total_cust_count}) ;;
  }

}

view: ecomm_brand_aggregates {
  view_label: "Ecomm Brand Aggregates"
  derived_table: {
    sql:
      SELECT
        ROW_NUMBER() OVER() as row_num_pk,
        brand,
        count(distinct customer_key)  AS brand_unique_customers,
        AVG(gross_net_sales_usd_total) AS brand_avg_ltv,
        SUM(count_orders) AS brand_total_orders,
        AVG(tenure) AS brand_avg_tenure,
        AVG(recency) AS brand_avg_recency
      FROM
        (SELECT
            brand,
            customer_key  AS customer_key,
            DATE_DIFF(CURRENT_DATE(), MAX(order_date_local), DAY) AS recency,
            sum(gross_net_sales_usd_total) AS gross_net_sales_usd_total,
            count(distinct order_id) AS count_orders,
            DATE_DIFF(CURRENT_DATE(), CAST(first_order_datetime_utc AS DATE), DAY) AS tenure
        FROM `chb-prod-data-cust.prod_ecomm_cust.ecomm_orders_w_first_txn_date`
        GROUP BY  brand, customer_key, first_order_datetime_utc)
      GROUP BY brand
      ORDER BY row_num_pk;;
  }
  dimension: row_num_pk{primary_key:yes hidden:yes}
  dimension: brand {hidden:yes}


  measure: avg_lifetime_value {
    #   view_label: "Ecomm Customer New Returning"
    label: "Average Lifetime Spend"
    hidden: yes
    value_format: "$#"
    type: min
    sql: ${TABLE}.brand_avg_ltv ;;
  }
  measure: average_tenure_days {
    #   view_label: "Ecomm Customer New Returning"
    label: "Average Tenure"
    hidden: yes
    value_format: "0"
    type: min
    sql: ${TABLE}.brand_avg_tenure ;;
  }
  measure: average_recency_days {
    #   view_label: "Ecomm Customer New Returning"
    label: "Average Recency"
    hidden: yes
    value_format: "0"
    type: min
    sql: ${TABLE}.brand_avg_recency ;;
  }
}

view: ecomm_customer_aggregates {
  view_label: "Ecomm Customer New Returning"
  derived_table: {
    sql:
        SELECT
            ROW_NUMBER() OVER() as row_num_pk,
            customer_key  AS customer_key,
            CAST(first_order_datetime_utc AS TIMESTAMP) AS first_order_datetime_utc,
            DATE_DIFF(CURRENT_DATE(), MAX(order_date_local), DAY) AS customer_recency,
            DATE_DIFF(CURRENT_DATE(), CAST(first_order_datetime_utc AS DATE), DAY) AS customer_tenure,
            sum(gross_net_sales_usd_total) AS customer_ltv,
            count(distinct order_id) AS customer_orders
            --count(distinct order_id)/DATE_DIFF(CURRENT_DATE(), CAST(first_order_datetime_utc AS DATE), DAY) AS customer_frequency

        FROM `chb-prod-data-cust.prod_ecomm_cust.ecomm_orders_w_first_txn_date`
        GROUP BY
          customer_key,
          first_order_datetime_utc
        ORDER BY row_num_pk;;
  }
  dimension: row_num_pk{primary_key:yes hidden:yes}
  dimension: customer_key {hidden:yes}

  dimension: customer_ltv {
    label: "Customer LTV"
    type: number
    sql: ${TABLE}.customer_ltv ;;
  }

  measure: avg_customer_ltv {
    label: "Average Lifetime Spend"
    view_label: "Ecomm Brand Aggregates"
    value_format: "$#"
    type: average
    sql: ROUND(${TABLE}.customer_ltv) ;;
  }

  dimension: customer_ftxn {
    label: "Customer First Txn Date"
    type: date
    sql: ${TABLE}.first_order_datetime_utc ;;
  }

  dimension: customer_tenure {
    label: "Customer Tenure"
    type: number
    sql: ${TABLE}.customer_tenure ;;
  }

  measure: avg_customer_tenure {
    label: "Average Tenure"
    view_label: "Ecomm Brand Aggregates"
    value_format: "0"
    type: average
    sql: ${TABLE}.customer_tenure ;;
  }

  dimension: customer_recency {
    label: "Customer Recency"
    type: number
    sql: ${TABLE}.customer_recency ;;
  }

  measure: avg_customer_recency {
    label: "Average Recency"
    view_label: "Ecomm Brand Aggregates"
    type: average
    value_format: "0"
    sql: ${TABLE}.customer_recency ;;
  }

  dimension: customer_frequency {
    label: "Customer Frequency"
    type: number
    sql: ${TABLE}.customer_orders ;;
  }
}













view: ecomm_customer_new_returning{
  sql_table_name: `chb-prod-data-cust.prod_ecomm_cust.ecomm_orders_w_first_txn_date`
    ;;

  dimension: pk {
    hidden:yes
    type:  string
    primary_key:  yes
    sql:  concat(${brand}, ${order_id}) ;;
  }

  dimension: brand {
    label: "Brand"
    hidden: yes
    type: string
    sql: ${TABLE}.brand ;;
  }

  dimension: customer_email {
    label: "Customer Email"
    hidden: yes
    type: string
    sql: ${TABLE}.customer_email ;;
  }

  dimension: customer_key {
    hidden: yes
    type: string
    sql: ${TABLE}.customer_key ;;
  }


  dimension: customer_type {
    label: "Customer Type"
    hidden: yes
    type: string
    sql: ${TABLE}.customer_type ;;
  }

  dimension: order_id {
    label: "Order ID"
    hidden: yes
    type: string
    sql: ${TABLE}.order_id ;;
  }



## order_datetime_local is corrected timestamp
  dimension_group: order_datetime_local {
    label: "Order Datetime"
    hidden: yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year

    ]
    sql: ${TABLE}.order_datetime_local ;;



  }

  dimension_group: order_date_local {
    label: "Order"
    hidden: yes
    type: time
    datatype: date
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year

    ]
    sql: ${TABLE}.order_date_local ;;
  }

  dimension: is_first_transaction {
    hidden: yes
    type: yesno
    sql: ${TABLE}.is_first_transaction ;;
  }

  measure: total_cust_count {
    label: "Count Total Customers"
    hidden: yes
    type: count_distinct
    sql: ${customer_key} ;;
  }


  measure: returning_rate {
    label: "Returning Rate"
    type: number
    hidden: yes
    value_format: "0.00\%"
    sql: (${ret_cust_count}/${total_cust_count})*100 ;;
  }

  measure: recruitment_rate {
    label: "Recruitment Rate"
    type: number
    hidden: yes
    value_format: "0.00\%"
    sql: (${new_cust_count}/${total_cust_count})*100 ;;
  }

  measure: cumulative_cust_count {
    label: "Cumul. Cust Count"
    type: running_total
    hidden: yes
    sql: ${new_cust_count} ;;
  }

  ####depricated measures
  measure: ret_cust_count {
    hidden: yes
    label: "Returning Customer Count"
    type: number

    sql: ${total_cust_count}-${new_cust_count};;
  }

  measure: new_cust_count {
    hidden: yes
    label: "New Customer Count"
    type: count_distinct
    filters: {
      field: customer_type
      value: "New"
    }
    sql: ${customer_key} ;;
  }

  measure: count_uniquecustomer {
    label: "Count of Unique Customers"
    hidden: yes
    type: count_distinct
    sql: ${customer_key} ;;
  }

  measure:  avg_spend{
    label: "Average Spend"
    value_format_name: usd
    hidden: yes
    type: number
    sql: sum(${TABLE}.gross_net_sales_usd_total)/${count_uniquecustomer} ;;
  }


}

view: ecomm_customer_avgtime_orders_brands_table {
  view_label: "Ecomm Customer New Returning"
  derived_table: {
    sql:
      SELECT
        ROW_NUMBER() OVER() as row_num_pk,
        brand,
        count(distinct customer_key)  AS brand_unique_customers,
        AVG(time_bet_purchases) AS brand_avg_time_bet_purch,
      FROM
        (SELECT
            brand,
            customer_key  AS customer_key,
            case when count(order_id)>1 then (date_diff(Max(order_date_local),Min(order_date_local),day)) / (NULLIF((count(order_id) - 1),0)) else 0 end AS time_bet_purchases
        FROM `chb-prod-data-cust.prod_ecomm_cust.ecomm_orders_w_first_txn_date`
        GROUP BY  customer_key,first_order_datetime_utc,brand)
      GROUP BY brand
      ORDER BY row_num_pk;;
  }

  dimension: row_num_pk{primary_key:yes hidden:yes}
  dimension: brand {hidden:yes}

  measure: average_time_between_purchases {
    #   view_label: "Ecomm Customer New Returning"
    label: "Average Time between Purchases"
    value_format: "0.00"
    type: min
    sql: ${TABLE}.brand_avg_time_bet_purch ;;}

}
