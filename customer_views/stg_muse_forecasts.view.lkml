view: stg_muse_forecasts {
  view_label: "Forecast"
  sql_table_name: `chb-prod-data-cust.prod_MUSE_ControlTower.stg_muse_forecasts`
    ;;

  dimension: brand {
    type: string
    sql: ${TABLE}.brand ;;
  }

  dimension: cal_month {
    label: "Period"
    value_format: "0"
    type: number
    sql: ${TABLE}.cal_month ;;
  }

  dimension: forecast_type {
    type: string
    sql: ${TABLE}.forecast_type ;;
  }

  dimension: forecast_value {
    type: number
    sql: ${TABLE}.forecast_value ;;
  }


  dimension: market {
    type: string
    sql: ${TABLE}.market ;;
  }

  dimension: store_code {
    type: number
    value_format: "0"
    sql: ${TABLE}.store_code ;;
  }

  dimension: store_location {
    type: string
    sql: ${TABLE}.store_location ;;
  }

  dimension: verical {
    type: string
    sql: ${TABLE}.verical ;;
  }


  measure: sum_forecast_value {
    type: sum_distinct
    sql_distinct_key: CONCAT(CAST(${TABLE}.cal_month AS STRING),CAST(${TABLE}.store_code AS STRING))  ;;
    label: "Forecast (SUM)"
    description: "Sum of forecasted figures"
    view_label: "Forecast"
    value_format: "#,##0"
    sql: ${forecast_value} ;;
  }

}



view: muse_calendar {
  view_label: "Muse Calendar"
  sql_table_name: `chb-prod-data-cust.prod_MUSE_ControlTower.muse_calendar`
    ;;


  dimension: pk {
    type: string
    primary_key: yes
    hidden: yes
    sql: ${TABLE}.pk ;;
  }

  dimension: calendar_enrolling_location_id {
    type: string
    sql: ${TABLE}.enrolling_location_id ;;
  }

  dimension: cal_month {
    value_format: "0"
    type: number
    sql: ${TABLE}.cal_month ;;
  }

  dimension_group: transaction_date {
    view_label: "MUSE Transactions Table"
    group_label: "Transaction Dimensions"
    label: "Transaction Date"
    type: time
    hidden: no
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.transaction_date ;;
  }


  # frs comparison for storeid and transaction_date
  dimension: frs_items_count {
    view_label: "FRS Transactions"
    label: "Items count"
    hidden: yes
    type:  number
    sql: ${TABLE}.items_count ;;
  }

  measure: frs_items_count1 {
    label: "Items Count"
    view_label: "FRS Transactions"
    type: sum_distinct
    sql_distinct_key: ${pk} ;;
    sql: ${frs_items_count} ;;
  }

  dimension: frs_transaction_count {
    view_label: "FRS Transactions"
    label: "Transaction count"
    hidden: yes
    type:  number
    sql: ${TABLE}.transaction_count ;;
  }

  measure: frs_transaction_count1 {
    label: "Transaction Count"
    view_label: "FRS Transactions"
    type: sum_distinct
    sql_distinct_key: ${pk} ;;
    sql: ${frs_transaction_count} ;;
  }

  dimension: frs_amount_local {
    view_label: "FRS Transactions"
    label: "Amount Local"
    hidden: yes
    type:  number
    sql: ${TABLE}.amount_local ;;
  }

  measure: frs_amount_local1 {
    label: "Amount Local"
    view_label: "FRS Transactions"
    type: sum_distinct
    sql_distinct_key: ${pk} ;;
    value_format: "#,##0;(#.00)"
    sql: ${frs_amount_local} ;;
  }

  dimension: frs_amount_usd {
    view_label: "FRS Transactions"
    label: "Amount USD"
    hidden: yes
    type:  number
    sql: ${TABLE}.amount_usd ;;
  }

  measure: frs_amount_usd1 {
    label: "Amount USD"
    view_label: "FRS Transactions"
    type: sum_distinct
    sql_distinct_key: ${pk} ;;
    value_format: "$#,##0;($#.00)"
    sql: ${frs_amount_usd} ;;
  }

}
