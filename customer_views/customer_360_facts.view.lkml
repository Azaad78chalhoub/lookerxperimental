view: customer_360_group_facts {
  view_label: "Customers"
  derived_table: {
    explore_source: base_customers_factretailsales {
      column: customer_unique_id {}
      column: transaction_count {field:base_customers_factretailsales.transaction_count}
      column: sales_amount_usd {field:base_customers_factretailsales.Sales_Amount_USD}
      column: group_transacted_customer_count { field: base_customers_distinct.group_transacted_customer_count}
      column: count_brands { field: locations.count_brands }
      column: first_date {field:group_lifetime_aggregations.first_date}
      column: count_new_cust_group {field:base_customers_factretailsales.count_new_cust_group}


      bind_filters: {from_field: base_customers_factretailsales.atr_trandate_date
        to_field: base_customers_factretailsales.atr_trandate_date}
      bind_filters: {from_field: base_customers_factretailsales.atr_trandate_month
        to_field: base_customers_factretailsales.atr_trandate_month}
      bind_filters: {from_field: base_customers_factretailsales.atr_trandate_quarter
        to_field: base_customers_factretailsales.atr_trandate_quarter}
      bind_filters: {from_field: base_customers_factretailsales.atr_trandate_year
        to_field: base_customers_factretailsales.atr_trandate_year}
      bind_filters: {from_field: base_customers_factretailsales.atr_trandate_week
        to_field: base_customers_factretailsales.atr_trandate_week}
      bind_filters: {from_field: locations.district_name
        to_field: locations.district_name}
      bind_filters: {from_field: base_customers_factretailsales.include_tax_select
        to_field: base_customers_factretailsales.include_tax_select }

    }
  }


  dimension: customer_unique_id  {
    hidden: yes
    primary_key: yes

  }


  dimension: count_new_cust_group {
    hidden: yes
  }

  dimension: new_returning_cust_group {
    view_label: "Customers"
    group_label: "New vs Returning"
    description: "New or Returning to Group"
    type: string
    sql:  case when ${count_new_cust_group}=1 then "New" else "Returning" end;;
  }





  dimension: sales_amount_usd {
    hidden: yes
  }

  dimension: customer_lifetime_group_spend {

    group_label: "Customer Dimensions"
    label: "Group Lifetime Spend"
    description: "Total customer lifetime spend across all brands (not filtered by date).  Includes tax by default unless exclude tax selected in sales
    . Can be used to segment"
    type: number
    value_format: "$0"
    sql:    ${sales_amount_usd};;
  }

  measure: sum_group_lifetime_spend {
    label: "TEST Group Average Lifetime Spend"
    group_label: "Group Customer Measures"
    hidden: yes
    type: sum_distinct
    sql_distinct_key: ${customer_unique_id} ;;
    value_format: "$0"
    sql: ${TABLE}.sales_amount_usd ;;
  }



  measure: ave_group_lifetime_spend {
    label: "Group Average Lifetime Spend"
    group_label: "Group Customer Measures"
    type: average_distinct
    sql_distinct_key: ${customer_unique_id} ;;
    value_format: "$0"
    sql: ${customer_lifetime_group_spend} ;;
  }

  dimension: transaction_count {
    hidden: yes
    group_label: "Customer Dimensions"
    label:"Customer Frequency"
    type: number
    description: "Frequency is the number of purchases made by a customer over a given period of time"
  }

  dimension: first_date {
    hidden: yes
  }

  dimension: group_frequency_range_sort {

    hidden: yes

    type: number
    sql: CASE
          WHEN ${transaction_count} <= 1 THEN 1
          WHEN ${transaction_count} <= 3 THEN 2
          WHEN ${transaction_count} <= 5 THEN 3
          WHEN ${transaction_count} <= 7 THEN 4
          WHEN ${transaction_count}  <= 10 THEN 5
          WHEN ${transaction_count}  > 10 THEN 6
          ELSE null
          END;;
  }

  dimension: group_frequency_range {
    order_by_field: group_frequency_range_sort
    view_label: "Customers"
    group_label: "Customer Dimensions"
    label: "Group Frequency Range"
    hidden: no
    description: "Cross Brand Frequency Range for RFM analysis"
    type: string
    sql: CASE
          WHEN ${transaction_count} <= 1 THEN "1"
          WHEN ${transaction_count} <= 3 THEN "2-3"
          WHEN ${transaction_count} <= 5 THEN "4-5"
          WHEN ${transaction_count} <= 7 THEN "6-7"
          WHEN ${transaction_count}  <= 10 THEN "8-10"
          WHEN ${transaction_count}  > 10 THEN ">10"
          ELSE null
          END;;
  }








}




view: customer_360_group_facts_v2 {
  view_label: "Customers"
  derived_table: {
    explore_source: base_customers_factretailsales_v2 {
      column: customer_unique_id {}
      column: transaction_count {field:base_customers_factretailsales_v2.transaction_count}
      column: sales_amount_usd {field:base_customers_factretailsales_v2.Sales_Amount_USD}
      column: group_transacted_customer_count { field: base_customers_distinct_v2.group_transacted_customer_count}
      column: count_brands { field: locations.count_brands }
      column: first_date {field:group_lifetime_aggregations.first_date}
      column: count_new_cust_group {field:base_customers_factretailsales_v2.count_new_cust_group}


      bind_filters: {from_field: base_customers_factretailsales_v2.atr_trandate_date
        to_field: base_customers_factretailsales_v2.atr_trandate_date}
      bind_filters: {from_field: base_customers_factretailsales_v2.atr_trandate_month
        to_field: base_customers_factretailsales_v2.atr_trandate_month}
      bind_filters: {from_field: base_customers_factretailsales_v2.atr_trandate_quarter
        to_field: base_customers_factretailsales_v2.atr_trandate_quarter}
      bind_filters: {from_field: base_customers_factretailsales_v2.atr_trandate_year
        to_field: base_customers_factretailsales_v2.atr_trandate_year}
      bind_filters: {from_field: base_customers_factretailsales_v2.atr_trandate_week
        to_field: base_customers_factretailsales_v2.atr_trandate_week}
      bind_filters: {from_field: locations.district_name
        to_field: locations.district_name}
      bind_filters: {from_field: base_customers_factretailsales_v2.include_tax_select
        to_field: base_customers_factretailsales_v2.include_tax_select }

    }
  }


  dimension: customer_unique_id  {
    hidden: yes
    primary_key: yes

  }


  dimension: count_new_cust_group {
    hidden: yes
  }

  dimension: new_returning_cust_group {
    view_label: "Customers"
    group_label: "New vs Returning"
    description: "New or Returning to Group"
    type: string
    sql:  case when ${count_new_cust_group}=1 then "New" else "Returning" end;;
  }





  dimension: sales_amount_usd {
    hidden: yes
  }

  dimension: customer_lifetime_group_spend {

    group_label: "Customer Dimensions"
    label: "Group Lifetime Spend"
    description: "Total customer lifetime spend across all brands (not filtered by date).  Includes tax by default unless exclude tax selected in sales
    . Can be used to segment"
    type: number
    value_format: "$0"
    sql:    ${sales_amount_usd};;
  }

  measure: sum_group_lifetime_spend {
    label: "TEST Group Average Lifetime Spend"
    group_label: "Group Customer Measures"
    hidden: yes
    type: sum_distinct
    sql_distinct_key: ${customer_unique_id} ;;
    value_format: "$0"
    sql: ${TABLE}.sales_amount_usd ;;
  }



  measure: ave_group_lifetime_spend {
    label: "Group Average Lifetime Spend"
    group_label: "Group Customer Measures"
    type: average_distinct
    sql_distinct_key: ${customer_unique_id} ;;
    value_format: "$0"
    sql: ${customer_lifetime_group_spend} ;;
  }

  dimension: transaction_count {
    hidden: yes
    group_label: "Customer Dimensions"
    label:"Customer Frequency"
    type: number
    description: "Frequency is the number of purchases made by a customer over a given period of time"
  }

  dimension: first_date {
    hidden: yes
  }

  dimension: group_frequency_range_sort {

    hidden: yes

    type: number
    sql: CASE
          WHEN ${transaction_count} <= 1 THEN 1
          WHEN ${transaction_count} <= 3 THEN 2
          WHEN ${transaction_count} <= 5 THEN 3
          WHEN ${transaction_count} <= 7 THEN 4
          WHEN ${transaction_count}  <= 10 THEN 5
          WHEN ${transaction_count}  > 10 THEN 6
          ELSE null
          END;;
  }

  dimension: group_frequency_range {
    order_by_field: group_frequency_range_sort
    view_label: "Customers"
    group_label: "Customer Dimensions"
    label: "Group Frequency Range"
    hidden: no
    description: "Cross Brand Frequency Range for RFM analysis"
    type: string
    sql: CASE
          WHEN ${transaction_count} <= 1 THEN "1"
          WHEN ${transaction_count} <= 3 THEN "2-3"
          WHEN ${transaction_count} <= 5 THEN "4-5"
          WHEN ${transaction_count} <= 7 THEN "6-7"
          WHEN ${transaction_count}  <= 10 THEN "8-10"
          WHEN ${transaction_count}  > 10 THEN ">10"
          ELSE null
          END;;
  }








}







view: customer_360_facts_v2 {
  view_label: "Customers"
  derived_table: {
    explore_source: base_customers_factretailsales_v2 {
      column: brand_customer_unique_id {}
      column: transaction_count {field:base_customers_factretailsales_v2.transaction_count}
      column: sales_amount_usd {field:base_customers_factretailsales_v2.Sales_Amount_USD}
      column: group_transacted_customer_count { field: base_customers_distinct_v2.group_transacted_customer_count}
      column: count_brands { field: locations.count_brands }
      column: first_date {field:group_lifetime_aggregations.first_date}
      column: brand_recency {field:brand_lifetime_aggregations.recency}
      column: count_new_cust_brand {field:base_customers_factretailsales_v2.count_new_cust_brand}
      column: count_returning_cust_brand {field:base_customers_factretailsales_v2.count_returning_cust_brand}
      column: proportion_of_discount_sales {field:base_customers_factretailsales_v2.proportion_of_discount_sales}
      bind_filters: {from_field: base_customers_factretailsales_v2.atr_trandate_date
        to_field: base_customers_factretailsales_v2.atr_trandate_date}
      bind_filters: {from_field: base_customers_factretailsales_v2.atr_trandate_month
        to_field: base_customers_factretailsales_v2.atr_trandate_month}
      bind_filters: {from_field: base_customers_factretailsales_v2.atr_trandate_quarter
        to_field: base_customers_factretailsales_v2.atr_trandate_quarter}
      bind_filters: {from_field: base_customers_factretailsales_v2.atr_trandate_year
        to_field: base_customers_factretailsales_v2.atr_trandate_year}
      bind_filters: {from_field: base_customers_factretailsales_v2.atr_trandate_week
        to_field: base_customers_factretailsales_v2.atr_trandate_week}
      bind_filters: {from_field: locations.district_name
        to_field: locations.district_name}

    }
  }





  dimension: brand_customer_unique_id  {
    hidden: yes
    primary_key: yes
    label: "Customer brand Hash Key"
  }

  dimension: count_new_cust_brand {
    hidden: yes
  }

  dimension: count_returning_cust_brand {
    hidden: yes
  }

  dimension: new_returning_cust_brand {
    view_label: "Customers"
    group_label: "New vs Returning"
    description: "New or Returning to Brand"
    type: string
    sql:  case when ${count_new_cust_brand}=1 then "New" else "Returning" end;;
  }

  dimension: returning_new_cust_brand {
    view_label: "Customers"
    group_label: "New vs Returning"
    description: "Returning or new to Brand"
    type: string
    sql:  case when ${count_returning_cust_brand}=1 then "Returning" else "New" end;;
  }

  dimension: transaction_count {

    group_label: "Customer Dimensions"
    label:"Brand Frequency"
    type: number
    description: "Frequency is the number of purchases made by a customer over a given period of time"
  }

  dimension: brand_transaction_range {
    view_label: "Customers"
    group_label: "Customer Dimensions"
    label: "Brand Transactions Range"
    hidden: no
    description: "Brand's clustered number of transactions"
    type: string
    sql: CASE
            WHEN ${transaction_count} <= 10 THEN CAST(${transaction_count} AS STRING)
            WHEN ${transaction_count} > 10 THEN "10+"
            ELSE NULL END;;
  }

  dimension: first_date {
    hidden: yes
  }

  dimension: frequency_range_sort {

    hidden: yes

    type: number
    sql: CASE
          WHEN ${transaction_count} <= 1 THEN 1
          WHEN ${transaction_count} <= 3 THEN 2
          WHEN ${transaction_count} <= 5 THEN 3
          WHEN ${transaction_count} <= 7 THEN 4
          WHEN ${transaction_count}  <= 10 THEN 5
          WHEN ${transaction_count}  > 10 THEN 6
          ELSE null
          END;;
  }

  dimension: brand_frequency_range {
    order_by_field: frequency_range_sort
    view_label: "Customers"
    group_label: "Customer Dimensions"
    label: "Brand Frequency Range."
    hidden: no
    description: "Frequency Range for RFM analysis"
    type: string
    sql: CASE
          WHEN ${transaction_count} <= 1 THEN "1"
          WHEN ${transaction_count} <= 3 THEN "2-3"
          WHEN ${transaction_count} <= 5 THEN "4-5"
          WHEN ${transaction_count} <= 7 THEN "6-7"
          WHEN ${transaction_count}  <= 10 THEN "8-10"
          WHEN ${transaction_count}  > 10 THEN ">10"
          ELSE null
          END;;
  }

  dimension: brand_recency  {
    hidden: yes


  }

  dimension:sales_amount_usd  {
    hidden: yes
    group_label: "Customer Dimensions"
    value_format: "$#,##0"
    label: "Customer Spend"
    description: "Net sales for a given customer over a given period of time. Can use as filter"
    type: number
  }

  # # dimension: lvl_vrsc_spend_range_sort {

  # #   hidden: yes

  # #   type: number
  # #   sql: CASE
  # #         WHEN ${sales_amount_usd} < 2700 THEN 3
  # #         WHEN ${sales_amount_usd} <=5400  THEN 2
  # #         WHEN ${sales_amount_usd} > 5400 THEN 1
  # #         ELSE null
  # #         END;;
  # # }

  # dimension:  lvl_vrsc_clientel_segment {
  #   order_by_field: lvl_vrsc_spend_range_sort
  #   view_label: "Customers"
  #   group_label: "Customer Dimensions"
  #   label: "LVL & VRSC Clienteling segments"
  #   hidden: no
  #   description: "VIP spend USD >5.4K Potential >=2.7K"
  #   type: string
  #   sql: CASE
  #         WHEN ${sales_amount_usd} < 2700 THEN ""
  #         WHEN ${sales_amount_usd} <=5400 THEN "Potential VIP"
  #         WHEN ${sales_amount_usd} > 5400 THEN "VIP"
  #         ELSE null
  #         END;;
  # }

  # dimension: tb_spend_range_sort {
  #   hidden: yes

  #   type: number
  #   sql: CASE
  #         WHEN ${sales_amount_usd} < 2700 THEN 3
  #         WHEN ${sales_amount_usd} <=4000  THEN 2
  #         WHEN ${sales_amount_usd} > 4000 THEN 1
  #         ELSE null
  #         END;;
  # }

  # dimension:  tb_clientel_segment {
  #   order_by_field: tb_spend_range_sort
  #   view_label: "Customers"
  #   group_label: "Customer Dimensions"
  #   label: "TBurch Clienteling segments"
  #   hidden: no
  #   description: "VIP spend USD >4K Potential >=2.7K"
  #   type: string
  #   sql: CASE
  #         WHEN ${sales_amount_usd} < 2700 THEN ""
  #         WHEN ${sales_amount_usd} <=4000 THEN "Potential VIP"
  #         WHEN ${sales_amount_usd} > 4000 THEN "VIP"
  #         ELSE null
  #         END;;
  # }

  # dimension: dg_spend_range_sort {
  #   hidden: yes

  #   type: number
  #   sql: CASE
  #         WHEN ${sales_amount_usd} < 10000 THEN 3
  #         WHEN ${sales_amount_usd} <=20000  THEN 2
  #         WHEN ${sales_amount_usd} > 20000 THEN 1
  #         ELSE null
  #         END;;
  # }

  # dimension:  dg_clientel_segment {
  #   order_by_field: dg_spend_range_sort
  #   view_label: "Customers"
  #   group_label: "Customer Dimensions"
  #   label: "D&G Clienteling segments"
  #   hidden: no
  #   description: "VIP spend USD >20K Potential >=10K "
  #   type: string
  #   sql: CASE
  #         WHEN ${sales_amount_usd} < 10000 THEN ""
  #         WHEN ${sales_amount_usd} <=20000 THEN "Potential VIP"
  #         WHEN ${sales_amount_usd} > 20000 THEN "VIP"
  #         ELSE null
  #         END;;
  # }

  # dimension: fcs_spend_range_sort {
  #   hidden: yes

  #   type: number
  #   sql: CASE
  #         WHEN ${sales_amount_usd} < 2700 THEN 4
  #         WHEN ${sales_amount_usd} <8000 THEN 3
  #         WHEN ${sales_amount_usd} <= 15000 THEN 2
  #         WHEN ${sales_amount_usd} > 15000 THEN 1
  #         ELSE null
  #         END;;
  # }

  # dimension:  fcs_clientel_segment {
  #   order_by_field: fcs_spend_range_sort
  #   view_label: "Customers"
  #   group_label: "Customer Dimensions"
  #   label: "FACES Clienteling segments"
  #   hidden: no
  #   description: "VIP spend USD 10-15K Potential VIP >=5K "
  #   type: string
  #   sql: CASE
  #         WHEN ${sales_amount_usd} < 2700 THEN ""
  #         WHEN ${sales_amount_usd} < 8000 THEN "Potential VIP"
  #         WHEN ${sales_amount_usd} <= 15000 THEN "VIP"
  #         WHEN ${sales_amount_usd} > 15000 THEN ""
  #         ELSE null
  #         END;;
  # }

  # dimension: swr_spend_range_sort {
  #   hidden: yes

  #   type: number
  #   sql: CASE
  #         WHEN ${sales_amount_usd} < 500 THEN 3
  #         WHEN ${sales_amount_usd} <= 2700 THEN 2
  #         WHEN ${sales_amount_usd} > 2700 THEN 1
  #         ELSE null
  #         END;;
  # }

  # dimension:  swr_clientel_segment {
  #   order_by_field: swr_spend_range_sort
  #   view_label: "Customers"
  #   group_label: "Customer Dimensions"
  #   label: "SWR Clienteling segments"
  #   hidden: no
  #   description: "VIP spend USD > 2.7 + Potential spend 500-2.7K"
  #   type: string
  #   sql: CASE
  #         WHEN ${sales_amount_usd} <  500 THEN ""
  #         WHEN ${sales_amount_usd} <= 2700 THEN "Potential VIP"
  #         WHEN ${sales_amount_usd} >  2700 THEN "VIP"
  #         ELSE null
  #         END;;
  # }

  # dimension: ch_spend_range_sort {
  #   hidden: yes

  #   type: number
  #   sql: CASE
  #         WHEN ${sales_amount_usd} < 5000 THEN 3
  #         WHEN ${sales_amount_usd} <=10000 THEN 2
  #         WHEN ${sales_amount_usd} > 10000 THEN 1
  #         ELSE null
  #         END;;
  # }

  # dimension:  ch_clientel_segment {
  #   order_by_field: ch_spend_range_sort
  #   view_label: "Customers"
  #   group_label: "Customer Dimensions"
  #   label: "C.HERRERA Clienteling segments"
  #   hidden: no
  #   description: "VIP spend USD >10K Potential VIP >=5K + Muse top 2 tiers"
  #   type: string
  #   sql: CASE
  #         WHEN ${sales_amount_usd} < 5000 THEN ""
  #         WHEN ${sales_amount_usd} <= 10000 THEN "Potential VIP"
  #         WHEN ${sales_amount_usd} > 10000 THEN "VIP"
  #         ELSE null
  #         END;;
  # }

  dimension: count_brands {
    hidden: yes
    type: number
  }
  dimension: proportion_of_discount_sales {
    label: "Brand Percentage Markdown Transactions"
    group_label: "Customer Dimensions"
    description: "In the selected period the percentage of markdown transactions made by the customer. Brand specific."
    type: number
    value_format: "0%"
  }



  dimension: discount_shopper_Type {
    group_label: "Customer Dimensions"
    label: "Brand Discount Shopper Type (Beta)"
    description: "In the selected period is the customer shopping markdown items. Brand specific."
    type: string
    sql: case when proportion_of_discount_sales=0 then "Full Price Only"
          when proportion_of_discount_sales=1 then "Discount Only"
          else "Full Price & Markdown" end ;;


    }

##this measue doesn't really work
    measure: customer_average_spend {
      hidden: yes
      description: "Average spend in the period of analysis"
      type: average
      value_format: "$#,##0"
      sql:${sales_amount_usd} ;;
    }

##this measue doesn't really work as bk_customer at brand level
    measure: ave_brands_shopped_in_period {
      hidden: yes
      label: "Avg Num Brands Shopped"
      description: "Average number of brands shopped in selected perdiod"
      type: average
      sql: ${count_brands} ;;



    }



    measure: monetary_25th {
      group_label:"Brand RFM Percentiles"
      type: percentile

      percentile: 25
      value_format: "$0"
      sql: ${TABLE}.sales_amount_usd ;;
    }

    measure: monetary_median {
      group_label:"Brand RFM Percentiles"
      type: percentile
      percentile: 50
      value_format: "$0"
      sql: ${TABLE}.sales_amount_usd ;;
    }

    measure: monetary_75th {
      group_label:"Brand RFM Percentiles"
      type: percentile
      percentile: 75
      value_format: "$0"
      sql: ${TABLE}.sales_amount_usd ;;
    }




    measure: frequency_25th {
      group_label:"Brand RFM Percentiles"
      type: percentile

      percentile: 25
      value_format: "0"
      sql: ${TABLE}.transaction_count ;;
    }

    measure: frequency_median {
      group_label:"Brand RFM Percentiles"
      type: percentile
      percentile: 50
      value_format: "0"
      sql: ${TABLE}.transaction_count ;;
    }

    measure: frequency_75th {
      group_label:"Brand RFM Percentiles"
      type: percentile
      percentile: 75
      value_format: "0"
      sql: ${TABLE}.transaction_count ;;
    }


    measure: recency_25th {
      group_label:"Brand RFM Percentiles"
      type: percentile

      percentile: 25
      value_format: "0"
      sql: ${TABLE}.brand_recency ;;
    }

    measure: recency_median {
      group_label:"Brand RFM Percentiles"
      type: percentile
      percentile: 50
      value_format: "0"
      sql: ${TABLE}.brand_recency ;;
    }

    measure: recency_75th {
      group_label:"Brand RFM Percentiles"
      type: percentile
      percentile: 75
      value_format: "0"
      sql: ${TABLE}.brand_recency ;;
    }





  }








view: customer_360_facts {
    view_label: "Customers"
    derived_table: {
      explore_source: base_customers_factretailsales {
        column: brand_customer_unique_id {}
        column: transaction_count {field:base_customers_factretailsales.transaction_count}
        column: sales_amount_usd {field:base_customers_factretailsales.Sales_Amount_USD}
        column: group_transacted_customer_count { field: base_customers_distinct.group_transacted_customer_count}
        column: count_brands { field: locations.count_brands }
        column: first_date {field:group_lifetime_aggregations.first_date}
        column: brand_recency {field:brand_lifetime_aggregations.recency}
        column: count_new_cust_brand {field:base_customers_factretailsales.count_new_cust_brand}
        column: count_returning_cust_brand {field:base_customers_factretailsales.count_returning_cust_brand}
        column: proportion_of_discount_sales {field:base_customers_factretailsales.proportion_of_discount_sales}
        bind_filters: {from_field: base_customers_factretailsales.atr_trandate_date
          to_field: base_customers_factretailsales.atr_trandate_date}
        bind_filters: {from_field: base_customers_factretailsales.atr_trandate_month
          to_field: base_customers_factretailsales.atr_trandate_month}
        bind_filters: {from_field: base_customers_factretailsales.atr_trandate_quarter
          to_field: base_customers_factretailsales.atr_trandate_quarter}
        bind_filters: {from_field: base_customers_factretailsales.atr_trandate_year
          to_field: base_customers_factretailsales.atr_trandate_year}
        bind_filters: {from_field: base_customers_factretailsales.atr_trandate_week
          to_field: base_customers_factretailsales.atr_trandate_week}
        bind_filters: {from_field: locations.district_name
          to_field: locations.district_name}

      }
    }





  dimension: brand_customer_unique_id  {
    hidden: yes
    primary_key: yes
    label: "Customer brand Hash Key"
  }

dimension: count_new_cust_brand {
  hidden: yes
}

  dimension: count_returning_cust_brand {
    hidden: yes
}

dimension: new_returning_cust_brand {
  view_label: "Customers"
  group_label: "New vs Returning"
  description: "New or Returning to Brand"
  type: string
  sql:  case when ${count_new_cust_brand}=1 then "New" else "Returning" end;;
}

  dimension: returning_new_cust_brand {
    view_label: "Customers"
    group_label: "New vs Returning"
    description: "Returning or new to Brand"
    type: string
    sql:  case when ${count_returning_cust_brand}=1 then "Returning" else "New" end;;
  }

  dimension: transaction_count {

    group_label: "Customer Dimensions"
    label:"Brand Frequency"
    type: number
    description: "Frequency is the number of purchases made by a customer over a given period of time"
  }

  dimension: brand_transaction_range {
    view_label: "Customers"
    group_label: "Customer Dimensions"
    label: "Brand Transactions Range"
    hidden: no
    description: "Brand's clustered number of transactions"
    type: string
    sql: CASE
            WHEN ${transaction_count} <= 10 THEN CAST(${transaction_count} AS STRING)
            WHEN ${transaction_count} > 10 THEN "10+"
            ELSE NULL END;;
  }

    dimension: first_date {
      hidden: yes
    }

  dimension: frequency_range_sort {

    hidden: yes

    type: number
    sql: CASE
          WHEN ${transaction_count} <= 1 THEN 1
          WHEN ${transaction_count} <= 3 THEN 2
          WHEN ${transaction_count} <= 5 THEN 3
          WHEN ${transaction_count} <= 7 THEN 4
          WHEN ${transaction_count}  <= 10 THEN 5
          WHEN ${transaction_count}  > 10 THEN 6
          ELSE null
          END;;
  }

  dimension: brand_frequency_range {
    order_by_field: frequency_range_sort
    view_label: "Customers"
    group_label: "Customer Dimensions"
    label: "Brand Frequency Range."
    hidden: no
    description: "Frequency Range for RFM analysis"
    type: string
    sql: CASE
          WHEN ${transaction_count} <= 1 THEN "1"
          WHEN ${transaction_count} <= 3 THEN "2-3"
          WHEN ${transaction_count} <= 5 THEN "4-5"
          WHEN ${transaction_count} <= 7 THEN "6-7"
          WHEN ${transaction_count}  <= 10 THEN "8-10"
          WHEN ${transaction_count}  > 10 THEN ">10"
          ELSE null
          END;;
  }

      dimension: brand_recency  {
        hidden: yes


      }

    dimension:sales_amount_usd  {
      hidden: yes
      group_label: "Customer Dimensions"
      value_format: "$#,##0"
      label: "Customer Spend"
      description: "Net sales for a given customer over a given period of time. Can use as filter"
      type: number
    }

  # dimension: lvl_vrsc_spend_range_sort {

  #   hidden: yes

  #   type: number
  #   sql: CASE
  #         WHEN ${sales_amount_usd} < 2700 THEN 3
  #         WHEN ${sales_amount_usd} <=5400  THEN 2
  #         WHEN ${sales_amount_usd} > 5400 THEN 1
  #         ELSE null
  #         END;;
  # }

  # dimension:  lvl_vrsc_clientel_segment {
  #   order_by_field: lvl_vrsc_spend_range_sort
  #   view_label: "Customers"
  #   group_label: "Customer Dimensions"
  #   label: "LVL & VRSC Clienteling segments"
  #   hidden: no
  #   description: "VIP spend USD >5.4K Potential >=2.7K"
  #   type: string
  #   sql: CASE
  #         WHEN ${sales_amount_usd} < 2700 THEN ""
  #         WHEN ${sales_amount_usd} <=5400 THEN "Potential VIP"
  #         WHEN ${sales_amount_usd} > 5400 THEN "VIP"
  #         ELSE null
  #         END;;
  # }

  # dimension: tb_spend_range_sort {
  #   hidden: yes

  #   type: number
  #   sql: CASE
  #         WHEN ${sales_amount_usd} < 2700 THEN 3
  #         WHEN ${sales_amount_usd} <=4000  THEN 2
  #         WHEN ${sales_amount_usd} > 4000 THEN 1
  #         ELSE null
  #         END;;
  # }

  # dimension:  tb_clientel_segment {
  #   order_by_field: tb_spend_range_sort
  #   view_label: "Customers"
  #   group_label: "Customer Dimensions"
  #   label: "TBurch Clienteling segments"
  #   hidden: no
  #   description: "VIP spend USD >4K Potential >=2.7K"
  #   type: string
  #   sql: CASE
  #         WHEN ${sales_amount_usd} < 2700 THEN ""
  #         WHEN ${sales_amount_usd} <=4000 THEN "Potential VIP"
  #         WHEN ${sales_amount_usd} > 4000 THEN "VIP"
  #         ELSE null
  #         END;;
  #   }

  #   dimension: dg_spend_range_sort {
  #     hidden: yes

  #     type: number
  #     sql: CASE
  #         WHEN ${sales_amount_usd} < 10000 THEN 3
  #         WHEN ${sales_amount_usd} <=20000  THEN 2
  #         WHEN ${sales_amount_usd} > 20000 THEN 1
  #         ELSE null
  #         END;;
  #   }

  #   dimension:  dg_clientel_segment {
  #     order_by_field: dg_spend_range_sort
  #     view_label: "Customers"
  #     group_label: "Customer Dimensions"
  #     label: "D&G Clienteling segments"
  #     hidden: no
  #     description: "VIP spend USD >20K Potential >=10K "
  #     type: string
  #     sql: CASE
  #         WHEN ${sales_amount_usd} < 10000 THEN ""
  #         WHEN ${sales_amount_usd} <=20000 THEN "Potential VIP"
  #         WHEN ${sales_amount_usd} > 20000 THEN "VIP"
  #         ELSE null
  #         END;;
  #     }

  #     dimension: fcs_spend_range_sort {
  #       hidden: yes

  #       type: number
  #       sql: CASE
  #         WHEN ${sales_amount_usd} < 2700 THEN 4
  #         WHEN ${sales_amount_usd} <8000 THEN 3
  #         WHEN ${sales_amount_usd} <= 15000 THEN 2
  #         WHEN ${sales_amount_usd} > 15000 THEN 1
  #         ELSE null
  #         END;;
  #     }

  #     dimension:  fcs_clientel_segment {
  #       order_by_field: fcs_spend_range_sort
  #       view_label: "Customers"
  #       group_label: "Customer Dimensions"
  #       label: "FACES Clienteling segments"
  #       hidden: no
  #       description: "VIP spend USD 10-15K Potential VIP >=5K "
  #       type: string
  #       sql: CASE
  #         WHEN ${sales_amount_usd} < 2700 THEN ""
  #         WHEN ${sales_amount_usd} < 8000 THEN "Potential VIP"
  #         WHEN ${sales_amount_usd} <= 15000 THEN "VIP"
  #         WHEN ${sales_amount_usd} > 15000 THEN ""
  #         ELSE null
  #         END;;
  # }

  # dimension: swr_spend_range_sort {
  #   hidden: yes

  #   type: number
  #   sql: CASE
  #         WHEN ${sales_amount_usd} < 500 THEN 3
  #         WHEN ${sales_amount_usd} <= 2700 THEN 2
  #         WHEN ${sales_amount_usd} > 2700 THEN 1
  #         ELSE null
  #         END;;
  # }

  # dimension:  swr_clientel_segment {
  #   order_by_field: swr_spend_range_sort
  #   view_label: "Customers"
  #   group_label: "Customer Dimensions"
  #   label: "SWR Clienteling segments"
  #   hidden: no
  #   description: "VIP spend USD > 2.7 + Potential spend 500-2.7K"
  #   type: string
  #   sql: CASE
  #         WHEN ${sales_amount_usd} <  500 THEN ""
  #         WHEN ${sales_amount_usd} <= 2700 THEN "Potential VIP"
  #         WHEN ${sales_amount_usd} >  2700 THEN "VIP"
  #         ELSE null
  #         END;;
  # }

  # dimension: ch_spend_range_sort {
  #   hidden: yes

  #   type: number
  #   sql: CASE
  #         WHEN ${sales_amount_usd} < 5000 THEN 3
  #         WHEN ${sales_amount_usd} <=10000 THEN 2
  #         WHEN ${sales_amount_usd} > 10000 THEN 1
  #         ELSE null
  #         END;;
  # }

  # dimension:  ch_clientel_segment {
  #   order_by_field: ch_spend_range_sort
  #   view_label: "Customers"
  #   group_label: "Customer Dimensions"
  #   label: "C.HERRERA Clienteling segments"
  #   hidden: no
  #   description: "VIP spend USD >10K Potential VIP >=5K + Muse top 2 tiers"
  #   type: string
  #   sql: CASE
  #         WHEN ${sales_amount_usd} < 5000 THEN ""
  #         WHEN ${sales_amount_usd} <= 10000 THEN "Potential VIP"
  #         WHEN ${sales_amount_usd} > 10000 THEN "VIP"
  #         ELSE null
  #         END;;
  # }

        dimension: count_brands {
          hidden: yes
          type: number
        }
  dimension: proportion_of_discount_sales {
    label: "Brand Percentage Markdown Transactions"
group_label: "Customer Dimensions"
    description: "In the selected period the percentage of markdown transactions made by the customer. Brand specific."
    type: number
    value_format: "0%"
  }



  dimension: discount_shopper_Type {
    group_label: "Customer Dimensions"
    label: "Brand Discount Shopper Type (Beta)"
    description: "In the selected period is the customer shopping markdown items. Brand specific."
    type: string
    sql: case when proportion_of_discount_sales=0 then "Full Price Only"
          when proportion_of_discount_sales=1 then "Discount Only"
          else "Full Price & Markdown" end ;;


    }

##this measue doesn't really work
    measure: customer_average_spend {
      hidden: yes
      description: "Average spend in the period of analysis"
      type: average
      value_format: "$#,##0"
      sql:${sales_amount_usd} ;;
    }

##this measue doesn't really work as bk_customer at brand level
    measure: ave_brands_shopped_in_period {
      hidden: yes
      label: "Avg Num Brands Shopped"
      description: "Average number of brands shopped in selected perdiod"
      type: average
      sql: ${count_brands} ;;



    }



    measure: monetary_25th {
      group_label:"Brand RFM Percentiles"
      type: percentile

      percentile: 25
      value_format: "$0"
      sql: ${TABLE}.sales_amount_usd ;;
    }

  measure: monetary_median {
    group_label:"Brand RFM Percentiles"
    type: percentile
    percentile: 50
    value_format: "$0"
    sql: ${TABLE}.sales_amount_usd ;;
  }

  measure: monetary_75th {
    group_label:"Brand RFM Percentiles"
    type: percentile
    percentile: 75
    value_format: "$0"
    sql: ${TABLE}.sales_amount_usd ;;
  }




  measure: frequency_25th {
    group_label:"Brand RFM Percentiles"
    type: percentile

    percentile: 25
    value_format: "0"
    sql: ${TABLE}.transaction_count ;;
  }

  measure: frequency_median {
    group_label:"Brand RFM Percentiles"
    type: percentile
    percentile: 50
    value_format: "0"
    sql: ${TABLE}.transaction_count ;;
  }

  measure: frequency_75th {
    group_label:"Brand RFM Percentiles"
    type: percentile
    percentile: 75
    value_format: "0"
    sql: ${TABLE}.transaction_count ;;
  }


  measure: recency_25th {
    group_label:"Brand RFM Percentiles"
    type: percentile

    percentile: 25
    value_format: "0"
    sql: ${TABLE}.brand_recency ;;
  }

  measure: recency_median {
    group_label:"Brand RFM Percentiles"
    type: percentile
    percentile: 50
    value_format: "0"
    sql: ${TABLE}.brand_recency ;;
  }

  measure: recency_75th {
    group_label:"Brand RFM Percentiles"
    type: percentile
    percentile: 75
    value_format: "0"
    sql: ${TABLE}.brand_recency ;;
  }





  }

































view: customer_360_facts_2nd_order {
  view_label: "Customers"
  derived_table: {
    explore_source: customer_360_facts.SQL_TABLE_NAME {
      column: percentile_25 {}
      column: percentile_50 {}
      column: percentile_75 {}
      column: sales_amount_usd {}
      column: brand_customer_unique_id {}
}
}

  dimension: brand_customer_unique_id  {
    hidden: yes
    primary_key: yes
    label: "Customer brand Hash Key"
  }

dimension: sales_amount_usd {
  hidden: yes
}

dimension: percentile_25{
  hidden: yes
}
  dimension: percentile_50{
    hidden: yes
  }
  dimension: percentile_75{
    hidden: yes
  }

dimension: cust_percetile_bin {
  type: string
  sql: case when ${sales_amount_usd}<=${percentile_25} then "under 25" else "above 25" end ;;
}



}



  view: customer_360_facts_lifetime {
    view_label: "Customers"
    derived_table: {
      explore_source: base_customers_factretailsales {
        column: brand_customer_unique_id {}
        column: transaction_count {field:base_customers_factretailsales.transaction_count}
        column: sales_amount_usd {field:base_customers_factretailsales.Sales_Amount_USD}
        column: group_transacted_customer_count { field: base_customers_distinct.group_transacted_customer_count}
        column: brand_tenure {field: brand_lifetime_aggregations.tenure}
        column: group_tenure {field: group_lifetime_aggregations.tenure}
        column: brand_recency {field: brand_lifetime_aggregations.recency}
        column: group_recency {field: group_lifetime_aggregations.recency}
        column: count_brands { field: locations.count_brands }
        bind_filters: {from_field: locations.district_name
          to_field: locations.district_name}
        bind_filters: {from_field: base_customers_factretailsales.include_tax_select
          to_field: base_customers_factretailsales.include_tax_select }
      filters: [base_customers_factretailsales.is_known_cust_txn: "yes"]
      }
    }
    dimension: brand_customer_unique_id {
      hidden: yes
      primary_key: yes
      label: "Customers Customer Key"
    }

    dimension: brand_tenure {
      hidden: yes
    }

    dimension: group_tenure {
      hidden: yes
    }

    dimension: brand_recency {
      hidden: yes
    }

    dimension: group_recency {
      hidden: yes
    }

dimension: sales_amount_usd {
  hidden: yes

}

    dimension: customer_lifetime_brand_spend {

      group_label: "Customer Dimensions"
      label: "Brand Lifetime Spend"
      description: "Total customer lifetime spend in a brand (not filtered by date).  Includes tax by default unless exclude tax selected in sales
      . Can be used to segment"
      type: number
      value_format: "$0"
      sql:    ${sales_amount_usd};;
    }


    measure: ave_brand_lifetime_spend {
      label: "Brand Average Lifetime Spend"
      group_label: "Brand Customer Measures"
      type: average_distinct
      sql_distinct_key: ${brand_customer_unique_id} ;;
      value_format: "$0"
      sql: ${customer_lifetime_brand_spend} ;;
    }



    dimension: transaction_count {

      group_label: "Customer Dimensions"
      label:"Brand Frequency LifeTime"
      type: number

      description: "How many times a customer shopped at the Brand. Lifetime frequency."
    }


    dimension: frequency_range_sort {

      hidden: yes

      type: number
      sql: CASE
          WHEN ${transaction_count} <= 1 THEN 1
          WHEN ${transaction_count} <= 3 THEN 2
          WHEN ${transaction_count} <= 5 THEN 3
          WHEN ${transaction_count} <= 7 THEN 4
          WHEN ${transaction_count}  <= 10 THEN 5
          WHEN ${transaction_count}  > 10 THEN 6

          END;;
    }

    dimension: brand_frequency_range_lifetime {
      order_by_field: frequency_range_sort
      view_label: "Customers"
      group_label: "Customer Dimensions"
      label: " Brand Frequency Lifetime Range"
      hidden: no
      description: "Brand Lifetime Frequency Range for RFM analysis"
      type: string
      sql: CASE
          WHEN ${transaction_count} <= 1 THEN "1"
          WHEN ${transaction_count} <= 3 THEN "2-3"
          WHEN ${transaction_count} <= 5 THEN "4-5"
          WHEN ${transaction_count} <= 7 THEN "6-7"
          WHEN ${transaction_count}  <= 10 THEN "8-10"
          WHEN ${transaction_count}  > 10 THEN ">10"

          END;;
    }

    dimension: brand_customer_time_between_purchases {
      hidden: yes
      group_label: "Customer Dimensions"
      description: "Average time between purchases for a customer (in Days) in the brand"
      value_format: "0"
      type: number
      sql: (${brand_tenure}-${brand_recency})/
      case when ${transaction_count}=0 or ${transaction_count}=1 then null
      else  ${transaction_count}-1 end;;
    }

    measure: brand_ave_time_between_purchases {
      group_label: "Brand Customer Measures"
      type: average_distinct
      sql_distinct_key: ${brand_customer_unique_id} ;;
      description: "How frequently a customer is making a purchase ? (in Days)"
      value_format: "0"
      sql: ${brand_customer_time_between_purchases} ;;
    }



    dimension: group_customer_time_between_purchases {
      hidden: yes
      group_label: "Customer Dimensions"
      description: "Average time between purchases for a customer (in Days) in the brand"
      value_format: "0"
      type: number
      sql: (${group_tenure}-${group_recency})/
              case when ${transaction_count}=0 or ${transaction_count}=1 then null
              else  ${transaction_count}-1 end;;
    }



  }





view: customer_360_facts_lifetime_v2 {
  view_label: "Customers"
  derived_table: {
    explore_source: base_customers_factretailsales_v2 {
      column: brand_customer_unique_id {}
      column: transaction_count {field:base_customers_factretailsales_v2.transaction_count}
      column: sales_amount_usd {field:base_customers_factretailsales_v2.Sales_Amount_USD}
      column: group_transacted_customer_count { field: base_customers_distinct_v2.group_transacted_customer_count}
      column: brand_tenure {field: brand_lifetime_aggregations.tenure}
      column: group_tenure {field: group_lifetime_aggregations.tenure}
      column: brand_recency {field: brand_lifetime_aggregations.recency}
      column: group_recency {field: group_lifetime_aggregations.recency}
      column: count_brands { field: locations.count_brands }
      bind_filters: {from_field: locations.district_name
        to_field: locations.district_name}
      bind_filters: {from_field: base_customers_factretailsales_v2.include_tax_select
        to_field: base_customers_factretailsales_v2.include_tax_select }
      filters: [base_customers_factretailsales_v2.is_known_cust_txn: "yes"]
    }
  }
  dimension: brand_customer_unique_id {
    hidden: yes
    primary_key: yes
    label: "Customers Customer Key"
  }

  dimension: brand_tenure {
    hidden: yes
  }

  dimension: group_tenure {
    hidden: yes
  }

  dimension: brand_recency {
    hidden: yes
  }

  dimension: group_recency {
    hidden: yes
  }

  dimension: sales_amount_usd {
    hidden: yes

  }

  dimension: customer_lifetime_brand_spend {

    group_label: "Customer Dimensions"
    label: "Brand Lifetime Spend"
    description: "Total customer lifetime spend in a brand (not filtered by date).  Includes tax by default unless exclude tax selected in sales
    . Can be used to segment"
    type: number
    value_format: "$0"
    sql:    ${sales_amount_usd};;
  }


  measure: ave_brand_lifetime_spend {
    label: "Brand Average Lifetime Spend"
    group_label: "Brand Customer Measures"
    type: average_distinct
    sql_distinct_key: ${brand_customer_unique_id} ;;
    value_format: "$0"
    sql: ${customer_lifetime_brand_spend} ;;
  }



  dimension: transaction_count {

    group_label: "Customer Dimensions"
    label:"Brand Frequency LifeTime"
    type: number

    description: "How many times a customer shopped at the Brand. Lifetime frequency."
  }


  dimension: frequency_range_sort {

    hidden: yes

    type: number
    sql: CASE
          WHEN ${transaction_count} <= 1 THEN 1
          WHEN ${transaction_count} <= 3 THEN 2
          WHEN ${transaction_count} <= 5 THEN 3
          WHEN ${transaction_count} <= 7 THEN 4
          WHEN ${transaction_count}  <= 10 THEN 5
          WHEN ${transaction_count}  > 10 THEN 6

          END;;
  }

  dimension: brand_frequency_range_lifetime {
    order_by_field: frequency_range_sort
    view_label: "Customers"
    group_label: "Customer Dimensions"
    label: " Brand Frequency Lifetime Range"
    hidden: no
    description: "Brand Lifetime Frequency Range for RFM analysis"
    type: string
    sql: CASE
          WHEN ${transaction_count} <= 1 THEN "1"
          WHEN ${transaction_count} <= 3 THEN "2-3"
          WHEN ${transaction_count} <= 5 THEN "4-5"
          WHEN ${transaction_count} <= 7 THEN "6-7"
          WHEN ${transaction_count}  <= 10 THEN "8-10"
          WHEN ${transaction_count}  > 10 THEN ">10"

          END;;
  }

  dimension: brand_customer_time_between_purchases {
    hidden: yes
    group_label: "Customer Dimensions"
    description: "Average time between purchases for a customer (in Days) in the brand"
    value_format: "0"
    type: number
    sql: (${brand_tenure}-${brand_recency})/
      case when ${transaction_count}=0 or ${transaction_count}=1 then null
      else  ${transaction_count}-1 end;;
  }

  measure: brand_ave_time_between_purchases {
    group_label: "Brand Customer Measures"
    type: average_distinct
    sql_distinct_key: ${brand_customer_unique_id} ;;
    description: "How frequently a customer is making a purchase ? (in Days)"
    value_format: "0"
    sql: ${brand_customer_time_between_purchases} ;;
  }



  dimension: group_customer_time_between_purchases {
    hidden: yes
    group_label: "Customer Dimensions"
    description: "Average time between purchases for a customer (in Days) in the brand"
    value_format: "0"
    type: number
    sql: (${group_tenure}-${group_recency})/
              case when ${transaction_count}=0 or ${transaction_count}=1 then null
              else  ${transaction_count}-1 end;;
  }



}
