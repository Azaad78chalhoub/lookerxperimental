view: base_customers_offset_sales {
  view_label: "Retail Sales Prev Period (beta)"
  sql_table_name: `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr`
    ;;


  dimension: id {
    label: "Transaction Item ID"
    description: "System generated unique ID for table row. i.e. transaction & item combination."
    primary_key: yes
    type: string

    sql: ${TABLE}.surrogate_key_generated_uid ;;
  }

  dimension: prev_amountlocal_beforetax {
    hidden: yes
    type: number
    sql: ${TABLE}.amountlocal_beforetax ;;
  }

  dimension: prev_amountusd_beforetax {
    hidden: yes
    type: number
    sql: ${TABLE}.amountusd_beforetax ;;
  }

  dimension: prev_atr_cginvoiceno {
    hidden: yes
    label: "Invoice Number"
    description: "Internally generated invoice number. May not match that generated at POS."

    type: string
    sql: ${TABLE}.atr_cginvoiceno ;;
  }

  dimension: prev_order_id_ecomm {
    description: "To join in CS with carriyo/oms, keep hidden"
    hidden: yes
    type: string
    sql: SPLIT(${TABLE}.atr_cginvoiceno, '_')[SAFE_OFFSET(1)] ;;
  }

  dimension: prev_atr_cginvoicenoline {
    hidden: yes
    type: string
    sql: ${TABLE}.atr_cginvoicenoline ;;
  }

  dimension: prev_atr_membershipid {
    hidden: yes
    type: string
    sql: ${TABLE}.atr_membershipid ;;
  }

  dimension: prev_atr_muse_id {
    hidden: yes
    type: string
    sql: ${TABLE}.atr_muse_id ;;
  }


  dimension_group: prev_atr_trandate {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.atr_trandate ;;
  }



  dimension: prev_av_cost_local {
    hidden: yes
    type: number
    sql: ${TABLE}.av_cost_local ;;
  }

  dimension: prev_av_cost_usd {
    hidden: yes
    type: number
    sql: ${TABLE}.av_cost_usd ;;
  }

  dimension: prev_bk_brand {
    hidden: yes
    type: number
    sql: ${TABLE}.bk_brand ;;
  }

  dimension: prev_bk_businessdate {
    label: "Prev Date Key"
    type: number
    sql: ${TABLE}.bk_businessdate ;;
  }

  dimension: prev_bk_customer {
    hidden: yes
    type: string
    sql: ${TABLE}.bk_customer ;;
  }




  dimension: prev_bk_productid {
    hidden: yes
    type: string
    sql: ${TABLE}.bk_productid ;;
  }



  dimension: prev_bk_storeid {
    hidden: yes
    type: number
    value_format_name: id
    sql: ${TABLE}.bk_storeid ;;
  }

  dimension: prev_brand_customer_unique_id {
    hidden: yes
    type: string
    sql: ${TABLE}.brand_customer_unique_id ;;
  }

  dimension: prev_crm_customer_id {
    hidden: yes
    type: string
    sql: ${TABLE}.crm_customer_id ;;
  }

  dimension: prev_cust_email {
    hidden: yes
    type: string
    sql: ${TABLE}.cust_email ;;
  }

  dimension: prev_cust_phone_number {
    hidden: yes
    type: string
    sql: ${TABLE}.cust_phone_number ;;
  }



  dimension: prev_customer_unique_id {
    hidden: yes
    type: number
    sql: ${TABLE}.customer_unique_id ;;
  }


  dimension: prev_discountamt_local {
    hidden: yes
    type: number
    sql: ${TABLE}.discountamt_local ;;
  }

  dimension: prev_discountamt_usd {
    hidden: yes
    type: number
    sql: ${TABLE}.discountamt_usd ;;
  }

  dimension: prev_is_generic_customer {
    hidden: yes
    type: yesno
    sql: ${TABLE}.is_generic_customer ;;
  }

  dimension: prev_mea_amountlocal {
    hidden: yes
    type: number
    sql: ${TABLE}.mea_amountlocal ;;
  }

  dimension: prev_mea_amountusd {
    hidden: yes
    type: number
    sql: ${TABLE}.mea_amountusd ;;
  }

  dimension: prev_mea_quantity {
    hidden: yes
    type: number
    sql: ${TABLE}.mea_quantity ;;
  }

  dimension: prev_min_brand_id {
    hidden: yes
    type: number
    sql: ${TABLE}.min_brand_id ;;
  }

  dimension: prev_new_cust_brand {
    hidden: yes
    type: string
    sql: ${TABLE}.new_cust_brand ;;
  }

  dimension: prev_new_cust_group {
    hidden: yes
    type: string
    sql: ${TABLE}.new_cust_group ;;
  }

  dimension: prev_pos_customer_id {
    hidden: yes
    type: string
    sql: ${TABLE}.pos_customer_id ;;
  }


  dimension: prev_sales_channel {
    hidden: yes
    type: string
    sql: ${TABLE}.sales_channel ;;
  }

  dimension: prev_surrogate_key {
    hidden: yes
    type: number
    sql: ${TABLE}.surrogate_key ;;
  }

  dimension: prev_surrogate_key_generated_uid {
    hidden: yes
    type: string
    sql: ${TABLE}.surrogate_key_generated_uid ;;
  }

  dimension: prev_taxamt_local {
    hidden: yes
    type: number
    sql: ${TABLE}.taxamt_local ;;
  }

  dimension: prev_taxamt_usd {
    hidden: yes
    type: number
    sql: ${TABLE}.taxamt_usd ;;
  }

  dimension: prev_xstore_register {
    hidden: yes
    type: number
    sql: ${TABLE}.xstore_register ;;
  }

  measure: prev_count {
    hidden: yes
    type: count
    drill_fields: []
  }



  measure: prev_Item_Count  {
    description: "Aggregate quantity of units sold previous period"
    type: sum
    sql: ${prev_mea_quantity} ;;
  }

  measure:  prev_transaction_count{
    description: "Count of transactions previous period"
    type: count_distinct
    sql: ${prev_atr_cginvoiceno} ;;
  }

  measure: prev_sales_amount_usd {
    label: "Prev Sales_Amount_USD"
    type: sum
    description: "Net sales is the sum of gross sales minus its returns, allowances, and discounts (without VAT)"
    value_format: "$#,##0;($#.00)"
    sql:
    {% if base_customers_factretailsales.include_tax_select._parameter_value == 'exc' %}

     ${prev_amountusd_beforetax}
    {% else %}
    ${prev_mea_amountusd}
    {% endif %}

     ;;
  }



  measure: prev_sales_smount_local {
    type: sum
    description: "Net sales is the sum of gross sales minus its returns, allowances, and discounts (without VAT)"
    value_format: "#,##0.00"
    sql:
    {% if base_customers_factretailsales.include_tax_select._parameter_value == 'exc' %}

     ${prev_amountlocal_beforetax}
    {% else %}
    ${prev_mea_amountlocal}
    {% endif %}

     ;;
  }
  measure: prev_UPT {
    group_label: "Basket Measures"
    label: "UPT prev period"
    value_format: "0.##"
    description: "Average units per transaction"
    type: number
    sql: ${prev_Item_Count}/nullif(${prev_transaction_count},0) ;;
  }

  measure: prev_VPT {
    group_label: "Basket Measures"
    label: "AOV prev period"
    value_format: "$0"
    description: "Average Value per Order"
    type: number
    sql: ${prev_sales_amount_usd}/nullif(${prev_transaction_count},0) ;;
  }

  measure: prev_customer_key_count {
    group_label: "Customer Related"
    label: "Prev Customer Unique Count (Transacted)"

    hidden: no
    type: count_distinct

    sql:  ${TABLE}.customer_unique_id;;
  }

  measure: prev_brand_customer_key_count {
    group_label: "Customer Related"
    label: "Prev Brand Customer Unique Count (Transacted)"

    hidden: no
    type: count_distinct

    sql:  ${TABLE}.brand_customer_unique_id;;
  }


}
