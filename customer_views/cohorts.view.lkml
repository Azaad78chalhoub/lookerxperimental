view: cohorts {
  sql_table_name: `chb-prod-data-cust.prod_segmentation.cohorts`
    ;;


  dimension: brand_id {
    type: number
    sql: ${TABLE}.brand_id ;;
  }

  dimension: brand_name {
    type: string
    sql: ${TABLE}.brand_name ;;
  }

  dimension: first_txn_period {
    type: string
    sql: ${TABLE}.first_txn_period ;;
  }

  dimension: n {
    type: number
    sql: ${TABLE}.n ;;
  }

  dimension: period {
    type: string
    sql: ${TABLE}.period ;;
  }

  measure: count_unique_customers {
      type: sum
      sql: ${TABLE}.count_unique_customers ;;
    }

  measure: transaction_count {
    type: sum
    sql: ${TABLE}.transaction_count ;;
  }

  measure: item_sold {
    type: sum
    sql: ${TABLE}.quantity ;;
  }

  measure: amount_usd {
    type: sum
    sql: ${TABLE}.amount_usd ;;
  }

}
