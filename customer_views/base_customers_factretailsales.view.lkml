include: "../_common/period_over_period_flexible.view"

view: base_customers_factretailsales {
  view_label: "Retail Sales"
  sql_table_name:  `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr`  ;;
  drill_fields: [id]


parameter: comparative_period_start {
  hidden: yes
  type: date_time
}

  parameter: comparative_period_end {
    hidden: yes
    type: date_time
  }




  # measure: customers_previous_period {

  #   type: sum
  #   sql:  (select count (distinct brand_customer_unique_id

  #   from  `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr`
  #   where min_brand_id=${min_brand_id}

  #   atr_trandate>=${% parameter comparative_period_start_date %} and atr_trandate<${% parameter comparative_period_end_date %}


  #   );;


  # }

dimension: anykey {

  description: "added to enable out join to previous period ndt as could not out join on an inequality only"
  type: number
  sql: case when 1=1 then 1 else 1 end ;;
}

  dimension: id {
   label: "Transaction Item ID"
  hidden: yes
   description: "System generated unique ID for table row. i.e. transaction & item combination."
    primary_key: yes
    type: string

    sql: ${TABLE}.surrogate_key_generated_uid ;;
  }

  dimension: data_source {
    description: "Source system for the sales record."
    type: string
    sql: upper(${TABLE}.data_source) ;;
  }

  dimension: brand_customer_unique_id {
    hidden: yes
    type: string
    sql: ${TABLE}.brand_customer_unique_id ;;
  }
  dimension: _fivetran_deleted {
    hidden: yes
    type: yesno
    sql: ${TABLE}._fivetran_deleted ;;
  }

  dimension_group: _fivetran_synced {
    hidden: yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}._fivetran_synced ;;
  }

  dimension: amountlocal_beforetax {
    hidden: yes
    type: number
    sql: ${TABLE}.amountlocal_beforetax ;;
  }

  dimension: amountusd_beforetax {
    hidden: yes
    type: number
    sql: ${TABLE}.amountusd_beforetax ;;
  }


  dimension: atr_activitytype {
    hidden: yes
    type: string
    sql: ${TABLE}.atr_activitytype ;;
  }

  dimension: atr_batchid {
    hidden: yes
    type: string
    sql: ${TABLE}.atr_batchid ;;
  }

  dimension: atr_cginvoiceno {
    label: "Invoice Number"
    description: "Internally generated invoice number. May not match that generated at POS."

    type: string
    sql: ${TABLE}.atr_cginvoiceno ;;
  }

  dimension: order_id_ecomm {
    description: "To join in CS with carriyo/oms, keep hidden"
    hidden: yes
    type: string
    sql: SPLIT(${TABLE}.atr_cginvoiceno, '_')[SAFE_OFFSET(1)] ;;
  }

  dimension: atr_cginvoicenoline {
    hidden: yes
    type: string
    sql: ${TABLE}.atr_cginvoicenoline ;;
  }

  dimension_group: atr_createdate {
    hidden: yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.atr_createdate ;;
  }

  dimension: atr_createdate_order {
    description: "CS - used to retrieve online order date"
    label: "ATR Created Date"
    type: date
    sql: ${TABLE}.atr_createdate ;;
  }


  dimension: atr_day {
    hidden: yes
    type: number
    sql: ${TABLE}.atr_day ;;
  }

  dimension_group: atr_documentdate {
    hidden: yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.atr_documentdate ;;
  }



  dimension: atr_homecurrency {
    label: "Local Currency"
    type: string
    sql: ${TABLE}.atr_homecurrency ;;
  }

  dimension: atr_itemseqno {
    hidden: yes
    type: number
    sql: ${TABLE}.atr_itemseqno ;;
  }



  dimension: atr_membershipid {
    hidden: yes
    type: string
    sql: ${TABLE}.atr_membershipid ;;
  }

  dimension: atr_muse_id {
    hidden: yes
    type: string
    sql: ${TABLE}.atr_muse_id ;;
  }



  dimension: atr_salesperson {
    label: "SalesPerson Staff ID"
    hidden: no
    type: string
    sql: ${TABLE}.atr_salesperson ;;
  }



  dimension: atr_storedayseqno {
    hidden: yes
    type: number
    sql: ${TABLE}.atr_storedayseqno ;;
  }

  dimension: atr_time {
    hidden: yes
    type: number
    sql: ${TABLE}.atr_time ;;
  }

  dimension: atr_timezone {
    hidden: yes
    type: string
    sql: ${TABLE}.atr_timezone ;;
  }

   extends: [period_over_period_flexible]

  dimension_group: pop_no_tz{
    sql: ${atr_trandate_date} ;;
  }

  dimension_group: atr_trandate {
    label: "Transaction"
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: (PARSE_DATE("%Y%m%d",  cast(${bk_businessdate} as string))) ;;
  }


  dimension: date_group {
    label: "Date Group"
    hidden: yes
    sql: {% if base_customers_factretailsales.atr_trandate_date._in_query %} CAST(${TABLE}.atr_trandate AS DATE)
        {% elsif base_customers_factretailsales.atr_trandate_month._in_query %} FORMAT_DATE('%Y-%m', ${TABLE}.atr_trandate )
        {% elsif base_customers_factretailsales.atr_trandate_quarter._in_query %} (FORMAT_TIMESTAMP('%Y-%m', TIMESTAMP_TRUNC(CAST(${TABLE}.atr_trandate AS TIMESTAMP), QUARTER)))
        {% elsif base_customers_factretailsales.atr_trandate_week._in_query %} CONCAT(CAST(EXTRACT(YEAR FROM ${TABLE}.atr_trandate ) AS STRING),'-', CAST(EXTRACT(WEEK(SUNDAY) FROM ${TABLE}.atr_trandate) AS STRING))
        {% elsif base_customers_factretailsales.atr_trandate_year._in_query %} EXTRACT(YEAR FROM ${TABLE}.atr_trandate )
        {% else %} CAST(${TABLE}.atr_trandate  AS DATE)
        {% endif %} ;;
  }


dimension: selected_start_date {

  type: date
  sql: {% date_start atr_trandate_date %} ;;
}

measure: min_start_date {
  hidden: yes
  type: min
  sql: ${selected_start_date} ;;
}

  dimension: selected_end_date {
 hidden: yes
    type: string
    sql: case when IFNULL({% date_end atr_trandate_date %},current_date())>=current_date() then current_date()


    else {% date_end atr_trandate_date %} end ;;
  }

  dimension:current_month{
    hidden: yes
    type: number
  sql:  extract(month from  (PARSE_DATE("%Y%m%d",  cast(${bk_businessdate} as string))));;

  }

  dimension:current_year{
    hidden: yes
    type: number
    sql:  extract(year from  (PARSE_DATE("%Y%m%d",  cast(${bk_businessdate} as string))));;

  }


  dimension: previous_month {
    hidden: yes
    type: number
    sql: extract(month from (date_add((PARSE_DATE("%Y%m%d", cast(${bk_businessdate} as string))), INTERVAL -1 MONTH)));;


  }

  dimension: previous_month_year {
    hidden: yes
    type: number
    sql:  extract(year from (date_add((PARSE_DATE("%Y%m%d", cast(${bk_businessdate} as string))), INTERVAL -1 MONTH))) ;;
  }

  measure: max_date {
    hidden: yes
    type: max
    sql: ${bk_businessdate} ;;
  }

  measure: min_date {
    hidden: yes
    type: min
    sql: ${bk_businessdate} ;;
  }

  measure: days_in_period2 {
    hidden: yes
    type: number
    sql:  DATE_DIFF (PARSE_DATE("%Y%m%d", cast(${max_date} as string)),
      PARSE_DATE("%Y%m%d",cast(${min_date} as string)), DAY) +1  ;;
  }

dimension: pre_month_total_cust {
  hidden: yes
  type: number
  sql: ${prev_month_tot_cust.last_month_total} ;;
}



  measure: retained_customer_rolling {
    hidden: yes
    view_label: "Retention Rate"
    description: "Number of customers retained from the previous month"
    type: count_distinct
    sql: ${prev_month_customers.brand_customer_unique_id} ;;
  }

  measure: retention_rate__monthly_rolling {
    view_label: "Retention Rate"
    description: "Month on Month retention rate. Add tran date month to explore column."
    type: number
   value_format: "0.000%"
    sql:  ${retained_customer_rolling}/NULLIF(min(${pre_month_total_cust}),0);;
  }


measure: total_customers_prev_period {
  view_label: "Retention Rate"
  type: number
  sql:
  case when {% parameter base_customers_distinct.district_country %}='All'
  and  {% parameter base_customers_distinct.district_brand %}<>'All'
    and  {% parameter base_customers_distinct.district_brand %}<>'Muse'
  then
  (select count (distinct brand_customer_unique_id)
                                  from
                                  `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr` as frs
                                  inner join `chb-prod-supplychain-data.prod_shared_dimensions.dim_retail_loc` as loc
                                  on frs.bk_storeid=loc.store
                                  where

                                  frs.min_brand_id=cast({% parameter base_customers_distinct.district_brand  %} as int64) and
                                  frs.atr_trandate >=cast(${dim_calendar.ret_range_start} as date)
                                  and frs.atr_trandate<cast(${dim_calendar.ret_range_end}  as date))
  when {% parameter base_customers_distinct.district_country %}='All'
    and {% parameter base_customers_distinct.district_brand %}='All'
  then
  (select count (distinct brand_customer_unique_id)
                                  from
                                  `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr` as frs
                                  inner join `chb-prod-supplychain-data.prod_shared_dimensions.dim_retail_loc` as loc
                                  on frs.bk_storeid=loc.store
                                  where


                                  frs.atr_trandate >=cast(${dim_calendar.ret_range_start} as date)
                                  and frs.atr_trandate<cast(${dim_calendar.ret_range_end}  as date))

  when {% parameter base_customers_distinct.district_country %}='All'
    and {% parameter base_customers_distinct.district_brand %}='Muse'
  then
    (select count (distinct brand_customer_unique_id)
                                  from
                                  `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr` as frs
                                  inner join `chb-prod-supplychain-data.prod_shared_dimensions.dim_retail_loc` as loc
                                  on frs.bk_storeid=loc.store
                                  where
                                  loc.store in  (SELECT distinct bk_storeid FROM
                                  `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr` WHERE atr_muse_id is not null)
                                  and
                                  frs.min_brand_id=cast({% parameter base_customers_distinct.district_brand  %} as int64) and
                                  frs.atr_trandate >=cast(${dim_calendar.ret_range_start} as date)
                                  and frs.atr_trandate<cast(${dim_calendar.ret_range_end}  as date))
  when {% parameter base_customers_distinct.district_country %}<>'All'
    and {% parameter base_customers_distinct.district_brand %}='Muse'
  then
            (select count (distinct brand_customer_unique_id)
                                  from
                                  `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr` as frs
                                  inner join `chb-prod-supplychain-data.prod_shared_dimensions.dim_retail_loc` as loc
                                  on frs.bk_storeid=loc.store
                                  where
                                  loc.country_desc = {% parameter base_customers_distinct.district_country %}
                                  and loc.store in  (SELECT distinct bk_storeid FROM
                                  `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr` WHERE atr_muse_id is not null)
                                  and
                                  frs.min_brand_id=cast({% parameter base_customers_distinct.district_brand  %} as int64) and
                                  frs.atr_trandate >=cast(${dim_calendar.ret_range_start} as date)
                                  and frs.atr_trandate<cast(${dim_calendar.ret_range_end}  as date))

   else
          (select count (distinct brand_customer_unique_id)
                                  from
                                  `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr` as frs
                                  inner join `chb-prod-supplychain-data.prod_shared_dimensions.dim_retail_loc` as loc
                                  on frs.bk_storeid=loc.store
                                  where
                                  loc.country_desc = {% parameter base_customers_distinct.district_country %}
                                  and
                                  frs.min_brand_id=cast({% parameter base_customers_distinct.district_brand  %} as int64) and
                                  frs.atr_trandate >=cast(${dim_calendar.ret_range_start} as date)
                                  and frs.atr_trandate<cast(${dim_calendar.ret_range_end}  as date))
end
  ;;
}



# dimension: ret_start_dim {
#   hidden: yes
#   type: date
#   sql: cast({% parameter base_customers_distinct.retention_range_start %} as date) ;;
# }


#   dimension: ret_end_dim {
#     hidden: yes
#     type: date
#     sql: cast({% parameter base_customers_distinct.retention_range_end %} as date) ;;
#   }

measure: retained_customer_count{
  view_label: "Retention Rate"

  description: "Total number of customers retained from the selected previous period"
  type: count_distinct
  sql: case when ${base_customers_distinct.retained_customer} then  ${brand_customer_unique_id} else null end;;



}


measure: retention_rate {
  view_label: "Retention Rate"


  type: number
  value_format: "0.00%"
  sql: ${retained_customer_count}/nullif(${total_customers_prev_period},0) ;;


}







  dimension: atr_tranno {
    hidden: yes
    type: number
    sql: ${TABLE}.atr_tranno ;;
  }

  dimension: atr_transeqno {
    hidden: yes
    type: number
    sql: ${TABLE}.atr_transeqno ;;
  }

  dimension: atr_trantype {
    hidden: yes
    type: string
    sql: ${TABLE}.atr_trantype ;;
  }

  dimension_group: atr_updatedate {
    hidden: yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.atr_updatedate ;;
  }

  dimension: av_cost_local {
    hidden: yes
    type: number
    sql: ${TABLE}.av_cost_local ;;
  }

  dimension: av_cost_usd {
    hidden: yes
    type: number
    sql: ${TABLE}.av_cost_usd ;;
  }

  dimension: bk_brand {
    hidden: yes
    type: number
    sql: ${TABLE}.bk_brand ;;
  }

  dimension: bk_businessdate {
    hidden: yes
    type: number
    sql: ${TABLE}.bk_businessdate ;;
  }

  dimension: bk_customer {
    hidden: yes
    type: string
    sql: ${TABLE}.bk_customer ;;
  }

  dimension: bk_loyalty {
    hidden: yes
    type: string
    sql: ${TABLE}.bk_loyalty ;;
  }

  dimension: bk_loyaltyprogram {
    hidden: yes
    type: string
    sql: ${TABLE}.bk_loyaltyprogram ;;
  }

  dimension: bk_productid {
    hidden: yes
    label:"Product ID"
    type: string
    sql: ${TABLE}.bk_productid ;;
  }

  dimension: bk_salestype {

    hidden: yes
    type: number
    sql: ${TABLE}.bk_salestype ;;
  }

  dimension: bk_storeid {
    label: "Store ID"
    hidden: yes
    type: number
    value_format_name: id
    sql: ${TABLE}.bk_storeid ;;
  }





  dimension: consignment_flag {
    hidden: yes
    type: string
    sql: ${TABLE}.consignment_flag ;;
  }

  dimension: conversion_rate {
    hidden: yes
    type: number
    sql: ${TABLE}.CONVERSION_RATE ;;
  }

  dimension_group: creationdatetime {
    hidden: yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.creationdatetime ;;
  }

  dimension: crm_customer_id {
    label: "CRM Customer ID"
    type: string
    sql: ${TABLE}.crm_customer_id ;;
  }

  dimension: customer_golden_id {
    hidden: yes
    type: string
    sql: ${TABLE}.customer_golden_id ;;
  }

  dimension: discountamt_local {
    hidden: yes

    type: number
    sql: ${TABLE}.discountamt_local ;;
  }

  dimension: discountamt_usd {

    type: number
    sql: ${TABLE}.discountamt_usd ;;
  }

  dimension: customer_unique_id {
    hidden: yes
    type: number
    sql: ${TABLE}.customer_unique_id ;;
  }

  dimension: mea_amountlocal {
    hidden: yes
    type: number
    sql: ${TABLE}.mea_amountlocal ;;
  }

  dimension: mea_amountusd {
    hidden: yes
    type: number
    sql: ${TABLE}.mea_amountusd ;;
  }

  dimension: mea_quantity {
    hidden: yes
    type: number
    sql: ${TABLE}.mea_quantity ;;
  }

  dimension: min_brand_id {
    hidden: yes
    type: number
    sql: ${TABLE}.min_brand_id ;;
  }

  dimension: pos_customer_id {
    hidden: yes
    type: string
    sql: ${TABLE}.pos_customer_id ;;
  }

  dimension: register {
    hidden: yes
    type: string
    sql: ${TABLE}.register ;;
  }

  dimension: sales_channel {
    description:"For sales not through usual channel. e.g WhatsApp, Concierge etc."
    type: string
    sql: ${TABLE}.sales_channel ;;
  }



  dimension: sales_channel_group {
    description:"Sales channel grouping"
    type: string
    sql: case
          when ${sales_channel} LIKE "%Clienteling - Distant Sale%"
          or ${sales_channel} LIKE "%Catalogue Emailers%"
          or ${sales_channel} LIKE "%Customer Service Sales%"
          or ${sales_channel} LIKE "%Digital Concierge%"
          or ${sales_channel} LIKE "%Home Appointment%"
          or ${sales_channel} LIKE "%Referral%"
          or ${sales_channel} LIKE "%Concierge Upsell%" then "Distant Clienteling Sales"
          when ${sales_channel} LIKE "%Clienteling - Store Sale%" then "In-Store Clienteling Sales"
          when ${sales_channel} LIKE "%E-Com Sale%"
          or ${sales_channel} LIKE "%E-commerce%"
          or ${sales_channel} LIKE "%Ecommerce Pop Up%"
          or ${sales_channel} LIKE "%Instagram%"
          or ${sales_channel} LIKE "%SnapChat%"
          or ${sales_channel} LIKE "%Social Channels%"
          or ${sales_channel} LIKE "%MUSE%"
          or ${sales_channel} LIKE "%THAHAB MARKETPLACE%"
          or ${sales_channel} LIKE "%The Luxury Circle%"
          or ${sales_channel} LIKE "%Tiktok%"
          or ${sales_channel} LIKE "%Whatsapp Sale%"
          or ${sales_channel} LIKE "%Virtual Appointment%"
          or ${sales_channel} LIKE "%Whatsapp/Phone Call Store Sales%"
          or ${sales_channel} LIKE "%Whatsapp/Phone Home Delivery%" then "E-Com Clienteling Sales"
          when ${sales_channel} LIKE "%Store Sale%"
          or ${sales_channel} LIKE "%Stores Sales%"
          or ${sales_channel} LIKE "%Store Appointment%" THEN "Store Sales"
          else ""
          end
          ;;
  }
  dimension:  clienteling_sales_channel {
    group_label: "Clienteling Dimensions"
    description:"is the sale a clienteling sale?"
    type: yesno
    sql: ${sales_channel_group} like '%Client%';;
  }

  dimension: taxamt_local {
    hidden: yes
    type: number
    sql: ${TABLE}.taxamt_local ;;
  }

  dimension: taxamt_usd {
    hidden: yes
    type: number
    sql: ${TABLE}.taxamt_usd ;;
  }

  dimension: is_discount_sale {
hidden: yes
    type: yesno
    sql: ifnull(${discountamt_usd},0)<>0 ;;
  }

  dimension: is_muse_txn{

    label: "Is Muse Txn"
    description: "Whether the transaction is linked to a Muse member."
    type: yesno
    sql: ${atr_muse_id} is not null;;
  }
  dimension: is_crm_txn{

    label: "Is CRM Txn"
    type: yesno
    description: "Note - will also include generic CRM accounts if not filtered out."
    sql:  ${crm_customer_id} is not null ;;
  }

  dimension: is_crm_txn_excl_generic{

    label: "Is CRM Txn Excl. Generic"
    type: yesno
    sql:  ${crm_customer_id} is not null and ${is_generic_customer}=false ;;
  }


  dimension: is_known_cust_txn{

    description: "Sale attributed to a known customer (i.e. CRM or Muse) including generic"
    type: yesno
    sql:  ${customer_unique_id} is not null  ;;
  }

  dimension: is_known_cust_txn_excl_generic{

    description: "Sale attributed to a known customer (i.e. CRM or Muse) excluding generic"
    type: yesno
    sql:  ${customer_unique_id} is not null and ${is_generic_customer}=false ;;
  }








measure:count_known_txn  {
  hidden: yes
  type: count_distinct
  sql: ${atr_cginvoiceno} ;;

  filters: {
    field: is_known_cust_txn_excl_generic
    value: "yes"  }
}


measure:count_unknown_txn  {
  hidden: yes
  type: count_distinct
  sql: ${atr_cginvoiceno} ;;

  filters: {
    field: is_known_cust_txn_excl_generic
    value: "no"  }
}

measure:proportion_known_txn {
  description: "Proportion of transactions linked to a known customer"
  type: number
  value_format: "0.00%"
  sql: IFNULL((${count_known_txn})/NULLIF(${transaction_count},0),0) ;;
}



dimension: is_generic_customer   {
  label: "Is Generic ID Txn"
  type: yesno
  sql: ${TABLE}.is_generic_customer ;;
}


  dimension: is_muse_signup_txn {
    description: "Is transaction associated with a Muse Member Sign-Up"
    type: yesno
    sql: case when ${transaction_attributes.is_muse_regi_txn} is null then false else true end ;;
    }


dimension: new_cust_brand {
  hidden: yes
  type: string
  sql: ${TABLE}.new_cust_brand;;
}

  dimension: returning_cust_brand {
    hidden: yes
    type: number
    sql: case when ${TABLE}.new_cust_brand = 'Returning' then 1 else 0 end ;;
  }

  dimension: new_cust_group {
    hidden: yes
    type: string
    sql: ${TABLE}.new_cust_group ;;

  }



  measure: count_new_cust_brand {
    group_label: "Customer Related"
    type: count_distinct
    sql: ${brand_customer_unique_id};;

    filters: {
      field:  new_cust_brand
      value: "New"  }
  }







measure: count_returning_cust_brand {
  group_label: "Customer Related"
  type: number
  sql: ${brand_customer_key_count}-${count_new_cust_brand} ;;
}



  measure: count_new_cust_group {
    group_label: "Customer Related"
    type: count_distinct
    sql: ${customer_unique_id};;

    filters: {
      field:  new_cust_group
      value: "New"  }
  }



  measure: count_returning_cust_group {
    group_label: "Customer Related"
    type: number
    sql: ${customer_key_count}-${count_new_cust_group} ;;
  }


measure: count_retained_customers {
  description: "Count of brand customers retained from the previous period.  Only works with Retention Period in Customer view."
  type: count_distinct
  group_label: "Customer Related"
  sql: case when ${base_customers_distinct.retained_customer} = true then ${base_customers_distinct.customer_unique_id}
  else null end;;


}



  parameter: include_tax_select {
    type: unquoted
    description: "Choose whether all sales metrics include sales tax or not"
    allowed_value: {
      label: "Sales Including Tax"
      value: "inc"
    }
    allowed_value: {
      label: "Sales Excluding Tax"
      value: "exc"
    }
  default_value: "inc"
    }


  measure:  brand_visits {
    label: "Total unique brands shopped"
    description: "Total numer of unique group brandsthe customers have shopped at in the selected transaction date range"
    hidden: no
    type: count_distinct
    sql: ${min_brand_id}  ;;
  }


  measure: count_of_discount_sales{

      type: count_distinct
      sql: ${atr_cginvoiceno} ;;

    filters: {
      field: is_discount_sale
      value: "yes"  }
  }

  measure: proportion_of_discount_sales{

    description: "Percentage of transactions which included a discount"
    type: number
    value_format: "0.00%"
    sql: IFNULL((${count_of_discount_sales})/NULLIF(${transaction_count},0),0) ;;
  }

  measure: count {
    hidden: yes
    type: count
    drill_fields: [id]
  }

  measure: Item_Count  {
    description: "Aggregate quantity of units sold"
    type: sum
    sql: ${mea_quantity} ;;
  }

  measure:  transaction_count{
    description: "Purchases, Returns, or Exchanges made by a customer in a store or online"
    type: count_distinct
    sql: ${atr_cginvoiceno} ;;
  }

  measure: Sales_Amount_USD {
    type: sum
    description: "Net sales is the sum of gross sales minus its returns, allowances, and discounts (without VAT)"
    value_format: "$#,##0;($#.00)"
    sql:
    {% if include_tax_select._parameter_value == 'exc' %}

     ${amountusd_beforetax}
    {% else %}
    ${mea_amountusd}
    {% endif %}

     ;;
  }


  measure: Sales_Amount_local {
    type: sum
    description: "Net sales is the sum of gross sales minus its returns, allowances, and discounts (without VAT)"
    value_format: "#,##0.00"
    sql:
    {% if include_tax_select._parameter_value == 'exc' %}

     ${amountlocal_beforetax}
    {% else %}
    ${mea_amountlocal}
    {% endif %}

     ;;
  }
  measure: UPT {
    group_label: "Basket Measures"
    label: "UPT"
    value_format: "0.##"
    description: "Average units per transaction"
    type: number
    sql: ${Item_Count}/nullif(${transaction_count},0) ;;
  }

  measure: VPT {
    group_label: "Basket Measures"
    label: "AOV"
    value_format: "$0"
    description: "Average Value per Order"
    type: number
    sql: ${Sales_Amount_USD}/nullif(${transaction_count},0) ;;
  }

  measure: customer_key_count {
    group_label: "Customer Related"
    label: "Customer Unique Count (Transacted)"

    hidden: no
    type: count_distinct

    sql:  ${TABLE}.customer_unique_id;;
  }

  measure: brand_customer_key_count {
    group_label: "Customer Related"
    label: "Brand Customer Unique Count (Transacted)"

    hidden: no
    type: count_distinct

    sql:  ${TABLE}.brand_customer_unique_id;;
  }

  measure: brand_acquisition_rate {
    group_label: "Customer Related"
    description: "Percentage of new customers shopped in the brand in the selected period"
    type: number
    value_format: "0%"
    sql: ${count_new_cust_brand}/${brand_customer_key_count} ;;
  }

  measure: acquisition_rate {
    label: "Acquisition Rate (General)"
    group_label: "Customer Related"
    description: "Percentage of new customers shopped in the selected period. Works single or multi-brand, country level etc."
    type: number
    value_format: "0%"
    sql: ${new_to_store_pp_ndt.count_previously_shopped}/${customer_key_count} ;;
  }

measure: new_customer_count{
  label:"New Customer Count (General)"
  group_label: "Customer Related"
  type: number
  sql:  ${customer_key_count}-${new_to_store_pp_ndt.count_previously_shopped};;
}


  measure: group_acquisition_rate {
    group_label: "Customer Related"
    description: "Percentage of new customers shopped in the group in the selected period"
    type: number
    value_format: "0%"
    sql: ${new_customer_count}/${customer_key_count} ;;
  }

  measure:  Known_Cust_Tran_Count{
    hidden: yes
    type: count_distinct
    sql: case when ${brand_customer_unique_id} is null and ${is_generic_customer} = false then null else ${atr_cginvoiceno} end;;

  }

  measure: customer_key_count_RFM {

    group_label: "Customer Related"
    label: "Customer Count for RFM Analysis ONLY"
    hidden: yes
    type: count_distinct
    drill_fields: [Sales_Amount_USD]
    sql:  ${TABLE}.brand_customer_unique_id;;
    html:{% if base_customers_factretailsales.Sales_Amount_USD._value < 20000 %}
          <div style="background-color: rgba(240,248,255,{{value}}); font-size:100%; text-align:center"><a href="{{ link }}" style="color: black" target="_new">{{ customer_key_count_RFM._rendered_value }} </a></div>
          {% elsif base_customers_factretailsales.Sales_Amount_USD._value >= 20000 and base_customers_factretailsales.Sales_Amount_USD._value < 60000 %}
          <div style="background-color: rgba(173,216,230,{{value}}); font-size:100%; text-align:center"><a href="{{ link }}" style="color: white" target="_new">{{ customer_key_count_RFM._rendered_value }} </a></div>
           {% elsif base_customers_factretailsales.Sales_Amount_USD._value >= 60000 and base_customers_factretailsales.Sales_Amount_USD._value < 100000 %}
          <div style="background-color: rgba(135,206,250,{{value}}); font-size:100%; text-align:center"><a href="{{ link }}" style="color: white" target="_new">{{ customer_key_count_RFM._rendered_value }} </a></div>
            {% elsif base_customers_factretailsales.Sales_Amount_USD._value >= 100000 and base_customers_factretailsales.Sales_Amount_USD._value < 400000 %}
          <div style="background-color: rgba(30,144,255,{{value}}); font-size:100%; text-align:center"><a href="{{ link }}" style="color: white" target="_new">{{ customer_key_count_RFM._rendered_value }} </a></div>
           {% elsif base_customers_factretailsales.Sales_Amount_USD._value >= 400000 and base_customers_factretailsales.Sales_Amount_USD._value < 800000 %}
          <div style="background-color: rgba(65,105,225,{{value}}); font-size:100%; text-align:center"><a href="{{ link }}" style="color: white" target="_new">{{ customer_key_count_RFM._rendered_value }} </a></div>
         {% elsif base_customers_factretailsales.Sales_Amount_USD._value >= 800000 and base_customers_factretailsales.Sales_Amount_USD._value < 1100000 %}
          <div style="background-color: rgba(0,0,255,{{value}}); font-size:100%; text-align:center"><a href="{{ link }}" style="color: white" target="_new">{{ customer_key_count_RFM._rendered_value }} </a></div>
      {% elsif base_customers_factretailsales.Sales_Amount_USD._value >= 1100000 and base_customers_factretailsales.Sales_Amount_USD._value < 3000000 %}
          <div style="background-color: rgba(25,35,150,{{value}}); font-size:100%; text-align:center"><a href="{{ link }}" style="color: white" target="_new">{{ customer_key_count_RFM._rendered_value }} </a></div>
      {% elsif base_customers_factretailsales.Sales_Amount_USD._value >= 3000000 and base_customers_factretailsales.Sales_Amount_USD._value < 6000000 %}
          <div style="background-color: rgba(0,0,128,{{value}}); font-size:100%; text-align:center"><a href="{{ link }}" style="color: white" target="_new">{{ customer_key_count_RFM._rendered_value }} </a></div>
       {% else %}
          <div style="background-color: rgba(0,0,128,0.99); font-size:100%; text-align:center"><a href="{{ link }}" style="color: white" target="_new">{{ customer_key_count_RFM._rendered_value }} </a></div>
          {% endif %}
          ;;}

      measure: customer_CRM_count {
        group_label: "Customer Related"
        hidden: yes
        label: "CRM member Unique Count (Transacted)"

        type: count_distinct
        sql:  ${TABLE}.crm_customer_id ;;
      }



      measure: Average_Frequency {
        group_label: "Customer Related"
        description: "Purchase frequency is the average number of purchases made by a customer over a given period of time"
        type: number
        value_format: "0.##"
        sql: ${Known_Cust_Tran_Count}/NULLIF(${customer_key_count},0) ;;
      }



      measure: AUR{
        group_label: "Basket Measures"
        label: "AUR"
        value_format: "$0"
        description: "Average value per item"
        type: number
        sql: ${Sales_Amount_USD}/NULLIF(${Item_Count},0) ;;
      }


      measure: know_customer_sales {
        hidden: yes
        group_label: "Customer Related"
        description: "Sales attributed to a known customer"
        value_format: "$#,##0"
        type: sum
        sql:
              CASE
                WHEN nullif(${TABLE}.brand_customer_unique_id,'') is null then 0 else (    {% if include_tax_select._parameter_value == 'exc' %}

     ${amountusd_beforetax}
    {% else %}
    ${mea_amountusd}
    {% endif %})

              END ;;
      }

      measure: unknow_sales {
        hidden: yes
        group_label: "Customer Related"
        value_format: "$#,##0"
        description: "Sales not linked to a known customer"

        type: sum
        sql:
              CASE
                WHEN nullif(${TABLE}.brand_customer_unique_id,'') is null then (    {% if include_tax_select._parameter_value == 'exc' %}

     ${amountusd_beforetax}
    {% else %}
    ${mea_amountusd}
    {% endif %}) else 0

              END ;;
      }

      measure: percentage_known_sales{
        hidden: yes
        description: "Known customer sales (excl. generic) / Total sales"
        group_label: "Customer Related"
        type: number
        value_format: "0.00%"
        sql:  ${know_customer_sales}/nullif(${Sales_Amount_USD},0};;
      }

      measure: average_spend {
        group_label: "Customer Related"
        description: "Average Net sales for a given customer over a given period of time"
        type: number
        value_format: "$#,##0"
        sql: ${know_customer_sales}/nullif(${customer_key_count},0) ;;
      }

      measure: muse_sales {
        group_label: "Customer Related"
        description: "Sales attributed to Muse member"
        type: sum
        value_format: "$#,##0"
        sql: case when ${atr_muse_id} is not null then (    {% if include_tax_select._parameter_value == 'exc' %}

     ${amountusd_beforetax}
    {% else %}
    ${mea_amountusd}
    {% endif %})
          else 0 end ;;

      }


  measure: estimated_muse_sales_goldenrecord {
    group_label: "Customer Related"
    description: "Estimated MUSE sales - based on golden record logic of linking MUSE IDs"
    type: sum
    value_format: "$#,##0"
    sql: case when ${base_customers_distinct.muse_member_id} is not null then (    {% if include_tax_select._parameter_value == 'exc' %}

           ${amountusd_beforetax}
          {% else %}
          ${mea_amountusd}
          {% endif %})
                else 0 end ;;

    }

    measure: non_muse_sales {
      group_label: "Customer Related"
      description: "Non-MUSE Sales"
      type: number
      hidden: no
      value_format: "$#,##0"
      sql:  nullif(${Sales_Amount_USD},0) - nullif(${muse_sales},0) ;;
    }

    measure: muse_sales_penetration {
      group_label: "Customer Related"
      description: "MUSE Sales Penetration"
      type: number
      hidden: no
      value_format: "0.00%"
      sql: ${muse_sales}/nullif((${muse_sales}+${non_muse_sales}), 0) ;;
    }




      measure: min_tran_date {
        hidden: yes
        type: date
        sql: min(cast(${TABLE}.atr_createdate as timestamp)) ;;
      }



}


























view: base_customers_factretailsales_cx {
  view_label: "Retail Sales"
  sql_table_name:  `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr`  ;;
  drill_fields: [id]

  dimension: id {
    label: "Transaction Item ID"
    description: "System generated unique ID for table row. i.e. transaction & item combination."
    primary_key: yes
    type: string

    sql: ${TABLE}.surrogate_key_generated_uid ;;
  }

  dimension: data_source {
    description: "Source system for the sales record."
    type: string
    sql: upper(${TABLE}.data_source) ;;
  }

  dimension: brand_customer_unique_id {
    hidden: yes
    type: string
    sql: ${TABLE}.brand_customer_unique_id ;;
  }
  dimension: _fivetran_deleted {
    hidden: yes
    type: yesno
    sql: ${TABLE}._fivetran_deleted ;;
  }

  dimension_group: _fivetran_synced {
    hidden: yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}._fivetran_synced ;;
  }

  dimension: amountlocal_beforetax {
    hidden: yes
    type: number
    sql: ${TABLE}.amountlocal_beforetax ;;
  }

  dimension: amountusd_beforetax {
    hidden: yes
    type: number
    sql: ${TABLE}.amountusd_beforetax ;;
  }


  dimension: atr_activitytype {
    hidden: yes
    type: string
    sql: ${TABLE}.atr_activitytype ;;
  }

  dimension: atr_batchid {
    hidden: yes
    type: string
    sql: ${TABLE}.atr_batchid ;;
  }

  dimension: atr_cginvoiceno {
    label: "Invoice Number"
    description: "Internally generated invoice number. May not match that generated at POS."

    type: string
    sql: ${TABLE}.atr_cginvoiceno ;;
  }

  dimension: atr_cginvoicenoline {
    hidden: yes
    type: string
    sql: ${TABLE}.atr_cginvoicenoline ;;
  }

  dimension_group: atr_createdate {
    hidden: yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.atr_createdate ;;
  }

  dimension: atr_day {
    hidden: yes
    type: number
    sql: ${TABLE}.atr_day ;;
  }

  dimension_group: atr_documentdate {
    hidden: yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.atr_documentdate ;;
  }



  dimension: atr_homecurrency {
    hidden: yes
    type: string
    sql: ${TABLE}.atr_homecurrency ;;
  }

  dimension: atr_itemseqno {
    hidden: yes
    type: number
    sql: ${TABLE}.atr_itemseqno ;;
  }



  dimension: atr_membershipid {
    hidden: yes
    type: string
    sql: ${TABLE}.atr_membershipid ;;
  }

  dimension: atr_muse_id {
    hidden: yes
    type: string
    sql: ${TABLE}.atr_muse_id ;;
  }



  dimension: atr_salesperson {
    label: "SalesPerson Staff ID"
    hidden: no
    type: string
    sql: ${TABLE}.atr_salesperson ;;
  }



  dimension: atr_storedayseqno {
    hidden: yes
    type: number
    sql: ${TABLE}.atr_storedayseqno ;;
  }

  dimension: atr_time {
    hidden: yes
    type: number
    sql: ${TABLE}.atr_time ;;
  }

  dimension: atr_timezone {
    hidden: yes
    type: string
    sql: ${TABLE}.atr_timezone ;;
  }

  extends: [period_over_period_flexible]

  dimension_group: pop_no_tz{
    sql: ${atr_trandate_date} ;;
  }

  dimension_group: atr_trandate {
    label: "Transaction"
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.atr_trandate ;;
  }


  dimension: date_group {
    label: "Date Group"
    hidden: yes
    sql: {% if base_customers_factretailsales_cx.atr_trandate_date._in_query %} CAST(${TABLE}.atr_trandate AS DATE)
        {% elsif base_customers_factretailsales_cx.atr_trandate_month._in_query %} FORMAT_DATE('%Y-%m', ${TABLE}.atr_trandate )
        {% elsif base_customers_factretailsales_cx.atr_trandate_quarter._in_query %} (FORMAT_TIMESTAMP('%Y-%m', TIMESTAMP_TRUNC(CAST(${TABLE}.atr_trandate AS TIMESTAMP), QUARTER)))
        {% elsif base_customers_factretailsales_cx.atr_trandate_week._in_query %} CONCAT(CAST(EXTRACT(YEAR FROM ${TABLE}.atr_trandate ) AS STRING),'-', CAST(EXTRACT(WEEK(SUNDAY) FROM ${TABLE}.atr_trandate) AS STRING))
        {% elsif base_customers_factretailsales_cx.atr_trandate_year._in_query %} EXTRACT(YEAR FROM ${TABLE}.atr_trandate )
        {% else %} CAST(${TABLE}.atr_trandate  AS DATE)
        {% endif %} ;;
  }


  dimension: selected_start_date {
    hidden: yes
    type: date
    sql: {% date_start atr_trandate_date %} ;;
  }


  dimension: selected_end_date {
    hidden: yes
    type: string
    sql: case when IFNULL({% date_end atr_trandate_date %},current_date())>=current_date() then current_date()


          else {% date_end atr_trandate_date %} end ;;
  }



  dimension: atr_tranno {
    hidden: yes
    type: number
    sql: ${TABLE}.atr_tranno ;;
  }

  dimension: atr_transeqno {
    hidden: yes
    type: number
    sql: ${TABLE}.atr_transeqno ;;
  }

  dimension: atr_trantype {
    hidden: yes
    type: string
    sql: ${TABLE}.atr_trantype ;;
  }

  dimension_group: atr_updatedate {
    hidden: yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.atr_updatedate ;;
  }

  dimension: av_cost_local {
    hidden: yes
    type: number
    sql: ${TABLE}.av_cost_local ;;
  }

  dimension: av_cost_usd {
    hidden: yes
    type: number
    sql: ${TABLE}.av_cost_usd ;;
  }

  dimension: bk_brand {
    hidden: yes
    type: number
    sql: ${TABLE}.bk_brand ;;
  }

  dimension: bk_businessdate {
    hidden: yes
    type: number
    sql: ${TABLE}.bk_businessdate ;;
  }

  dimension: bk_customer {
    hidden: yes
    type: string
    sql: ${TABLE}.bk_customer ;;
  }

  dimension: bk_loyalty {
    hidden: yes
    type: string
    sql: ${TABLE}.bk_loyalty ;;
  }

  dimension: bk_loyaltyprogram {
    hidden: yes
    type: string
    sql: ${TABLE}.bk_loyaltyprogram ;;
  }

  dimension: bk_productid {
    hidden: yes
    label:"Product ID"
    type: string
    sql: ${TABLE}.bk_productid ;;
  }

  dimension: bk_salestype {

    hidden: yes
    type: number
    sql: ${TABLE}.bk_salestype ;;
  }

  dimension: bk_storeid {
    label: "Store ID"
    hidden: yes
    type: number
    value_format_name: id
    sql: ${TABLE}.bk_storeid ;;
  }





  dimension: consignment_flag {
    hidden: yes
    type: string
    sql: ${TABLE}.consignment_flag ;;
  }

  dimension: conversion_rate {
    hidden: yes
    type: number
    sql: ${TABLE}.CONVERSION_RATE ;;
  }

  dimension_group: creationdatetime {
    hidden: yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.creationdatetime ;;
  }

  dimension: crm_customer_id {

    type: string
    sql: ${TABLE}.crm_customer_id ;;
  }

  dimension: customer_golden_id {
    hidden: yes
    type: string
    sql: ${TABLE}.customer_golden_id ;;
  }

  dimension: discountamt_local {
    hidden: yes

    type: number
    sql: ${TABLE}.discountamt_local ;;
  }

  dimension: discountamt_usd {

    type: number
    sql: ${TABLE}.discountamt_usd ;;
  }

  dimension: customer_unique_id {
    hidden: yes
    type: number
    sql: ${TABLE}.customer_unique_id ;;
  }

  dimension: mea_amountlocal {
    hidden: yes
    type: number
    sql: ${TABLE}.mea_amountlocal ;;
  }

  dimension: mea_amountusd {
    hidden: yes
    type: number
    sql: ${TABLE}.mea_amountusd ;;
  }

  dimension: mea_quantity {
    hidden: yes
    type: number
    sql: ${TABLE}.mea_quantity ;;
  }

  dimension: min_brand_id {
    hidden: yes
    type: number
    sql: ${TABLE}.min_brand_id ;;
  }

  dimension: pos_customer_id {
    hidden: yes
    type: string
    sql: ${TABLE}.pos_customer_id ;;
  }

  dimension: register {
    hidden: yes
    type: string
    sql: ${TABLE}.register ;;
  }

  dimension: sales_channel {
    description:"For sales not through usual channel. e.g WhatsApp, Concierge etc."
    type: string
    sql: ${TABLE}.sales_channel ;;
  }

  dimension: taxamt_local {
    hidden: yes
    type: number
    sql: ${TABLE}.taxamt_local ;;
  }

  dimension: taxamt_usd {
    hidden: yes
    type: number
    sql: ${TABLE}.taxamt_usd ;;
  }

  dimension: is_discount_sale {
    hidden: yes
    type: yesno
    sql: ifnull(${discountamt_usd},0)<>0 ;;
  }

  dimension: is_muse_txn{

    label: "Is Muse Txn"
    type: yesno
    sql: ${atr_muse_id} is not null;;
  }
  dimension: is_crm_txn{

    label: "Is CRM Txn"
    type: yesno
    sql:  ${crm_customer_id} is not null ;;
  }

  dimension: is_known_cust_txn{

    description: "Sale attributed to a known customer (i.e. CRM or Muse)"
    type: yesno
    sql:  ${customer_unique_id} is not null ;;
  }

  dimension: is_known_cust_txn_ex_generic{

    description: "Sale attributed to a known customer (i.e. CRM or Muse)"
    type: yesno
    sql:  ${customer_unique_id} is not null and ${is_generic_customer}=false ;;
  }


  dimension: is_generic_customer   {
    label: "Is Generic ID Txn"
    type: yesno
    sql: ${TABLE}.is_generic_customer ;;
  }


  dimension: is_muse_signup_txn {
    description: "Is transaction associated with a Muse Member Sign-Up"
    type: yesno
    sql: case when ${transaction_attributes.is_muse_regi_txn} is null then false else true end ;;
  }


  dimension: new_cust_brand {
    hidden: yes
    type: number
    sql: case when ${TABLE}.new_cust_brand = 'New' then 1 else 0 end ;;

  }

  dimension: new_cust_group {
    hidden: yes
    type: number
    sql: case when ${TABLE}.new_cust_group = 'New' then 1 else 0 end ;;

  }

  measure: count_new_cust_brand {
    group_label: "Customer Related"
    type: sum_distinct
    sql_distinct_key: concat(${brand_customer_unique_id},${bk_businessdate});;
    sql: ${new_cust_brand} ;;
  }

  measure: count_returning_cust_brand {
    group_label: "Customer Related"
    type: number
    sql: ${brand_customer_key_count}-${count_new_cust_brand} ;;
  }

  measure: count_new_cust_group {
    group_label: "Customer Related"
    type: sum_distinct
    sql_distinct_key: concat(${customer_unique_id},${bk_businessdate});;
    sql: ${new_cust_group} ;;
  }

  measure: count_returning_cust_group {
    group_label: "Customer Related"
    type: number
    sql: ${customer_key_count}-${count_new_cust_group} ;;
  }




  parameter: include_tax_select {
    type: unquoted
    description: "Choose whether all sales metrics include sales tax or not"
    allowed_value: {
      label: "Sales Including Tax"
      value: "inc"
    }
    allowed_value: {
      label: "Sales Excluding Tax"
      value: "exc"
    }
    default_value: "inc"
  }





  measure: count_of_discount_sales{

    type: count_distinct
    sql: ${atr_cginvoiceno} ;;

    filters: {
      field: is_discount_sale
      value: "yes"  }
  }

  measure: proportion_of_discount_sales{

    description: "Percentage of transactions which included a discount"
    type: number
    value_format: "0.00%"
    sql: IFNULL((${count_of_discount_sales})/NULLIF(${transaction_count},0),0) ;;
  }

  measure: count {
    hidden: yes
    type: count
    drill_fields: [id]
  }

  measure: Item_Count  {
    description: "Aggregate quantity of units sold"
    type: sum
    sql: ${mea_quantity} ;;
  }

  measure:  transaction_count{
    description: "Purchases, Returns, or Exchanges made by a customer in a store or online"
    type: count_distinct
    sql: ${atr_cginvoiceno} ;;
  }

  measure: Sales_Amount_USD {
    type: sum
    description: "Net sales is the sum of gross sales minus its returns, allowances, and discounts (without VAT)"
    value_format: "$#,##0;($#.00)"
    sql:
    {% if include_tax_select._parameter_value == 'exc' %}

     ${amountusd_beforetax}
    {% else %}
    ${mea_amountusd}
    {% endif %}

     ;;
  }
  measure: UPT {
    group_label: "Basket Measures"
    label: "UPT"
    value_format: "0.##"
    description: "Average units per transaction"
    type: number
    sql: ${Item_Count}/nullif(${transaction_count},0) ;;
  }

  measure: VPT {
    group_label: "Basket Measures"
    label: "AOV"
    value_format: "$0"
    description: "Average Value per Order"
    type: number
    sql: ${Sales_Amount_USD}/nullif(${transaction_count},0) ;;
  }

  measure: customer_key_count {
    group_label: "Customer Related"
    label: "Customer Unique Count (Transacted)"
    description: "A person who buys goods across brands part of Chalhoub Group"
    hidden: no
    type: count_distinct

    sql:  ${TABLE}.customer_unique_id;;
  }

  measure: brand_customer_key_count {
    group_label: "Customer Related"
    label: "Brand Customer Unique Count (Transacted)"
    description: "A person who buys goods across brands part of Chalhoub Group"
    hidden: no
    type: count_distinct

    sql:  ${TABLE}.brand_customer_unique_id;;
  }

  measure:  Known_Cust_Tran_Count{
    hidden: yes
    type: count_distinct
    sql: case when ${brand_customer_unique_id} is null then null else ${atr_cginvoiceno} end;;

  }

  measure: customer_key_count_RFM {

    group_label: "Customer Related"
    label: "Customer Count for RFM Analysis ONLY"
    hidden: yes
    type: count_distinct
    drill_fields: [Sales_Amount_USD]
    sql:  ${TABLE}.brand_customer_unique_id;;
    html:{% if base_customers_factretailsales.Sales_Amount_USD._value < 20000 %}
          <div style="background-color: rgba(240,248,255,{{value}}); font-size:100%; text-align:center"><a href="{{ link }}" style="color: black" target="_new">{{ customer_key_count_RFM._rendered_value }} </a></div>
          {% elsif base_customers_factretailsales.Sales_Amount_USD._value >= 20000 and base_customers_factretailsales.Sales_Amount_USD._value < 60000 %}
          <div style="background-color: rgba(173,216,230,{{value}}); font-size:100%; text-align:center"><a href="{{ link }}" style="color: white" target="_new">{{ customer_key_count_RFM._rendered_value }} </a></div>
           {% elsif base_customers_factretailsales.Sales_Amount_USD._value >= 60000 and base_customers_factretailsales.Sales_Amount_USD._value < 100000 %}
          <div style="background-color: rgba(135,206,250,{{value}}); font-size:100%; text-align:center"><a href="{{ link }}" style="color: white" target="_new">{{ customer_key_count_RFM._rendered_value }} </a></div>
            {% elsif base_customers_factretailsales.Sales_Amount_USD._value >= 100000 and base_customers_factretailsales.Sales_Amount_USD._value < 400000 %}
          <div style="background-color: rgba(30,144,255,{{value}}); font-size:100%; text-align:center"><a href="{{ link }}" style="color: white" target="_new">{{ customer_key_count_RFM._rendered_value }} </a></div>
           {% elsif base_customers_factretailsales.Sales_Amount_USD._value >= 400000 and base_customers_factretailsales.Sales_Amount_USD._value < 800000 %}
          <div style="background-color: rgba(65,105,225,{{value}}); font-size:100%; text-align:center"><a href="{{ link }}" style="color: white" target="_new">{{ customer_key_count_RFM._rendered_value }} </a></div>
         {% elsif base_customers_factretailsales.Sales_Amount_USD._value >= 800000 and base_customers_factretailsales.Sales_Amount_USD._value < 1100000 %}
          <div style="background-color: rgba(0,0,255,{{value}}); font-size:100%; text-align:center"><a href="{{ link }}" style="color: white" target="_new">{{ customer_key_count_RFM._rendered_value }} </a></div>
      {% elsif base_customers_factretailsales.Sales_Amount_USD._value >= 1100000 and base_customers_factretailsales.Sales_Amount_USD._value < 3000000 %}
          <div style="background-color: rgba(25,35,150,{{value}}); font-size:100%; text-align:center"><a href="{{ link }}" style="color: white" target="_new">{{ customer_key_count_RFM._rendered_value }} </a></div>
      {% elsif base_customers_factretailsales.Sales_Amount_USD._value >= 3000000 and base_customers_factretailsales.Sales_Amount_USD._value < 6000000 %}
          <div style="background-color: rgba(0,0,128,{{value}}); font-size:100%; text-align:center"><a href="{{ link }}" style="color: white" target="_new">{{ customer_key_count_RFM._rendered_value }} </a></div>
       {% else %}
          <div style="background-color: rgba(0,0,128,0.99); font-size:100%; text-align:center"><a href="{{ link }}" style="color: white" target="_new">{{ customer_key_count_RFM._rendered_value }} </a></div>
          {% endif %}
          ;;}

      measure: customer_CRM_count {
        group_label: "Customer Related"
        hidden: yes
        label: "CRM member Unique Count (Transacted)"

        type: count_distinct
        sql:  ${TABLE}.crm_customer_id ;;
      }


  measure:  brand_visits {
    description: "How many brands the customer visited"
    hidden: no
    type: count_distinct
    sql: ${min_brand_id}  ;;
  }

  dimension: test {
    type: string
    sql: select 1 ;;
  }


      measure: Average_Frequency {
        group_label: "Customer Related"
        description: "Purchase frequency is the average number of purchases made by a customer over a given period of time"
        type: number
        value_format: "0.##"
        sql: ${Known_Cust_Tran_Count}/NULLIF(${customer_key_count},0) ;;
      }



      # measure: AUR{
      #   group_label: "Basket Measures"
      #   label: "AUR"
      #   value_format: "$0"
      #   description: "Average value per item"
      #   type: number
      #   sql: ${Sales_Amount_USD}/NULLIF(${Item_Count},0) ;;
      # }




      measure: know_customer_sales {
        hidden: yes
        group_label: "Customer Related"
        description: "Sales attributed to a known customer"
        value_format: "$#,##0"
        type: sum
        sql:
              CASE
                WHEN nullif(${TABLE}.brand_customer_unique_id,'') is null or ${is_generic_customer}=true then 0 else (    {% if include_tax_select._parameter_value == 'exc' %}

     ${amountusd_beforetax}
    {% else %}
    ${mea_amountusd}
    {% endif %})

              END ;;
      }

      measure: unknow_sales {
        hidden: yes
        group_label: "Customer Related"
        value_format: "$#,##0"
        description: "Sales not linked to a known customer"

        type: sum
        sql:
              CASE
                WHEN nullif(${TABLE}.brand_customer_unique_id,'') is null then (    {% if include_tax_select._parameter_value == 'exc' %}

     ${amountusd_beforetax}
    {% else %}
    ${mea_amountusd}
    {% endif %}) else 0

              END ;;
      }

      measure: percentage_known_sales{
        hidden: yes
        description: "Known customer sales (excl. generic) / Total sales"
        group_label: "Customer Related"
        type: number
        value_format: "0.00%"
        sql:  ${know_customer_sales}/nullif(${Sales_Amount_USD},0};;
      }

      measure: average_spend {
        group_label: "Customer Related"
        description: "Average Net sales for a given customer over a given period of time"
        type: number
        value_format: "$#,##0"
        sql: ${know_customer_sales}/nullif(${customer_key_count},0) ;;
      }

      measure: muse_sales {
        group_label: "Customer Related"
        description: "Sales attributed to Muse member"
        type: sum
        value_format: "$#,##0"
        sql: case when ${atr_muse_id} is not null then (    {% if include_tax_select._parameter_value == 'exc' %}

               ${amountusd_beforetax}
              {% else %}
              ${mea_amountusd}
              {% endif %})
                    else 0 end ;;

        }

        measure: non_muse_sales {
          group_label: "Customer Related"
          description: "Non-MUSE Sales"
          type: number
          hidden: no
          value_format: "$#,##0"
          sql:  nullif(${Sales_Amount_USD},0) - nullif(${muse_sales},0) ;;
        }

        measure: muse_sales_penetration {
          group_label: "Customer Related"
          description: "MUSE Sales Penetration"
          type: number
          hidden: no
          value_format: "0.00%"
          sql: ${muse_sales}/nullif((${muse_sales}+${non_muse_sales}), 0) ;;
        }




        measure: min_tran_date {
          hidden: yes
          type: date
          sql: min(cast(${TABLE}.atr_createdate as timestamp)) ;;
        }





      }



view: base_customers_factretailsales_v2 {
  extends: [base_customers_factretailsales]



  dimension_group: atr_trandate {
    label: "Transaction"
    hidden: yes
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.atr_trandate ;;
  }



  measure: count_retained_customers {
    description: "Count of brand customers retained from the previous period.  Only works with Retention Period in Customer view."
    type: count_distinct
    group_label: "Customer Related"
    sql: case when ${base_customers_distinct_v2.retained_customer} = true then ${base_customers_distinct_v2.customer_unique_id}
      else null end;;


  }


  measure: estimated_muse_sales_goldenrecord {
    group_label: "Customer Related"
    description: "Estimated MUSE sales - based on golden record logic of linking MUSE IDs"
    type: sum
    value_format: "$#,##0"
    sql: case when ${base_customers_distinct_v2.muse_member_id} is not null then (    {% if include_tax_select._parameter_value == 'exc' %}

                 ${amountusd_beforetax}
                {% else %}
                ${mea_amountusd}
                {% endif %})
                      else 0 end ;;

    }




  dimension: date_group {
    label: "Date Group"
    hidden: yes
    sql: {% if base_customers_factretailsales_v2.atr_trandate_date._in_query %} CAST(${TABLE}.atr_trandate AS DATE)
        {% elsif base_customers_factretailsales_v2.atr_trandate_month._in_query %} FORMAT_DATE('%Y-%m', ${TABLE}.atr_trandate )
        {% elsif base_customers_factretailsales_v2.atr_trandate_quarter._in_query %} (FORMAT_TIMESTAMP('%Y-%m', TIMESTAMP_TRUNC(CAST(${TABLE}.atr_trandate AS TIMESTAMP), QUARTER)))
        {% elsif base_customers_factretailsales_v2.atr_trandate_week._in_query %} CONCAT(CAST(EXTRACT(YEAR FROM ${TABLE}.atr_trandate ) AS STRING),'-', CAST(EXTRACT(WEEK(SUNDAY) FROM ${TABLE}.atr_trandate) AS STRING))
        {% elsif base_customers_factretailsales_v2.atr_trandate_year._in_query %} EXTRACT(YEAR FROM ${TABLE}.atr_trandate )
        {% else %} CAST(${TABLE}.atr_trandate  AS DATE)
        {% endif %} ;;
  }



  dimension: selected_start_date {
hidden: no
    type: date
    sql: {% date_start atr_trandate_date %} ;;
  }

  dimension: selected_end_date {
    hidden: no
    type: string
    sql: case when IFNULL({% date_end atr_trandate_date %},current_date())>=current_date() then current_date()


          else {% date_end atr_trandate_date %} end ;;
  }

  measure: max_date {
    hidden: no
    type: max
    sql: ${bk_businessdate} ;;
  }

  measure: min_date {
    hidden: no
    type: min
    sql: ${bk_businessdate} ;;
  }

measure: days_in_period2 {
  type: number
  sql:  DATE_DIFF (PARSE_DATE("%Y%m%d", cast(${max_date} as string)),
          PARSE_DATE("%Y%m%d",cast(${min_date} as string)), DAY) +1  ;;
}


  }



view: golden_circle_txn {
  view_label: "Retail Sales"
  sql_table_name:  `chb-prod-data-cust.prod_Customer360.golden_circle_transactions`  ;;


  dimension: bk_storeid{
    hidden: yes
    sql: ${TABLE}.bk_storeid ;;
  }

  dimension: bk_businessdate {
    hidden:yes
    sql: ${TABLE}.bk_businessdate ;;
  }

  dimension: atr_tranno {
    hidden: yes
    sql: ${TABLE}.atr_tranno ;;
  }

  dimension:golden_circle_promo_flag  {
    description: "Is the transacation linked to the Golden Circle promotion."
    label: "Is Golden Circ. txn"
    type: yesno
    sql: ${TABLE}.golden_circle_promo_flag ;;
  }

  }
