view: cx_trainings {
  derived_table: {
    sql: SELECT
          locations.district_name  AS locations_district_name,
          locations.country_desc  AS locations_country_desc
      FROM `chb-prod-data-cust.prod_base_customers_final.base_customers_distinct`

           AS base_customers_focus
      LEFT JOIN `chb-prod-data-cust.prod_Customer360.cx_responses_denorm`
           AS cx_responses_denorm ON base_customers_focus.brand_customer_unique_id=cx_responses_denorm.brand_customer_unique_id

      INNER JOIN `chb-prod-supplychain-data.prod_shared_dimensions.dim_retail_loc`  AS locations ON cx_responses_denorm.store_id_c=cast(locations.store as string)
      INNER JOIN `chb-prod-supplychain-data.prod_shared_dimensions.dim_alternate_bu_hierarchy`  AS dim_alternate_bu_hierarchy ON cast(locations.store as string)=dim_alternate_bu_hierarchy.store_code
      WHERE (NOT (base_customers_focus.is_generic_customer ) OR (base_customers_focus.is_generic_customer ) IS NULL) AND (dim_alternate_bu_hierarchy.brand ) IS NOT NULL
      GROUP BY
          1,
          2
      ORDER BY
          1
      LIMIT 500
       ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  dimension: locations_district_name {
    label: "CX Trainings Brand"
    #hidden: yes
    type: string
    sql: ${TABLE}.locations_district_name ;;
  }

  dimension: locations_country_desc {
    label: "CX Trainings Country"
    #hidden:  yes
    type: string
    sql: ${TABLE}.locations_country_desc ;;
  }

  set: detail {
    fields: [locations_district_name, locations_country_desc]
  }

  measure: numbers_attended {
    label: "Numbers Attended"
    description: "Number of FOH staff that have completed the CX trainings(Level 2)"
    type:  number
    sql:
    case
      when  ${locations_district_name} LIKE "%SWAROVSKI%" and ${locations_country_desc} LIKE "%Saudi Arabia%" THEN 113
      when ${locations_district_name} LIKE "%SWAROVSKI%" and ${locations_country_desc} LIKE "%United Arab Emirates%" THEN 73
      when ${locations_district_name} LIKE "%FACES%" and ${locations_country_desc} LIKE "%Saudi Arabia%" THEN 147
      when ${locations_district_name} LIKE "%FACES%" and ${locations_country_desc} LIKE "%United Arab Emirates%" THEN 11
      when ${locations_district_name} LIKE "%MICHAEL KORS%" and ${locations_country_desc} LIKE "%Saudi Arabia%" THEN 28
      when ${locations_district_name} LIKE "%MICHAEL KORS%" and ${locations_country_desc} LIKE "%United Arab Emirates%" THEN 55
      when ${locations_district_name} LIKE "%TORY BURCH%" and ${locations_country_desc} LIKE "%Saudi Arabia%" THEN 35
      when ${locations_district_name} LIKE "%TORY BURCH%" and ${locations_country_desc} LIKE "%United Arab Emirates%" THEN 31
      when ${locations_district_name} LIKE "%LEVEL SHOES%" and ${locations_country_desc} LIKE "%United Arab Emirates%" THEN 49
      when ${locations_district_name} LIKE "%LACOSTE%" and ${locations_country_desc} LIKE "%Saudi Arabia%" THEN 30
      when ${locations_district_name} LIKE "%LACOSTE%" and ${locations_country_desc} LIKE "%United Arab Emirates%" THEN 38
      else null end
      ;;

    }

  measure: numbers_ongoing {
    label: "Numbers Ongoing"
    description: "Number of FOH staff that have not completed the CX trainings(Level 2)"
    type:  number
    sql:
    case
      when  ${locations_district_name} LIKE "%SWAROVSKI%" and ${locations_country_desc} LIKE "%Saudi Arabia%" and ${numbers_attended} = 113 THEN 21
      when ${locations_district_name} LIKE "%SWAROVSKI%" and ${locations_country_desc} LIKE "%United Arab Emirates%" and ${numbers_attended} = 73 THEN 14
      when ${locations_district_name} LIKE "%FACES%" and ${locations_country_desc} LIKE "%Saudi Arabia%" and ${numbers_attended} = 147 THEN 110
      when ${locations_district_name} LIKE "%FACES%" and ${locations_country_desc} LIKE "%United Arab Emirates%" and ${numbers_attended} = 11 THEN 4
      when ${locations_district_name} LIKE "%MICHAEL KORS%" and ${locations_country_desc} LIKE "%Saudi Arabia%" and ${numbers_attended} = 28 THEN 7
      when ${locations_district_name} LIKE "%MICHAEL KORS%" and ${locations_country_desc} LIKE "%United Arab Emirates%" and ${numbers_attended} = 55 THEN 8
      when ${locations_district_name} LIKE "%TORY BURCH%" and ${locations_country_desc} LIKE "%Saudi Arabia%" and ${numbers_attended} = 35 THEN 1
      when ${locations_district_name} LIKE "%TORY BURCH%" and ${locations_country_desc} LIKE "%United Arab Emirates%" and ${numbers_attended} = 31 THEN 3
      when ${locations_district_name} LIKE "%LEVEL SHOES%" and ${locations_country_desc} LIKE "%United Arab Emirates%" and ${numbers_attended} = 49 THEN 61
      when ${locations_district_name} LIKE "%LACOSTE%" and ${locations_country_desc} LIKE "%Saudi Arabia%" and ${numbers_attended} = 30 THEN 13
      when ${locations_district_name} LIKE "%LACOSTE%" and ${locations_country_desc} LIKE "%United Arab Emirates%" and ${numbers_attended} = 38 THEN 13
      else null end
      ;;

    }



}
