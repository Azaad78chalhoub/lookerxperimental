view: ecomm_customer_transactional_pop {
  view_label: "Ecomm Customer New Returning"
  derived_table: {
    sql: WITH txn_base AS (
    SELECT
      txn.brand,
      txn.customer_key,
      txn.order_date_local,
      txn.original_order_id,
{% if _explore._name == "tryano" %}

      {% if tryano.common_date._in_query %} CAST(order_date_local  AS DATE)
      {% elsif tryano.common_month._in_query %} FORMAT_DATE('%Y-%m', order_date_local )
      {% elsif tryano.common_quarter._in_query %} (FORMAT_TIMESTAMP('%Y-%m', TIMESTAMP_TRUNC(CAST(order_date_local AS TIMESTAMP), QUARTER)))
      {% elsif tryano.common_week._in_query %} CONCAT(CAST(EXTRACT(YEAR FROM order_date_local ) AS STRING),'-', CAST(EXTRACT(WEEK(SUNDAY) FROM order_date_local) AS STRING))
      {% elsif tryano.common_year._in_query %} EXTRACT(YEAR FROM order_date_local )
      {% else %} CAST(order_date_local  AS DATE)
      {% endif %}

{% elsif _explore._name == "swarovski" %}

      {% if swarovski.common_date_date._in_query %} CAST(order_date_local  AS DATE)
      {% elsif swarovski.common_date_month._in_query %} FORMAT_DATE('%Y-%m', order_date_local )
      {% elsif swarovski.common_date_quarter._in_query %} (FORMAT_TIMESTAMP('%Y-%m', TIMESTAMP_TRUNC(CAST(order_date_local AS TIMESTAMP), QUARTER)))
      {% elsif swarovski.common_date_week._in_query %} CONCAT(CAST(EXTRACT(YEAR FROM order_date_local ) AS STRING),'-', CAST(EXTRACT(WEEK(SUNDAY) FROM order_date_local) AS STRING))
      {% elsif swarovski.common_date_year._in_query %} EXTRACT(YEAR FROM order_date_local )
      {% else %} CAST(order_date_local  AS DATE)
      {% endif %}


{% elsif _explore._name == "faces" %}

      {% if faces.common_date._in_query %} CAST(order_date_local  AS DATE)
      {% elsif faces.common_month._in_query %} FORMAT_DATE('%Y-%m', order_date_local )
      {% elsif faces.common_quarter._in_query %} (FORMAT_TIMESTAMP('%Y-%m', TIMESTAMP_TRUNC(CAST(order_date_local AS TIMESTAMP), QUARTER)))
      {% elsif faces.common_week._in_query %} CONCAT(CAST(EXTRACT(YEAR FROM order_date_local ) AS STRING),'-', CAST(EXTRACT(WEEK(SUNDAY) FROM order_date_local) AS STRING))
      {% elsif faces.common_year._in_query %} EXTRACT(YEAR FROM order_date_local )
      {% else %} CAST(order_date_local  AS DATE)
      {% endif %}

{% elsif _explore._name == "stg_comm_chanel_faces_ga_landing_pages" %}

      {% if stg_comm_chanel_faces_ga_landing_pages.common_date._in_query %} CAST(order_date_local  AS DATE)
      {% elsif stg_comm_chanel_faces_ga_landing_pages.common_month._in_query %} FORMAT_DATE('%Y-%m', order_date_local )
      {% elsif stg_comm_chanel_faces_ga_landing_pages.common_quarter._in_query %} (FORMAT_TIMESTAMP('%Y-%m', TIMESTAMP_TRUNC(CAST(order_date_local AS TIMESTAMP), QUARTER)))
      {% elsif stg_comm_chanel_faces_ga_landing_pages.common_week._in_query %} CONCAT(CAST(EXTRACT(YEAR FROM order_date_local ) AS STRING),'-', CAST(EXTRACT(WEEK(SUNDAY) FROM order_date_local) AS STRING))
      {% elsif stg_comm_chanel_faces_ga_landing_pages.common_year._in_query %} EXTRACT(YEAR FROM order_date_local )
      {% else %} CAST(order_date_local  AS DATE)
      {% endif %}

{% elsif _explore._name == "lvl_facts_union" %}

      {% if lvl_facts_union.common_date._in_query %} CAST(order_date_local  AS DATE)
      {% elsif lvl_facts_union.common_month._in_query %} FORMAT_DATE('%Y-%m', order_date_local )
      {% elsif lvl_facts_union.common_quarter._in_query %} (FORMAT_TIMESTAMP('%Y-%m', TIMESTAMP_TRUNC(CAST(order_date_local AS TIMESTAMP), QUARTER)))
      {% elsif lvl_facts_union.common_week._in_query %} CONCAT(CAST(EXTRACT(YEAR FROM order_date_local ) AS STRING),'-', CAST(EXTRACT(WEEK(SUNDAY) FROM order_date_local) AS STRING))
      {% elsif lvl_facts_union.common_year._in_query %} EXTRACT(YEAR FROM order_date_local )
      {% else %} CAST(order_date_local  AS DATE)
      {% endif %}

{% elsif _explore._name == "ecomm_orders_new_vs_returning_pop" %}
    {% if ecomm_customer_transactional_pop.order_date_local_date._in_query %} CAST(order_date_local  AS DATE)
      {% elsif ecomm_customer_transactional_pop.order_date_local_month._in_query %} FORMAT_DATE('%Y-%m', order_date_local )
      {% elsif ecomm_customer_transactional_pop.order_date_local_quarter._in_query %} (FORMAT_TIMESTAMP('%Y-%m', TIMESTAMP_TRUNC(CAST(order_date_local AS TIMESTAMP), QUARTER)))
      {% elsif ecomm_customer_transactional_pop.order_date_local_week._in_query %} CONCAT(CAST(EXTRACT(YEAR FROM order_date_local ) AS STRING),'-', CAST(EXTRACT(WEEK(SUNDAY) FROM order_date_local) AS STRING))
      {% elsif ecomm_customer_transactional_pop.order_date_local_year._in_query %} EXTRACT(YEAR FROM order_date_local )
      {% else %} CAST(order_date_local  AS DATE) {% endif %}

{% else %}
    {% if ecomm_customer_transactional_pop.order_date_local_date._in_query %} CAST(order_date_local  AS DATE)
      {% elsif ecomm_customer_transactional_pop.order_date_local_month._in_query %} FORMAT_DATE('%Y-%m', order_date_local )
      {% elsif ecomm_customer_transactional_pop.order_date_local_quarter._in_query %} (FORMAT_TIMESTAMP('%Y-%m', TIMESTAMP_TRUNC(CAST(order_date_local AS TIMESTAMP), QUARTER)))
      {% elsif ecomm_customer_transactional_pop.order_date_local_week._in_query %} CONCAT(CAST(EXTRACT(YEAR FROM order_date_local ) AS STRING),'-', CAST(EXTRACT(WEEK(SUNDAY) FROM order_date_local) AS STRING))
      {% elsif ecomm_customer_transactional_pop.order_date_local_year._in_query %} EXTRACT(YEAR FROM order_date_local )
      {% else %} CAST(order_date_local  AS DATE) {% endif %}

{% endif %} AS date_group,
      txn.order_id,
      txn.order_status,
      txn.gross_net_sales_usd_total,
      txn.gross_revenue_usd_total,
      txn.customer_city,
      txn.customer_country,
      txn.store_id,
      txn.is_muse_member
    FROM `chb-prod-data-cust.prod_ecomm_cust.ecomm_orders_w_first_txn_date` txn
  )
    SELECT DISTINCT
      brand,
      customer_key,
      order_date_local,
      date_group,
      order_id,
      order_status,
      gross_net_sales_usd_total,
      gross_revenue_usd_total,
      customer_city,
      customer_country,
      original_order_id,
      store_id,
      is_muse_member
    FROM txn_base ;;
  }
  dimension: pk {
    hidden: yes
    primary_key: yes
    type: string
    sql: CONCAT(${TABLE}.brand, ${TABLE}.customer_key, ${TABLE}.order_id, ${TABLE}.order_date_local)  ;;
  }

  dimension: customer_key {
    hidden: no
    label: "Customer Key"
    description: "Unique Customer Email at Brand level (concatenation of Brand Name and Customer Email"
    type: string
    sql: ${TABLE}.customer_key ;;
  }

  dimension: order_id {
    hidden: no
    label: "Order ID"
    description: "Concatenation of Brand Name and Original Order ID from ecom platform"
    type: string
    sql: ${TABLE}.order_id ;;
  }

  dimension: brand {
    hidden: no
    label: "Brand"
    type: string
    sql: ${TABLE}.brand ;;
  }

  dimension: date_group {
    hidden: yes
    label: "Date Group"
    type: string
    sql: ${TABLE}.date_group ;;
  }

  dimension: date {type:date hidden:yes sql:${TABLE}.date;;}
  dimension: week {type:string hidden:yes sql:${TABLE}.date;;}
  dimension: month {type:string hidden:yes sql:${TABLE}.date;;}
  dimension: quarter {type:string hidden:yes sql:${TABLE}.date;;}
  dimension: year {type:number hidden:yes sql:${TABLE}.date;;}

  dimension_group: order_date_local {
    label: "Order"
    hidden: no
    type: time
    datatype: date
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year

    ]
    sql: ${TABLE}.order_date_local ;;
  }

  dimension: order_status {
    hidden: no
    label: "Order Status"
    description: "SALES or REFUNDED"
    type: string
    sql: ${TABLE}.order_status ;;
  }

  dimension: customer_city {
    hidden: no
    label: "Customer City"
    description: "City where order has been delivered"
    type: string
    sql: ${TABLE}.customer_city ;;
  }

  dimension: customer_country {
    hidden: no
    label: "Customer Country"
    description: "Country where order has been delivered"
    type: string
    sql: ${TABLE}.customer_country ;;
  }

  dimension: store_id {
    hidden: yes
    label: "Store ID"
    type: string
    sql: ${TABLE}.store_id ;;
  }

  dimension: is_muse_member {
    hidden: no
    label: "Is Muse Member"
    type: string
    sql: ${TABLE}.is_muse_member ;;
  }


  dimension: original_order_id {
    hidden: no
    label: "Orignal Order ID"
    description: "Original Order ID from ecom platform"
    type: string
    sql: ${TABLE}.original_order_id ;;
  }

  measure: gross_net_sales_usd_total {
    label: "Net Sales Amount USD"
    type: sum
    description: "Net sales is the sum of gross sales minus its returns, allowances, and discounts (without VAT)"
    value_format: "$#,##0"
    sql: ${TABLE}.gross_net_sales_usd_total ;;
  }

  measure: gross_revenue_usd_total {
    label: "Gross Sales Amount USD"
    description: "Gross sales are the grand total of all sale transactions reported in a period, without any deductions included within the figure"
    type: sum
    value_format: "$#,##0"
    sql: ${TABLE}.gross_revenue_usd_total ;;
  }

  measure: count_unique_customers {
    label: "Customer Unique Count"
    description: "Unique Count of customers who transacted online"
    type: count_distinct
    sql: ${customer_key} ;;
  }

  measure: count_orders {
    label: "Order Count"
    description: "Purchases, Returns, or Exchanges made by a customer in a store or online."
    type: count_distinct
    sql: ${order_id} ;;
  }

  measure: aov_ {
    label: "AOV"
    description: "AOV stands for Average Value per Order. Calculation based on Gross Sales Amount USD"
    value_format: "$#,##0"
    type: number
    sql: IFNULL(${gross_revenue_usd_total}/NULLIF(${count_orders},0),0) ;;

  }

  measure: frequency {
    label: "Average Frequency"
    description: "Purchase frequency is the average number of purchases made by a customer over a given period of time"
    type: number
    value_format: "0.00"
    sql: IFNULL(${count_orders}/NULLIF(${count_unique_customers},0),0) ;;
  }

  measure:  avg_spend{
    label: "Average Spend"
    description: "Average Gross sales for a given customer over a given period of time. Calculation based on Gross Sales Amount USD"
    value_format: "$#"
    type: number
    sql: ROUND(${gross_revenue_usd_total}/${count_unique_customers}) ;;
  }

}


view: ecomm_customer_type_pop {
  view_label: "Ecomm Customer New Returning"
  derived_table: {
    sql:

    SELECT
    DISTINCT
      brand,
      customer_key,
      date_group,
      customer_type
    FROM

    (SELECT
      customer_key,
      brand,
{% if _explore._name == "tryano" %}

      {% if tryano.common_date._in_query %} CAST(order_date_local  AS DATE)
      {% elsif tryano.common_month._in_query %} FORMAT_DATE('%Y-%m', order_date_local )
      {% elsif tryano.common_quarter._in_query %} (FORMAT_TIMESTAMP('%Y-%m', TIMESTAMP_TRUNC(CAST(order_date_local AS TIMESTAMP), QUARTER)))
      {% elsif tryano.common_week._in_query %} CONCAT(CAST(EXTRACT(YEAR FROM order_date_local ) AS STRING),'-', CAST(EXTRACT(WEEK(SUNDAY) FROM order_date_local) AS STRING))
      {% elsif tryano.common_year._in_query %} EXTRACT(YEAR FROM order_date_local )
      {% else %} CAST(order_date_local  AS DATE)
      {% endif %}

{% elsif _explore._name == "stg_comm_swarovski_combined" %}

      {% if stg_comm_swarovski_combined.common_date_date._in_query %} CAST(order_date_local  AS DATE)
      {% elsif stg_comm_swarovski_combined.common_date_month._in_query %} FORMAT_DATE('%Y-%m', order_date_local )
      {% elsif stg_comm_swarovski_combined.common_date_quarter._in_query %} (FORMAT_TIMESTAMP('%Y-%m', TIMESTAMP_TRUNC(CAST(order_date_local AS TIMESTAMP), QUARTER)))
      {% elsif stg_comm_swarovski_combined.common_date_week._in_query %} CONCAT(CAST(EXTRACT(YEAR FROM order_date_local ) AS STRING),'-', CAST(EXTRACT(WEEK(SUNDAY) FROM order_date_local) AS STRING))
      {% elsif stg_comm_swarovski_combined.common_date_year._in_query %} EXTRACT(YEAR FROM order_date_local )
      {% else %} CAST(order_date_local  AS DATE)
      {% endif %}


{% elsif _explore._name == "faces" %}

      {% if faces.common_date._in_query %} CAST(order_date_local  AS DATE)
      {% elsif faces.common_month._in_query %} FORMAT_DATE('%Y-%m', order_date_local )
      {% elsif faces.common_quarter._in_query %} (FORMAT_TIMESTAMP('%Y-%m', TIMESTAMP_TRUNC(CAST(order_date_local AS TIMESTAMP), QUARTER)))
      {% elsif faces.common_week._in_query %} CONCAT(CAST(EXTRACT(YEAR FROM order_date_local ) AS STRING),'-', CAST(EXTRACT(WEEK(SUNDAY) FROM order_date_local) AS STRING))
      {% elsif faces.common_year._in_query %} EXTRACT(YEAR FROM order_date_local )
      {% else %} CAST(order_date_local  AS DATE)
      {% endif %}

{% elsif _explore._name == "stg_comm_chanel_faces_ga_landing_pages" %}

      {% if stg_comm_chanel_faces_ga_landing_pages.common_date._in_query %} CAST(order_date_local  AS DATE)
      {% elsif stg_comm_chanel_faces_ga_landing_pages.common_month._in_query %} FORMAT_DATE('%Y-%m', order_date_local )
      {% elsif stg_comm_chanel_faces_ga_landing_pages.common_quarter._in_query %} (FORMAT_TIMESTAMP('%Y-%m', TIMESTAMP_TRUNC(CAST(order_date_local AS TIMESTAMP), QUARTER)))
      {% elsif stg_comm_chanel_faces_ga_landing_pages.common_week._in_query %} CONCAT(CAST(EXTRACT(YEAR FROM order_date_local ) AS STRING),'-', CAST(EXTRACT(WEEK(SUNDAY) FROM order_date_local) AS STRING))
      {% elsif stg_comm_chanel_faces_ga_landing_pages.common_year._in_query %} EXTRACT(YEAR FROM order_date_local )
      {% else %} CAST(order_date_local  AS DATE)
      {% endif %}

{% elsif _explore._name == "lvl_facts_union" %}

      {% if lvl_facts_union.common_date._in_query %} CAST(order_date_local  AS DATE)
      {% elsif lvl_facts_union.common_month._in_query %} FORMAT_DATE('%Y-%m', order_date_local )
      {% elsif lvl_facts_union.common_quarter._in_query %} (FORMAT_TIMESTAMP('%Y-%m', TIMESTAMP_TRUNC(CAST(order_date_local AS TIMESTAMP), QUARTER)))
      {% elsif lvl_facts_union.common_week._in_query %} CONCAT(CAST(EXTRACT(YEAR FROM order_date_local ) AS STRING),'-', CAST(EXTRACT(WEEK(SUNDAY) FROM order_date_local) AS STRING))
      {% elsif lvl_facts_union.common_year._in_query %} EXTRACT(YEAR FROM order_date_local )
      {% else %} CAST(order_date_local  AS DATE)
      {% endif %}

{% elsif _explore._name == "ecomm_orders_new_vs_returning_pop" %}
    {% if ecomm_customer_transactional_pop.order_date_local_date._in_query %} CAST(order_date_local  AS DATE)
      {% elsif ecomm_customer_transactional_pop.order_date_local_month._in_query %} FORMAT_DATE('%Y-%m', order_date_local )
      {% elsif ecomm_customer_transactional_pop.order_date_local_quarter._in_query %} (FORMAT_TIMESTAMP('%Y-%m', TIMESTAMP_TRUNC(CAST(order_date_local AS TIMESTAMP), QUARTER)))
      {% elsif ecomm_customer_transactional_pop.order_date_local_week._in_query %} CONCAT(CAST(EXTRACT(YEAR FROM order_date_local ) AS STRING),'-', CAST(EXTRACT(WEEK(SUNDAY) FROM order_date_local) AS STRING))
      {% elsif ecomm_customer_transactional_pop.order_date_local_year._in_query %} EXTRACT(YEAR FROM order_date_local )
      {% else %} CAST(order_date_local  AS DATE) {% endif %}

{% else %}
    {% if ecomm_customer_transactional_pop.order_date_local_date._in_query %} CAST(order_date_local  AS DATE)
      {% elsif ecomm_customer_transactional_pop.order_date_local_month._in_query %} FORMAT_DATE('%Y-%m', order_date_local )
      {% elsif ecomm_customer_transactional_pop.order_date_local_quarter._in_query %} (FORMAT_TIMESTAMP('%Y-%m', TIMESTAMP_TRUNC(CAST(order_date_local AS TIMESTAMP), QUARTER)))
      {% elsif ecomm_customer_transactional_pop.order_date_local_week._in_query %} CONCAT(CAST(EXTRACT(YEAR FROM order_date_local ) AS STRING),'-', CAST(EXTRACT(WEEK(SUNDAY) FROM order_date_local) AS STRING))
      {% elsif ecomm_customer_transactional_pop.order_date_local_year._in_query %} EXTRACT(YEAR FROM order_date_local )
      {% else %} CAST(order_date_local  AS DATE) {% endif %}

{% endif %} AS date_group,

     MIN(customer_type) AS customer_type
    FROM `chb-prod-data-cust.prod_ecomm_cust.ecomm_orders_w_first_txn_date`
    GROUP BY 1,2,3
    );;
  }

  dimension: pk {
    hidden: yes
    primary_key: yes
    type: string
    sql: CONCAT(${TABLE}.customer_key, ${TABLE}.date_group)  ;;
  }

  dimension: customer_key {
    hidden: yes
    type: string
    sql: ${TABLE}.customer_key ;;
  }

  dimension: brand {
    label: "Brand"
    hidden: yes
    type: string
    sql: ${TABLE}.brand ;;
  }

  dimension: date_group {
    label: "date group"
    hidden: yes
    type: string
    sql: ${TABLE}.date_group ;;
  }

  dimension: customer_type {
    label: "Customer Type"
    view_label: "Ecomm Customer New Returning"
    description: "New or Returning"
    hidden: no
    type: string
    sql: ${TABLE}.customer_type ;;
  }

  measure: total_cust_count {
    type: count_distinct
    hidden: yes
    sql: ${customer_key} ;;
  }


  measure: returning_rate {
    label: "Returning Rate"
    description: "How many unique customers shopped back over a given period of time out of all unique customers in the same period ?"
    view_label: "Ecomm Customer New Returning"
    type: number
    value_format: "0.00\%"
    sql: (${ret_cust_count_fixed}/${total_cust_count})*100 ;;
  }

  measure: recruitment_rate {
    label: "Recruitment Rate"
    description: "How many unique customers were recruited over a given period of time out of all unique customers in the same period ?"
    view_label: "Ecomm Customer New Returning"
    type: number
    value_format: "0.00\%"
    sql: (${new_cust_count_fixed}/${total_cust_count})*100 ;;
  }

  measure: cumulative_cust_count {
   view_label: "Ecomm Customer New Returning"
    label: "Cumul. Cust Count"
    type: running_total
    sql: ${new_cust_count} ;;
  }

  measure: ret_cust_count {
    hidden: yes
    label: "Returning Customer Count"
    type: number
    sql: ${total_cust_count}-${new_cust_count};;
  }

  measure: new_cust_count_fixed {
    hidden: no
    label: "New  Customer Count"
    type: number
    sql: ${new_cust_count}-${ret_cust_count_fixed};;
  }

  measure: total_cust_count_fixed {
    type: number
    label: "Total  Customer Count"
    hidden: no
    sql: ${new_cust_count_fixed} + ${ret_cust_count_fixed} ;;
  }

  measure: new_cust_count {
    hidden: yes
    label: "New Customer Count"
    type: count_distinct
    filters: {
      field: customer_type
      value: "New"
    }
    sql: ${customer_key} ;;
  }


  measure: ret_cust_count_fixed {
    # hidden: yes
    label: "Returning Customer Count"
    type: count_distinct
    filters: {
      field: customer_type
      value: "Returning"
    }
    sql: ${customer_key} ;;
  }

}



view: ecomm_customer_pop_pop {
  derived_table: {
    sql:
    WITH pop_period_1_table AS (
    SELECT
{% if _explore._name == "tryano" %}

      {% if tryano.common_date._in_query %} CAST(order_date_local  AS DATE)
      {% elsif tryano.common_month._in_query %} FORMAT_DATE('%Y-%m', order_date_local )
      {% elsif tryano.common_quarter._in_query %} (FORMAT_TIMESTAMP('%Y-%m', TIMESTAMP_TRUNC(CAST(order_date_local AS TIMESTAMP), QUARTER)))
      {% elsif tryano.common_week._in_query %} CONCAT(CAST(EXTRACT(YEAR FROM order_date_local ) AS STRING),'-', CAST(EXTRACT(WEEK(SUNDAY) FROM order_date_local) AS STRING))
      {% elsif tryano.common_year._in_query %} EXTRACT(YEAR FROM order_date_local )
      {% else %}CAST(order_date_local  AS DATE)
      {% endif %}

{% elsif _explore._name == "stg_comm_swarovski_combined" %}

      {% if stg_comm_swarovski_combined.common_date_date._in_query %} CAST(order_date_local  AS DATE)
      {% elsif stg_comm_swarovski_combined.common_date_month._in_query %} FORMAT_DATE('%Y-%m', order_date_local )
      {% elsif stg_comm_swarovski_combined.common_date_quarter._in_query %} (FORMAT_TIMESTAMP('%Y-%m', TIMESTAMP_TRUNC(CAST(order_date_local AS TIMESTAMP), QUARTER)))
      {% elsif stg_comm_swarovski_combined.common_date_week._in_query %} CONCAT(CAST(EXTRACT(YEAR FROM order_date_local ) AS STRING),'-', CAST(EXTRACT(WEEK(SUNDAY) FROM order_date_local) AS STRING))
      {% elsif stg_comm_swarovski_combined.common_date_year._in_query %} EXTRACT(YEAR FROM order_date_local )
      {% else %} CAST(order_date_local  AS DATE)
      {% endif %}


{% elsif _explore._name == "faces" %}

      {% if faces.common_date._in_query %} CAST(order_date_local  AS DATE)
      {% elsif faces.common_month._in_query %} FORMAT_DATE('%Y-%m', order_date_local )
      {% elsif faces.common_quarter._in_query %} (FORMAT_TIMESTAMP('%Y-%m', TIMESTAMP_TRUNC(CAST(order_date_local AS TIMESTAMP), QUARTER)))
      {% elsif faces.common_week._in_query %} CONCAT(CAST(EXTRACT(YEAR FROM order_date_local ) AS STRING),'-', CAST(EXTRACT(WEEK(SUNDAY) FROM order_date_local) AS STRING))
      {% elsif faces.common_year._in_query %} EXTRACT(YEAR FROM order_date_local )
      {% else %} CAST(order_date_local  AS DATE)
      {% endif %}

{% elsif _explore._name == "stg_comm_chanel_faces_ga_landing_pages" %}

      {% if stg_comm_chanel_faces_ga_landing_pages.common_date._in_query %} CAST(order_date_local  AS DATE)
      {% elsif stg_comm_chanel_faces_ga_landing_pages.common_month._in_query %} FORMAT_DATE('%Y-%m', order_date_local )
      {% elsif stg_comm_chanel_faces_ga_landing_pages.common_quarter._in_query %} (FORMAT_TIMESTAMP('%Y-%m', TIMESTAMP_TRUNC(CAST(order_date_local AS TIMESTAMP), QUARTER)))
      {% elsif stg_comm_chanel_faces_ga_landing_pages.common_week._in_query %} CONCAT(CAST(EXTRACT(YEAR FROM order_date_local ) AS STRING),'-', CAST(EXTRACT(WEEK(SUNDAY) FROM order_date_local) AS STRING))
      {% elsif stg_comm_chanel_faces_ga_landing_pages.common_year._in_query %} EXTRACT(YEAR FROM order_date_local )
      {% else %} CAST(order_date_local  AS DATE)
      {% endif %}

{% elsif _explore._name == "lvl_facts_union" %}

      {% if lvl_facts_union.common_date._in_query %} CAST(order_date_local  AS DATE)
      {% elsif lvl_facts_union.common_month._in_query %} FORMAT_DATE('%Y-%m', order_date_local )
      {% elsif lvl_facts_union.common_quarter._in_query %} (FORMAT_TIMESTAMP('%Y-%m', TIMESTAMP_TRUNC(CAST(order_date_local AS TIMESTAMP), QUARTER)))
      {% elsif lvl_facts_union.common_week._in_query %} CONCAT(CAST(EXTRACT(YEAR FROM order_date_local ) AS STRING),'-', CAST(EXTRACT(WEEK(SUNDAY) FROM order_date_local) AS STRING))
      {% elsif lvl_facts_union.common_year._in_query %} EXTRACT(YEAR FROM order_date_local )
      {% else %} CAST(order_date_local  AS DATE)
      {% endif %}

{% elsif _explore._name == "ecomm_orders_new_vs_returning_pop" %}
    {% if ecomm_customer_transactional_pop.order_date_local_date._in_query %} CAST(order_date_local  AS DATE)
      {% elsif ecomm_customer_transactional_pop.order_date_local_month._in_query %} FORMAT_DATE('%Y-%m', order_date_local )
      {% elsif ecomm_customer_transactional_pop.order_date_local_quarter._in_query %} (FORMAT_TIMESTAMP('%Y-%m', TIMESTAMP_TRUNC(CAST(order_date_local AS TIMESTAMP), QUARTER)))
      {% elsif ecomm_customer_transactional_pop.order_date_local_week._in_query %} CONCAT(CAST(EXTRACT(YEAR FROM order_date_local ) AS STRING),'-', CAST(EXTRACT(WEEK(SUNDAY) FROM order_date_local) AS STRING))
      {% elsif ecomm_customer_transactional_pop.order_date_local_year._in_query %} EXTRACT(YEAR FROM order_date_local )
      {% else %} CAST(order_date_local  AS DATE) {% endif %}

{% else %}
    {% if ecomm_customer_transactional_pop.order_date_local_date._in_query %} CAST(order_date_local  AS DATE)
      {% elsif ecomm_customer_transactional_pop.order_date_local_month._in_query %} FORMAT_DATE('%Y-%m', order_date_local )
      {% elsif ecomm_customer_transactional_pop.order_date_local_quarter._in_query %} (FORMAT_TIMESTAMP('%Y-%m', TIMESTAMP_TRUNC(CAST(order_date_local AS TIMESTAMP), QUARTER)))
      {% elsif ecomm_customer_transactional_pop.order_date_local_week._in_query %} CONCAT(CAST(EXTRACT(YEAR FROM order_date_local ) AS STRING),'-', CAST(EXTRACT(WEEK(SUNDAY) FROM order_date_local) AS STRING))
      {% elsif ecomm_customer_transactional_pop.order_date_local_year._in_query %} EXTRACT(YEAR FROM order_date_local )
      {% else %} CAST(order_date_local  AS DATE) {% endif %}

{% endif %} AS date_group,
    min(order_date_local) AS period_1_start,
    max(order_date_local)  AS period_1_end,

    {% if ecomm_customer_transactional_pop.order_date_local_date._in_query %}
      1
    {% else %}
      DATE_DIFF( max(order_date_local),min(order_date_local) , day)
    {% endif %} AS period_1_days

    FROM `chb-prod-data-cust.prod_ecomm_cust.ecomm_orders_w_first_txn_date`
    GROUP BY 1
    ),
    pop_period_2_table AS (
    SELECT DISTINCT date_group,
    period_1_start, period_1_end,
    {% if compare_to._parameter_value == "Period" %}
    DATE_SUB(period_1_start , INTERVAL period_1_days DAY)
    {% elsif compare_to._parameter_value == "IsoYear" %}
    DATE_SUB(period_1_start, INTERVAL (DATE_DIFF(DATE(EXTRACT(YEAR FROM period_1_start) - 1 , 12 ,31 ), DATE(EXTRACT(YEAR FROM period_1_start) - 1 , 1 ,1 ), ISOWEEK)) WEEK)
    {% elsif compare_to._parameter_value == "Custom" %}
    {% date_start compare_to_date_range %}
    {% elsif compare_to._parameter_value == "Month" %}
    DATE_TRUNC(DATE_SUB(DATE_TRUNC(period_1_start, MONTH), INTERVAL 1 DAY), MONTH)
        {% elsif compare_to._parameter_value == "Week" %}
    DATE_SUB(period_1_start, INTERVAL 7 DAY)
    {% elsif compare_to._parameter_value == "Year" %}
    DATE_TRUNC(DATE_SUB(DATE_TRUNC(period_1_start, YEAR), INTERVAL 1 DAY)  , YEAR)
    {% elsif compare_to._parameter_value == "" %}
    period_1_start
    {% else %}
    DATE_SUB(period_1_start, INTERVAL 1 {% parameter compare_to %})
    {% endif %}  AS period_2_start,

    {% if compare_to._parameter_value == "Period" %}
    DATE_SUB(period_1_start, INTERVAL 0 DAY)
    {% elsif compare_to._parameter_value == "IsoYear" %}
    DATE_SUB(period_1_end, INTERVAL (DATE_DIFF(DATE(EXTRACT(YEAR FROM period_1_end) - 1 , 12 ,31 ), DATE(EXTRACT(YEAR FROM period_1_end) - 1 , 1 ,1 ), ISOWEEK)) WEEK)
    {% elsif compare_to._parameter_value == "Custom" %}
    DATE_SUB(CAST({% date_end compare_to_date_range %} AS DATE), INTERVAL 1 DAY)
    {% elsif compare_to._parameter_value == "Month" %}
    DATE_SUB(period_1_start, INTERVAL 1 DAY)
        {% elsif compare_to._parameter_value == "Week" %}
    DATE_SUB(period_1_start, INTERVAL 1 DAY)
    {% elsif compare_to._parameter_value == "Year" %}
    DATE_SUB(DATE_TRUNC(period_1_start, YEAR), INTERVAL 1 DAY)
    {% elsif compare_to._parameter_value == "" %}
    period_1_end
    {% else %}

    DATE_SUB(period_1_end, INTERVAL 1 {% parameter compare_to %})
    {% endif %} AS period_2_end

    FROM pop_period_1_table
    ), this_period_txn AS (
      SELECT
        brand,
        customer_type,
        date_group,
        COUNT(DISTINCT customer_key) AS count_customers_this_period,
        SUM(count_orders) AS count_orders_this_period,
        SUM(gross_net_sales_usd_total) AS gross_net_sales_usd_total_this_period,
        SUM(gross_revenue_usd_total) AS gross_revenue_usd_total_this_period,
        ARRAY_AGG(customer_key) AS unique_customer_list_this_period,

        COUNT(DISTINCT(CASE WHEN customer_type = 'New'
        THEN customer_key ELSE NULL END)) AS count_new_customers
      FROM (

        SELECT txn.customer_key,
               txn.brand,
              pop.date_group,
              MIN(txn.customer_type) AS customer_type,
              COUNT(DISTINCT order_id) AS count_orders,
              SUM(gross_net_sales_usd_total) AS gross_net_sales_usd_total,
               SUM(gross_revenue_usd_total)  AS gross_revenue_usd_total
      FROM
        `chb-prod-data-cust.prod_ecomm_cust.ecomm_orders_w_first_txn_date` txn
      INNER JOIN pop_period_2_table pop ON 1=1 AND (txn.order_date_local BETWEEN pop.period_1_start AND pop.period_1_end)
       GROUP BY brand, pop.date_group, txn.customer_key )
      GROUP BY brand, customer_type, date_group
), last_period_txn AS (
     SELECT
        brand,
        customer_type,
        date_group,
        COUNT(DISTINCT customer_key) AS count_customers_last_period,
        SUM(count_orders) AS count_orders_last_period,
        SUM(gross_net_sales_usd_total) AS gross_net_sales_usd_total_last_period,
        SUM(gross_revenue_usd_total) AS gross_revenue_usd_total_last_period,
        ARRAY_AGG(customer_key) AS unique_customer_list_last_period,

        COUNT(DISTINCT(CASE WHEN customer_type = 'New'
        THEN customer_key ELSE NULL END)) AS count_new_customers
      FROM (
        SELECT txn.customer_key,
               txn.brand,
              pop.date_group,
              MIN(txn.customer_type) AS customer_type,
              COUNT(DISTINCT order_id) AS count_orders,
              SUM(gross_net_sales_usd_total) AS gross_net_sales_usd_total,
               SUM(gross_revenue_usd_total)  AS gross_revenue_usd_total
      FROM
        `chb-prod-data-cust.prod_ecomm_cust.ecomm_orders_w_first_txn_date` txn
      INNER JOIN pop_period_2_table pop ON 1=1 AND (txn.order_date_local BETWEEN pop.period_2_start AND pop.period_2_end)
      GROUP BY brand, pop.date_group, txn.customer_key)
      GROUP BY brand, customer_type, date_group
),  customer_count AS

  ( SELECT brand, date_group,  count(distinct customer) AS unique_customers_retention FROM (
  SELECT
  DISTINCT
    a.brand, a.customer_type,
    a.date_group, customer
  FROM this_period_txn a, UNNEST(unique_customer_list_this_period) as customer
  UNION ALL
    SELECT
    DISTINCT
    a.brand, a.customer_type,
    a.date_group, customer
  FROM this_period_txn a
  LEFT JOIN last_period_txn b ON a.brand = b.brand
  AND a.date_group = b.date_group AND a.customer_type = b.customer_type, UNNEST(unique_customer_list_last_period) as customer
) GROUP BY brand, date_group )
  SELECT
    a.brand,
    a.customer_type,
    a.date_group,
    a.count_customers_this_period,
    a.count_orders_this_period,
    a.gross_net_sales_usd_total_this_period,
    a.gross_revenue_usd_total_this_period,
    b.count_customers_last_period,
    b.count_orders_last_period,
    b.gross_net_sales_usd_total_last_period,
    b.gross_revenue_usd_total_last_period,

    a.count_new_customers AS count_new_customers_this_period,
    b.count_new_customers AS count_new_customers_last_period,
    cc.unique_customers_retention

  FROM this_period_txn a
  LEFT JOIN last_period_txn b ON a.brand = b.brand AND a.date_group = b.date_group AND a.customer_type = b.customer_type
  LEFT JOIN customer_count cc ON a.brand = cc.brand AND a.date_group = cc.date_group ;;

}

  dimension: pk {
    hidden: yes
    primary_key: yes
    type: string
    sql: CONCAT(${TABLE}.brand, ${TABLE}.date_group, ${TABLE}.customer_type)  ;;
  }

  dimension: date_group {
    hidden: yes
    type: string
    sql: ${TABLE}.date_group ;;
  }

  dimension: brand {
    label: "Brand"
    hidden: yes
    type: string
    sql: ${TABLE}.brand ;;
  }

  dimension: customer_type {
    label: "Customer Type"
    description: "New or Returning"
    hidden: yes
    type: string
    sql: ${TABLE}.customer_type ;;
  }

  measure: count_customers_this_period {
    label: "Count Customers This Period"
    view_label: "Period over Period"
    type: sum_distinct
    hidden: yes
    sql_distinct_key: ${pk} ;;
    sql: ${TABLE}.count_customers_this_period ;;

  }

  measure: count_customers_last_period {
    label: "Count Customers Last Period"
    view_label: "Period over Period"
    type: sum_distinct
    hidden: yes
    sql_distinct_key: ${pk} ;;
    sql: ${TABLE}.count_customers_last_period ;;
  }

  measure: count_customers_pop {
    label: "Count Unique Customers PoP%"
    view_label: "Period over Period"
    description: "Unique customers this period divided by unique customers last period "
    type: number
    hidden: yes
    value_format: "0.0%"
    sql: ${count_customers_this_period}/NULLIF(${count_customers_last_period},0) ;;
  }

  measure: count_orders_this_period {
    label: "Count Orders This Period"
    view_label: "Period over Period"
    type: sum_distinct
    hidden: yes
    sql_distinct_key: ${pk} ;;
    sql: ${TABLE}.count_orders_this_period ;;
  }

  measure: count_orders_last_period {
    label: "Count Orders Last Period"
    view_label: "Period over Period"
    type: sum_distinct
    hidden: yes
    sql_distinct_key: ${pk} ;;
    sql: ${TABLE}.count_orders_last_period ;;

  }

  measure: count_orders_pop {
    label: "Count Orders PoP%"
    view_label: "Period over Period"
    description: "Unique orders this period divided by unique orders last period "
    type: number
    hidden: yes
    value_format: "0.0%"
    sql: ${count_orders_this_period}/NULLIF(${count_orders_last_period},0) ;;
  }

  measure: gross_net_sales_usd_total_this_period {
    label: "Sales Amount USD This Period"
    view_label: "Period over Period"
    type: sum_distinct
    hidden: yes
    value_format: "$#,##0"
    sql_distinct_key: ${pk} ;;
    sql: ${TABLE}.gross_net_sales_usd_total_this_period ;;
  }

  measure: gross_net_sales_usd_total_last_period {
    label: "Sales Amount USD Last Period"
    view_label: "Period over Period"
    hidden: yes
    type: sum_distinct
    value_format: "$#,##0"
    sql_distinct_key: ${pk} ;;
    sql: ${TABLE}.gross_net_sales_usd_total_last_period ;;
  }

  measure: sales_usd_pop {
    label: "Net Sales Amount USD PoP%"
    view_label: "Period over Period"
    description: "Net sales this period divided by net sales last period "
    type: number
    hidden: yes
    value_format: "0.0%"
    sql: ${gross_net_sales_usd_total_this_period}/NULLIF(${gross_net_sales_usd_total_last_period},0) ;;
  }


  measure: gross_revenue_usd_total_last_period {
    label: "Revenue Amount USD This Period"
    view_label: "Period over Period"
    hidden: yes
    type: sum_distinct
    value_format: "$#,##0"
    sql_distinct_key: ${pk} ;;
    sql: ${TABLE}.gross_revenue_usd_total_last_period ;;
  }

  measure: gross_revenue_usd_total_this_period {
    label: "Revenue Amount USD This Period"
    view_label: "Period over Period"
    hidden: yes
    type: sum_distinct
    value_format: "$#,##0"
    sql_distinct_key: ${pk} ;;
    sql: ${TABLE}.gross_net_sales_usd_total_this_period ;;
  }

  measure: revenue_usd_pop {
    label: "Gross Sales Amount USD PoP%"
    view_label: "Period over Period"
    description: "Gross sales this period divided by gross sales last period "
    type: number
    hidden: yes
    value_format: "0.0%"
    sql: ${gross_revenue_usd_total_this_period}/NULLIF(${gross_revenue_usd_total_last_period},0) ;;
  }



  measure: unique_customers_retention {
    label: "Retained"
    view_label: "Period over Period"
    hidden: yes
    sql_distinct_key: CONCAT(${TABLE}.brand, ${TABLE}.date_group) ;;
    type: sum_distinct
    sql: ${TABLE}.unique_customers_retention ;;

  }

  measure: retained_customers {
    label: "Retained Customers Count"
    view_label: "Period over Period"
    description: "Won't work with customer_type"
    type: number
    sql:
    {% if ecomm_customer_type_pop.customer_type._in_query %}
    NULL
    {%else%}
    ((${count_customers_last_period} + ${count_customers_this_period})
    -${unique_customers_retention})
    {% endif %};;
  }

  measure: retention_rate {
    label: "Retention Rate"
    view_label: "Period over Period"
    description: "Number of unique customers who shopped at least once in a given period of time and the prior period out of total customers whos shopped during the prior period - Won't work with customer_type"
    value_format: "0.00\%"
    type: number
    sql:
    {% if ecomm_customer_type_pop.customer_type._in_query %}
    NULL
    {%else%}
    ${retained_customers} / NULLIF(${count_customers_last_period},0)*100
    {% endif %};;
  }

  measure: count_new_customers_this_period {
    label: "New Customers This Period"
    view_label: "Period over Period"
    hidden: yes
    type: sum
    sql: ${TABLE}.count_new_customers_this_period ;;
  }

  measure: count_new_customers_last_period {
    label: "New Customers Last Period"
    view_label: "Period over Period"
    hidden: yes
    type: sum
    sql: ${TABLE}.count_new_customers_last_period ;;
  }

  measure: new_customers_count_pop{
    label: "New Customers PoP%"
    view_label: "Period over Period"
    type: number
    hidden: yes
    description: "Unique new customers this period divided by unique new customers last period "
    value_format: "0.0%"
    sql: ${count_new_customers_this_period}/NULLIF(${count_new_customers_last_period},0) ;;
  }








  parameter: compare_to {
    view_label: "Period over Period"
    description: "Choose the period you would like to compare to. Must be used with Order Date dimension"
    label: "Compare To:"
    type: unquoted
    allowed_value: {
      label: "Nothing"
      value: ""
    }
    allowed_value: {
      label: "Previous Period"
      value: "Period"
    }
    allowed_value: {
      label: "Previous Iso Year"
      value: "IsoYear"
    }
    allowed_value: {
      label: "Previous Calendar Week"
      value: "Week"
    }
    allowed_value: {
      label: "Previous Calendar Month"
      value: "Month"
    }
    allowed_value: {
      label: "Previous Calendar Year"
      value: "Year"
    }
#     allowed_value: {
#       label: "Custom"
#       value: "Custom"
#     }
    default_value: ""
  }

}


view: ecomm_brand_aggregates_pop {
  view_label: "Lifetime Brand Aggregates"
  derived_table: {
    sql:
      SELECT
        ROW_NUMBER() OVER() as row_num_pk,
        brand,
        count(distinct customer_key)  AS brand_unique_customers,
        AVG(gross_revenue_usd_total) AS brand_avg_ltv,
        SUM(count_orders) AS brand_total_orders,
        AVG(tenure) AS brand_avg_tenure,
        AVG(recency) AS brand_avg_recency
      FROM
        (SELECT
            brand,
            a.customer_key  AS customer_key,
            DATE_DIFF(CURRENT_DATE(), MAX(order_date_local), DAY) AS recency,
            sum(gross_revenue_usd_total) AS gross_revenue_usd_total,
            count(distinct order_id) AS count_orders,
            DATE_DIFF(CURRENT_DATE(), CAST(b.first_order_datetime_utc AS DATE), DAY) AS tenure
        FROM
          `chb-prod-data-cust.prod_ecomm_cust.ecomm_orders_w_first_txn_date` a
          LEFT JOIN (
            SELECT customer_key, MIN(order_datetime_utc) AS first_order_datetime_utc
            FROM `chb-prod-data-cust.prod_ecomm_cust.ecomm_orders_w_first_txn_date`
            GROUP BY customer_key) b ON a.customer_key = b.customer_key
        GROUP BY  brand, a.customer_key, b.first_order_datetime_utc)
      GROUP BY brand
      ORDER BY row_num_pk;;
  }
  dimension: row_num_pk{primary_key:yes hidden:yes}
  dimension: brand {hidden:yes}


  measure: avg_lifetime_value {
    #   view_label: "Ecomm Customer New Returning"
    label: "Average Lifetime Spend"
#     hidden: yes
    value_format: "$#"
    type: min
    sql: ${TABLE}.brand_avg_ltv ;;
  }
  measure: average_tenure_days {
    #   view_label: "Ecomm Customer New Returning"
    label: "Average Tenure"
#     hidden: yes
    value_format: "0"
    type: min
    sql: ${TABLE}.brand_avg_tenure ;;
  }
  measure: average_recency_days {
    #   view_label: "Ecomm Customer New Returning"
    label: "Average Recency"
#     hidden: yes
    value_format: "0"
    type: min
    sql: ${TABLE}.brand_avg_recency ;;
  }
}

view: ecomm_customer_aggregates_pop {
  view_label: "Lifetime Customer Aggregates"
  derived_table: {
    sql:
        SELECT
            ROW_NUMBER() OVER() as row_num_pk,
            a.customer_key  AS customer_key,
            CAST(b.first_order_datetime_utc AS TIMESTAMP) AS first_order_datetime_utc,
            DATE_DIFF(CURRENT_DATE(), MAX(order_date_local), DAY) AS customer_recency,
            DATE_DIFF(CURRENT_DATE(), CAST(b.first_order_datetime_utc AS DATE), DAY) AS customer_tenure,
            sum(gross_revenue_usd_total) AS customer_ltv,
            count(distinct order_id) AS customer_orders
            --count(distinct order_id)/DATE_DIFF(CURRENT_DATE(), CAST(first_order_datetime_utc AS DATE), DAY) AS customer_frequency

        FROM `chb-prod-data-cust.prod_ecomm_cust.ecomm_orders_w_first_txn_date` a
        LEFT JOIN (
            SELECT customer_key, MIN(order_datetime_utc) AS first_order_datetime_utc
            FROM `chb-prod-data-cust.prod_ecomm_cust.ecomm_orders_w_first_txn_date`
            GROUP BY customer_key) b ON a.customer_key = b.customer_key
        GROUP BY
          a.customer_key,
          b.first_order_datetime_utc
        ORDER BY row_num_pk;;
  }
  dimension: row_num_pk{primary_key:yes hidden:yes}
  dimension: customer_key {hidden:yes}

  dimension: customer_ltv {
    label: "Customer Lifetime Value"
    description: "Gross sales all years for a given customer. Calculation based on Gross Sales Amount USD"
    type: number
    sql: ${TABLE}.customer_ltv ;;
  }

  measure: avg_customer_ltv {
    label: "Average Lifetime Spend"
    description: "Average Gross sales all years for a given customer. Calculation based on Gross Sales Amount USD "
    view_label: "Lifetime Customer Aggregates"
    value_format: "$#"
    type: average
    sql: ROUND(${TABLE}.customer_ltv) ;;
  }

  dimension: customer_ftxn {
    label: "Customer First Txn Date"
    description: "When was the first time a customer transacted"
    type: date
    sql: ${TABLE}.first_order_datetime_utc ;;
  }

  dimension: customer_tenure {
    label: "Customer Tenure"
    description: "When was the first time member/customer has made a purchase ? (number of days)"
    type: number
    sql: ${TABLE}.customer_tenure ;;
  }

  measure: avg_customer_tenure {
    label: "Average Tenure"
    description: "When was the first time a customer has made a purchase ? (number of days)"
    view_label: "Lifetime Customer Aggregates"
    value_format: "0"
    type: average
    sql: ${TABLE}.customer_tenure ;;
  }

  dimension: customer_time_between_purchases {
    description: "Average time between purchases for a customer (number of days)"
    value_format: "0.##"
    type: number
    sql: ${customer_tenure}/nullif(${customer_frequency},0) ;;
  }

  measure: ave_time_between_purchases {
    type: average
    description: "Average number of days between purchases (number of days)"
    value_format: "0.##"
    sql: ${customer_time_between_purchases} ;;
  }


  dimension: customer_recency {
    label: "Customer Recency"
    description: "How recently a customer has made a purchase ? (number of days)"
    type: number
    sql: ${TABLE}.customer_recency ;;
  }

  measure: avg_customer_recency {
    label: "Average Recency"
    description: "How recently a customer has made a purchase ? (number of days)"
    view_label: "Lifetime Customer Aggregates"
    type: average
    value_format: "0"
    sql: ${TABLE}.customer_recency ;;
  }

  dimension: customer_frequency {
    label: "Customer Frequency"
    description: "How many times a customer has made a purchase ?"
    type: number
    sql: ${TABLE}.customer_orders ;;
  }

  dimension: recency_range {
    view_label: "Lifetime Customer Aggregates"
    group_label: "Segment Customer"
    label: "Recency Segment"
    hidden: no
    description: "Recency Range for RFM analysis"
    type: string
    sql: CASE
          WHEN ${customer_recency} <= 30 THEN "1 Month"
          WHEN ${customer_recency} <= 183 THEN "2-6 Months"
          WHEN ${customer_recency} <= 365 THEN "7-12 Months"
          WHEN ${customer_recency} <= 548 THEN "13-18 Months"
               WHEN ${customer_recency} <= 730 THEN "18-24 Months"
          WHEN ${customer_recency} > 730 THEN "25 Months+"
          ELSE null
          END;;
}

  dimension: frequency_range_sort {
    hidden: yes
    type: number
    sql: CASE
          WHEN ${customer_frequency} <= 1 THEN 1
          WHEN ${customer_frequency} <= 2 THEN 2
          WHEN ${customer_frequency} <= 3 THEN 3
          WHEN ${customer_frequency} <= 5 THEN 4
          WHEN ${customer_frequency} <= 7 THEN 5
          WHEN ${customer_frequency}  <= 10 THEN 6
          WHEN ${customer_frequency}  > 10 THEN 7
          ELSE null
          END;;
  }

  dimension: frequency_range {
    order_by_field: frequency_range_sort
    view_label: "Lifetime Customer Aggregates"
    group_label: "Segment Customer"
    label: "Frequency Segment"
    hidden: no
    description: "Frequency Range for RFM analysis (1/2/3/4-5/6-7/8-10/>10)"
    type: string
    sql: CASE
          WHEN ${customer_frequency} <= 1 THEN "1"
          WHEN ${customer_frequency} <= 2 THEN "2"
          WHEN ${customer_frequency} <= 3 THEN "3"
          WHEN ${customer_frequency} <= 5 THEN "4-5"
          WHEN ${customer_frequency} <= 7 THEN "6-7"
          WHEN ${customer_frequency}  <= 10 THEN "8-10"
          WHEN ${customer_frequency}  > 10 THEN ">10"
          ELSE null
          END;;
  }
}
