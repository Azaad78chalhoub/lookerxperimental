view: dim_retail_product_unified {
  label: "Products inc CH (beta)"
  sql_table_name: `chb-prod-data-cust.prod_Customer360.dim_retail_product_unified`
    ;;

  dimension: barcode {
    type: string
    sql: ${TABLE}.barcode ;;
  }

  dimension: brand {
    type: string
    sql: ${TABLE}.brand ;;
  }

  dimension: class {
    hidden: yes
    type: number
    sql: ${TABLE}.class ;;
  }

  dimension: class_name {
    type: string
    sql: ${TABLE}.class_name ;;
  }

  dimension: dept_name {
    type: string
    sql: ${TABLE}.dept_name ;;
  }

  dimension: dept_no {
    type: number
    hidden: yes
    sql: ${TABLE}.dept_no ;;
  }

  dimension: diff_1 {
    type: string
    hidden: yes
    sql: ${TABLE}.diff_1 ;;
  }

  dimension: diff_2 {
    hidden: yes
    type: string
    sql: ${TABLE}.diff_2 ;;
  }

  dimension: division {
    type: string
    sql: ${TABLE}.division ;;
  }

  dimension: division_no {
    hidden: yes
    type: number
    sql: ${TABLE}.division_no ;;
  }

  dimension: gender {
    hidden: yes
    type: string
    sql: ${TABLE}.gender ;;
  }

  dimension: group_name {
    type: string
    sql: ${TABLE}.group_name ;;
  }

  dimension: group_no {
    hidden: yes
    type: number
    sql: ${TABLE}.group_no ;;
  }

  dimension: item {
    type: string
    sql: ${TABLE}.item ;;
  }

  dimension: item_desc {
    type: string
    sql: ${TABLE}.item_desc ;;
  }

  dimension: item_style {
    type: string
    sql: ${TABLE}.item_style ;;
  }

  dimension_group: last_update_datetime {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.last_update_datetime ;;
  }

  dimension: line {
    type: string
    sql: ${TABLE}.line ;;
  }

  dimension: standard_uom {
    hidden: yes
    type: string
    sql: ${TABLE}.standard_uom ;;
  }

  dimension: sub_class {
    hidden: yes
    type: number
    sql: ${TABLE}.sub_class ;;
  }

  dimension: subclass_name {
    type: string
    sql: ${TABLE}.subclass_name ;;
  }

  dimension: taxo_class {
    type: string
    sql: ${TABLE}.taxo_class ;;
  }

  dimension: taxo_subclass {
    type: string
    sql: ${TABLE}.taxo_subclass ;;
  }

  dimension: vpn {
    type: string
    sql: ${TABLE}.vpn ;;
  }

  measure: count {
    hidden: yes
    type: count
    drill_fields: [group_name, dept_name, subclass_name, class_name]
  }
}
