view: unique_offers {
  sql_table_name: `chb-prod-data-cust.prod_MUSE_ControlTower.unique_offers`
    ;;


  dimension: pk {
    type: string
    hidden: yes
    primary_key: yes
    sql: CONCAT(CAST(${bit_reference} AS STRING),CAST(${member_id} AS STRING),${tag_info},${offer_name})  ;;

  }

  dimension: base_points {
    type: number
    hidden: yes
    sql: ${TABLE}.base_points ;;
  }

  measure: sum_base_points {
    label: "Base Points - Sum"
    type: sum
    value_format: "#,##0"
    hidden: no
    sql_distinct_key: ${pk} ;;
    sql: ${base_points} ;;
  }

  dimension: billing_location {
    type: string
    sql: ${TABLE}.billing_location ;;
  }

  dimension: billing_location_code {
    type: string
    sql: ${TABLE}.billing_location_code ;;
  }

  dimension: billing_location_code_info {
    type: string
    sql: ${TABLE}.billing_location_code_info ;;
  }

  dimension: billing_location_info {
    type: string
    sql: ${TABLE}.billing_location_info ;;
  }

  dimension: billing_sponsor {
    type: string
    sql: ${TABLE}.billing_sponsor ;;
  }

  dimension_group: bit {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.bit_date ;;
  }

  dimension: bit_id {
    type: number
    sql: ${TABLE}.bit_id ;;
  }

  dimension: bit_mode {
    type: string
    sql: ${TABLE}.bit_mode ;;
  }

  dimension: bit_reference {
    type: string
    sql: ${TABLE}.bit_reference ;;
  }

  dimension: bit_time {
    type: string
    hidden: yes
    sql: ${TABLE}.bit_time ;;
  }

  dimension: bit_type {
    type: string
    sql: ${TABLE}.bit_type ;;
  }

  dimension: bonus_points {
    type: number
    hidden: yes
    sql: ${TABLE}.bonus_points ;;
  }

  measure: bonus_points_sum {
    label: "Bonus Points - Sum"
    type: sum
    value_format: "#,##0"
    hidden: no
    # sql_distinct_key: ${pk} ;;
    sql: ${bonus_points} ;;
  }

  dimension: burned {
    type: number
    hidden: yes
    sql: ${TABLE}.burned ;;
  }


  measure: burned_sum {
    label: "Burned Points - Sum"
    type: sum
    value_format: "#,##0"
    hidden: no
    sql_distinct_key: ${pk} ;;
    sql: ${burned} ;;

  }

  dimension: comment {
    type: string
    hidden:  yes
    sql: ${TABLE}.comment ;;
  }

  dimension: company_name {
    type: string
    sql: ${TABLE}.company_name ;;
  }

  dimension: country {
    type: string
    map_layer_name: countries
    sql: ${TABLE}.country ;;
  }

  dimension: currency_code {
    type: string
    sql: ${TABLE}.currency_code ;;
  }

  dimension_group: datess {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    label: "Transaction"
    sql: ${TABLE}.datess ;;
  }

  dimension: earned {
    hidden: yes
    type: number
    sql: ${TABLE}.earned ;;
  }

  measure: earned_sum {
    label: "Earned Points - Sum"
    type: sum
    value_format: "#,##0"
    # sql_distinct_key: ${pk} ;;
    sql: ${earned} ;;
  }

  dimension: eligible_spend {
    hidden: yes
    type: number
    sql: ${TABLE}.eligible_spend ;;
  }

  measure: eligible_spend_sum {
    label: "Eligible Spend - Sum"
    type: sum
    value_format: "#,##0"
    sql_distinct_key: ${pk} ;;
    sql: ${eligible_spend} ;;
  }

  dimension: ex_amount {
    hidden: yes
    type: number
    sql: ${TABLE}.ex_amount ;;
  }

  dimension: ex_amount_1 {
    hidden: yes
    type: number
    sql: ${TABLE}.ex_amount_1 ;;
  }

  dimension: expired {
    hidden: yes
    type: number
    sql: ${TABLE}.expired ;;
  }

  dimension: expired_points {
    hidden: yes
    type: number
    sql: ${TABLE}.expired_points ;;
  }

  dimension: h_bit_category {
    type: string
    sql: ${TABLE}.h_bit_category ;;
  }

  dimension: h_bit_mode {
    type: string
    sql: ${TABLE}.h_bit_mode ;;
  }

  dimension: h_bit_source_generated_id {
    type: string
    sql: ${TABLE}.h_bit_source_generated_id ;;
  }

  dimension: h_pos_id {
    hidden: yes
    type: string
    sql: ${TABLE}.h_pos_id ;;
  }

  dimension: l_line_tax {
    hidden: yes
    type: number
    sql: ${TABLE}.l_line_tax ;;
  }

  dimension: location {
    type: string
    sql: ${TABLE}.location ;;
  }

  dimension: location_code {
    type: string
    sql: ${TABLE}.location_code ;;
  }

  dimension: member_id {
    hidden: yes
    type: number
    sql: ${TABLE}.member_id ;;
  }

  dimension: month {
    hidden: yes
    type: string
    sql: ${TABLE}.month ;;
  }

  dimension: muse_location {
    hidden: yes
    type: string
    sql: ${TABLE}.muse_location ;;
  }

  dimension: offer_burned {
    hidden: yes
    type: number
    sql: ${TABLE}.offer_burned ;;
  }

  dimension: offer_earned {
    hidden: yes
    type: number
    sql: ${TABLE}.offer_earned ;;
  }

  dimension: offer_name {
    type: string
    sql: ${TABLE}.offer_name ;;
  }

  dimension: partial_reversal {
    type: string
    sql: ${TABLE}.partial_reversal ;;
  }

  dimension_group: process_datess {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    label: "Process"
    sql: ${TABLE}.process_datess ;;
  }

  dimension: processed_time {
    type: string
    hidden: yes
    sql: ${TABLE}.processed_time ;;
  }

  dimension: quarter {
    hidden: yes
    type: string
    sql: ${TABLE}.quarter ;;
  }

  dimension: spend_aed {
    type: number
    hidden: yes
    sql: ${TABLE}.spend_aed ;;
  }

  measure: spend_aed_sum {
    type: sum
    sql: ${spend_aed} ;;
  }

  dimension: spend_local {
    type: number
    hidden: yes
    sql: ${TABLE}.spend_local ;;
  }

  measure: spend_local_sum {
    type: sum
    sql: ${spend_local} ;;
  }


  dimension: spend_with_vat {
    type: number
    hidden: yes
    sql: ${TABLE}.spend_with_vat ;;
  }


  measure: spend_with_vat_sum {
    label: "Spend with Vat - Sum"
    sql_distinct_key: ${pk} ;;
    type: sum
    sql: ${spend_with_vat} ;;
  }


  dimension: sponsor {
    type: string
    sql: ${TABLE}.sponsor ;;
  }

  dimension: tag {
    type: string
    hidden: yes
    sql: ${TABLE}.tag ;;
  }

  dimension: tag_info {
    type: string
    sql: ${TABLE}.tag_info ;;
  }




  }
