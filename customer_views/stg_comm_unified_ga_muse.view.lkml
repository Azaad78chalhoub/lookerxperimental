view: stg_comm_unified_ga_muse {
  sql_table_name: `chb-prod-data-cust.prod_MUSE_ControlTower.stg_comm_unified_ga_muse`
    ;;

 ####### DIMENSIONS #######

  dimension: brand_security{
    type: string
    hidden: yes
    sql: ${brand_name} ;;
  }

  dimension: bounces {
    type: number
    hidden: yes
    sql: ${TABLE}.bounces ;;
  }

  dimension: brand_name {
    type: string
    sql: ${TABLE}.brand_name ;;
  }

  dimension: campaign {
    type: string
    sql: ${TABLE}.campaign ;;
  }

  dimension_group: date {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.date ;;
  }

  dimension: device_category {
    type: string
    sql: ${TABLE}.device_category ;;
  }

  dimension: ecom_site {
    type: string
    sql: ${TABLE}.ecom_site ;;
  }

  dimension: keyword {
    type: string
    sql: ${TABLE}.keyword ;;
  }

  dimension: medium {
    type: string
    sql: ${TABLE}.medium ;;
  }

  dimension: pageviews {
    type: number
    hidden: yes
    sql: ${TABLE}.pageviews ;;
  }

  dimension: product_adds_to_cart {
    type: number
    hidden: yes
    sql: ${TABLE}.product_adds_to_cart ;;
  }

  dimension: product_checkouts {
    type: number
    hidden: yes
    sql: ${TABLE}.product_checkouts ;;
  }

  dimension: session_duration {
    type: number
    hidden: yes
    sql: ${TABLE}.session_duration ;;
  }

  dimension: sessions {
    type: number
    hidden: yes
    sql: ${TABLE}.sessions ;;
  }

  dimension: source {
    type: string
    sql: ${TABLE}.source ;;
  }

  dimension: unique_pageviews {
    type: number
    hidden: yes
    sql: ${TABLE}.unique_pageviews ;;
  }

  dimension: user_type {
    type: string
    sql: ${TABLE}.user_type ;;
  }

  dimension: users {
    type: number
    hidden: yes
    sql: ${TABLE}.users ;;
  }

  ####### MEASURES #######

  measure: session_count{
    label: "# of Sessions"
    group_label: "GA"
    type: sum
    sql: CASE when ${sessions} is null then 0
        Else ${sessions}
        END;;
  }

  measure: bounce_count {
    label: "# of bounces"
    group_label: "GA"
    type: sum
    sql: CASE when ${bounces} is null then 0
          Else ${bounces}
          END;;
  }

  measure: bounce_rate {
    label: "Bounce Rate %"
    group_label: "GA"
    type: number
    value_format_name: percent_2
    sql: NULLIF(${bounce_count},0)/NULLIF(${session_count},0) ;;
  }

  measure: pageviews_per_session {
    type: number
    label: "Pageviews per Session"
    group_label: "GA"
    value_format_name: decimal_2
    sql: ${number_of_pageviews}/NULLIF(${session_count},0) ;;
  }

  measure: avg_time_on_site {
    type: number
    label: "Avg Time On Site"
    group_label: "GA"
    value_format: "h:mm:ss"
    sql: ${sum_session_duration}/NULLIF(${session_count},0)/86400 ;;
  }

  measure: add_to_basket_rate {
    type: number
    label: "Add to Basket %"
    group_label: "GA"
    value_format_name: percent_2
    sql: ${sum_product_adds_to_cart}/NULLIF(${number_of_pageviews},0) ;;
  }

  measure: cart_abandonment_rate {
    type: number
    label: "Cart Abandonment %"
    group_label: "GA"
    value_format_name: percent_2
    sql: (${sum_product_adds_to_cart}-${sum_product_checkouts})/NULLIF(${sum_product_adds_to_cart},0) ;;
  }

  measure: checkout_to_add_rate {
    type: number
    label: "Checkout to Add to Basket %"
    group_label: "GA"
    value_format_name: percent_2
    sql: ${sum_product_checkouts}/NULLIF(${sum_product_adds_to_cart},0) ;;
  }

  measure: number_of_pageviews {
    type: sum
    hidden: no
    label: "# of Page views"
    group_label: "GA"
    sql: ${TABLE}.pageviews ;;
  }


  ####### UNUSED MEASURES #######

  measure: sum_session_duration {
    type: sum
    hidden: yes
    label: "Sessions duration"
    sql: ${TABLE}.session_duration ;;
  }

  measure: sum_product_adds_to_cart {
    type: sum
    hidden: yes
    label: "# of Products Added to Cart"
    sql: ${TABLE}.product_adds_to_cart ;;
  }

  measure: sum_product_checkouts {
    type: sum
    hidden: yes
    label: "# of Product Checkouts"
    sql: ${TABLE}.product_checkouts ;;
  }

  measure: sum_users {
    type: sum
    hidden: yes
    label: "# of Users"
    sql: ${TABLE}.users ;;
  }
}
