view: segmentation_history {

  # parameter: date_of_segmentation {
  #   type: date_time
  #   view_label: "Segmentation"
  # }

  # derived_table: {
  #   sql:
  #     SELECT
  #     DISTINCT
  #       brand_id,
  #       customer_unique_id,
  #       lifecycle,
  #       experience,
  #       value,
  #       vip,
  #           CASE WHEN value = 'low' THEN 1
  #     WHEN value = 'medium' THEN 2
  #     WHEN value = 'big' THEN 3
  #     WHEN value = 'very_big' THEN 4 END
  # +
  #   CASE
  #     WHEN lifecycle = 'churned' THEN 1
  #     WHEN lifecycle = 'sporadic' THEN 2
  #     WHEN lifecycle = 'new' THEN 3
  #     WHEN lifecycle = 'active' THEN 4
  #     END AS score
  #     FROM
  #       `chb-prod-data-cust.prod_segmentation.segmentation_history`
  #     WHERE to_date <= CAST(CAST({% parameter date_of_segmentation %} AS DATE) AS STRING)
  #       AND to_date >  CAST(CAST({% parameter date_of_segmentation %} AS DATE) AS STRING) ;;
  # }

  sql_table_name: `chb-prod-data-cust.prod_segmentation.segmentation_history`;;

  dimension: id {
    label: "id"
    primary_key: yes
    hidden: yes
    type: string
    sql: CONCAT(CAST(${TABLE}.brand_id AS STRING), CAST(${TABLE}.date_to AS STRING), CAST(${TABLE}.customer_unique_id AS STRING)) ;;
  }

  dimension: active_per_possible {
    type: number
    hidden: yes
    view_label: "Segmentation"
    sql: ${TABLE}.active_per_possible ;;
  }

  dimension: brand_customer_unique_id {
    type: number
    hidden: yes
    view_label: "Segmentation"
    sql: CONCAT(CAST(${TABLE}.brand_id AS STRING),'_', CAST(${TABLE}.customer_unique_id AS STRING)) ;;
  }

  dimension: brand_id {
    type: number
    hidden: yes
    view_label: "Segmentation"
    sql: ${TABLE}.brand_id ;;
  }

  dimension: brand_name {
    type: string
    hidden: yes
    view_label: "Segmentation"
    sql: ${TABLE}.brand_name ;;
  }

  dimension: count_unique_quarters {
    type: number
    hidden: yes
    view_label: "Segmentation"
    sql: ${TABLE}.count_unique_quarters ;;
  }

  dimension: cumulative_count_possible_seasons {
    type: number
    hidden: yes
    view_label: "Segmentation"
    sql: ${TABLE}.cumulative_count_possible_seasons ;;
  }

  dimension: cumulative_recency {
    type: number
    hidden: yes
    view_label: "Segmentation"
    sql: ${TABLE}.cumulative_recency ;;
  }

  dimension: customer_unique_id {
    type: number
    hidden: yes
    view_label: "Segmentation"
    sql: ${TABLE}.customer_unique_id ;;
  }

  dimension: lifecycle_history {
    type: string
    view_label: "Segmentation"
    group_label: "History"
    sql: ${TABLE}.lifecycle ;;
  }

  dimension: mean_season_sales {
    type: number
    hidden: yes
    view_label: "Segmentation"
    sql: ${TABLE}.mean_season_sales ;;
  }

  dimension: from_date{
    type: date
    view_label: "Segmentation"
    group_label: "History"
    hidden: yes
    sql: ${TABLE}.from_date ;;
  }

  dimension: to_date{
    type: date
    view_label: "Segmentation"
    group_label: "History"
    hidden: yes
    sql: ${TABLE}.to_date ;;
  }

  dimension: period{
    type: string
    view_label: "Segmentation"
    group_label: "History"
    sql: ${TABLE}.period ;;
  }

  dimension: value_history {
    type: string
    view_label: "Segmentation"
    group_label: "History"
    sql: ${TABLE}.value ;;
  }


}



view: segmentation_flow
{

  parameter: months_of_difference {
    type: unquoted
    label: "Months of difference:"
    view_label: "Segmentation"
    description: "Difference in months between periods to compare (safely to use over 12)"
    group_label: "History"

    allowed_value: {
      label: "36 months"
      value: "36"
    }

    allowed_value: {
      label: "24 months"
      value: "24"
    }

    allowed_value: {
      label: "21 months"
      value: "21"
    }

    allowed_value: {
      label: "18 months"
      value: "18"
    }


    allowed_value: {
      label: "15 months"
      value: "15"
    }

    allowed_value: {
      label: "12 months"
      value: "12"
    }
    allowed_value: {
      label: "9 months"
      value: "9"
    }

    allowed_value: {
      label: "6 months"
      value: "6"
    }
    allowed_value: {
      label: "3 months"
      value: "3"
    }
    default_value: "24"
  }







  derived_table: {
    sql:
    WITH new_customer_period AS (
      SELECT
      DISTINCT
        brand_id,
        customer_unique_id,
        MIN(to_date) AS new_period,
        DATE_ADD(CAST(MIN(to_date) AS DATE), INTERVAL {% parameter months_of_difference %} MONTH) AS period_later
      FROM `chb-prod-data-cust.prod_segmentation.segmentation_history`
      WHERE 1=1
          -- AND brand_id = 8
          AND lifecycle='new'
      GROUP BY
        brand_id,
        customer_unique_id
  ), flow AS (
        SELECT
        DISTINCT
               sh.brand_id,
               sh.customer_unique_id,
               ncp.new_period,
               ncp.period_later,
               'new' AS lifecycle_from,
               sh.lifecycle AS lifecycle_to
        FROM new_customer_period ncp
        LEFT JOIN `chb-prod-data-cust.prod_segmentation.segmentation_history`  sh
               ON ncp.brand_id = sh.brand_id AND ncp.customer_unique_id = sh.customer_unique_id
              AND  LEFT(CAST(ncp.period_later AS STRING),7)  = LEFT(CAST(sh.to_date AS STRING),7)
        WHERE ncp.new_period <= DATE_ADD(CURRENT_DATE(), INTERVAL - ({% parameter months_of_difference %} +1) MONTH)
          AND sh.lifecycle IS NOT NULL

  ) --SELECT * FROM  flow WHERE lifecycle_to = 'active' -- months_of_difference

  , stage AS (


  SELECT
    brand_id,
    LEFT(CAST(period_later AS STRING),7) AS period_later,
    LEFT(CAST(new_period AS STRING),7) AS period,
    lifecycle_from,
    count(distinct customer_unique_id) AS base_customer_count,
    count(distinct CASE WHEN lifecycle_to = 'active' THEN customer_unique_id ELSE NULL END) AS active_count,
    count(distinct CASE WHEN lifecycle_to = 'sporadic' THEN customer_unique_id ELSE NULL END) AS sporadic_count,
    count(distinct CASE WHEN lifecycle_to = 'churned' THEN customer_unique_id ELSE NULL END) AS churned_count,
  FROM flow
  WHERE LEFT(CAST(new_period AS STRING),7) is not null and brand_id is not null
  GROUP BY
    brand_id,
    LEFT(CAST(new_period AS STRING),7),
    LEFT(CAST(period_later AS STRING),7),
    lifecycle_from
    --,lifecycle_to
  )

    SELECT *,
        active_count/base_customer_count AS active_pct,
        churned_count/base_customer_count AS churned_pct
    FROM stage;;
  }

  dimension: pk {
    type: string
    primary_key: yes
    hidden: yes
    view_label: "Segmentation"
    sql: CONCAT(CAST(${TABLE}.brand_id AS STRING), '_',  CAST(${TABLE}.period AS STRING));;
  }


  dimension: brand_id {
    type: number
    hidden: yes
    view_label: "Segmentation"
    sql: ${TABLE}.brand_id ;;
  }

  dimension: period {
    type: number
    hidden: yes
    view_label: "Segmentation"
    group_label: "History"
    sql: ${TABLE}.period ;;
  }

  dimension: period_later {
    type: number
    hidden: yes
    view_label: "Segmentation"
    group_label: "History"
    sql: ${TABLE}.period_later ;;
  }



  measure: active_count {
    type: sum
    view_label: "Segmentation"
    group_label: "History"
    sql: ${TABLE}.active_count ;;
  }

  measure: sporadic_count {
    type: sum
    view_label: "Segmentation"
    group_label: "History"
    sql: ${TABLE}.sporadic_count ;;
  }

  measure: churned_count {
    type: sum
    view_label: "Segmentation"
    group_label: "History"
    sql: ${TABLE}.churned_count ;;
  }

  measure: base_customer_count {
    type: sum
    view_label: "Segmentation"
    group_label: "History"
    hidden: no
    sql: ${TABLE}.base_customer_count ;;
  }

  measure: active_pct {
    type: number
    value_format: "0.000"
    view_label: "Segmentation"
    group_label: "History"
    sql: IFNULL(${active_count}/NULLIF(${base_customer_count},0),0) ;;
  }

  measure: churned_pct {
    type: number
    value_format: "0.000"
    view_label: "Segmentation"
    group_label: "History"
    sql: IFNULL(${churned_count}/NULLIF(${base_customer_count},0),0) ;;
  }


}
