view: base_customers_distinct {
  view_label: "Customers"

  sql_table_name: `chb-prod-data-cust.prod_base_customers_final.base_customers_distinct`

    ;;




  dimension: brand_customer_unique_id {
    group_label: "IDs and Keys"
    primary_key: yes
    type: string
    sql: ${TABLE}.brand_customer_unique_id ;;
  }

  dimension: is_generic_customer {
    group_label: "Customer Dimensions"
    type: yesno
    sql: ${TABLE}.is_generic_customer ;;
  }

  dimension: muse_enrolled_brand {
    group_label: "Customer Attributes"
    type: string
    sql: ${TABLE}.muse_enrolled_brand ;;
  }

  dimension: brand_id {
    hidden: yes
    type: number
    sql: ${TABLE}.brand_id ;;
  }

  dimension: brand_name {
    group_label: "Customer Attributes"
   hidden: yes
    type: string
    sql: ${TABLE}.brand_name ;;
  }

  dimension: MUSE_tier {
    group_label: "Customer Attributes"
    hidden: no
    type: string
    sql: ${TABLE}.muse_tier;;
  }


  dimension: MUSE_status {
    group_label: "Customer Attributes"
    hidden: no
    type: string
    sql: ${TABLE}.muse_status;;
  }

  dimension: MUSE_total_points {
    group_label: "Customer Attributes"
    hidden: no
    type: number
    sql: ${TABLE}.muse_total_points;;
  }


  dimension: MUSE_tier_name {
    group_label: "Customer Attributes"
    label: "Muse tier name"
    hidden: no
    type: string
    sql: CASE WHEN ${MUSE_tier} = 'T1' THEN 'Emerald'
     WHEN ${MUSE_tier} = 'T2' THEN 'Sapphire'
     WHEN ${MUSE_tier} = 'T3' THEN 'Ruby'
     WHEN ${MUSE_tier} = 'T4' THEN 'Elite'
ELSE null END
    ;;
  }


dimension_group: muse_registration

{
  type: time
  timeframes: [
    raw,
    date,
    week,
    month,
    quarter,
    year
  ]
  convert_tz: no
  datatype: date
  sql: ${TABLE}.muse_registration_date   ;;
}


  dimension_group: brand_registration {

    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.brand_registration_date ;;
  }

  dimension: email_validated {
    group_label: "Customer Attributes"
    type: string
    label: "{% if _user_attributes['customer_pii_access'] == 'yes' %} Email
    {% else %} Email (Redacted due to insufficient permissions)
    {% endif %}"
    sql:
      {% if _user_attributes['customer_pii_access']  == 'yes' %}
          ${TABLE}.email_validated
      {% else %}
          MD5(${TABLE}.email_validated)
      {% endif %}   ;;

  }








  dimension: mobile_validated {

    group_label: "Customer Attributes"

    type: string

    label: "{% if _user_attributes['customer_pii_access'] == 'yes' %} Mobile Number
    {% else %} Mobile (Redacted due to insufficient permissions)
    {% endif %}"
    sql:
      {% if _user_attributes['customer_pii_access']  == 'yes' %}
          ${TABLE}.mobile_validated
      {% else %}
          MD5(${TABLE}.mobile_validated )
      {% endif %}   ;;

    }

  dimension: last_name {
    group_label: "Customer Attributes"

    type: string

    label: "{% if _user_attributes['customer_pii_access'] == 'yes' %} Last Name
    {% else %} Last Name (Redacted due to insufficient permissions)
    {% endif %}"
    sql:
      {% if _user_attributes['customer_pii_access']  == 'yes' %}
          ${TABLE}.last_name
      {% else %}
          MD5(${TABLE}.last_name)
      {% endif %}   ;;

    }

  dimension: first_name {
    group_label: "Customer Attributes"

    type: string

    label: "{% if _user_attributes['customer_pii_access'] == 'yes' %} First Name
    {% else %} First Name (Redacted due to insufficient permissions)
    {% endif %}"
    sql:
      {% if _user_attributes['customer_pii_access']  == 'yes' %}
          ${TABLE}.first_name
      {% else %}
          MD5(${TABLE}.first_name)
      {% endif %}   ;;

    }



  dimension: gender {
    group_label: "Customer Attributes"
    type: string
    sql: ${TABLE}.gender ;;
  }

  dimension: Age {
    group_label: "Customer Attributes"
    type: number
    sql: date_diff(current_date(),${birth_date},year)
;;

}

    dimension: age_sort {
      hidden: yes
      type: number
      sql: CASE
                        WHEN ${Age} <= 15 THEN 1
                          WHEN ${Age} <= 18 THEN 2
                          WHEN ${Age} <= 21 THEN 3
                          WHEN ${Age} <= 24 THEN 4
                          WHEN ${Age} <= 30 THEN 5
                          WHEN ${Age} <= 30 THEN 6
                          WHEN ${Age} <= 36 THEN 7
                          WHEN ${Age} <= 45 THEN 8
                          WHEN ${Age} <= 54 THEN 9
                          WHEN ${Age} > 54  THEN 10
                          ELSE null
                          END;;
    }




  dimension: Age_Range {
    group_label: "Customer Attributes"
    order_by_field: age_sort
    hidden: no
    type: string
    sql: CASE
                          WHEN ${Age} <= 15 THEN "<15"
                          WHEN ${Age} <= 18 THEN "15-18"
                          WHEN ${Age} <= 21 THEN "18-21"
                          WHEN ${Age} <= 24 THEN "21-24"
                          WHEN ${Age} <= 30 THEN "24-30"
                          WHEN ${Age} <= 36 THEN "30-36"
                          WHEN ${Age} <= 45 THEN "36-45"
                          WHEN ${Age} <= 54 THEN "45-54"
                          WHEN ${Age} > 54  THEN ">54"
                          ELSE null
                          END;;
  }

  dimension: age_sort_CRM {
    hidden: yes
    type: number
    sql: CASE
                        WHEN ${Age} <= 18 THEN 1
                          WHEN ${Age} <= 20 THEN 2
                          WHEN ${Age} <= 29 THEN 3
                          WHEN ${Age} <= 39 THEN 4
                          WHEN ${Age} <= 49 THEN 5
                          WHEN ${Age} <= 59 THEN 6
                          WHEN ${Age} > 60  THEN 7
                          ELSE null
                          END;;
  }
  dimension: Age_Range_CRM {
    group_label: "Customer Attributes"
    label: "Age Range CRM"
    order_by_field: age_sort_CRM
    hidden: no
    type: string
    sql: CASE
                          WHEN ${Age} <= 18 THEN "<18"
                          WHEN ${Age} <= 20 THEN "18-20"
                          WHEN ${Age} <= 29 THEN "21-29"
                          WHEN ${Age} <= 39 THEN "30-39"
                          WHEN ${Age} <= 49 THEN "40-49"
                          WHEN ${Age} <= 59 THEN "50-59"
                          WHEN ${Age} > 60  THEN ">60"
                          ELSE null
                          END;;
  }



  dimension: customer_unique_id {
    group_label: "IDs and Keys"
    value_format: "0"
    type: number
    sql: ${TABLE}.customer_unique_id ;;
  }

  dimension: customer_unique_id_source {
    group_label: "IDs and Keys"
    type: string
    sql: ${TABLE}.customer_unique_id_source ;;
  }








  dimension: muse_member_id {
    group_label: "IDs and Keys"
    type: number
    sql: ${TABLE}.muse_member_id ;;
  }

  dimension: salesforce_account_id {
    group_label: "IDs and Keys"
    label: "Salesforce Account ID"
    hidden: yes
    type: string
    sql: ${TABLE}.salesforce_account_id ;;
  }



  dimension: Is_Muse_member {
    group_label: "Customer Dimensions"
    label: "Is Muse Member"
    type: yesno
    sql: ${muse_member_id} is not null  ;;
  }

  dimension: Is_CRM_member {
    group_label: "Customer Dimensions"
    label: "Is CRM Member"
    type: yesno
    sql: ${speedbus_id} is not null  ;;
  }





  dimension_group: birth {


    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      month_name,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.birthdate ;;
  }

  dimension: speedbus_id {
    group_label: "IDs and Keys"
    label: "CRM Customer ID (Speedbus ID)"
    type: string
    sql: ${TABLE}.speedbus_id ;;
  }

  dimension: country_of_residence {
    group_label: "Customer Attributes"
    type: string
    sql: ${TABLE}.country_of_residence;;

  }

    dimension: nationality {
    group_label: "Customer Attributes"
    type: string
    sql: ${TABLE}.nationality;;

  }

    dimension: residency {

    group_label: "Customer Attributes"
    type: string
    sql: ${TABLE}.residency;;

  }

    dimension: ethnicity {
    group_label: "Customer Attributes"
    type: string
    sql: ${TABLE}.ethnicity;;

  }

  dimension: preferred_language {
    group_label: "Customer Attributes"
    type: string
    sql: ${TABLE}.preferred_language ;;
  }

  dimension: contactability_y_n {
    group_label: "Customer flags"
    type: string
    sql: case when ${contactability} = 'Not Contactable' then 'No' else 'Yes'END
      ;;
  }
  dimension: nationality_y_n {
    group_label: "Customer flags"
    type: string
    sql: case when ${nationality} is null then 'No' else 'Yes'END   ;;
  }
  dimension: country_of_residence_y_n {
    group_label: "Customer flags"
    type: string
    sql: case when ${country_of_residence} is null then 'No' else 'Yes'END   ;;
  }
  dimension: residency_y_n {
    group_label: "Customer flags"
    type: string
    sql: case when ${residency} is null then 'No' else 'Yes'END   ;;
  }
  dimension: gender_y_n {
    group_label: "Customer flags"
    type: string
    sql: case when ${gender} is null then 'No' else 'Yes'END   ;;
  }
  dimension: prefered_language_y_n {
    group_label: "Customer flags"
    type: string
    sql: case when ${preferred_language} is null then 'No' else 'Yes'END   ;;
  }
  dimension: birthdate_y_n {
    group_label: "Customer flags"
    type: string
    sql: case when ${birth_date} is null then 'No' else 'Yes'END   ;;
  }

  dimension: Top_customers_KUW {
    group_label: "Sample Sets"
    label: "Sample Tory Burch Kuwait"
    type: yesno
    sql: cast(${customer_unique_id} as string) in ('5032285801094836789','-3671467048073081836','3887468338792290309','-2539454867156629618','5797284501182980024','-5856138111560293847','-437259322853300478','1117668028809507032','-6345252528250689966','-4250411145581735999','-2043272004870216452','-4595019755719968680','809104745913950410','5768781305434607045','-4322489404612401074','-6048388536002200684','8063365991229348126','-6428712072628227897','-5172858609997489738','8668169576540765331','-8098475012317150215','4877846241277458146','-3007618660187629822','6089646235331919950','-533169631603978549','2190225652016802395','4601121362642261106','-724579416392370290','-5555597079350949979','299234571350634887','-2190955291633721526','-3296176887546989825','4919739970841092','-8195255373146014740','4476760117533819454','7838673206137231555','8670302734387680646','3992220887662003388','-6212715815526800478','1632070615942166583','6534914383510730399','-6362430479940386463','-6398187681518299365','-1258480237590027604','-4620233820124689794','8124804490225792589','-5098600562481080874','-1557061413493149749','4425858705755459177')
      ;;
  }
  dimension: Top_customers_KSA{
    group_label: "Sample Sets"
    label: "Sample Tory Burch KSA"
    type: yesno
    sql: cast(${customer_unique_id} as string) in ('-2937231614805652292','5933459771839985272','630086719532228714','8952217292989770086','527384431091823813','-8715399836209436875','-3225682860065075287','-1426898712134467638','8555113626532239785','4299140198095587195','-7830701480042686601','-5010661848665476996','-6724533639412554549','2929703100027383576','3061260221873340722','8533229519699665241','-5818795516181843696','-3183860485270977993','8644748212621567746','-4296237149721118439','8832742587598488157','1244244850546233521','-8174830071441728120','7571389327594561259','-3295647607996287605','349824137439815518','8602690556222599462','1978549735677718807','3121601762319250346','4893264254342394623','4289666932106008098','47091640023058348','-1803121876595726981','-2984384926884619688','5969488305402073658','-7613671111265048707','-1671536612357310257','5383099782392008884','2022410275845746689','-227853116885090714','6756663682701791261','-3853845969563553210','3093319982918332326','-6818065757732271545','8393083059884607025','-4994456034465021843','-7039604714084708085','8803364404437027413','-4023943403301545081','-8308716353773068714','-1198272944913349305','8977625140499170105','-6488545127741942914','-5950436724737651520','-7046959616354181120','-7069359782602144950','-3703796559521349654','3181021746678704756','-7431170302340503123','-808730975663033617','-3440334575508337672','-5703114883403710760','-4280558333058065381','-1613679239580046084','6998118893955668810','3464917581835035138','3974900579174653886','-4502255426838976367','-5464507967988465329','8297540715649142633','3443338774063398155','8935375940693256868','7945813943346267049','-8077245356128733473','-3633862893865148598','8734367244756417150','8406606358317680357','-6311406397863215345','7078648032056670511','-7671098701511108891','6160673315680934147','-808850568438982686','-4802888355358413241','2604695765258109052','1806750669233068071','-5739463974536753838','6209340357680017197','-6465429665767017236','8957006491275741321','8483661349828179855','1782465420579646034','8273028134440305466','-3644515813678090141','3662506028946171371','20948332377731155','2609360278372336573','-7246302946751754525','8380558937776372001','-8331841617345896842','-4070489291391082524','-2764724197222405879','-7052906804575454245','4050325777991542604','3866122170837602668','-750265225463340996','4917366877124120103','-2261450139515492587','6789193868375037183','5577766879035016153','-6305495162360542132','-2501676105325271270','5531755300217789564','2695013856533756199','-7783162903135700939','6739367792646509695','-5071556985269736036','6327413937172306938','-9081052670853689792','-4304446347534309970','8057452217133721059','7289288582566349979','-5351928432949822526','6945099781093425976','8648283030777588085','-6589263306713676811','-7453659658890576644','-8073006370224924105','-2950228031565165536','-6308190796967939868','3043923967922189454','6266420482050848537','-7210374043970025341','376673972333763320','-3891885320845664494','7777413579533643744','-8725115255996733250','-6723394992520500069','-3218162701336687444','-7627476821347446395','-8397592756302628879','-926203200235709275','-8972261349014103789','-4429108548802439305','-951876357102603287','6553707142382305144','-2982771223235377602','-5267659954448557014','-937261055820077488','6445345789020751524','2068329152430836590','7508397038919334245','8748444427235721249','-4808526043544608800','8441545792352246866','-7461843720538355126','-7133607777632332706','-2477746013826739040','-819704768715896379','-3351719354719737699','-8425540376867846041','-6084796156061880960','405620333404543708','-6679902281305009244','4187787493273822872','-9069143179583785453','-5287788967052724671','-5678177586836083464','-1277620373908937136','-6460341389179751893','-5191537983174918947','-788419036096533365','3356499828642698763','-2807211638306531435','156196757805598087','7258391127116817275','3210464725596369144','5139203135059741589','4111820225874149892','-9178779909169781980','-2009810121097735274','6466715857955568858','-3964126121557096977','-4947292390992006866','6570597439705537805','-1975904332938930586','-875285971521520793','2883912353064207168','-3169911574686160047','1753209159798389075','569415197265409741','-664487984688981673','-6737687775677239933','-7466605233644581746','-130276464124733783','-3903845886980090018','5506613851847484550','7608551975615655533','1242430816996437693','-4801495869426637683','-5755247609452436777','-5545303364244675423','8501743445539761822','-7130394604365919624','3515892167394983204','7517172716311927030','-1060757488835155705','-689580197665754847','8695744540019796788','-6862884336498301233','-1918243417315766881','4021701032575663435','-5917600742134607411','6049671357163366746','8786174919612202900','-4886918717679420159','-6361527211571941915','7162214656579014781','2150406294504505567','-2227872795134431050','2670355189053383691','-4063013422146265534','6891169528288347565','-2439359023041378197','-9095673499462383499','2280013434860024539','1345569658059519849','-3927321258702480149','-3947138885390227095','-4808808792241766997','9152332453002472807','-4379794877795891203','5651802273910276510','2714176381166277114','-2894758649551716838','-5113954125827473277','-1976123298523126314','6714304384285932960','7114235223621587350','-4646319212491892134','5474595313886618490','-6073121479332496386','3065153795370130579','4614058503634598757','-7781258305808125281','7703853643691022962','-1052000074189214358','7562393227899337981','-1388112122212648275','-9114017902798413587','-7013343791923849367','-5072380843541702073','-3667308420290827491','-8141579694087509776','-4030481468987312350','-4542919568702140145','-4526416582653066762','-6800086837533706668','7662519648517498142','-3219925589555869664','4637825998044159435','-2545185110537225023','3052912805358825441','1906480270052268894','-8609313437639624045','6043333255681758318','-414910471411307659','5406844265864578915','-7454774151785624626','-281143139196206832','-1328255815678734070','-5451432020754910684','-2214241580108400092','-3660188025330218748','5591568536864193828','5299281570835664139','-4830710126116407798','-8431627490603358620','-7624307791086439953','5845080600323643457','-6730106612924164672','-4170057944059530245','-6592729187793387878','-1868465269284246300','1421452368328692145','-218835499886626079','-829060402235063322','-1756597793682787118','1386624786541941634','514123643605395468','-5956273296669757363','-1194890731751080630','-2441540085753474705','-3979940489345350288','-8858612653446017853','-2504102424067579906','-8908569753223897428','482822425156558094','5551204062195098203','-6959309066145695805','4637260074894426706') ;;

  }
  dimension: Top_customers_UAE {
    group_label: "Sample Sets"
    label: "Sample Tory Burch UAE"
    type: yesno
    sql: cast(${customer_unique_id} as string) in ('-5668250355634631506','-693570828158151264','3597922930763645671','-8222178730364909301','-5293347753133769115','4687018406543504228','6039795093080885113','8278187503933735508','1300814105631478558','3622611125393639778','7011892554164625484','5816188129980649002','-5711544182653943239','8843824360995936549','1652438487414499141','-5153094756356462723','-4925780256881383970','8344553217250609065','-1148264316344192630','619279342996793086','-498055553686440749','7884237435627744755','-3839079382604151048','8930487200643671490','554924825149971295','-1773993604539548240','8579185625235756584','3265103260762444341','-7391423555825816455','7231324182012288304','-8806364786664059890','5097831181756730257','-3531384715260789709','-7509347425348981997','1634482717250791578','3356227819863215762','-2345893087593993313','8096156603083254244','1841293378896613882','128963952955864419','8467435981059581322','-5369688907493515570','6104997293878645963','-730082798881411544','-1129437008724074246','-6030184191607979769','4148421346630489945','-9057100475052563765','4337568902993065800','-8350069710756194911','-1612105112663071181','1154624162060762144','-1132870429777815421','2404359534295529207','8958928502481622490','8250293872340003049','364992904952943039','3118179955120620920','-7573001593703262507','9075333207206251528','2707416552107478230','-7112842915107005369','-8234176472762591763','-7393706027715054354','4893376787219206777','-5696149736265485923','-2069025536952162504','-966365942898315578','-9139150831148595415','1417951014064217331','8079429301653942960','-7716626478086949695','2522640010124998130','8914563870973066262','8263440204934900000','-3493177225961020246','-6316035112110005401','-4165936632492345124','-3692776460517790497','6269400457415591648','-1427388329856219351','3919027637362425729','-7897914153498136051','-8975376745736147222','-2421187239444951618','-6492600910005722643','-6252252349643489273','-5402526787367058908','-549821476922606915','6864264262625598777','-4399778369544653117','15512799837659905','1025403328021512118','3834607114176365226','3669065165622431145','5199222321349823410','7873492656392475069','-6652332361551577875','-474969193030975904','2464602745202517392','-7914327479627941148','4417739754915051858','7592993484130796765','6051176103253913181','7122961188356476376','-2072221504299761315','-5428266400089252045','4861401612499831813','4295907304720748279','-454837231361043986','-8704912005312847213','-8604057731308084058','-3580045949530235233','-2981624800814310137','1664268267928801861','-2395002054938688383','-3624302594848488097','2747648483804323239','-1442774145989511374','3918602393208255870','-6139370692037481177','-809292983423426343','-4527116179859684593','879908953995920931','-7094365471004088052','2391463747141546329','984937451766884488','-6696612946723103198','-7238194703041823270','-5717274610824275430','-6799373253642348487','-8148156892965222793','2283684473760231958','4848261577389312228','8604385628739167827','8454956762002305675','-570012170050058959','-917347724245724935','3583321486449806697','-7732199399678516814','1821426127631595629','-9219097448751538053','257666436635769250','5173170593780070947','3297079905301146317','-6670686357535245731','3226617978585688062','-666260146708600003','1941549892436168552','2687854292497306447','-923044888637827333','6598435759179153115','2925484028882819471','-7730756945889758106','492854842305000883','-6214984020352979630','-7936041800899110342','1459147893300376537','-640936612451373306','-388581740176394133','221920692384372501','8288016863901844433','-4176501916174728633','8061473414028455220','1152664631066926812','8537152576827608328','7674218884268014517','1973297883835843504','-3758283008352682187','-5366236027918978773','-761284024667289469','608995118128681712','-7149020646397619446','8077905499726330755','-2777381810798951400','7394236018084153228','2657637287751925338','-8157764619207819412','-7159959549975790385','2548086301767641273','2405185562797193980','5184354689957826312','-3324390563230872979','-7785692350818538416','-4636928973979667637','-7969564087556513809','-4146945691921830514','7580199166695885551','-2448110043249036861','-4204798705691108030','-2111339445288852377','-5295502492009619419','-2533463675029557874','3563479549425020247','1181800403945481444','8673674088841228936','5310617813178637360','4142466516405211395','5497368137291396831','8049766363982976485','-898826678749056943','-2720207150274605316','2705837315465829987','-8938983275007660753','-1464904662728552473','-192327034672163689','7341337798186171719','-4925155511099268409','-6858520944919381353','-7622925090033189758','-6907641332753064596','7085509912298757087','-5878947167418282675','-4088046533171716071','1907045334079249205','2910699735740669183','3469441274196024386','-8534623648529101427','8759666361210167197','-3339180322639714323','-2831286573183146302','-4407953719039794806','1213543958805671489','-7349504971255592198','4238605068051292243','1356412815424701507','1756025682110510652','-4321981155835532819','80931428216032031','3881363580405736322','2691017913445507122','8767514923387239724','2678506808450696512','1247690590833725638','8568701531413687207','-3757626150967713953','-3461682841213952564','980495153561415565','4100855630130868091','5531163296105479681','-6676325557567103901','-6226330742376262065','223320638424055971','8634939369351200580','2077229001198484476','3832663304067450792','6239553105273288325','7336822399794660810','-47712231046247510','3889009475096688912','-7257988975021431547','721148294147514595','8180352446829248679','2031675934881666332','-7097722628879210457','-2681501902870830815','6863112599761073361','-4649253795018879702','5447719821862115407','-5102079604039721406','8245073080819829740','5200599734357008294','-2534526579783169897','-6037110097422400250','4601763495591945432','-8942804111919080076','-730497313166421761','-4849170122488532657','2471950790475456140','-7954409793525068138','-3317127144740674227','1433663473471810375','3298031150993762301','-2559157147479707351','2605353063106882077','-3321279320151441691','4242975082769473649','117985435396742111','3278047708177661451','-3090652203154479274','5384916242908227657','3984151686096477420','1662673375326058024','3970816852130481214','4266319085059956381','5333103332820500781','2455517248205096676','3605227751096916059','-5963979115815538748','-7497146467108990792','6670133567436716731','8003164315075025419','-341295160001433984','-6560841728786539172','-5404181124579773651','7441880433165340871','7111770092441185853','6344048694421275425','7510258049476349973','-1438333308559394296')
      ;;
  }

dimension: contactability {
  type: string

  sql: case when NULLIF(${TABLE}.email_validated,'') is null and  NULLIF(${TABLE}.mobile_validated,'') is null then "Not Contactable"
  when NULLIF(${TABLE}.email_validated,'') is not null and  NULLIF(${TABLE}.mobile_validated,'') is null then "Email Only"
  when NULLIF(${TABLE}.email_validated,'') is null and  NULLIF(${TABLE}.mobile_validated,'') is not null then "Mobile Only"

  else "Email & Mobile" end ;;

}
  dimension: contactability_email {
    type: string
    sql: CASE WHEN ${email_validated} IS NOT NULL THEN 'Yes' ELSE 'No' END;;
  }
  dimension: contactability_mobile {
    type: string
    sql: CASE WHEN ${mobile_validated} IS NOT NULL THEN 'Yes' ELSE 'No' END;;
  }


  dimension: is_new_customer_universal {
    type: yesno
    sql: case when ${cust_first_date_by_store.brand_customer_unique_id} is null then false else true end ;;
  }

measure: group_transacted_customer_count {
  hidden: yes
  group_label: "Group Customer Measures"
  type: count_distinct
  description: "A person who buys goods across brands part of Chalhoub Group"
  sql: ${customer_unique_id} ;;
}


measure: group_total_customer_count {
  group_label: "Group Customer Measures"
  description: "Total count of customers in the database. Not filterable by any dimension"
  type: number
  sql: (select count(distinct customer_unique_id)
  from `chb-prod-data-cust.prod_base_customers_final.base_customers_distinct`);;

}

measure: count_group_customers {
  hidden: yes
  group_label: "Group Customer Measures"
  description: "Number of cross brand customer profiles. Can be filtered by other customer attributes."
  type: count_distinct
  sql: ${customer_unique_id} ;;
}

measure: count {
  hidden: yes
  type: count
  drill_fields: [last_name, brand_name, first_name]
}



# -------------------------customer sample sets--------------------------






  dimension: muse_retained_members{
    group_label: "Sample Sets"

    description: "Muse members who have shopped pre and post muse launch. This sample set is dynamic and will grow over time."
    type: yesno
    sql: ${brand_customer_unique_id} in (

                        select distinct brand_customer_unique_id

                        from
                        `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr`
                        where atr_trandate>date_add( '2019-08-25',INTERVAL ( select DATE_DIFF('2019-08-25',CURRENT_DATE(),DAY)) DAY)
                        and atr_trandate <'2019-08-25'
                        and brand_customer_unique_id
                        in (
                        select brand_customer_unique_id
                        from
                        `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr`
                        where atr_trandate<CURRENT_DATE()
                        and atr_trandate >'2019-08-25'
                        and atr_muse_id is not null
                        and brand_customer_unique_id in
                        (select brand_customer_unique_id from `chb-prod-data-cust.prod_base_customers_final.base_customers_distinct`
                        where muse_member_id is not null)
                        ))

                                                and muse_member_id not in(
            select muse_member_id from (

            SELECT count(distinct customer_unique_id), muse_member_id FROM `chb-prod-data-cust.prod_base_customers_final.base_customers_distinct`
            where muse_member_id is not null
            group by muse_member_id
            having count(distinct customer_unique_id)>1
            )
            )
            ;;

    }

    dimension: muse_reactivated_members{

      group_label: "Sample Sets"
      description: "Muse members who have shopped before 17/09/2017 and then not again until after Muse launch."
      type: yesno
      sql: ${brand_customer_unique_id} in (


                                  select brand_customer_unique_id
                                  from
                                  `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr`
                                  where atr_trandate<CURRENT_DATE()
                                  and atr_trandate >'2019-08-25'
                                  and atr_muse_id is not null
                                  and brand_customer_unique_id in
                                  (select brand_customer_unique_id from `chb-prod-data-cust.prod_base_customers_final.base_customers_distinct`
                                  where muse_member_id is not null)


                                       and brand_customer_unique_id not in

                              ( select distinct brand_customer_unique_id

                                from
                                `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr`
                                where atr_trandate<='2019-08-25'
                                and atr_trandate>='2018-09-17')

                                and brand_customer_unique_id in

                              ( select distinct brand_customer_unique_id

                                from
                                `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr`
                                where           atr_trandate<='2018-09-17')



                                )
                                                        and muse_member_id not in(
                select muse_member_id from (

                SELECT count(distinct customer_unique_id), muse_member_id FROM `chb-prod-data-cust.prod_base_customers_final.base_customers_distinct`
                where muse_member_id is not null
                group by muse_member_id
                having count(distinct customer_unique_id)>1
                )
                );;

      }

      dimension: muse_retained_members_static{

        group_label: "Sample Sets"
        description: "Muse members who have shopped 12 months pre and 12 month post muse launch. The sample set is static."
        type: yesno
        sql: ${brand_customer_unique_id} in (

                                         select distinct brand_customer_unique_id

                                    from
                                    `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr`
                                    where atr_trandate>='2018-08-25'  and atr_trandate <'2019-08-25'

                                    and brand_customer_unique_id
                                    in (
                                    select brand_customer_unique_id
                                    from
                                    `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr`
                                    where  atr_trandate >='2019-08-25'
                                    and atr_trandate <'2020-08-25'
                                    and atr_muse_id is not null

                                    )
                                    and brand_customer_unique_id in
                                  (select brand_customer_unique_id from `chb-prod-data-cust.prod_base_customers_final.base_customers_distinct`
                                  where muse_member_id is not null))
                                                          and muse_member_id not in(
                    select muse_member_id from (

                    SELECT count(distinct customer_unique_id), muse_member_id FROM `chb-prod-data-cust.prod_base_customers_final.base_customers_distinct`
                    where muse_member_id is not null
                    group by muse_member_id
                    having count(distinct customer_unique_id)>1
                    )
                    )

                                  ;;

        }


        dimension: non_muse_retained_members_static{

          group_label: "Sample Sets"
          description: "Non-Muse members (CRM only) who have shopped 12 months pre and 12 month post muse launch. The sample set is static."
          type: yesno
          sql: ${brand_customer_unique_id} in (

                                                      select distinct brand_customer_unique_id

                                            from
                                            `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr`
                                            where atr_trandate>='2018-08-25'  and atr_trandate <'2019-08-25'
                                            and brand_customer_unique_id
                                            in (
                                            select brand_customer_unique_id
                                            from
                                            `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr`
                                            where atr_trandate<CURRENT_DATE()
                                            and atr_trandate >='2019-08-25'
                                            and atr_trandate <'2020-08-25'
                                            )
                                            and brand_customer_unique_id  in (
                                            select brand_customer_unique_id from
                                            `chb-prod-data-cust.prod_base_customers_final.base_customers_distinct`
                                            where muse_member_id is null));;


          }

          dimension: non_muse_retained_members{

            group_label: "Sample Sets"
            description: "Non-Muse members who have shopped pre and post muse launch. This sample set is dynamic and will grow over time."
            type: yesno
            sql: ${brand_customer_unique_id} in (select distinct brand_customer_unique_id

                                                    from
                                                    `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr`
                                                    where

                                                    atr_trandate>date_add( '2019-08-25',INTERVAL ( select DATE_DIFF('2019-08-25',CURRENT_DATE(),DAY)) DAY)
                                                   and
                                                    atr_trandate <'2019-08-25'
                                                    and brand_customer_unique_id
                                                    in (
                                                    select brand_customer_unique_id
                                                    from
                                                    `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr`
                                                    where atr_trandate<CURRENT_DATE()
                                                    and atr_trandate >'2019-08-25'

                                                    )
                                                    and brand_customer_unique_id  in (
                                                    select brand_customer_unique_id from
                                                    `chb-prod-data-cust.prod_base_customers_final.base_customers_distinct`
                                                    where muse_member_id is null)
                                         ) ;;

            }

            dimension: new_muse_exclusive {

              label: "New Muse Only Customers"
              group_label: "Sample Sets"
              description: "New Muse members who have no CRM record"
              type: yesno
              sql: ${brand_customer_unique_id} in
                            (select brand_customer_unique_id from `chb-prod-data-cust.prod_base_customers_final.base_customers_distinct` where muse_member_id is not null and speedbus_id is null
                            and brand_customer_unique_id in (


                                  select brand_customer_unique_id
                                  from
                                  `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr`
                                  where
                                  atr_trandate >='2019-08-25'
                                  and crm_customer_id is null)
                                  and muse_member_id not in(
                                   select muse_member_id from (

                                   SELECT count(distinct customer_unique_id), muse_member_id FROM `chb-prod-data-cust.prod_base_customers_final.base_customers_distinct`
                                   where muse_member_id is not null
                                   group by muse_member_id
                                   having count(distinct customer_unique_id)>1
                                   )));;
            }




            dimension: new_crm_exclusive {

              label: "New CRM Only Customers"
              group_label: "Sample Sets"
              description: "New CRM members since Muse launch who have not signed up to Muse."
              type: yesno
              sql: ${brand_customer_unique_id} in
                            (select brand_customer_unique_id from `chb-prod-data-cust.prod_base_customers_final.base_customers_distinct` where muse_member_id is null and speedbus_id is not null
                            and brand_customer_unique_id in (


                                  select brand_customer_unique_id
                                  from
                                  `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr`
                                  where
                                  atr_trandate >='2019-08-25'
                                  and crm_customer_id is not null
                                  and atr_muse_id is null
                                  and brand_customer_unique_id is not null

                                  and brand_customer_unique_id
                                  not in (
                                   select brand_customer_unique_id  from

                                   `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr`
                                    where
                                  atr_trandate <'2019-08-25'
                                  and brand_customer_unique_id  is not null
                                   )));;
            }




            dimension: is_muse_elite {

              group_label: "Sample Sets"
              type: yesno
              sql: cast(${muse_member_id} as string) in ('720101730854',  '720101481797', '720101093386', '720100964843', '720100829228', '720100622367', '720100523839', '720100485682', '720100479396', '720100260374',
                                                        '720100221160', '720100211997', '720100182305', '720100116303', '720100377350', '720102693713', '720100039299', '720100491151', '720100324048', '720100338238',
                                                        '720101145616', '720100286320', '720100580177', '720100271991', '720100161077', '720100932998', '720101634528', '720100778912', '720100660243', '720100384059',
                                                        '720100456030', '720100049413', '720100131906', '720101712688', '720100170607', '720100722043', '720100308140', '720100480063', '720100259376', '720100580359',
                                                        '720101005398', '720100750283', '720100731390', '720100462053', '720100102568', '720100427213', '720100197964', '720100157828', '720100092967', '720100213779',
                                                         '720100749038',  '720100162992', '720100329070', '720100352585', '720102784074', '720100214462', '720101457078', '720100740250', '720100280737', '720101370727',
                                                        '720102451898','720104239135') ;;
            }


# retention rate



parameter: district_brand {
  label: "1 - Retention Brand Select"
  view_label: "Retention Rate"

  description: "You need to select the brand (district) for retention calculation. This must match the selected district from locations view."

  allowed_value: { label: "All"  value: "0"}
  allowed_value: { label: "Muse Brands"  value: "-1"}

allowed_value: {
    label: "ACQUA DI PARMA"  value: "910001"
  }
allowed_value: {
    label: "ALICE & OLIVIA"  value: "74"
  }
allowed_value: {
    label: "ATELIER COLOGNE"  value: "1330001"
  }
allowed_value: {
    label: "B2B RETAIL GIFTS"  value: "40"
  }
allowed_value: {
    label: "B8ta"  value: "1390001"
  }
allowed_value: {
    label: "BABY DIOR"  value: "1040301"
  }
allowed_value: {
    label: "BACCARAT"  value: "14"
  }
allowed_value: {
    label: "BEAUTY CONCESSION"  value: "18"
  }
allowed_value: {
    label: "BEAUTY DISTRICT"  value: "1430501"
  }
allowed_value: {
    label: "BEAUTY NATION"  value: "1210001"
  }
allowed_value: {
    label: "BELL & ROSS"  value: "16"
  }
allowed_value: {
    label: "BENEFIT"  value: "840002"
  }
allowed_value: {
    label: "BERLUTI"  value: "30"
  }
allowed_value: {
    label: "BERNARDAUD"  value: "1150001"
  }
allowed_value: {
    label: "BOBBI BROWN"  value: "1470001"
  }
allowed_value: {
    label: "BOND  NO:9"  value: "1020301"
  }
allowed_value: {
    label: "BONPOINT"  value: "700101"
  }
allowed_value: {
    label: "BY FOUZ"  value: "1370001"
  }
allowed_value: {
    label: "BY KILIAN"  value: "1280001"
  }
allowed_value: {
    label: "CADENZZA"  value: "87"
  }
allowed_value: {
    label: "CAROLINA HERRERA"  value: "31"
  }
allowed_value: {
    label: "CELINE"  value: "25"
  }
allowed_value: {
    label: "CH BEAUTY"  value: "1440301"
  }
allowed_value: {
    label: "CHANEL"  value: "1080301"
  }
allowed_value: {
    label: "CHAUMET"  value: "75"
  }
allowed_value: {
    label: "CHRISTOFLE"  value: "13"
  }
allowed_value: {
    label: "COURCELLES"  value: "1300001"
  }
allowed_value: {
    label: "DAUM"  value: "11"
  }
allowed_value: {
    label: "DOLCE&GABBANA"  value: "630001"
  }
allowed_value: {
    label: "DSQUARED2"  value: "38"
  }
allowed_value: {
    label: "DUTY FREE"  value: "1120502"
  }
allowed_value: {
    label: "DYLANS CANDY BAR"  value: "1340101"
  }
allowed_value: {
    label: "ELEMIS"  value: "1490001"
  }
allowed_value: {
    label: "EX NIHILO"  value: "1520001"
  }
allowed_value: {
    label: "EXHIBITION"  value: "730101"
  }
allowed_value: {
    label: "FACES"  value: "17"
  }
allowed_value: {
    label: "FACES EXTERNAL"  value: "170002"
  }
allowed_value: {
    label: "FACTORY OUTLET"  value: "5"
  }
allowed_value: {
    label: "FARFETCH"  value: "1360001"
  }
allowed_value: {
    label: "FASHION CONCESSION"  value: "1230701"
  }
allowed_value: {
    label: "FILA"  value: "1410001"
  }
allowed_value: {
    label: "FREDERIC MALLE"  value: "1480001"
  }
allowed_value: {
    label: "FURLA"  value: "1400001"
  }
allowed_value: {
    label: "GEOX"  value: "27"
  }
allowed_value: {
    label: "GHAWALI"  value: "1090101"
  }
allowed_value: {
    label: "GIFT CONCESSION"  value: "7"
  }
allowed_value: {
    label: "GIORGIO ARMANI"  value: "890002"
  }
allowed_value: {
    label: "HACKETT"  value: "36"
  }
allowed_value: {
    label: "HUGO BOSS"  value: "1390301"
  }
allowed_value: {
    label: "ICE-WATCH"  value: "1000301"
  }
allowed_value: {
    label: "IL GUFO"  value: "1290001"
  }
allowed_value: {
    label: "JAYSTRONGWATER "  value: "12"
  }
allowed_value: {
    label: "KARL LAGERFELD"  value: "86"
  }
allowed_value: {
    label: "KATAKEET"  value: "21"
  }
allowed_value: {
    label: "KENZO"  value: "39"
  }
allowed_value: {
    label: "LACOSTE"  value: "26"
  }
allowed_value: {
    label: "LALIQUE"  value: "15"
  }
allowed_value: {
    label: "LANCOME"  value: "880002"
  }
allowed_value: {
    label: "LANVIN"  value: "640101"
  }
allowed_value: {
    label: "LEVEL KIDS"  value: "1110001"
  }
allowed_value: {
    label: "LEVEL KIDS EXTERNAL"  value: "590002"
  }
allowed_value: {
    label: "LEVEL SHOES"  value: "55"
  }
allowed_value: {
    label: "LEVEL SHOES EXTERNAL"  value: "59"
  }
allowed_value: {
    label: "LLADRO"  value: "50"
  }
allowed_value: {
    label: "LOCCITANE"  value: "19"
  }
allowed_value: {
    label: "LOEWE"  value: "54"
  }
allowed_value: {
    label: "LONGCHAMP"  value: "33"
  }
allowed_value: {
    label: "MAKE UP FOR EVER"  value: "60"
  }
allowed_value: {
    label: "MARC BY MARC JACOBS"  value: "29"
  }
allowed_value: {
    label: "MARC JACOBS"  value: "28"
  }
allowed_value: {
    label: "MARINA RINALDI"  value: "1140001"
  }
allowed_value: {
    label: "MAX & CO"  value: "66"
  }
allowed_value: {
    label: "MAX FACTOR"  value: "1170501"
  }
allowed_value: {
    label: "MAX MARA"  value: "49"
  }
allowed_value: {
    label: "MAYBELLINE"  value: "71"
  }
allowed_value: {
    label: "MELVITA"  value: "1190001"
  }
allowed_value: {
    label: "MICHAEL KORS"  value: "34"
  }
allowed_value: {
    label: "MIKIMOTO"  value: "69"
  }
allowed_value: {
    label: "MOLTON BROWN"  value: "20"
  }
allowed_value: {
    label: "MULBERRY"  value: "48"
  }
allowed_value: {
    label: "NARS"  value: "1180001"
  }
allowed_value: {
    label: "NEEMAH"  value: "1270001"
  }
allowed_value: {
    label: "OFF WHITE"  value: "1510101"
  }
allowed_value: {
    label: "ON BOARD SALES"  value: "1120101"
  }
allowed_value: {
    label: "PARFUMS CHRISTIAN DIOR"  value: "1030001"
  }
allowed_value: {
    label: "PAUL & JOE"  value: "32"
  }
allowed_value: {
    label: "PAUL SMITH"  value: "24"
  }
allowed_value: {
    label: "PAULE KA"  value: "1250001"
  }
allowed_value: {
    label: "PENHALIGON'S"  value: "1240001"
  }
allowed_value: {
    label: "PERFUMES LOEWE"  value: "540002"
  }
allowed_value: {
    label: "PROENZA SCHOULER"  value: "67"
  }
allowed_value: {
    label: "PURIFICACION GARCIA"  value: "1060001"
  }
allowed_value: {
    label: "RALPH LAUREN"  value: "1050301"
  }
allowed_value: {
    label: "RAOUL"  value: "1220601"
  }
allowed_value: {
    label: "RENE CAOVILLA"  value: "1260001"
  }
allowed_value: {
    label: "REPLAY"  value: "61"
  }
allowed_value: {
    label: "RIMMEL"  value: "920001"
  }
allowed_value: {
    label: "ROGER & GALLET"  value: "1320001"
  }
allowed_value: {
    label: "SAKS EXTERNAL"  value: "1200702"
  }
allowed_value: {
    label: "SAKS FIFTH AVENUE"  value: "1200701"
  }
allowed_value: {
    label: "SALVATORE FERRAGAMO"  value: "1380001"
  }
allowed_value: {
    label: "SCARPE"  value: "22"
  }
allowed_value: {
    label: "SELECTIVE"  value: "10010004"
  }
allowed_value: {
    label: "SELECTIVE KIOSK"  value: "10010001"
  }
allowed_value: {
    label: "SEPHORA"  value: "72"
  }
allowed_value: {
    label: "SMASHBOX"  value: "1070001"
  }
allowed_value: {
    label: "SONIA RYKIEL"  value: "52"
  }
allowed_value: {
    label: "ST DUPONT"  value: "9"
  }
allowed_value: {
    label: "STELLA MC CARTNEY"  value: "1420001"
  }
allowed_value: {
    label: "SWAROVSKI"  value: "8"
  }
allowed_value: {
    label: "T DESIGN"  value: "58"
  }
allowed_value: {
    label: "TAG HEUER"  value: "1160301"
  }
allowed_value: {
    label: "TAGZ"  value: "23"
  }
allowed_value: {
    label: "TANAGRA"  value: "6"
  }
allowed_value: {
    label: "TARTINE & CHOCOLAT"  value: "53"
  }
allowed_value: {
    label: "THE VISITOR"  value: "620101"
  }
allowed_value: {
    label: "TORY BURCH"  value: "37"
  }
allowed_value: {
    label: "TRYANO"  value: "1100001"
  }
allowed_value: {
    label: "TRYANO - EXTERNAL"  value: "1100002"
  }
allowed_value: {
    label: "TUMI"  value: "10"
  }
allowed_value: {
    label: "URBAN DECAY"  value: "1130001"
  }
allowed_value: {
    label: "VERSACE"  value: "1500001"
  }
allowed_value: {
    label: "VERTU"  value: "1010301"
  }
allowed_value: {
    label: "VILEBREQUIN"  value: "35"
  }
allowed_value: {
    label: "YSL"  value: "1090002"
  }
allowed_value: {
    label: "ZADIG & VOLTAIRE"  value: "83"
  }
  default_value: "All"
}

# "Bahrain,Egypt,Kuwait,Morocco,Qatar,Saudi Arabia,United Arab Emirates"

  parameter: district_country {
    label: "2 - Retention Country Select"

    view_label: "Retention Rate"

    description: "If you want to restrict retention rate to a market. This must match the selected country from locations view."
    allowed_value: {
      label: "All"  value: "All"
    }
    allowed_value: {
      label: "Bahrain"  value: "Bahrain"
    }
    allowed_value: {
      label: "Egypt"  value: "'Egypt'"
    }
    allowed_value: {
      label: "Kuwait"  value: "'Kuwait'"
    }
    allowed_value: {
      label: "Morocco"  value: "'Morocco'"
    }
    allowed_value: {
      label: "Qatar"  value: "'Qatar'"
    }
    allowed_value: {
      label: "Saudi Arabia"  value: "'Saudi Arabia'"
    }
    allowed_value: {
      label: "United Arab Emirates"  value: "United Arab Emirates"
    }
    default_value: "All"
}


parameter: retention_period_offset {
hidden: yes
  description: "Use this to offset the retained from period by 1 year prior. e.g. to see customer in 2021 who are retained from 2019"
  allowed_value: {
    label: "No Offset"
    value: "0"
  }
  allowed_value: {
    label: "-1 year"
    value: "1"
  }
  default_value: "0"
}





#   dimension: retained_customer_redundant {
# hidden: yes
#     label: "Is Retained Customer"
#     group_label: "Retention Rate"
#     description: "Indicates if customer is retained from the previous period. Use Retention Period parameter in Customer view to select the period for retention."

#     type: yesno
#     sql: ${brand_customer_unique_id} in
#       (
#       select brand_customer_unique_id from `chb-prod-data-cust.prod_base_customers_final.base_customers_distinct`

#                           where  brand_customer_unique_id in (

#                             select brand_customer_unique_id
#                                   from
#                                   `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr`
#                                   where
#                                   atr_trandate >=
#                                   date_add(
#                                   (date_add(${base_customers_factretailsales.selected_start_date}, INTERVAL -1*(${retention_comparison_range}) DAY)), INTERVAL -1*(${retention_year_off}) YEAR)
#                                   and atr_trandate<
#                                   date_add(
#                                   ${base_customers_factretailsales.selected_start_date}, INTERVAL -1*(${retention_year_off}) YEAR)

#     )
#     );;


# }


  parameter: retention_range_start {
    label: "4 - Retention Start Date (custom)"
    view_label: "Retention Rate"


    type: date

}

  parameter: retention_range_end {
    label: "5 - Retention End Date (custom)"
    view_label: "Retention Rate"


    type: date

  }


dimension: ret_range_start {
  hidden: yes
  type: date
  sql: case when
  {% parameter dim_calendar.retention_period %} ="-1" then cast({% parameter retention_range_start %} as date)
  else date_add( ${dim_calendar.selected_start},INTERVAL -1*(${dim_calendar.retention_comparison_range}) DAY)
end;;
}

  dimension: ret_range_end {
hidden: yes
    type: date
    sql: case when
        {% parameter dim_calendar.retention_period %} ="-1" then cast({% parameter retention_range_end %} as date)
        else  ${dim_calendar.selected_start}
      end;;
  }



dimension: retained_customer  {
  description: "Did the customer shop in the selected previous period. Used in retention calculations"
  type: yesno
  sql:  ${brand_customer_unique_id} in (
    select brand_customer_unique_id
                                  from
                                  `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr`
                                  where
                                  atr_trandate >=${dim_calendar.ret_range_start}
                                  and atr_trandate<${dim_calendar.ret_range_end} );;
}



  dimension: retained_customer_universal  {
    description: "Did the customer shop in the selected previous period. Used in retention calculations"
    type: yesno
    sql:  ${brand_customer_unique_id} in (
           select brand_customer_unique_id
                                        from
                                        `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr`
                                        where
                                        atr_trandate >=${dim_calendar.ret_range_start}
                                        and atr_trandate<${dim_calendar.ret_range_end} );;
    }








}






view: base_customers_focus {
  view_label: "Customers"

  sql_table_name: `chb-prod-data-cust.prod_base_customers_final.base_customers_distinct`

    ;;

  dimension: brand_customer_unique_id {
    group_label: "IDs and Keys"
    type: string
    sql: ${TABLE}.brand_customer_unique_id ;;
  }

  dimension: is_generic_customer {
    group_label: "Customer Dimensions"
    type: yesno
    sql: ${TABLE}.is_generic_customer ;;
  }

  dimension: muse_enrolled_brand {
    group_label: "Customer Attributes"
    type: string
    sql: ${TABLE}.muse_enrolled_brand ;;
  }

  dimension: brand_id {
    hidden: yes
    type: number
    sql: ${TABLE}.brand_id ;;
  }

  dimension: brand_name {
    group_label: "Customer Attributes"
   hidden: yes
    type: string
    sql: ${TABLE}.brand_name ;;
  }

dimension_group: muse_registration

{
  type: time
  timeframes: [
    raw,
    date,
    week,
    month,
    quarter,
    year
  ]
  convert_tz: no
  datatype: date
  sql: ${TABLE}.muse_registration_date   ;;
}


  dimension_group: brand_registration {

    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.brand_registration_date ;;
  }

  dimension: email_validated {
    group_label: "Customer Attributes"
    hidden: yes
    type: string
    sql: ${TABLE}.email_validated ;;
  }

  dimension: first_name {
    hidden: yes
    group_label: "Customer Attributes"
    type: string
    sql: ${TABLE}.first_name ;;
  }

  dimension: gender {
    group_label: "Customer Attributes"
    type: string
    sql: ${TABLE}.gender ;;
  }

  dimension: Age {
    group_label: "Customer Attributes"
    type: number
    sql: date_diff(current_date(),${birth_date},year)
;;

}

    dimension: age_sort {
      hidden: yes
      type: number
      sql: CASE
                        WHEN ${Age} <= 15 THEN 1
                          WHEN ${Age} <= 18 THEN 2
                          WHEN ${Age} <= 21 THEN 3
                          WHEN ${Age} <= 24 THEN 4
                          WHEN ${Age} <= 30 THEN 5
                          WHEN ${Age} <= 30 THEN 6
                          WHEN ${Age} <= 36 THEN 7
                          WHEN ${Age} <= 45 THEN 8
                          WHEN ${Age} <= 54 THEN 9
                          WHEN ${Age} > 54  THEN 10
                          ELSE null
                          END;;
    }




  dimension: Age_Range {
    group_label: "Customer Attributes"
    order_by_field: age_sort
    hidden: no
    type: string
    sql: CASE
                          WHEN ${Age} <= 15 THEN "<15"
                          WHEN ${Age} <= 18 THEN "15-18"
                          WHEN ${Age} <= 21 THEN "18-21"
                          WHEN ${Age} <= 24 THEN "21-24"
                          WHEN ${Age} <= 30 THEN "24-30"
                          WHEN ${Age} <= 36 THEN "30-36"
                          WHEN ${Age} <= 45 THEN "36-45"
                          WHEN ${Age} <= 54 THEN "45-54"
                          WHEN ${Age} > 54  THEN ">54"
                          ELSE null
                          END;;
  }

  dimension: age_sort_CRM {
    hidden: yes
    type: number
    sql: CASE
                        WHEN ${Age} <= 18 THEN 1
                          WHEN ${Age} <= 20 THEN 2
                          WHEN ${Age} <= 29 THEN 3
                          WHEN ${Age} <= 39 THEN 4
                          WHEN ${Age} <= 49 THEN 5
                          WHEN ${Age} <= 59 THEN 6
                          WHEN ${Age} > 60  THEN 7
                          ELSE null
                          END;;
  }
  dimension: Age_Range_CRM {
    group_label: "Customer Attributes"
    label: "Age Range CRM"
    order_by_field: age_sort_CRM
    hidden: no
    type: string
    sql: CASE
                          WHEN ${Age} <= 18 THEN "<18"
                          WHEN ${Age} <= 20 THEN "18-20"
                          WHEN ${Age} <= 29 THEN "21-29"
                          WHEN ${Age} <= 39 THEN "30-39"
                          WHEN ${Age} <= 49 THEN "40-49"
                          WHEN ${Age} <= 59 THEN "50-59"
                          WHEN ${Age} > 60  THEN ">60"
                          ELSE null
                          END;;
  }



  dimension: customer_unique_id {
    group_label: "IDs and Keys"
    value_format: "0"
    type: number
    sql: ${TABLE}.customer_unique_id ;;
  }

  dimension: customer_unique_id_source {
    group_label: "IDs and Keys"
    type: string
    sql: ${TABLE}.customer_unique_id_source ;;
  }

  dimension: last_name {
    group_label: "Customer Attributes"
    hidden: yes
    type: string
    sql: ${TABLE}.last_name ;;
  }

  dimension: mobile_validated {
    hidden: yes
    group_label: "Customer Attributes"

    type: string
    sql: ${TABLE}.mobile_validated ;;
  }

  dimension: muse_member_id {
    group_label: "IDs and Keys"
    type: number
    sql: ${TABLE}.muse_member_id ;;
  }

  dimension: salesforce_account_id {
    group_label: "IDs and Keys"
    label: "Salesforce Account ID"
    hidden: yes
    type: string
    sql: ${TABLE}.salesforce_account_id ;;
  }



  dimension: Is_Muse_member {
    group_label: "Customer Dimensions"
    label: "Is Muse Member"
    type: yesno
    sql: ${muse_member_id} is not null  ;;
  }

  dimension: Is_CRM_member {
    group_label: "Customer Dimensions"
    label: "Is CRM Member"
    type: yesno
    sql: ${speedbus_id} is not null  ;;
  }





  dimension_group: birth {


    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.birthdate ;;
  }

  dimension: speedbus_id {
    group_label: "IDs and Keys"
    type: string
    sql: ${TABLE}.speedbus_id ;;
  }

  dimension: country_of_residence {
    group_label: "Customer Attributes"
    type: string
    sql: ${TABLE}.country_of_residence;;

  }

    dimension: nationality {
    group_label: "Customer Attributes"
    type: string
    sql: ${TABLE}.nationality;;

  }

    dimension: residency {

    group_label: "Customer Attributes"
    type: string
    sql: ${TABLE}.residency;;

  }

    dimension: ethnicity {
    group_label: "Customer Attributes"
    type: string
    sql: ${TABLE}.ethnicity;;

  }

  dimension: preferred_language {
    group_label: "Customer Attributes"
    type: string
    sql: ${TABLE}.preferred_language ;;
  }






dimension: contactability {
  type: string
  sql: case when NULLIF(${email_validated},'') is null and  NULLIF(${mobile_validated},'') is null then "Not Contactable"
  when NULLIF(${email_validated},'') is not null and  NULLIF(${mobile_validated},'') is null then "Email Only"
  when NULLIF(${email_validated},'') is null and  NULLIF(${mobile_validated},'') is not null then "Mobile Only"
  else "Email & Mobile" end ;;

}







            measure: group_transacted_customer_count {
              hidden: yes
              group_label: "Group Customer Measures"
              type: count_distinct
              description: "A person who buys goods across brands part of Chalhoub Group"
              sql: ${customer_unique_id} ;;
            }




measure: count_group_customers {

  group_label: "Group Registered Customer Measures"
  description: "Number of cross brand customer profiles. Can be filtered by other customer attributes."
  type: count_distinct
  sql: ${customer_unique_id} ;;
}










dimension: is_prospect {
  type: yesno
  sql: case when ${brand_customer_unique_id} not in
(
select distinct brand_customer_unique_id
from `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr`
where brand_customer_unique_id is not null ) then true else false end;;
}

  measure: brand_id_count {
    label: "Brand Registered Customer Count"
    description: "Count of brand level customers"
    type: count_distinct
    sql: ${brand_customer_unique_id} ;;
  }

}





view: base_customers_distinct_v2 {
  extends: [base_customers_distinct]



  dimension: retention_comparison_range {
    type: number
    hidden: yes
    sql: case when {% parameter dim_calendar.retention_period %} ="0" then
        DATE_DIFF(${base_customers_factretailsales_v2.selected_end_date}, ${base_customers_factretailsales_v2.selected_start_date}, DAY)
        else cast({% parameter dim_calendar.retention_period %} as int64) end;;
  }



  dimension: retained_customer {

    label: "Is Retained Customer"
    group_label: "Retention Rate"
    description: "Indicates if customer is retained from the previous period. Use Retention Period parameter in Customer view to select the period for retention."

    type: yesno
    sql: ${brand_customer_unique_id} in
      (
      select brand_customer_unique_id from `chb-prod-data-cust.prod_base_customers_final.base_customers_distinct`

                           where  brand_customer_unique_id in (

                            select brand_customer_unique_id
                                  from
                                  `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr`
                                  where
                                  atr_trandate >=

                                  (date_add(${base_customers_factretailsales_v2.selected_start_date}, INTERVAL -1*(${retention_comparison_range}) DAY))
                                  and atr_trandate<

                                  ${base_customers_factretailsales_v2.selected_start_date}

    )
    );;


    }


  }
