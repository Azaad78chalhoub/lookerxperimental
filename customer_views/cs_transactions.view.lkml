view: cs_transactions {
  view_label: "Customer Service"
  sql_table_name: `chb-prod-data-cust.prod_CS_ControlTower.prod_cs_transactions`;;

  dimension: id {
    primary_key: no
    hidden: yes
    type: string
    sql: ${TABLE}.id ;;
  }

  dimension: transaction_id {
    primary_key: yes
    hidden: yes
    type: string
    sql: CONCAT(${TABLE}.transaction_id, CAST(${TABLE}.customer_unique_id AS STRING)) ;;
  }

  dimension: case_number_id {
    primary_key: no
    hidden: yes
    type: string
    sql: CONCAT(${TABLE}.case_number, CAST(${TABLE}.customer_unique_id AS STRING)) ;;
  }

  dimension: customer_unique_id {
    primary_key: no
    hidden: no
    type: string
    sql: ${TABLE}.customer_unique_id ;;
  }


  dimension: bu_brand_name {
    hidden: yes
    type: string
    sql: ${TABLE}.bu_brand_name ;;
  }

  dimension: brand_name {
    hidden: no
    description: "Brands unified with Customer360 explore"
    type: string
    sql: UPPER(${TABLE}.brand_name) ;;
  }



  dimension: transaction_type {
    hidden: no
    type: string
    sql: ${TABLE}.transaction_type ;;
  }

  dimension: account_id {
    hidden: no
    group_label: "Customer Dimensions"
    type: string
    sql: ${TABLE}.account_id ;;
  }

  dimension: source {
    hidden: no
    type: string
    sql: ${TABLE}.source ;;
  }

  dimension: brand {
    hidden: no
    type: string
    sql: UPPER(${TABLE}.brand) ;;
  }


  dimension: case_number {
    hidden: no
    type: string
    sql: ${TABLE}.case_number ;;
  }

  dimension: category {
    hidden: no
    label: "Category"
    type: string
    sql: ${TABLE}.category ;;
  }

  dimension: channel {
    hidden: no
    type: string
    sql: ${TABLE}.channel ;;
  }

  dimension: classification {
    hidden: no
    type: string
    sql: ${TABLE}.classification ;;
  }

  dimension: subclassification {
    hidden: no
    label: "Subclassification"
    type: string
    sql: ${TABLE}.subclassification ;;
  }

  dimension: country {
    hidden: no
    type: string
    sql: ${TABLE}.country ;;
  }

  dimension: contact_email {
    hidden: no
    group_label: "Customer Dimensions"
    type: string
    sql: ${TABLE}.contact_email ;;
    link: {
      label: "See Customer transactions details"
      url: "https://chalhoubgroup.de.looker.com/looks/3969?&f[cs_transactions.contact_email]={{ value | url_encode }}"
    }
  }

  dimension_group: closed_date {
    hidden: no
    type: time
    label: "Closed"
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.closed_date ;;
  }

  dimension: contact_id {
    hidden: yes
    type: date
    sql: ${TABLE}.contact_id ;;
  }
  dimension: contact_mobile {
    hidden: no
    group_label: "Customer Dimensions"
    type: string
    sql: ${TABLE}.contact_mobile ;;
  }

  dimension: contact_phone {
    hidden: no
    group_label: "Customer Dimensions"
    type: string
    sql: ${TABLE}.contact_phone ;;
  }

  dimension_group: created_date {
    hidden: no
    label: "Created"
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.created_date ;;
  }

  dimension: is_closed {
    hidden: no
    type: yesno
    sql: ${TABLE}.is_closed ;;
  }

  dimension: case_age_hours {
    label: "Open Case Age"
    hidden: no
    type: number
    sql: ${TABLE}.case_age_hours ;;
  }

  dimension: origin {
    hidden: no
    type: string
    sql: ${TABLE}.origin ;;
  }

  dimension: owner_id {
    hidden: yes
    type: string
    sql: ${TABLE}.owner_id ;;
  }

  dimension: priority {
    hidden: no
    type: string
    sql: ${TABLE}.priority ;;
  }

  dimension: reason {
    hidden: no
    type: string
    sql: ${TABLE}.reason ;;
  }

  dimension: subject {
    hidden: no
    type: string
    sql: ${TABLE}.subject ;;
  }

  dimension: status {
    hidden: no
    type: string
    sql: ${TABLE}.status ;;
  }

  dimension: supplied_email {
    hidden: yes
    group_label: "Customer Dimensions"
    type: string
    sql: ${TABLE}.supplied_email ;;
  }
  dimension: supplied_name {
    hidden: yes
    group_label: "Customer Dimensions"
    type: string
    sql: ${TABLE}.supplied_name ;;
  }

  dimension: supplied_phone {
    hidden: yes
    group_label: "Customer Dimensions"
    type: string
    sql: ${TABLE}.supplied_phone ;;
  }
  dimension: support_category {
    hidden: no
    type: string
    sql: ${TABLE}.support_category ;;
  }

  dimension: type {
    hidden: no
    type: string
    sql: ${TABLE}.type ;;
  }

  dimension: call_duration_in_seconds {
    hidden: yes
    type: number
    sql: ${TABLE}.call_duration_in_seconds ;;
  }
  dimension: call_disposition {
    hidden: yes
    type: string
    sql: ${TABLE}.call_disposition ;;
  }
  dimension: survey_name {
    hidden: no
    type: string
    sql: ${TABLE}.survey_name ;;
  }
  dimension: csat_score {
    label: "CSAT score"
    hidden: no
    type: number
    sql: ${TABLE}.csat_score ;;
  }

  dimension: csat_score_pct {
    label: "CSAT score (percentage)"
    description: "CSAT score for CS ranges between 1 and 5, hence value divided by 5"
    value_format: "0%"
    type: number
    sql: ${TABLE}.csat_score/5 ;;
  }


  dimension: profile_id {
    hidden: yes
    type: string
    sql: ${TABLE}.profile_id ;;
  }
  dimension: user_name {
    hidden: no
    group_label: "Agent Dimensions"
    type: string
    sql: ${TABLE}.user_name ;;
  }

  dimension: user_email {
    hidden: no
    group_label: "Agent Dimensions"
    type: string
    sql: ${TABLE}.user_email ;;
  }
  dimension: community_nickname {
    hidden: yes
    type: string
    sql: ${TABLE}.community_nickname ;;
  }

  dimension: first_name {
    hidden: no
    group_label: "Customer Dimensions"
    type: string
    sql: ${TABLE}.first_name ;;
  }

  dimension: last_name {
    hidden: no
    group_label: "Customer Dimensions"
    type: string
    sql: ${TABLE}.last_name ;;
  }

  dimension: email {
    hidden: no
    group_label: "Customer Dimensions"
    type: string
    sql: ${TABLE}.email ;;
  }

  dimension: closed_reason {
    hidden: no
    type: string
    sql: ${TABLE}.closed_reason ;;
  }

  dimension: last_modified_by_id {
    hidden: no
    type: string
    sql: ${TABLE}.last_modified_by_id ;;
  }

  dimension: last_modified_date {
    hidden: no
    type: date
    sql: ${TABLE}.last_modified_date ;;
  }


  dimension: created_datetime {
    hidden: no
    type: date_time
    sql: ${TABLE}.created_datetime ;;
  }

  dimension: first_updated_datetime {
    hidden: no
    type: date_time
    sql: ${TABLE}.case_first_update_datetime;;
  }

  dimension: first_response_time {
    hidden: no
    label: "Case First Response Time (hours)"
    description: "Based on first status change as thats only thing we have"
    type: number
    sql: DATETIME_DIFF(CAST(${TABLE}.case_first_update_datetime AS DATETIME),
    CAST(${TABLE}.created_datetime AS DATETIME), HOUR);;
  }

  dimension: case_closed_on_the_same_day {
    hidden: no
    type: string
    sql: CASE WHEN ${TABLE}.case_closed_on_the_same_day IS NOT NULL THEN 'Yes' ELSE 'No' END;;
  }

  measure: count_cases_closed_on_the_same_day {
    hidden: no
    type: count_distinct
    filters: [transaction_type: "CASE"]
    sql: ${TABLE}.case_closed_on_the_same_day;;
  }

  measure: first_time_resolution_sf {
    label: "First Time Resolution (SF formula)"
    type: number
    value_format: "0.0%"
    sql: ${count_cases_closed_on_the_same_day}/${case_count} ;;

  }


  measure: avg_first_response_time {
    hidden: no
    label: "Average First Response Time (hours)"
    description: "Based on first status change as thats only thing we have"
    type: average
    value_format: "0.0"
    filters: [transaction_type: "CASE"]
    sql: ${first_response_time};;
  }


  measure: count_transactions {
    hidden: no
    type: count_distinct
    sql: ${transaction_id} ;;
  }

  measure: count_customers {
    hidden: no
    type: count_distinct
    sql: ${account_id} ;;
  }

  measure: count_case_customers {
    hidden: yes
    type: count_distinct
    filters: [transaction_type: "CASE"]
    sql: ${account_id} ;;
  }

  measure: count_agents {
    hidden: no
    type: count_distinct
    sql: ${user_name} ;;
  }

  measure: avg_csat {
    label: "Average CSAT"
    hidden: no
    value_format: "0.00"
    filters: [transaction_type: "CASE"]
    type: average
    sql: ${csat_score} ;;
  }


  measure: avg_csat_pct {
    label: "Average CSAT (percentage)"
    hidden: no
    value_format: "0%"
    filters: [transaction_type: "CASE"]
    type: average
    sql: ${csat_score_pct} ;;
  }


  measure: case_count {
    hidden: no
    label: "Count Cases"
    type: count_distinct
    filters: [transaction_type: "CASE"]
    sql: ${transaction_id} ;;
  }

  measure: avg_case_age_hours {
    hidden: no
    label: "Speed of Response"
    type: average_distinct
    value_format: "0"
    sql_distinct_key: ${transaction_id} ;;
    filters: [transaction_type: "CASE"]
    sql:${TABLE}.case_age_hours;;
  }

  measure: avg_case_age_hours_email_web {
    hidden: no
    label: "Speed of Response (Email, Web Only)"
    type: average_distinct
    value_format: "0.00"
    sql_distinct_key: ${transaction_id} ;;
    filters: [transaction_type: "CASE"]
    sql: case when ${origin}='Web' or ${origin} = 'Email'
    then ${TABLE}.case_age_hours  else null end;;
  }

  measure: avg_call_age_hours {
    hidden: yes
    type: average_distinct
    value_format: "0"
    sql_distinct_key: ${transaction_id} ;;
    filters: [transaction_type: "CALL"]
    sql: ${TABLE}.case_age_hours ;;
  }




  # how many cases were resolved by an agent
  # the first time a customer contacted them vs the total number of cases
  # FTR (First time resolution ) = 1- ( total number of cases/Total unique customers)?


  measure: ftr {
    label: "First Time Resolution (Calculation)"
    type: number
    value_format: "0\%"
    sql: IFNULL((${count_case_customers}/NULLIF(${case_count},0)*100),0) ;;
  }

  # created_date
  # FRT ( First response time) = Case time create - Case time 1st update
  # measure: first_response_time {
  #   label: "First Response Time"
  #   type: number
  #   value_format: "0"
  #   sql: ${created_date_raw} -  ;;
  #}


  measure: calls_handled {
    label: "Calls handled"
    type:  sum_distinct
    sql_distinct_key: CONCAT(${TABLE}.brand_name,  ${TABLE}.call_date) ;;
    group_label: "Cisco Brand Measures"
    sql: ${TABLE}.calls_handled ;;

  }

  measure: calls_abandoned {
    label: "Calls abandoned"
    type:  sum_distinct
    sql_distinct_key: CONCAT(${TABLE}.brand_name,  ${TABLE}.call_date) ;;
    group_label: "Cisco Brand Measures"
    sql: ${TABLE}.calls_abandoned ;;

  }

  measure: calls_handled_by_other {
    label: "Calls handled by other"
    type:  sum_distinct
    sql_distinct_key: CONCAT(${TABLE}.brand_name,  ${TABLE}.call_date) ;;
    group_label: "Cisco Brand Measures"
    sql: ${TABLE}.calls_handled_by_other ;;

  }

  measure: calls_presented {
    label: "Calls presented"
    type:  sum_distinct
    sql_distinct_key: CONCAT(${TABLE}.brand_name,  ${TABLE}.call_date) ;;
    group_label: "Cisco Brand Measures"
    sql: ${TABLE}.calls_presented ;;

  }

  measure: avg_queue_time {
    label: "Average Queue Time"
    type:  average_distinct
    value_format: "0"
    sql_distinct_key: CONCAT(${TABLE}.brand_name,  ${TABLE}.call_date) ;;
    group_label: "Cisco Brand Measures"
    sql: ${TABLE}.avg_queue_time ;;

  }

  measure: abandon_call_rate {
    label: "Abandon Call Rate"
    type:  number
    value_format: "0%"
    group_label: "Cisco Brand Measures"
    sql: ${calls_abandoned}/NULLIF(${calls_presented},0) ;;

  }


  # measure: grade_of_service {
  #   label: "Grade of service"
  #   type:  number
  #   sql_distinct_key: CONCAT(${TABLE}.brand_name,  ${TABLE}.call_date) ;;
  #   group_label: "Cisco Brand Measures"
  #   sql: CASE WHEN ${avg_queue_time} >  ;;

  # }





}



view: cs_base_customers {
  view_label: "Customer Service"
  sql_table_name: `chb-prod-data-cust.prod_CS_ControlTower.cs_base_customers`;;

  dimension: id {
    primary_key: no
    hidden: yes
    type: string
    sql: ${TABLE}.id ;;
  }


  dimension: transaction_id {
    primary_key: yes
    hidden: yes
    type: string
    sql: ${TABLE}.transaction_id ;;
  }


  dimension: customer_unique_id {
    primary_key: no
    hidden: yes
    type: number
    sql: ${TABLE}.customer_unique_id ;;
  }

  dimension: brand_id {
    primary_key: no
    hidden: yes
    type: number
    sql: ${TABLE}.brand_id ;;
  }

  dimension: brand_customer_unique_id {
    primary_key: no
    hidden: yes
    type: string
    sql: ${TABLE}.brand_customer_unique_id ;;
  }

  dimension: transaction_type {
    hidden: no
    type: string
    sql: ${TABLE}.transaction_type ;;
  }

  dimension: account_id {
    hidden: yes
    group_label: "Customer Dimensions"
    type: string
    sql: ${TABLE}.account_id ;;
  }

  dimension: source {
    hidden: no
    type: string
    sql: ${TABLE}.source ;;
  }

  dimension: brand {
    hidden: yes
    type: string
    sql: ${TABLE}.brand ;;
  }


  dimension: case_number {
    hidden: no
    type: string
    sql: ${TABLE}.case_number ;;
  }

  dimension: category {
    hidden: no
    label: "Category"
    type: string
    sql: ${TABLE}.category ;;
  }

  dimension: channel {
    hidden: no
    type: string
    sql: ${TABLE}.channel ;;
  }

  dimension: classification {
    hidden: no
    type: string
    sql: ${TABLE}.classification ;;
  }

  dimension: country {
    hidden: no
    type: string
    sql: ${TABLE}.country ;;
  }

  # dimension: contact_email {
  #   hidden: no
  #   group_label: "Customer Dimensions"
  #   type: string
  #   sql: ${TABLE}.contact_email ;;
  # }

  dimension_group: closed_date {
    hidden: no
    type: time
    label: "Closed"
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.closed_date ;;
  }

  dimension: contact_id {
    hidden: yes
    type: date
    sql: ${TABLE}.contact_id ;;
  }
  # dimension: contact_mobile {
  #   hidden: no
  #   group_label: "Customer Dimensions"
  #   type: string
  #   sql: ${TABLE}.contact_mobile ;;
  # }

  # dimension: contact_phone {
  #   hidden: no
  #   group_label: "Customer Dimensions"
  #   type: string
  #   sql: ${TABLE}.contact_phone ;;
  # }

  dimension_group: created_date {
    hidden: no
    label: "Created"
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.created_date ;;
  }

  dimension: is_closed {
    hidden: no
    type: yesno
    sql: ${TABLE}.is_closed ;;
  }

  dimension: case_age_hours {
    label: "Open Case Age (Hours)"
    hidden: no
    type: number
    sql: ${TABLE}.case_age_hours ;;
  }

  dimension: origin {
    hidden: no
    type: string
    sql: ${TABLE}.origin ;;
  }

  dimension: owner_id {
    hidden: yes
    type: string
    sql: ${TABLE}.owner_id ;;
  }

  dimension: priority {
    hidden: no
    type: string
    sql: ${TABLE}.priority ;;
  }

  dimension: reason {
    hidden: no
    type: string
    sql: ${TABLE}.reason ;;
  }

  dimension: subject {
    hidden: no
    type: string
    sql: ${TABLE}.subject ;;
  }

  dimension: status {
    hidden: no
    type: string
    sql: ${TABLE}.status ;;
  }

  # dimension: supplied_email {
  #   hidden: yes
  #   group_label: "Customer Dimensions"
  #   type: string
  #   sql: ${TABLE}.supplied_email ;;
  # }
  # dimension: supplied_name {
  #   hidden: yes
  #   group_label: "Customer Dimensions"
  #   type: string
  #   sql: ${TABLE}.supplied_name ;;
  # }

  # dimension: supplied_phone {
  #   hidden: yes
  #   group_label: "Customer Dimensions"
  #   type: string
  #   sql: ${TABLE}.supplied_phone ;;
  # }
  dimension: support_category {
    hidden: no
    type: string
    sql: ${TABLE}.support_category ;;
  }

  dimension: type {
    hidden: no
    type: string
    sql: ${TABLE}.type ;;
  }

  dimension: call_duration_in_seconds {
    hidden: yes
    type: number
    sql: ${TABLE}.call_duration_in_seconds ;;
  }
  dimension: call_disposition {
    hidden: yes
    type: string
    sql: ${TABLE}.call_disposition ;;
  }

  dimension: csat_score {
    label: "CSAT score"
    hidden: yes
    type: number
    sql: ${TABLE}.csat_score ;;
  }




  dimension: profile_id {
    hidden: yes
    type: string
    sql: ${TABLE}.profile_id ;;
  }
  dimension: user_name {
    hidden: no
    group_label: "Agent Dimensions"
    type: string
    sql: ${TABLE}.user_name ;;
  }

  dimension: user_email {
    hidden: no
    group_label: "Agent Dimensions"
    type: string
    sql: ${TABLE}.user_email ;;
  }
  dimension: community_nickname {
    hidden: yes
    type: string
    sql: ${TABLE}.community_nickname ;;
  }

  # dimension: first_name {
  #   hidden: no
  #   group_label: "Customer Dimensions"
  #   type: string
  #   sql: ${TABLE}.first_name ;;
  # }

  # dimension: last_name {
  #   hidden: no
  #   group_label: "Customer Dimensions"
  #   type: string
  #   sql: ${TABLE}.last_name ;;
  # }

  # dimension: email {
  #   hidden: no
  #   group_label: "Customer Dimensions"
  #   type: string
  #   sql: ${TABLE}.email ;;
  # }

  dimension: closed_reason {
    hidden: no
    type: string
    sql: ${TABLE}.closed_reason ;;
  }

  dimension: last_modified_by_id {
    hidden: no
    type: string
    sql: ${TABLE}.last_modified_by_id ;;
  }

  dimension: last_modified_date {
    hidden: no
    type: date
    sql: ${TABLE}.last_modified_date ;;
  }


  measure: count_transactions {
    hidden: no
    type: count_distinct
    sql: ${transaction_id} ;;
  }

  measure: count_customers {
    hidden: no
    type: count_distinct
    sql: ${account_id} ;;
  }

  measure: count_case_customers {
    hidden: yes
    type: count_distinct
    filters: [transaction_type: "CASE"]
    sql: ${account_id} ;;
  }

  measure: count_agents {
    hidden: no
    type: count_distinct
    sql: ${user_name} ;;
  }

  measure: avg_csat {
    label: "Average CSAT"
    hidden: no
    value_format: "0.0"
    type: average
    sql: ${csat_score} ;;
  }


  measure: case_count {
    hidden: no
    label: "Count Cases"
    type: count_distinct
    filters: [transaction_type: "CASE"]
    sql: ${transaction_id} ;;
  }

  measure: avg_case_age_hours {
    hidden: no
    label: "Speed of Response"
    description: "Average of case age in hours"
    type: average_distinct
    value_format: "0"
    sql_distinct_key: ${transaction_id} ;;
    filters: [transaction_type: "CASE"]
    sql:${TABLE}.case_age_hours;;
  }

  measure: avg_case_age_hours_email_web {
    hidden: no
    label: "Speed of Response (Email, Web Only)"
    description: "Average of case age in hours"
    type: average_distinct
    value_format: "0.00"
    sql_distinct_key: ${transaction_id} ;;
    filters: [transaction_type: "CASE"]
    sql: case when ${origin}='Web' or ${origin} = 'Email'
      then ${TABLE}.case_age_hours  else null end;;
  }

  measure: avg_call_age_hours {
    hidden: yes
    type: average_distinct
    value_format: "0"
    sql_distinct_key: ${transaction_id} ;;
    filters: [transaction_type: "CALL"]
    sql: ${TABLE}.case_age_hours ;;
  }

  measure: ftr {
    label: "First Time Resolution (Calculation)"
    type: number
    value_format: "0\%"
    sql: IFNULL((${count_case_customers}/NULLIF(${case_count},0)*100),0) ;;
  }


  # measure: calls_handled {
  #   label: "Calls handled"
  #   type:  sum_distinct
  #   sql_distinct_key: CONCAT(${TABLE}.brand_name,  ${TABLE}.call_date) ;;
  #   group_label: "Cisco Brand Measures"
  #   sql: ${TABLE}.calls_handled ;;

  # }

  # measure: calls_abandoned {
  #   label: "Calls abandoned"
  #   type:  sum_distinct
  #   sql_distinct_key: CONCAT(${TABLE}.brand_name,  ${TABLE}.call_date) ;;
  #   group_label: "Cisco Brand Measures"
  #   sql: ${TABLE}.calls_abandoned ;;

  # }

  # measure: calls_handled_by_other {
  #   label: "Calls handled by other"
  #   type:  sum_distinct
  #   sql_distinct_key: CONCAT(${TABLE}.brand_name,  ${TABLE}.call_date) ;;
  #   group_label: "Cisco Brand Measures"
  #   sql: ${TABLE}.calls_handled_by_other ;;

  # }

  # measure: calls_presented {
  #   label: "Calls presented"
  #   type:  sum_distinct
  #   sql_distinct_key: CONCAT(${TABLE}.brand_name,  ${TABLE}.call_date) ;;
  #   group_label: "Cisco Brand Measures"
  #   sql: ${TABLE}.calls_presented ;;

  # }

  # measure: avg_queue_time {
  #   label: "Average Queue Time"
  #   type:  average_distinct
  #   value_format: "0"
  #   sql_distinct_key: CONCAT(${TABLE}.brand_name,  ${TABLE}.call_date) ;;
  #   group_label: "Cisco Brand Measures"
  #   sql: ${TABLE}.avg_queue_time ;;

  # }

  # measure: abandon_call_rate {
  #   label: "Abandon Call Rate"
  #   type:  number
  #   value_format: "0%"
  #   group_label: "Cisco Brand Measures"
  #   sql: ${calls_abandoned}/NULLIF(${calls_presented},0) ;;

  # }


}
