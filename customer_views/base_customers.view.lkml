view: base_customers {
  label: "Customers"
  sql_table_name: `chb-prod-data-cust.prod_base_customers_final.base_customers_distinct`
    ;;

  dimension: brand_customer_unique_id {
    group_label: "IDs and Keys"
    value_format: "0"
    type: string
    sql: ${TABLE}.brand_customer_unique_id ;;

  }


  dimension: is_generic_customer {
    type: yesno
    sql: ${TABLE}.is_generic_customer ;;
  }


  dimension: brand_id {
    group_label: "IDs and Keys"
    type: number
    hidden: yes
    sql: ${TABLE}.brand_id ;;
  }

  dimension: brand_name {
    label: "CRM Originating Brand"
    type: string
    sql: ${TABLE}.brand_name ;;
  }

  dimension_group: brand_registration {
    label: "CRM Registration"
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.brand_registration_date ;;
  }

  dimension_group: muse_registration

  {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.muse_registration_date   ;;
  }

  dimension: email_validated {
    group_label: "Personal Information"
    hidden: yes
    type: string
    sql: ${TABLE}.email_validated ;;
  }

  dimension: first_name {
    group_label: "Personal Information"
    hidden: yes
    type: string
    sql: ${TABLE}.first_name ;;
  }

  dimension: gender {
    group_label: "Personal Information"
    type: string
    sql: ${TABLE}.gender ;;
  }

  dimension: preferred_language {
    group_label: "Customer Attributes"
    hidden: yes
    type: string
    sql: ${TABLE}.preferred_language ;;
  }


  dimension_group: birth {

    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.birthdate ;;
  }
  dimension: customer_unique_id {
    group_label: "IDs and Keys"
    value_format: "0"
    type: number
    sql: ${TABLE}.customer_unique_id ;;
  }

  dimension: customer_unique_id_source {
    group_label: "IDs and Keys"
    type: string
    sql: ${TABLE}.customer_unique_id_source ;;
  }

  dimension: last_name {
    group_label: "Personal Information"
    hidden: yes
    type: string
    sql: ${TABLE}.last_name ;;
  }

  dimension: mobile_validated {
    group_label: "Personal Information"
    hidden: yes
    type: string
    sql: ${TABLE}.mobile_validated ;;
  }

  dimension: muse_member_id {
    group_label: "IDs and Keys"
    type: number
    sql: ${TABLE}.muse_member_id ;;
  }

  dimension: Is_Muse_member {
    label: "Is Muse Member"
    type: yesno
    sql: ${muse_member_id} is not null  ;;
  }

  dimension: Is_CRM_member {
    label: "Is CRM Member"
    type: yesno
    sql: ${speedbus_id} is not null  ;;
  }

  dimension_group: registration {

    hidden: yes
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.registration_date ;;
  }

  dimension: speedbus_id {
    group_label: "IDs and Keys"
    label: "CRM ID"
    type: string
    sql: ${TABLE}.speedbus_id ;;
  }

  dimension_group: first_txn {
    label: "Customer First txn"
    group_label: "Tenure Dimensions"
    type: time
    timeframes: [
      raw,
      date,

    ]
    sql: (select cast(min(atr_trandate) as timestamp) from
      `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr` where brand_customer_unique_id=${brand_customer_unique_id}) ;;
  }


  dimension_group: last_txn {
    label: "Customer Last txn"
    group_label: "Tenure Dimensions"
    type: time
    timeframes: [
      raw,
      date,

    ]
    sql: (select cast(max(atr_trandate) as timestamp) from
      `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr` where brand_customer_unique_id=${brand_customer_unique_id}) ;;
  }




  dimension: first_date {
    hidden: yes
    type: date
    sql: (select cast(min(atr_trandate) as timestamp) from
      `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr` where customer_unique_id=${customer_unique_id}) ;;
  }



  dimension: last_date {
    hidden: yes
    type: date
    sql: (select cast(max(atr_trandate) as timestamp) from
      `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr` where brand_customer_unique_id=${brand_customer_unique_id}) ;;
  }

  measure: frs_startdate {
    hidden: yes
    type: date
    sql: min(${factretailsales.atr_trandate_raw}) ;;
  }

  measure: frs_enddate {
    hidden: yes
    type: date
    sql: max(${factretailsales.atr_trandate_raw}) ;;
  }

  dimension: recency {
    group_label: "Tenure Dimensions"
    type: number

    sql: date_diff(CURRENT_DATE(), ${last_date} , DAY) ;;
  }

dimension: muse_enrolled_brand {
  type: string
  sql: ${TABLE}.muse_enrolled_brand ;;
}



  dimension: tenure {
    group_label: "Tenure Dimensions"
    type: number
    sql: case when date_diff(${last_date},${first_date},day) = 0 then null else date_diff(${last_date},${first_date},day) end;;
  }


  dimension: customer_lifetime_spend {
    group_label: "Tenure Dimensions"
    label: "Customer Lifetime Spend"
    description: "Total lifetime spend, not filtered by data. Can be used to segment."
    type: number
    value_format: "$#,##0"
    sql: (select sum(mea_amountusd) from `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr`
      where brand_customer_unique_id=${brand_customer_unique_id});;
  }




















measure: brands_shopped_in_period{
  type: string
  sql: (select cast(count(distinct min_brand_id) as string)
  from  `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr`
  where customer_unique_id=${customer_unique_id}
  and atr_trandate=>${frs_startdate}
  and atr_trandate<${frs_enddate});;
}

# measure: ave_brands_shopped {
#   type: average
#   sql: ${brands_shopped} ;;
# }






#-----------------------Muse sample sets-------------------



  parameter: muse_start_date {
    type: date_time
    default_value: "2019-08-25"
    description: "Use this field to select the Muse launch date.  Pre/Post Muse Launch sample sets will reflect this. Defaults to 25-Aug-2019 if not selected."
  }

  dimension: casted_start_date {
    label: "Selected Muse Start Date"
    description: "for validation of Muse start/end date filters"
    type: date
    sql: cast({% parameter muse_start_date %} as date) ;;
  }

  parameter: muse_end_date {
    type: date_time
    default_value: "2020-08-26"
    description: "Use this field to select the Muse launch date.  Pre/Post Muse Launch sample sets will reflect this. Defaults to 26-Aug-2020 is not selected."
  }

  dimension: casted_end_date {
    label: "Selected Muse End Date"
    description: "for validation of Muse start/end date filters"
    type: date
    sql: cast({% parameter muse_end_date %} as date) ;;
  }


dimension: days_diff {
  type: number
  sql: DATE_DIFF (${casted_start_date}, ${casted_end_date}, DAY) ;;
}


  dimension: muse_traffic_in {
    type: yesno
    group_label: "Sample Sets"
    label: "Is MUSE Traffic In"
    description: "Identifies a customer as Traffic In to Brand A, if shopped at Brand A but enrolled in another brand"
    sql: (${muse_enrolled_brand} != ${locations.district_name})
    -- OR (${muse_enrolled_brand} IS NULL)

     ;;
  }


  dimension: muse_retained_members{
    group_label: "Sample Sets"
    description: "Muse members who have shopped pre and post muse launch (default 25-Aug-19). This sample set is dynamic and will grow over time."
    type: yesno
    sql: ${brand_customer_unique_id} in (

            select distinct brand_customer_unique_id

            from
            `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr`
          where atr_trandate>date_add( ${casted_start_date},INTERVAL ( select DATE_DIFF(${casted_start_date},CURRENT_DATE(),DAY)) DAY)
            and atr_trandate <${casted_start_date}
            and brand_customer_unique_id
            in (
            select brand_customer_unique_id
            from
            `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr`
            where atr_trandate<CURRENT_DATE()
            and atr_trandate >${casted_start_date}
            and atr_muse_id is not null
            and brand_customer_unique_id in
            (select brand_customer_unique_id from `chb-prod-data-cust.prod_base_customers_final.base_customers_distinct`
            where muse_member_id is not null)
            ))


             and muse_member_id not in(
                    select muse_member_id from (

                    SELECT count(distinct customer_unique_id), muse_member_id FROM `chb-prod-data-cust.prod_base_customers_final.base_customers_distinct`
                    where muse_member_id is not null
                    group by muse_member_id
                    having count(distinct customer_unique_id)>1
                    ))


        and muse_member_id not in
            (720101715939,  720101726712,720101847823, 720101889064,720101670910,720101679747,720101669284)

;;

    }

  dimension: muse_reactivated_members{
    group_label: "Sample Sets"
    description: "Muse members who have shopped more than a year before launch date and then not again until after Muse launch."
    type: yesno
    sql: ${brand_customer_unique_id} in (


                  select brand_customer_unique_id
                  from
                  `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr`
                  where atr_trandate<CURRENT_DATE()
                  and atr_trandate >${casted_start_date}
                  and atr_muse_id is not null
                  and brand_customer_unique_id in
                  (select brand_customer_unique_id from `chb-prod-data-cust.prod_base_customers_final.base_customers_distinct`
                  where muse_member_id is not null)


                       and brand_customer_unique_id not in

              ( select distinct brand_customer_unique_id

                from
                `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr`
                where atr_trandate<=${casted_start_date}
                and atr_trandate>=date_add(${casted_start_date},-365,day)

                and brand_customer_unique_id in

              ( select distinct brand_customer_unique_id

                from
                `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr`
                where           atr_trandate<=date_add(${casted_start_date},-365,day)



                )
                                        and muse_member_id not in(
                  select muse_member_id from (

                  SELECT count(distinct customer_unique_id), muse_member_id FROM `chb-prod-data-cust.prod_base_customers_final.base_customers_distinct`
                  where muse_member_id is not null
                  group by muse_member_id
                  having count(distinct customer_unique_id)>1
                  )
                  )
                     and muse_member_id not in
            (720101715939,  720101726712,720101847823, 720101889064,720101670910,720101679747,720101669284)
                  ;;

    }

    dimension: muse_retained_members_static{
      group_label: "Sample Sets"
      description: "Muse members who have shopped 12 months pre and 12 month post muse launch. The sample set is static."
      type: yesno
      sql: ${brand_customer_unique_id} in (

                     select distinct brand_customer_unique_id

                from
                `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr`
                where atr_trandate>=DATE_ADD(
                ${casted_start_date}, INTERVAL ${days_diff} DAY)
                and atr_trandate <${casted_start_date}

                and brand_customer_unique_id
                in (
                select brand_customer_unique_id
                from
                `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr`
                where  atr_trandate >=${casted_start_date}
                and atr_trandate <${casted_end_date}
                and atr_muse_id is not null

                )
                and brand_customer_unique_id in
              (select brand_customer_unique_id from `chb-prod-data-cust.prod_base_customers_final.base_customers_distinct`
              where muse_member_id is not null))
                                      and muse_member_id not in(
select muse_member_id from (

SELECT count(distinct customer_unique_id), muse_member_id FROM `chb-prod-data-cust.prod_base_customers_final.base_customers_distinct`
where muse_member_id is not null
group by muse_member_id
having count(distinct customer_unique_id)>1
)
)
   and muse_member_id not in
            (720101715939,  720101726712,720101847823, 720101889064,720101670910,720101679747,720101669284)

              ;;

      }


      dimension: non_muse_retained_members_static{
        group_label: "Sample Sets"
        description: "Non-Muse members (CRM Only) who have shopped 12 months pre and 12 month post muse launch. The sample set is static."
        type: yesno
        sql: ${brand_customer_unique_id} in (

                              select distinct brand_customer_unique_id

                    from
                          `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr`
                                where atr_trandate>=date_add(${casted_start_date},INTERVAL DATE_DIFF(${casted_end_date}, ${casted_start_date}, day) DAY)   and atr_trandate <${casted_start_date}
                                and brand_customer_unique_id
                                in (
                                select brand_customer_unique_id
                                from
                                `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr`
                                where atr_trandate<CURRENT_DATE()
                                and atr_trandate >=${casted_start_date}
                                and atr_trandate <${casted_end_date}
                                )
                                and brand_customer_unique_id  in (
                                select brand_customer_unique_id from
                                `chb-prod-data-cust.prod_base_customers_final.base_customers_distinct`
                                where muse_member_id is null
                                 and  lower(first_name) not in ('national', '"valued','unknown', 'valued'))
                               );;


        }


  dimension: brands_shopped {
    description: "Brands shopped at"
    type: number
    value_format: "#.##"
    sql: (select count(distinct bk_brand)
            from `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr`
           where customer_unique_id in (select distinct customer_unique_id
                                    from `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr`
                                  where brand_customer_unique_id = ${brand_customer_unique_id}))

                                  ;;}

    dimension: non_muse_retained_members{
          group_label: "Sample Sets"
          description: "Non-Muse members who have shopped pre and post muse launch. This sample set is dynamic and will grow over time."
          type: yesno
          sql: ${brand_customer_unique_id} in (select distinct brand_customer_unique_id

                        from
                        `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr`
                        where

                        atr_trandate>date_add( ${casted_start_date},INTERVAL ( select DATE_DIFF(${casted_start_date},CURRENT_DATE(),DAY)) DAY)
                       and
                        atr_trandate <${casted_start_date}
                        and brand_customer_unique_id
                        in (
                        select brand_customer_unique_id
                        from
                        `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr`
                        where atr_trandate<CURRENT_DATE()
                        and atr_trandate >${casted_start_date}'

                        )
                        and brand_customer_unique_id  in (
                        select brand_customer_unique_id from
                        `chb-prod-data-cust.prod_base_customers_final.base_customers_distinct`
                        where muse_member_id is null
                        and  lower(first_name) not in ('national', '"valued','unknown', 'valued'))
             ) ;;

          }



  dimension: non_muse_members{
    group_label: "Sample Sets"
    description: "Non-Muse members who have shopped post muse launch. This sample set is dynamic and will grow over time."
    type: yesno
    sql: ${brand_customer_unique_id} in (select distinct brand_customer_unique_id

                              from
                              `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr`
                              where

                              atr_trandate>date_add( ${casted_start_date},INTERVAL ( select DATE_DIFF(${casted_start_date},CURRENT_DATE(),DAY)) DAY)
                             --and
                             -- atr_trandate <'2019-08-25'
                              and brand_customer_unique_id
                              in (
                              select brand_customer_unique_id
                              from
                              `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr`
                              where atr_trandate<CURRENT_DATE()
                              and atr_trandate >${casted_start_date}

                              )
                              and brand_customer_unique_id  in (
                              select brand_customer_unique_id from
                              `chb-prod-data-cust.prod_base_customers_final.base_customers_distinct`
                              where muse_member_id is null
                              and  lower(first_name) not in ('national', '"valued','unknown', 'valued'))
                   ) ;;

    }




  dimension: new_muse_exclusive {
    label: "New Muse Only Customers"
    group_label: "Sample Sets"
    description: "New Muse members who have no CRM record"
    type: yesno
    sql: ${brand_customer_unique_id} in
            (select brand_customer_unique_id from `chb-prod-data-cust.prod_base_customers_final.base_customers_distinct` where muse_member_id is not null
            and speedbus_id is null
            and brand_customer_unique_id in (


                  select brand_customer_unique_id
                  from
                  `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr`
                  where
                  atr_trandate >=${casted_start_date}
                  and crm_customer_id is null
                  )
                                and brand_customer_unique_id
                  not in (
                   select brand_customer_unique_id  from

                   `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr`
                    where
                  atr_trandate <${casted_start_date}
                  and brand_customer_unique_id  is not null
                   )
                  and muse_member_id not in(
                   select muse_member_id from (

                   SELECT count(distinct customer_unique_id), muse_member_id FROM `chb-prod-data-cust.prod_base_customers_final.base_customers_distinct`
                   where muse_member_id is not null
                   group by muse_member_id
                   having count(distinct customer_unique_id)>1
                   )));;
  }

  dimension: new_muse {
    label: "New Muse Customers"
    group_label: "Sample Sets"
    description: "ALL New Muse members who have not registered prior to Muse launch."
    type: yesno
    sql: ${brand_customer_unique_id} in
            (select brand_customer_unique_id from `chb-prod-data-cust.prod_base_customers_final.base_customers_distinct` where muse_member_id is not null
            and brand_customer_unique_id in (


                  select brand_customer_unique_id
                  from
                  `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr`
                  where
                  atr_trandate >=${casted_start_date})
                  and brand_customer_unique_id
                  not in (
                   select brand_customer_unique_id  from

                   `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr`
                    where
                  atr_trandate <${casted_start_date}
                  and brand_customer_unique_id  is not null
                   )

                  and muse_member_id not in(
                   select muse_member_id from (

                   SELECT count(distinct customer_unique_id), muse_member_id FROM `chb-prod-data-cust.prod_base_customers_final.base_customers_distinct`
                   where muse_member_id is not null
                   group by muse_member_id
                   having count(distinct customer_unique_id)>1
                   )));;
  }




  dimension: new_crm_exclusive {
    label: "New CRM Only Customers"
    group_label: "Sample Sets"
    description: "New CRM members since Muse launch who have not signed up to Muse."
    type: yesno
    sql: ${brand_customer_unique_id} in
            (select brand_customer_unique_id from `chb-prod-data-cust.prod_base_customers_final.base_customers_distinct` where muse_member_id is null and speedbus_id is not null
            and brand_customer_unique_id in (


                  select brand_customer_unique_id
                  from
                  `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr`
                  where
                  atr_trandate >=${casted_start_date}
                  and crm_customer_id is not null
                  and atr_muse_id is null
                  and brand_customer_unique_id is not null

                  and brand_customer_unique_id
                  not in (
                   select brand_customer_unique_id  from

                   `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr`
                    where
                  atr_trandate <${casted_start_date}'
                  and brand_customer_unique_id  is not null
                   )
                   and brand_customer_unique_id  in (
                        select brand_customer_unique_id from
                        `chb-prod-data-cust.prod_base_customers_final.base_customers_distinct`
                        where muse_member_id is null
                        and  lower(first_name) not in ('national', '"valued','unknown', 'valued'))
                  ));;
  }


          dimension: is_muse_elite {
            group_label: "Sample Sets"
            type: yesno
            sql: cast(${muse_member_id} as string) in ('720101730854',  '720101481797', '720101093386', '720100964843', '720100829228', '720100622367', '720100523839', '720100485682', '720100479396', '720100260374',
                                                        '720100221160', '720100211997', '720100182305', '720100116303', '720100377350', '720102693713', '720100039299', '720100491151', '720100324048', '720100338238',
                                                        '720101145616', '720100286320', '720100580177', '720100271991', '720100161077', '720100932998', '720101634528', '720100778912', '720100660243', '720100384059',
                                                        '720100456030', '720100049413', '720100131906', '720101712688', '720100170607', '720100722043', '720100308140', '720100480063', '720100259376', '720100580359',
                                                        '720101005398', '720100750283', '720100731390', '720100462053', '720100102568', '720100427213', '720100197964', '720100157828', '720100092967', '720100213779',
                                                         '720100749038',  '720100162992', '720100329070', '720100352585', '720102784074', '720100214462', '720101457078', '720100740250', '720100280737', '720101370727',
                                                        '720102451898','720104239135') ;;
          }

          measure: average_lifetime_spend{
            description: "Average spent over the lifetime of a customer, not filtered by date."
            type: average
            value_format: "$#,##0"
            sql: ${customer_lifetime_spend} ;;
          }

          measure: average_recency {
            group_label: "Tenure Dimensions"
            type: average
            sql: ${recency} ;;
          }

  measure: brand_customer_count {
    type: count_distinct
    sql: ${brand_customer_unique_id} ;;
    }

  measure: customer_count {
    hidden: yes
    type: count_distinct
    sql: ${brand_customer_unique_id} ;;
  }


  measure: group_customer_count {
    type: count_distinct
    sql: ${customer_unique_id} ;;
  }


  measure: count {
    type: count
    drill_fields: [last_name, brand_name, first_name]
  }

  dimension: recency_range {
    view_label: "Customers"
    group_label: "Tenure Dimensions"
    label: "Recency Range"
    hidden: no
    description: "Recency Range for RFM analysis"
    type: string
    sql: CASE
          WHEN ${recency} <= 30 THEN "1 Month"
          WHEN ${recency} <= 183 THEN "2-6 Months"
          WHEN ${recency} <= 365 THEN "7-12 Months"
          WHEN ${recency} <= 548 THEN "13-18 Months"
               WHEN ${recency} <= 730 THEN "18-24 Months"
          WHEN ${recency} > 730 THEN "25 Months+"
          ELSE null
          END;;
  }

  measure: distinct_brands_shopped{
    description: "Distinct brands shopped by Member"
    type: count_distinct
#     value_format: "##.##"
    sql:${brand_id}  ;;
  }









}
