view: rfm_scoring {
  sql_table_name: `chb-prod-data-cust.prod_Customer360.rfm_scoring`
    ;;

  dimension: brand_customer_unique_id {
    hidden: yes
    type: string
    sql: ${TABLE}.brand_customer_unique_id ;;
  }

  dimension: final_score {
    label: "Total Score"
    view_label: "Customers"
    group_label: "RFM scoring"
    type: number
    sql: ${TABLE}.final_score ;;
  }

  dimension: frequency_range {
    view_label: "Customers"
    group_label: "RFM scoring"
    type: string
    sql: ${TABLE}.frequency_range ;;
  }

  dimension: frequency_score {
    view_label: "Customers"
    group_label: "RFM scoring"
    type: number
    sql: ${TABLE}.frequency_score ;;
  }

  dimension: recency_range {
    view_label: "Customers"
    group_label: "RFM scoring"
    type: string
    sql: ${TABLE}.recency_range ;;
  }

  dimension: recency_score {
    view_label: "Customers"
    group_label: "RFM scoring"
    type: number
    sql: ${TABLE}.recency_score ;;
  }

  dimension: segment {
    view_label: "Customers"
    group_label: "RFM scoring"
    type: string
    sql: ${TABLE}.segment ;;
  }

  dimension: value_range {
    view_label: "Customers"
    group_label: "RFM scoring"
    type: string
    sql: ${TABLE}.value_range ;;
  }

  dimension: value_score {
    view_label: "Customers"
    group_label: "RFM scoring"
    type: number
    sql: ${TABLE}.value_score ;;
  }

}
