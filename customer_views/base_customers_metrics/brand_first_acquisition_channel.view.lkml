view: brand_first_acquisition_channel {
  view_label: "Customers"
  sql_table_name: `chb-prod-data-cust.prod_Customer360.brand_first_acquisition_channel`
    ;;

  dimension: brand_acquisition_channel {
    group_label: "Aquisition Channel (Beta)"
    type: string
    sql: ${TABLE}.brand_acquisition_channel ;;
  }

  dimension: brand_customer_unique_id {
    type: string
    hidden: yes
    sql: ${TABLE}.brand_customer_unique_id ;;
  }

}
