view: customer_item_path_next_transaction {
  view_label: "Customers"
  sql_table_name: `chb-prod-data-cust.prod_Customer360.customer_item_path_next_transaction`
    ;;

  dimension: brand_customer_unique_id {
    primary_key: yes
    hidden: yes
    type: string
    sql: ${TABLE}.brand_customer_unique_id ;;
  }

  dimension: brand_name {
    hidden: yes
    type: string
    sql: ${TABLE}.brand_name ;;
  }

  dimension_group: first_txn {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    hidden: yes
    sql: ${TABLE}.first_txn_date ;;
  }

  dimension: first_txn_division {
    group_label: "Path"
    hidden: yes
    type: string
    sql: ${TABLE}.first_txn_division ;;
  }

  dimension: first_txn_subclass {
    group_label: "Path"
    hidden: yes
    type: string
    sql: ${TABLE}.first_txn_subclass ;;
  }

  dimension_group: next_txn {
    label: "Next Txn"
    type: time
    timeframes: [
      date,
    ]
    group_label: "Path"
    convert_tz: no
    datatype: date
    sql: ${TABLE}.next_txn_date ;;
  }

  dimension: next_txn_division {
    group_label: "Path"
    type: string
    sql: ${TABLE}.next_txn_division ;;
  }

  dimension: next_txn_subclass {
    group_label: "Path"
    type: string
    sql: ${TABLE}.next_txn_subclass ;;
  }

}
