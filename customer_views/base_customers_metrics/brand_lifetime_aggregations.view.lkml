view: brand_lifetime_aggregations {
  view_label: "Customers"
  sql_table_name: `chb-prod-data-cust.prod_Customer360.brand_lifetime_aggregations`
    ;;

  dimension: brand_customer_unique_id {
    type: string
    primary_key: yes
    hidden: yes
    sql: ${TABLE}.brand_customer_unique_id ;;
  }

  dimension_group: brand_first_txn {
    label: "Brand First Txn"
    description: "First time a customer is making a purchase (in Days) in the brand"
    group_label: "Customer Dimensions"
    type: time
    timeframes: [
      raw,
      date,
    ]
    sql: ${TABLE}.first_txn_date ;;

    }

  dimension_group: brand_last_txn {
    label: "Brand Last Txn"
    description: "Last time a customer is making a purchase (in Days) in the brand"
    group_label: "Customer Dimensions"
    type: time
    timeframes: [
      raw,
      date,
    ]
    sql: ${TABLE}.last_txn_date ;;
  }

  dimension: first_date {
    hidden: yes
    type: date
    sql: CAST(${TABLE}.first_txn_date AS DATE) ;;
  }

  dimension: last_date {
    hidden: yes
    type: date
    sql: CAST(${TABLE}.last_txn_date AS DATE) ;;
  }

  dimension: recency {
    label: "Brand Recency"
    group_label: "Customer Dimensions"
    type: number
    sql: ${TABLE}.recency ;;
  }

  dimension: tenure {
    label: "Brand Tenure"
    group_label: "Customer Dimensions"
    type: number
    sql: ${TABLE}.tenure ;;
  }

  dimension: total_sales_usd {
    group_label: "Customer Dimensions"
    hidden: yes
    type: number
    sql: ${TABLE}.total_sales_usd ;;
  }

  dimension: transaction_in_f12m {
    group_label: "Customer Dimensions"
    label: "Transaction in first 12 months"
    type: yesno
    sql: CAST(${TABLE}.transaction_in_f12m AS BOOL) ;;
  }

  dimension: transaction_count {
    group_label: "Customer Dimensions"
    hidden: yes
    type: number
    sql: ${TABLE}.transaction_count ;;
  }

  dimension: fav_store_name {
    label: "Favourite Store Name"
    group_label: "Customer Dimensions"
    type: string
    sql: ${TABLE}.fav_store_name ;;
  }

  measure: brand_average_recency {
    group_label: "Brand Customer Measures"
    value_format: "0"
    description: "Average of how recently customers have made a purchase in the brand"
    type: average_distinct
    sql_distinct_key: ${brand_customer_unique_id} ;;
    sql: ${recency} ;;
  }
  measure: brand_average_tenure {
    group_label: "Brand Customer Measures"
    description: "When was the first time a customer has made a purchase in the brand ? (in Days)"
    type: average_distinct
    sql_distinct_key: ${brand_customer_unique_id} ;;
    value_format: "0"
    sql: ${tenure} ;;

  }

  dimension: brand_recency_sort {

    hidden: yes

    type: number
    sql: CASE
                          WHEN ${recency} <= 30 THEN 1
                          WHEN ${recency} <= 183 THEN 2
                          WHEN ${recency} <= 365 THEN 3
                          WHEN ${recency} <= 548 THEN 4
                               WHEN ${recency} <= 730 THEN 5
                          WHEN ${recency} > 730 THEN 6
                          ELSE null
                          END;;
  }


  dimension: brand_recency_range {
    order_by_field: brand_recency_sort
    view_label: "Customers"
    group_label: "Customer Dimensions"
    label: "Brand Recency Range"
    hidden: no
    description: "Brand Recency Range for RFM analysis"

    type: string
    sql: CASE
            WHEN ${recency} <= 30 THEN "1 Month"
            WHEN ${recency} <= 183 THEN "2-6 Months"
            WHEN ${recency} <= 365 THEN "7-12 Months"
            WHEN ${recency} <= 548 THEN "13-18 Months"
            WHEN ${recency} <= 730 THEN "18-24 Months"
            WHEN ${recency} > 730 THEN "25 Months+"
            ELSE null
          END;;

    }


}
