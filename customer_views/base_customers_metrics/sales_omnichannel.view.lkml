view: sales_omnichannel {
  view_label: "Customers"
  sql_table_name: `chb-prod-data-cust.prod_Customer360.sales_omnichannel`
    ;;

  dimension: pk {
    hidden: yes
    primary_key: yes
    type: string
    sql: CONCAT(${TABLE}.brand_customer_unique_id, CAST(${TABLE}.bk_businessdate AS STRING)) ;;
  }

  dimension: bk_businessdate {
    hidden: yes
    type: number
    sql: ${TABLE}.bk_businessdate ;;
  }

  dimension: brand_customer_unique_id {
    hidden: yes
    type: string
    sql: ${TABLE}.brand_customer_unique_id ;;
  }

  dimension: brand_id {
    hidden: yes
    type: number
    sql: ${TABLE}.brand_id ;;
  }

  dimension: ecomm_flag_time_based {
    hidden: no
    group_label: "Omni-Channel filters"
    description: "Customers who have shopped online. Time based"
    type: yesno
    sql: ${TABLE}.ecomm_flag ;;
  }

  dimension: offline_flag_time_based {
    hidden: no
    group_label: "Omni-Channel filters"
    description: "Customers who have shopped offline. Time based"
    type: yesno
    sql: ${TABLE}.offline_flag ;;
  }

  dimension: online_offline_omnichannel_time_based {
    hidden: no
    group_label: "Omni-Channel filters"
    label: "Online offline or omnichannel - time based"
    description: "Whether customer shops only offline or online or is omnichannel. Based on datetime"
    type: string
    sql: ${TABLE}.online_offline_omnichannel ;;
  }

  # measure: count {
  #   type: count
  #   drill_fields: []
  # }
}
