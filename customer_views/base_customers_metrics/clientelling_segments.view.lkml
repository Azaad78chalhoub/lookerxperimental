view: clientelling_segments {
  view_label: "Customers"
  sql_table_name: `chb-prod-data-cust.prod_Customer360.clientelling_segments`
    ;;

  dimension: brand_customer_unique_id {
    type: string
    hidden: yes
    primary_key: yes
    sql: ${TABLE}.brand_customer_unique_id ;;
  }

  dimension: brand_id {
    type: number
    hidden: yes
    sql: ${TABLE}.brand_id ;;
  }

  dimension: clienteling_segment {
    group_label: "Clienteling Dimensions"
    type: string
    sql: ${TABLE}.clientelling_segment ;;
  }


  dimension: clienteling_y_n {
    group_label: "Clienteling Dimensions"
    type: string
    sql: CASE WHEN ${clienteling_segment} IN ('2. Potential VIP','1. VIP') THEN 'Y' ELSE 'N' END ;;
  }

}
