view: customer_first_transaction {
  sql_table_name: `chb-prod-data-cust.prod_Customer360.customer_first_transaction`
    ;;

  dimension: brand_customer_unique_id {
    hidden: yes
    type: string
    sql: ${TABLE}.brand_customer_unique_id ;;
  }

  dimension: first_purchase_division {
    label: "First Txn Division"
    view_label: "Customers"
    group_label: "Path"
    type: string
    sql: ${TABLE}.first_purchase_division ;;
  }

  dimension: first_purchase_subclass {
    label: "First Txn Subclass"
    view_label: "Customers"
    group_label: "Path"
    type: string
    sql: ${TABLE}.first_purchase_subclass ;;
  }

  dimension_group: first_txn {
    hidden: yes
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.first_txn_date ;;
  }

}
