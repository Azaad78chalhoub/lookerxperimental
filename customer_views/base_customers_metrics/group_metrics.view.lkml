view: group_metrics {
  view_label: "Retail Sales"
  sql_table_name: `chb-prod-data-cust.prod_Customer360.group_metrics`
    ;;

  dimension: atr_trandate {
    hidden: yes
    type: date
    sql: ${TABLE}.atr_trandate ;;
  }

  dimension: customer_unique_id {
    type: number
    hidden: yes
    sql: ${TABLE}.customer_unique_id ;;
  }

  dimension: group_sales_local {
    hidden: yes
    type: number
    sql: ${TABLE}.group_sales_local ;;
  }

  dimension: group_sales_usd {
    hidden: yes
    type: number
    sql: ${TABLE}.group_sales_usd ;;
  }

  dimension: group_transaction_count {
    hidden: yes
    type: number
    sql: ${TABLE}.group_transaction_count ;;
  }

  dimension: pk {
    type: string
    primary_key: yes
    hidden: yes
    sql: ${TABLE}.pk ;;
  }

  measure: group_transaction_count_sum {
    type: sum_distinct
    label: "Group Transaction Count"
    group_label: "Group Metrics"
    description: "Total transaction count for customer in the Chalhoub group, not brand filtered"
    sql_distinct_key: ${pk} ;;
    sql: ${group_transaction_count} ;;
  }
  measure: group_sales_usd_sum {
    type: sum_distinct
    label: "Group Sales Amount USD"
    group_label: "Group Metrics"
    description: "Total USD sales for customer in the Chalhoub group, not brand filtered"
    value_format: "$#,##0;($#.00)"
    sql_distinct_key: ${pk} ;;
    sql: ${group_sales_usd} ;;
  }

  measure: group_sales_local_sum {
    type: sum_distinct
    label: "Group Sales Amount Local"
    group_label: "Group Metrics"
    description: "Total local currency sales for customer in the Chalhoub group, not brand filtered"
    value_format: "$#,##0;($#.00)"
    sql_distinct_key: ${pk} ;;
    sql: ${group_sales_local} ;;
  }
}
