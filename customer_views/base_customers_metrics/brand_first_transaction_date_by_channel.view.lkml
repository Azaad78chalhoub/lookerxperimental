view: brand_first_transaction_date_by_channel {
  view_label: "Customer First Transaction Dates"
  sql_table_name: `chb-prod-data-cust.prod_Customer360.brand_first_transaction_date_by_channel`
    ;;

  dimension: brand_customer_unique_id {
    type: string
    hidden: yes
    sql: ${TABLE}.brand_customer_unique_id ;;
  }

  dimension_group: brand_first_offline_transaction {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.brand_first_offline_transaction_date ;;
  }

  dimension_group: brand_first_online_app_transaction {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.brand_first_online_app_transaction_date ;;
  }

  dimension_group: brand_first_online_transaction {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.brand_first_online_transaction_date ;;
  }

  dimension_group: brand_first_online_web_transaction {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.brand_first_online_web_transaction_date ;;
  }

  measure: count {
    hidden: yes
    type: count
    drill_fields: []
  }
}
