view: group_benchmarks {
  view_label: "Group Benchmarks"
  sql_table_name: `chb-prod-data-cust.prod_Customer360.group_benchmarks`
    ;;

  dimension: base_transaction_date_id {
    primary_key: yes
    hidden: yes
    type: number
    sql: ${TABLE}.base_transaction_date_id ;;
  }

  dimension: count_active_customers {
    description: "Count of active group customers per day"
    hidden: yes
    type: number
    sql: ${TABLE}.count_active_customers ;;
  }

  dimension: avg_recency {
    description: "Average recency of customers per date"
    hidden: yes
    type: number
    sql: ${TABLE}.avg_recency ;;
  }

  dimension: count_new_customers {
    description: "Count of new (brand-wise new) group customers per day"
    type: number
    hidden: yes
    sql: ${TABLE}.count_new_customers ;;
  }

  dimension: count_returning_customers {
    description: "Count of returning (brand-wise returning) group customers per day"
    type: number
    hidden: yes
    sql: ${TABLE}.count_returning_customers ;;
  }

  dimension: footwear_group_sales_amount_usd {
    description: "Total sales of footwear category per day (group wise)"
    hidden: yes
    type: number
    sql: ${TABLE}.footwear_group_sales_amount_usd ;;
  }

  dimension: footwear_group_transaction_count {
    description: "Total transaction count of footwear category per day (group wise)"
    hidden: yes
    type: number
    sql: ${TABLE}.footwear_group_transaction_count ;;
  }

  dimension: total_customer_count {
    description: "Rolling sum of brand unique customers"
    hidden: yes
    type: number
    sql: ${TABLE}.total_customer_count ;;
  }

  dimension: total_ltv {
    description: "Rolling LTV of all known customers"
    hidden: yes
    type: number
    sql: ${TABLE}.total_ltv ;;
  }

  dimension: total_transactions {
    description: "Rolling Transaction count of all known customers"
    hidden: yes
    type: number
    sql: ${TABLE}.total_transactions ;;
  }

  measure: max_total_ltv {
    hidden: yes
    description: "Maximum total ltv from selected period (will be the last date)"
    type: max
    sql: ${total_ltv} ;;
  }


  measure: max_total_transactions {
    hidden: yes
    description: "Maximum total transactions from selected period (will be the last date)"
    type: max
    sql: ${total_transactions} ;;
  }

  measure: max_total_customer_count {
    hidden: yes
    description: "Maximum total customer count from selected period (will be the last date)"
    type: max
    sql: ${total_customer_count} ;;
  }

  measure: average_group_ltv {
    description: "Average lifetime value of group customer per date (Will pick largest from selected period)"
    type: number
    label: "Average Group LTV"
    value_format: "0.0"
    sql: ${max_total_ltv}/${max_total_customer_count} ;;
  }

  measure: average_group_lifetime_frequency {
    description: "Average frequency of group customer per date (Will pick largest from selected period)"
    type: number
    label: "Average Group Lifetime Frequency"
    value_format: "0.0"
    sql: ${max_total_transactions}/${max_total_customer_count} ;;
  }

  measure: sum_footwear_group_transaction_count {
    description: "Total transaction count of footwear category per day (group wise)"
    type: sum
    hidden: yes
    sql: ${footwear_group_transaction_count} ;;
  }

  measure: sum_footwear_group_sales_amount_usd {
    description: "Total sales of footwear category per day (group wise)"
    type: sum
    hidden: yes
    sql: ${footwear_group_sales_amount_usd} ;;
  }

  measure: average_group_footwear_aov {
    description: "Average Order Value of footwear product sale "
    label: "AOV Group Footwear"
    value_format: "0.0"
    type: number
    sql: ${sum_footwear_group_sales_amount_usd}/${sum_footwear_group_transaction_count} ;;
  }

  measure: max_avg_recency {
    description: "Average Recency from given period"
    label: "Average Recency"
    value_format: "0.0"
    type: max
    sql: ${avg_recency} ;;
  }

}
