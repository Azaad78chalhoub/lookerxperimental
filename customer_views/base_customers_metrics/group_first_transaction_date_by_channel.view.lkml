view: group_first_transaction_date_by_channel {
  view_label: "Customer First Transaction Dates"
  sql_table_name: `chb-prod-data-cust.prod_Customer360.group_first_transaction_date_by_channel`
    ;;

  dimension: customer_unique_id {
    type: number
    hidden: yes
    sql: ${TABLE}.customer_unique_id ;;
  }

  dimension_group: group_first_offline_transaction {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.group_first_offline_transaction_date ;;
  }

  dimension_group: group_first_online_app_transaction {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.group_first_online_app_transaction_date ;;
  }

  dimension_group: group_first_online_transaction {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.group_first_online_transaction_date ;;
  }

  dimension_group: group_first_online_web_transaction {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.group_first_online_web_transaction_date ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
