view: communication_consent {
  view_label: "Customers"
  sql_table_name: `chb-prod-data-cust.prod_Customer360.communication_consent`
    ;;

  dimension: brand_customer_unique_id {
    type: string
    hidden: yes
    sql: ${TABLE}.brand_customer_unique_id ;;
  }

  dimension: customer_unique_id {
    type: number
    hidden: yes
    sql: ${TABLE}.customer_unique_id ;;
  }

  dimension: source {
    group_label: "Communication Consent (beta)"
    type: string
    sql: ${TABLE}.source ;;
  }

  dimension: is_consent_email {
    group_label: "Communication Consent (beta)"
    type: yesno
    sql: ${TABLE}.is_consent_email ;;
  }

  dimension: is_consent_push_notification {
    group_label: "Communication Consent (beta)"
    type: yesno
    sql: ${TABLE}.is_consent_push_notification ;;
  }

  dimension: is_consent_sms {
    group_label: "Communication Consent (beta)"
    type: yesno
    sql: ${TABLE}.is_consent_sms ;;
  }

  dimension: is_consent_whatsapp {
    group_label: "Communication Consent (beta)"
    type: yesno
    sql: ${TABLE}.is_consent_whatsapp ;;
  }

  dimension: email {
    type: string
    hidden: yes
    sql: ${TABLE}.email ;;
  }

  dimension: mobile {
    type: string
    hidden: yes
    sql: ${TABLE}.mobile ;;
  }

}
