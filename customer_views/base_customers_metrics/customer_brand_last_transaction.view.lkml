view: customer_brand_last_transaction {
  sql_table_name: `chb-prod-data-cust.prod_Customer360.customer_brand_last_transaction`
    ;;

  dimension: brand_customer_unique_id {
    hidden: yes
    type: string
    sql: ${TABLE}.brand_customer_unique_id ;;
  }

  dimension_group: last_txn {
    hidden: yes
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.last_txn_date ;;
  }

  dimension: last_txn_store_id {
    view_label: "Customers"
    group_label: "Brand Last Transaction"
    label: "Brand Last Transaction Store ID"
    type: number
    sql: ${TABLE}.last_txn_store_id ;;
  }

  dimension: last_txn_store_name {
    view_label: "Customers"
    group_label: "Brand Last Transaction"
    label: "Brand Last Transaction Store Name"
    type: string
    sql: ${TABLE}.last_txn_store_name ;;
  }

}
