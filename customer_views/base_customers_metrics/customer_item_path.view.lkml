view: customer_item_path {
  sql_table_name: `chb-prod-data-cust.prod_Customer360.customer_item_path`
    ;;

  dimension: brand_customer_unique_id {
    hidden: yes
    type: string
    sql: ${TABLE}.brand_customer_unique_id ;;
  }

  dimension: multi_division {
    view_label: "Customers"
    group_label: "Path"
    type: number
    sql: ${TABLE}.multi_division ;;
  }

  dimension: multi_subclass {
    view_label: "Customers"
    group_label: "Path"
    type: number
    sql: ${TABLE}.multi_subclass ;;
  }

}
