view: brand_omnichannel {
  view_label: "Customers"
  sql_table_name: `chb-prod-data-cust.prod_Customer360.brand_omnichannel`
    ;;

  dimension: brand_customer_unique_id {
    type: string
    hidden: yes
    primary_key: yes
    sql: ${TABLE}.brand_customer_unique_id ;;
  }

  dimension: ecomm_cust {
    group_label: "Omni-Channel filters"
    type: yesno
    description: "Customers who have shopped online."
    sql: ${TABLE}.ecomm_flag;;
  }

  dimension: offline_cust {
    group_label: "Omni-Channel filters"
    type: yesno
    description: "Customers who have shopped offline."
    sql: ${TABLE}.offline_flag;;
  }

  dimension: online_ofline_omnichannel{
    hidden: yes
    label: "(beta) online offline or omnichannel"
    group_label: "Omni-Channel filters"
    description: "Whether customer shops only offline or online or is omnichannel."
    type: string
    sql: ${TABLE}.online_offline_omnichannel ;;
  }

  dimension: app_web_flag {
    label: "(beta) App or Web "
    group_label: "App or Web"
    description: "Whether customer shops only web or app"
    type: string
    sql: ${TABLE}.app_web_flag ;;
  }

}
