view: group_first_acquisition_channel {
  view_label: "Customers"
  sql_table_name: `chb-prod-data-cust.prod_Customer360.group_first_acquisition_channel`
    ;;

  dimension: customer_unique_id {

    hidden: yes
    type: number
    sql: ${TABLE}.customer_unique_id ;;
  }

  dimension: group_acquisition_channel {
    group_label: "Aquisition Channel (Beta)"
    type: string
    sql: ${TABLE}.group_acquisition_channel ;;
  }

}
