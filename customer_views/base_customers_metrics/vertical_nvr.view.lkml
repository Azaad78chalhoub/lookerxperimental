view: vertical_nvr {
  view_label: "Customers"
  sql_table_name: `chb-prod-data-cust.prod_Customer360.vertical_nvr`
    ;;

  dimension: surrogate_key_generated_uid {
    type: string
    primary_key: yes
    hidden: yes
    sql: ${TABLE}.surrogate_key_generated_uid ;;
  }

  dimension: beauty_nvr {
    label: "Vertical Beauty New vs Returning"
    description: "If customer is new or returning for beauty vertical"
    type: string
    sql: ${TABLE}.beauty_nvr ;;
  }

  dimension: fashion_nvr {
    label: "Vertical Fashion New vs Returning"
    description: "If customer is new or returning for fashion vertical"
    type: string
    sql: ${TABLE}.fashion_nvr ;;
  }

  dimension: bk_businessdate {
    type: number
    hidden: yes
    sql: ${TABLE}.bk_businessdate ;;
  }

  dimension: customer_unique_id {
    type: number
    hidden: yes
    sql: ${TABLE}.customer_unique_id ;;
  }

  dimension: first_beauty_date_id {
    type: number
    hidden: yes
    sql: ${TABLE}.first_beauty_date_id ;;
  }

  dimension: first_fashion_date_id {
    type: number
    hidden: yes
    sql: ${TABLE}.first_fashion_date_id ;;
  }

  dimension: vertical {
    type: string
    hidden: yes
    sql: ${TABLE}.vertical ;;
  }

}
