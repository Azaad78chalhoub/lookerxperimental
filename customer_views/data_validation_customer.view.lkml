view: data_validation_customer {
  sql_table_name: `chb-prod-data-cust.prod_data_quality.customer_tables`
    ;;

  dimension: cd_brand_customer_unique_id {
    label: "Unique Brand Generic Hkeys"
    group_label: "Numbers"
    type: number
    sql: ${TABLE}.cd_brand_customer_unique_id ;;
  }

  dimension: cd_brand_id {
    label: "Unique Brand IDs"
    group_label: "Numbers"
    type: number
    sql: ${TABLE}.cd_brand_id ;;
  }

  dimension: cd_brand_name {
    label: "Unique Brand Names"
    group_label: "Numbers"
    type: number
    sql: ${TABLE}.cd_brand_name ;;
  }

  dimension: cd_email_validated {
    label: "Unique Email Validated"
    group_label: "Numbers"
    type: number
    sql: ${TABLE}.cd_email_validated ;;
  }

  dimension: cd_mobile_validated {
    label: "Unique Mobile Validated"
    group_label: "Numbers"
    type: number
    sql: ${TABLE}.cd_mobile_validated ;;
  }

  dimension: cd_birthdate {
    label: "Unique Birthdate"
    group_label: "Numbers"
    type: number
    sql: ${TABLE}.cd_birthdate ;;
  }

  dimension: cd_email {
    label: "Unique Email"
    group_label: "Numbers"
    type: number
    sql: ${TABLE}.cd_email ;;
  }

  dimension: cd_mobile {
    label: "Unique Mobile"
    group_label: "Numbers"
    type: number
    sql: ${TABLE}.cd_mobile ;;
  }

  dimension: cd_orco_pos_id {
    label: "Unique ORCO POS ID"
    group_label: "Numbers"
    type: number
    sql: ${TABLE}.cd_orco_pos_id ;;
  }

  dimension: cd_speedbus_id {
    label: "Unique Speedbus ID"
    group_label: "Numbers"
    type: number
    sql: ${TABLE}.cd_speedbus_id ;;
  }

  dimension: cd_urbanbuz_id {
    label: "Unique Urbanbuz ID"
    group_label: "Numbers"
    type: number
    sql: ${TABLE}.cd_urbanbuz_id ;;
  }

  dimension: cd_xcenter_pos_id {
    label: "Unique XCENTER POS ID"
    group_label: "Numbers"
    type: number
    sql: ${TABLE}.cd_xcenter_pos_id ;;
  }

  dimension: cnt_email_validated_speedbus_id {
    label: "Count Speedbus ID with multiple Email Validated"
    group_label: "Numbers"
    type: number
    sql: ${TABLE}.cnt_email_validated_speedbus_id ;;
  }

  dimension: cnt_speedbus_id_email_validated {
    label: "Count Email Validated with multiple Speedbus ID"
    group_label: "Numbers"
    type: number
    value_format_name: id
    sql: ${TABLE}.cnt_speedbus_id_email_validated ;;
  }

  dimension: count_rows {
    type: number
    sql: ${TABLE}.count_rows ;;
  }

  dimension: count_full_rows {
    group_label: "Row Score"
    label: "Count Full Rows"
    type: number
    sql: ${TABLE}.count_full_rows ;;
  }

  dimension: customer_row_score {
    group_label: "Row Score"
    label: "Customer Rows Score"
    hidden: no
    type: number
    sql: ${TABLE}.customer_row_score ;;
  }

  dimension: customer_row_score_20 {
    group_label: "Row Score"
    label: "Customer Rows Score 0-20"
    hidden: no
    type: number
    sql: ${TABLE}.customer_row_score_20 ;;
  }

  dimension: customer_row_score_20_50 {
    group_label: "Row Score"
    label: "Customer Rows Score 20-50"
    hidden: no
    type: number
    sql: ${TABLE}.customer_row_score_20_50 ;;
  }

  dimension: customer_row_score_50_70 {
    group_label: "Row Score"
    label: "Customer Rows Score 50-70"
    hidden: no
    type: number
    sql: ${TABLE}.customer_row_score_50_70 ;;
  }

  dimension: customer_row_score_70_90 {
    group_label: "Row Score"
    label: "Customer Rows Score 70-90"
    hidden: no
    type: number
    sql: ${TABLE}.customer_row_score_70_90 ;;
  }

  dimension: customer_row_score_90 {
    group_label: "Row Score"
    label: "Customer Rows Score 90-100"
    hidden: no
    type: number
    sql: ${TABLE}.customer_row_score_90 ;;
  }

  dimension: full_possible_score {
    group_label: "Row Score"
    label: "Full Possible Score"
    hidden: no
    type: number
    sql: ${TABLE}.full_possible_score ;;
  }

  dimension: dim_table_score {
    group_label: "Row Score"
    label: "Quality Table Score %"
    hidden: no
    value_format: "0.0%"
    type: number
    sql: IFNULL(${customer_row_score}/NULLIF(${full_possible_score}, 0), 0) ;;
  }

  dimension: is_generic_customer {
    label: "Count Generic Customers"
    group_label: "Numbers"
    type: number
    sql: ${TABLE}.is_generic_customer ;;
  }

  dimension: gender {
    label: "Count Unique Gender"
    group_label: "Numbers"
    type: number
    sql: ${TABLE}.cd_gender ;;
  }

  dimension: cd_muse_member_id {
    label: "Count Unique Muse Member Id"
    group_label: "Numbers"
    type: number
    sql: ${TABLE}.cd_muse_member_id ;;
  }

  dimension: nationality {
    label: "Count Unique Nationality"
    group_label: "Numbers"
    type: number
    sql: ${TABLE}.cd_nationality ;;
  }

  dimension: country_of_residence {
    label: "Count Unique Country of Residence"
    group_label: "Numbers"
    type: number
    sql: ${TABLE}.cd_country_of_residence ;;
  }


  dimension: residency {
    label: "Count Unique Residency"
    group_label: "Numbers"
    type: number
    sql: ${TABLE}.cd_residency ;;
  }

  dimension: ethnicity {
    label: "Count Unique Ethnicity"
    group_label: "Numbers"
    type: number
    sql: ${TABLE}.cd_ethnicity ;;
  }

  dimension: first_name {
    label: "Count Unique First Name"
    group_label: "Numbers"
    type: number
    sql: ${TABLE}.cd_first_name ;;
  }

  dimension: last_name {
    label: "Count Unique Last Name"
    group_label: "Numbers"
    type: number
    sql: ${TABLE}.cd_last_name ;;
  }

  dimension: cd_preferred_language {
    label: "Count Unique Preferred Language"
    group_label: "Numbers"
    type: number
    sql: ${TABLE}.cd_preferred_language ;;
  }

  dimension: ms_brand_customer_unique_id {
    label: "Recorded Brand Generic Hkey"
    group_label: "Numbers"
    type: number
    sql: ${TABLE}.ms_brand_customer_unique_id ;;
  }

  dimension: ms_brand_id {
    label: "Recorded Brand ID"
    group_label: "Numbers"
    type: number
    sql: ${TABLE}.ms_brand_id ;;
  }

  dimension: ms_brand_name {
    label: "Recorded Brand Name"
    group_label: "Numbers"
    type: number
    sql: ${TABLE}.ms_brand_name ;;
  }

  dimension: ms_email_validated {
    label: "Recorded Email Validated"
    group_label: "Numbers"
    type: number
    sql: ${TABLE}.ms_email_validated ;;
  }

  dimension: ms_mobile_validated {
    label: "Recorded Mobile Validated"
    group_label: "Numbers"
    type: number
    sql: ${TABLE}.ms_mobile_validated ;;
  }

  dimension: ms_birthdate {
    label: "Recorded Birthdate"
    group_label: "Numbers"
    type: number
    sql: ${TABLE}.ms_birthdate ;;
  }

  dimension: ms_email {
    label: "Recorded Email"
    group_label: "Numbers"
    type: number
    sql: ${TABLE}.ms_email;;
  }

  dimension: ms_mobile {
    label: "Recorded Mobile"
    group_label: "Numbers"
    type: number
    sql: ${TABLE}.ms_mobile;;
  }

  dimension: ms_orco_pos_id {
    label: "Recorded ORCO POS ID"
    group_label: "Numbers"
    type: number
    sql: ${TABLE}.ms_orco_pos_id ;;
  }

  dimension: ms_speedbus_id {
    label: "Recorded Speedbus ID"
    group_label: "Numbers"
    type: number
    sql: ${TABLE}.ms_speedbus_id ;;
  }

  dimension: ms_urbanbuz_id {
    label: "Recorded Urbanbuz ID"
    group_label: "Numbers"
    type: number
    sql: ${TABLE}.ms_urbanbuz_id ;;
  }

  dimension: ms_xcenter_pos_id {
    label: "Recorded XCENTER POS ID"
    group_label: "Numbers"
    type: number
    sql: ${TABLE}.ms_xcenter_pos_id ;;
  }


  dimension: ms_gender {
    label: "Recorded Gender"
    group_label: "Numbers"
    type: number
    sql: ${TABLE}.ms_gender ;;
  }

  dimension: ms_nationality {
    label: "Recorded Nationality"
    group_label: "Numbers"
    type: number
    sql: ${TABLE}.ms_nationality ;;
  }

  dimension: ms_country_of_residence {
    label: "Recorded Country of Residence"
    group_label: "Numbers"
    type: number
    sql: ${TABLE}.ms_country_of_residence ;;
  }


  dimension: ms_residency {
    label: "Recorded Residency"
    group_label: "Numbers"
    type: number
    sql: ${TABLE}.ms_residency ;;
  }

  dimension: ms_ethnicity {
    label: "Recorded Ethnicity"
    group_label: "Numbers"
    type: number
    sql: ${TABLE}.ms_ethnicity ;;
  }

  dimension: ms_first_name {
    label: "Recorded First Name"
    group_label: "Numbers"
    type: number
    sql: ${TABLE}.ms_first_name ;;
  }

  dimension: ms_last_name {
    label: "Recorded Last Name"
    group_label: "Numbers"
    type: number
    sql: ${TABLE}.ms_last_name ;;
  }

  dimension: ms_preferred_language {
    label: "Recorded Preferred Language"
    group_label: "Numbers"
    type: number
    sql: ${TABLE}.ms_preferred_language ;;
  }

  dimension: ms_muse_member_id {
    label: "Recorded Muse Member Id"
    group_label: "Numbers"
    type: number
    sql: ${TABLE}.ms_muse_member_id ;;
  }


  dimension: table_name {
    type: string
    sql: ${TABLE}.table_name ;;
  }

  dimension: count_unique_tb_key {
    type: number
    sql: ${TABLE}.count_unique_tb_key ;;
  }

  ### percentages (divided by count_rows)


  dimension: cd_brand_customer_unique_id_pct {
    label: "Unique Brand Generic Hkeys %"
    group_label: "Percentages"
    type: number
    value_format: "0%"
    sql: ${cd_brand_customer_unique_id}/NULLIF(${count_unique_tb_key},0) ;;
  }

  dimension: cd_email_validated_pct {
    label: "Unique Email Validated %"
    group_label: "Percentages"
    type: number
    value_format: "0%"
    sql: ${cd_email_validated}/NULLIF(${count_unique_tb_key},0) ;;
  }

  dimension: cd_mobile_validated_pct {
    label: "Unique Mobile Validated %"
    group_label: "Percentages"
    type: number
    value_format: "0%"
    sql: ${cd_mobile_validated}/NULLIF(${count_unique_tb_key},0)  ;;
  }


  dimension: cd_email_pct {
    label: "Unique Email %"
    group_label: "Percentages"
    type: number
    value_format: "0%"
    sql: ${cd_email}/NULLIF(${count_unique_tb_key},0) ;;
  }

  dimension: cd_mobile_pct {
    label: "Unique Mobile %"
    group_label: "Percentages"
    type: number
    value_format: "0%"
    sql: ${cd_mobile}/NULLIF(${count_unique_tb_key},0)  ;;
  }

  dimension: cd_muse_member_id_pct {
    label: "Unique Muse Member Id %"
    group_label: "Percentages"
    type: number
    value_format: "0%"
    sql: ${cd_muse_member_id}/NULLIF(${count_unique_tb_key},0)  ;;
  }


  dimension: cd_orco_pos_id_pct {
    label: "Unique ORCO POS ID %"
    group_label: "Percentages"
    type: number
    value_format: "0%"
    sql: ${cd_orco_pos_id}/NULLIF(${count_unique_tb_key},0) ;;
  }

  dimension: cd_speedbus_id_pct {
    label: "Unique Speedbus ID %"
    group_label: "Percentages"
    type: number
    value_format: "0%"
    sql: ${cd_speedbus_id}/NULLIF(${count_unique_tb_key},0) ;;
  }

  dimension: cd_urbanbuz_id_pct {
    label: "Unique Urbanbuz ID %"
    group_label: "Percentages"
    type: number
    value_format: "0%"
    sql: ${cd_urbanbuz_id}/NULLIF(${count_unique_tb_key},0) ;;
  }

  dimension: cd_xcenter_pos_id_pct {
    label: "Unique XCENTER POS ID %"
    group_label: "Percentages"
    type: number
    value_format: "0%"
    sql: ${cd_xcenter_pos_id}/NULLIF(${count_unique_tb_key},0)  ;;
  }

  # dimension: cnt_email_validated_speedbus_id {
  #   label: "Count Speedbus ID with multiple Email Validated"
  #   group_label: "Numbers"
  #   type: number
  #   sql: ${TABLE}.cnt_email_validated_speedbus_id ;;
  # }

  # dimension: cnt_speedbus_id_email_validated {
  #   label: "Count Email Validated with multiple Speedbus ID"
  #   group_label: "Numbers"
  #   type: number
  #   value_format_name: id
  #   sql: ${TABLE}.cnt_speedbus_id_email_validated ;;
  # }



  dimension: is_generic_customer_pct {
    label: "Count Generic Customers %"
    group_label: "Percentages"
    type: number
    value_format: "0%"
    sql: ${is_generic_customer}/NULLIF(${count_unique_tb_key},0)  ;;
  }

  dimension: ms_brand_customer_unique_id_pct {
    label: "Recorded Brand Generic Hkey %"
    group_label: "Percentages"
    type: number
    value_format: "0%"
    sql: ${ms_brand_customer_unique_id}/NULLIF(${count_unique_tb_key},0) ;;
  }

  dimension: ms_email_validated_pct {
    label: "Recorded Email Validated %"
    group_label: "Percentages"
    type: number
    value_format: "0%"
    sql: ${ms_email_validated}/NULLIF(${count_unique_tb_key},0) ;;
  }

  dimension: ms_mobile_validated_pct {
    label: "Recorded Mobile Validated %"
    group_label: "Percentages"
    type: number
    value_format: "0%"
    sql: ${ms_mobile_validated}/NULLIF(${count_unique_tb_key},0) ;;
  }

  dimension: ms_email_pct {
    label: "Recorded Email %"
    group_label: "Percentages"
    type: number
    value_format: "0%"
    sql: ${ms_email}/NULLIF(${count_unique_tb_key},0) ;;
  }

  dimension: ms_mobile_pct {
    label: "Recorded Mobile %"
    group_label: "Percentages"
    type: number
    value_format: "0%"
    sql: ${ms_mobile}/NULLIF(${count_unique_tb_key},0) ;;
  }

  dimension: ms_orco_pos_id_pct {
    label: "Recorded ORCO POS ID %"
    group_label: "Percentages"
    type: number
    value_format: "0%"
    sql: ${ms_orco_pos_id}/NULLIF(${count_unique_tb_key},0) ;;
  }

  dimension: ms_speedbus_id_pct {
    label: "Recorded Speedbus ID %"
    group_label: "Percentages"
    type: number
    value_format: "0%"
    sql: ${ms_speedbus_id}/NULLIF(${count_unique_tb_key},0) ;;
  }

  dimension: ms_urbanbuz_id_pct {
    label: "Recorded Urbanbuz ID %"
    group_label: "Percentages"
    type: number
    value_format: "0%"
    sql: ${ms_urbanbuz_id}/NULLIF(${count_unique_tb_key},0) ;;
  }

  dimension: ms_birthdate_pct {
    label: "Recorded Birthdate %"
    group_label: "Percentages"
    type: number
    value_format: "0%"
    sql: ${ms_birthdate}/NULLIF(${count_unique_tb_key},0) ;;
  }

  dimension: ms_xcenter_pos_id_pct {
    label: "Recorded XCENTER POS ID %"
    group_label: "Percentages"
    type: number
    value_format: "0%"
    sql: ${ms_xcenter_pos_id}/NULLIF(${count_unique_tb_key},0);;
  }


  dimension: ms_gender_pct {
    label: "Recorded Gender %"
    group_label: "Percentages"
    type: number
    value_format: "0%"
    sql: ${ms_gender}/NULLIF(${count_unique_tb_key},0) ;;
  }

  dimension: ms_nationality_pct {
    label: "Recorded Nationality %"
    group_label: "Percentages"
    type: number
    value_format: "0%"
    sql: ${ms_nationality}/NULLIF(${count_unique_tb_key},0) ;;
  }

  dimension: ms_country_of_residence_pct {
    label: "Recorded Country of Residence %"
    group_label: "Percentages"
    type: number
    value_format: "0%"
    sql: ${ms_country_of_residence}/NULLIF(${count_unique_tb_key},0) ;;
  }


  dimension: ms_residency_pct {
    label: "Recorded Residency %"
    group_label: "Percentages"
    type: number
    value_format: "0%"
    sql: ${ms_residency}/NULLIF(${count_unique_tb_key},0) ;;
  }

  dimension: ms_ethnicity_pct {
    label: "Recorded Ethnicity %"
    group_label: "Percentages"
    type: number
    value_format: "0%"
    sql: ${ms_ethnicity}/NULLIF(${count_unique_tb_key},0) ;;
  }

  dimension: ms_first_name_pct {
    label: "Recorded First Name %"
    group_label: "Percentages"
    type: number
    value_format: "0%"
    sql: ${ms_first_name}/NULLIF(${count_unique_tb_key},0) ;;
  }

  dimension: ms_last_name_pct {
    label: "Recorded Last Name %"
    group_label: "Percentages"
    type: number
    value_format: "0%"
    sql: ${ms_last_name}/NULLIF(${count_unique_tb_key},0) ;;
  }

  dimension: ms_preferred_language_pct {
    label: "Recorded Preferred Language %"
    group_label: "Percentages"
    type: number
    value_format: "0%"
    sql: ${ms_preferred_language}/NULLIF(${count_unique_tb_key},0) ;;
  }

  dimension: ms_muse_member_id_pct {
    label: "Recorded Muse Member Id %"
    group_label: "Percentages"
    type: number
    value_format: "0%"
    sql: ${ms_muse_member_id}/NULLIF(${count_unique_tb_key},0) ;;
  }


  dimension: table_name2 {
      label: "New Table Name"
      hidden: no
      type: string
      sql: CASE
          WHEN ${TABLE}.table_name = "chb-prod-data-cust.prod_base_customers_source.urbanbuz_customers"  THEN "Urbanbuz Source"
          WHEN ${TABLE}.table_name = "chb-prod-data-cust.prod_base_customers_source.sf_group_customers"  THEN "SF Group Source"
          WHEN ${TABLE}.table_name = "chb-prod-data-cust.prod_base_customers_source.sf_lvlshoes_customers"  THEN "SF Level Source"
          WHEN ${TABLE}.table_name = "chb-prod-data-cust.prod_base_customers_source.pos_xcenter_customers"  THEN "Xcenter Source"
          WHEN ${TABLE}.table_name = "chb-prod-data-cust.prod_base_customers_source.pos_orco_customers"  THEN "Orpos Source"
          WHEN ${TABLE}.table_name = "chb-prod-data-cust.prod_base_customers_source.muse_customers"  THEN "Muse Source"
          WHEN ${TABLE}.table_name = "chb-prod-data-cust.prod_base_customers_final.base_customers_distinct"  THEN "Customer 360"
          ELSE NULL END;;
}

  dimension: table_name3 {
    label: "New Table Name 2"
    hidden: no
    type: string
    sql:replace(
          replace(
            replace(
              replace(
                replace(
                  replace(
                      replace(table_name, 'chb-prod-data-cust.prod_base_customers_',''),
                        '_customers',''),'final.base_distinct', 'Customer 360'), 'source.','Source '),'cleaned_step','Cleaned Step '),
                        'agg_step', 'Agg Step '),'.',' ');;

}


  dimension: customer_360 {
    label: "Is Customer 360"
    hidden: no
    type: string
    sql:CASE WHEN ${table_name3} = 'Customer 360' THEN 1 ELSE 0 END ;;
 }

  measure: vis_count_full_rows {
    label: "Count Full rows"
    hidden: no
    type: max
    sql: ${count_full_rows} ;;
  }

  measure: vis_count_unique_tb_key {
    label: "Unique TB Key"
    hidden: no
    type: max
    sql: ${count_unique_tb_key} ;;
  }

  measure: vis_ms_email_validated {
    label: "Unique Email Validated"
    hidden: no
    type: max
    sql: CASE WHEN ${customer_360} = 1 THEN ${cd_email_validated} ELSE ${ms_email} END ;;
  }

  measure: vis_ms_phone_validated {
    label: "Unique Phone Validated"
    hidden: no
    type: max
    sql: CASE WHEN ${customer_360} = 1 THEN ${cd_mobile_validated} ELSE ${ms_mobile} END ;;
  }

  measure: vis_ms_last_name {
    label: "Unique Last Name"
    hidden: no
    type: max
    sql: ${ms_last_name} ;;
  }

  measure: vis_ms_first_name {
    label: "Unique First Name"
    hidden: no
    type: max
    sql: ${ms_first_name} ;;
  }

  measure: vis_ms_gender {
    label: "Unique Gender"
    hidden: no
    type: max
    sql: ${ms_gender} ;;
  }

  measure: vis_ms_birthdate {
    label: "Unique Birthdate"
    hidden: no
    type: max
    sql: ${ms_birthdate} ;;
  }

  measure: vis_ms_nationality {
    label: "Unique Nationality"
    hidden: no
    type: max
    sql: ${ms_nationality} ;;
  }

  measure: vis_ms_residency {
    label: "Unique Residency"
    hidden: no
    type: max
    sql: ${ms_residency} ;;
  }

  measure: vis_ms_ethnicity {
    label: "Unique Ethnicity"
    hidden: no
    type: max
    sql: ${ms_ethnicity} ;;
  }

  measure: vis_ms_preferred_language {
    label: "Unique Preferred Language"
    hidden: no
    type: max
    sql: ${ms_preferred_language} ;;
  }

  measure: vis_ms_country_of_residence {
    label: "Unique Country of Residence"
    hidden: no
    type: max
    sql: ${ms_country_of_residence} ;;
  }

  measure: vis_table_score {
    label: "Quality Table Score %"
    hidden: no
    value_format: "0.0%"
    type: max
    sql: IFNULL(${customer_row_score}/NULLIF(${full_possible_score}, 0), 0) ;;
  }

  measure: vis_table_score_0_20 {
    label: "% Customers with Score % 0-20"
    hidden: no
    value_format: "0.0%"
    type: max
    sql: IFNULL(${customer_row_score_20}/NULLIF(${count_unique_tb_key}, 0), 0) ;;
  }

  measure: vis_table_score_20_50 {
    label: "% Customers with Score % 20-50"
    hidden: no
    value_format: "0.0%"
    type: max
    sql: IFNULL(${customer_row_score_20_50}/NULLIF(${count_unique_tb_key}, 0), 0) ;;
  }

  measure: vis_table_score_50_70 {
    label: "% Customers with Score % 50-70"
    hidden: no
    value_format: "0.0%"
    type: max
    sql: IFNULL(${customer_row_score_50_70}/NULLIF(${count_unique_tb_key}, 0), 0) ;;
  }

  measure: vis_table_score_70_90 {
    label: "% Customers with Score % 70-90"
    hidden: no
    value_format: "0.0%"
    type: max
    sql: IFNULL(${customer_row_score_70_90}/NULLIF(${count_unique_tb_key}, 0), 0) ;;
  }

  measure: vis_table_score_90 {
    label: "% Customers with Score % 90-100"
    hidden: no
    value_format: "0.0%"
    type: max
    sql: IFNULL(${customer_row_score_90}/NULLIF(${count_unique_tb_key}, 0), 0) ;;
  }
  measure: vis_table_score_50_70_count {
    label: "Count Customers with Score % 50-70"
    hidden: no
    type: max
    sql: IFNULL(${customer_row_score_50_70}, 0) ;;
  }

  measure: vis_table_score_70_90_count {
    label: "Count Customers with Score % 70-90"
    hidden: no
    type: max
    sql: IFNULL(${customer_row_score_70_90}, 0) ;;
  }

  measure: vis_table_score_90_count {
    label: "Count Customers with Score % 90-100"
    hidden: no
    type: max
    sql: IFNULL(${customer_row_score_90}, 0) ;;
  }

  measure: vis_table_score_50_100_count {
    label: "Count Customers with Score % 50-100"
    hidden: no
    type: max
    sql: IFNULL(${customer_row_score_50_70}, 0) +
    IFNULL(${customer_row_score_70_90}, 0) +
    IFNULL(${customer_row_score_90}, 0);;
  }

}
