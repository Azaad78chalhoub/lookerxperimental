view: redemptions_product {
  sql_table_name: `chb-prod-data-cust.prod_MUSE_ControlTower.redemptions_product`
    ;;


  dimension: pk {
    type: string
    hidden: yes
    primary_key: yes
    sql: CONCAT(CAST(${bit_reference} AS STRING),CAST(${member_id} AS STRING))  ;;

  }

  dimension_group: bit {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.bit_date ;;
  }

  dimension: bit_reference {
    label: "Product Bit Reference"
    type: string
    sql: ${TABLE}.bit_reference ;;
  }

  dimension: currency_code {
    type: string
    sql: ${TABLE}.currency_code ;;
  }

  dimension: location_code {
    type: string
    sql: ${TABLE}.location_code ;;
  }

  dimension: location_name {
    type: string
    sql: ${TABLE}.location_name ;;
  }

  dimension: market {
    hidden: yes
    type: string
    sql: ${TABLE}.market ;;
  }

  dimension: market_redeeming {
    label: "Product Redeeming Market"
    description: "Derived from currency code"
    hidden: no
    type: string
    sql: case when ${currency_code}='AED' then 'United Arab Emirates'
        when ${currency_code}='KWD' then 'Kuwait'
        when ${currency_code}='SAR' then 'Saudi Arabia'
        else ${currency_code}
        end;;
  }

  dimension: member_id {
    type: number
    hidden: yes
    sql: ${TABLE}.member_id ;;
  }

  dimension: points_burned {
    type: number
    sql: ${TABLE}.points_burned ;;
  }

  measure: sum_points_burned {
    type: sum
    sql_distinct_key: ${pk} ;;
    label: "Burned Points (SUM)"
    view_label: "Redemptions Product"
    description: "Points redeemed"
    value_format: "#,##0"
    sql: ${points_burned};;
  }


  measure: sum_topup_spend  {
    sql_distinct_key: ${pk} ;;
    label: "Topup Spend USD (SUM)"
    view_label: "Redemptions Product"
    description: "Topup Spend"
    hidden: no
    type: sum
    value_format: "$#,##0;(#.00)"
    sql: CASE WHEN ${currency_code} = "AED" THEN ${topup_spend}/3.67
              WHEN ${currency_code} = "KWD" THEN ${topup_spend}*3.25
              WHEN ${currency_code} = "SAR" THEN ${topup_spend}/3.75
              ELSE ${topup_spend}
         END;;
  }


  dimension_group: process {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.process_date ;;
  }

  dimension_group: redemption {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.redemption_date ;;
  }

  dimension: sku_quantity {
    type: number
    sql: ${TABLE}.sku_quantity ;;
  }

  dimension: sponsor {
    type: string
    sql: UPPER(${TABLE}.sponsor) ;;
  }


  dimension: topup_spend {
    type: number
    sql: ${TABLE}.top_spend ;;
  }

  dimension: total_spend {
    type: number
    sql: ${TABLE}.total_spend ;;
  }

  dimension: value_of_points_burned {
    type: number
    sql: ${TABLE}.value_of_points_burned ;;
  }

  measure: redemptions_count {
    type: count_distinct
    sql: ${bit_reference};;
  }

}
