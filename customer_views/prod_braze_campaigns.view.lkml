view: prod_braze_campaigns {
  view_label: "Braze"
  sql_table_name: `chb-prod-data-cust.prod_braze.prod_braze_campaigns`
    ;;

  dimension: pk {
    type: string
    hidden: yes
    primary_key: yes
    sql: ${TABLE}.event_id ;;

  }

  dimension: app_click {
    type: number
    sql: ${TABLE}.app_click ;;
  }

  dimension: app_impression {
    type: number
    sql: ${TABLE}.app_impression ;;
  }

  dimension_group: app_impression {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.app_impression_date ;;
  }

  dimension: bounce {
    type: number
    sql: ${TABLE}.bounce ;;
  }

  dimension: campaign_id {
    type: string
    sql: ${TABLE}.campaign_id ;;
  }

  dimension: campaign_name {
    type: string
    sql: ${TABLE}.campaign_name ;;
  }

  dimension_group: event_send {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.event_send_date ;;
  }

  dimension_group: event_open {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.event_open_date ;;
  }

  dimension_group: event_click {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.event_click_date ;;
  }

  dimension: campaign_type {
    type: string
    sql: ${TABLE}.campaign_type ;;
  }

  dimension: canvas_name {
    type: string
    sql: ${TABLE}.canvas_name ;;
  }

  dimension: canvas_step_name {
    type: string
    sql: ${TABLE}.canvas_step_name ;;
  }

  dimension: channel_android_push {
    type: yesno
    sql: ${TABLE}.channel_android_push ;;
  }

  dimension: channel_email {
    type: yesno
    sql: ${TABLE}.channel_email ;;
  }

  dimension: channel_ios_push {
    type: yesno
    sql: ${TABLE}.channel_ios_push ;;
  }

  dimension: channel_web_push {
    type: yesno
    sql: ${TABLE}.channel_web_push ;;
  }

  dimension: channel_webhook {
    type: yesno
    sql: ${TABLE}.channel_webhook ;;
  }

  dimension: clicks_email {
    type: number
    sql: ${TABLE}.clicks_email ;;
  }

  dimension: conversion {
    type: number
    sql: ${TABLE}.conversion ;;
  }

  dimension_group: created {
    label: "Campaign Created Date"
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.created_at ;;
  }

  dimension: custom_event_name {
    type: string
    sql: ${TABLE}.custom_event_name ;;
  }

  dimension: email {
    type: string
    sql: ${TABLE}.email ;;
  }

  dimension: email_click {
    type: number
    sql: ${TABLE}.email_click ;;
  }

  dimension: email_delivery {
    type: number
    sql: ${TABLE}.email_delivery ;;
  }

  dimension: email_markasspam {
    type: number
    sql: ${TABLE}.email_markasspam ;;
  }

  dimension: email_opened {
    type: number
    sql: ${TABLE}.email_opened ;;
  }

  dimension: email_softbounce {
    type: number
    sql: ${TABLE}.email_softbounce ;;
  }

  dimension: email_unsubscribe {
    type: number
    sql: ${TABLE}.email_unsubscribe ;;
  }

  dimension: external_user_id {
    type: string
    sql: ${TABLE}.external_user_id ;;
  }

  dimension: makes_any_purchase {
    type: number
    sql: ${TABLE}.makes_any_purchase ;;
  }

  dimension: makes_specific_purchase {
    type: number
    sql: ${TABLE}.makes_specific_purchase ;;
  }

  dimension: open {
    type: number
    sql: ${TABLE}.open ;;
  }

  dimension: performs_custom_event {
    type: number
    sql: ${TABLE}.performs_custom_event ;;
  }

  dimension: schedule_type {
    type: string
    sql: ${TABLE}.schedule_type ;;
  }

  dimension: send {
    type: number
    sql: ${TABLE}.send ;;
  }

  dimension: specific_purchase_product_name {
    type: string
    sql: ${TABLE}.specific_purchase_product_name ;;
  }

  dimension: starts_session {
    type: number
    sql: ${TABLE}.starts_session ;;
  }

  dimension: tag_dont_use {
    type: string
    sql: ${TABLE}.tag_dont_use ;;
  }

  dimension: tag_elite {
    type: string
    sql: ${TABLE}.tag_elite ;;
  }

  dimension: tag_promotion {
    type: string
    sql: ${TABLE}.tag_promotion ;;
  }

  dimension: tag_region {
    type: string
    sql: ${TABLE}.tag_region ;;
  }

  dimension: tag_tactical_campaign {
    type: string
    sql: ${TABLE}.tag_tactical_campaign ;;
  }

  dimension: tag_tracking {
    type: string
    sql: ${TABLE}.tag_tracking ;;
  }

  dimension: tag_trigger {
    type: string
    sql: ${TABLE}.tag_trigger ;;
  }

  dimension: user_id {
    type: string
    sql: ${TABLE}.user_id ;;
  }

  measure: count_events {
    type: count_distinct
    sql: ${pk} ;;
  }


}
