view: date_mapper {
  derived_table: {
    sql: select 'x' as source_date, 'y' as target_records ;;
  }

  dimension: source_date {
    hidden: yes
  }

  dimension: target_records  {
    hidden: yes
  }
}







view: sales_unified_comparative {
  sql_table_name: `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr`
    ;;







  dimension: id {
    label: "Transaction Item ID"
    hidden: yes
    description: "System generated unique ID for table row. i.e. transaction & item combination."
    primary_key: yes
    type: string

    sql: ${TABLE}.surrogate_key_generated_uid ;;
  }


  dimension: date_mapper_target {
    type: string
    sql: case when 1=1 then 'y' else 'y' end;;
  }


  dimension: amountlocal_beforetax {
    hidden: yes
    type: number
    sql: ${TABLE}.amountlocal_beforetax ;;
  }

  dimension: amountusd_beforetax {
    hidden: yes
    type: number
    sql: ${TABLE}.amountusd_beforetax ;;
  }



  dimension: atr_cginvoiceno {
    hidden: yes
    type: string
    sql: ${TABLE}.atr_cginvoiceno ;;
  }

  dimension: atr_cginvoicenoline {
    hidden: yes
    type: string
    sql: ${TABLE}.atr_cginvoicenoline ;;
  }

  dimension_group: atr_createdate {
    hidden: yes
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.atr_createdate ;;
  }

  dimension: atr_day {
    hidden: yes
    type: number
    sql: ${TABLE}.atr_day ;;
  }

  dimension_group: atr_documentdate {
    hidden: yes
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.atr_documentdate ;;
  }

  dimension: atr_homecurrency {
    hidden: yes
    type: string
    sql: ${TABLE}.atr_homecurrency ;;
  }

  dimension: atr_itemseqno {
    hidden: yes
    type: string
    sql: ${TABLE}.atr_itemseqno ;;
  }

  dimension: atr_itemstatus {
    hidden: yes
    type: string
    sql: ${TABLE}.atr_itemstatus ;;
  }


  dimension: atr_muse_id {
    hidden: yes
    type: string
    sql: ${TABLE}.atr_muse_id ;;
  }

  dimension: atr_promotionid {
    hidden: yes
    type: number
    value_format_name: id
    sql: ${TABLE}.atr_promotionid ;;
  }

  dimension: atr_promotionid2 {
    hidden: yes
    type: number
    sql: ${TABLE}.atr_promotionid2 ;;
  }

  dimension: atr_salesperson {
    hidden: yes
    type: string
    sql: ${TABLE}.atr_salesperson ;;
  }

  dimension: atr_status {
    hidden: yes
    type: string
    sql: ${TABLE}.atr_status ;;
  }

  dimension: atr_storedayseqno {
    hidden: yes
    type: number
    sql: ${TABLE}.atr_storedayseqno ;;
  }

  dimension: atr_timezone {
    hidden: yes
    type: number
    sql: ${TABLE}.atr_timezone ;;
  }

  dimension_group: atr_trandate {
    label: "Comparative Range"
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.atr_trandate ;;
  }


  dimension: date_start {
    hidden: yes

    type: date
    sql: {% date_start atr_trandate_date %} ;;
  }


  dimension: date_end {
    hidden: yes

    type: date
    sql: {% date_end atr_trandate_date %} ;;
  }



  dimension: date_mapper_target2 {
    type: string
    sql: case when ${atr_trandate_date}>= ${date_start} and ${atr_trandate_date}<=${date_end} then 'y' else 'a' end ;;
  }



  dimension: atr_tranno {
    hidden: yes
    type: string
    sql: ${TABLE}.atr_tranno ;;
  }

  dimension: atr_transeqno {
    hidden: yes
    type: string
    sql: ${TABLE}.atr_transeqno ;;
  }

  dimension: atr_trantype {
    hidden: yes
    type: string
    sql: ${TABLE}.atr_trantype ;;
  }

  dimension_group: atr_updatedate {
    hidden: yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.atr_updatedate ;;
  }

  dimension: av_cost_local {
    hidden: yes
    type: number
    sql: ${TABLE}.av_cost_local ;;
  }

  dimension: av_cost_usd {
    hidden: yes
    type: number
    sql: ${TABLE}.av_cost_usd ;;
  }

  dimension: bk_brand {
    hidden: yes
    type: number
    sql: ${TABLE}.bk_brand ;;
  }

  dimension: bk_businessdate {
   value_format: "0"
    type: number
    sql: ${TABLE}.bk_businessdate ;;
  }

  dimension: bk_customer {
    hidden: yes
    type: string
    sql: ${TABLE}.bk_customer ;;
  }


  dimension: bk_productid {
    hidden: yes
    type: string
    sql: ${TABLE}.bk_productid ;;
  }

  dimension: bk_salestype {
    hidden: yes
    type: number
    sql: ${TABLE}.bk_salestype ;;
  }

  dimension: bk_storeid {
    hidden: yes
    type: number
    value_format_name: id
    sql: ${TABLE}.bk_storeid ;;
  }

  dimension: brand_customer_unique_id {
    hidden: yes
    type: string
    sql: ${TABLE}.brand_customer_unique_id ;;
  }

  dimension: crm_customer_id {
    hidden: yes
    type: string
    sql: ${TABLE}.crm_customer_id ;;
  }

  dimension: cust_email {
    hidden: yes
    type: string
    sql: ${TABLE}.cust_email ;;
  }

  dimension: cust_phone_number {
    hidden: yes
    type: string
    sql: ${TABLE}.cust_phone_number ;;
  }



  dimension: customer_unique_id {
    hidden: yes
    type: number
    sql: ${TABLE}.customer_unique_id ;;
  }

  dimension: data_source {
    hidden: yes
    type: string
    sql: ${TABLE}.data_source ;;
  }

  dimension: discountamt_local {
    hidden: yes
    type: number
    sql: ${TABLE}.discountamt_local ;;
  }

  dimension: discountamt_usd {
    hidden: yes
    type: number
    sql: ${TABLE}.discountamt_usd ;;
  }

  dimension: is_generic_customer {
    hidden: yes
    type: yesno
    sql: ${TABLE}.is_generic_customer ;;
  }

  dimension: mea_amountlocal {
    hidden: yes
    type: number
    sql: ${TABLE}.mea_amountlocal ;;
  }

  dimension: mea_amountusd {
    hidden: yes
    type: number
    sql: ${TABLE}.mea_amountusd ;;
  }

  dimension: mea_quantity {
    hidden: yes
    type: number
    sql: ${TABLE}.mea_quantity ;;
  }

  dimension: min_brand_id {
    hidden: yes
    type: number
    sql: ${TABLE}.min_brand_id ;;
  }

  dimension: new_cust_brand {

    hidden: yes
    type: string
    sql: ${TABLE}.new_cust_brand ;;
  }

  dimension: new_cust_group {
    hidden: yes
    type: string
    sql: ${TABLE}.new_cust_group ;;
  }

  dimension: pos_customer_id {
    hidden: yes
    type: string
    sql: ${TABLE}.pos_customer_id ;;
  }

  dimension: register {
    hidden: yes
    type: string
    sql: ${TABLE}.register ;;
  }

  dimension: sales_channel {
    hidden: yes
    type: string
    sql: ${TABLE}.sales_channel ;;
  }

  dimension: surrogate_key {
    hidden: yes
    type: number
    sql: ${TABLE}.surrogate_key ;;
  }

  dimension: surrogate_key_generated_uid {
    hidden: yes
    type: string
    sql: ${TABLE}.surrogate_key_generated_uid ;;
  }

  dimension: taxamt_local {
    hidden: yes
    type: number
    sql: ${TABLE}.taxamt_local ;;
  }

  dimension: taxamt_usd {
    hidden: yes
    type: number
    sql: ${TABLE}.taxamt_usd ;;
  }

  dimension: xstore_register {
    hidden: yes
    type: number
    sql: ${TABLE}.xstore_register ;;
  }


  measure: Sales_Amount_USD {
    type: sum

    value_format: "$#,##0;($#.00)"
    sql: ${amountusd_beforetax}

     ;;
  }

  measure: count {
    hidden: yes
    type: count
    drill_fields: []
  }
}
