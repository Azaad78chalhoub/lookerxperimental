view: dim_calendar {
  label: "Calendar"
  sql_table_name: `chb-prod-data-cust.prod_Customer360.dim_date`
    ;;








  parameter: retention_period{
    label: "3 - Retention Period Select"
    view_label: "Retention Rate"

    description: "This selects the date range priod to the selected transaction dates as the basis for customer retention rate.
    e.g. selecting 30 days will calculate custoemrs who shopped in the current selected transaction date range and shopped in the preceding 30 days."
    type: string
    allowed_value: {
      label: "Custom"
      value: "-1"
    }
    allowed_value: {
      label: "Reletive to seleted range"
      value: "0"
    }
    allowed_value: {
      label: "Shoped in preceeding 30 days"
      value: "30"
    }
    allowed_value: {
      label: "Shoped in preceeding 60 days"
      value: "60"
    }
    allowed_value: {
      label: "Shoped in preceeding 90 days"
      value: "90"
    }
    allowed_value: {
      label: "Shoped in preceeding 6 months"
      value: "183"
    }
    allowed_value: {
      label: "Shoped in preceeding 12 months"
      value: "365"
    }
    default_value: "0"
  }



  parameter: retention_period_offset {
    hidden: yes
    description: "Use this to offset the retained from period by 1 year prior. e.g. to see customer in 2021 who are retained from 2019"
    allowed_value: {
      label: "No Offset"
      value: "0"
    }
    allowed_value: {
      label: "-1 year"
      value: "1"
    }
    default_value: "0"
  }

  dimension: retention_year_off {
    type: number
    hidden: yes
    sql: cast({% parameter retention_period_offset %} as int64) ;;
  }

  dimension: retention_comparison_range {
    hidden: yes
    type: number

    sql: case when {% parameter retention_period %} ="0" then
        DATE_DIFF(${dim_calendar.selected_end}, ${dim_calendar.selected_start}, DAY)
        else cast({% parameter retention_period %} as int64) end;;
  }



#   dimension: retained_customer_redundant {
# hidden: yes
#     label: "Is Retained Customer"
#     group_label: "Retention Rate"
#     description: "Indicates if customer is retained from the previous period. Use Retention Period parameter in Customer view to select the period for retention."

#     type: yesno
#     sql: ${brand_customer_unique_id} in
#       (
#       select brand_customer_unique_id from `chb-prod-data-cust.prod_base_customers_final.base_customers_distinct`

#                           where  brand_customer_unique_id in (

#                             select brand_customer_unique_id
#                                   from
#                                   `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr`
#                                   where
#                                   atr_trandate >=
#                                   date_add(
#                                   (date_add(${base_customers_factretailsales.selected_start_date}, INTERVAL -1*(${retention_comparison_range}) DAY)), INTERVAL -1*(${retention_year_off}) YEAR)
#                                   and atr_trandate<
#                                   date_add(
#                                   ${base_customers_factretailsales.selected_start_date}, INTERVAL -1*(${retention_year_off}) YEAR)

#     )
#     );;


# }


  parameter: retention_range_start {
    label: "4 - Retention Start Date (custom)"
    view_label: "Retention Rate"


    type: date

  }

  parameter: retention_range_end {
    label: "5 - Retention Start Date (custom)"
    view_label: "Retention Rate"


    type: date

  }


  dimension: ret_range_start {
    hidden: yes
    type: date
    sql: case when
        {% parameter retention_period %} ="-1" then cast({% parameter retention_range_start %} as date)
        else date_add( ${dim_calendar.selected_start},INTERVAL -1*(${retention_comparison_range}) DAY)
      end;;
  }

  dimension: ret_range_end {
    hidden: yes
    type: date
    sql: case when
        {% parameter retention_period %} ="-1" then cast({% parameter retention_range_end %} as date)
        else  ${dim_calendar.selected_start}
      end;;
  }




  dimension: ret_range_start_int {
    description: "Helper dimension for retention rate. Exposed for testing only"
    value_format: "0"
    type: number
    sql:  CAST (FORMAT_DATE('%Y%m%d',(${ret_range_start})) as INT64) ;;
  }



  dimension: ret_range_end_int {
    description: "Helper dimension for retention rate. Exposed for testing only"
    value_format: "0"
    type: number
    sql:  CAST (FORMAT_DATE('%Y%m%d',(${ret_range_end})) as INT64) ;;
  }



dimension: selected_granularity {
  hidden: yes
  type: number
  sql:  {% if calendar_month._in_query  %}  1

   {% elsif calendar_quarter._in_query  %}  2


   {% elsif cal_half_year._in_query  %}  3


     {% elsif calendar_year._in_query  %}  4

        {% else %} 0 {% endif %} ;;
}

parameter: custom_comparative_dates {
  hidden: yes
  label: "Custom Date Offset"
  description: "To specify a particular previous point in time, select Custom in the Comparative Period filter."
  type: date
}


  parameter: comparitive_period_offset {
    hidden: yes

    description: "Use this to offset the retained from period by 1 year prior. e.g. to see customer in 2021 who are retained from 2019"
    allowed_value: {
      label: "No Offset"
      value: "0"
    }
    allowed_value: {
      label: "-1 year"
      value: "1"
    }
    default_value: "0"
  }




  dimension: custom_start {
hidden: yes

    type: date
    sql: {% date_start custom_comparative_dates %} ;;
  }





  dimension: selected_start {
    hidden: yes

    type: date
    sql: {% date_start calendar_date %} ;;
  }

dimension: selected_start_int {
  hidden: yes
  type: number
  sql:  CAST (FORMAT_DATE('%Y%m%d',(${selected_start})) as INT64) ;;

}

  dimension: selected_end {
    hidden: yes
    type: string
    sql: case when IFNULL({% date_end calendar_date %},current_date())>=current_date() then current_date()


          else {% date_end calendar_date %} end ;;
  }


  dimension: comparative_range {
    hidden: yes
    type: number

    sql:  DATE_DIFF(${selected_end}, ${selected_start}, DAY);;
  }




  dimension: relative_period_end {

    hidden: yes
    value_format: "0"
    type: number
    sql:  CAST (FORMAT_DATE('%Y%m%d',${selected_start} ) as INT64);;

  }

  dimension: relative_period_start {
    hidden: yes

    value_format: "0"
    type: number
    sql:

     CAST (FORMAT_DATE('%Y%m%d',(DATE_ADD (${selected_start}, INTERVAL -1*${comparative_range}  DAY))) as INT64)
    ;;
  }



  dimension: businessdate_offset {
    hidden: yes
    type: number
    value_format: "0"
    sql:


             CAST ((FORMAT_DATE('%Y%m%d',
          DATE_ADD  (PARSE_DATE("%Y%m%d", cast(${dim_date_key} as string)), INTERVAL -1*${comparative_range} DAY))) as INT64)
  ;;
  }







  dimension: dim_date_key {
    label: "Date Key"
    primary_key: yes
    type: number
    sql: ${TABLE}.dim_date_key ;;
  }

  dimension: bk_businessdate {
  hidden: yes
    type: number
    sql: ${TABLE}.dim_date_key ;;
  }


  dimension: _fivetran_deleted {
    hidden: yes
    type: yesno
    sql: ${TABLE}._fivetran_deleted ;;
  }

  dimension_group: _fivetran_synced {
    hidden: yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}._fivetran_synced ;;
  }






  dimension: cal_day_in_month {
    group_label: "Calendar Attributes"
    type: number
    sql: ${TABLE}.cal_day_in_month ;;
  }

  dimension: cal_day_in_week {
    group_label: "Calendar Attributes"
    type: string
    sql: ${TABLE}.cal_day_in_week ;;
  }

  dimension: cal_day_in_week_no {
    group_label: "Calendar Attributes"
    type: number
    sql: ${TABLE}.cal_day_in_week_no ;;
  }

  dimension: cal_day_in_year {
    group_label: "Calendar Attributes"
    type: number
    sql: ${TABLE}.cal_day_in_year ;;
  }

  dimension: cal_month {
    group_label: "Calendar Attributes"

    type: number
    sql: ${TABLE}.cal_month ;;
  }

  dimension: cal_month_name {
    group_label: "Calendar Attributes"
    type: string
    sql: ${TABLE}.cal_month_name ;;
  }

  dimension: cal_month_no {
    group_label: "Calendar Attributes"
    type: number
    sql: ${TABLE}.cal_month_no ;;
  }

  dimension: cal_month_year {
    group_label: "Calendar Attributes"
    type: string
    sql: ${TABLE}.cal_month_year ;;
  }

  dimension: cal_quarter {
    group_label: "Calendar Attributes"
    type: number
    sql: ${TABLE}.cal_quarter ;;
  }

  dimension: cal_quarter_name {
    group_label: "Calendar Attributes"
    type: string
    sql: ${TABLE}.cal_quarter_name ;;
  }

  dimension: cal_quarter_no {
    group_label: "Calendar Attributes"
    type: number
    sql: ${TABLE}.cal_quarter_no ;;
  }

  dimension: cal_quarter_year {
    group_label: "Calendar Attributes"
    type: string
    sql: ${TABLE}.cal_quarter_year ;;
  }

  dimension: cal_half_year {
    group_label: "Calendar Attributes"
    type: string
    sql: concat(case when ${cal_month_no}<7 then '1H ' else '2H ' end , cast(${cal_year} as string));;
  }

  dimension: cal_week {
    group_label: "Calendar Attributes"
    type: number
    sql: ${TABLE}.cal_week ;;
  }

  dimension: cal_week_in_year {
    group_label: "Calendar Attributes"
    type: number
    sql: ${TABLE}.cal_week_in_year ;;
  }

  dimension: cal_week_name {
    group_label: "Calendar Attributes"
    type: string
    sql: ${TABLE}.cal_week_name ;;
  }

  dimension: cal_week_year {
    group_label: "Calendar Attributes"
    type: string
    sql: ${TABLE}.cal_week_year ;;
  }

  dimension: cal_year {
    group_label: "Calendar Attributes"}

  dimension_group: calendar {
    type: time

    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.calendar_date ;;
  }

  dimension: calendar_start {
    hidden: yes

    type: date
    sql: {% date_start calendar_date %} ;;
  }


  dimension: calendar_end {
    hidden: yes

    type: date
    sql: {% date_end calendar_date %} ;;
  }



  dimension: date_mapper_source2 {
    hidden: yes
    type: string
    sql: case when ${calendar_date}>= ${calendar_start} and ${calendar_date}<=${calendar_end} then 'x' else 'a' end ;;
  }

  dimension: filterdate {
    hidden: yes
    type: string
    sql: ${TABLE}.filterdate ;;
  }

  dimension: filterdatesort {
    hidden: yes
    type: number
    sql: ${TABLE}.filterdatesort ;;
  }


  dimension: date_prev_month{
    hidden: yes
    type: number



  }



  dimension: holiday_desc {
    group_label: "Calendar Attributes"
    type: string
    sql: ${TABLE}.holiday_desc ;;
  }

  dimension: holiday_flag {
    group_label: "Calendar Attributes"
    type: string
    sql: ${TABLE}.holiday_flag ;;
  }

  dimension_group: update {
    hidden: yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.update_time ;;
  }

  dimension: week_day_flag {
    group_label: "Calendar Attributes"
    type: string
    sql: ${TABLE}.week_day_flag ;;
  }

  dimension: week_end_flag {
    group_label: "Calendar Attributes"
    type: string
    sql: ${TABLE}.week_end_flag ;;
  }

  measure: count {
    hidden: yes
    type: count
    drill_fields: [cal_quarter_name, cal_month_name, cal_week_name]
  }
}
