view: adjust_levelshoes {
  sql_table_name: `chb-prod-data-cust.prod_Customer360.adjust_levelshoes`;;
  view_label: "Adjust App"

  dimension: id {
    primary_key: yes
    hidden: yes
    type: string
    sql: CONCAT(${brand_customer_unique_id}, ${order_id});;
  }

  dimension: brand_customer_unique_id {
    type: string
    sql: ${TABLE}.brand_customer_unique_id ;;
  }

  dimension: customer_unique_id {
    type: number
    sql: ${TABLE}.customer_unique_id ;;
  }

  dimension: activity_kind {
    type: string
    sql: ${TABLE}.activity_kind ;;
  }

  dimension: adid {
    type: string
    sql: ${TABLE}.adid ;;
  }

  dimension: connection_type {
    type: string
    sql: ${TABLE}.connection_type ;;
  }

  dimension: currency {
    type: string
    sql: ${TABLE}.currency ;;
  }

  dimension: device_name {
    type: string
    sql: ${TABLE}.device_name ;;
  }

  dimension: event_name {
    type: string
    sql: ${TABLE}.event_name ;;
  }

  dimension: idfa {
    type: string
    sql: ${TABLE}.idfa ;;
  }

  dimension: idfv {
    type: string
    sql: ${TABLE}.idfv ;;
  }

  dimension: isp {
    type: string
    sql: ${TABLE}.isp ;;
  }

  dimension: language {
    type: string
    sql: ${TABLE}.language ;;
  }

  dimension: network_name {
    type: string
    sql: ${TABLE}.network_name ;;
  }

  dimension: order_id {
    type: string
    sql: ${TABLE}.order_id ;;
  }

  dimension: original_order_id {
    type: string
    sql: ${TABLE}.original_order_id ;;
  }

  dimension: os_name {
    type: string
    sql: ${TABLE}.os_name ;;
  }

  dimension_group: transaction_datetime {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.transaction_datetime ;;
  }

  measure: count {
    type: count
    drill_fields: [device_name, network_name, event_name, os_name]
  }
}
