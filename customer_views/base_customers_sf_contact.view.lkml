view: base_customers_sf_contact {
    sql_table_name:`chb-prod-data-cust.prod_base_customers_final.base_customers_sf_contact`;;

    dimension: salesforce_contact_id {
      label: "Salesforce Contact ID  (Beta)"
      view_label: "Customers"
      group_label: "IDs and Keys"
      type: string
      sql: ${TABLE}.salesforce_contact_id;;
    }

    dimension: brand_id {
      hidden: yes
      type: string
      sql: ${TABLE}.brand_id;;
    }

    dimension: customer_unique_id {
      hidden: yes
      type: string
      sql: ${TABLE}.customer_unique_id;;
    }

    dimension: brand_customer_unique_id {
      hidden: yes
      type: string
      sql: ${TABLE}.brand_customer_unique_id;;
    }

    dimension: salesforce_account_id {
      hidden: yes
      type: string
      sql: ${TABLE}.salesforce_account_id;;
    }

}
