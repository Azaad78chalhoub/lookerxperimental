view: test_date_filter {

  derived_table: {    explore_source: factretailsales {

      column: id {field:factretailsales.id}

      column: Transaction_Count {field:factretailsales.Transaction_Count}
      column: sales_amount_usd {field:factretailsales.Sales_Amount_USD}
      column: customer_count { field: base_customers.customer_count}
      column: atr_cginvoiceno {field: factretailsales.atr_cginvoiceno}



      bind_filters: {from_field: locations.district_name
        to_field: locations.district_name}
}

}
dimension: id {}

  dimension: Transaction_Count {
    label:"xxxxxxxxxxxxxxxxxx"}


  dimension: atr_cginvoiceno {
    hidden: yes
  }

  filter: test_period_filter {
    group_label: "Date Test"
    type: date
  }

  measure: invoice_count {
    type: count_distinct
    sql: ${atr_cginvoiceno} ;;
  }



}
