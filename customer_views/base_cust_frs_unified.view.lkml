include: "../_common/period_over_period_flexible.view"

view: base_cust_frs_unified {
  view_label: "Retail Sales"
  sql_table_name:  `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr`



        ;;
  drill_fields: [id]

  dimension: id {
    hidden: yes
    primary_key: yes

    type: string


    sql: ${TABLE}.surrogate_key_generated_uid ;;
  }

  dimension: brand_customer_unique_id {
    hidden: yes
    type: string
    sql: ${TABLE}.brand_customer_unique_id ;;
  }
  dimension: _fivetran_deleted {
    hidden: yes
    type: yesno
    sql: ${TABLE}._fivetran_deleted ;;
  }

  dimension_group: _fivetran_synced {
    hidden: yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}._fivetran_synced ;;
  }

  dimension: amountlocal_beforetax {
    hidden: yes
    type: number
    sql: ${TABLE}.amountlocal_beforetax ;;
  }

  dimension: amountusd_beforetax {
    hidden: yes
    type: number
    sql: ${TABLE}.amountusd_beforetax ;;
  }


  dimension: atr_activitytype {
    hidden: yes
    type: string
    sql: ${TABLE}.atr_activitytype ;;
  }

  dimension: atr_batchid {
    hidden: yes
    type: string
    sql: ${TABLE}.atr_batchid ;;
  }

  dimension: atr_cginvoiceno {
    hidden: yes
    type: string
    sql: ${TABLE}.atr_cginvoiceno ;;
  }

  dimension: atr_cginvoicenoline {
    hidden: yes
    type: string
    sql: ${TABLE}.atr_cginvoicenoline ;;
  }

  dimension_group: atr_createdate {
    hidden: yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.atr_createdate ;;
  }

  dimension: atr_day {
    hidden: yes
    type: number
    sql: ${TABLE}.atr_day ;;
  }

  dimension_group: atr_documentdate {
    hidden: yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.atr_documentdate ;;
  }



  dimension: atr_homecurrency {
    hidden: yes
    type: string
    sql: ${TABLE}.atr_homecurrency ;;
  }

  dimension: atr_itemseqno {
    hidden: yes
    type: number
    sql: ${TABLE}.atr_itemseqno ;;
  }



  dimension: atr_membershipid {
    hidden: yes
    type: string
    sql: ${TABLE}.atr_membershipid ;;
  }

  dimension: atr_muse_id {
    hidden: yes
    type: string
    sql: ${TABLE}.atr_muse_id ;;
  }



  dimension: atr_salesperson {
    label: "SalesPerson Staff ID"
    hidden: no
    type: string
    sql: ${TABLE}.atr_salesperson ;;
  }



  dimension: atr_storedayseqno {
    hidden: yes
    type: number
    sql: ${TABLE}.atr_storedayseqno ;;
  }

  dimension: atr_time {
    hidden: yes
    type: number
    sql: ${TABLE}.atr_time ;;
  }

  dimension: atr_timezone {
    hidden: yes
    type: string
    sql: ${TABLE}.atr_timezone ;;
  }

  extends: [period_over_period_flexible]

  dimension_group: pop_no_tz{
    sql: ${atr_trandate_date} ;;
  }

  dimension_group: atr_trandate {
    label: "Transaction"
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.atr_trandate ;;
  }


  dimension: date_group {
    label: "Date Group"
    hidden: yes
    sql: {% if base_cust_frs_unified.atr_trandate_date._in_query %} CAST(${TABLE}.atr_trandate AS DATE)
        {% elsif base_cust_frs_unified.atr_trandate_month._in_query %} FORMAT_DATE('%Y-%m', ${TABLE}.atr_trandate )
        {% elsif base_cust_frs_unified.atr_trandate_quarter._in_query %} (FORMAT_TIMESTAMP('%Y-%m', TIMESTAMP_TRUNC(CAST(${TABLE}.atr_trandate AS TIMESTAMP), QUARTER)))
        {% elsif base_cust_frs_unified.atr_trandate_week._in_query %} CONCAT(CAST(EXTRACT(YEAR FROM ${TABLE}.atr_trandate ) AS STRING),'-', CAST(EXTRACT(WEEK(SUNDAY) FROM ${TABLE}.atr_trandate) AS STRING))
        {% elsif base_cust_frs_unified.atr_trandate_year._in_query %} EXTRACT(YEAR FROM ${TABLE}.atr_trandate )
        {% else %} CAST(${TABLE}.atr_trandate  AS DATE)
        {% endif %} ;;
  }



  dimension: atr_tranno {
    hidden: yes
    type: number
    sql: ${TABLE}.atr_tranno ;;
  }

  dimension: atr_transeqno {
    hidden: yes
    type: number
    sql: ${TABLE}.atr_transeqno ;;
  }

  dimension: atr_trantype {
    hidden: yes
    type: string
    sql: ${TABLE}.atr_trantype ;;
  }

  dimension_group: atr_updatedate {
    hidden: yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.atr_updatedate ;;
  }

  dimension: av_cost_local {
    hidden: yes
    type: number
    sql: ${TABLE}.av_cost_local ;;
  }

  dimension: av_cost_usd {
    hidden: yes
    type: number
    sql: ${TABLE}.av_cost_usd ;;
  }

  dimension: bk_brand {
    hidden: yes
    type: number
    sql: ${TABLE}.bk_brand ;;
  }

  dimension: bk_businessdate {
    hidden: yes
    type: number
    sql: ${TABLE}.bk_businessdate ;;
  }

  dimension: bk_customer {
    hidden: yes
    type: string
    sql: ${TABLE}.bk_customer ;;
  }

  dimension: bk_loyalty {
    hidden: yes
    type: string
    sql: ${TABLE}.bk_loyalty ;;
  }

  dimension: bk_loyaltyprogram {
    hidden: yes
    type: string
    sql: ${TABLE}.bk_loyaltyprogram ;;
  }

  dimension: bk_productid {
    hidden: yes
    label:"Product ID"
    type: string
    sql: ${TABLE}.bk_productid ;;
  }

  dimension: bk_salestype {

    hidden: yes
    type: number
    sql: ${TABLE}.bk_salestype ;;
  }

  dimension: bk_storeid {
    label: "Store ID"
    hidden: yes
    type: number
    value_format_name: id
    sql: ${TABLE}.bk_storeid ;;
  }





  dimension: consignment_flag {
    hidden: yes
    type: string
    sql: ${TABLE}.consignment_flag ;;
  }

  dimension: conversion_rate {
    hidden: yes
    type: number
    sql: ${TABLE}.CONVERSION_RATE ;;
  }

  dimension_group: creationdatetime {
    hidden: yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.creationdatetime ;;
  }

  dimension: crm_customer_id {
    hidden: yes
    type: string
    sql: ${TABLE}.crm_customer_id ;;
  }

  dimension: customer_golden_id {
    hidden: yes
    type: string
    sql: ${TABLE}.customer_golden_id ;;
  }

  dimension: discountamt_local {
    hidden: yes

    type: number
    sql: ${TABLE}.discountamt_local ;;
  }

  dimension: discountamt_usd {
    hidden: yes
    type: number
    sql: ${TABLE}.discountamt_usd ;;
  }

  dimension: customer_unique_id {
    hidden: yes
    type: number
    sql: ${TABLE}.customer_unique_id ;;
  }

  dimension: mea_amountlocal {
    hidden: yes
    type: number
    sql: ${TABLE}.mea_amountlocal ;;
  }

  dimension: mea_amountusd {
    hidden: yes
    type: number
    sql: ${TABLE}.mea_amountusd ;;
  }

  dimension: mea_quantity {
    hidden: yes
    type: number
    sql: ${TABLE}.mea_quantity ;;
  }

  dimension: min_brand_id {
    hidden: yes
    type: number
    sql: ${TABLE}.min_brand_id ;;
  }

  dimension: pos_customer_id {
    hidden: yes
    type: string
    sql: ${TABLE}.pos_customer_id ;;
  }

  dimension: register {
    hidden: yes
    type: string
    sql: ${TABLE}.register ;;
  }

  dimension: sales_channel {
    description:"For sales not through usual channel. e.g WhatsApp, Concierge etc."
    type: string
    sql: ${TABLE}.sales_channel ;;
  }

  dimension: taxamt_local {
    hidden: yes
    type: number
    sql: ${TABLE}.taxamt_local ;;
  }

  dimension: taxamt_usd {
    hidden: yes
    type: number
    sql: ${TABLE}.taxamt_usd ;;
  }

  dimension: is_discount_sale {
    hidden: yes
    type: yesno
    sql: ifnull(${discountamt_usd},0)<>0 ;;
  }

  dimension: is_muse_txn{
    hidden: yes
    label: "Is Muse Txn"
    type: yesno
    sql: ${atr_muse_id} is not null;;
  }
  dimension: is_crm_txn{
    hidden: yes
    label: "Is CRM Txn"
    type: yesno
    sql:  ${crm_customer_id} is not null ;;
  }

  dimension: is_known_cust_txn{

    description: "Sale attributed to a known customer (i.e. CRM or Muse)"
    type: yesno
    sql:  ${customer_unique_id} is not null ;;
  }


  dimension: is_generic_customer   {
    label: "Is Generic ID Txn"
    type: yesno
    sql: ${TABLE}.is_generic_customer ;;
  }


  dimension: is_muse_signup_txn {
    description: "Is transaction associated with a Muse Member Sign-Up"
    type: yesno
    sql: case when ${transaction_attributes.is_muse_regi_txn} is null then false else true end ;;
  }

  dimension: xstore_register {
    label: "Xstore Register"
    type: number
    sql: ${TABLE}.xstore_register ;;
  }

  dimension: sales_channel_xstore {
    label: "Sales Channel"
    type: string
    sql: ${TABLE}.sales_channel ;;
  }

  measure: count_of_discount_sales{
    hidden: yes
    type: count_distinct
    sql: ${atr_cginvoiceno} ;;

    filters: {
      field: is_discount_sale
      value: "yes"  }
  }

  measure: proportion_of_discount_sales{
    description: "Percentage of transactions which included a discount"
    type: number
    value_format: "0.00%"
    sql: (${Transaction_Count}-${count_of_discount_sales})/NULLIF(${count_of_discount_sales},0) ;;
  }

  measure: count {
    hidden: yes
    type: count
    drill_fields: [id]
  }

  measure: Item_Count  {
    description: "Aggregate quantity of units sold"
    type: sum
    sql: ${mea_quantity} ;;
  }

  measure:  Transaction_Count{
    description: "Purchases, Returns, or Exchanges made by a customer in a store or online"
    type: count_distinct
    sql: ${atr_cginvoiceno} ;;
  }

  measure: Sales_Amount_USD {
    type: sum
    description: "Net sales is the sum of gross sales minus its returns, allowances, and discounts (without VAT)"
    value_format: "$#,##0;($#.00)"
    sql: ${mea_amountusd} ;;
  }
  measure: UPT {
    group_label: "Basket Measures"
    label: "UPT"
    value_format: "0.##"
    description: "Average units per transaction"
    type: number
    sql: ${Item_Count}/nullif(${Transaction_Count},0) ;;
  }

  measure: VPT {
    group_label: "Basket Measures"
    label: "AOV"
    value_format: "$0"
    description: "Average Value per Order"
    type: number
    sql: ${Sales_Amount_USD}/nullif(${Transaction_Count},0) ;;
  }

  measure: customer_key_count {
    group_label: "Customer Related"
    label: "Customer Unique Count (Transacted)"
    description: "A person who buys goods across brands part of Chalhoub Group"
    hidden: no
    type: count_distinct
    sql:  ${TABLE}.customer_unique_id;;
  }

  measure:  Known_Cust_Tran_Count{
    hidden: yes
    type: count_distinct
    sql: case when ${brand_customer_unique_id} is null then null else ${atr_cginvoiceno} end;;

  }

  measure: customer_key_count_RFM {

    group_label: "Customer Related"
    label: "Customer Count for RFM Analysis ONLY"
    hidden: yes
    type: count_distinct
    drill_fields: [Sales_Amount_USD]
    sql:  ${TABLE}.brand_customer_unique_id;;
    html:{% if base_customers_sales_unified.Sales_Amount_USD._value < 20000 %}
          <div style="background-color: rgba(240,248,255,{{value}}); font-size:100%; text-align:center"><a href="{{ link }}" style="color: black" target="_new">{{ customer_key_count_RFM._rendered_value }} </a></div>
          {% elsif base_customers_sales_unified.Sales_Amount_USD._value >= 20000 and base_customers_sales_unified.Sales_Amount_USD._value < 60000 %}
          <div style="background-color: rgba(173,216,230,{{value}}); font-size:100%; text-align:center"><a href="{{ link }}" style="color: white" target="_new">{{ customer_key_count_RFM._rendered_value }} </a></div>
           {% elsif base_customers_sales_unified.Sales_Amount_USD._value >= 60000 and base_customers_sales_unified.Sales_Amount_USD._value < 100000 %}
          <div style="background-color: rgba(135,206,250,{{value}}); font-size:100%; text-align:center"><a href="{{ link }}" style="color: white" target="_new">{{ customer_key_count_RFM._rendered_value }} </a></div>
            {% elsif base_customers_sales_unified.Sales_Amount_USD._value >= 100000 and base_customers_sales_unified.Sales_Amount_USD._value < 400000 %}
          <div style="background-color: rgba(30,144,255,{{value}}); font-size:100%; text-align:center"><a href="{{ link }}" style="color: white" target="_new">{{ customer_key_count_RFM._rendered_value }} </a></div>
           {% elsif base_customers_sales_unified.Sales_Amount_USD._value >= 400000 and base_customers_sales_unified.Sales_Amount_USD._value < 800000 %}
          <div style="background-color: rgba(65,105,225,{{value}}); font-size:100%; text-align:center"><a href="{{ link }}" style="color: white" target="_new">{{ customer_key_count_RFM._rendered_value }} </a></div>
         {% elsif base_customers_sales_unified.Sales_Amount_USD._value >= 800000 and base_customers_sales_unified.Sales_Amount_USD._value < 1100000 %}
          <div style="background-color: rgba(0,0,255,{{value}}); font-size:100%; text-align:center"><a href="{{ link }}" style="color: white" target="_new">{{ customer_key_count_RFM._rendered_value }} </a></div>
      {% elsif base_customers_sales_unified.Sales_Amount_USD._value >= 1100000 and base_customers_sales_unified.Sales_Amount_USD._value < 3000000 %}
          <div style="background-color: rgba(25,35,150,{{value}}); font-size:100%; text-align:center"><a href="{{ link }}" style="color: white" target="_new">{{ customer_key_count_RFM._rendered_value }} </a></div>
      {% elsif base_customers_sales_unified.Sales_Amount_USD._value >= 3000000 and base_customers_sales_unified.Sales_Amount_USD._value < 6000000 %}
          <div style="background-color: rgba(0,0,128,{{value}}); font-size:100%; text-align:center"><a href="{{ link }}" style="color: white" target="_new">{{ customer_key_count_RFM._rendered_value }} </a></div>
       {% else %}
          <div style="background-color: rgba(0,0,128,0.99); font-size:100%; text-align:center"><a href="{{ link }}" style="color: white" target="_new">{{ customer_key_count_RFM._rendered_value }} </a></div>
          {% endif %}
          ;;}

      measure: customer_CRM_count {
        group_label: "Customer Related"
        hidden: yes
        label: "CRM member Unique Count (Transacted)"

        type: count_distinct
        sql:  ${TABLE}.crm_customer_id ;;
      }



      measure: Average_Frequency {
        group_label: "Customer Related"
        description: "Purchase frequency is the average number of purchases made by a customer over a given period of time"
        type: number
        value_format: "0.##"
        sql: ${Known_Cust_Tran_Count}/NULLIF(${customer_key_count},0) ;;
      }



      measure: AUR{
        group_label: "Basket Measures"
        label: "AUR"
        value_format: "$0"
        description: "Average value per item"
        type: number
        sql: ${Sales_Amount_USD}/NULLIF(${Item_Count},0) ;;
      }


      measure: know_customer_sales {
        hidden: yes
        group_label: "Customer Related"
        description: "Sales attributed to a known customer"
        value_format: "$#,##0"
        type: sum
        sql:
              CASE
                WHEN nullif(${TABLE}.brand_customer_unique_id,'') is null then 0 else ${mea_amountusd}

              END ;;
      }

      measure: unknow_sales {
        hidden: yes
        group_label: "Customer Related"
        value_format: "$#,##0"
        description: "Sales not linked to a known customer"

        type: sum
        sql:
              CASE
                WHEN nullif(${TABLE}.brand_customer_unique_id,'') is null then ${mea_amountusd} else 0

              END ;;
      }

      measure: percentage_known_sales{
        hidden: yes
        description: "Known customer sales (excl. generic) / Total sales"
        group_label: "Customer Related"
        type: number
        value_format: "0.00%"
        sql:  ${know_customer_sales}/nullif(${Sales_Amount_USD},0};;
      }

      measure: average_spend {
        group_label: "Customer Related"
        description: "Average Net sales for a given customer over a given period of time"
        type: number
        value_format: "$#,##0"
        sql: ${know_customer_sales}/nullif(${customer_key_count},0) ;;
      }

      measure: muse_sales {
        group_label: "Customer Related"
        description: "Sales attributed to Muse member"
        type: sum
        value_format: "$#,##0"
        sql: case when ${atr_muse_id} is not null then ${mea_amountusd}
          else 0 end ;;

      }

      measure: non_muse_sales {
        group_label: "Customer Related"
        description: "Non-MUSE Sales"
        type: number
        hidden: no
        value_format: "$#,##0"
        sql:  nullif(${Sales_Amount_USD},0) - nullif(${muse_sales},0) ;;
      }

      measure: muse_sales_penetration {
        group_label: "Customer Related"
        description: "MUSE Sales Penetration"
        type: number
        hidden: no
        value_format: "0.00%"
        sql: ${muse_sales}/nullif((${muse_sales}+${non_muse_sales}), 0) ;;
      }




      measure: min_tran_date {
        hidden: yes
        type: date
        sql: min(cast(${TABLE}.atr_createdate as timestamp)) ;;
      }


    }
