include: "../_common/period_over_period_flexible.view"

view: factretailsales {
  label: "Sales Fact"
  sql_table_name: `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr`
    ;;
  drill_fields: [id]

  dimension: id {

    primary_key: yes
    hidden: yes
    type: number
    sql: ${TABLE}.surrogate_key ;;
  }

  dimension: _fivetran_deleted {
    type: yesno
    hidden: yes
    sql: ${TABLE}._fivetran_deleted ;;
  }

  dimension_group: _fivetran_synced {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}._fivetran_synced ;;
  }

  dimension: amountlocal_beforetax {
    hidden: yes
    type: number
    sql: ${TABLE}.amountlocal_beforetax ;;
  }

  dimension: amountusd_beforetax {
    hidden: yes
    type: number
    sql: ${TABLE}.amountusd_beforetax ;;
  }

  dimension: atr_action {
    hidden: yes
    type: string
    sql: ${TABLE}.atr_action ;;
  }

  dimension: atr_activitytype {
    hidden: yes
    type: string
    sql: ${TABLE}.atr_activitytype ;;
  }

  dimension: atr_batchid {
    hidden: yes
    type: string
    sql: ${TABLE}.atr_batchid ;;
  }

  dimension: atr_cginvoiceno {
    hidden: yes
    type: string
    sql: ${TABLE}.atr_cginvoiceno ;;
  }

  dimension: atr_cginvoicenoline {
    hidden: yes
    type: string
    sql: ${TABLE}.atr_cginvoicenoline ;;
  }

  dimension_group: atr_createdate {
    hidden: yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.atr_createdate ;;
  }

  dimension: atr_day {
    hidden: yes
    type: number
    sql: ${TABLE}.atr_day ;;
  }

  dimension_group: atr_documentdate {
    hidden: yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.atr_documentdate ;;
  }

  dimension: atr_errordesc {
    hidden: yes
    type: string
    sql: ${TABLE}.atr_errordesc ;;
  }

  dimension: atr_homecurrency {
    hidden: yes
    type: string
    sql: ${TABLE}.atr_homecurrency ;;
  }

  dimension: atr_itemseqno {
    hidden: yes
    type: number
    sql: ${TABLE}.atr_itemseqno ;;
  }

  dimension: atr_itemstatus {
    hidden: yes
    type: string
    sql: ${TABLE}.atr_itemstatus ;;
  }

  dimension: atr_membershipid {
    hidden: yes
    type: string
    sql: ${TABLE}.atr_membershipid ;;
  }

  dimension: atr_muse_id {
    hidden: yes
    type: string
    sql: ${TABLE}.atr_muse_id ;;
  }

  dimension: atr_promotionid {
    hidden: yes
    type: number
    value_format_name: id
    sql: ${TABLE}.atr_promotionid ;;
  }

  dimension: atr_promotionid2 {
    hidden: yes
    type: number
    sql: ${TABLE}.atr_promotionid2 ;;
  }

  dimension: atr_salesperson {
    label: "SalesPerson Staff ID"

    type: string
    sql: ${TABLE}.atr_salesperson ;;
  }

  dimension: atr_stagestatus {
    hidden: yes
    type: string
    sql: ${TABLE}.atr_stagestatus ;;
  }

  dimension: atr_status {
    hidden: yes
    type: string
    sql: ${TABLE}.atr_status ;;
  }

  dimension: atr_storedayseqno {
    hidden: yes
    type: number
    sql: ${TABLE}.atr_storedayseqno ;;
  }

  dimension: atr_time {
    hidden: yes
    type: number
    sql: ${TABLE}.atr_time ;;
  }

  dimension: atr_timezone {
    hidden: yes
    type: string
    sql: ${TABLE}.atr_timezone ;;
  }

  extends: [period_over_period_flexible]

  dimension_group: pop_no_tz{
    sql: ${atr_trandate_date} ;;
  }

  dimension_group: atr_trandate {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.atr_trandate ;;
  }


dimension: tran_date {
  type: date
  sql: ${TABLE}.atr_trandate;;
}

  dimension: atr_tranno {
    hidden: yes
    type: number
    sql: ${TABLE}.atr_tranno ;;
  }

  dimension: atr_transeqno {
    hidden: yes
    type: number
    sql: ${TABLE}.atr_transeqno ;;
  }

  dimension: atr_trantype {
    hidden: yes
    type: string
    sql: ${TABLE}.atr_trantype ;;
  }

  dimension_group: atr_updatedate {
    hidden: yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.atr_updatedate ;;
  }

  dimension: av_cost_local {
    hidden: yes
    type: number
    sql: ${TABLE}.av_cost_local ;;
  }

  dimension: av_cost_usd {
    hidden: yes
    type: number
    sql: ${TABLE}.av_cost_usd ;;
  }

  dimension: bk_brand {
    hidden: yes
    type: number
    sql: ${TABLE}.bk_brand ;;
  }

  dimension: bk_businessdate {
    hidden: yes
    type: number
    sql: ${TABLE}.bk_businessdate ;;
  }

  dimension: bk_customer {
    hidden: no
    type: string
    sql: ${TABLE}.bk_customer ;;
  }

  dimension: bk_loyalty {
    hidden: yes
    type: string
    sql: ${TABLE}.bk_loyalty ;;
  }

  dimension: bk_loyaltyprogram {
    hidden: yes
    type: string
    sql: ${TABLE}.bk_loyaltyprogram ;;
  }

  dimension: bk_productid {
    hidden: yes
    type: string
    sql: ${TABLE}.bk_productid ;;
  }

  dimension: bk_salestype {
    hidden: yes
    type: number
    sql: ${TABLE}.bk_salestype ;;
  }

  dimension: bk_storeid {
    hidden: yes
    type: number
    value_format_name: id
    sql: ${TABLE}.bk_storeid ;;
  }

  dimension: bk_top20 {
    hidden: yes
    type: string
    sql: ${TABLE}.bk_top20 ;;
  }

  dimension: brand_customer_unique_id {
    hidden: no
    type: string
    sql: ${TABLE}.brand_customer_unique_id ;;
  }

  dimension: consignment_flag {
    hidden: yes
    type: string
    sql: ${TABLE}.consignment_flag ;;
  }

  dimension: conversion_rate {
    hidden: yes
    type: number
    sql: ${TABLE}.CONVERSION_RATE ;;
  }

  dimension_group: creationdatetime {
    hidden: yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.creationdatetime ;;
  }

  dimension: crm_customer_id {
    hidden: yes
        type: string
    sql: ${TABLE}.crm_customer_id ;;
  }

  dimension: customer_golden_id {
    hidden: yes
    type: string
    sql: ${TABLE}.customer_golden_id ;;
  }

  dimension: discountamt_local {
    hidden: yes
    type: number
    sql: ${TABLE}.discountamt_local ;;
  }

  dimension: discountamt_usd {
    hidden: yes
    type: number
    sql: ${TABLE}.discountamt_usd ;;
  }

  dimension: customer_unique_id {
    hidden: yes
    type: number
    sql: ${TABLE}.customer_unique_id ;;
  }

  dimension: mea_amountlocal {
    hidden: yes
    type: number
    sql: ${TABLE}.mea_amountlocal ;;
  }

  dimension: mea_amountusd {
    hidden: yes
    type: number
    sql: ${TABLE}.mea_amountusd ;;
  }

  dimension: mea_quantity {
    hidden: yes
    type: number
    sql: ${TABLE}.mea_quantity ;;
  }

  dimension: min_brand_id {
    hidden: yes
    type: number
    sql: ${TABLE}.min_brand_id ;;
  }

  dimension: pos_customer_id {
    hidden: yes
    type: string
    sql: ${TABLE}.pos_customer_id ;;
  }

  dimension: taxamt_local {
    hidden: yes
    type: number
    sql: ${TABLE}.taxamt_local ;;
  }

  dimension: taxamt_usd {
    hidden: yes
    type: number
    sql: ${TABLE}.taxamt_usd ;;
  }


  dimension: is_discount_sale {
    type: yesno
    sql: ifnull(${discountamt_usd},0)<>0 ;;
  }

  dimension: is_muse_txn{
    type: yesno
    sql: ${atr_muse_id} is not null;;
  }
  dimension: is_crm_txn{
    type: yesno
    sql:  ${crm_customer_id} is not null ;;
  }

  dimension: txn_customer_type {
    type: string
    sql: if (nullif(${atr_muse_id},'') is not null, "Muse", if(
      nullif(${crm_customer_id},'') is not null or  nullif(${pos_customer_id},'') is not null, "CRM", "Unknown")) ;;
  }



  dimension: is_generic_customer   {
    label: "Is Generic ID Txn"
    type: yesno
    sql: ${TABLE}.is_generic_customer ;;
  }


  dimension: is_known_cust_txn{

    description: "Sale attributed to a known customer (i.e. CRM or Muse)"
    type: yesno
    sql:  ${customer_unique_id} is not null ;;
  }

  measure: count_of_discount_sales{
    type: count
    filters: {
      field: is_discount_sale
      value: "yes"  }
  }

  measure: proportion_of_discount_sales{
    description: "Percentage of transactions which included a discount"
    type: number
    value_format: "0.00%"
    sql: (${Transaction_Count}-${count_of_discount_sales})/NULLIF(${count_of_discount_sales},0) ;;
  }

  measure: count {
    hidden: yes
    type: count
    drill_fields: [id]
  }


  parameter: IR_Sales_Filter {
    view_label: "Sales Fact"
    description: "Choose the period you would like to compare to. Must be used with Current Date Range filter"
    label: "IR Sales Filter"
    type: unquoted
    allowed_value: {
      label: "Muse IR Only"
      value: "Yes"
    }
    allowed_value: {
      label: "All Sales"
      value: "No"
    }
    }

  measure: Item_Count  {
    type: sum
    sql: ${mea_quantity} ;;
  }

  measure:  Transaction_Count{
    type: count_distinct
    sql: ${atr_cginvoiceno} ;;
  }

  measure:  Known_Cust_Tran_Count{
    type: count_distinct
    sql: case when ${brand_customer_unique_id} is null then null else ${atr_cginvoiceno} end;;

  }


  measure:  brand_visits {
    description: "How many brands the customer visited"
    type: count_distinct
    sql: concat( cast(${atr_trandate_date} as string),cast(${min_brand_id} as string)) ;;
  }



  measure: Sales_Amount_USD {
    type: sum
    value_format: "$#,##0.00;($#.00)"
    sql: ${mea_amountusd} ;;
  }
  measure: UPT {
    label: "UPT"
    value_format: "0.##"
    description: "Average number of units per transaction"
    type: number
    sql: ${Item_Count}/nullif(${Transaction_Count},0) ;;
  }

  measure: VPT {
    label: "AOV"
    value_format: "$0.##"
    description: "Average order value / average basket value"
    type: number
    sql: ${Sales_Amount_USD}/nullif(${Transaction_Count},0) ;;
  }

 measure: customer_key_count {
    label: "Customer Unique Count (Transacted)"
    hidden: no
    type: count_distinct
    sql:  ${TABLE}.customer_unique_id;;
  }

  measure: brand_customer_key_count {
    label: "Brand Customer Unique Count (Transacted)"
    hidden: no
    type: count_distinct
    sql:  ${TABLE}.brand_customer_unique_id;;
  }

  measure: member_visits {
    label: "Member Unique Visits (distinct dates visited)"
    hidden: no
    type: count_distinct
    sql:  concat( cast(${atr_trandate_date} as string), ${atr_muse_id});;
  }






  measure: customer_key_count_RFM {
    label: "Customer Count for RFM Analysis ONLY"
    hidden: no
    type: count_distinct
    drill_fields: [Sales_Amount_USD]
    sql:  ${TABLE}.brand_customer_unique_id;;
    html:{% if factretailsales.Sales_Amount_USD._value < 20000 %}
    <div style="background-color: rgba(240,248,255,{{value}}); font-size:100%; text-align:center"><a href="{{ link }}" style="color: black" target="_new">{{ customer_key_count_RFM._rendered_value }} </a></div>
    {% elsif factretailsales.Sales_Amount_USD._value >= 20000 and factretailsales.Sales_Amount_USD._value < 60000 %}
    <div style="background-color: rgba(173,216,230,{{value}}); font-size:100%; text-align:center"><a href="{{ link }}" style="color: white" target="_new">{{ customer_key_count_RFM._rendered_value }} </a></div>
     {% elsif factretailsales.Sales_Amount_USD._value >= 60000 and factretailsales.Sales_Amount_USD._value < 100000 %}
    <div style="background-color: rgba(135,206,250,{{value}}); font-size:100%; text-align:center"><a href="{{ link }}" style="color: white" target="_new">{{ customer_key_count_RFM._rendered_value }} </a></div>
      {% elsif factretailsales.Sales_Amount_USD._value >= 100000 and factretailsales.Sales_Amount_USD._value < 400000 %}
    <div style="background-color: rgba(30,144,255,{{value}}); font-size:100%; text-align:center"><a href="{{ link }}" style="color: white" target="_new">{{ customer_key_count_RFM._rendered_value }} </a></div>
     {% elsif factretailsales.Sales_Amount_USD._value >= 400000 and factretailsales.Sales_Amount_USD._value < 800000 %}
    <div style="background-color: rgba(65,105,225,{{value}}); font-size:100%; text-align:center"><a href="{{ link }}" style="color: white" target="_new">{{ customer_key_count_RFM._rendered_value }} </a></div>
   {% elsif factretailsales.Sales_Amount_USD._value >= 800000 and factretailsales.Sales_Amount_USD._value < 1100000 %}
    <div style="background-color: rgba(0,0,255,{{value}}); font-size:100%; text-align:center"><a href="{{ link }}" style="color: white" target="_new">{{ customer_key_count_RFM._rendered_value }} </a></div>
{% elsif factretailsales.Sales_Amount_USD._value >= 1100000 and factretailsales.Sales_Amount_USD._value < 3000000 %}
    <div style="background-color: rgba(25,35,150,{{value}}); font-size:100%; text-align:center"><a href="{{ link }}" style="color: white" target="_new">{{ customer_key_count_RFM._rendered_value }} </a></div>
{% elsif factretailsales.Sales_Amount_USD._value >= 3000000 and factretailsales.Sales_Amount_USD._value < 6000000 %}
    <div style="background-color: rgba(0,0,128,{{value}}); font-size:100%; text-align:center"><a href="{{ link }}" style="color: white" target="_new">{{ customer_key_count_RFM._rendered_value }} </a></div>
 {% else %}
    <div style="background-color: rgba(0,0,128,0.99); font-size:100%; text-align:center"><a href="{{ link }}" style="color: white" target="_new">{{ customer_key_count_RFM._rendered_value }} </a></div>
    {% endif %}
    ;;}

 measure: customer_CRM_count {
    hidden: no
    type: count_distinct
    sql:  ${TABLE}.crm_customer_id ;;
  }

  measure: muse_key_count {
    label: "Muse member Unique Count (Transacted)"
    hidden: no
    type: count_distinct
    sql:  ${TABLE}.atr_muse_id ;;
  }

  measure: Average_Frequency {
    description: "Average number of time shopped per customer in the given period"
    type: number
    value_format: "0.##"
    sql: ${Known_Cust_Tran_Count}/NULLIF(${customer_key_count},0) ;;
  }

  measure: AUR{
    label: "AUR"
    value_format: "$0.##"
    description: "Average unit retail price"
    type: number
    sql: ${Sales_Amount_USD}/NULLIF(${Item_Count},0) ;;
  }


  measure: know_customer_sales {
    description: "Sales attributed to a known customer"
    value_format: "$#,##0"
    type: sum
    sql:
      CASE
        WHEN nullif(${brand_customer_unique_id},'') is null then 0 else ${mea_amountusd}

      END ;;
  }

  measure: unknow_sales {
    description: "Sales not lonked to a known customer"
    value_format: "$0.##"
    type: sum
    sql:
      CASE
        WHEN nullif(${brand_customer_unique_id},'') is null then ${mea_amountusd} else 0

      END ;;
  }

  measure: percentage_known_sales{
    type: number
    value_format: "0.00%"
    sql:  ${know_customer_sales}/nullif(${Sales_Amount_USD},0);;
  }

  measure: average_spend {
    description: "Total Sales / Customer Count"
    type: number
    value_format: "$#,##0"
    sql: ${know_customer_sales}/nullif(${customer_key_count},0) ;;
  }

  measure: muse_sales {
    description: "Sales attributed to Muse ID only."
    type: sum
    value_format: "$#,##0"
    sql: case when ${atr_muse_id} is not null then ${mea_amountusd}
    else 0 end ;;

  }



  measure: non_muse_sales {
    description: "Non-MUSE Sales"
    type: number
    hidden: no
    value_format: "$#,##0"
    sql:  nullif(${Sales_Amount_USD},0) - nullif(${muse_sales},0) ;;
  }


  measure: muse_sales_penetration {
    description: "MUSE Sales Penetration (%)"
    type: number
    hidden: no
    value_format: "0%"
    sql:  nullif(${muse_sales},0)/nullif(${Sales_Amount_USD},0) ;;

  }


  measure: pre_muse_sales{
  hidden:yes
    type: sum
    sql: case when ${atr_trandate_date}<'2019-08-25' then ${mea_amountusd} else 0 end;;
  }

  measure: muse_ir_sales {
    label: "Muse IR Sales"
    description: "Post Muse launch, only Muse related sales are included"
    value_format: "$#,##0"
    type: number
    sql: ${pre_muse_sales}+${muse_sales} ;;

  }

  measure: min_tran_date {
    type: date
    sql: min(cast(${TABLE}.atr_createdate as timestamp)) ;;
  }



  measure: cross_brand_sales_pre_muse_test{
    group_label: "Cross Brand Analysis"
    type: min
    value_format: "$#,##0"

    sql: (select sum(mea_amountusd)
        from
        (
        select distinct frs.atr_cginvoiceno,
        frs.mea_amountusd, frs.atr_cginvoicenoline,frs.id

        from  `chb-prod-data-cust.prod_base_customers_final.base_customers_factretailsales` as frs
        inner join `chb-prod-supplychain-data.prod_shared_dimensions.dim_retail_loc` as loc
        on frs.bk_storeid=loc.store
        and frs.min_brand_id<>${TABLE}.min_brand_id
        and frs.customer_unique_id=${TABLE}.customer_unique_id
        and frs.bk_businessdate >=20180825 and frs.bk_businessdate <20190825
        ));;
        }

}
