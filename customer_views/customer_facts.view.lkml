# If necessary, uncomment the line below to include explore_source.

# include: "customer_360.model"




view: new_to_store_pp_ndt {
  derived_table: {
    explore_source: base_customers_factretailsales {
      column: customer_unique_id {field:returning_cust_dyn_pp.customer_unique_id}
      column: selected_start_int {field: dim_calendar.selected_start_int}

      bind_filters: { to_field: returning_cust_dyn_pp.district_name
        from_field:locations.district_name }

      bind_filters: {to_field:returning_cust_dyn_pp.country_desc
        from_field:locations.country_desc}

      bind_filters: {to_field:returning_cust_dyn_pp.store_name
        from_field:locations.store_name}

      bind_filters: {to_field:returning_cust_dyn_pp.is_muse_store
        from_field:locations.is_muse_store}


      bind_filters: { to_field: dim_calendar.calendar_date
        from_field: dim_calendar.calendar_date}
      bind_filters: { to_field: dim_calendar.calendar_month
        from_field: dim_calendar.calendar_month}
      bind_filters: { to_field: dim_calendar.calendar_quarter
        from_field: dim_calendar.calendar_quarter}
      bind_filters: { to_field: dim_calendar.calendar_year
        from_field: dim_calendar.calendar_year}


    }}


  dimension: customer_unique_id {
    hidden: yes
  }
  dimension: selected_start_int{
    hidden: yes
  }

  measure: count_previously_shopped {
    description: "Generalised version of vew vs returing. Works with any combintaion of brands, stores & counties fro Locations view"
    label: "Returning Customer_Count (General)"
    view_label: "Retail Sales"
    group_label: "Customer Related"
    type: count_distinct
    sql: ${customer_unique_id} ;;
  }



  }



view: new_to_store_ndt {
  derived_table: {
    explore_source: base_customers_factretailsales {
      column: max_new_to_store {field:base_customers_factretailsales.max_new_to_store}
      column: customer_unique_id {field:returning_cust_dyn.customer_unique_id}

      bind_filters: { to_field: returning_cust_dyn.district_name
        from_field:locations.district_name }

      bind_filters: {to_field:returning_cust_dyn.country_desc
        from_field:locations.country_desc}

      bind_filters: {to_field:returning_cust_dyn.store_name
        from_field:locations.store_name}

      bind_filters: {to_field:returning_cust_dyn.is_muse_store
        from_field:locations.is_muse_store}




      bind_filters: { to_field: dim_calendar.calendar_date
        from_field: dim_calendar.calendar_date}
      bind_filters: { to_field: dim_calendar.calendar_month
        from_field: dim_calendar.calendar_month}
      bind_filters: { to_field: dim_calendar.calendar_quarter
        from_field: dim_calendar.calendar_quarter}
      bind_filters: { to_field: dim_calendar.calendar_year
        from_field: dim_calendar.calendar_year}



}}

dimension: max_new_to_store{}
dimension: customer_unique_id {}


measure: count_new_cust_dyn {
  type: count_distinct
  sql: case when ${max_new_to_store}='new' then ${customer_unique_id} else null end ;;


}

    measure: count_returning_cust_dyn {
      type: count_distinct
      sql: case when ${max_new_to_store}='returning' then ${customer_unique_id} else null end ;;


    }


}


view: prev_period_ndt {
  derived_table: {
    explore_source: base_customers_factretailsales {
      column: customer_unique_id_prev {field:customer_day_store.customer_unique_id_prev}
      column:  bk_businessdate {field:customer_day_store.bk_businessdate}
      column: next_month {field:customer_day_store.next_month}
      column: next_month_year {field:customer_day_store.next_month_year}
      column: next_quarter {field:customer_day_store.next_quarter}
      column: next_quarter_year {field:customer_day_store.next_quarter_year}
      column: next_half {field:customer_day_store.next_half}
      column: next_half_year {field:customer_day_store.next_half_year}
      column: next_year {field:customer_day_store.next_year}

      column: anykey {field:customer_day_store.anykey}

      bind_filters: { to_field: customer_day_store.district_name
        from_field:locations.district_name }

      bind_filters: {to_field:customer_day_store.country_desc
        from_field:locations.country_desc}

      bind_filters: {to_field:customer_day_store.store_name
        from_field:locations.store_name}

      bind_filters: {to_field:customer_day_store.is_muse_store
        from_field:locations.is_muse_store}

      bind_filters: {to_field:customer_day_store.is_muse_txn
        from_field:base_customers_factretailsales.is_muse_txn}



      bind_filters: { to_field: dim_calendar.calendar_date
        from_field: dim_calendar.calendar_date}
      bind_filters: { to_field: dim_calendar.calendar_month
        from_field: dim_calendar.calendar_month}
      bind_filters: { to_field: dim_calendar.calendar_quarter
        from_field: dim_calendar.calendar_quarter}
      bind_filters: { to_field: dim_calendar.calendar_year
        from_field: dim_calendar.calendar_year}



    }
  }

  dimension: customer_unique_id_prev {

  }
  dimension: anykey {}
  dimension: bk_businessdate {}
  dimension: next_month{}
  dimension:next_month_year{}
  dimension:next_quarter{}
  dimension:next_quarter_year{}
  dimension:next_half{}
  dimension:next_half_year{}
  dimension:next_year {}



  measure: count_prev_cust {
    type: count_distinct
    sql: ${customer_unique_id_prev} ;;
  }
}





view: retention_rate_ndt {
  derived_table: {
    explore_source: base_customers_factretailsales {
      column: customer_unique_id_prev {field:retained_cust.customer_unique_id_prev}
      column:  bk_businessdate {field:retained_cust.bk_businessdate}
      column: next_month {field:retained_cust.next_month}
      column: next_month_year {field:retained_cust.next_month_year}
      column: next_quarter {field:retained_cust.next_quarter}
      column: next_quarter_year {field:retained_cust.next_quarter_year}
      column: next_half {field:retained_cust.next_half}
      column: next_half_year {field:retained_cust.next_half_year}
      column: next_year {field:retained_cust.next_year}

      column: customer_key_count {field:base_customers_factretailsales.customer_key_count}

   bind_filters: { to_field: retained_cust.district_name
                  from_field:locations.district_name }

      bind_filters: {to_field:retained_cust.country_desc
        from_field:locations.country_desc}

      bind_filters: {to_field:retained_cust.store_name
                      from_field:locations.store_name}

      bind_filters: {to_field:retained_cust.is_muse_store
        from_field:locations.is_muse_store}

      bind_filters: {to_field:retained_cust.is_muse_txn
        from_field:base_customers_factretailsales.is_muse_txn}



      bind_filters: { to_field: dim_calendar.calendar_date
        from_field: dim_calendar.calendar_date}
      bind_filters: { to_field: dim_calendar.calendar_month
        from_field: dim_calendar.calendar_month}
      bind_filters: { to_field: dim_calendar.calendar_quarter
        from_field: dim_calendar.calendar_quarter}
      bind_filters: { to_field: dim_calendar.calendar_year
        from_field: dim_calendar.calendar_year}



    }
  }

  dimension: customer_unique_id_prev {

  }
  dimension: bk_businessdate {}
  dimension: next_month{}
  dimension:next_month_year{}
  dimension:next_quarter{}
  dimension:next_quarter_year{}
  dimension:next_half{}
  dimension:next_half_year{}
  dimension:next_year {}



  measure: count_retained_cust {
    type: count_distinct
    sql: ${customer_unique_id_prev} ;;
  }
}


view: prev_period_total_custs {

  derived_table: {
    sql:  select count(distinct brand_unique_customer_id) as cust_count,
    bk_businessdate,
    min_brand_id
    from `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr`
    group by    bk_businessdate,
    min_brand_id;;

    }


dimension: cust_count{}

dimension: bk_buinessdate{}
dimension:min_brand_id{}






}



view: prev_month_tot_cust{
  derived_table: {
    sql:
    SELECT count(distinct brand_customer_unique_id ) as last_month_total,
    min_brand_id,
case when extract(month from PARSE_DATE("%Y%m%d", cast(bk_businessdate as string))) =12 then  (extract(year from PARSE_DATE("%Y%m%d", cast(bk_businessdate as string))))+1 else extract(year from PARSE_DATE("%Y%m%d", cast(bk_businessdate as string))) end as year_next_month,
case when extract(month from PARSE_DATE("%Y%m%d", cast(bk_businessdate as string)))=12 then 1 else (extract(month from PARSE_DATE("%Y%m%d", cast(bk_businessdate as string))))+1 end as next_month


from `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr`
where brand_customer_unique_id is not null
and is_generic_customer=false
group by year_next_month, next_month, min_brand_id ;;
  }

  dimension: some_key {
    hidden: yes
    primary_key: yes
    sql: concat(cast(${min_brand_id} as string, cast(${next_month} as string), cast(${year_next_month} as string)) ;;
  }

  dimension: min_brand_id {
    hidden: yes
  }

  dimension: last_month_total {
    hidden: yes

  }
  dimension: year_next_month {
    hidden:yes
  }
  dimension: next_month {
    hidden:yes
  }
}


view: prev_month_customers{
  derived_table: {
    sql:   select distinct brand_customer_unique_id, extract(year from PARSE_DATE("%Y%m%d", cast(bk_businessdate as string)))as theyear,
extract(month from PARSE_DATE("%Y%m%d", cast(bk_businessdate as string))) as themonth
,case when extract(month from PARSE_DATE("%Y%m%d", cast(bk_businessdate as string))) =12 then  (extract(year from PARSE_DATE("%Y%m%d", cast(bk_businessdate as string))))+1
else extract(year from PARSE_DATE("%Y%m%d", cast(bk_businessdate as string))) end as year_next_month,
case when extract(month from PARSE_DATE("%Y%m%d", cast(bk_businessdate as string)))=12 then 1 else (extract(month from PARSE_DATE("%Y%m%d", cast(bk_businessdate as string))))+1 end as next_month
from
`chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr`
where brand_customer_unique_id is not null

and is_generic_customer=false;;


    }


  dimension: some_key {
    hidden: yes
    primary_key: yes
    sql: concat(cast(${brand_customer_unique_id} as string, cast(${next_month} as string), cast(${year_next_month} as string)) ;;
  }

    dimension: brand_customer_unique_id {
      hidden: yes
    }

    dimension:  year_next_month{
      hidden: yes
    }

  dimension:  next_month{
    hidden: yes
  }
}





view: rolling_retention_rate {
  derived_table: {
      explore_source: base_customers_factretailsales {
        column: Sales_Amount_USD {}
        column: brand_customer_key_count {}
        column: customer_key_count {}
        column: bk_businessdate {}
        column: district_name {field:locations.district_name }
        column: previous_month {}
        column: previous_month_year {}
        column: current_year {}
        column: current_month {}
        column: min_brand_id {}

      }
  }

  dimension: some_key {
    type: string
    hidden: yes
    primary_key: yes
    sql: concat(cast(${TABLE}.min_brand_id as string), cast(${TABLE}.current_year as string), cast(${TABLE}.current_month as string)) ;;
  }



}


view: customer_cross_brand_v2 {
  view_label: "Customers"
  derived_table: {
    explore_source: base_customers_factretailsales_v2 {
      column: customer_unique_id {}
      column: transaction_count {field:base_customers_factretailsales_v2.transaction_count}
      column: sales_amount_usd {field:base_customers_factretailsales_v2.Sales_Amount_USD}
      column: group_transacted_customer_count { field: base_customers_distinct_v2.group_transacted_customer_count}
      column: brand_visits { field: base_customers_factretailsales_v2.brand_visits }

      bind_filters: {from_field: base_customers_factretailsales_v2.atr_trandate_date
        to_field: base_customers_factretailsales_v2.atr_trandate_date}
      bind_filters: {from_field: base_customers_factretailsales_v2.atr_trandate_month
        to_field: base_customers_factretailsales_v2.atr_trandate_month}
      bind_filters: {from_field: base_customers_factretailsales_v2.atr_trandate_quarter
        to_field: base_customers_factretailsales_v2.atr_trandate_quarter}
      bind_filters: {from_field: base_customers_factretailsales_v2.atr_trandate_year
        to_field: base_customers_factretailsales_v2.atr_trandate_year}
      bind_filters: {from_field: base_customers_factretailsales_v2.atr_trandate_week
        to_field: base_customers_factretailsales_v2.atr_trandate_week}
      bind_filters: {from_field: locations.district_name
        to_field: locations.district_name}

    }
  }


  dimension: customer_unique_id {
    primary_key: yes
    hidden: yes

  }


  dimension: brand_visits {
    label: "Number of Brands Shopped"
    description: "The number of group brands a customer has shopped in in the selected transaction period."
    type: number
  }

}







view: prospects {
  derived_table :{
    sql:SELECT distinct brand_customer_unique_id,
      customer_unique_id,
      brand_id,
      brand_registration_date,
      mobile_validated,
      email_validated,
      country_of_residence



        FROM `chb-prod-data-cust.prod_base_customers_final.base_customers_distinct`
        where
        brand_customer_unique_id in
      (
      select distinct brand_customer_unique_id
      from `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr`
      where brand_customer_unique_id is not null ) ;;

    }


    dimension: brand_id {
    }

    dimension: brand_customer_unique_id {
      type: string
      primary_key: yes
      sql: ${TABLE}.brand_customer_unique_id ;;
    }

    measure: prospect_count {
      type: count_distinct
      sql: ${brand_customer_unique_id} ;;
    }

    dimension_group: brand_registration{
      type: time
      timeframes: [
        raw,
        time,
        date,
        week,
        month,
        quarter,
        year
      ]
      sql: ${TABLE}.brand_registration_date;;
    }


    dimension: email_validated {
      hidden: yes
      sql: ${TABLE}.email_validated ;;
    }

    dimension: mobile_validated {
      hidden: yes
      sql: ${TABLE}.mobile_validated ;;
    }


    dimension: contactability {
      type: string
      sql: case when NULLIF(${email_validated},'') is null and  NULLIF(${mobile_validated},'') is null then "Not Contactable"
            when NULLIF(${email_validated},'') is not null and  NULLIF(${mobile_validated},'') is null then "Email Only"
            when NULLIF(${email_validated},'') is null and  NULLIF(${mobile_validated},'') is not null then "Mobile Only"
            else "Email & Mobile" end ;;


      }
    }



view: muse_facts {
  view_label: "Customers"
  derived_table: {


    explore_source: factretailsales {



      column: atr_muse_id {field:factretailsales.atr_muse_id}
      column: member_visits {field:factretailsales.member_visits}
      column: district_name {field: locations.district_name}


        bind_filters: {from_field: factretailsales.atr_trandate_date
          to_field: factretailsales.atr_trandate_date}
        bind_filters: {from_field: factretailsales.atr_trandate_month
          to_field: factretailsales.atr_trandate_month}
        bind_filters: {from_field: factretailsales.atr_trandate_quarter
          to_field: factretailsales.atr_trandate_quarter}
        bind_filters: {from_field: factretailsales.atr_trandate_year
          to_field: factretailsales.atr_trandate_year}
        bind_filters: {from_field: factretailsales.atr_trandate_week
          to_field: factretailsales.atr_trandate_week}

      bind_filters: {from_field: locations.district_name
        to_field: locations.district_name}
    }
  }


  dimension: atr_muse_id {
    hidden: yes
    primary_key: yes

  }

  dimension: customer_count {}

  dimension: member_visits {
    hidden: yes
  }


  dimension: Muse_Visitor_type{
    view_label: "Customers"
    description: "Visitor Type for IR"
    label : "Muse Visitor Type"
    hidden: no
    type: string
    sql: case when ${member_visits}  = 1  then 'Single'
              when ${member_visits} = 2  then 'Twice'
              when ${member_visits} >= 3 and ${member_visits} <=5 then '3 - 5 Visits'
              when ${member_visits} > 5 then ' 5 + Visits'
              else null
             end ;;
  }


}



view: customer_facts {
  view_label: "Customers"
  derived_table: {


    explore_source: factretailsales {



      column: brand_customer_unique_id {field:factretailsales.brand_customer_unique_id}
      column: customer_unique_id {field:factretailsales.customer_unique_id}
      column: Transaction_Count {field:factretailsales.Transaction_Count}
      column: sales_amount_usd {field:factretailsales.Sales_Amount_USD}
      column: customer_count { field: base_customers.customer_count}
      column: first_date {field:base_customers.first_date}
      column: district_name {field: locations.district_name}
      column: brand_visits {field: factretailsales.brand_visits }

      column: tran_date {field:factretailsales.tran_date}
      column: atr_trandate_date  {field:factretailsales.atr_trandate_date }
      column: atr_trandate_week {field:factretailsales.atr_trandate_week }
      column: atr_trandate_month  {field:factretailsales.atr_trandate_month }
      column: atr_trandate_quarter  {field:factretailsales.atr_trandate_quarter }
      column: atr_trandate_year {field:factretailsales.atr_trandate_year }
      derived_column: date {
        sql:
        {% if factretailsales.atr_trandate_date._in_query %} CAST(tran_date  AS DATE)
        {% elsif factretailsales.atr_trandate_month._in_query %} FORMAT_DATE('%Y-%m', tran_date)
        {% elsif factretailsales.atr_trandate_quarter._in_query %} FORMAT_DATE('%Y-%Q', tran_date)
        {% elsif factretailsales.atr_trandate_week._in_query %} EXTRACT(WEEK(MONDAY) FROM tran_date)
        {% else %} EXTRACT(YEAR FROM tran_date )
        {% endif %};;



}




      bind_filters: {from_field: factretailsales.atr_trandate_date
        to_field: factretailsales.atr_trandate_date}
      bind_filters: {from_field: factretailsales.atr_trandate_month
        to_field: factretailsales.atr_trandate_month}
      bind_filters: {from_field: factretailsales.atr_trandate_quarter
        to_field: factretailsales.atr_trandate_quarter}
      bind_filters: {from_field: factretailsales.atr_trandate_year
        to_field: factretailsales.atr_trandate_year}
      bind_filters: {from_field: factretailsales.atr_trandate_week
        to_field: factretailsales.atr_trandate_week}
      bind_filters: {from_field: locations.district_name
        to_field: locations.district_name}

    }
  }


  dimension: customer_count {
    hidden: yes
  }



  dimension :  brand_visits {
    hidden:  yes
  }

  dimension: Transaction_Count {
    group_label: "Tenure Dimensions"
    label:"Customer Frequency"
    type: number
  description: "How many times a customer shopped at the brand. Can be used as frequency in the selected period."
  }

  dimension: first_date {
    hidden: yes
  }

  dimension: Visitor_type{
    view_label: "Customers"
    description: "Visitor Type for IR"
    label : "Visitor Type"
    hidden: no
    type: string
    sql: case when ${brand_visits}  = 1  then 'Single'
              when ${brand_visits} = 2  then 'Repeat'
              when ${brand_visits} >= 3 and ${brand_visits} <=5 then '3 - 5 Visitors'
              when ${brand_visits} > 5 then ' 5 + Visitors'
              else null
             end ;;
  }
  dimension: brand_customer_unique_id  {

    hidden: yes
    primary_key: yes
    label: "Customer brand Hash Key"
  }

dimension:sales_amount_usd  {
  value_format: "$#,##0"
  label: "Customer Spend (Brand)"
  description: "Total spend by (brand level) customer in the period. Can use as filter"
  type: number
}





  measure: cross_brand_sales_pre_muse_golden{
    group_label: "Cross Brand Analysis"
    hidden: yes
    type: sum
    value_format: "$#,##0"
    sql: ${base_cross_brand_sales_pre_muse_golden} ;;

  }




  dimension: base_cross_brand_sales_pre_muse_golden{
    hidden: yes
    type: number


##this measue doesn't really work
    sql: (select sum(mea_amountusd)
        from
        (
        select distinct frs.atr_cginvoiceno,
        frs.mea_amountusd, frs.atr_cginvoicenoline,frs.id
        from
        `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr` as frs
        inner join `chb-prod-supplychain-data.prod_shared_dimensions.dim_retail_loc` as loc
        on frs.bk_storeid=loc.store
        where loc.district_name <>  ${TABLE}.district_name
        and frs.customer_unique_id=${TABLE}.customer_unique_id
        and frs.bk_businessdate >=20180825 and frs.bk_businessdate <20190825
        ));;
  }
##this measue doesn't really work
  measure: cross_brand_sales_post_muse{
    hidden: yes
    group_label: "Cross Brand Analysis"
    type: min

    sql: (select sum(frs.mea_amountusd)
        from  `chb-prod-data-custprod_base_customers_final.base_customers_sales_unified_nvr` as frs
        inner join `chb-prod-supplychain-data.prod_shared_dimensions.dim_retail_loc` as loc
        on frs.bk_storeid=loc.store
        where loc.district_name <>  ${TABLE}.district_name
        and frs.customer_unique_id=${TABLE}.customer_unique_id
        and frs.bk_businessdate >=20190825 and frs.bk_businessdate <20200825

        );;
  }




  dimension: count_brands {
    hidden: yes
    type: number
  }

##this measue doesn't really work
  measure: customer_average_spend {
    hidden: yes
    description: "Average spend in the period of analysis"
    type: average
    value_format: "$#,##0"
    sql:${sales_amount_usd} ;;
    }

##this measue doesn't really work as bk_customer at brand level
  measure: ave_brands_shopped_in_period {
    hidden: yes
    label: "Avg Num Brands Shopped"
    description: "Average number of brands shopped in selected perdiod"
    type: average
    sql: ${count_brands} ;;



  }
  }





view: customer_facts_lifetime {
  view_label: "Customers"
  derived_table: {
    explore_source: factretailsales {
      column: brand_customer_unique_id {}
      column: brand_visits{}
      column: Transaction_Count {field:factretailsales.Transaction_Count}
      column: sales_amount_usd {field:factretailsales.Sales_Amount_USD}
      column: customer_count { field: base_customers.customer_count}
      column: tenure {field: base_customers.tenure}
      column: count_brands { field: locations.count_brands }
      bind_filters: {from_field: locations.district_name
        to_field: locations.district_name}

      }
    }
  dimension: brand_customer_unique_id {
    hidden: yes
    primary_key: yes
    label: "Customers Customer Key"
  }

  dimension: tenure {
    hidden: yes
  }
  dimension: brand_visits {
    hidden: yes
  }


    dimension: Transaction_Count {
      hidden: no
      label:"Customer Frequency LifeTime"
      type: number
      description: "How many times a customer shopped at the brand. Lifetime frequency."
    }

  dimension: frequency_range {
    view_label: "Customers"
    group_label: "Tenure Dimensions"
    label: "Frequency Range"
    hidden: no
    description: "Frequency Range for RFM analysis"
    type: string
    sql: CASE
          WHEN ${Transaction_Count} <= 1 THEN "1"
          WHEN ${Transaction_Count} <= 3 THEN "2-3"
          WHEN ${Transaction_Count} <= 5 THEN "4-5"
          WHEN ${Transaction_Count} <= 7 THEN "6-7"
          WHEN ${Transaction_Count}  <= 10 THEN "8-10"
          WHEN ${Transaction_Count}  > 10 THEN "10+"
          ELSE null
          END;;
  }



  dimension: Visitor_type_life{
    view_label: "Customers"
    description: "Visitor Type for IR"
    label : "Visitor Type lifetime"
    hidden: no
    type: string
    sql: case when ${brand_visits}  = 1  then 'Single'
              when ${brand_visits} = 2  then 'Repeat'
              when ${brand_visits} >= 3 and ${brand_visits} <=5 then '3 - 5 Visitors'
              when ${brand_visits} > 5 then ' 5 + Visitors'
              else null
             end ;;
  }

dimension: customer_time_between_purchases {
  group_label: "Tenure Dimensions"
  description: "Aveareg time between purchases for a customer"
  value_format: "0.##"
  type: number
  sql: ${tenure}/nullif(${Transaction_Count},0) ;;
}

measure: ave_time_between_purchases {
  type: average
  value_format: "0.##"
  sql: ${customer_time_between_purchases} ;;
}

measure: average_tenure {
  description: "Average number of days since first purchase"
  type: average
  value_format: "0.##"
  sql: ${tenure} ;;

}

  }






view: customer_facts_golden {
  view_label: "Customers"
  derived_table: {


    explore_source: factretailsales {



      column: customer_unique_id {field:factretailsales.customer_unique_id}

      column: sales_amount_usd {field:factretailsales.Sales_Amount_USD}

      column: brand_visits {field: factretailsales.brand_visits }
      column: district_name {field: locations.district_name}
      column: tran_date {field:factretailsales.tran_date}
      column: atr_trandate_date  {field:factretailsales.atr_trandate_date }
      column: atr_trandate_week {field:factretailsales.atr_trandate_week }
      column: atr_trandate_month  {field:factretailsales.atr_trandate_month }
      column: atr_trandate_quarter  {field:factretailsales.atr_trandate_quarter }
      column: atr_trandate_year {field:factretailsales.atr_trandate_year }
      derived_column: date {
        sql:
        {% if factretailsales.atr_trandate_date._in_query %} CAST(tran_date  AS DATE)
        {% elsif factretailsales.atr_trandate_month._in_query %} FORMAT_DATE('%Y-%m', tran_date)
        {% elsif factretailsales.atr_trandate_quarter._in_query %} FORMAT_DATE('%Y-%Q', tran_date)
        {% elsif factretailsales.atr_trandate_week._in_query %} EXTRACT(WEEK(MONDAY) FROM tran_date)
        {% else %} EXTRACT(YEAR FROM tran_date )
        {% endif %};;
          }
        }

        }

    dimension: customer_unique_id {
      primary_key: yes
      hidden: yes
    }

dimension: brand_visits {}

  dimension: Visitor_type_golden{
    view_label: "Customers"
    description: "Visitor Type for IR"
    label : "Visitor Type Golden"
    hidden: no
    type: string
    sql: case when ${brand_visits}  = 1  then 'Single'
              when ${brand_visits} = 2  then 'Repeat'
              when ${brand_visits} >= 3 and ${brand_visits} <=5 then '3 - 5 Visitors'
              when ${brand_visits} > 5 then ' 5 + Visitors'
              else null
             end ;;
  }

    dimension: tran_date {}

    dimension: date {type:date
      hidden:yes}


    dimension: Transaction_Count {
      group_label: "Tenure Dimensions"
      label:"Customer Frequency"
      type: number
      description: "How many times a customer shopped at the brand. Can be used as frequency in the selected period."
    }

    dimension: first_date {
      hidden: yes
    }

  dimension: count_brands_shopped_pre_muse_uae {
    group_label: "Cross Brand Analysis (Muse)"
    type: number
    sql: (select count(distinct frs.min_brand_id)
          from  `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr` as frs
          inner join `chb-prod-supplychain-data.prod_shared_dimensions.dim_retail_loc` as loc
          on frs.bk_storeid=loc.store
          inner join `chb-prod-data-cust.prod_Customer360.muse_store_codes` as m
          on loc.store=m.bk_store
          and frs.customer_unique_id=${TABLE}.customer_unique_id
          and frs.bk_businessdate >=20180417 and frs.bk_businessdate <20190825

          );;
  }

  dimension: count_brands_shopped_post_muse_uae {
    group_label: "Cross Brand Analysis (Muse)"
    type: number
    sql: (select count(distinct frs.min_brand_id)
        from  `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr` as frs
        inner join `chb-prod-supplychain-data.prod_shared_dimensions.dim_retail_loc` as loc
        on frs.bk_storeid=loc.store
        inner join `chb-prod-data-cust.prod_Customer360.muse_store_codes` as m
        on loc.store=m.bk_store

        and frs.customer_unique_id=${TABLE}.customer_unique_id
        and frs.bk_businessdate >=20190825 and frs.bk_businessdate <20210101

        );;
  }

  measure: ave_cross_brand_adoption_pre_muse_uae{
    group_label: "Cross Brand Analysis"
    description: "Average number of other brands shopped"
    type: average
    value_format: "0.##"
    sql: ${count_brands_shopped_pre_muse_uae};;
  }


  measure: ave_cross_brand_adoption_post_muse_uae{

    group_label: "Cross Brand Analysis"
    description: "Average number of other brands shopped"
    type: average
    value_format: "0.##"
    sql: ${count_brands_shopped_post_muse_uae} ;;
  }


  dimension: count_brands_shopped_pre_muse_kwt {
    group_label: "Cross Brand Analysis (Muse)"
    type: number
    sql: (select count(distinct frs.min_brand_id)
          from  `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr` as frs
          inner join `chb-prod-supplychain-data.prod_shared_dimensions.dim_retail_loc` as loc
          on frs.bk_storeid=loc.store
          inner join `chb-prod-data-cust.prod_Customer360.muse_store_codes` as m
          on loc.store=m.bk_store
          and frs.customer_unique_id=${TABLE}.customer_unique_id
          and frs.bk_businessdate >=20180901 and frs.bk_businessdate <20191101

          );;
  }

  dimension: count_brands_shopped_post_muse_kwt {
    group_label: "Cross Brand Analysis (Muse)"
    type: number
    sql: (select count(distinct frs.min_brand_id)
        from  `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr` as frs
        inner join `chb-prod-supplychain-data.prod_shared_dimensions.dim_retail_loc` as loc
        on frs.bk_storeid=loc.store
        inner join `chb-prod-data-cust.prod_Customer360.muse_store_codes` as m
        on loc.store=m.bk_store

        and frs.customer_unique_id=${TABLE}.customer_unique_id
        and frs.bk_businessdate >=20191101 and frs.bk_businessdate <20210101

        );;
  }

  measure: ave_cross_brand_adoption_pre_muse_kwt{
    group_label: "Cross Brand Analysis"
    description: "Average number of other brands shopped"
    type: average
    value_format: "0.##"
    sql: ${count_brands_shopped_pre_muse_kwt};;
  }


  measure: ave_cross_brand_adoption_post_muse_kwt{

    group_label: "Cross Brand Analysis"
    description: "Average number of other brands shopped"
    type: average
    value_format: "0.##"
    sql: ${count_brands_shopped_post_muse_kwt} ;;
  }



    dimension:sales_amount_usd  {
      value_format: "$#,##0"
      label: "Customer Spend (Group)"
      description: "Total spend by customer in the period. Can use as filter"
      type: number
    }


    dimension: count_brands {
      hidden: yes
      type: number
    }

##this measue doesn't really work
    measure: customer_average_spend {
      hidden: yes
      description: "Average spend in the period of analysis"
      type: average
      value_format: "$#,##0"
      sql:${sales_amount_usd} ;;
    }

##this measue doesn't really work as bk_customer at brand level
    measure: ave_brands_shopped_in_period {
      hidden: yes
      label: "Avg Num Brands Shopped"
      description: "Average number of brands shopped in selected perdiod"
      type: average
      sql: ${count_brands} ;;



    }
    }







view: transaction_attributes {
  view_label: "Retail Sales"
  derived_table: {
    sql:
    SELECT id, 1 as is_muse_regi_txn FROM `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr` as frs

inner join
(select distinct muse_member_id, muse_registration_date
from
`chb-prod-data-cust.prod_base_customers_final.base_customers_distinct`
where muse_member_id is not null
) as m
on frs.atr_muse_id=cast(m.muse_member_id as string)
and frs.atr_trandate=m.muse_registration_date
where frs.atr_muse_id is not null ;;

  }

  dimension: id {
    primary_key: yes
    hidden: yes
  }
  dimension: is_muse_regi_txn {
    hidden: yes
  }
}








view: customer_facts_hkey {
  view_label: "Customers"
  derived_table: {
      sql:
      SELECT
        frs.customer_unique_id,

        SUM(frs.mea_amountusd) AS sales_usd
      FROM  `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr` as frs
      where bk_businessdate >=20180825 and bk_businessdate <20190825
      GROUP BY  customer_unique_id
       ;;

    }
    dimension: customer_unique_id {
      primary_key: yes
    }


    dimension: sales_usd {}


  dimension: base_x_brand_sales_pre_muse{
    hidden: yes
    group_label: "Cross Brand Analysis"
    value_format: "$#,##0"

    sql:${sales_usd}  ;;

  }

  measure: total_x_brand_sales_pre_muse{
    hidden: yes
    value_format: "$#,##0"
    type: sum
    sql:${base_x_brand_sales_pre_muse}  ;;
  }

  measure: avg_x_brand_sales_pre_muse{
    hidden: yes
    value_format: "$#,##0"
    type: average
    sql:${base_x_brand_sales_pre_muse}  ;;
  }


  }


view: dev_c360_new_returning{
view_label: "C360 Customer New Returning"
derived_table: {
  sql:

     SELECT DISTINCT
     min_brand_id,
      brand_customer_unique_id,
    new_cust_brand ,
    new_cust_group,
      date_group,

    FROM
    (
    SELECT
      min_brand_id,
      brand_customer_unique_id,



      {% if base_cust_frs_unified.atr_trandate_date._in_query %} CAST(atr_trandate  AS DATE)
      {% elsif base_cust_frs_unified.atr_trandate_month._in_query %} FORMAT_DATE('%Y-%m', atr_trandate )
      {% elsif base_cust_frs_unified.atr_trandate_quarter._in_query %} (FORMAT_TIMESTAMP('%Y-%m', TIMESTAMP_TRUNC(CAST(atr_trandate AS TIMESTAMP), QUARTER)))
      {% elsif base_cust_frs_unified.atr_trandate_week._in_query %} CONCAT(CAST(EXTRACT(YEAR FROM atr_trandate ) AS STRING),'-', CAST(EXTRACT(WEEK(SUNDAY) FROM atr_trandate) AS STRING))
      {% elsif base_cust_frs_unified.atr_trandate_year._in_query %} EXTRACT(YEAR FROM atr_trandate)
      {% else %} CAST(atr_trandate AS DATE)
      {% endif %}

 AS date_group,

   min(new_cust_brand) as new_cust_brand,
   min(new_cust_group) as new_cust_group,

    FROM  `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr`

    group by 1,2,3
  )



 ;;
}


dimension: min_brand {
  hidden: yes
}

dimension: brand_customer_unique_id {
  hidden: yes
}






  dimension: date_group {
    hidden: yes
    label: "Date Group"
    type: string
    sql: ${TABLE}.date_group ;;
  }

}



view: c360_new_returning{
  view_label: "C360 Customer New Returning"
  derived_table: {
    sql:

     SELECT DISTINCT
     min_brand_id,
      brand_customer_unique_id,
    new_cust_brand ,
    new_cust_group,
      date_group,

    FROM
    (
    SELECT
      min_brand_id,
      brand_customer_unique_id,



      {% if base_customers_factretailsales.atr_trandate_date._in_query %} CAST(atr_trandate  AS DATE)
      {% elsif base_customers_factretailsales.atr_trandate_month._in_query %} FORMAT_DATE('%Y-%m', atr_trandate )
      {% elsif base_customers_factretailsales.atr_trandate_quarter._in_query %} (FORMAT_TIMESTAMP('%Y-%m', TIMESTAMP_TRUNC(CAST(atr_trandate AS TIMESTAMP), QUARTER)))
      {% elsif base_customers_factretailsales.atr_trandate_week._in_query %} CONCAT(CAST(EXTRACT(YEAR FROM atr_trandate ) AS STRING),'-', CAST(EXTRACT(WEEK(SUNDAY) FROM atr_trandate) AS STRING))
      {% elsif base_customers_factretailsales.atr_trandate_year._in_query %} EXTRACT(YEAR FROM atr_trandate)
      {% else %} CAST(atr_trandate AS DATE)
      {% endif %}

 AS date_group,

   min(new_cust_brand) as new_cust_brand,
   min(new_cust_group) as new_cust_group,

    FROM  `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr`

    group by 1,2,3
  )



 ;;
  }

  dimension: pk {
    hidden: yes
    primary_key: yes
    type: string
    sql: CONCAT(${TABLE}.brand_customer_unique_id, ${TABLE}.date_group)  ;;
  }


  dimension: min_brand {
    hidden: yes
  }

  dimension: brand_customer_unique_id {
    hidden: yes
  }






  dimension: date_group {
    hidden: yes
    label: "Date Group"
    type: string
    sql: ${TABLE}.date_group ;;
  }

}


view: c360_new_returning_v2{
  view_label: "C360 Customer New Returning"
  derived_table: {
    sql:

     SELECT DISTINCT
     min_brand_id,
      brand_customer_unique_id,
    new_cust_brand ,
    new_cust_group,
      date_group,

    FROM
    (
    SELECT
      min_brand_id,
      brand_customer_unique_id,



      {% if base_customers_factretailsales_v2.atr_trandate_date._in_query %} CAST(atr_trandate  AS DATE)
      {% elsif base_customers_factretailsales_v2.atr_trandate_month._in_query %} FORMAT_DATE('%Y-%m', atr_trandate )
      {% elsif base_customers_factretailsales_v2.atr_trandate_quarter._in_query %} (FORMAT_TIMESTAMP('%Y-%m', TIMESTAMP_TRUNC(CAST(atr_trandate AS TIMESTAMP), QUARTER)))
      {% elsif base_customers_factretailsales_v2.atr_trandate_week._in_query %} CONCAT(CAST(EXTRACT(YEAR FROM atr_trandate ) AS STRING),'-', CAST(EXTRACT(WEEK(SUNDAY) FROM atr_trandate) AS STRING))
      {% elsif base_customers_factretailsales_v2.atr_trandate_year._in_query %} EXTRACT(YEAR FROM atr_trandate)
      {% else %} CAST(atr_trandate AS DATE)
      {% endif %}

 AS date_group,

   min(new_cust_brand) as new_cust_brand,
   min(new_cust_group) as new_cust_group,

    FROM  `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr`

    group by 1,2,3
  )



 ;;
  }

  dimension: pk {
    hidden: yes
    primary_key: yes
    type: string
    sql: CONCAT(${TABLE}.brand_customer_unique_id, ${TABLE}.date_group)  ;;
  }


  dimension: min_brand {
    hidden: yes
  }

  dimension: brand_customer_unique_id {
    hidden: yes
  }






  dimension: date_group {
    hidden: yes
    label: "Date Group"
    type: string
    sql: ${TABLE}.date_group ;;
  }

}



view: customer_nps_group {
  view_label: "Customers"
  derived_table: {
    explore_source: base_customers_factretailsales {
      column: brand_customer_unique_id {}
      column: transaction_count {field:base_customers_factretailsales.transaction_count}
      column: sales_amount_usd {field:base_customers_factretailsales.Sales_Amount_USD}
      column: group_transacted_customer_count { field: base_customers_distinct.group_transacted_customer_count}
      column: count_brands { field: locations.count_brands }
      column: first_date {field:base_customers_distinct.first_date}
      column: brand_recency {field:base_customers_distinct.brand_recency}
      column: count_new_cust_brand {field:base_customers_factretailsales.count_new_cust_brand}
      column: proportion_of_discount_sales {field:base_customers_factretailsales.proportion_of_discount_sales}
      bind_filters: {from_field: base_customers_factretailsales.atr_trandate_date
        to_field: base_customers_factretailsales.atr_trandate_date}
      bind_filters: {from_field: base_customers_factretailsales.atr_trandate_month
        to_field: base_customers_factretailsales.atr_trandate_month}
      bind_filters: {from_field: base_customers_factretailsales.atr_trandate_quarter
        to_field: base_customers_factretailsales.atr_trandate_quarter}
      bind_filters: {from_field: base_customers_factretailsales.atr_trandate_year
        to_field: base_customers_factretailsales.atr_trandate_year}
      bind_filters: {from_field: base_customers_factretailsales.atr_trandate_week
        to_field: base_customers_factretailsales.atr_trandate_week}
      bind_filters: {from_field: locations.district_name
        to_field: locations.district_name}

    }
  }
  }









view: c360_crossbrand{
  view_label: "Customers"
  derived_table: {
    sql:

     SELECT DISTINCT
     brand_count,
      customer_unique_id,

      date_group,

    FROM
    (
    SELECT
      count(distinct min_brand_id) as brand_count,
      customer_unique_id,



      {% if base_customers_factretailsales.atr_trandate_date._in_query %} CAST(atr_trandate  AS DATE)
      {% elsif base_customers_factretailsales.atr_trandate_month._in_query %} FORMAT_DATE('%Y-%m', atr_trandate )
      {% elsif base_customers_factretailsales.atr_trandate_quarter._in_query %} (FORMAT_TIMESTAMP('%Y-%m', TIMESTAMP_TRUNC(CAST(atr_trandate AS TIMESTAMP), QUARTER)))
      {% elsif base_customers_factretailsales.atr_trandate_week._in_query %} CONCAT(CAST(EXTRACT(YEAR FROM atr_trandate ) AS STRING),'-', CAST(EXTRACT(WEEK(SUNDAY) FROM atr_trandate) AS STRING))
      {% elsif base_customers_factretailsales.atr_trandate_year._in_query %} EXTRACT(YEAR FROM atr_trandate)
      {% else %} CAST(atr_trandate AS DATE)
      {% endif %}

 AS date_group,


    FROM  `chb-prod-data-cust.prod_base_customers_final.base_customers_sales_unified_nvr`

    group by 2,3
  )



 ;;
  }

  dimension: pk {
    hidden: yes
    primary_key: yes
    type: string
    sql: CONCAT(${TABLE}.customer_unique_id, ${TABLE}.date_group)  ;;
  }


  dimension: brand_count {
    group_label: "Customer Dimensions"
    label: "Number of Brands Shopped"
    description: "Numebr of brands the customer has shopped at in the selected transaction date range."

  }

  dimension: customer_unique_id {
    hidden: yes
  }






  dimension: date_group {
    hidden: yes
    label: "Date Group"
    type: string
    sql: ${TABLE}.date_group ;;
  }

}





view: customer_cross_brand {
  view_label: "Customers"
  derived_table: {
    explore_source: base_customers_factretailsales {
      column: customer_unique_id {}
      column: transaction_count {field:base_customers_factretailsales.transaction_count}
      column: sales_amount_usd {field:base_customers_factretailsales.Sales_Amount_USD}
      column: group_transacted_customer_count { field: base_customers_distinct.group_transacted_customer_count}
      column: brand_visits { field: base_customers_factretailsales.brand_visits }

      bind_filters: {from_field: base_customers_factretailsales.atr_trandate_date
        to_field: base_customers_factretailsales.atr_trandate_date}
      bind_filters: {from_field: base_customers_factretailsales.atr_trandate_month
        to_field: base_customers_factretailsales.atr_trandate_month}
      bind_filters: {from_field: base_customers_factretailsales.atr_trandate_quarter
        to_field: base_customers_factretailsales.atr_trandate_quarter}
      bind_filters: {from_field: base_customers_factretailsales.atr_trandate_year
        to_field: base_customers_factretailsales.atr_trandate_year}
      bind_filters: {from_field: base_customers_factretailsales.atr_trandate_week
        to_field: base_customers_factretailsales.atr_trandate_week}
      bind_filters: {from_field: locations.district_name
        to_field: locations.district_name}

    }
  }


dimension: customer_unique_id {
  primary_key: yes
  hidden: yes

}


dimension: brand_visits {
  label: "Number of Brands Shopped"
  description: "The number of group brands a customer has shopped in in the selected transaction period."
  type: number
}

}

view: cust_first_date_by_store {
  sql_table_name:  `chb-prod-data-cust.prod_Customer360.customer_first_known_dates`

  ;;

  dimension: first_date {
    hidden: yes
    type: number
    sql: ${TABLE}.first_date ;;
  }

  dimension: brand_customer_unique_id {
    hidden: yes
    type: number
    sql: ${TABLE}.brand_customer_unique_id ;;
  }
  dimension: bk_storeid {
    hidden: yes
    type: number
    sql: ${TABLE}.bk_storeid ;;
  }




}




view: customer_day_store {

  #used to get total customers in the previous period, whether they shopped current period or not
  sql_table_name: `chb-prod-data-cust.prod_Customer360.fact_cust_store_day`
    ;;

  dimension: pk {
    hidden: yes
    type: string
    primary_key: yes
    sql: concat(cast(${bk_businessdate} as string),cast(bk)${bk_storeid} as string), cast(${customer_unique_id_prev} as string) ;;
  }

  dimension: anykey {
    type: number
    sql: ${TABLE}.anykey ;;
  }

  dimension: bk_businessdate {
    hidden: yes
    type: number
    sql: ${TABLE}.bk_businessdate ;;
  }

  dimension: bk_storeid {
    hidden: yes
    type: number
    value_format_name: id
    sql: ${TABLE}.bk_storeid ;;
  }

  dimension: customer_unique_id_prev {
    hidden: yes
    type: string
    sql: ${TABLE}.customer_unique_id ;;
  }

  dimension: muse_id {
    hidden: yes
    type: string
    sql: ${TABLE}.muse_id ;;
  }

  dimension: district_name {
    hidden: yes
    type: string
    sql: ${TABLE}.district_name ;;
  }

  dimension:  store_name {
    hidden: yes
    type: string
    sql: ${TABLE}.store_name ;;
  }

  dimension:  country_desc {
    hidden: yes
    type: string
    sql: ${TABLE}.country_desc ;;
  }
  dimension: is_muse_store {
    hidden: yes
    type: yesno
    sql: ${TABLE}.is_muse_store ;;
  }

  dimension: is_muse_txn {
    hidden: yes
    type: yesno
    sql: ${TABLE}.is_muse_txn ;;
  }

measure: cust_count_prev_period_v2 {
  view_label: "Retention Rate V2 (beta)"

  type: count_distinct
  sql: ${customer_unique_id_prev} ;;
}



  measure: muse_count_prev_period_v2 {

    view_label: "Retention Rate V2 (beta)"
    type: count_distinct
    sql: case when ${muse_id} is not null then ${customer_unique_id_prev} else null end ;;
  }


  dimension: next_month{
    hidden: yes
    type: number
    sql: ${TABLE}.next_month ;;
  }


  dimension:next_month_year{
    hidden: yes
    type: number
    sql: ${TABLE}.next_month_year ;;
  }
  dimension:next_quarter{
    hidden: yes
    type: number
    sql: ${TABLE}.next_quarter ;;
  }
  dimension:next_quarter_year{
    hidden: yes
    type: number
    sql: ${TABLE}.next_quarter_year ;;
  }
  dimension:next_half{
    hidden: yes
    type: number
    sql: ${TABLE}.next_half ;;
  }
  dimension:next_half_year{
    hidden: yes
    type: number
    sql: ${TABLE}.next_half_year ;;
  }
  dimension:next_year {
    hidden: yes
    type: number
    sql: ${TABLE}.next_year ;;
  }

  measure: count {
    hidden: yes
    type: count
    drill_fields: []
  }
}



view: retained_cust {
  #get count of customers retained fromprevious period
  sql_table_name: `chb-prod-data-cust.prod_Customer360.fact_cust_store_day`
    ;;


    dimension: pk {
      hidden: yes
      type: string
      primary_key: yes
      sql: concat(cast(${bk_businessdate} as string),cast(bk)${bk_storeid} as string), cast(${customer_unique_id_prev} as string) ;;
    }

  dimension: bk_businessdate {
    hidden: yes
    type: number
    sql: ${TABLE}.bk_businessdate ;;
  }

  dimension: bk_storeid {
    hidden: yes

    type: number
    value_format_name: id
    sql: ${TABLE}.bk_storeid ;;
  }

  dimension: customer_unique_id_prev {
    hidden: yes

    type: string
    sql: ${TABLE}.customer_unique_id ;;
  }

  dimension: muse_id {
    hidden: yes
    type: string
    sql: ${TABLE}.muse_id ;;
  }

  dimension: district_name {
    hidden: yes
    type: string
    sql: ${TABLE}.district_name ;;
  }

  dimension:  store_name {
    hidden: yes
    type: string
    sql: ${TABLE}.store_name ;;
  }

  dimension:  country_desc {
    hidden: yes
    type: string
    sql: ${TABLE}.country_desc ;;
  }

  dimension: is_muse_store {
    hidden: yes
    type: yesno
    sql: ${TABLE}.is_muse_store ;;
  }

  dimension: is_muse_txn {
    hidden: yes
    type: yesno
    sql: ${TABLE}.is_muse_txn ;;
  }

  dimension: next_month{
    hidden: yes
    type: number
    sql: ${TABLE}.next_month ;;
  }

  dimension:next_month_year{
    hidden: yes
    type: number
    sql: ${TABLE}.next_month_year ;;
  }
  dimension:next_quarter{
    hidden: yes
    type: number
    sql: ${TABLE}.next_quarter ;;
  }
  dimension:next_quarter_year{
    hidden: yes
    type: number
    sql: ${TABLE}.next_quarter_year ;;
  }
  dimension:next_half{
    hidden: yes
    type: number
    sql: ${TABLE}.next_half ;;
  }
  dimension:next_half_year{
    hidden: yes
    type: number
    sql: ${TABLE}.next_half_year ;;
  }
  dimension:next_year {
    hidden: yes
    type: number
    sql: ${TABLE}.next_year ;;
  }




  measure: retained_customer_count_v2{

    view_label: "Retention Rate V2 (beta)"
    type: count_distinct
    sql: ${customer_unique_id_prev} ;;
  }


  measure: muse_retained_count_v2 {

    view_label: "Retention Rate V2 (beta)"
    type: count_distinct
    sql: case when ${muse_id} is not null then ${customer_unique_id_prev} else null end ;;
  }

measure: retention_rate_v2{

  view_label: "Retention Rate V2 (beta)"
  type: number
  value_format: "0.00%"
  sql: ${retained_customer_count_v2}/nullif(${customer_day_store.cust_count_prev_period_v2},0) ;;
}

measure: muse_retention_rate_v2{

  view_label: "Retention Rate V2 (beta)"
  type: number
  value_format: "0.00%"
  sql: ${muse_retained_count_v2}/nullif(${customer_day_store.muse_count_prev_period_v2},0) ;;
}

  measure: count {
    hidden: yes
    type: count
    drill_fields: []
  }
}




view: returning_cust_dyn {
  sql_table_name: `chb-prod-data-cust.prod_Customer360.fact_cust_store_day_nvr`
    ;;

  dimension: pk {
    hidden: yes
    type: string
    primary_key: yes
    sql: concat(cast(${bk_businessdate} as string),cast(bk)${bk_storeid} as string), cast(${customer_unique_id} as string) ;;
  }



  dimension: bk_businessdate {
    hidden: yes
    type: number
    sql: ${TABLE}.bk_businessdate ;;
  }

  dimension: bk_storeid {
    hidden: yes
    type: number
    value_format_name: id
    sql: ${TABLE}.bk_storeid ;;
  }

  dimension: customer_unique_id {
    hidden: yes
    type: string
    sql: ${TABLE}.customer_unique_id ;;
  }



  dimension: district_name {
    hidden: yes
    type: string
    sql: ${TABLE}.district_name ;;
  }

  dimension:  store_name {
    hidden: yes
    type: string
    sql: ${TABLE}.store_name ;;
  }

  dimension:  country_desc {
    hidden: yes
    type: string
    sql: ${TABLE}.country_desc ;;
  }
  dimension: is_muse_store {
    hidden: yes
    type: yesno
    sql: ${TABLE}.is_muse_store ;;
  }



dimension: new_to_store {
  type: string
  sql: ${TABLE}.new_to_store ;;
}






  measure: count {
    hidden: yes
    type: count
    drill_fields: []
  }
}


  view: returning_cust_dyn_pp {
    sql_table_name: `chb-prod-data-cust.prod_Customer360.fact_cust_store_day_nvr`
      ;;

    dimension: pk {
      hidden: yes
      type: string
      primary_key: yes
      sql: concat(cast(${bk_businessdate} as string),cast(bk)${bk_storeid} as string), cast(${customer_unique_id} as string) ;;
    }



    dimension: bk_businessdate {
      hidden: yes
      type: number
      sql: ${TABLE}.bk_businessdate ;;
    }

    dimension: bk_storeid {
      hidden: yes
      type: number
      value_format_name: id
      sql: ${TABLE}.bk_storeid ;;
    }

    dimension: customer_unique_id {
      hidden: yes
      type: string
      sql: ${TABLE}.customer_unique_id ;;
    }



    dimension: district_name {
      hidden: yes
      type: string
      sql: ${TABLE}.district_name ;;
    }

    dimension:  store_name {
      hidden: yes
      type: string
      sql: ${TABLE}.store_name ;;
    }

    dimension:  country_desc {
      hidden: yes
      type: string
      sql: ${TABLE}.country_desc ;;
    }
    dimension: is_muse_store {
      hidden: yes
      type: yesno
      sql: ${TABLE}.is_muse_store ;;
    }



    dimension: new_to_store {
      hidden: yes
      type: string
      sql: ${TABLE}.new_to_store ;;
    }






    measure: count {
      hidden: yes
      type: count
      drill_fields: []
    }
  }
