include: "/_common/dim_retail_loc.view"



view: min_brands {
  derived_table: {

    sql:
    select distinct l.district as bk_brand, q.min_brand, l.district_name
from
`chb-prod-supplychain-data.prod_shared_dimensions.dim_retail_loc`   as l
inner join
(

select min(district) as min_brand, district_name
from
`chb-prod-supplychain-data.prod_shared_dimensions.dim_retail_loc`
group by district_name) as q
on l.district_name=q.district_name;;

    }
    dimension: min_brand {
      hidden: yes
    }
    dimension: bk_brand {
      hidden: yes
      primary_key: yes
    }
    dimension: district_name {
      hidden: yes
    }
    }





view: locations {

  extends: [dim_retail_loc]


dimension: iso_country_code {
  label: "ISO Country Code"
  map_layer_name: countries
  type: string
  sql: case when ${area_name}='KUWAIT' then 'KWT'
  when ${area_name}='KUWAIT AIRPORT FREE ZONE' then 'KWT'
  when ${area_name}='UAE' then 'ARE'
  when ${area_name}='KSA' then 'SAU'
  when ${area_name}='EGYPT AIRPORT FREEZONE' then 'EGY'
  when ${area_name}='EGYPT' then 'EGY'
  when ${area_name}='MOROCCO' then 'MAR'
  when ${area_name}='JAFZA' then 'ARE'
  when ${area_name}='UAE (EXTERNAL)' then 'ARE'
  else ${area_name} end ;;
}

  dimension: city {
    view_label: "Locations"
    label: "City"
    description: "City coming from Oracle RMS"
    type: string
    sql: ${TABLE}.city ;;
  }

  dimension: country_desc {
    view_label: "Locations"
    label: "Country"
    description: "Country coming from Oracle RMS"
    type: string
    suggest_explore: dim_retail_loc_store
    suggest_dimension: dim_retail_loc_store.store
    sql: ${TABLE}.country_desc ;;
  }

dimension: is_muse_store {
  type: yesno
  sql: ${store} in (select bk_store from  `chb-prod-data-cust.prod_Customer360.muse_store_codes`);;
}


  dimension: district_name {
    label: "District Name (Brand)"
    view_label: "Locations"
    description: "District Name coming from Oracle RMS"
    type: string
    suggest_explore: dim_retail_loc_district_name
    suggest_dimension: dim_retail_loc_district_name.district_name
    sql: ${TABLE}.district_name ;;
  }


  dimension: store_name_segregation {
    group_label: "Locations"
    description: "Segregation of retail stores for Lacoste"
    label: "Channel"
    view_label: "Locations"
    type: string
    hidden: yes
    suggest_explore: dim_retail_loc_store_name
    suggest_dimension: dim_retail_loc_store_name.store_name
    sql: CASE WHEN ${TABLE}.store_name LIKE "LACOSTE - TRYANO%" THEN "TRYANO" WHEN ${TABLE}.store_name LIKE "LACOSTE - ECOMMERCE%"  THEN "ECOMMERCE" WHEN ${TABLE}.store_name LIKE "%LACOSTE - MERAAS OUTLET VILLAGE%" THEN "OUTLET" WHEN ${TABLE}.store_name LIKE "LACOSTE - NORTH COAST - ALEXANDRIA" THEN "OUTLET" WHEN ${TABLE}.store_name LIKE "LACOSTE -%" THEN "STORE" END;;
  }


  dimension: mall_name {
    type: string
    hidden: no
    sql: ${TABLE}.mall_name ;;
  }



  dimension: CX_Prority_Store  {
    type: yesno
    sql: case when ${store} in (3029,3047,3046,3034,3025,3033,3044,3027,3043,3037,3048,3059,3053,3024,3069,
      3136,3060,3050,3129,52010,52011,52012,52056,52003,52004,52043,52009,52014,52020,52034,52044,52035,52036,
      52024,52027,52054,52028,52029,52030,52031,52051,30006,30017,30029,30027,30019,30013,30022,30002,30012,30004,30036,
      30076,30034,30044,30032,30035,30057,30038,30040,30070,30083,30081,30043,30094,30079,30080)
      then true else false end
        ;;
  }



  dimension_group: store_close {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.store_close_date ;;
  }

  dimension: store_format {
    type: number
    hidden: yes
    view_label: "Locations"
    description: "Store Format of location in Oracle RMS"
    sql: ${TABLE}.store_format ;;
  }

  dimension: store_name {
    view_label: "Locations"
    label: "Location Name"
    description: "Name of location in Oracle RMS"
    type: string
    suggest_explore: dim_retail_loc_store_name
    suggest_dimension: dim_retail_loc_store_name.store_name
    sql: ${TABLE}.store_name ;;
  }


  dimension_group: store_open {
    type: time
    view_label: "Locations"
    description: "Opening date of location in Oracle RMS"
#     hidden: yes
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.store_open_date ;;
    hidden: yes
  }

  dimension: total_square_ft {
    type: number
    hidden: yes

    sql: ${TABLE}.total_square_ft ;;
  }

  dimension: vat_region {
    hidden: yes
    type: number

    sql: ${TABLE}.vat_region ;;
  }

  measure: count {
    type: count
    hidden: yes
    drill_fields: [detail*]
  }

  measure: count_brands {
    hidden: yes
    type:  count_distinct
    sql: ${TABLE}.district_name ;;
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      area_name,
      district_name,
      store_name,
      mall_name,
      region_name,
      chain_name,
      channel_name
    ]
  }

  # ---- Vertical mapping

  dimension: MUSE_Vertical{
    view_label: "Locations"
    label: "MUSE Vertical mapping"
    hidden: no
    type: string
    sql: CASE WHEN ${TABLE}.district_name  in ("GHAWALI", "LOCCITANE",  "PENHALIGON'S", "MOLTON BROWN", "KILIAN PARIS", "NARS", "FACES",  "ROGER & GALLET", "URBAN DECAY",  "YSL",  "GIORGIO ARMANI", "BOBBI BROWN",  "FREDERIC MALLE", "ATELIER COLOGNE",  "BY KILIAN") THEN "BEAUTY"
              WHEN ${TABLE}.district_name in ("SWAROVSKI",  "LACOSTE",  "MICHAEL KORS", "TUMI", "DOLCE & GABBANA",  "TORY BURCH", "WEEKEND MAX MARA", "KARL LAGERFELD", "FILA", "LEVEL SHOES",  "PAUL SMITH", "LONGCHAMP",  "FURLA",  "KENZO",  "MAX MARA", "SALVATORE FERRAGAMO",  "STELLA MCCARTNEY", "TRYANO", "HACKETT",  "ALICE & OLIVIA", "DSQUARED2",  "ZADIG & VOLTAIRE", "MARINA RINALDI", "PAULE KA", "RENE CAOVILLA",  "IL GUFO",  "COURCELLES", "VILEBREQUIN",  "STELLA MC CARTNEY",  "DOLCE&GABBANA","VERSACE" , "HUGO BOSS")  THEN "FASHION"
              WHEN ${TABLE}.district_name in ("TANAGRA",  "CHRISTOFLE", "BACCARAT", "b8ta", "ST DUPONT",  "DYLAN'S CANDY BAR",  "B8ta") THEN "LIFESTYLE"
              WHEN ${TABLE}.district_name like ("DYLAN%") THEN "LIFESTYLE"
              ELSE "NA" END;;
  }




dimension: CRM_Vertical{
  view_label: "Locations"
  label: "CRM Vertical mapping"
  hidden: no
  type: string
  sql: CASE WHEN ${TABLE}.district_name  in ("BY KILIAN", "ELEMIS", "FACES" ,"GHAWALI", "LOCCITANE", "MOLTON BROWN", "NARS" ,"MAKE UP FOR EVER", "PENHALIGON'S", "YSL" ) THEN "BEAUTY"
             WHEN ${TABLE}.district_name in ("KARL LAGERFELD",  "KENZO", "LACOSTE",  "MICHAEL KORS", "SALVATORE FERRAGAMO", "SWAROVSKI", "TORY BURCH","TANAGRA", "FACTORY OUTLET", "TRYANO", "TUMI" )  THEN "FASHION"
              ELSE "NA" END;;
}

 }


view: customer_locations {

  view_label: "Locations"
  extends: [dim_retail_loc]

dimension: city {
  sql: ${TABLE}.city ;;
  hidden: yes
}

  dimension_group: store_close {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.store_close_date ;;
  }

  dimension_group: store_open {
    type: time
    view_label: "Locations"
    description: "Opening date of location in Oracle RMS"
     hidden: yes
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.store_open_date ;;
  }

  dimension: country_desc_swa {
 hidden: yes
    sql: CASE WHEN ${TABLE}.store_name LIKE '%ECOMMERCE%' THEN 'ECOM'
      ELSE ${TABLE}.country_desc END ;;
  }


  dimension: store_name_segregation {
 hidden: yes
    sql: CASE WHEN ${TABLE}.store_name LIKE "LACOSTE - TRYANO%" THEN "TRYANO" WHEN ${TABLE}.store_name LIKE "LACOSTE - ECOMMERCE%"  THEN "ECOMMERCE" WHEN ${TABLE}.store_name LIKE "%LACOSTE - MERAAS OUTLET VILLAGE%" THEN "OUTLET" WHEN ${TABLE}.store_name LIKE "LACOSTE - NORTH COAST - ALEXANDRIA" THEN "OUTLET" WHEN ${TABLE}.store_name LIKE "LACOSTE -%" THEN "STORE" END;;
  }

  dimension: selling_square_ft {
    hidden: yes
    sql: ${TABLE}.selling_square_ft ;;
  }

  dimension: total_square_ft {
    hidden: yes
    sql: ${TABLE}.total_square_ft ;;
  }


  dimension: store_format {
  hidden: yes
    sql: ${TABLE}.store_format ;;
  }

  dimension: store_name {
    hidden: yes
    sql: ${TABLE}.store_name ;;
  }


  dimension: is_ecommerce {
    hidden: yes
    sql: ${store_name} LIKE "%ECOMMERCE%" ;;
  }


  dimension: country_desc {
    view_label: "Locations"
    hidden: yes
    label: "Country"
    description: "Country coming from Oracle RMS"
    type: string
    suggest_explore: dim_retail_loc_store
    suggest_dimension: dim_retail_loc_store.store
    sql: ${TABLE}.country_desc ;;
  }



  dimension: district_name {
    view_label: "Locations"
    description: "District Name coming from Oracle RMS"
    type: string
    suggest_explore: dim_retail_loc_district_name
    suggest_dimension: dim_retail_loc_district_name.district_name
    sql: ${TABLE}.district_name ;;
  }


  dimension: chain_name {
    type: string
    hidden: yes
    sql: ${TABLE}.chain_name ;;
  }

  measure: count_brands {
    hidden: yes
    type:  count_distinct
    sql: ${TABLE}.district_name ;;
  }



}
