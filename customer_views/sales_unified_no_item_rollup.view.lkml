view: sales_unified_no_item_rollup {
  sql_table_name: `chb-prod-data-cust.prod_Customer360.sales_unified_no_item_rollup`
    ;;

  dimension: amountlocal_beforetax {
    type: number
    sql: ${TABLE}.amountlocal_beforetax ;;
  }

  dimension: amountusd_beforetax {
    type: number
    sql: ${TABLE}.amountusd_beforetax ;;
  }

  dimension: atr_muse_id {
    type: string
    sql: ${TABLE}.atr_muse_id ;;
  }

  dimension: bk_brand {
    type: number
    sql: ${TABLE}.bk_brand ;;
  }

  dimension: bk_businessdate {
    type: number
    sql: ${TABLE}.bk_businessdate ;;
  }

  dimension: bk_storeid {
    type: number
    value_format_name: id
    sql: ${TABLE}.bk_storeid ;;
  }

  dimension: brand_customer_unique_id {
    type: string
    sql: ${TABLE}.brand_customer_unique_id ;;
  }

  dimension: crm_customer_id {
    type: string
    sql: ${TABLE}.crm_customer_id ;;
  }

  dimension: customer_unique_id {
    type: number
    sql: ${TABLE}.customer_unique_id ;;
  }

  dimension: discountamt_local {
    type: number
    sql: ${TABLE}.discountamt_local ;;
  }

  dimension: discountamt_usd {
    type: number
    sql: ${TABLE}.discountamt_usd ;;
  }

  dimension: mea_amountlocal {
    type: number
    sql: ${TABLE}.mea_amountlocal ;;
  }

  dimension: mea_amountusd {
    type: number
    sql: ${TABLE}.mea_amountusd ;;
  }

  dimension: mea_quantity {
    type: number
    sql: ${TABLE}.mea_quantity ;;
  }

  dimension: min_brand_id {
    type: number
    sql: ${TABLE}.min_brand_id ;;
  }

  dimension: new_cust_brand {
    type: string
    sql: ${TABLE}.new_cust_brand ;;
  }

  dimension: new_cust_group {
    type: string
    sql: ${TABLE}.new_cust_group ;;
  }

  dimension: pos_customer_id {
    type: string
    sql: ${TABLE}.pos_customer_id ;;
  }

  dimension: taxamt_local {
    type: number
    sql: ${TABLE}.taxamt_local ;;
  }

  dimension: taxamt_usd {
    type: number
    sql: ${TABLE}.taxamt_usd ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
