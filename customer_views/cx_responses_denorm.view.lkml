view: cx_responses_denorm {
  view_label: "CX Responses"
  sql_table_name: `chb-prod-data-cust.prod_Customer360.cx_responses_denorm`
    ;;


  dimension: purchase_c {
    hidden: yes

    type: string
    sql: ${TABLE}.purchase_c ;;
  }

dimension: frs_tran_key {
  hidden: yes
  type: string
  sql: ${TABLE}.frs_tran_key ;;
}

dimension: brand_customer_unique_id {
  hidden: yes
  type: string

  sql: ${TABLE}.brand_customer_unique_id ;;

}


  dimension: name {
    hidden: yes
    primary_key: yes
    type: string
    sql: ${TABLE}.name ;;
  }

  dimension: bk_customer {
    hidden: yes
    type: string
    sql: ${TABLE}.BK_Customer ;;
  }




  measure: nps_responses {
    label: "Count of NPS Responses"
    type: count_distinct
    sql: case when ${likely_recommend_store_c} is not null then ${crm_customer_id} else null end;;
  }

  measure: nps_promoters {
    label: "Count NPS Promotors"
    type: count_distinct
    sql: case when ${nps_category} = "Promotor" then ${crm_customer_id} else null end;;
  }

  measure: nps_passives {
    label: "Count NPS Passives"
    type: count_distinct
    sql: case when ${nps_category} = "Passive" then ${crm_customer_id} else null end;;
  }

  measure: nps_detractors {
    label: "Count NPS Detractors"
    type: count_distinct
    sql: case when ${nps_category} = "Detractor" then ${crm_customer_id} else null end;;
  }

  measure: nps_score {
    label: "NPS Score"
    value_format: "0%"
    type: number
    sql: case when ${nps_promoters}-${nps_detractors}<0 then 0
          when ${nps_responses}>0 then  ((${nps_promoters}-${nps_detractors})/${nps_responses})
          else null end;;
  }

#pushed for changes on 27042021 - Rabia
  measure: ces_responses {
    label: "Count of CES Responses"
    description: "Total number of CES respondents"
    type: count_distinct
    sql: case when ${ease_of_goal_accomplishment_c} is not null then ${crm_customer_id} else null end;;
  }

  measure: ces_positive {
    label: "Count CES positive responses"
    description: "Count of respondents with a very easy experience"
    type: count_distinct
    sql: case when ${ease_of_goal_accomplishment_c} = "Very Easy" then ${crm_customer_id} else null end;;
  }

  measure: ces_neutral {
    label: "Count CES neutral responses"
    description: "Count of respondents with experience that took a bit of time"
    type: count_distinct
    sql: case when ${ease_of_goal_accomplishment_c} = " It took a bit of time" then ${crm_customer_id} else null end;;
  }

  measure: ces_negative {
    label: "Count CES negative responses"
    description: "Count of respondents with a very difficult experience"
    type: count_distinct
    sql: case when ${ease_of_goal_accomplishment_c} = "Very difficult" then ${crm_customer_id} else null end;;
  }

  measure: ces_calculations {
    label: "CES calculations"
    hidden:  yes
    type:number
    sql: ${ces_positive}*3 + ${ces_neutral}*2 + ${ces_negative}*1;;
}

  measure: ces_score {
    label: "CES Score"
    type:number
    value_format: "0%"
    sql: ${ces_calculations}/(${ces_responses}*3);;
  }

  measure: csat_responses {
    label: "Count of CSAT Responses"
    description: "Total number of CSAT respondents"
    type: count_distinct
    sql:  case when ${experience_with_store_staff_c} is not null then ${crm_customer_id} else null end;;
    }

  measure: csat_calculations {
    label: "CSAT Calculations"
    hidden:  yes
    type: average
    sql: case when ${experience_with_store_staff_c} is not null then ${experience_with_store_staff_c} else null end;;
  }

  measure: csat_score {
    label: "CSAT Score"
    type:number
    value_format: "0%"
    sql: ${csat_calculations}/5 ;;
  }

########


  dimension_group: business_day_date_c {
    hidden: yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.business_day_date_c ;;
  }

  dimension: cafe_survey_completed_c {
    hidden: yes
    type: string
    sql: ${TABLE}.cafe_survey_completed_c ;;
  }

  dimension: case_owner_c {
    hidden: yes
    type: string
    sql: ${TABLE}.case_owner_c ;;
  }

  dimension: cashier_id_c {
    hidden: yes
    type: string
    sql: ${TABLE}.cashier_id_c ;;
  }

  dimension: cashier_name_c {
    hidden: yes
    type: string
    sql: ${TABLE}.cashier_name_c ;;
  }

  dimension: contact_c {
    hidden: yes
    type: string
    sql: ${TABLE}.contact_c ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_date ;;
  }

  dimension: crm_customer_id {
    hidden: yes
    type: string
    sql: ${TABLE}.CRM_Customer_ID ;;
  }

  dimension: csat_follow_up_0_5_c {
    label:"Negative Follow Up"
    type: string
    sql: ${TABLE}.csat_follow_up_0_5_c ;;
  }

  dimension: csat_follow_up_6_10_c {
    label: "Positive Follow Up"
    type: string
    sql: ${TABLE}.csat_follow_up_6_10_c ;;
  }

  dimension: csat_score_c {
    hidden: yes
    type: number
    sql: ${TABLE}.csat_score_c ;;
  }

  dimension: currency_iso_code {
    hidden: yes
    type: string
    sql: ${TABLE}.currency_iso_code ;;
  }

  dimension: customer_id_c {
    hidden: yes
    type: string
    sql: ${TABLE}.customer_id_c ;;
  }

  dimension: customer_name_c {
    hidden: yes
    type: string
    sql: ${TABLE}.customer_name_c ;;
  }

  dimension: customer_satisfaction_c {
    hidden: yes
    type: string
    sql: ${TABLE}.customer_satisfaction_c ;;
  }

  dimension: customer_service_quality_of_experience_c {
    hidden: yes
    type: string
    sql: ${TABLE}.customer_service_quality_of_experience_c ;;
  }

  dimension: customer_service_time_to_address_c {
    hidden: yes
    type: string
    sql: ${TABLE}.customer_service_time_to_address_c ;;
  }

  dimension: customer_service_understand_concerns_c {
    hidden: yes
    type: string
    sql: ${TABLE}.customer_service_understand_concerns_c ;;
  }

  dimension: delivery_c {
    hidden: yes
    type: number
    sql: ${TABLE}.delivery_c ;;
  }

  dimension: desc_brand {
    hidden: yes
    type: string
    sql: ${TABLE}.Desc_Brand ;;
  }

  dimension: ease_of_goal_accomplishment_c {
    label: "CES Response"
    type: string
    sql: ${TABLE}.ease_of_goal_accomplishment_c ;;
  }

  dimension: email_c {
    hidden: yes
    type: string
    sql: ${TABLE}.email_c ;;
  }

  dimension: emp_name {
    hidden: yes
    type: string
    sql: ${TABLE}.emp_name ;;
  }

  dimension: experience_with_store_staff_c {
    label: "CSAT Response"
    type: number
    sql: ${TABLE}.experience_with_store_staff_c ;;
  }

  dimension: experience_with_us_service_c {
    hidden: yes
    type: number
    sql: ${TABLE}.experience_with_us_service_c ;;
  }

  dimension: facebook_improvement_c {
    hidden: yes
    type: string
    sql: ${TABLE}.facebook_improvement_c ;;
  }

  dimension: facebook_improvement_other_c {
    hidden: yes
    type: string
    sql: ${TABLE}.facebook_improvement_other_c ;;
  }

  dimension: familiar_with_l_occitane_cafe_delivery_c {
    hidden: yes
    type: string
    sql: ${TABLE}.familiar_with_l_occitane_cafe_delivery_c ;;
  }

  dimension: favorite_beauty_brand_c {
    hidden: yes
    type: string
    sql: ${TABLE}.favorite_beauty_brand_c ;;
  }

  dimension: favorite_fashion_brand_c {
    hidden: yes
    type: string
    sql: ${TABLE}.favorite_fashion_brand_c ;;
  }

  dimension: favorite_fragrance_brand_c {
    hidden: yes
    type: string
    sql: ${TABLE}.favorite_fragrance_brand_c ;;
  }

  dimension: favorite_jewelry_brand_c {
    hidden: yes
    type: string
    sql: ${TABLE}.favorite_jewelry_brand_c ;;
  }

  dimension: favorite_makeup_brand_c {
    hidden: yes
    type: string
    sql: ${TABLE}.favorite_makeup_brand_c ;;
  }

  dimension: favorite_skincare_brand_c {
    hidden: yes
    type: string
    sql: ${TABLE}.favorite_skincare_brand_c ;;
  }

  dimension: favorite_travel_bag_brand_c {
    hidden: yes
    type: string
    sql: ${TABLE}.favorite_travel_bag_brand_c ;;
  }

  dimension: improve_one_thing_c {
    label: "Improve One Thing"
    group_label: "Conditional Responses"
    type: string
    sql: ${TABLE}.improve_one_thing_c ;;
  }

  dimension: improve_one_thing_other_c {
    label: "Improve One Other Thing"
    group_label: "Conditional Responses"
    type: string
    sql: ${TABLE}.improve_one_thing_other_c ;;
  }

  dimension: instagram_improvement_c {
    hidden: yes
    type: string
    sql: ${TABLE}.instagram_improvement_c ;;
  }

  dimension: instagram_improvement_other_c {
    hidden: yes
    type: string
    sql: ${TABLE}.instagram_improvement_other_c ;;
  }

  dimension: invoice_number_c {
    hidden: yes
    type: string
    sql: ${TABLE}.invoice_number_c ;;
  }

  dimension: is_deleted {
    hidden: yes
    type: yesno
    sql: ${TABLE}.is_deleted ;;
  }

  dimension: lead_source_c {
    hidden: yes
    type: string
    sql: ${TABLE}.lead_source_c ;;
  }

  dimension: lead_source_other_c {
    hidden: yes
    type: string
    sql: ${TABLE}.lead_source_other_c ;;
  }

  dimension: likely_recommend_brand_c {
    hidden: yes
    type: number
    sql: ${TABLE}.likely_recommend_brand_c ;;
  }

  dimension: likely_recommend_store_c {
    label: "NPS Response"
    type: number
    sql: ${TABLE}.likely_recommend_store_c ;;
  }

  dimension: main_reason_to_come_to_store_c {
    hidden: yes
    type: string
    sql: ${TABLE}.main_reason_to_come_to_store_c ;;
  }

  dimension: missing_disappointing_other_c {
    hidden: yes
    type: string
    sql: ${TABLE}.missing_disappointing_other_c ;;
  }

  dimension: missing_dissapointing_c {
    hidden: yes
    type: string
    sql: ${TABLE}.missing_dissapointing_c ;;
  }



  dimension: nps_category {
    label: "Customer NPS Category"
    type: string
    sql: ${TABLE}.NPS_Category ;;
  }

  dimension: order_number_c {
    hidden: yes
    type: string
    sql: ${TABLE}.order_number_c ;;
  }

  dimension: place_order_c {
    hidden: yes
    type: number
    sql: ${TABLE}.place_order_c ;;
  }

  dimension: previous_browsing_c {
    hidden: yes
    sql: ${TABLE}.previous_browsing_c ;;
  }

  dimension: previous_browsing_other_c {
    hidden: yes
    type: string
    sql: ${TABLE}.previous_browsing_other_c ;;
  }



  dimension: purchase_currency_iso_code {
    hidden: yes
    type: string
    sql: ${TABLE}.purchase_currency_iso_code ;;
  }

  dimension: reason_recommend_friend_c {
    label: "What Stood Out"
    group_label: "Conditional Responses"
    type: string
    sql: ${TABLE}.reason_recommend_friend_c ;;
  }

  dimension: reason_recommend_friend_other_c {
    hidden: yes
    type: string
    sql: ${TABLE}.reason_recommend_friend_other_c ;;
  }

  dimension: recommend_facebook_c {
    hidden: yes
    type: number
    sql: ${TABLE}.recommend_facebook_c ;;
  }

  dimension: recommend_instagram_c {
    hidden: yes
    type: number
    sql: ${TABLE}.recommend_instagram_c ;;
  }

  dimension: recommend_store_c {
    hidden: yes
    type: number
    sql: ${TABLE}.recommend_store_c ;;
  }

  dimension: recommend_website_c {
    hidden: yes
    type: number
    sql: ${TABLE}.recommend_website_c ;;
  }

  dimension: respondent_id_c {
    hidden: yes
    type: string
    sql: ${TABLE}.respondent_id_c ;;
  }

  dimension: sales_associate_id_c {
    hidden: yes
    type: string
    sql: ${TABLE}.sales_associate_id_c ;;
  }

  dimension: sales_associate_name_c {
    hidden: yes
    type: string
    sql: ${TABLE}.sales_associate_name_c ;;
  }

  dimension: sales_associate_welcome_c {
    hidden: yes
    type: number
    sql: ${TABLE}.sales_associate_welcome_c ;;
  }

  dimension: sr_name {
    hidden: yes
    type: string
    sql: ${TABLE}.sr_name ;;
  }

  dimension: store_city_c {
    hidden: yes
    type: string
    sql: ${TABLE}.store_city_c ;;
  }

  dimension: store_id_c {
    hidden: yes
    type: string
    sql: ${TABLE}.store_id_c ;;
  }

  dimension: store_market_c {
    hidden: yes
    type: string
    sql: ${TABLE}.store_market_c ;;
  }

  dimension: styling_beauty_advice_c {
    type: number
    hidden: yes
    sql: ${TABLE}.styling_beauty_advice_c ;;
  }

  dimension: survey_brand {
    type: string
    sql: ${TABLE}.Survey_Brand ;;
  }

  dimension_group: survey_record_created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.survey_record_created_date ;;
  }

  dimension: survey_response_status_c {
    hidden: yes
    type: string
    sql: ${TABLE}.survey_response_status_c ;;
  }

  dimension: survey_title_c {
    hidden: yes
    type: string
    sql: ${TABLE}.survey_title_c ;;
  }

  dimension: survey_type_c {
    hidden: yes
    type: string
    sql: ${TABLE}.survey_type_c ;;
  }

  dimension_group: system_modstamp {
    hidden: yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.system_modstamp ;;
  }

  dimension: tax_amount_c {
    hidden: yes
    type: number
    sql: ${TABLE}.tax_amount_c ;;
  }

  dimension: tax_amount_usd_c {
    hidden: yes
    type: number
    sql: ${TABLE}.tax_amount_usd_c ;;
  }

  dimension: tender_type_group_c {
    hidden: yes
    type: string
    sql: ${TABLE}.tender_type_group_c ;;
  }

  dimension: total_amount_c {
    hidden: yes
    type: number
    sql: ${TABLE}.total_amount_c ;;
  }

  dimension: total_amount_usd_c {
    hidden: yes
    type: number
    sql: ${TABLE}.total_amount_usd_c ;;
  }

  dimension: total_discount_amount_c {
    hidden: yes
    type: number
    sql: ${TABLE}.total_discount_amount_c ;;
  }

  dimension: total_discount_amount_usd_c {
    hidden: yes
    type: number
    sql: ${TABLE}.total_discount_amount_usd_c ;;
  }

  dimension: transaction_code_c {
    hidden: yes
    type: string
    sql: ${TABLE}.transaction_code_c ;;
  }

  dimension_group: transaction {
    hidden: yes
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.transaction_date ;;
  }

  dimension: tran_date_key {
    hidden: yes
    type: number
    value_format: "#"
    sql: cast(FORMAT_DATE('%Y%m%d', ${transaction_date}) as int64) ;;

  }

  dimension_group: transaction_date_time_c {
    hidden: yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.transaction_date_time_c ;;
  }

  dimension: transaction_number_c {
    hidden: yes
    type: string
    sql: ${TABLE}.transaction_number_c ;;
  }

  dimension: transaction_sequence_number_c {
    hidden: yes
    type: string
    sql: ${TABLE}.transaction_sequence_number_c ;;
  }

  dimension: transaction_type_c {
    hidden: yes
    type: string
    sql: ${TABLE}.transaction_type_c ;;
  }

  dimension: twitter_improvement_c {
    hidden: yes
    type: string
    sql: ${TABLE}.twitter_improvement_c ;;
  }

  dimension: twitter_improvement_other_c {
    hidden: yes
    type: string
    sql: ${TABLE}.twitter_improvement_other_c ;;
  }

  dimension: user_experience_c {
    hidden: yes
    type: number
    sql: ${TABLE}.user_experience_c ;;
  }


  dimension: what_can_we_improve_c {
    hidden: yes
    type: string
    sql: ${TABLE}.what_can_we_improve_c ;;
  }

  dimension: what_can_we_improve_others_c {
    hidden: yes
    type: string
    sql: ${TABLE}.what_can_we_improve_others_c ;;
  }




  measure: count {
    type: count
    drill_fields: [sr_name, name, emp_name]
  }
}
