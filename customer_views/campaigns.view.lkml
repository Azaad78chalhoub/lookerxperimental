include: "../_common/period_over_period_flexible.view"

view: prod_campaigns_sales_last_send_attribution {
  view_label: "Retail Sales"
  sql_table_name: `chb-prod-data-cust.prod_campaigns.prod_campaigns_sales_last_send_attribution`;;


  parameter: days_since_campaign {
    view_label: "Campaigns"
    description: "Days since campaign date for offline sales calculation"
    label: "Sales period:"
    type: unquoted


    allowed_value: {
      label: "15 days"
      value: "15"
    }
    allowed_value: {
      label: "10 days"
      value: "10"
    }
    allowed_value: {
      label: "7 days"
      value: "7"
    }
    allowed_value: {
      label: "5 days"
      value: "5"
    }
    allowed_value: {
      label: "2 days"
      value: "2"
    }
    default_value: "15"
  }


  dimension: id {
    label: "id"
    primary_key: yes
    hidden: yes
    type: string
    sql: ${TABLE}.id ;;
  }

  dimension: country {
    type: string
    hidden: yes
    sql: ${TABLE}.country ;;
  }

  dimension: campaign_email_id {
    label: "campaign_email_id"
    hidden: yes
    type: string
    sql: ${TABLE}.campaign_email_id ;;
  }

  dimension: transaction_type {
    label: "Transaction Type"
    description: "ECOMM or OFFLINE transaction"
    hidden: no
    type: string
    sql: ${TABLE}.transaction_type ;;
  }

  dimension: customer_unique_id {
    hidden: yes
    type: string
    sql: ${TABLE}.customer_unique_id ;;
  }

  dimension: brand_customer_unique_id {
    hidden: yes
    type: string
    sql: ${TABLE}.brand_customer_unique_id ;;
  }

  dimension: email {
    hidden: yes
    type: string
    view_label: "Campaigns"
    sql: ${TABLE}.email ;;
  }

  dimension: campaign_name {
    label: "Campaign Name"
    description: "Name of the Campaign in SFMC"
    hidden: no
    type: string
    view_label: "Campaigns"
    sql: ${TABLE}.campaign_name ;;
  }

  extends: [period_over_period_flexible]

  dimension_group: pop_no_tz{
    sql: ${email_date_sent} ;;
  }

  dimension: email_date_sent {
    hidden: no
    description: "Date of the sent email"
    type: date
    view_label: "Campaigns"
    sql: CAST(${TABLE}.email_date_sent AS TIMESTAMP) ;;
  }

  dimension: campaign_send_month {
    hidden: no
    description: "Month of the sent email"
    type: date_month
    view_label: "Campaigns"
    sql:CAST(${TABLE}.email_date_sent AS TIMESTAMP) ;;
  }

  dimension: campaign_send_month_name {
    hidden: no
    label: "Month Name"
    description: "Month name of the sent email"
    type: date_month_name
    view_label: "Campaigns"
    sql:CAST(${TABLE}.email_date_sent AS TIMESTAMP) ;;
  }

  dimension: campaign_send_year {
    hidden: no
    type: date_year
    description: "Yearof the sent email"
    view_label: "Campaigns"
    sql:CAST(${TABLE}.email_date_sent AS TIMESTAMP) ;;
  }

  dimension: email_date_opened {
    hidden: no
    type: date
    description: "Date of the email's first open event"
    view_label: "Campaigns"
    sql: CAST(${TABLE}.email_date_opened AS TIMESTAMP) ;;
  }

  dimension: email_unsubscribed_date {
    hidden: no
    type: date
    description: "Date of the unsubscription even"
    view_label: "Campaigns"
    sql: CAST(${TABLE}.email_unsubscribed_date AS TIMESTAMP) ;;
  }


  dimension: brand_name {
    hidden: no
    type: string
    description: "Name of the brand"
    view_label: "Campaigns"
    sql: ${TABLE}.brand_name ;;
  }

  dimension: brand_id {
    hidden: yes
    type: number
    view_label: "Campaigns"
    sql: ${TABLE}.brand_id ;;
  }


  dimension: bu_brand_name {
    hidden: yes
    type: string
    view_label: "Campaigns"
    sql: ${TABLE}.bu_brand_name ;;
  }

  dimension: is_hard_bounce {
    hidden: no
    type: yesno
    description: "True/False if there was a hard bounce"
    view_label: "Campaigns"
    sql: ${TABLE}.is_hard_bounce ;;
  }

  dimension: is_soft_bounce {
    hidden: no
    type: yesno
    description: "True/False if there was a soft bounce"
    view_label: "Campaigns"
    sql: ${TABLE}.is_soft_bounce ;;
  }

  dimension: number_of_total_clicks {
    hidden: no
    type: number
    description: "Number of total clicks per customer"
    view_label: "Campaigns"
    sql: ${TABLE}.number_of_total_clicks ;;
  }

  dimension: number_of_unique_clicks {
    hidden: no
    type: number
    description: "Number of unique clicks per customer"
    view_label: "Campaigns"
    sql: ${TABLE}.number_of_unique_clicks ;;
  }

  # ga dimensions
  # users,
  # new_users,
  # product_list_views,
  # product_detail_views,

  dimension: ga_sessions {
    hidden: no
    type: number
    view_label: "Google Analytics"
    description: "Number of sessions"
    label: "Sessions"
    sql: ${TABLE}.sessions ;;
  }

  dimension: ga_sessions_duration {
    hidden: no
    type: number
    description: "Session duration"
    view_label: "Google Analytics"
    label: "Sessions Duration"
    sql: ${TABLE}.sessions_duration ;;
  }

  dimension: ga_bounces {
    hidden: no
    type: number
    description: "Number of bounces"
    view_label: "Google Analytics"
    label: "Bounces"
    sql: ${TABLE}.bounces ;;
  }

  dimension: ga_pageviews {
    hidden: no
    type: number
    description: "Number of pageviews"
    view_label: "Google Analytics"
    label: "Page Views"
    sql: ${TABLE}.pageviews ;;
  }

  dimension: ga_unique_pageviews {
    hidden: no
    type: number
    description: "Number of unique pageviews"
    view_label: "Google Analytics"
    label: "Unique Page Views"
    sql: ${TABLE}.unique_pageviews ;;
  }

  dimension: ga_product_adds_to_cart {
    hidden: no
    type: number
    description: "Number of products adds to cart"
    view_label: "Google Analytics"
    label: "Product Adds To Cart"
    sql: ${TABLE}.product_adds_to_cart ;;
  }

  dimension: ga_product_checkouts {
    hidden: no
    type: number
    description: "Number of product checkouts"
    view_label: "Google Analytics"
    label: "Product Checkouts"
    sql: ${TABLE}.product_checkouts ;;
  }

  dimension: ga_users {
    hidden: no
    type: number
    description: "Number of GA users"
    view_label: "Google Analytics"
    label: "Users"
    sql: ${TABLE}.users ;;
  }

  dimension: ga_new_users {
    hidden: no
    type: number
    description: "Number of GA new users"
    view_label: "Google Analytics"
    label: "New Users"
    sql: ${TABLE}.new_users ;;
  }

  dimension: ga_product_list_views {
    hidden: no
    type: number
    description: "Number of product list views"
    view_label: "Google Analytics"
    label: "Product List Views"
    sql: ${TABLE}.product_list_views ;;
  }

  dimension: ga_product_detail_views {
    hidden: no
    type: number
    description: "Number of product detail views"
    view_label: "Google Analytics"
    label: "Product Detail Views"
    sql: ${TABLE}.product_detail_views ;;
  }

  dimension: transaction_id {
    hidden: no
    label: "Order ID"
    description: "ID of the sales transaction"
    view_label: "Retail Sales"
    type: string
    sql: ${TABLE}.d{{days_since_campaign._parameter_value }}_transaction_id ;;
  }


  dimension: product_id {
    hidden: yes
    type: number
    view_label: "Campaigns"
    sql: ${TABLE}.d{{days_since_campaign._parameter_value }}_product_id ;;
  }

  dimension: email_opened {
    hidden: no
    type: yesno
    description: "True/False if the email was opened"
    view_label: "Campaigns"
    sql: ${TABLE}.email_opened ;;
  }

  measure: count_sendouts {
    hidden: no
    label: "Count of Sends"
    group_label: "Campaign view"
    description: "Total number of unique emails per campaign"
    view_label: "Campaigns"
    type: count_distinct
    sql: ${email} ;;
  }

  measure: count_sendouts_add {
    hidden: no
    label: "Count of Sends (Additive)"
    description: "Additive number of emails send for group of campaigns"
    group_label: "Campaign view"
    view_label: "Campaigns"
    type: count_distinct
    sql: CONCAT(${email}, ${campaign_name}) ;;
  }

  measure: count_open_add {
    hidden: no
    label: "Count of Unique Open (Additive)"
    description: "Additive number of emails opened for group of campaigns"
    group_label: "Campaign view"
    view_label: "Campaigns"
    type: count_distinct
    sql: CONCAT(CASE WHEN ${TABLE}.email_opened THEN ${TABLE}.email ELSE NULL END, ${campaign_name}) ;;
  }


# Note(Patrick): NEVER CHANGE IT
  measure: count_email_unique_open {
    hidden: no
    label: "Count Unique Opened"
    description: "Count of unique emails when email was opened"
    group_label: "Campaign view"
    view_label: "Campaigns"
    type: count_distinct
    sql: CASE WHEN ${TABLE}.email_opened THEN ${TABLE}.email ELSE NULL END ;;
  }


  measure: count_email_opened_pct {
    hidden: no
    group_label: "Campaign view"
    view_label: "Campaigns"
    label: "Unique Open Rate %"
    description: "Percentage of opened emails (per received emails)"
    value_format: "0.0%"
    type: number
    sql:  ${count_email_unique_open}/${count_receivers} ;;
  }

# Note(Patrick): NEVER CHANGE IT
  measure: count_hard_bounce {
    hidden: no
    group_label: "Campaign view"
    view_label: "Campaigns"
    description: "Count of unique emails when there was hard bounce"
    type: count_distinct
    sql: CASE WHEN ${is_hard_bounce} THEN ${TABLE}.email ELSE NULL END ;;
  }

# Note(Patrick): NEVER CHANGE IT
  measure: count_bounce {
    hidden: no
    group_label: "Campaign view"
    description: "Count of unique emails when there was any bounce"
    view_label: "Campaigns"
    type: count_distinct
    sql: CASE WHEN ${is_hard_bounce} OR ${is_soft_bounce} THEN ${TABLE}.email ELSE NULL END ;;
  }

  measure: count_receivers {
    hidden: no
    label: "Count of Receivers (delivered)"
    group_label: "Campaign view"
    description: "Receive = Sendouts - bounces (also - delivered)"
    view_label: "Campaigns"
    type: number
    sql: ${count_sendouts} - ${count_bounce} ;;
  }

  measure: count_receivers_add {
    hidden: no
    label: "Count of Receivers (Additive)"
    description: "Additive number of emails delivered for group of campaigns"
    group_label: "Campaign view"
    view_label: "Campaigns"
    type: count_distinct
    sql: CONCAT(CASE WHEN ${TABLE}.is_hard_bounce = False AND ${TABLE}.is_soft_bounce = False
      THEN ${TABLE}.email ELSE NULL END, ${campaign_name}) ;;
  }

  measure: count_click_add {
    hidden: no
    label: "Count of Unique click (Additive)"
    description: "Additive number of emails clicked for group of campaigns"
    group_label: "Campaign view"
    view_label: "Campaigns"
    type: count_distinct
    sql: CONCAT(CASE WHEN ${number_of_total_clicks} > 0
      THEN ${TABLE}.email ELSE NULL END, ${campaign_name}) ;;
  }

  measure: open_pct_add {
    hidden: no
    label: "Unique Open Rate % (Additive)"
    description: "Percentage of opened emails per receivers - additive"
    group_label: "Campaign view"
    view_label: "Campaigns"
    value_format: "0.0%"
    type: number
    sql:  ${count_open_add}/${count_receivers_add} ;;
  }

  measure: click_pct_add {
    hidden: no
    label: "Unique Click Rate % (Additive)"
    description: "Percentage of clicked emails per receivers - additive"
    group_label: "Campaign view"
    view_label: "Campaigns"
    value_format: "0.0%"
    type: number
    sql:  ${count_click_add}/${count_receivers_add} ;;
  }



  measure: count_bounce_pct {
    hidden: no
    group_label: "Campaign view"
    view_label: "Campaigns"
    description: "Percentage of bounced emails"
    label: "Bounce Rate %"
    value_format: "0.0%"
    type: number
    sql: ${count_bounce}/${count_sendouts} ;;
  }

  measure: delivery_pct {
    hidden: no
    group_label: "Campaign view"
    view_label: "Campaigns"
    description: "Percentage succesful deliveries per all sendouts"
    label: "Delivery Rate %"
    value_format: "0.0%"
    type: number
    sql: ${count_receivers}/${count_sendouts} ;;
  }

  measure: count_hard_bounce_pct {
    hidden: no
    group_label: "Campaign view"
    view_label: "Campaigns"
    description: "Percentage of hard bounced emails"
    label: "Hard Bounce Rate %"
    value_format: "0.0%"
    type: number
    sql: ${count_hard_bounce}/${count_sendouts} ;;
  }

# Note(Patrick): NEVER CHANGE IT
  measure: count_soft_bounce {
    hidden: no
    group_label: "Campaign view"
    description: "Count of unique emails when there was soft bounce"
    view_label: "Campaigns"
    type: count_distinct
    sql: CASE WHEN ${is_soft_bounce} THEN ${TABLE}.email ELSE NULL END ;;
  }

  measure: count_soft_bounce_pct {
    hidden: no
    group_label: "Campaign view"
    view_label: "Campaigns"
    label: "Soft Bounce Rate %"
    description: "Percentage of soft bounced emails"
    value_format: "0.0%"
    type: number
    sql: ${count_soft_bounce}/${count_sendouts} ;;
  }

# NOTE(Patrick): NEVER CHANGE IT
  measure: count_unique_clicked {
    hidden: no
    label: "Count Unique Clicked"
    description: "Count of emails with link click"
    view_label: "Campaigns"
    group_label: "Campaign view"
    type: count_distinct
    sql: CASE WHEN ${number_of_total_clicks} > 0 THEN ${TABLE}.email ELSE NULL END;;
  }

  measure: total_count_clicked {
    hidden: no
    label: "Total clicks"
    type: sum_distinct
    description: "Total number of clicks in email"
    sql_distinct_key: ${campaign_email_id} ;;
    view_label: "Campaigns"
    group_label: "Campaign view"
    sql: ${TABLE}.number_of_total_clicks ;;
  }

  measure: count_clicked_pct {
    hidden: no
    group_label: "Campaign view"
    view_label: "Campaigns"
    value_format: "0.0%"
    label: "Unique Click Rate %"
    description: "Percentage of clicked emails (clicks per sendouts)"
    type: number
    sql: ${count_unique_clicked}/${count_receivers} ;;
  }

  measure: offline_sum_amount_usd {
    hidden: no
    label: "Offline Sales Amount USD"
    view_label: "Retail Sales"
    description: "Offline USD sales attributed to a campaign. Works correctly only with campaign filter or dimension"
    value_format: "$#,##0.00"
    type: sum
    filters: {
      field: transaction_type
      value: "OFFLINE"
    }
    sql: ${TABLE}.d{{days_since_campaign._parameter_value }}_usd_amount ;;
  }


  measure: count_offline_items {
    hidden: no
    label: "Offline items count"
    view_label: "Retail Sales"
    description: "Number of items sold offline attributed to a campaign. Works correctly only with campaign filter or dimension"
    type: sum
    filters: {
      field: transaction_type
      value: "OFFLINE"
    }
    sql: ${TABLE}.d{{days_since_campaign._parameter_value }}_items_count ;;
  }

  measure: count_offline_transactions {
    hidden: no
    label: "Offline Transaction Count"
    view_label: "Retail Sales"
    description: "Number of offline transactions attributed to a campaign. Works correctly only with campaign filter or dimension"
    type: count_distinct
    filters: {
      field: transaction_type
      value: "OFFLINE"
    }
    sql: ${TABLE}.d{{days_since_campaign._parameter_value }}_transaction_id ;;
  }

  measure: count_offline_transacted_customers{
    hidden: no
    label: "Offline Transacted Customer Count"
    view_label: "Retail Sales"
    description: "Number of customers who made offline transaction attributed to a campaign.
    Works correctly only with campaign filter or dimension"
    type: count_distinct
    filters: {
      field: transaction_type
      value: "OFFLINE"
    }
    sql: ${TABLE}.d{{days_since_campaign._parameter_value }}_transacted_customer ;;
  }



  measure: ecomm_sum_amount_usd {
    hidden: no
    label: "Ecomm Sales Amount USD"
    description: "Ecommerce USD sales attributed to a campaign. Works correctly only with campaign filter or dimension"
    view_label: "Retail Sales"
    value_format: "$#,##0.00"
    type: sum
    filters: {
      field: transaction_type
      value: "ECOMM"
    }
    sql: ${TABLE}.d{{days_since_campaign._parameter_value }}_usd_amount ;;
  }


  measure: count_ecomm_items {
    hidden: no
    label: "Ecomm items count"
    view_label: "Retail Sales"
    description: "Number of items sold online attributed to a campaign. Works correctly only with campaign filter or dimension"
    type: sum
    filters: {
      field: transaction_type
      value: "ECOMM"
    }
    sql: ${TABLE}.d{{days_since_campaign._parameter_value }}_items_count ;;
  }

  measure: count_ecomm_transactions {
    hidden: no
    label: "Ecomm Transaction Count"
    view_label: "Retail Sales"
    description: "Number of online transactions attributed to a campaign. Works correctly only with campaign filter or dimension"
    type: count_distinct
    filters: {
      field: transaction_type
      value: "ECOMM"
    }
    sql: ${TABLE}.d{{days_since_campaign._parameter_value }}_transaction_id ;;
  }

  measure: count_ecomm_transacted_customers{
    hidden: no
    label: "Ecomm Transacted Customer Count"
    view_label: "Retail Sales"
    description: "Number of customers who made online transaction attributed to a campaign.
    Works correctly only with campaign filter or dimension"
    type: count_distinct
    filters: {
      field: transaction_type
      value: "ECOMM"
    }
    sql: ${TABLE}.d{{days_since_campaign._parameter_value }}_transacted_customer ;;
  }



  measure: total_sum_amount_usd {
    hidden: no
    label: "Total Sales Amount USD"
    description: "All USD sales attributed to a campaign. Works correctly only with campaign filter or dimension"
    view_label: "Retail Sales"
    value_format: "$#,##0.00"
    type: sum
    sql: ${TABLE}.d{{days_since_campaign._parameter_value }}_usd_amount ;;
  }


  measure: total_items {
    hidden: no
    label: "Total items count"
    description: "Number of items sold attributed to a campaign. Works correctly only with campaign filter or dimension"
    view_label: "Retail Sales"
    type: sum
    sql: ${TABLE}.d{{days_since_campaign._parameter_value }}_items_count ;;
  }

  measure: total_transactions {
    hidden: no
    label: "Total Transaction Count"
    description: "Number of transactions attributed to a campaign. Works correctly only with campaign filter or dimension"
    view_label: "Retail Sales"
    type: count_distinct
    sql: ${TABLE}.d{{days_since_campaign._parameter_value }}_transaction_id ;;
  }

  measure: count_transacted_customers{
    hidden: no
    label: "Total Transacted Customer Count"
    description: "Number of customers who made a transaction attributed to a campaign.
    Works correctly only with campaign filter or dimension"
    view_label: "Retail Sales"
    type: count_distinct
    sql: ${TABLE}.d{{days_since_campaign._parameter_value }}_transacted_customer ;;
  }


  measure: count_campaigns {
    hidden: no
    group_label: "Customer view"
    view_label: "Campaigns"
    type: count_distinct
    sql: ${campaign_name} ;;
  }

  measure: first_campaign_date {
    hidden: yes
    group_label: "Customer view"
    view_label: "Campaigns"
    type: min
    sql: ${email_date_sent} ;;
  }

  measure: last_campaign_date {
    hidden: yes
    group_label: "Customer view"
    view_label: "Campaigns"
    type: max
    sql: ${email_date_sent} ;;
  }

  measure: days_between_first_and_last_campaign {
    hidden: no
    group_label: "Customer view"
    view_label: "Campaigns"
    type: number
    sql: date_diff(${last_campaign_date}, ${first_campaign_date}, day) ;;
  }

  measure: campaign_frequency {
    hidden: no
    group_label: "Customer view"
    view_label: "Campaigns"
    value_format: "0.00%"
    type: number
    sql: IFNULL(${count_campaigns}/NULLIF(${days_between_first_and_last_campaign},0),0)  ;;
  }

}

view: campaign_frs_sales_base {
  sql_table_name: `chb-prod-data-cust.prod_campaigns.campaign_frs_sales_base` ;;

  dimension: pk {
    primary_key: yes
    hidden: yes
    type: string
    sql: ${TABLE}.pk ;;
  }

  # dimension: frs_brand_customer_unique_id {
  #   hidden: yes
  #   type: string
  #   sql: ${TABLE}.brand_customer_unique_id ;;
  # }

  dimension: frs_transaction_date {
    label: "Transaction Date - sales only"
    view_label: "Retail Sales"
    hidden: no
    type: date
    sql: CAST(${TABLE}.transaction_date AS TIMESTAMP) ;;
  }

  dimension: frs_total_sales_usd {
    hidden: yes
    type: number
    sql: ${TABLE}.total_sales_usd ;;
  }

  dimension: bu_brand_name {
    hidden: yes
    type: string
    sql: ${TABLE}.bu_brand_name ;;
  }

  dimension: country {
    label: "Country - sales only"
    view_label: "Retail Sales"
    description: "Region split for total sales"
    hidden: no
    type: string
    sql: ${TABLE}.country ;;
  }

  dimension: frs_brand_name {
    label: "Brand Name - sales only"
    view_label: "Retail Sales"
    type: string
    sql: ${TABLE}.brand_name ;;
  }

  dimension: brand_id {
    hidden: yes
    type: string
    sql: ${TABLE}.brand_id ;;
  }

  dimension: vertical {
    label: "Vertical Name - sales only"
    view_label: "Retail Sales"
    hidden: no
    type: string
    sql: ${TABLE}.vertical ;;
  }

  dimension: campaign_count {
    hidden: no
    label: "Campaign count for given day"
    description: "Count of unique campaigns sent on given day"
    type: number
    view_label: "Retail Sales"
    sql: ${TABLE}.unique_campaigns;;
  }

  dimension: daily_campaign_list {
    hidden: no
    label: "Campaign List sent on given day"
    description: "Text field of all campaigns sent on given day"
    type: string
    view_label: "Retail Sales"
    sql: ${TABLE}.campaign_list;;
  }

  dimension: campaign_list {
    hidden: no
    label: "Campaign List impacting sales on given day"
    description: "All campaigns impacting transactions on selected day"
    type: string
    view_label: "Retail Sales"
    sql: ${TABLE}.d{{prod_campaigns_sales_last_send_attribution.days_since_campaign._parameter_value }}_campaign_list;;
  }

  measure: sum_frs_total_sales_usd {
    view_label: "Retail Sales"
    label: "All Sales USD (non campaign related)"
    description: "All sales in given time, not related to campaign attribution. Used as a reference"
    sql_distinct_key: ${pk} ;;
    value_format: "$#,##0.00;($#.00)"
    type: sum_distinct
    sql: ${frs_total_sales_usd} ;;
  }

  measure: total_campaign_sales {
    hidden: yes
    label: "Campaign Sales Amount USD - (Distinct Total from all campaigns)"
    description: "It's aggreagate of sales attributed to any campaign in given time.
    It won't work with campaign dimension or campaign filter correctly.
    To obtain sales per campaign, use 'Total Sales Amount USD'"
    view_label: "Retail Sales"
    value_format: "$#,##0.00"
    sql_distinct_key: ${pk} ;;
    type: sum_distinct
    sql: ${TABLE}.d{{prod_campaigns_sales_last_send_attribution.days_since_campaign._parameter_value }}_campaign_sales;;
  }

  measure: campaign_sales_percentage {
    hidden: no
    label: "Campaign sales per total"
    description: "aggreagate of sales attributed to any campaign in given time
    divided by all sales in given time. It won't work with campaign dimension or campaign filter correctly"
    view_label: "Retail Sales"
    value_format: "0.0%"
    type: number
    sql: IFNULL(${total_campaign_sales}/NULLIF(${sum_frs_total_sales_usd},0),0);;
  }

}

view: stg_campaigns_sales_ga {
  view_label: "Google Analytics"
  sql_table_name: `chb-prod-data-cust.prod_campaigns.stg_campaigns_sales_ga`;;


  dimension: ga_campaign_name {
    hidden: yes
    type: string
    sql: ${TABLE}.ga_campaign_name ;;
  }

  dimension: ga_email {
    hidden: yes
    type: string
    sql: ${TABLE}.ga_email ;;
  }

  dimension: ga_usd_amount {
    label: "GA USD Amount"
    hidden: no
    type: number
    sql: ${TABLE}.ga_usd_amount ;;
  }

  dimension: ga_items_count {
    label: "GA Items Count"
    hidden: no
    type: number
    sql: ${TABLE}.ga_items_count ;;
  }

  dimension: ga_transaction_id {
    label: "GA Order ID"
    hidden: no
    type: string
    sql: ${TABLE}.ga_transaction_id ;;
  }

  dimension: ga_products_id {
    label: "GA Product ID"
    hidden: no
    type: string
    sql: ${TABLE}.ga_products_id ;;
  }

  dimension: ga_transacted_customer {
    label: "GA Customer ID"
    hidden: yes
    type: string
    sql: ${TABLE}.ga_transacted_customer ;;
  }

  measure: sum_ga_usd_amount {
    hidden: no
    label: "GA Total USD Amount"
    sql_distinct_key: CONCAT(${ga_transaction_id},'_', ${ga_products_id}) ;;
    type: sum_distinct
    sql: ${ga_usd_amount} ;;
  }

  measure: ga_total_customers {
    hidden: no
    label: "GA Total Customers"
    sql_distinct_key: ${ga_transaction_id} ;;
    type: count_distinct
    sql: ${ga_transacted_customer} ;;
  }

  measure: ga_total_orders {
    hidden: no
    label: "GA Total Orders"
    sql_distinct_key: ${ga_transaction_id}  ;;
    type: count_distinct
    sql: ${ga_transaction_id} ;;
  }



}
