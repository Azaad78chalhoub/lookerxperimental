
view: current_segments {
  sql_table_name: `chb-prod-data-cust.prod_segmentation.daily_segmentation`
  ;;

  dimension: brand_customer_unique_id {
    label: "brand_customer_unique_id"
    view_label: "Segmentation"
    hidden: yes
    primary_key: yes
    type: string
    sql: ${TABLE}.brand_customer_unique_id;;
  }

  dimension: lifecycle {
    view_label: "Segmentation"
    group_label: "Current"
    label: "Lifecycle"
    type: string
    sql: ${TABLE}.lifecycle;;
  }

  dimension: recency {
    view_label: "Segmentation"
    group_label: "Current"
    label: "Recency"
    hidden: yes
    type: number
    sql: ${TABLE}.recency;;
  }

  dimension: lifecycle_reason_description_detailed {
    view_label: "Segmentation"
    description: "Detailed reason why given customer has its lifecycle segment"
    group_label: "Explanation"
    label: "Lifecycle reason description detailed"
    hidden: no
    type: string
    sql: ${TABLE}.lifecycle_reason_description_detailed;;
  }

  dimension: lifecycle_reason_description {
    view_label: "Segmentation"
    description: "General reason why given customer has its lifecycle segment"
    group_label: "Explanation"
    label: "Lifecycle reason description"
    hidden: no
    type: string
    sql: ${TABLE}.lifecycle_reason_description;;
  }

  dimension: brand_medium_value_threshold {
    view_label: "Segmentation"
    description: "Brands medium value threshold for mean season sales"
    group_label: "Explanation"
    label: "Brand medium value threshold"
    hidden: no
    type: number
    sql: ${TABLE}.brand_medium_value_threshold;;
  }

  dimension: brand_big_value_threshold {
    view_label: "Segmentation"
    description: "Brands big value threshold for mean season sales"
    group_label: "Explanation"
    label: "Brand big value threshold"
    hidden: no
    type: number
    sql: ${TABLE}.brand_big_value_threshold;;
  }

  dimension: brand_very_big_value_threshold {
    view_label: "Segmentation"
    description: "Brands very big value threshold for mean season sales"
    group_label: "Explanation"
    label: "Brand very big value threshold"
    hidden: no
    type: number
    sql: ${TABLE}.brand_very_big_value_threshold;;
  }

  dimension: brand_active_per_possible_median {
    view_label: "Segmentation"
    description: "Brands median for active_per_possible"
    group_label: "Explanation"
    label: "Brand active per possible median"
    hidden: no
    type: number
    sql: ${TABLE}.brand_active_per_possible_median;;
  }

  dimension: brand_active_per_possible_q3 {
    view_label: "Segmentation"
    description: "Brands Q3 for active_per_possible"
    group_label: "Explanation"
    label: "Brand active per possible q3"
    hidden: no
    type: number
    sql: ${TABLE}.brand_active_per_possible_q3;;
  }

  dimension: season_length {
    view_label: "Segmentation"
    description: "Brands season length in days"
    group_label: "Explanation"
    label: "Season Length"
    hidden: no
    type: number
    sql: ${TABLE}.season_length;;
  }

  dimension: churn_cutoff {
    view_label: "Segmentation"
    description: "Brands churn cutoff in days"
    group_label: "Explanation"
    label: "Churn Cutoff"
    hidden: no
    type: number
    sql: ${TABLE}.churn_cutoff;;
  }


  dimension: experience {
    view_label: "Segmentation"
    group_label: "Current"
    label: "Experience"
    hidden: yes
    type: string
    sql: ${TABLE}.experience;;
  }

  dimension: value {
    view_label: "Segmentation"
    group_label: "Current"
    label: "Value"
    type: string
    sql: ${TABLE}.value;;
  }

  dimension: vip {
    view_label: "Segmentation"
    group_label: "Current"
    label: "VIP"
    type: yesno
    sql: ${TABLE}.vip;;
  }

  dimension: score {
    view_label: "Segmentation"
    group_label: "Current"
    label: "Score"
    type: number
    sql: ${TABLE}.score;;
  }

  measure: avg_recency {
    view_label: "Segmentation"
    group_label: "Current"
    label: "Average recency"
    type: average_distinct
    value_format: "0"
    sql_distinct_key: ${brand_customer_unique_id} ;;
    sql: ${TABLE}.recency;;
  }

  measure: avg_tenure {
    view_label: "Segmentation"
    group_label: "Current"
    label: "Average tenure"
    type: average_distinct
    value_format: "0"
    sql_distinct_key: ${brand_customer_unique_id} ;;
    sql: ${TABLE}.tenure;;
  }

  measure: min_recency {
    view_label: "Segmentation"
    group_label: "Current"
    label: "Minimum recency"
    type: min
    sql: ${TABLE}.recency;;
  }

  measure: max_recency {
    view_label: "Segmentation"
    group_label: "Current"
    label: "Maximum recency"
    type: max
    value_format: "0"
    sql: ${TABLE}.recency;;
  }

  measure: avg_active_per_possible {
    view_label: "Segmentation"
    group_label: "Current"
    label: "Average Active Per Possible"
    type: average_distinct
    value_format: "0.00"
    sql_distinct_key: ${brand_customer_unique_id} ;;
    sql: ${TABLE}.active_per_possible;;
  }

  measure: avg_mean_season_sales {
    view_label: "Segmentation"
    group_label: "Current"
    label: "Average Mean Season Sales"
    type: average_distinct
    value_format: "0.00"
    sql_distinct_key: ${brand_customer_unique_id} ;;
    sql: ${TABLE}.mean_season_sales;;
  }

}
