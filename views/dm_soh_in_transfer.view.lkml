view: dm_soh_in_transfer {
  sql_table_name: `chb-prod-supplychain-data.prod_supply_chain.dm_soh_in_transfer`
    ;;

  dimension: area_name {
    type: string
    sql: ${TABLE}.area_name ;;
  }

  dimension: atrb_boy_girl {
    type: string
    sql: ${TABLE}.atrb_boy_girl ;;
  }

  dimension: attrb_color {
    type: string
    sql: ${TABLE}.attrb_color ;;
  }

  dimension: attrb_made_of {
    type: string
    sql: ${TABLE}.attrb_made_of ;;
  }

  dimension: attrb_theme {
    type: string
    sql: ${TABLE}.attrb_theme ;;
  }

  dimension: barcode {
    type: string
    sql: ${TABLE}.barcode ;;
  }

  dimension: brand {
    type: string
    sql: ${TABLE}.brand ;;
  }

  dimension: business_unit {
    type: string
    sql: ${TABLE}.business_unit ;;
  }

  dimension: cancelled_qty_from {
    type: number
    sql: ${TABLE}.cancelled_qty_from ;;
  }

  dimension: cancelled_qty_to {
    type: number
    sql: ${TABLE}.cancelled_qty_to ;;
  }

  dimension: chain_name {
    type: string
    sql: ${TABLE}.chain_name ;;
  }

  dimension: channel_name {
    type: string
    sql: ${TABLE}.channel_name ;;
  }

  dimension: city {
    type: string
    sql: ${TABLE}.city ;;
  }

  dimension: class {
    type: number
    sql: ${TABLE}.class ;;
  }

  dimension: class_name {
    type: string
    sql: ${TABLE}.class_name ;;
  }

  dimension: cogs_last182_value_sum {
    type: number
    sql: ${TABLE}.cogs_last182_value_sum ;;
  }

  dimension: cogs_last30_value_sum {
    type: number
    sql: ${TABLE}.cogs_last30_value_sum ;;
  }

  dimension: cogs_last365_value_sum {
    type: number
    sql: ${TABLE}.cogs_last365_value_sum ;;
  }

  dimension: cogs_last90_value_sum {
    type: number
    sql: ${TABLE}.cogs_last90_value_sum ;;
  }

  dimension: country_desc {
    type: string
    sql: ${TABLE}.country_desc ;;
  }

  dimension: country_id {
    type: string
    sql: ${TABLE}.country_id ;;
  }

  dimension: country_of_manu {
    type: string
    sql: ${TABLE}.country_of_manu ;;
  }

  dimension: currency_code {
    type: string
    sql: ${TABLE}.currency_code ;;
  }

  dimension_group: date_mc {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.date_mc ;;
  }

  dimension: dept_name {
    type: string
    sql: ${TABLE}.dept_name ;;
  }

  dimension: dept_no {
    type: number
    sql: ${TABLE}.dept_no ;;
  }

  dimension: dgr_ind {
    type: string
    sql: ${TABLE}.dgr_ind ;;
  }

  dimension: district_name {
    type: string
    sql: ${TABLE}.district_name ;;
  }

  dimension: distro_qty_from {
    type: number
    sql: ${TABLE}.distro_qty_from ;;
  }

  dimension: distro_qty_to {
    type: number
    sql: ${TABLE}.distro_qty_to ;;
  }

  dimension: division {
    type: string
    sql: ${TABLE}.division ;;
  }

  dimension: division_no {
    type: number
    sql: ${TABLE}.division_no ;;
  }

  dimension: entity_type {
    type: string
    sql: ${TABLE}.entity_type ;;
  }

  dimension: fill_qty_from {
    type: number
    sql: ${TABLE}.fill_qty_from ;;
  }

  dimension: fill_qty_to {
    type: number
    sql: ${TABLE}.fill_qty_to ;;
  }

  dimension: gender {
    type: string
    sql: ${TABLE}.gender ;;
  }

  dimension: grey_mkt_ind {
    type: string
    sql: ${TABLE}.grey_mkt_ind ;;
  }

  dimension: group_name {
    type: string
    sql: ${TABLE}.group_name ;;
  }

  dimension: group_no {
    type: number
    sql: ${TABLE}.group_no ;;
  }

  dimension: item {
    type: string
    sql: ${TABLE}.item ;;
  }

  dimension: item_desc {
    type: string
    sql: ${TABLE}.item_desc ;;
  }

  dimension: item_style {
    type: string
    sql: ${TABLE}.item_style ;;
  }

  dimension: line {
    type: string
    sql: ${TABLE}.line ;;
  }

  dimension: loc {
    type: number
    sql: ${TABLE}.loc ;;
  }

  dimension: loc_name {
    type: string
    sql: ${TABLE}.loc_name ;;
  }

  dimension: mall_name {
    type: string
    sql: ${TABLE}.mall_name ;;
  }

  dimension: module {
    type: string
    sql: ${TABLE}.module ;;
  }

  dimension: physical_wh {
    type: number
    sql: ${TABLE}.physical_wh ;;
  }

  dimension: received_qty_from {
    type: number
    sql: ${TABLE}.received_qty_from ;;
  }

  dimension: received_qty_to {
    type: number
    sql: ${TABLE}.received_qty_to ;;
  }

  dimension: reconciled_qty_from {
    type: number
    sql: ${TABLE}.reconciled_qty_from ;;
  }

  dimension: reconciled_qty_to {
    type: number
    sql: ${TABLE}.reconciled_qty_to ;;
  }

  dimension: recurrence {
    type: string
    sql: ${TABLE}.recurrence ;;
  }

  dimension: region_name {
    type: string
    sql: ${TABLE}.region_name ;;
  }

  dimension: sales_last182_qty_sum {
    type: number
    sql: ${TABLE}.sales_last182_qty_sum ;;
  }

  dimension: sales_last182_value_sum {
    type: number
    sql: ${TABLE}.sales_last182_value_sum ;;
  }

  dimension: sales_last30_qty_sum {
    type: number
    sql: ${TABLE}.sales_last30_qty_sum ;;
  }

  dimension: sales_last30_value_sum {
    type: number
    sql: ${TABLE}.sales_last30_value_sum ;;
  }

  dimension: sales_last365_qty_sum {
    type: number
    sql: ${TABLE}.sales_last365_qty_sum ;;
  }

  dimension: sales_last365_value_sum {
    type: number
    sql: ${TABLE}.sales_last365_value_sum ;;
  }

  dimension: sales_last90_qty_sum {
    type: number
    sql: ${TABLE}.sales_last90_qty_sum ;;
  }

  dimension: sales_last90_value_sum {
    type: number
    sql: ${TABLE}.sales_last90_value_sum ;;
  }

  dimension: sap_item_code {
    type: string
    sql: ${TABLE}.sap_item_code ;;
  }

  dimension: season_desc {
    type: string
    sql: ${TABLE}.season_desc ;;
  }

  dimension: selected_qty_from {
    type: number
    sql: ${TABLE}.selected_qty_from ;;
  }

  dimension: selected_qty_to {
    type: number
    sql: ${TABLE}.selected_qty_to ;;
  }

  dimension: sep_category {
    hidden: yes
    sql: ${TABLE}.sep_category ;;
  }

  dimension: ship_qty_from {
    type: number
    sql: ${TABLE}.ship_qty_from ;;
  }

  dimension: ship_qty_to {
    type: number
    sql: ${TABLE}.ship_qty_to ;;
  }

  dimension: size_uda {
    type: string
    sql: ${TABLE}.size_uda ;;
  }

  dimension: standard_uom {
    type: string
    sql: ${TABLE}.standard_uom ;;
  }

  dimension: state {
    type: string
    sql: ${TABLE}.state ;;
  }

  dimension: stock_on_hand {
    type: number
    sql: ${TABLE}.stock_on_hand ;;
  }

  dimension: store_format {
    type: number
    sql: ${TABLE}.store_format ;;
  }

  dimension: sub_line {
    type: string
    sql: ${TABLE}.sub_line ;;
  }

  dimension: subclass {
    type: number
    sql: ${TABLE}.subclass ;;
  }

  dimension: subclass_name {
    type: string
    sql: ${TABLE}.subclass_name ;;
  }

  dimension: sup_class {
    type: string
    sql: ${TABLE}.sup_class ;;
  }

  dimension: sup_subclass {
    type: string
    sql: ${TABLE}.sup_subclass ;;
  }

  dimension: taxo_class {
    type: string
    sql: ${TABLE}.taxo_class ;;
  }

  dimension: taxo_subclass {
    type: string
    sql: ${TABLE}.taxo_subclass ;;
  }

  dimension: tsf_qty_from {
    type: number
    sql: ${TABLE}.tsf_qty_from ;;
  }

  dimension: tsf_qty_to {
    type: number
    sql: ${TABLE}.tsf_qty_to ;;
  }

  dimension: uda_16_attribute {
    type: string
    sql: ${TABLE}.uda_16_attribute ;;
  }

  dimension: uda_zone {
    type: string
    sql: ${TABLE}.uda_zone ;;
  }

  dimension: unit_cost_usd {
    type: number
    sql: ${TABLE}.unit_cost_usd ;;
  }

  dimension: usage_specificity {
    type: string
    sql: ${TABLE}.usage_specificity ;;
  }

  dimension: vpn {
    type: string
    sql: ${TABLE}.vpn ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      area_name,
      mall_name,
      loc_name,
      group_name,
      district_name,
      region_name,
      class_name,
      chain_name,
      channel_name,
      dept_name,
      subclass_name
    ]
  }
}

view: dm_soh_in_transfer__sep_category {
  dimension: item {
    type: string
    sql: ${TABLE}.ITEM ;;
  }

  dimension: sep_category {
    type: string
    sql: ${TABLE}.SEP_CATEGORY ;;
  }

  dimension: uda_value {
    type: number
    sql: ${TABLE}.UDA_VALUE ;;
  }
}
