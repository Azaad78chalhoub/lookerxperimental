view: stg_comm_sfcc_combined_sales_orders_items {
  sql_table_name: `chb-prod-stage-comm.prod_comm.stg_comm_sfcc_combined_sales_orders_items`
    ;;

  dimension: amount_refunded {
    type: number
    sql: ${TABLE}.amount_refunded ;;
  }

  dimension: brand {
    type: string
    sql: ${TABLE}.brand ;;
  }

  dimension: canceled_amount_local {
    type: number
    sql: ${TABLE}.canceled_amount_local ;;
  }

  dimension: canceled_amount_usd {
    type: number
    sql: ${TABLE}.canceled_amount_usd ;;
  }

  dimension: city {
    type: string
    sql: ${TABLE}.city ;;
  }

  dimension: customer_email {
    type: string
    sql: ${TABLE}.customer_email ;;
  }

  dimension: discount_amount {
    type: number
    sql: ${TABLE}.discount_amount ;;
  }

  dimension: discount_refunded {
    type: number
    sql: ${TABLE}.discount_refunded ;;
  }

  dimension: free_gift_bag {
    type: string
    sql: ${TABLE}.Free_Gift_Bag ;;
  }

  dimension: free_greeting_card {
    type: string
    sql: ${TABLE}.Free_Greeting_Card ;;
  }

  dimension: gross_local {
    type: number
    sql: ${TABLE}.gross_local ;;
  }

  dimension: gross_net_sales {
    type: number
    sql: ${TABLE}.gross_net_sales ;;
  }

  dimension: gross_net_sales_usd {
    type: number
    sql: ${TABLE}.gross_net_sales_usd ;;
  }

  dimension: gross_revenue_amount_local {
    type: number
    sql: ${TABLE}.gross_revenue_amount_local ;;
  }

  dimension: gross_revenue_amount_usd {
    type: number
    sql: ${TABLE}.gross_revenue_amount_usd ;;
  }

  dimension: gross_usd {
    type: number
    sql: ${TABLE}.gross_usd ;;
  }

  dimension: item_comments {
    type: string
    sql: ${TABLE}.item_comments ;;
  }

  dimension_group: order {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.order_date ;;
  }

  dimension_group: order_datetime {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.order_datetime AS TIMESTAMP) ;;
  }

  dimension: order_id {
    type: string
    sql: ${TABLE}.order_id ;;
  }

  dimension: price {
    type: number
    sql: ${TABLE}.price ;;
  }

  dimension: primary_key {
    type: string
    sql: ${TABLE}.primary_key ;;
  }

  dimension: qty_canceled {
    type: number
    sql: ${TABLE}.qty_canceled ;;
  }

  dimension: qty_ordered {
    type: number
    sql: ${TABLE}.qty_ordered ;;
  }

  dimension: qty_refunded {
    type: number
    sql: ${TABLE}.qty_refunded ;;
  }

  dimension: qty_shipped {
    type: number
    sql: ${TABLE}.qty_shipped ;;
  }

  dimension_group: record {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.record_date ;;
  }

  dimension_group: record_datetime {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.record_datetime AS TIMESTAMP) ;;
  }

  dimension: record_type {
    type: string
    sql: ${TABLE}.record_type ;;
  }

  dimension: refund_amount_local {
    type: number
    sql: ${TABLE}.refund_amount_local ;;
  }

  dimension: refund_amount_usd {
    type: number
    sql: ${TABLE}.refund_amount_usd ;;
  }

  dimension: shipment_net_sales {
    type: number
    sql: ${TABLE}.shipment_net_sales ;;
  }

  dimension: shipment_net_sales_usd {
    type: number
    sql: ${TABLE}.shipment_net_sales_usd ;;
  }

  dimension: shipped_local {
    type: number
    sql: ${TABLE}.shipped_local ;;
  }

  dimension: shipped_rev_amount_local {
    type: number
    sql: ${TABLE}.shipped_rev_amount_local ;;
  }

  dimension: shipped_rev_amount_usd {
    type: number
    sql: ${TABLE}.shipped_rev_amount_usd ;;
  }

  dimension: shipped_usd {
    type: number
    sql: ${TABLE}.shipped_usd ;;
  }

  dimension: sku {
    type: string
    sql: ${TABLE}.sku ;;
  }

  dimension: source {
    type: string
    sql: ${TABLE}.source ;;
  }

  dimension: status {
    type: string
    sql: ${TABLE}.status ;;
  }

  dimension: store_country {
    type: string
    sql: ${TABLE}.store_country ;;
  }

  dimension: store_id {
    type: string
    sql: ${TABLE}.store_id ;;
  }

  dimension: tender_type_group {
    type: string
    sql: ${TABLE}.tender_type_group ;;
  }

  dimension: vertical {
    type: string
    sql: ${TABLE}.vertical ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
