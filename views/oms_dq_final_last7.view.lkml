view: oms_dq_final_last7 {
  sql_table_name: `chb-exp-004.DQ_final_rule_score.OMS_DQ_Final_Last7`
    ;;

  dimension: brand {
    type: string
    sql: ${TABLE}.brand ;;
  }

  dimension_group: dq {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.dq_date ;;
  }

  dimension: dq_dimension {
    type: string
    sql: ${TABLE}.dq_dimension ;;
  }

  dimension: num_rows_passed {
    type: number
    sql: ${TABLE}.num_rows_passed ;;
  }

  dimension: num_rows_processed {
    type: number
    sql: ${TABLE}.num_rows_processed ;;
  }

  dimension: rule_name {
    type: string
    sql: ${TABLE}.rule_name ;;
  }

  measure: count {
    type: count
    drill_fields: [rule_name]
  }
}
