test: distr_hist_sales_is_accurate {
  explore_source: dm_distr_sales {
    column: revenue_net_usd {
      field: dm_distr_sales.revenue_net_usd
    }
    column: invoiced_quantity_sum {
      field: dm_distr_sales.invoiced_quantity_sum
    }
    filters: {

      field: dm_distr_sales.invoice_date
      value: "2020/04/01 to 2020/06/21"
    }
    filters: {
      field: dm_distr_sales.district
      value: "FMCG"
    }

  }
  assert: saleqty_rev_is_expected_value {
    expression: round(${dm_distr_sales.revenue_net_usd},0) = 763251  AND  round(${dm_distr_sales.invoiced_quantity_sum},0) = 270613 ;;
  }
}


test: reversal_hist_values_are_accurate {
  explore_source: new_reversals {

    column: reversal_local {
      field: new_reversals.reversal_local
    }

    column: reversal_usd {
      field: new_reversals.reversal_usd
    }

    filters: {

      field:  new_reversals.date_date
      value: "2020/07/31"
    }

    filters: {

      field:  new_reversals.org_num
      value: "8001"
    }

  }
  assert: reversal_hist_values_as_expected {
    expression: round(${new_reversals.reversal_local},0) = 49835 AND  round(${new_reversals.reversal_usd},0) = 13570  ;;
  }
}







test: salestock_hist_values_are_accurate {
  explore_source: fact_soh_sales_poc {
    column: total_sales_amt_usd {
      field: fact_soh_sales_poc.total_sales_amt_usd
    }
    column: total_quantity_sold {
      field: fact_soh_sales_poc.total_quantity_sold
    }

    column: last_day_stock_quantity {
      field: fact_soh_sales_poc.last_day_stock_quantity
    }

    column: last_day_stock_value {
      field: fact_soh_sales_poc.last_day_stock_value
    }

    filters: {

      field:  fact_soh_sales_poc.stock_date
      value: "2020/07/01"
    }

  }
  assert: salestock_hist_values_as_expected {
    expression: round(${fact_soh_sales_poc.total_sales_amt_usd},0) = 4746662 AND  round(${fact_soh_sales_poc.total_quantity_sold},0) = 162279
      AND  round(${fact_soh_sales_poc.last_day_stock_quantity},0) = 74109502   AND round(${fact_soh_sales_poc.last_day_stock_value},0) = 602663022   ;;
  }
}




test: trans_hist_values_are_accurate {
  explore_source: fact_soh_sales_poc {
    column: total_sales_amt_usd {
      field: fact_soh_sales_poc.total_sales_amt_usd
    }
    column: total_quantity_sold {
      field: fact_soh_sales_poc.total_quantity_sold
    }

    column: last_day_stock_quantity {
      field: fact_soh_sales_poc.last_day_stock_quantity
    }

    column: last_day_stock_value {
      field: fact_soh_sales_poc.last_day_stock_value
    }

    filters: {

      field:  fact_soh_sales_poc.stock_date
      value: "2020/07/01"
    }

  }
  assert: trans_hist_values_as_expected {
    expression: round(${fact_soh_sales_poc.total_sales_amt_usd},0) = 4746662 AND  round(${fact_soh_sales_poc.total_quantity_sold},0) = 162279
      AND  round(${fact_soh_sales_poc.last_day_stock_quantity},0) = 74109502   AND round(${fact_soh_sales_poc.last_day_stock_value},0) = 602663022   ;;
  }
}
