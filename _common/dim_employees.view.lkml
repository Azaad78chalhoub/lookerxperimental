view: dim_employees {
  view_label: "Employees"
  sql_table_name: `chb-prod-supplychain-data.prod_shared_dimensions.dim_employees`
    ;;

  dimension: _file {
    type: string
    hidden: yes
    sql: ${TABLE}._file ;;
  }

  dimension: pk {
    type: string
    primary_key: yes
    hidden: yes
    sql: CONCAT(${_line}, " - ", ${column_0}, " - ",${column_19}) ;;
  }

  dimension_group: _fivetran_synced {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}._fivetran_synced ;;
  }

  dimension: _line {
    type: number
    hidden: yes
    sql: ${TABLE}._line ;;
  }

  dimension_group: _modified {
    hidden: yes
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}._modified ;;
  }

  dimension: column_0 {
    type: string
    label: "Employee-ID"
    sql: ${TABLE}.column_0 ;;
  }

  dimension: column_1 {
    type: string
    hidden: yes
    label: "Salutation"
    sql: ${TABLE}.column_1 ;;
  }

  dimension: column_10 {
    label: "Country"
    hidden: yes
    type: string
    sql: ${TABLE}.column_10 ;;
  }

  dimension: column_12 {
    label: "Contact Number (Official)"
    hidden: yes
    type: string
    sql: ${TABLE}.column_12 ;;
  }

  dimension: column_13 {
    label: "Contact Number (Personal)"
    type: string
    hidden: yes
    sql: ${TABLE}.column_13 ;;
  }

  dimension: column_14 {
    hidden: yes
    type: string
    sql: ${TABLE}.column_14 ;;
  }

  dimension: column_15 {
    hidden: yes
    type: string
    sql: ${TABLE}.column_15 ;;
  }

  dimension: column_16 {
    hidden: yes
    type: string
    sql: ${TABLE}.column_16 ;;
  }

  dimension: column_17 {
    hidden: yes
    type: number
    sql: ${TABLE}.column_17 ;;
  }

  dimension: column_18 {
    hidden: yes
    type: number
    sql: ${TABLE}.column_18 ;;
  }

  dimension: column_19 {
    hidden: yes
    type: string
    sql: ${TABLE}.column_19 ;;
  }

  dimension: column_2 {
    label: "First Name"
    type: string
    hidden: yes
    sql: ${TABLE}.column_2 ;;
  }

  dimension: masked_first_name{
    type: string
    hidden: yes
    label: "Masked FN"
    sql: CONCAT(LPAD(${column_2}, 2), RPAD('*', LENGTH(${column_2})-2, '*'));;
  }

  dimension: full_name {
    type: string
    sql:    {% if _user_attributes['access_sensitive_data'] == 'no' %}
    CONCAT(${masked_first_name}," ",${column_4})
    {% else %}
    CONCAT(${column_2}," ",${column_4})
    {% endif %};;
  }
  dimension: column_20 {
    type: string
    hidden: yes
    sql: ${TABLE}.column_20 ;;
  }

  dimension: column_21 {
    type: string
    hidden: yes
    sql: ${TABLE}.column_21 ;;
  }

  dimension: column_22 {
    type: string
    hidden: yes
    sql: ${TABLE}.column_22 ;;
  }

  dimension: column_23 {
    type: number
    hidden: yes
    sql: ${TABLE}.column_23 ;;
  }

  dimension: column_24 {
    type: number
    hidden: yes
    sql: ${TABLE}.column_24 ;;
  }

  dimension: column_25 {
    type: string
    hidden: yes
    sql: ${TABLE}.column_25 ;;
  }

  dimension: column_26 {
    type: string
    hidden: yes
    label: "Gender"
    sql: ${TABLE}.column_26 ;;
  }

  dimension: column_27 {
    type: string
    hidden: yes
    label: "Age"
    sql: ${TABLE}.column_27 ;;
  }

  dimension: column_28 {
    type: string
    hidden: yes
    sql: ${TABLE}.column_28 ;;
  }

  dimension: column_29 {
    type: string
    hidden: yes
    label: "Nationality"
    sql: ${TABLE}.column_29 ;;
  }

  dimension: column_3 {
    type: string
    hidden: yes
    label: "Middle Name"
    sql: ${TABLE}.column_3 ;;
  }

  dimension: column_30 {
    type: string
    hidden: yes
    sql: ${TABLE}.column_30 ;;
  }

  dimension: column_31 {
    type: string
    hidden: yes
    label: "Date of Birth"
    sql: ${TABLE}.column_31 ;;
  }

  dimension: column_32 {
    type: string
    hidden: yes
    sql: ${TABLE}.column_32 ;;
  }

  dimension: column_33 {
    type: string
    hidden: yes
    label: "Legal Entity"
    sql: ${TABLE}.column_33 ;;
  }

  dimension: column_34 {
    type: string
    hidden: no
    label: "Designation"
    sql: ${TABLE}.column_34 ;;
  }

  dimension: column_4 {
    type: string
    hidden: yes
    label: "Last Name"
    sql: ${TABLE}.column_4 ;;
  }

  dimension: column_5 {
    type: string
    hidden: yes
    label: "Address"
    sql: ${TABLE}.column_5 ;;
  }

  dimension: column_7 {
    type: number
    hidden: yes
    sql: ${TABLE}.column_7 ;;
  }

  dimension: column_8 {
    type: string
    hidden: yes
    label: "Email-ID"
    sql: ${TABLE}.column_8 ;;
  }

  dimension: column_9 {
    type: string
    hidden: yes
    label: "City"
    sql: ${TABLE}.column_9 ;;
  }

  measure: count {
    hidden: yes
    type: count
    drill_fields: []
  }
}
