view: usr_mgt_approved_requests {
  sql_table_name: chb-looker-user-mgt.looker_usr_mgt.view_approved_requests ;;

  dimension: brand {
    type: string
    sql: ${TABLE}.Brand ;;
  }

  dimension: country {
    type: string
    map_layer_name: countries
    sql: ${TABLE}.Country ;;
  }

  dimension: email_address {
    type: string
    sql: ${TABLE}.Email_address ;;
  }

  dimension: first_name {
    type: string
    sql: ${TABLE}.First_Name ;;
  }

  dimension: last_name {
    type: string
    sql: ${TABLE}.Last_Name ;;
  }

  dimension: looker_model {
    type: string
    sql: ${TABLE}.Looker_Model ;;
  }

  dimension_group: timestamp {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.Timestamp ;;
  }

  dimension: vertical {
    type: string
    sql: ${TABLE}.Vertical ;;
  }

  dimension: profile {
    type: string
    sql: ${TABLE}.Profile ;;
  }

  measure: count {
    type: count
    drill_fields: [first_name, last_name]
  }
}
