# START DOCUMENTATION


# This view allows for custom fiscal dates. It generates calendar dates that will join to the relevant explores.
# Analysts/ Developers can add their own fiscal columns (Week, Month, Year). Example is April
# Adjust generated_date_array as necessary (2024-21-31 etc)

# END DOCUMENTATION

view: fiscal_dates {
  derived_table: {
    sql: SELECT
        date,
        extract(week from date_sub(date_trunc(date,week(monday)), interval  12 week)) as april_fiscal_week,
        extract(month from date_sub(date_trunc(date,week(monday)), interval  12 week)) as april_fiscal_month,
        extract(year from date_sub(date_trunc(date,week(monday)), interval  12 week)) as april_fiscal_year,
      from unnest(generate_date_array('2019-01-01', '2023-12-31', interval 1 day)) as date
      order by date
       ;;
  }

  dimension: calendar_date {
    type: date
    datatype: date
    hidden: yes
    label: "Calendar Date"
    sql: ${TABLE}.date ;;
  }

  dimension: april_fiscal_week {
    type: number
    hidden: no
    label: "Fiscal Week (starting April)"
    description: "Fiscal Week Starting Monday of Week containing April 1"
    sql: ${TABLE}.april_fiscal_week ;;
  }

  dimension: april_fiscal_month {
    type: number
    hidden: no
    label: "Fiscal Month (starting April)"
    description: "Fiscal Month Starting Monday of Week containing April 1"
    sql: ${TABLE}.april_fiscal_month ;;
  }

  dimension: april_fiscal_year {
    type: number
    hidden: no
    label: "Fiscal Year (starting April)"
    description: "Fiscal Year Starting Monday of Week containing April 1"
    sql: ${TABLE}.april_fiscal_year ;;
  }

}
