view: levelshoes_budgets_daily {
  sql_table_name: `chb-prod-supplychain-data.prod_supply_chain.levelshoes_budgets_daily`
    ;;

  dimension: pk {
    sql: concat(${TABLE}.day, ${TABLE}.business_unit_code_col, ${TABLE}.zone_col, ${TABLE}.location_col) ;;
    primary_key: yes
    hidden: yes
  }

  dimension_group: date {
    type: time
    hidden: yes
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    sql: ${TABLE}.day ;;
  }

  dimension: business_unit {
    type: string
    description: "Business Unit"
    label: "Business Unit"
    hidden: yes
    sql: ${TABLE}.business_unit_code_col ;;
  }

  dimension: zone {
    type: string
    description: "Zone"
    label: "Zone"
    hidden: yes
    sql: ${TABLE}.zone_col ;;
  }

  dimension: store_code {
    type: number
    description: "Store Code"
    label: "Store"
    hidden: yes
    sql: ${TABLE}.location_col ;;
  }

  measure: r0_sales_budget {
    type: sum
    hidden: no
    label: "R0 Sales Budget"
    sql: ${TABLE}.daily_r0_sales_budget_col ;;
  }

  measure: r1_sales_budget {
    type: sum
    hidden: no
    label: "R1 Sales Budget"
    sql: ${TABLE}.daily_r1_sales_budget_col ;;
  }

  measure: r2_sales_budget {
    type: sum
    hidden: no
    label: "R2 Sales Budget"
    sql: ${TABLE}.daily_r2_sales_budget_col ;;
  }

  measure: r0_margin_budget {
    type: sum
    hidden: no
    label: "R0 Margin Budget"
    sql: ${TABLE}.daily_r0_margin_budget_col ;;
  }

  measure: r1_margin_budget {
    type: sum
    hidden: no
    label: "R1 Margin Budget"
    sql: ${TABLE}.daily_r1_margin_budget_col ;;
  }

  measure: r2_margin_budget {
    type: sum
    hidden: no
    label: "R2 Margin Budget"
    sql: ${TABLE}.daily_r2_margin_budget_col ;;
  }

  measure: count {
    type: count
    hidden: yes
    drill_fields: []
  }

##------------MTD R0, R1, R2 Sales and Margins Budgets Local Calculations

  ##sales
  measure: r0_mtd_sales_budgets {
    description: "R0 MTD Sales Budgets (Local Currency)"
    type: sum
    value_format_name: decimal_0
    sql: CASE WHEN ${date_date} >= DATE_TRUNC(CURRENT_DATE(), MONTH) AND ${date_date} < CURRENT_DATE() THEN (${TABLE}.daily_r0_sales_budget_col)
      ELSE null END ;;
  }

  measure: r1_mtd_sales_budgets {
    description: "R1 MTD Sales Budgets (Local Currency)"
    type: sum
    value_format_name: decimal_0
    sql: CASE WHEN ${date_date} >= DATE_TRUNC(CURRENT_DATE(), MONTH) AND ${date_date} < CURRENT_DATE() THEN (${TABLE}.daily_r1_sales_budget_col)
      ELSE null END ;;
  }

  measure: r2_mtd_sales_budgets {
    description: "R2 MTD Sales Budgets (Local Currency)"
    type: sum
    value_format_name: decimal_0
    sql: CASE WHEN ${date_date} >= DATE_TRUNC(CURRENT_DATE(), MONTH) AND ${date_date} < CURRENT_DATE() THEN (${TABLE}.daily_r2_sales_budget_col)
      ELSE null END ;;
  }

  ##margins
  measure: r0_mtd_margin_budgets {
    description: "R0 MTD Margin Budgets (Local Currency)"
    type: sum
    value_format_name: decimal_0
    sql: CASE WHEN ${date_date} >= DATE_TRUNC(CURRENT_DATE(), MONTH) AND ${date_date} < CURRENT_DATE() THEN (${TABLE}.daily_r0_margin_budget_col)
      ELSE null END ;;
  }

  measure: r1_mtd_margin_budgets {
    description: "R1 MTD Margin Budgets (Local Currency)"
    type: sum
    value_format_name: decimal_0
    sql: CASE WHEN ${date_date} >= DATE_TRUNC(CURRENT_DATE(), MONTH) AND ${date_date} < CURRENT_DATE() THEN (${TABLE}.daily_r1_margin_budget_col)
      ELSE null END ;;
  }

  measure: r2_mtd_margin_budgets {
    description: "R2 MTD Margin Budgets (Local Currency)"
    type: sum
    value_format_name: decimal_0
    sql: CASE WHEN ${date_date} >= DATE_TRUNC(CURRENT_DATE(), MONTH) AND ${date_date} < CURRENT_DATE() THEN (${TABLE}.daily_r2_margin_budget_col)
      ELSE null END ;;
  }

##------------YTD R0, R1, R2 Sales and Margins Budgets Local Calculations

  ##sales
  measure: r0_ytd_sales_budgets {
    description: "R0 YTD Sales Budgets (Local Currency)"
    type: sum
    value_format_name: decimal_0
    sql: CASE WHEN ${date_date} >= DATE_TRUNC(CURRENT_DATE(), YEAR) AND ${date_date} < CURRENT_DATE() THEN (${TABLE}.daily_r0_sales_budget_col)
      ELSE null END ;;
  }

  measure: r1_ytd_sales_budgets {
    description: "R1 YTD Sales Budgets (Local Currency)"
    type: sum
    value_format_name: decimal_0
    sql: CASE WHEN ${date_date} >= DATE_TRUNC(CURRENT_DATE(), YEAR) AND ${date_date} < CURRENT_DATE() THEN (${TABLE}.daily_r1_sales_budget_col)
      ELSE null END ;;
  }

  measure: r2_ytd_sales_budgets {
    description: "R2 YTD Sales Budgets (Local Currency)"
    type: sum
    value_format_name: decimal_0
    sql: CASE WHEN ${date_date} >= DATE_TRUNC(CURRENT_DATE(), YEAR) AND ${date_date} < CURRENT_DATE() THEN (${TABLE}.daily_r2_sales_budget_col)
      ELSE null END ;;
  }

  ##margins
  measure: r0_ytd_margin_budgets {
    description: "R0 YTD Margin Budgets (Local Currency)"
    type: sum
    value_format_name: decimal_0
    sql: CASE WHEN ${date_date} >= DATE_TRUNC(CURRENT_DATE(), YEAR) AND ${date_date} < CURRENT_DATE() THEN (${TABLE}.daily_r0_margin_budget_col)
      ELSE null END ;;
  }

  measure: r1_ytd_margin_budgets {
    description: "R1 YTD Margin Budgets (Local Currency)"
    type: sum
    value_format_name: decimal_0
    sql: CASE WHEN ${date_date} >= DATE_TRUNC(CURRENT_DATE(), YEAR) AND ${date_date} < CURRENT_DATE() THEN (${TABLE}.daily_r1_margin_budget_col)
      ELSE null END ;;
  }

  measure: r2_ytd_margin_budgets {
    description: "R2 YTD Margin Budgets (Local Currency)"
    type: sum
    value_format_name: decimal_0
    sql: CASE WHEN ${date_date} >= DATE_TRUNC(CURRENT_DATE(), YEAR) AND ${date_date} < CURRENT_DATE() THEN (${TABLE}.daily_r2_margin_budget_col)
      ELSE null END ;;
  }

}
