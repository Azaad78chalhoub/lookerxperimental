view: dim_bu_cdl {
  sql_table_name: `chb-prod-supplychain-data.prod_shared_dimensions.dim_bu_cdl_mch`
    ;;

  # dimension: pk {
  #   primary_key: yes
  #   hidden: yes
  #   sql: concat(${prod_num}, "-", ${org_num} ;;
  # }

  dimension: bu_code_1 {
    type: string
    hidden: yes
    sql: ${TABLE}.bu_code_1 ;;
  }

  dimension: bu_code_2 {
    type: string
    hidden: yes
    sql: ${TABLE}.bu_code_2 ;;
  }

  dimension: bu_code_3 {
    type: string
    hidden: yes
    sql: ${TABLE}.bu_code_3 ;;
  }

  dimension: bu_hier_code {
    type: string
    hidden: yes
    sql: ${TABLE}.bu_hier_code ;;
  }

  dimension: business_unit_final {
    type: string
    hidden: yes
    sql: ${TABLE}.business_unit_final ;;
  }

  dimension: channel_name {
    type: string
    hidden: yes
    sql: ${TABLE}.channel_name ;;
  }

  dimension: dl_class {
    type: number
    hidden: yes
    sql: ${TABLE}.dl_class ;;
  }

  dimension: dl_dept {
    type: number
    hidden: yes
    sql: ${TABLE}.dl_dept ;;
  }

  dimension: org_num {
    type: string
    hidden: yes
    sql: ${TABLE}.org_num ;;
  }

  dimension: prod_num {
    type: string
    hidden: yes
    sql: ${TABLE}.prod_num ;;
  }

  dimension: sku_class {
    type: number
    hidden: yes
    sql: ${TABLE}.sku_class ;;
  }

  dimension: sku_dept_no {
    type: number
    hidden: yes
    sql: ${TABLE}.sku_dept_no ;;
  }

  measure: count {
    type: count
    hidden: yes
    drill_fields: [channel_name]
  }
}
