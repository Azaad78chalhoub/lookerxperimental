view: cost_facts {

  derived_table: {
    sql_trigger_value: SELECT FLOOR(((TIMESTAMP_DIFF(CURRENT_TIMESTAMP(),'1970-01-01 00:00:00',SECOND)) - 60*60*5)/(60*60*24)) ;;

    sql:
    SELECT
    unit_cost_usd,
    unit_cost,
    status,
    stock_date,
    org_num,
    prod_num
    FROM `chb-prod-supplychain-data.prod_supply_chain.dm_soh_sales`
    WHERE stock_date > "2018-01-01" ;;
  }


  dimension: pk {
    hidden: yes
    primary_key: yes
    type: string
    sql: CONCAT(${stock_date_raw}, " - ", ${prod_num}, " - ", ${org_num}) ;;
  }

  dimension: org_num {
    type: string
    hidden: no
    view_label: "Product Data in RMS"
    label: "Location ID"
    sql: ${TABLE}.org_num ;;
  }

  dimension: prod_num {
    type: string
    hidden: no
    view_label: "Product Data in RMS"
    label: "Product ID"
    sql: ${TABLE}.prod_num ;;
  }

  dimension: stock_date_raw {
    type: date
    hidden: yes
    sql: ${TABLE}.stock_date ;;
  }

  dimension: unit_cost_usd {
    type: number
    view_label: "Product Data in RMS"
    label: "WAC USD"
    description: "Weighted Average Unit Cost (WAC) USD"
    value_format_name: usd
    hidden: no
    sql: ${TABLE}.unit_cost_usd ;;
  }

  dimension: unit_cost {
    type: number
    label: "WAC Local"
    description: "Weighted Average Unit Cost (WAC) Local"
    view_label: "Product Data in RMS"
    value_format_name: decimal_2
    hidden: no
    sql: ${TABLE}.unit_cost ;;
  }

  dimension: status {
    type: string
    view_label: "Product Data in RMS"
    hidden: no
    sql: ${TABLE}.status ;;
  }


}

# view: cost_table {
#   # Or, you could make this view a derived table, like this:
#   derived_table: {
#     sql: SELECT
#         user_id as user_id
#         , COUNT(*) as lifetime_orders
#         , MAX(orders.created_at) as most_recent_purchase_at
#       FROM orders
#       GROUP BY user_id
#       ;;
#   }
#
#   # Define your dimensions and measures here, like this:
#   dimension: user_id {
#     description: "Unique ID for each user that has ordered"
#     type: number
#     sql: ${TABLE}.user_id ;;
#   }
#
#   dimension: lifetime_orders {
#     description: "The total number of orders for each user"
#     type: number
#     sql: ${TABLE}.lifetime_orders ;;
#   }
#
#   dimension_group: most_recent_purchase {
#     description: "The date when each user last ordered"
#     type: time
#     timeframes: [date, week, month, year]
#     sql: ${TABLE}.most_recent_purchase_at ;;
#   }
#
#   measure: total_lifetime_orders {
#     description: "Use this for counting lifetime orders across many users"
#     type: sum
#     sql: ${lifetime_orders} ;;
#   }
# }
