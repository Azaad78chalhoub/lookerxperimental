view: budgetstarget {

  # added table below to get the targets in AED

  derived_table: {
   sql_trigger_value: SELECT FLOOR((TIMESTAMP_DIFF(CURRENT_TIMESTAMP(),'1970-01-01 00:00:00',SECOND)) / (6*60*60)) ;;
    sql:
with fixed_budgetstarget as(
SELECT targets.*,
budget_usd*conversion_rate AS budget_aed,
budget_usd_r1*conversion_rate AS budget_aed_r1,
budget_usd_r2*conversion_rate AS budget_aed_r2,
budget_usd_r2_alt*conversion_rate AS budget_aed_r2_alt,
budget_usd_r3*conversion_rate AS budget_aed_r3,
cogs_usd*conversion_rate AS cogs_aed,
target_margin*conversion_rate AS target_margin_aed,
target_margin_r1*conversion_rate AS target_margin_r1_aed,
target_margin_r2*conversion_rate AS target_margin_r2_aed,
target_margin_r3*conversion_rate AS target_margin_r3_aed,
target_usd*conversion_rate AS target_aed,
target_usd_r1*conversion_rate AS target_aed_r1,
target_usd_r2*conversion_rate AS target_aed_r2,
row_number() OVER (PARTITION by concat(targets.business_date, targets.bu_code, cast(targets.bk_store as string)) order by budget_usd DESC) as rown
FROM `chb-prod-supplychain-data.prod_supply_chain.budgets_target` targets
LEFT JOIN
`chb-prod-supplychain-data.prod_shared_dimensions.dim_conversion_daily_rates` rates
ON targets.business_date=DATE(rates.conversion_date)
AND from_currency="USD"
AND to_currency="AED"
AND conversion_type="Corporate"
)
SELECT * FROM fixed_budgetstarget
where rown = 1;;
  }


  label: "Targets"
  drill_fields: [id]

  dimension: id {
    primary_key: yes
    type: number
    hidden: yes
    sql: ${TABLE}.id ;;
  }

  dimension: _fivetran_deleted {
    type: yesno
    hidden: yes
    sql: ${TABLE}._fivetran_deleted ;;
  }

  dimension_group: _fivetran_synced {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}._fivetran_synced ;;
  }

  dimension: from_currency {
    type: string
    hidden: yes
    sql: "USD";;
  }

  dimension: to_currency {
    type: string
    hidden: yes
    sql: "AED";;
  }

  dimension: conversion_type {
    type: string
    hidden: yes
    sql: "1001";;
  }

  dimension: bk_store {
    type: string
    hidden: yes
    sql: ${TABLE}.bk_store ;;
  }

  dimension: bu_code {
    type: string
    hidden: yes
    sql: ${TABLE}.bu_code ;;
  }

  dimension: budget_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.budget_usd ;;
  }


  dimension: budget_aed {
    type: number
    hidden: yes
    sql: ${TABLE}.budget_aed ;;
  }

  dimension: budget_usd_r1 {
    type: number
    hidden: yes
    sql: ${TABLE}.budget_usd_r1 ;;
  }

  dimension: budget_usd_r2 {
    type: number
    hidden: no
    sql: ${TABLE}.budget_usd_r2 ;;
  }

  dimension: budget_usd_r2_alt {
    type: number
    hidden: yes
    sql: ${TABLE}.budget_usd_r2_alt ;;
  }

  dimension: budget_usd_r3 {
    type: number
    hidden: yes
    sql: ${TABLE}.budget_usd_r3 ;;
  }

  dimension_group: business {
    type: time
    timeframes: [
      raw,
      date,
      week,
      month,
      quarter,
      year
    ]
    convert_tz: no
    datatype: date
    hidden: yes
    sql: ${TABLE}.business_date ;;
  }

  dimension: cogs_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.cogs_usd ;;
  }

  dimension: month {
    type: string
    hidden: yes
    sql: ${TABLE}.month ;;
  }

  dimension: target_margin {
    type: number
    hidden: yes
    sql: ${TABLE}.target_margin ;;
  }

  dimension: target_margin_r1 {
    type: number
    hidden: yes
    sql: ${TABLE}.target_margin_r1 ;;
  }

  dimension: target_margin_r2 {
    type: number
    hidden: yes
    sql: ${TABLE}.target_margin_r2 ;;
  }

  dimension: target_margin_r3 {
    type: number
    hidden: yes
    sql: ${TABLE}.target_margin_r3 ;;
  }

  dimension: target_usd {
    type: number
    hidden: yes
    sql: ${TABLE}.target_usd ;;
  }

  dimension: target_usd_r1 {
    type: number
    hidden: yes
    sql: ${TABLE}.target_usd_r1 ;;
  }

  dimension: target_usd_r2 {
    type: number
    hidden: yes
    sql: ${TABLE}.target_usd_r2 ;;
  }


#   #################################
#   ######       MEASURES       #####
#   #################################

  measure: budget_r0_usd{
    type: sum
    label: "Budget R0 (USD)"
    value_format_name: usd
    sql: ${TABLE}.budget_usd ;;
  }

  measure: budget_r1_usd{
    type: sum
    label: "Budget R1 (USD)"
    value_format_name: usd
    sql: ${TABLE}.budget_usd_r1 ;;
  }

measure: budget_r2_usd{
  type: sum
  label: "Budget R2 (USD)"
  value_format_name: usd
  sql: ${TABLE}.budget_usd_r2 ;;
}

measure: budget_combined_usd {
  type: sum
  label: "Budget Combined (USD)"
  description: "Shows the newest value of the budget targets that is not 0"
  value_format_name: usd
  sql: COALESCE ( ${TABLE}.budget_usd_r2, ${TABLE}.budget_usd_r1, ${TABLE}.budget_usd);;
}

  measure: budget_r2_usd_alt{
    type: sum
    label: "Budget R2 (USD) - Including Concessions"
    value_format_name: usd
    sql: ${TABLE}.budget_usd_r2_alt ;;
  }

  measure: budget_r0_aed{
    type: sum
    value_format_name: decimal_2
    label: "Budget R0 (AED)"
    sql: ${budget_aed};;
  }

  measure: budget_r1_aed{
    type: sum
    label: "Budget R1 (AED)"
    value_format_name: decimal_2
    sql: ${TABLE}.budget_aed_r1 ;;
  }

  measure: budget_r2_aed{
    type: sum
    label: "Budget R2 (AED)"
    value_format_name: decimal_2
    sql: ${TABLE}.budget_aed_r2 ;;
  }

  measure: budget_combined_aed {
    type: sum
    label: "Budget Combined (AED)"
    description: "Shows the newest value of the budget targets that is not 0"
    value_format_name: decimal_2
    sql: COALESCE ( ${TABLE}.budget_aed_r2, ${TABLE}.budget_aed_r1, ${TABLE}.budget_aed);;
  }

  measure: budget_r2_aed_alt{
    type: sum
    label: "Budget R2 (AED) - Including Concessions"
    value_format_name: decimal_2
    sql: ${TABLE}.budget_aed_r2_alt ;;
  }
  # parameter: budget_selection {
  #   label: "Budget selection (USD)"
  #   view_label: "Parameters"
  #   type: string
  #   default_value: "R2"
  #   allowed_value: {
  #     label: "R2"
  #     value: "R2"
  #   }
  #   allowed_value: {
  #     label: "R0"
  #     value: "R0"
  #   }
  #   allowed_value: {
  #     label: "R1"
  #     value: "R1"
  #   }
  # }

  # measure: budgets_from_param{
  #   type: number
  #   label: "Budget (USD) based on user req"
  #   value_format_name: usd
  #   description: "Sales budget based on input from the user (R0,R1 or R2) "
  #   group_label: "Budget"
  #   sql:
  #   {% if fact_retail_sales.budget_selection._parameter_value == "'R2'" %}
  #   ${budget_r2_usd}
  #   {% elsif fact_retail_sales.budget_selection._parameter_value == "'R1'" %}
  #   ${budget_r1_usd}
  #   {% else %}
  #   ${budget_r0_usd}
  #   {% endif %};;
  # }

  # measure: budgets_from_param2{
  #   type: number
  #   label: "Budget (USD) based on user requirement"
  #   value_format_name: usd
  #   description: "Sales budget based on input from the user (R0,R1 or R2) "
  #   group_label: "Budget"
  #   sql:
  #   {% if fact_soh_sales_poc.budget_selection._parameter_value == "'R2'" %}
  #   ${budget_r2_usd}
  #   {% elsif fact_soh_sales_poc.budget_selection._parameter_value == "'R1'" %}
  #   ${budget_r1_usd}
  #   {% else %}
  #   ${budget_r0_usd}
  #   {% endif %};;
  # }
  measure: count {
    type: count
    hidden: yes
    drill_fields: [id]
  }
}
