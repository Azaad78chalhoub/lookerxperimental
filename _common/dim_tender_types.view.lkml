view: dim_tender_types {
  sql_table_name: `chb-prod-supplychain-data.prod_shared_dimensions.dim_tender_types`
    ;;

  view_label: "Tender Types"

  dimension_group: create {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.create_date ;;
    hidden: yes
  }

  dimension: currency_code {
    label: "Tender Type Currency Code"
    type: string
    sql: ${TABLE}.currency_code ;;
    hidden: yes
  }

  dimension_group: effective {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    hidden: yes
    sql: ${TABLE}.effective_date ;;
  }

  dimension: modify_date {
    type: string
    sql: ${TABLE}.modify_date ;;
    hidden: yes
  }

  dimension: tender_type_desc {
    label: "Tender Type Desc"
    type: string
    sql: ${TABLE}.tender_type_desc ;;
  }

  dimension: tender_type_group {
    label: "Tender Type Group"
    type: string
    sql: ${TABLE}.tender_type_group ;;
  }

  dimension: tender_type_id {
    type: number
    sql: ${TABLE}.tender_type_id ;;
    hidden: yes
    primary_key: yes
  }

}
