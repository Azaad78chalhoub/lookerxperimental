view: customer_fact_table {
  view_label: "Customer Information"

  derived_table: {
    explore_source: fact_retail_sales {
      column: customer_id {}
#       column: atr_muse_id {}
      # column: bk_customer {}
      column: customer_granularity {}
      column: net_sales_amount_usd {}
      column: transaction_count {}
#       column: current_period_transaction_count {}
#       column: previous_period_transaction_count {}
      column: cross_brand_adoption {}
      column: first_atr_trandate {}
      column: last_atr_trandate {}
      # filters: {
      #   field: fact_retail_sales.net_sales_amount_usd
      #   value: ">0"
      # }
      filters: {
        field: fact_retail_sales.atr_trandate_date
        value: "3 years"
        # value: ""
      }
      bind_filters: {
        from_field: fact_retail_sales.customer_granularity_parameter
        to_field: fact_retail_sales.customer_granularity_parameter
      }
    }
  }

  filter: customer_purchase_date {
    description: "Choose the date range that defines the period for the date filtered measures"
    type: date
  }

  dimension: pk {
    primary_key: yes
    hidden: yes
    type: string
    sql: CONCAT(CAST(${customer_id} AS STRING), CAST(${customer_granularity} AS STRING)) ;;
  }

  dimension: customer_id {
#     primary_key: yes
    hidden: yes
    label: "Sales Information Customer ID"
    sql: ${TABLE}.customer_id ;;
  }

  dimension: customer_granularity {
    label: "Brand / Group"
  }

  dimension: first_atr_trandate {
#     hidden: yes
    group_label: "Transaction Date Information"
    label: "First Transaction Date"
    type: date
  }

  dimension: last_atr_trandate {
    hidden: yes
    group_label: "Transaction Date Information"
    label: "Last Transaction Date"
    type: date
  }

  dimension: current_period_start {
    hidden: yes
    type: date
    sql: {% date_start customer_purchase_date %} ;;
  }

#   dimension: current_period_transaction_count {
#     type: number
#     sql: ${TABLE}.current_period_transaction_count ;;
#   }
#
#   dimension: previous_period_transaction_count {
#     type: number
#     sql: ${TABLE}.previous_period_transaction_count ;;
#   }

  dimension: current_period_end {
    hidden: yes
    type: date
    sql: DATE_SUB(CAST({% date_end customer_purchase_date %} AS DATE), INTERVAL 1 DAY) ;;
  }

  dimension: previous_period_start {
    hidden: yes
    type: date
    sql: DATE_SUB(CAST(${current_period_start} AS DATE), INTERVAL ${days_in_period} DAY) ;;
  }

  dimension: previous_period_end {
    hidden: yes
    type: date
    sql: DATE_SUB(CAST(${current_period_start} AS DATE), INTERVAL 1 DAY) ;;
  }

  dimension_group: in_period {
    hidden: yes
    type: duration
    intervals: [day]
    description: "Gives the number of days in the current period date range"
    sql_start: {% date_start customer_purchase_date %} ;;
    sql_end: {% date_end customer_purchase_date %} ;;
  }

  dimension: is_first_purchase {
#     hidden: yes
    type: yesno
    sql: ${fact_retail_sales.business_date} = ${first_atr_trandate} ;;
  }

  dimension: is_new_customer {
    description: "New Customer if the first purchase was in the Customer Purchase Date Filter"
    type: yesno
    sql: ${first_atr_trandate} between CAST({% date_start customer_purchase_date %} AS DATE) AND CAST({% date_end customer_purchase_date %} AS DATE) ;;
  }

  dimension: is_purchase_in_range {
#     hidden: yes
    type: yesno
    sql: ${fact_retail_sales.business_date} BETWEEN ${current_period_start} AND ${current_period_end} ;;
  }

  dimension: is_purchase_in_previous_range {
    # hidden: yes
    type: yesno
    sql: ${fact_retail_sales.business_date} BETWEEN ${previous_period_start} AND ${previous_period_end} ;;
    # sql:
    #   ${fact_retail_sales.business_date}
    #   BETWEEN
    #     DATE_SUB(CAST({% date_start customer_purchase_date %} AS DATE), INTERVAL ${days_in_period} DAY)
    #       AND
    #     DATE_SUB(CAST({% date_end customer_purchase_date %} AS DATE), INTERVAL ${days_in_period}-1 DAY) ;;
  }

  dimension: is_customer_purchase_in_current_and_previous_range {
#     hidden: yes
    type: yesno
    sql: ${is_purchase_in_range} AND ${is_purchase_in_previous_range} ;;
#     sql: ${current_period_transaction_count} > 0 AND ${previous_period_transaction_count} > 0 ;;
  }

  dimension: days_between_transaction_and_first_transaction {
    hidden: yes
    type: number
    sql: FLOOR(DATE_DIFF(${fact_retail_sales.atr_trandate_date}, ${first_atr_trandate}, day) / 365.0) ;;
  }

  dimension: days_between_first_last {
    hidden: yes
    type: number
    sql: date_diff(${last_atr_trandate}, ${first_atr_trandate}, day) ;;
  }

  dimension: ave_days_between_purchases {
    hidden: yes
    type: number
    sql: ${days_between_first_last}/nullif(${transaction_count} - 1,0) ;;
  }

  dimension: cross_brand_adoption {
    hidden: yes
    type: number
    sql: ${TABLE}.cross_brand_adoption ;;
  }

  dimension: transaction_count {
    hidden: yes
    type: number
    sql: ${TABLE}.transaction_count ;;
  }

  dimension: ltv {
    hidden: yes
    label: "LTV"
    type: number
    sql: ${TABLE}.net_sales_amount_usd ;;
  }

  dimension: tenure_days {
    description: "The number of days since first purchase"
    type: number
    sql: date_diff(current_date, ${first_atr_trandate}, day) ;;
  }

  dimension: tenure_days_tier {
    description: "The number of days since first purchase"
    type: tier
    tiers: [0,30,60,90,120]
    sql: ${tenure_days} ;;
  }

  dimension: days_since_last_purchase {
    group_label: "Transaction Date Information"
    description: "The number of days since latest purchase"
    type: number
    sql: date_diff(current_date, ${last_atr_trandate}, day) ;;
  }

  dimension: days_since_last_purchase_tier {
    group_label: "Transaction Date Information"
    description: "The number of days since latest purchase"
    type: tier
    tiers: [0,30,60,90,120]
    sql: ${days_since_last_purchase} ;;
  }

  measure: total_ltv {
    label: "Total LTV"
    description: "The total LTV per customer, can use Customer Granularity Parameter so breakdown measure"
    type: sum
    sql: ${ltv} ;;
    value_format_name: usd
  }

  measure: average_ltv {
    label: "Average LTV"
    description: "The average LTV per customer, can use Customer Granularity Parameter so breakdown measure"
    type: average
    sql: ${ltv} ;;
    value_format_name: usd
  }

  measure: total_active_years {
    description: "The number of years the user has been active since first purchase, can use Customer Granularity Parameter so breakdown measure"
    type: count_distinct
    sql: ${days_between_transaction_and_first_transaction} ;;
    value_format_name: decimal_0
  }

  measure: lt_average_annual_spend {
    label: "Average LT Annual Spend"
    description: "LTV / Active Years, can use Customer Granularity Parameter so breakdown measure"
    type: number
    sql: 1.0 * ${total_ltv} / NULLIF(${total_active_years},0) ;;
    value_format_name: usd
  }

  measure: ave_cross_brand_adoption {
    label: "Average Cross Brand Adoption"
    description: "The average number of brands purchased per customer"
    type:  average
    sql: ${cross_brand_adoption} ;;
    value_format_name: decimal_2
  }

  measure: ave_time_between_purchases {
    label: "Average Time Between Purchases"
    description: "The average number of days between purchases per customer"
    type: average
    sql: ${ave_days_between_purchases} ;;
    filters: [days_between_first_last: ">0"]
    value_format_name: decimal_1
    html: {{rendered_value}} days ;;
  }

  measure: average_tenure_days {
    description: "The average number of days since first purchase per customer"
    type: average
    sql: ${tenure_days} ;;
    value_format_name: decimal_1
    html: {{rendered_value}} days ;;
  }

  measure: average_days_since_last_purchase {
    description: "The average number of days since most recent purchase per customer"
    type: average
    sql: ${days_since_last_purchase} ;;
    value_format_name: decimal_1
    html: {{rendered_value}} days ;;
  }

  #####################################
  # CUSTOMER PURCHASE FILTER MEASURES #
  #####################################

  measure: new_customer_count {
    group_label: "Customer Purchase Date Filter Measures"
    description: "Count of customers who's first purchase date is in Customer Purchase Date Filter"
    type: count_distinct
    sql: ${customer_id} ;;
    filters: [is_new_customer: "yes"]
    value_format_name: decimal_0
  }

  measure: current_period_customer_count {
    group_label: "Customer Purchase Date Filter Measures"
    description: "Count of customers who's purchased in Customer Purchase Date Filter"
    type: count_distinct
    sql: ${customer_id} ;;
    filters: [is_purchase_in_range: "yes"]
    value_format_name: decimal_0
  }

  measure: recruitment_rate {
    group_label: "Customer Purchase Date Filter Measures"
    description: "Customers who had a first transaction in period / Customers who had a transaction in period"
    type: number
    sql: 1.0 * ${new_customer_count} / NULLIF(${current_period_customer_count}, 0) ;;
    value_format_name: percent_1
  }

  measure: new_customer_revenue {
    group_label: "Customer Purchase Date Filter Measures"
    description: "Revenue for customers who's first purchase date is in Customer Purchase Date Filter"
    type: sum
    sql: ${fact_retail_sales.amountusd_beforetax} ;;
    sql_distinct_key: ${fact_retail_sales.id} ;;
    filters: [is_new_customer: "yes"]
    value_format_name: usd
  }

  measure: current_period_customer_revenue {
    group_label: "Customer Purchase Date Filter Measures"
    description: "Revenue for customers who's purchased in Customer Purchase Date Filter"
    type: sum
    sql: ${fact_retail_sales.amountusd_beforetax} ;;
    sql_distinct_key: ${fact_retail_sales.id} ;;
    filters: [is_purchase_in_range: "yes"]
    value_format_name: usd
  }

  measure: previous_period_customer_revenue {
    group_label: "Customer Purchase Date Filter Measures"
    description: "Revenue for customers who's purchased in the Date Range previous to Customer Purchase Date Filter"
    type: sum
    sql: ${fact_retail_sales.amountusd_beforetax} ;;
    sql_distinct_key: ${fact_retail_sales.id} ;;
    filters: [is_purchase_in_previous_range: "yes"]
    value_format_name: usd
  }

  measure: retained_customer_revenue {
    group_label: "Customer Purchase Date Filter Measures"
    description: "Revenue for customers who's purchased in the both the Customer Purchase Date Filter and the previous Date Range"
    type: sum
    sql: ${fact_retail_sales.amountusd_beforetax} ;;
    sql_distinct_key: ${fact_retail_sales.id} ;;
    filters: [is_customer_purchase_in_current_and_previous_range: "yes"]
    value_format_name: usd
  }

  measure: previous_period_customer_count {
    group_label: "Customer Purchase Date Filter Measures"
    description: "Count of customers who's purchased in the Date Range previous to Customer Purchase Date Filter"
    type: count_distinct
    sql: ${customer_id} ;;
    filters: [is_purchase_in_previous_range: "yes"]
    value_format_name: decimal_0
  }

  measure: retained_customer_count {
    group_label: "Customer Purchase Date Filter Measures"
    description: "Count of customers who's purchased in the both the Customer Purchase Date Filter and the previous Date Range"
    type: count_distinct
    sql: ${customer_id} ;;
    filters: [is_customer_purchase_in_current_and_previous_range: "yes"]
    value_format_name: decimal_0
  }

  measure: retention_rate {
    group_label: "Customer Purchase Date Filter Measures"
    description: "Customers who had a transaction in both current and previous period / Customers who had a transaction in previous period"
    type: number
    sql: 1.0 * ${retained_customer_count} / NULLIF(${previous_period_customer_count}, 0) ;;
    value_format_name: percent_1
  }

}

view: customer_active_years {
  derived_table: {
    explore_source: fact_retail_sales {
      column: total_active_years { field: muse_customer_fact_table.total_active_years }
      column: customer_id {}
      column: customer_granularity {}
      filters: {
        field: fact_retail_sales.atr_trandate_date
        value: ""
      }
      bind_filters: {
        from_field: fact_retail_sales.customer_granularity_parameter
        to_field: fact_retail_sales.customer_granularity_parameter
      }
    }

  }
  dimension: total_active_years {
    hidden: yes
    type: number
  }

  dimension: pk {
    primary_key: yes
    hidden: yes
    type: string
    sql: CONCAT(CAST(${customer_id} AS STRING), CAST(${customer_granularity} AS STRING)) ;;
  }

  dimension: customer_id {
    hidden: yes
  }

  dimension: customer_granularity {
    hidden: yes
  }

  measure: average_active_years {
    type: average
    sql: ${total_active_years} ;;
    value_format_name: decimal_2
  }

}
