view: ch_concession {
  label: "Concession"
  sql_table_name: `chb-prod-supplychain-data.prod_shared_dimensions.dim_ch_concession`
    ;;


  dimension: pk {
    primary_key: yes
    hidden: yes
    sql: CONCAT(${class},${dept},${store},${concession_store}) ;;
  }


  dimension: concession_product {
    type: yesno
    hidden: no
    label: "Store Concession"
    view_label: "Concession"
    sql: ${concession_store} IS NOT NULL AND ${concessionstore_name} != "Non-Chalhoub";;
  }

#   dimension: concession_or_consigment_product {
#     type: yesno
#     hidden: no
#     label: "Concession or Consignment"
#     view_label: "Concession"
#     sql: ${concession_store} IS NOT NULL AND ${concessionstore_name} = "Non-Chalhoub";;
#   }

  dimension: ann_min_fee {
    type: number
    hidden: yes
    sql: ${TABLE}.ann_min_fee ;;
  }

  dimension: class {
    type: number
    hidden: yes
    sql: ${TABLE}.class ;;
  }

  dimension: concession_bu {
    type: string
    hidden: yes
    sql: ${TABLE}.concession_bu ;;
  }

  dimension: concession_bu_name {
    type: string
    hidden: yes
    sql: ${TABLE}.concession_bu_name ;;
  }

  dimension: concession_store {
    type: number
    hidden: yes
    sql: ${TABLE}.concession_store ;;
  }

  dimension: concessionstore_name {
    type: string
    label: "Concession Store"
    view_label: "Concession"
    hidden: no
    sql: ${TABLE}.concessionstore_name ;;
  }

  dimension_group: create {
    type: time
    hidden: yes
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: CAST(${TABLE}.create_date AS TIMESTAMP) ;;
  }

  dimension: create_id {
    type: string
    hidden: yes
    sql: ${TABLE}.create_id ;;
  }

  dimension: dept {
    type: number
    hidden: yes
    sql: ${TABLE}.dept ;;
  }

  dimension: ext_customer {
    type: number
    hidden: yes
    sql: ${TABLE}.ext_customer ;;
  }

  dimension: ext_supplier {
    type: number
    hidden: yes
    sql: ${TABLE}.ext_supplier ;;
  }

  dimension: inc_gen_freq {
    type: string
    hidden: yes
    sql: ${TABLE}.inc_gen_freq ;;
  }

  dimension: internal_ind {
    type: string
    hidden: yes
    sql: ${TABLE}.internal_ind ;;
  }

  dimension: non_chalhoub_cstore_ind {
    type: string
    hidden: yes
    sql: ${TABLE}.non_chalhoub_cstore_ind ;;
  }

  dimension: non_chalhoub_store_ind {
    type: string
    hidden: yes
    sql: ${TABLE}.non_chalhoub_store_ind ;;
  }

  dimension: process_mode {
    type: string
    hidden: yes
    sql: ${TABLE}.process_mode ;;
  }

  dimension: store {
    type: number
    hidden: yes
    sql: ${TABLE}.store ;;
  }

  dimension: store_name {
    type: string
    hidden: yes
    sql: ${TABLE}.store_name ;;
  }

  dimension: subclass {
    type: number
    hidden: yes
    sql: ${TABLE}.subclass ;;
  }

  dimension: is_ext_concession {
    type: yesno
    label: "External Concession"
    description: "Flag to identify external concession items"
    sql: ${concessionstore_name} like "%EXTERNAL CONCESSION%";;
  }

  dimension: is_int_concession {
    type: yesno
    label: "Internal Concession"
    description: "Flag to identify internal concession items"
    sql: ${concession_store} != 0 AND ${concessionstore_name} not like "%EXTERNAL CONCESSION%";;
  }


  measure: count {
    type: count
    hidden: yes
    drill_fields: [concessionstore_name, concession_bu_name, store_name]
  }
}
