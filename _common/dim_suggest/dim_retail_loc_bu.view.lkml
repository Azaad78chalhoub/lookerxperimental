view: dim_retail_loc_bu {
  derived_table: {
    persist_for: "24 hours"
    sql: SELECT DISTINCT business_unit FROM prod_supply_chain.dm_soh_sales WHERE stock_date > DATE_SUB(CURRENT_DATE(), INTERVAL 90 DAY) ;;
  }
  dimension: business_unit {}
}
