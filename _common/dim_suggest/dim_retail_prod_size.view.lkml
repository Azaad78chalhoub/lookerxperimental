view: dim_retail_prod_size {
  derived_table: {
    persist_for: "24 hours"
    sql: SELECT DISTINCT size_uda AS size FROM `chb-prod-supplychain-data.prod_shared_dimensions.dim_retail_product` ;;
  }
  dimension: size {}
}
