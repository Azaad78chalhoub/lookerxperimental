view: dim_retail_loc_store {
  derived_table: {
    persist_for: "24 hours"
    sql: SELECT distinct cast(store as string) location_id FROM prod_shared_dimensions.dim_retail_loc
          union all
          select distinct cast(wh as string)  from prod_shared_dimensions.dim_wh_loc
       ;;
  }

  dimension: location_id {}

}
