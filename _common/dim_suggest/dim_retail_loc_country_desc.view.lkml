view: dim_retail_loc_country_desc {
  derived_table: {
    persist_for: "24 hours"
    sql: SELECT DISTINCT country_desc FROM prod_supply_chain.dm_soh_sales WHERE stock_date = DATE_SUB(CURRENT_DATE(), INTERVAL 7 DAY) ;;
  }
  dimension: country_desc {}
}
