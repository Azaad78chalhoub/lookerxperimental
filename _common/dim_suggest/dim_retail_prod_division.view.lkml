view: dim_retail_prod_division {
  derived_table: {
    persist_for: "24 hours"
    sql: SELECT DISTINCT division FROM `chb-prod-supplychain-data.prod_shared_dimensions.dim_retail_product` ;;
  }
  dimension: division {}
}
