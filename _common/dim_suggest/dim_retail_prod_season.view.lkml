view: dim_retail_prod_season {
  derived_table: {
    persist_for: "24 hours"
    sql: SELECT DISTINCT season_desc AS season FROM `chb-prod-supplychain-data.prod_shared_dimensions.dim_retail_product` ;;
  }
  dimension: season {}
}
