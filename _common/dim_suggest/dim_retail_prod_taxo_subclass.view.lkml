view: dim_retail_prod_taxo_subclass {
  derived_table: {
    persist_for: "24 hours"
    sql: SELECT DISTINCT taxo_subclass FROM `chb-prod-supplychain-data.prod_shared_dimensions.dim_retail_product` ;;
  }
  dimension: taxo_subclass {}
}
