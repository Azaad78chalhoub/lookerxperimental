view: dim_retail_loc_district_name {
  derived_table: {
    persist_for: "24 hours"
    sql: SELECT DISTINCT district_name FROM prod_supply_chain.dm_soh_sales WHERE stock_date = DATE_SUB(CURRENT_DATE(), INTERVAL 7 DAY) ;;
  }
  dimension: district_name {}
}
