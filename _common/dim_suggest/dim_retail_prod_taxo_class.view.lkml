view: dim_retail_prod_taxo_class {
  derived_table: {
    persist_for: "24 hours"
    sql: SELECT DISTINCT taxo_class FROM `chb-prod-supplychain-data.prod_shared_dimensions.dim_retail_product` ;;
  }
  dimension: taxo_class {}
}
