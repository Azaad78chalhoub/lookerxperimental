view: dim_retail_loc_store_name {
  derived_table: {
    persist_for: "24 hours"
    sql: SELECT  store loc, store_name loc_name , store_name store_name FROM prod_shared_dimensions.dim_retail_loc
    union all
    select  wh loc, wh_name loc_name , null store_name from prod_shared_dimensions.dim_wh_loc
 ;;
  }
  dimension: loc_name {}

  dimension: loc {}

  dimension: store_name {}

}
