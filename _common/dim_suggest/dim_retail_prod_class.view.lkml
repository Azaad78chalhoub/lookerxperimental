view: dim_retail_prod_class {
  derived_table: {
    persist_for: "24 hours"
    sql: SELECT DISTINCT class_name FROM `chb-prod-supplychain-data.prod_shared_dimensions.dim_retail_product` ;;
  }
  dimension: class_name {}
}
