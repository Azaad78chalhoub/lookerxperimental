project_name: "chalhoub"

# # Use local_dependency: To enable referencing of another project
# # on this instance with include: statements
#
# local_dependency: {
#   project: "name_of_other_project"
# }

constant: drill_link_listener {
  # hidden drill link to listen to current filters
  value: "order_items.drill_link_listener._link"
}

constant: find_filters {
  value: "
  {% assign query_array = @{drill_link_listener} | split: '&' %}
  {% assign filters = '' %}
  {% for qs in query_array %}
  {% assign qs_check = qs | slice: 0,2 %}
  {% if qs_check contains 'f[' %}
  {% assign filters = filters | append: '&' | append: qs %}
  {% endif %}
  {% endfor %}
  {{ filters }}"
}


constant: granularity_dimension_label {
  value: "
  {% if granularity_of_analysis_parameter._parameter_value == 'overall' %}
  Select Dimension 1
  {% elsif granularity_of_analysis_parameter._parameter_value == 'brand' %}
  Brand
  {% elsif granularity_of_analysis_parameter._parameter_value == 'month' %}
  Month
  {% elsif granularity_of_analysis_parameter._parameter_value == 'week' %}
  Week
  {% elsif granularity_of_analysis_parameter._parameter_value == 'date' %}
  Date
  {% elsif granularity_of_analysis_parameter._parameter_value == 'item' %}
  Item
  {% elsif granularity_of_analysis_parameter._parameter_value == 'item_no' %}
  Item No
  {% elsif granularity_of_analysis_parameter._parameter_value == 'item_vpn' %}
  Item VPN
  {% elsif granularity_of_analysis_parameter._parameter_value == 'item_style_number' %}
  Item Style Number
  {% elsif granularity_of_analysis_parameter._parameter_value == 'class_name' %}
  Class
  {% elsif granularity_of_analysis_parameter._parameter_value == 'subclass_name' %}
  Sub Class
  {% elsif granularity_of_analysis_parameter._parameter_value == 'taxonomy_1' %}
  Taxonmy 1
  {% elsif granularity_of_analysis_parameter._parameter_value == 'taxonomy_2' %}
  Taxonmy 2
  {% elsif granularity_of_analysis_parameter._parameter_value == 'colour' %}
  Colour
  {% elsif granularity_of_analysis_parameter._parameter_value == 'size' %}
  Size
  {% elsif granularity_of_analysis_parameter._parameter_value == 'gender' %}
  Gender
  {% elsif granularity_of_analysis_parameter._parameter_value == 'item_grading' %}
  Item Grading
  {% elsif granularity_of_analysis_parameter._parameter_value == 'division' %}
  Division
  {% elsif granularity_of_analysis_parameter._parameter_value == 'season' %}
  Season
  {% elsif granularity_of_analysis_parameter._parameter_value == 'business_unit' %}
  Business Unit
  {% elsif granularity_of_analysis_parameter._parameter_value == 'country' %}
  Country
  {% elsif granularity_of_analysis_parameter._parameter_value == 'boat' %}
  Boat
  {% elsif granularity_of_analysis_parameter._parameter_value == 'district' %}
  District
  {% elsif granularity_of_analysis_parameter._parameter_value == 'store' %}
  Store
  {% else %}
  Select Dimension 1
  {% endif %}
  "
}

constant: granularity_dimension_label_b {
  value: "
  {% if granularity_of_analysis_parameter_b._parameter_value == 'overall' %}
  Select Dimension 2
  {% elsif granularity_of_analysis_parameter_b._parameter_value == 'brand' %}
  Brand
  {% elsif granularity_of_analysis_parameter_b._parameter_value == 'month' %}
  Month
  {% elsif granularity_of_analysis_parameter_b._parameter_value == 'week' %}
  Week
  {% elsif granularity_of_analysis_parameter_b._parameter_value == 'date' %}
  Date
  {% elsif granularity_of_analysis_parameter_b._parameter_value == 'item' %}
  Item
  {% elsif granularity_of_analysis_parameter_b._parameter_value == 'item_no' %}
  Item No
  {% elsif granularity_of_analysis_parameter_b._parameter_value == 'item_vpn' %}
  Item VPN
  {% elsif granularity_of_analysis_parameter_b._parameter_value == 'item_style_number' %}
  Item Style Number
  {% elsif granularity_of_analysis_parameter_b._parameter_value == 'class_name' %}
  Class
  {% elsif granularity_of_analysis_parameter_b._parameter_value == 'subclass_name' %}
  Sub Class
  {% elsif granularity_of_analysis_parameter_b._parameter_value == 'taxonomy_1' %}
  Taxonmy 1
  {% elsif granularity_of_analysis_parameter_b._parameter_value == 'taxonomy_2' %}
  Taxonmy 2
  {% elsif granularity_of_analysis_parameter_b._parameter_value == 'colour' %}
  Colour
  {% elsif granularity_of_analysis_parameter_b._parameter_value == 'size' %}
  Size
  {% elsif granularity_of_analysis_parameter_b._parameter_value == 'gender' %}
  Gender
  {% elsif granularity_of_analysis_parameter_b._parameter_value == 'item_grading' %}
  Item Grading
  {% elsif granularity_of_analysis_parameter_b._parameter_value == 'division' %}
  Division
  {% elsif granularity_of_analysis_parameter_b._parameter_value == 'season' %}
  Season
  {% elsif granularity_of_analysis_parameter_b._parameter_value == 'business_unit' %}
  Business Unit
  {% elsif granularity_of_analysis_parameter_b._parameter_value == 'country' %}
  Country
  {% elsif granularity_of_analysis_parameter_b._parameter_value == 'boat' %}
  Boat
  {% elsif granularity_of_analysis_parameter_b._parameter_value == 'district' %}
  District
  {% elsif granularity_of_analysis_parameter_b._parameter_value == 'store' %}
  Store
  {% else %}
  Select Dimension 2
  {% endif %}
  "
}
